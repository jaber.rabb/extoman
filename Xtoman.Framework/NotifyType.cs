﻿namespace Xtoman.Framework
{
    public enum NotifyType
    {
        Success,
        Error,
        Info
    }
}
