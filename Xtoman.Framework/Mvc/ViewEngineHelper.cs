﻿using System.Linq;
using System.Web.Mvc;

namespace Xtoman.Framework
{
    public static class ViewEngineHelper
    {
        public static RazorViewEngine DisableVbhtml(this RazorViewEngine engine)
        {
            engine.AreaViewLocationFormats = FilterOutVbhtml(engine.AreaViewLocationFormats);
            engine.AreaMasterLocationFormats = FilterOutVbhtml(engine.AreaMasterLocationFormats);
            engine.AreaPartialViewLocationFormats = FilterOutVbhtml(engine.AreaPartialViewLocationFormats);
            engine.ViewLocationFormats = FilterOutVbhtml(engine.ViewLocationFormats);
            engine.MasterLocationFormats = FilterOutVbhtml(engine.MasterLocationFormats);
            engine.PartialViewLocationFormats = FilterOutVbhtml(engine.PartialViewLocationFormats);
            engine.FileExtensions = FilterOutVbhtml(engine.FileExtensions);

            return engine;
        }

        public static string[] FilterOutVbhtml(string[] source)
        {
            return source.Where(s => !s.Contains("vbhtml")).ToArray();
        }
    }

    public class CSRazorViewEngine : RazorViewEngine
    {
        public CSRazorViewEngine()
        {
            AreaViewLocationFormats = ViewEngineHelper.FilterOutVbhtml(AreaViewLocationFormats);
            AreaMasterLocationFormats = ViewEngineHelper.FilterOutVbhtml(AreaMasterLocationFormats);
            AreaPartialViewLocationFormats = ViewEngineHelper.FilterOutVbhtml(AreaPartialViewLocationFormats);
            ViewLocationFormats = ViewEngineHelper.FilterOutVbhtml(ViewLocationFormats);
            PartialViewLocationFormats = ViewEngineHelper.FilterOutVbhtml(PartialViewLocationFormats);
            MasterLocationFormats = ViewEngineHelper.FilterOutVbhtml(MasterLocationFormats);
        }
    }
}
