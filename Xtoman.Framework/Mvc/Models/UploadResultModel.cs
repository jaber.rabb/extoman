﻿namespace Xtoman.Framework.Mvc
{
    public class AddResultModel : ResultModel
    {
        public object Id { get; set; }
    }

    public class UploadResultModel : ResultModel
    {
        public string Url { get; set; }
        public int MediaId { get; set; }
    }

    public class WalletAddressResult : ResultModel
    {
        public string Address { get; set; }
        public string PayTagId { get; set; }
    }
}
