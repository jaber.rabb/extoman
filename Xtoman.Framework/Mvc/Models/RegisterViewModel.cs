﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Framework.Mvc
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "نام")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }

        [EmailAddress]
        [Display(Name = "ایمیل")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "موبایل", ShortName = "09")]
        [RegularExpression(@"^09\d{9}$", ErrorMessage = "موبایل را بدرستی وارد کنید")]
        public string Mobile { get; set; }



        [DataType(DataType.PhoneNumber)]
        [Display(Name = "موبایل یا تلفن دوم (اختیاری)")]
        public string Phone { get; set; }
    }
}