﻿namespace Xtoman.Framework.Mvc
{
    public class ResultViewModel
    {
        public bool Status { get; set; }
        public string Text { get; set; }
    }
}
