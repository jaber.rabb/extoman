﻿namespace Xtoman.Framework.Mvc
{
    public class ResultModel
    {
        public bool Status { get; set; }
        public string Text { get; set; }
    }
}
