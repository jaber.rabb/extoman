﻿using AutoMapper;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Xtoman.Domain.Models;

namespace Xtoman.Framework.Mvc
{
    ///// <summary>
    ///// just for mark viewmodels
    ///// </summary>
    //public interface IBaseViewModel
    //{
    //    int Id { get; set; }
    //    void BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext);
    //}

    /// <summary>
    /// Base view model
    /// </summary>
    [ModelBinder(typeof(BaseViewModelBinder))]
    public abstract class BaseViewModel/* : IBaseViewModel*/
    {
        [Display(Name = "ردیف")]
        public virtual int Id { get; set; }

        public virtual void BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
        }
    }

    public abstract class BaseViewModel<TViewModel, TEntity> : BaseViewModel where TViewModel : class where TEntity : class, IBaseEntity
    {
        /// <summary>
        /// Maps the specified view model to a entity object.
        /// </summary>
        public TEntity ToEntity()
        {
            return Mapper.Map<TEntity>(CastToDerivedClass(this));
        }

        /// <summary>
        /// Maps the specified view model to a entity object.
        /// </summary>
        public TEntity ToEntity(TEntity entity)
        {
            return Mapper.Map(CastToDerivedClass(this), entity);
        }

        /// <summary>
        /// Maps a entity to a view model instance.
        /// </summary>
        public TViewModel FromEntity(TEntity model)
        {
            return Mapper.Map<TViewModel>(model);
        }

        public ICollection<TViewModel> FromEntity(ICollection<TEntity> model)
        {
            return Mapper.Map<ICollection<TViewModel>>(model);
        }

        #region Private
        /// <summary>
        /// Gets the derived class.
        /// </summary>
        protected TViewModel CastToDerivedClass(BaseViewModel<TViewModel, TEntity> baseInstance)
        {
            return Mapper.Map<TViewModel>(baseInstance);
        }
        #endregion
    }
}
