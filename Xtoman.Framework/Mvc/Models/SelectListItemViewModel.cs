﻿using Xtoman.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class SelectListItemViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }
    }

    public static class SelectListItemViewModelHelper
    {
        public static SelectList SelectList(this List<SelectListItemViewModel> items, object selectedValue = null)
        {
            return selectedValue != null ?
                new SelectList(items, "Id", "Name", "Group", selectedValue, null) :
                new SelectList(items, "Id", "Name", "Group");
        }
    }
}
