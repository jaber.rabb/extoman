﻿
namespace Xtoman.Framework.Mvc
{
    public class DeleteConfirmationModel : BaseViewModel
    {
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string WindowId { get; set; }
    }
}