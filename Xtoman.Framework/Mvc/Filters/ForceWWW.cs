﻿using Xtoman.Utility;
using System;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class ForceWWWAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ModifyUrlAndRedirectPermanent(filterContext);
            base.OnActionExecuting(filterContext);
        }

        private static void ModifyUrlAndRedirectPermanent(ActionExecutingContext filterContext)
        {
            if (CanIgnoreRequest(filterContext))
                return;

            var url = GetAbsuloteUrl(filterContext.RequestContext.HttpContext.Request.Url); //filterContext.RequestContext.HttpContext.Request.Url.AbsoluteUri.ToString(CultureInfo.InvariantCulture)
            var absoluteUrl = HttpUtility.UrlDecode(filterContext.RequestContext.HttpContext.Request.Url.AbsoluteUri.ToString(CultureInfo.InvariantCulture));
            var absoluteUrlToLower = ToLowerCase(filterContext, absoluteUrl); //filterContext.RequestContext.HttpContext.Request.Url.AbsoluteUri.ToLowerInvariant();

            absoluteUrlToLower = ForceWwwAndCom(filterContext, absoluteUrlToLower);
            absoluteUrlToLower = AvoidTrailingSlashes(filterContext, absoluteUrlToLower);

            File.AppendAllLines($@"C:\inetpub\Xtoman.com\UploadFiles\\a.txt", new[] { $"url : {url}" });
            File.AppendAllLines($@"C:\inetpub\Xtoman.com\UploadFiles\\a.txt", new[] { $"absoluteUrlToLower : {absoluteUrlToLower}" });
            if (!url.Equals(absoluteUrlToLower))
            {
                filterContext.Result = new RedirectResult(absoluteUrlToLower, permanent: true);
            }
        }

        private static string AvoidTrailingSlashes(ActionExecutingContext filterContext, string absoluteUrl)
        {
            if (!IsRootRequest(filterContext) && absoluteUrl.EndsWith("/") && AppSettingManager.AvoidTrailingSlash)
                return absoluteUrl.TrimEnd('/');
            return absoluteUrl;
        }

        private static bool IsRootRequest(ActionExecutingContext filterContext)
        {
            return filterContext.RequestContext.HttpContext.Request.Url.AbsolutePath == "/";
        }

        private static bool CanIgnoreRequest(ActionExecutingContext filterContext)
        {
            return filterContext.IsChildAction
                   || filterContext.RequestContext.HttpContext.Request.IsAjaxRequest()
                   || filterContext.RequestContext.HttpContext.Request.IsLocal;
            //|| filterContext.RequestContext.HttpContext.Request.Url.AbsoluteUri.Contains("?");
        }

        private static string ToLowerCase(ActionExecutingContext filterContext, string absoluteUrl)
        {
            var segments = filterContext.RequestContext.HttpContext.Request.Url.Segments;
            if (segments.Length == 2 && segments[1].Trim('/').Length == 4)
                return absoluteUrl;

            //var query = filterContext.RequestContext.HttpContext.Request.Url.Query;
            var query = new UriBuilder(filterContext.RequestContext.HttpContext.Request.Url.AbsoluteUri).Query.UrlDecode();

            var result = absoluteUrl.ToLowerInvariant();
            if (query.HasValue())
                result = result.Replace(query.ToLowerInvariant(), query);
            return result;
        }

        private static string ForceWwwAndCom(ActionExecutingContext filterContext, string absoluteUrl)
        {
            var builder = new UriBuilder(absoluteUrl);
            if (!builder.Host.StartsWith("www") && AppSettingManager.ForceToWww)
                builder.Host = "www." + builder.Host;
            if (builder.Scheme == "http" /*!filterContext.RequestContext.HttpContext.Request.IsSecureConnection */&& AppSettingManager.ForceToHttps)
                builder.Scheme = "https";

            var url = GetAbsuloteUrl(builder.Uri);
            return url;

            //if (absoluteUrl.StartsWith("http://www") || absoluteUrl.StartsWith("https://www"))
            //    return absoluteUrl;
            //var result = absoluteUrl.Replace("http://", "http://www.").Replace("https://", "https://www.");
            //return result;//.Replace("birij.net", "birij.com").Replace("birij.ir", "birij.com");
        }

        private static string GetAbsuloteUrl(Uri uri)
        {
            //return uri.GetComponents(UriComponents.Scheme | UriComponents.Host | UriComponents.PathAndQuery, UriFormat.UriEscaped);
            return uri.GetComponents(UriComponents.AbsoluteUri & ~UriComponents.Port, UriFormat.UriEscaped);
        }
    }
}