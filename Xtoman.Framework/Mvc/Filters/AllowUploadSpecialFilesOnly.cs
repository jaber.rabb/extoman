﻿using Xtoman.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public enum FileExtentions
    {
        [Description(".jpg,.jpeg,.gif,.png,.bmp")]
        Images
    }

    public class AllowUploadSpecialFilesOnlyAttribute : ActionFilterAttribute
    {
        readonly List<string> _toFilter = new List<string>();
        private string _extensionsWhiteList;

        public AllowUploadSpecialFilesOnlyAttribute(FileExtentions extention)
        {
            var extensionsWhiteList = extention.ToDescription();
            AddWhiteList(extensionsWhiteList);
        }

        public AllowUploadSpecialFilesOnlyAttribute(string extensionsWhiteList)
        {
            AddWhiteList(extensionsWhiteList);
        }

        private void AddWhiteList(string extensionsWhiteList)
        {
            CheckHelper.NotEmpty(extensionsWhiteList, nameof(extensionsWhiteList));

            _extensionsWhiteList = extensionsWhiteList;
            var extensions = extensionsWhiteList.Split(',');
            foreach (var ext in extensions.Where(ext => !string.IsNullOrWhiteSpace(ext)))
            {
                _toFilter.Add(ext.ToLowerInvariant().Trim());
            }
        }

        bool canUpload(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName)) return false;

            var ext = Path.GetExtension(fileName.ToLowerInvariant());
            return _toFilter.Contains(ext);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var files = filterContext.HttpContext.Request.Files;
            foreach (string file in files)
            {
                var postedFile = files[file];
                if (postedFile == null || postedFile.ContentLength == 0) continue;

                if (!canUpload(postedFile.FileName))
                {
                    throw new InvalidOperationException(
                       string.Format("You are not allowed to upload {0} file. Please upload only these files: {1}.",
                                       Path.GetFileName(postedFile.FileName),
                                       _extensionsWhiteList));
                }
            }

            base.OnActionExecuting(filterContext);
        }

        public static bool IsImageFile(HttpPostedFileBase photoFile)
        {
            using (var img = Image.FromStream(photoFile.InputStream))
            {
                return img.Width > 0;
            }
        }
    }
}
