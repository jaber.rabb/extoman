﻿using Xtoman.Utility;
using System;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class PreventSpamAttribute : ActionFilterAttribute
    {
        public int DelayRequest = 10;
        public bool AddAddress = true;
        // باید نتیجه اجرای اکشن را بررسی کند یا خیر
        public bool CheckResult = true;
        public string ErrorMessage = "درخواست‌های شما در مدت زمان معقولی صورت نگرفته است.";
        private string HashValue = "";

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var cache = context.HttpContext.Cache;
            HashValue = MakeKey(context.HttpContext);

            // ابتدا چک می‌کنیم که آیا شناسه‌ی یکتای درخواست در کش موجود نباشد
            if (cache[HashValue] != null)
            {
                // یک خطا اضافه می‌کنیم ModelState اگر موجود بود یعنی کمتر از زمان موردنظر درخواست مجددی صورت گرفته و به
                context.Controller.ViewData.ModelState.AddModelError("ExcessiveRequests", ErrorMessage);
            }
            else
            {
                // اگر موجود نبود یعنی درخواست با زمانی بیشتر از مقداری که تعیین کرده‌ایم انجام شده
                // پس شناسه درخواست جدید را با پارامتر زمانی که تعیین کرده بودیم به شیئ کش اضافه می‌کنیم
                cache.Add(HashValue, true, null, DateTime.Now.AddSeconds(DelayRequest), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
            }

            base.OnActionExecuting(context);
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (CheckResult && context.Controller.ViewBag.ExecuteResult != null && context.Controller.ViewBag.ExecuteResult != true)
            {
                var cache = context.HttpContext.Cache;
                if (cache[HashValue] != null)
                    cache.Remove(HashValue);
            }
            base.OnActionExecuted(context);
        }

        private string MakeKey(HttpContextBase context)
        {
            var request = context.Request;
            var cache = context.Cache;
            var IP = HttpContextHelper.IpAddress(context);
            var targetInfo = AddAddress ? (request.RawUrl + request.QueryString) : "";
            var browser = request.UserAgent;
            var Uniquely = string.Concat(IP, targetInfo/*, browser*/);
            var hashValue = Uniquely.ToMd5Hash(); /*string.Join("", MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(Uniquely)).Select(s => s.ToString("x2")))*/
            return hashValue;
        }
    }
}

//[HttpPost]
//[PreventSpam(AddAddress = true, DelayRequest = 20)]
//[ValidateAntiForgeryToken]
//public Task<ActionResult> InsertPost(NewPostModel model)
//{
//    if (ModelState.IsValid)
//    {
//        var newPost = dbContext.InsertPost(model);
//        if (newPost != null)
//        {
//            ViewBag.ExecuteResult = true;
//        }
//    }
//    if (ModelState.IsValidField("ExcessiveRequests") == true)
//    {
//        ViewBag.ExecuteResult = false;
//    }
//    return View();
//}