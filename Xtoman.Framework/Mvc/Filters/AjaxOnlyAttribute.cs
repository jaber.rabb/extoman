﻿using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class AjaxOnlyAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                base.OnActionExecuting(filterContext);
            }
            else
            {
                filterContext.Result = new HttpNotFoundResult();
                //throw new InvalidOperationException("This operation can only be accessed via Ajax requests");
            }
        }
    }

    public class NotAjaxAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAjaxRequest())
            {
                base.OnActionExecuting(filterContext);
            }
            else
            {
                filterContext.Result = new HttpNotFoundResult();
                //throw new InvalidOperationException("This operation can only be accessed via Ajax requests");
            }
        }
    }
}
