﻿using System;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class SeconsToTimeSpanModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value == null || string.IsNullOrEmpty(value.AttemptedValue))
                return null;
            try
            {
                return TimeSpan.FromSeconds(Convert.ToDouble(value.AttemptedValue));
            }
            catch (FormatException e)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, e);
                return null;
            }
        }
    }
}
