﻿using System.Linq;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class IntArrayModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value == null || string.IsNullOrEmpty(value.AttemptedValue))
                return null;

            return value
                .AttemptedValue
                .Split(',')
                .Select(int.Parse)
                .ToArray();
        }
        //http://localhost:54119/Designs/Multiple?ids=24041&ids=24117
        //public ActionResult Multiple([ModelBinder(typeof(IntArrayModelBinder))] int[] ids)
        //{
        //}
        //ModelBinders.Binders.Add(typeof(int[]), new IntArrayModelBinder());
    }
}
