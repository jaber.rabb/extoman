﻿using Xtoman.Utility;
using System;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class IntModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            try
            {
                var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
                if (value == null || string.IsNullOrEmpty(value.AttemptedValue))
                    return null;

                return Convert.ToInt32(value.AttemptedValue.Trim().Fa2En().Replace(",", ""));
            }
            catch (Exception e)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, e);
                return 0;
            }
        }
    }

    public class DecimalModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            try
            {
                var valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
                if (valueResult != null && valueResult.AttemptedValue.Trim() != "")
                {
                    return Convert.ToDecimal(valueResult.AttemptedValue.Trim().Fa2En().Replace(",", ""));
                }
            }
            catch (Exception e)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, e);
            }
            return 0;
        }
    }
}
