﻿using Xtoman.Utility;
using System;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class StringModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            try
            {
                string result = null;
                if (controllerContext.Controller.ValidateRequest && bindingContext.ModelMetadata.RequestValidationEnabled)
                    result = bindingContext.ValueProvider.GetValue(bindingContext.ModelName)?.AttemptedValue;
                else
                    result = controllerContext.HttpContext.Request.Unvalidated().Form[bindingContext.ModelName];

                if (string.IsNullOrEmpty(result))
                    return null;

                return result?.FixPersianChars().Fa2En().Trim();//.CleanString();
            }
            catch (Exception e)
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, e);
                return null;
            }
        }
    }
}
