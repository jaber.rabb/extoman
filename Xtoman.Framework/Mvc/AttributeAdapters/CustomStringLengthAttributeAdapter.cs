﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class CustomStringLengthAttributeAdapter : StringLengthAttributeAdapter
    {
        public CustomStringLengthAttributeAdapter(ModelMetadata metadata, ControllerContext context, StringLengthAttribute attribute)
            : base(metadata, context, attribute)
        {
            if (attribute.ErrorMessage == null)
            {
                var message = "";
                if (attribute.MinimumLength != 0)
                    message += " باید حداقل {2} حرف";
                if (attribute.MaximumLength != int.MaxValue)
                    message += (message == "" ? "باید حداکثر " : " و حداکثر ") + " {1} حرف";
                message += " باشد";
                attribute.ErrorMessage = "{0} " + message;
            }
        }
    }
}
