﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class CustomMinLengthAttributeAdapter : MinLengthAttributeAdapter
    {
        public CustomMinLengthAttributeAdapter(ModelMetadata metadata, ControllerContext context, MinLengthAttribute attribute)
            : base(metadata, context, attribute)
        {
            if (attribute.ErrorMessage == null)
            {
                var message = "{0} حداقل باید {1} " + (metadata.ModelType == typeof(string) ? "حرف" : "رقم") + " باشد";
                attribute.ErrorMessage = "" + message;
            }
        }
    }
}
