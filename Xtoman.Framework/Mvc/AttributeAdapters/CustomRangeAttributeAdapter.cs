﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class CustomRangeAttributeAdapter : RangeAttributeAdapter
    {
        public CustomRangeAttributeAdapter(ModelMetadata metadata, ControllerContext context, RangeAttribute attribute)
            : base(metadata, context, attribute)
        {
            if (attribute.ErrorMessage == null)
            {
                var message = "";
                if (metadata.ModelType == typeof(string))
                {
                    if ((int)attribute.Minimum != 0)
                        message += " باید حداقل {2} حرف";
                    if ((int)attribute.Maximum != int.MaxValue)
                        message += (message == "" ? "باید حداکثر " : " و حداکثر ") + " {1} حرف";
                }
                else
                {
                    if ((int)attribute.Minimum != 0)
                        message += " باید حداقل {2}";
                    if ((int)attribute.Maximum != int.MaxValue)
                        message += (message == "" ? "باید حداکثر " : " و حداکثر ") + " {1}";
                }
                message += " باشد";
                attribute.ErrorMessage = "{0} " + message;
            }
        }
    }
}
