﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class CustomModelMetadataProvider : DataAnnotationsModelMetadataProvider
    {
        protected override ModelMetadata CreateMetadata(
                                 IEnumerable<Attribute> attributes,
                                 Type containerType,
                                 Func<object> modelAccessor,
                                 Type modelType,
                                 string propertyName)
        {
            //var attributeList = attributes.ToList();
            //if (attributeList.OfType<CurrencyAttribute>().Any())
            //{
            //    attributeList.Add(new UIHintAttribute("Currency"));
            //    attributeList.Add(new DisplayFormatAttribute
            //    {
            //        ApplyFormatInEditMode = true,
            //        DataFormatString = "{0:C}"
            //    });
            //}
            //var description = attributes.SingleOrDefault(a => typeof(DescriptionAttribute) == a.GetType());
            //if (description != null)
            //    metadata.Description = ((DescriptionAttribute)description).Description;
            //var displayName = (attributes.SingleOrDefault(a => typeof(DisplayAttribute) == a.GetType()) as DisplayAttribute)?.Name;
            //if (containerType != null)
            //{
            //    var displayName = metadata.GetDisplayName();
            //    metadata.DisplayName = Translator.Translate(displayName);
            //}


            var metadata = base.CreateMetadata(attributes, containerType, modelAccessor, modelType, propertyName);
            var displayName = metadata.GetDisplayName();
            if (propertyName != null)
            {
                var a = metadata.DisplayName;
            }
            return metadata;
        }
    }

    //public class ConventionMetadataProvider : AssociatedMetadataProvider
    //{
    //    protected override ModelMetadata CreateMetadata(IEnumerable<Attribute> attributes, Type containerType, Func<object> modelAccessor, Type modelType, string propertyName)
    //    {
    //        var metadata = new ModelMetadata(this, containerType, modelAccessor, modelType, propertyName);
    //        if (propertyName == null)
    //            return metadata;
    //        if (propertyName.EndsWith("Id"))
    //        {
    //            metadata.TemplateHint = "HiddenInput";
    //            metadata.HideSurroundingHtml = true;
    //            /* note that this property will be changed to HideSurroundingHtml in a future release. */
    //        }
    //        if (propertyName.EndsWith("X"))
    //        {
    //            metadata.IsRequired = true;
    //        }
    //        metadata.DisplayName = propertyName;
    //        return metadata;
    //    }
    //}

}
