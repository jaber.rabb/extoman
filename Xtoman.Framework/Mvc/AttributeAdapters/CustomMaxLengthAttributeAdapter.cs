﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class CustomMaxLengthAttributeAdapter : MaxLengthAttributeAdapter
    {
        public CustomMaxLengthAttributeAdapter(ModelMetadata metadata, ControllerContext context, MaxLengthAttribute attribute)
            : base(metadata, context, attribute)
        {
            if (attribute.ErrorMessage == null)
            {
                var message = "{0} حداکثر باید {1} " + (metadata.ModelType == typeof(string) ? "حرف" : "رقم") + " باشد";
                attribute.ErrorMessage = "" + message;
            }
        }
    }
}
