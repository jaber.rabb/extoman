﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public static class ImageHelper
    {
        public static MvcHtmlString ImageLink(this HtmlHelper htmlHelper, string imgSrc, string alt, string actionName, string controllerName, object routeValues, object htmlAttributes, object imgHtmlAttributes)
        {
            var urlHelper = ((Controller)htmlHelper.ViewContext.Controller).Url;
            var imgTag = new TagBuilder("img");
            imgTag.MergeAttribute("src", imgSrc);
            imgTag.MergeAttributes((IDictionary<string, string>)imgHtmlAttributes, true);
            var url = urlHelper.Action(actionName, controllerName, routeValues);

            var imglink = new TagBuilder("a");
            imglink.MergeAttribute("href", url);
            imglink.InnerHtml = imgTag.ToString();
            imglink.MergeAttributes((IDictionary<string, string>)htmlAttributes, true);

            return new MvcHtmlString(imglink.ToString());
        }

        public static MvcHtmlString Image(this HtmlHelper helper, string src, string alt = null, object htmlAttributes = null)
        {
            var tb = new TagBuilder("img");
            tb.Attributes.Add("src", helper.Encode(src));
            if (alt != null)
                tb.Attributes.Add("alt", helper.Encode(alt));
            if (htmlAttributes != null)
            {
                var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
                tb.MergeAttributes(attributes);
            }
            return new MvcHtmlString(tb.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString ImageFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, object htmlAttributes, string noImage, params string[] folders)
        {
            var data = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
            var path = "~/";
            foreach (var folderName in folders)
            {
                path += folderName + "/";
            }
            path += (data.Model == null ? noImage : data.Model.ToString());

            var img = new TagBuilder("img");
            img.Attributes.Add("src", UrlHelper.GenerateContentUrl(path, helper.ViewContext.HttpContext));

            if (htmlAttributes != null)
            {
                var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
                img.MergeAttributes(attributes);
            }

            return new MvcHtmlString(img.ToString());
        }
    }
}