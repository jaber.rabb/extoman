﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Xtoman.Framework.Mvc
{
    public static class FormHelper
    {
        public static MvcForm BeginForm(this HtmlHelper htmlHelper, string url, FormMethod method, object htmlAttributes = null)
        {
            var attributes = new Dictionary<string, object>();
            attributes.Add("action", url);
            if (htmlAttributes != null)
            {
                var properties = htmlAttributes.GetType().GetProperties();
                foreach (var propertyInfo in properties)
                    attributes.Add(propertyInfo.Name, propertyInfo.GetValue(htmlAttributes, null));
            }
            return htmlHelper.BeginForm(null, null, method, attributes);
        }
    }
}

