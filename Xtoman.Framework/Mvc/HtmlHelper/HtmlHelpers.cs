﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using Xtoman.Framework.DependencyResolution;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Framework.Mvc
{
    public static class HtmlHelpers
    {
        public static string ToDescription<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            var attributes = (DescriptionAttribute[])expression.GetPropInfo().GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : expression.Name;
        }

        public static string ToDisplay<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, DisplayProperty property = DisplayProperty.Name)
        {
            var attributes = (DisplayAttribute[])expression.GetPropInfo().GetCustomAttributes(typeof(DisplayAttribute), false);
            if (attributes.Length > 0)
            {
                var attr = attributes[0];
                return attr.GetType().GetProperty(property.ToString()).GetValue(attr, null) as string;
            }
            return expression.Name;
        }

        //In >= MVC4, just use @Html.DisplayNameFor
        //Before that : use your own Helper
        public static MvcHtmlString GetDisplayName<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression
        )
        {
            var metaData = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData);
            string value = metaData.DisplayName ?? (metaData.PropertyName ?? ExpressionHelper.GetExpressionText(expression));
            return MvcHtmlString.Create(value);
        }

        private class ViewDataContainer : IViewDataContainer
        {
            public ViewDataDictionary ViewData { get; set; }

            public ViewDataContainer(ViewDataDictionary viewData)
            {
                ViewData = viewData;
            }
        }

        public static HelperResult LocalizedEditor<T, TLocalizedModelLocal>(this HtmlHelper<List<T>> helper,
            string name,
            Func<int, HelperResult> localizedTemplate,
            Func<T, HelperResult> standardTemplate,
            T model)
            where T : ILocalizedModel<TLocalizedModelLocal>
            where TLocalizedModelLocal : ILocalizedModelLocal
        {
            var viewContext = new ViewContext();

            //viewContext.Controller = helper.ViewContext.Controller;
            //viewContext.RequestContext = helper.ViewContext.RequestContext;
            //viewContext.HttpContext = helper.ViewContext.HttpContext;
            //viewContext.FormContext = helper.ViewContext.FormContext;
            //viewContext.ClientValidationEnabled = helper.ViewContext.ClientValidationEnabled;
            //viewContext.View = helper.ViewContext.View;
            //viewContext.TempData = helper.ViewContext.TempData;

            Mapper.Map(helper.ViewContext, viewContext);
            viewContext.ViewData = new ViewDataDictionary<T>(model);
            var viewDataContainer = new ViewDataContainer(viewContext.ViewData);
            var htmlHelper = new HtmlHelper<T>(viewContext, viewDataContainer);
            return htmlHelper.LocalizedEditor<T, TLocalizedModelLocal>(name, localizedTemplate, standardTemplate);
        }

        public static HelperResult LocalizedEditor<T, TLocalizedModelLocal>(this HtmlHelper<T> helper,
            string name,
            Func<int, HelperResult> localizedTemplate,
            Func<T, HelperResult> standardTemplate)
            where T : ILocalizedModel<TLocalizedModelLocal>
            where TLocalizedModelLocal : ILocalizedModelLocal
        {
            return new HelperResult(writer =>
            {
                var localizationSupported = helper.ViewData.Model.Locales.Count > 0;

                if (localizationSupported)
                {
                    var tabStrip = new StringBuilder();
                    var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

                    tabStrip.AppendLine($"<div id=\"{name}\" class=\"nav-tabs-custom nav-tabs-localized-fields\">");
                    tabStrip.AppendLine("<ul class=\"nav nav-tabs\">");

                    //default tab
                    tabStrip.AppendLine("<li class=\"active\">");
                    tabStrip.AppendLine(
                        $"<a data-tab-name=\"{name}-fa-IR-tab\" href=\"#{name}-fa-IR-tab\" data-toggle=\"tab\"><img alt=\'\' src=\'{urlHelper.Content("~/Content/flags/ir.png")}\'> فارسی</a>");
                    tabStrip.AppendLine("</li>");

                    var languageService = IoC.Container.GetInstance<ILanguageService>();
                    foreach (var locale in helper.ViewData.Model.Locales)
                    {
                        //languages
#pragma warning disable CS0618 // Type or member is obsolete
                        var language = languageService.GetById(locale.LanguageId);
#pragma warning restore CS0618 // Type or member is obsolete
                        if (language == null)
                            throw new Exception("Language cannot be loaded");

                        tabStrip.AppendLine("<li>");
                        //var iconUrl = urlHelper.Content("~/Content/flags/" + ((List<Country>)language.Countries)[0]?.TwoLetterISORegionName + ".png");
                        tabStrip.AppendLine(
                            $"<a data-tab-name=\"{name}-{language.LanguageCode}-tab\" href=\"#{name}-{language.LanguageCode}-tab\" data-toggle=\"tab\"><img alt='' src='{null}'> {HttpUtility.HtmlEncode(language.Name)}</a>");

                        tabStrip.AppendLine("</li>");
                    }
                    tabStrip.AppendLine("</ul>");

                    //default tab
                    tabStrip.AppendLine("<div class=\"tab-content\">");
                    tabStrip.AppendLine($"<div class=\"tab-pane fade in active\" id=\"{name}-fa-IR-tab\">");
                    tabStrip.AppendLine(standardTemplate(helper.ViewData.Model).ToHtmlString());
                    tabStrip.AppendLine("</div>");

                    for (int i = 0; i < helper.ViewData.Model.Locales.Count; i++)
                    {
                        //languages
#pragma warning disable CS0618 // Type or member is obsolete
                        var language = languageService.GetById(helper.ViewData.Model.Locales[i].LanguageId);
#pragma warning restore CS0618 // Type or member is obsolete

                        tabStrip.AppendLine($"<div class=\"tab-pane fade\" id=\"{name}-{language.LanguageCode}-tab\">");
                        tabStrip.AppendLine(localizedTemplate(i).ToHtmlString());
                        tabStrip.AppendLine("</div>");
                    }
                    tabStrip.AppendLine("</div>");
                    tabStrip.AppendLine("</div>");
                    writer.Write(new MvcHtmlString(tabStrip.ToString()));
                }
                else
                {
                    standardTemplate(helper.ViewData.Model).WriteTo(writer);
                }
            });
        }
    }
}