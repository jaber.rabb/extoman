﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public static class DropDownHelper
    {
        public static IEnumerable<SelectListItem> GetDropDownList<T>(this IEnumerable<T> source, string textField = "Name", string valueField = "Id", string selectedValue = null) where T : class
        {
            var list = new List<SelectListItem> { new SelectListItem { Text = "-انتخاب کنید-", Value = string.Empty } };
            var lisData = source.Select(m => new SelectListItem
            {
                Text = m.GetType().GetProperty(textField).GetValue(m, null).ToString(),
                Value = m.GetType().GetProperty(valueField).GetValue(m, null).ToString(),
                Selected = (selectedValue != null) && ((string)m.GetType().GetProperty(valueField).GetValue(m, null) == selectedValue)
            }).ToList();
            list.AddRange(lisData);
            return list;
        }

        //public static IEnumerable<SelectListItem> GetDropDownList<T>(this IEnumerable<T> source, string textField = "Name", string valueField = "Id", IEnumerable<int?> selectedValues = null) where T : class
        //{
        //    var list = new List<SelectListItem> { new SelectListItem { Text = "-انتخاب کنید-", Value = string.Empty } };
        //    var lisData = source.Select(m => new SelectListItem
        //    {
        //        Text = m.GetType().GetProperty(textField).GetValue(m, null).ToString(),
        //        Value = m.GetType().GetProperty(valueField).GetValue(m, null).ToString(),
        //        Selected = (selectedValues != null) && (selectedValues.Contains(m.GetType().GetProperty(valueField).GetValue(m, null).ToInt())),
        //    }).ToList();
        //    list.AddRange(lisData);
        //    return list;
        //}
        //public static List<SelectListItem> GetDropDownList<T>(this Enum aaa, T selectedValue)
        //{
        //    List<SelectListItem> list = new List<SelectListItem>();
        //    list.Add(new SelectListItem { Text = "-انتخاب کنید-", Value = string.Empty });
        //    var lisData = Enum.GetValues(aaa.GetType()).Cast<T>().Select(m => new SelectListItem
        //    {
        //        Text = m.ToString(),
        //        Value = Convert.ToInt32(m).ToString(),
        //        Selected = Convert.ToInt32(m).ToString() == Convert.ToInt32(T.tos).ToString(),
        //    }).ToList();
        //    list.AddRange(lisData);
        //    return list;
        //}

        public static MvcHtmlString MyListBox(this HtmlHelper helper, string name, IEnumerable items, IEnumerable<string> selectedValues, object htmlAttributes = null, string dataValueField = "Id", string dataTextField = "Name")
        {
            var sel = new TagBuilder("select");
            sel.Attributes.Add("id", helper.ViewData.TemplateInfo.GetFullHtmlFieldId(name));
            sel.Attributes.Add("name", helper.ViewData.TemplateInfo.GetFullHtmlFieldName(name));
            sel.Attributes.Add("multiple", null);

            foreach (var item in items)
            {
                var value = item.GetType().GetProperty(dataValueField).GetValue(item, null).ToString();
                var text = item.GetType().GetProperty(dataTextField).GetValue(item, null).ToString();
                var opt = new TagBuilder("option");
                opt.Attributes.Add("value", value);
                if (selectedValues.Contains(value))
                    opt.Attributes.Add("selected", null);
                opt.SetInnerText(text);
                sel.InnerHtml += opt + "\n";
            }
            if (htmlAttributes != null)
                sel.MergeAttributes((IDictionary<string, string>)htmlAttributes, true);
            //if (htmlAttributes != null)
            //{
            //    var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            //    sel.MergeAttributes(attributes);
            //}

            return new MvcHtmlString(sel.ToString());
        }
    }
}