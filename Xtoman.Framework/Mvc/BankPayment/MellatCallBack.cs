﻿namespace Xtoman.Framework.Mvc
{
    public class MellatCallBack
    {
#pragma warning disable IDE1006 // Naming Styles
        public string refId { get; set; }
        public int resCode { get; set; }
        public long saleOrderId { get; set; }
        public long? saleReferenceId { get; set; }
        public string cardHolderInfo { get; set; }
        public string cardHolderPan { get; set; }
#pragma warning restore IDE1006 // Naming Style
    }
}
