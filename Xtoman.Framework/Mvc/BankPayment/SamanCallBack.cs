﻿namespace Xtoman.Framework.Mvc
{
    public class SamanCallBack
    {
        public string State { get; set; } // State text
        public int StateCode { get; set; } // State code
        public string RefNum { get; set; } // Reference number
        public long ResNum { get; set; } // Reservation number
        public string MID { get; set; } // Merchant Id
    }
}
