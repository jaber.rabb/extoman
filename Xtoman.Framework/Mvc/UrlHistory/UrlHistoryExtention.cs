﻿using AutoMapper;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Framework.DependencyResolution;
using Xtoman.Service;

namespace Xtoman.Framework.Mvc
{
    public static class UrlHistoryExtention
    {
        public static async Task SaveUrlsAsync<TModel, TLocalizedModel>(this IUrlHistoryService urlHistoryService, TModel entity, IUrlHistoryModel<TLocalizedModel> model)
            where TModel : BaseEntity, IBaseEntity, IUrlHistoryEntity
            where TLocalizedModel : ILocalizedUrlHistoryModel
        {
            var languageId = await LocalizationExtensions.GetCurrentLanguageIdAsync();
            if (model.Url != model.OriginalUrl)
                await urlHistoryService.SaveUrlAsync(entity, model.OriginalUrl, languageId);

            foreach (var localized in model.Locales)
            {
                var localeEntity = Mapper.Map<TLocalizedModel, TModel>(localized);
                localeEntity.Id = entity.Id;

                if (string.IsNullOrWhiteSpace(localized.OriginalUrl) && !string.IsNullOrEmpty(localized.Url))
                    await urlHistoryService.SaveUrlAsync(localeEntity, localized.Url, localized.LanguageId);

                if (localized.Url != localized.OriginalUrl)
                    await urlHistoryService.SaveUrlAsync(localeEntity, localized.OriginalUrl, localized.LanguageId);
            }
        }

        public static async Task<TModel> FindByUrlAsync<TModel>(this TModel model, string url)
            where TModel : BaseEntity, IBaseEntity, IUrlHistoryEntity
        {
            var languageId = await LocalizationExtensions.GetCurrentLanguageIdAsync();
            var urlHistoryService = IoC.Container.GetInstance<IUrlHistoryService>();
            return await FindByUrlAsync<TModel>(urlHistoryService, url);
        }

        public static async Task<TModel> FindByUrlAsync<TModel>(this IUrlHistoryService urlHistoryService, string url)
            where TModel : BaseEntity, IBaseEntity, IUrlHistoryEntity
        {
            var languageId = await LocalizationExtensions.GetCurrentLanguageIdAsync();
            var entityId = await urlHistoryService.FindEntityIdByUrlAsync<TModel>(url, languageId);
            if (entityId.HasValue)
            {
                var repository = IoC.Container.GetInstance<IRepository<TModel>>();
                return await repository.GetByIdAsync(entityId.Value);
            }
            return null;
        }

        public static async Task<TModel> FindByUrlAsync<TModel>(this IRepository<TModel> repository, IUrlHistoryService urlHistoryService, string url)
            where TModel : BaseEntity, IBaseEntity, IUrlHistoryEntity
        {
            var entity = await repository.Table.Where(p => p.Url == url).SingleOrDefaultAsync();
            if (entity == null)
            {
                var languageId = await LocalizationExtensions.GetCurrentLanguageIdAsync();
                var entityId = await urlHistoryService.FindEntityIdByUrlAsync<TModel>(url, languageId);
                if (entityId.HasValue)
                {
                    return await repository.GetByIdAsync(entityId.Value);
                }
                return null;
            }
            return entity;
        }

        public static async Task<TModel> FindByUrlAsNoTrackingAsync<TModel>(this IRepository<TModel> repository, IUrlHistoryService urlHistoryService, string url)
            where TModel : BaseEntity, IBaseEntity, IUrlHistoryEntity
        {
            var entity = await repository.TableNoTracking.Where(p => p.Url == url).SingleOrDefaultAsync();
            if (entity == null)
            {
                var languageId = await LocalizationExtensions.GetCurrentLanguageIdAsync();
                var entityId = await urlHistoryService.FindEntityIdByUrlAsync<TModel>(url, languageId);
                if (entityId.HasValue)
                {
                    return await repository.TableNoTracking.Where(p => p.Id == entityId.Value).SingleOrDefaultAsync();
                }
                return null;
            }
            return entity;
        }

        public static TModel FindByUrlAsNoTracking<TModel>(this IRepository<TModel> repository, IUrlHistoryService urlHistoryService, string url)
            where TModel : BaseEntity, IBaseEntity, IUrlHistoryEntity
        {
            var entity = repository.TableNoTracking.Where(p => p.Url == url).SingleOrDefault();
            if (entity == null)
            {
                var languageId = LocalizationExtensions.GetCurrentLanguageId();
                var entityId = urlHistoryService.FindEntityIdByUrl<TModel>(url, languageId);
                if (entityId.HasValue)
                {
                    return repository.TableNoTracking.Where(p => p.Id == entityId.Value).SingleOrDefault();
                }
                return null;
            }
            return entity;
        }

        public static IsUniqueResult IsUniqueUrl<TModel, TLocalizedModel>(this IRepository<TModel> repository, IUrlHistoryModel<TLocalizedModel> model, bool fillLanguageName = false)
            where TModel : BaseEntity, IBaseEntity, IUrlHistoryEntity
            where TLocalizedModel : ILocalizedUrlHistoryModel
        {
            var urlHistoryService = IoC.Container.GetInstance<IUrlHistoryService>();
            return IsUniqueUrl(repository, urlHistoryService, model, fillLanguageName);
        }
        public static IsUniqueResult IsUniqueUrl<TModel, TLocalizedModel>(this IUrlHistoryService urlHistoryService, IUrlHistoryModel<TLocalizedModel> model, bool fillLanguageName = false)
            where TModel : BaseEntity, IBaseEntity, IUrlHistoryEntity
            where TLocalizedModel : ILocalizedUrlHistoryModel
        {
            var repository = IoC.Container.GetInstance<IRepository<TModel>>();
            return IsUniqueUrl(repository, urlHistoryService, model, fillLanguageName);
        }
        public static IsUniqueResult IsUniqueUrl<TModel, TLocalizedModel>(this IRepository<TModel> repository, IUrlHistoryService urlHistoryService, IUrlHistoryModel<TLocalizedModel> model, bool fillLanguageName = false)
            where TModel : BaseEntity, IBaseEntity, IUrlHistoryEntity
            where TLocalizedModel : ILocalizedUrlHistoryModel
        {
            var languageId = LocalizationExtensions.GetCurrentLanguageId();
            var result1 = model.Url == null ? true : repository.TableNoTracking.Any(p => p.Url == model.Url && p.Id != model.Id) == false;
            var result2 = urlHistoryService.IsUniqueUrl<TModel>(model.Url, languageId);
            var result = new IsUniqueResult { IsUnique = result1 && result2 };
            ILanguageService languageService = null;
            if (fillLanguageName)
                languageService = IoC.Container.GetInstance<ILanguageService>();
            foreach (var item in model.Locales)
            {
                var isUnique = string.IsNullOrWhiteSpace(item.Url) ? true : urlHistoryService.IsUniqueUrl<TModel>(item.Url, item.LanguageId);
                string languageName = null;
                if (isUnique == false && fillLanguageName)
                    languageName = languageService.TableNoTracking.Where(p => p.Id == item.LanguageId).Select(p => p.Name).Single();
                result.Locales.Add(new IsUniqueLocaleResult
                {
                    IsUnique = isUnique,
                    LanguageId = item.LanguageId,
                    LanguageName = languageName
                });
            }
            return result;
        }

        public static Task<IsUniqueResult> IsUniqueUrlAsync<TModel, TLocalizedModel>(this IRepository<TModel> repository, IUrlHistoryModel<TLocalizedModel> model, bool fillLanguageName = false)
            where TModel : BaseEntity, IBaseEntity, IUrlHistoryEntity
            where TLocalizedModel : ILocalizedUrlHistoryModel
        {
            var urlHistoryService = IoC.Container.GetInstance<IUrlHistoryService>();
            return IsUniqueUrlAsync(repository, urlHistoryService, model, fillLanguageName);
        }

        public static Task<IsUniqueResult> IsUniqueUrlAsync<TModel, TLocalizedModel>(this IUrlHistoryService urlHistoryService, IUrlHistoryModel<TLocalizedModel> model, bool fillLanguageName = false)
            where TModel : BaseEntity, IBaseEntity, IUrlHistoryEntity
            where TLocalizedModel : ILocalizedUrlHistoryModel
        {
            var repository = IoC.Container.GetInstance<IRepository<TModel>>();
            return IsUniqueUrlAsync(repository, urlHistoryService, model, fillLanguageName);
        }

        public static async Task<IsUniqueResult> IsUniqueUrlAsync<TModel, TLocalizedModel>(this IRepository<TModel> repository, IUrlHistoryService urlHistoryService, IUrlHistoryModel<TLocalizedModel> model, bool fillLanguageName = false)
            where TModel : BaseEntity, IBaseEntity, IUrlHistoryEntity
            where TLocalizedModel : ILocalizedUrlHistoryModel
        {
            var languageId = await LocalizationExtensions.GetCurrentLanguageIdAsync();
            var result1 = model.Url == null ? true : (!(await repository.TableNoTracking.AnyAsync(p => p.Url == model.Url && p.Id != model.Id)));
            var result2 = await urlHistoryService.IsUniqueUrlAsync<TModel>(model.Url, languageId);
            var result = new IsUniqueResult { IsUnique = result1 && result2 };

            ILanguageService languageService = null;
            if (fillLanguageName)
                languageService = IoC.Container.GetInstance<ILanguageService>();
            foreach (var item in model.Locales)
            {
                var isUnique = string.IsNullOrWhiteSpace(item.Url) ? true : await urlHistoryService.IsUniqueUrlAsync<TModel>(item.Url, item.LanguageId);
                string languageName = null;
                if (!isUnique && fillLanguageName)
                    languageName = await languageService.TableNoTracking.Where(p => p.Id == item.LanguageId).Select(p => p.Name).SingleAsync();
                result.Locales.Add(new IsUniqueLocaleResult
                {
                    IsUnique = isUnique,
                    LanguageId = item.LanguageId,
                    LanguageName = languageName
                });
            }
            return result;
        }
    }

    public class IsUniqueLocaleResult
    {
        public int LanguageId { get; set; }
        public string LanguageName { get; set; }
        public bool IsUnique { get; set; }
    }

    public class IsUniqueResult
    {
        public IsUniqueResult()
        {
            Locales = new List<IsUniqueLocaleResult>();
        }

        public bool IsUnique { get; set; }
        public List<IsUniqueLocaleResult> Locales { get; set; }
    }
}
