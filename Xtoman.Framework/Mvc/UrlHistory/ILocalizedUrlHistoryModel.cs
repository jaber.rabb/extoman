﻿using System.Collections.Generic;

namespace Xtoman.Framework.Mvc
{
    public interface IUrlHistoryModel<LocalizedUrlHistoryModel> where LocalizedUrlHistoryModel : ILocalizedUrlHistoryModel
    {
        int Id { get; set; }
        string Url { get; set; }
        string OriginalUrl { get; set; }

        IList<LocalizedUrlHistoryModel> Locales { get; set; }
    }

    public interface ILocalizedUrlHistoryModel
    {
        string Url { get; set; }
        string OriginalUrl { get; set; }
        int LanguageId { get; set; }
    }
}
