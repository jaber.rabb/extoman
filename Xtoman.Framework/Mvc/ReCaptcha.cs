﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using Xtoman.Utility;

namespace Xtoman.Framework.Mvc
{
    public static class ReCaptcha
    {
        private const string reCAPTCHAVerifyURL = "https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}";
        public static RecaptchaResponse ValidateCaptcha(this HttpContextBase httpContextBase, string secretKey)
        {
            var recaptchaResponse = httpContextBase.Request["g-recaptcha-response"];
            var client = new WebClient();
            var reply = "";
            var retryCount = 0;
            while (!reply.HasValue())
            {
                retryCount++;
                if (retryCount >= 3)
                    break;
                reply = client.DownloadString(string.Format(reCAPTCHAVerifyURL, secretKey, recaptchaResponse));
            }
            return reply.HasValue() ? JsonConvert.DeserializeObject<ReCaptchaResponse>(reply).Response : RecaptchaResponse.Other;
        }

        public static async Task<RecaptchaResponse> ValidateCaptchaAsync(this HttpContextBase httpContextBase, string secretKey)
        {
            var recaptchaResponse = httpContextBase.Request["g-recaptcha-response"];
            var client = new WebClient();
            var reply = "";
            var retryCount = 0;
            while (!reply.HasValue())
            {
                retryCount++;
                if (retryCount >= 3)
                    break;
                reply = await client.DownloadStringTaskAsync(string.Format(reCAPTCHAVerifyURL, secretKey, recaptchaResponse));
            }
            return reply.HasValue() ? JsonConvert.DeserializeObject<ReCaptchaResponse>(reply).Response : RecaptchaResponse.Other;
        }
    }

    public class ReCaptchaResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("error-codes")]
        public List<string> ErrorCodes { get; set; }

        [JsonIgnore]
        public RecaptchaResponse Response
        {
            get
            {
                if (Success)
                    return RecaptchaResponse.Success;

                var error = ErrorCodes[0].ToLower();
                switch (error)
                {
                    case ("missing-input-secret"):
                        return RecaptchaResponse.MissingInputSecret;
                    case ("invalid-input-secret"):
                        return RecaptchaResponse.InvalidInputSecret;
                    case ("missing-input-response"):
                        return RecaptchaResponse.MissingInputResponse;
                    case ("invalid-input-response"):
                        return RecaptchaResponse.InvalidInputResponse;
                    case ("bad-request"):
                        return RecaptchaResponse.BadRequest;
                    default:
                        return RecaptchaResponse.Other;
                }
            }
        }
    }

    public enum RecaptchaResponse
    {
        [Display(Name = "reCAPTCHA successed.")]
        Success,
        [Display(Name = "The secret parameter is missing.")]
        MissingInputSecret,
        [Display(Name = "The secret parameter is invalid or malformed.")]
        InvalidInputSecret,
        [Display(Name = "The response parameter is missing.")]
        MissingInputResponse,
        [Display(Name = "The response parameter is invalid or malformed.")]
        InvalidInputResponse,
        [Display(Name = "The request is invalid or malformed.")]
        BadRequest,
        [Display(Name = "Error occured. Please try again")]
        Other
    }
}
