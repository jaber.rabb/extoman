﻿using Xtoman.Framework.Xml;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class SitemapResult : ActionResult
    {
        private readonly Sitemap _sitemap;
        private readonly bool _cData;

        public SitemapResult(Sitemap sitemap, bool cData = false)
        {
            _sitemap = sitemap;
            _cData = cData;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (_sitemap == null)
                return;

            var result = new SitemapGenerator(_sitemap, _cData).Generate();
            context.HttpContext.Response.Clear();
            context.HttpContext.Response.ContentType = "text/xml";
            context.HttpContext.Response.Write(result);
            context.HttpContext.Response.Flush();
            context.HttpContext.Response.End();
        }
    }
}

/*
<url>
    <loc>http://example.com/sample.html</loc>
    <image:image>
		<image:loc>http://example.com/photo.jpg</image:loc>
		<image:caption>caption</image:caption>
		<image:geo_location>Limerick, Ireland</image:geo_location>
		<image:title>title</image:title>
    </image:image>
    <image:image>
		<image:loc>http://example.com/photo.jpg</image:loc>
		<image:caption>caption</image:caption>
		<image:geo_location>Limerick, Ireland</image:geo_location>
		<image:title>title</image:title>
        // ERROR
        <image:loc>http://blog.eventcenter.ir/wp-content/uploads/2016/07/fear_of_public_speaking.png</image:loc>
		<image:title><![CDATA[fear_of_public_speaking]]></image:title>
		<image:caption><![CDATA[شروع سخنرانی و غلبه بر ترس]]></image:caption>
    </image:image>
</url> 
*/

//<?xml-stylesheet type="text/xsl" href="//blog.eventcenter.ir/main-sitemap.xsl"?>
//https://www.xml-sitemaps.com/sitemap_images.xml
//http://blog.eventcenter.ir/sitemap_index.xml