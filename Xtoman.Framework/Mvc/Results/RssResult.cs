﻿using Xtoman.Utility;
using Xtoman.Framework.Xml;
using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel.Syndication;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Xtoman.Framework.Mvc
{
    public class RssResult : ActionResult
    {
        readonly string _feedTitle;
        readonly List<SyndicationItem> _allItems;
        readonly string _language;
        public RssResult(string feedTitle, IList<FeedItem> rssItems, string language = "fa-IR")
        {
            _feedTitle = feedTitle;
            _allItems = mapToSyndicationItem(rssItems);
            _language = language;
        }

        private static List<SyndicationItem> mapToSyndicationItem(IList<FeedItem> rssItems)
        {
            var results = new List<SyndicationItem>();
            foreach (var item in rssItems)
            {
                var uri = new Uri(item.Url);
                var feedItem = new SyndicationItem(
                        title: item.Title.CorrectRtl(),
                        content: SyndicationContent.CreateHtmlContent(item.Content.CorrectRtlBody()),
                        itemAlternateLink: uri,
                        id: item.Url.ToSHA1Hash(),
                        lastUpdatedTime: item.LastUpdatedTime
                        )
                {
                    PublishDate = item.PublishDate
                };
                feedItem.Authors.Add(new SyndicationPerson(item.AuthorName, item.AuthorName, uri.Host));
                results.Add(feedItem);
            }
            return results;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            CheckHelper.NotNull(context, nameof(context));

            writeToResponse(context.HttpContext);
        }

        private void writeToResponse(HttpContextBase httpContext)
        {
            var feed = new SyndicationFeed
            {
                Title = new TextSyndicationContent(_feedTitle.CorrectRtl()),
                Language = _language,
                Items = _allItems
            };
            addChannelLinks(httpContext, feed);

            var feedData = syndicationFeedToString(feed);
            // Interoperability with feed readers could be improved by avoiding Namespace Prefix: a10
            feedData = feedData.Replace("xmlns:a10", "xmlns:atom").Replace("a10:", "atom:");

            var response = httpContext.Response;
            response.ContentEncoding = Encoding.UTF8;
            response.ContentType = "application/rss+xml";
            response.Write(feedData);
            response.End();
        }

        private static void addChannelLinks(HttpContextBase httpContext, SyndicationFeed feed)
        {
            // Improved interoperability with feed readers by implementing atom:link with rel="self"
            var baseUrl = new UriBuilder(httpContext.Request.Url.Scheme, httpContext.Request.Url.Host).Uri;
            var feedLink = new Uri(baseUrl, httpContext.Request.RawUrl);
            feed.Links.Add(SyndicationLink.CreateSelfLink(feedLink));
            feed.Links.Add(new SyndicationLink { Uri = baseUrl, RelationshipType = "alternate" });
        }

        private static string syndicationFeedToString(SyndicationFeed feed)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var rssWriter = XmlWriter.Create(memoryStream, new XmlWriterSettings { Indent = true }))
                {
                    var formatter3 = new Rss20FeedFormatter(feed);
                    formatter3.WriteTo(rssWriter);
                    rssWriter.Close();
                }
                return Encoding.UTF8.GetString(memoryStream.ToArray());
            }
        }
    }
}
