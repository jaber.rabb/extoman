﻿using System.Collections.Generic;

namespace Xtoman.Framework.Mvc
{
    public interface ILocalizedModel<TLocalizedModel> where TLocalizedModel : ILocalizedModelLocal
    {
        IList<TLocalizedModel> Locales { get; set; }
    }

    public interface ILocalizedModelLocal
    {
        int LanguageId { get; set; }
    }
}
