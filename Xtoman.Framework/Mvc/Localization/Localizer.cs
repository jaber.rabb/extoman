namespace Xtoman.Framework.Mvc
{
    public delegate LocalizedString Localizer(string text, params object[] args);
}