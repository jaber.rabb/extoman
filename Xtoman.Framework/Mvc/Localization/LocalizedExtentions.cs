﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Framework.DependencyResolution;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Framework.Mvc
{
    public static class LocalizationExtensions
    {
        public static async Task<int> GetCurrentLanguageIdAsync()
        {
            var languageService = IoC.Container.GetInstance<ILanguageService>();
            var languageCode = CultureHelper.GetCurrentNeutralCultureName();
            var language = await languageService.TableNoTracking.SingleOrDefaultAsync(x => x.LanguageCode == languageCode);
            return language?.Id ?? 0;
        }

        public static int GetCurrentLanguageId()
        {
            var languageService = IoC.Container.GetInstance<ILanguageService>();
            var languageCode = CultureHelper.GetCurrentNeutralCultureName();
            var language = languageService.TableNoTracking.SingleOrDefault(x => x.LanguageCode == languageCode);
            var currentLanguageId = language?.Id ?? 0;
            return currentLanguageId;
        }

        public static Task<Language> GetCurrentLanguageAsync()
        {
            var languageService = IoC.Container.GetInstance<ILanguageService>();
            var languageCode = CultureHelper.GetCurrentNeutralCultureName();
            return languageService.Table.FirstOrDefaultAsync(x => x.LanguageCode == languageCode);
        }

        /// <summary>
        /// Returns a valid culture name based on "name" parameter. If "name" is not valid, it returns the default culture "en-US"
        /// </summary>
        /// <param name="name">Culture's name (e.g. en-US)</param>
        public static async Task<string> GetImplementedCultureAsync(string name)
        {
            var cultures = await IoC.Container.GetInstance<ILanguageService>().TableNoTracking.Select(x => x.LanguageCode).ToListAsync();
            // make sure it's not null
            if (string.IsNullOrEmpty(name))
                return CultureHelper.GetDefaultCultureName(); // return Default culture

            // make sure it is a valid culture first
            if (!CultureHelper.ValidCultures.Any(c => c.Equals(name, StringComparison.InvariantCultureIgnoreCase)))
                return CultureHelper.GetDefaultCultureName(); // return Default culture if it is invalid

            // if it is implemented, accept it
            if (cultures.Any(c => c.Equals(name, StringComparison.InvariantCultureIgnoreCase)))
                return name; // accept it

            // Find a close match. For example, if you have "en-US" defined and the user requests "en-GB", 
            // the function will return closes match that is "en-US" because at least the language is the same (ie English)  
            var n = CultureHelper.GetNeutralCultureName(name);
            foreach (var c in cultures)
            {
                if (c.StartsWith(n))
                    return c;
            }

            // else 
            // It is not implemented
            return CultureHelper.GetDefaultCultureName(); // return Default culture as no match found
        }

        /// <summary>
        /// Returns a valid culture name based on "name" parameter. If "name" is not valid, it returns the default culture "en-US"
        /// </summary>
        /// <param name="name">Culture's name (e.g. en-US)</param>
        public static string GetImplementedCulture(string name)
        {
            var cultures = IoC.Container.GetInstance<ILanguageService>().TableNoTracking.Select(x => x.LanguageCode).ToList();
            // make sure it's not null
            if (string.IsNullOrEmpty(name))
                return CultureHelper.GetDefaultCultureName(); // return Default culture

            // make sure it is a valid culture first
            if (!CultureHelper.ValidCultures.Any(c => c.Equals(name, StringComparison.InvariantCultureIgnoreCase)))
                return CultureHelper.GetDefaultCultureName(); // return Default culture if it is invalid

            // if it is implemented, accept it
            if (cultures.Any(c => c.Equals(name, StringComparison.InvariantCultureIgnoreCase)))
                return name; // accept it

            // Find a close match. For example, if you have "en-US" defined and the user requests "en-GB", 
            // the function will return closes match that is "en-US" because at least the language is the same (ie English)  
            var n = CultureHelper.GetNeutralCultureName(name);
            foreach (var c in cultures)
            {
                if (c.StartsWith(n))
                    return c;
            }

            // else 
            // It is not implemented
            return CultureHelper.GetDefaultCultureName(); // return Default culture as no match found
        }

        public static async Task UpdateLocalesAsync<TModel, TLocalizedModel>(this ILocalizedPropertyService localizedPropertyService, TModel model, IEnumerable<TLocalizedModel> locales)
           where TModel : BaseEntity, IBaseEntity, ILocalizedEntity
           where TLocalizedModel : ILocalizedModelLocal
        {
            foreach (var localized in locales)
            {
                var entity = Mapper.Map<TLocalizedModel, TModel>(localized);
                entity.Id = model.Id;
                await localizedPropertyService.SaveLocalizedValueAsync(entity, localized.LanguageId);
            }
        }

        #region GetLocalized
        public static async Task<TModel> GetLocalizedAsync<TModel, TEntity>(this TModel viewModel, string lang = "fa")
            where TModel : BaseViewModel<TModel, TEntity>, ILocalizedEntity
            where TEntity : class, IBaseEntity
        {
            CheckHelper.NotNull(viewModel, nameof(viewModel));
            if (lang != "fa")
                System.Threading.Thread.CurrentThread.CurrentCulture = CultureHelper.GetCultureInfo(lang);
            var language = await GetCurrentLanguageAsync();
            new Exception("viewModel1 : " + viewModel.Serialize()).LogError();
            new Exception("language.LanguageCode : " + language.LanguageCode).LogError();
            if (language.LanguageCode != "fa")
            {
                var service = IoC.Container.GetInstance<ILocalizedPropertyService>();
                var props = typeof(TEntity).GetProperties().Where(p => Attribute.IsDefined(p, typeof(LocalizedPropertyAttribute)) && p.CanRead && p.CanWrite);
                var modelProps = typeof(TModel).GetProperties().Where(p => p.CanRead && p.CanWrite);
                foreach (var prop in props)
                {
                    var modelProp = modelProps.SingleOrDefault(p => p.Name == prop.Name);
                    if (modelProp != null)
                    {
                        var localeKeyGroup = typeof(TEntity).Name;
                        var localeKey = prop.Name;

                        var valueStr = await service.GetLocalizedValueAsync(language.Id, viewModel.Id, localeKeyGroup, localeKey);
                        var value = Convert.ChangeType(valueStr, prop.PropertyType);

                        modelProp.SetValue(viewModel, value, null);
                    }
                }
            }
            new Exception("viewModel2 : " + viewModel.Serialize()).LogError();
            return viewModel;
        }

        public static async Task<TModel> GetLocalizedAsync<TModel>(this TModel entity) where TModel : BaseEntity, IBaseEntity, ILocalizedEntity
        {
            CheckHelper.NotNull(entity, nameof(entity));
            //var languageId = GetCurrentLanguageId();
            //if (languageId > 0)
            var language = await GetCurrentLanguageAsync();
            if (language.LanguageCode != "fa")
            {
                var service = IoC.Container.GetInstance<ILocalizedPropertyService>();
                entity = await service.GetLocalizedValueAsync(entity, language.Id);
            }
            return entity;
        }

        public static async Task<TLocalizedModel> GetLocalizedAsync<TModel, TLocalizedModel>(this TModel entity, int languageId) where TModel : BaseEntity, IBaseEntity, ILocalizedEntity where TLocalizedModel : ILocalizedModelLocal
        {
            CheckHelper.NotNull(entity, nameof(entity));

            var result = default(TLocalizedModel);

            if (languageId > 0)
            {
                var service = IoC.Container.GetInstance<ILocalizedPropertyService>();
                var model = await service.GetLocalizedValueAsync(entity, languageId);
                result = Mapper.Map<TModel, TLocalizedModel>(model);
            }

            return result;
        }

        public static async Task<string> GetLocalizedAsync<T>(this T entity, Expression<Func<T, string>> keySelector)
            where T : BaseEntity, ILocalizedEntity
        {
            var cuurentLanuageId = await GetCurrentLanguageIdAsync();
            return await GetLocalizedAsync(entity, keySelector, cuurentLanuageId);
        }

        /// <summary>
        /// Get localized property of an entity
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="languageId">Language identifier</param>
        /// <param name="returnDefaultValue">A value indicating whether to return default value (if localized is not found)</param>
        /// <param name="ensureTwoPublishedLanguages">A value indicating whether to ensure that we have at least two published languages; otherwise, load only default value</param>
        /// <returns>Localized property</returns>
        public static Task<string> GetLocalizedAsync<T>(this T entity, Expression<Func<T, string>> keySelector, int languageId, bool returnDefaultValue = true, bool ensureTwoPublishedLanguages = true) where T : BaseEntity, IBaseEntity, ILocalizedEntity
        {
            return GetLocalizedAsync<T, string>(entity, keySelector, languageId, returnDefaultValue, ensureTwoPublishedLanguages);
        }

        /*
         **************************************************************************************
         * Sync to Xtoman.Service.WorkflowMessageService(GetLocalized)
         **************************************************************************************
         */

        /// <summary>
        /// Get localized property of an entity
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <typeparam name="TPropType">Property type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="languageId">Language identifier</param>
        /// <param name="returnDefaultValue">A value indicating whether to return default value (if localized is not found)</param>
        /// <param name="ensureTwoPublishedLanguages">A value indicating whether to ensure that we have at least two published languages; otherwise, load only default value</param>
        /// <returns>Localized property</returns>
        public static async Task<TPropType> GetLocalizedAsync<T, TPropType>(this T entity, Expression<Func<T, TPropType>> keySelector, int languageId, bool returnDefaultValue = true, bool ensureTwoPublishedLanguages = true)
            where T : BaseEntity, IBaseEntity, ILocalizedEntity
        {
            CheckHelper.NotNull(entity, nameof(entity));

            var propInfo = keySelector.GetPropInfo();
            var result = default(TPropType);

            //load localized value
            string localeKeyGroup = typeof(T).Name;
            string localeKey = propInfo.Name;

            string resultStr = await GetLocalizedAsync(entity, localeKeyGroup, localeKey, languageId, ensureTwoPublishedLanguages);

            //set default value if required
            if (string.IsNullOrEmpty(resultStr) && returnDefaultValue)
            {
                var localizer = keySelector.Compile();
                result = localizer(entity);
            }

            return result;
        }

        /// <summary>
        /// Get localized property of an entity
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <typeparam name="TPropType">Property type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="localeKeyGroup">localeKeyGroup</param>
        /// <param name="localeKey">localeKey</param>
        /// <param name="languageId">Language identifier</param>
        /// <param name="returnDefaultValue">A value indicating whether to return default value (if localized is not found)</param>
        /// <param name="ensureTwoPublishedLanguages">A value indicating whether to ensure that we have at least two published languages; otherwise, load only default value</param>
        /// <returns>Localized property</returns>
        public static async Task<string> GetLocalizedAsync<T>(this T entity, string localeKeyGroup, string localeKey, int languageId, bool ensureTwoPublishedLanguages = true) where T : BaseEntity, IBaseEntity, ILocalizedEntity
        {
            CheckHelper.NotNull(entity, nameof(entity));

            string resultStr = string.Empty;

            if (languageId > 0)
            {
                //ensure that we have at least two published languages
                bool loadLocalizedValue = true;
                if (ensureTwoPublishedLanguages)
                {
                    var service = IoC.Container.GetInstance<ILanguageService>();
                    var totalPublishedLanguages = await service.TableNoTracking.CountAsync();
                    loadLocalizedValue = totalPublishedLanguages >= 2;
                }

                //localized value
                if (loadLocalizedValue)
                {
                    var service = IoC.Container.GetInstance<ILocalizedPropertyService>();
                    resultStr = await service.GetLocalizedValueAsync(languageId, entity.Id, localeKeyGroup, localeKey);
                }
            }

            return resultStr;
        }
        #endregion
    }
}
