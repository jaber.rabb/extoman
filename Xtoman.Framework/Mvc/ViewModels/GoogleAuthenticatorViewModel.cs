﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Framework.Mvc
{
    public class GoogleAuthenticatorViewModel
    {
        [Required]
        [Display(Name = "رمز گوگل")]
        public string SecretKey { get; set; }
        [Required]
        [Display(Name = "آدرس بارکد")]
        public string BarcodeUrl { get; set; }
        [Required]
        [Display(Name = "کد 6 رقمی")]
        public string Code { get; set; }
    }
}
