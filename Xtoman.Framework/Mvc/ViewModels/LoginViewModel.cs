﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Framework.Mvc
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "ایمیل، شماره موبایل یا نام کاربری")]
        public string EmailOrPhoneNumberOrUsername { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "کلمه عبور")]
        public string Password { get; set; }

        [Display(Name = "مرا به خاطر بسپار؟")]
        public bool RememberMe { get; set; }
    }
}
