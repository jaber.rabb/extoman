﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Xtoman.Framework.Mvc.Attributes;

namespace Xtoman.Framework.Mvc
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "ایمیل یا موبایل")]
        [Remote("EmailOrPhoneIsUnique", "Account", ErrorMessage = "این ایمیل یا موبایل قبلا ثبت شده.", HttpMethod = "POST")]
        [RegularExpression(@"^(?=\d{11}$)(09)\d+|^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$", ErrorMessage = "ایمیل یا تلفن همراه وارد شده اشتباه است")]
        public string EmailOrPhone { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "کلمه عبور باید حداقل {2} کاراکتر باشد.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "کلمه عبور")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "تکرار کلمه عبور")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "تکرار کلمه عبور با کلمه عبور وارد شده همخوانی ندارد. ")]
        public string ConfirmPassword { get; set; }

        [EnforceTrue(ErrorMessage = "قوانین و مقررات باید پذیرفته شود.")]
        public bool Terms { get; set; }
    }
}
