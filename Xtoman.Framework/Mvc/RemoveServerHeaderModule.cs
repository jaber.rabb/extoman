﻿using Xtoman.Utility;
using System;
using System.Web;

namespace Xtoman.Framework.Mvc
{
    public class RemoveServerHeaderModule : IHttpModule
    {
        private static readonly string[] _headersToRemoveCache = new string[] { "X-AspNet-Version", "X-AspNetMvc-Version", "Server" };


        public void Init(HttpApplication app)
        {
            app.PreSendRequestHeaders += app_PreSendRequestHeaders;
        }

        static void app_PreSendRequestHeaders(object sender, EventArgs e)
        {
            CheckPreSendRequestHeaders(sender);
        }

        public static void CheckPreSendRequestHeaders(Object sender)
        {
            //capture the current request
            var currentResponse = ((HttpApplication)sender).Response;

            //removing headers
            //it only works with IIS 7.x's integrated pipeline
            _headersToRemoveCache.ForEach(h => currentResponse.Headers.Remove(h));

            //modify the "Server" Http Header
            //currentResponse.Headers.Set("Server", "Test");
        }

        public void Dispose()
        {
        }
    }

    //<system.webServer>
    //<modules>
    //    <add name="RemoveServerHeaderModule" type="Xtoman.Framework.Mvc.RemoveServerHeaderModule, Xtoman.Framework"/>
    //</modules>

    //<system.webServer>
    //<httpProtocol>
    //    <customHeaders>
    //        <remove name="X-Powered-By"/>
    //    </customHeaders>
    //</httpProtocol>
}
