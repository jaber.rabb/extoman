﻿using Xtoman.Utility;
using System.Web.Mvc;
using WebMarkupMin.AspNet4.Common;
using WebMarkupMin.AspNet4.Mvc;
using WebMarkupMin.Core;

namespace Xtoman.Framework.Mvc
{
    public static class WebMarkupMinConfig
    {
        #region Readme
        //https://github.com/Taritsyn/WebMarkupMin/wiki
        //http://andrewlock.net/html-minification-using-webmarkupmin-in-asp-net-core/
        //http://ajaxmin.codeplex.com/
        //https://github.com/YUICompressor-NET/YUICompressor.NET
        //https://github.com/xoofx/NUglify
        //https://github.com/Taritsyn/WebMarkupMin/wiki/WebMarkupMin:-ASP.NET-4.X-HTTP-modules
        //<configuration>
        //    <system.webServer>
        //        <modules>
        //            <add name="HtmlMinificationModule"
        //                type="WebMarkupMin.AspNet4.HttpModules.HtmlMinificationModule, WebMarkupMin.AspNet4.HttpModules" />
        //            <add name="XhtmlMinificationModule"
        //                type="WebMarkupMin.AspNet4.HttpModules.XhtmlMinificationModule, WebMarkupMin.AspNet4.HttpModules" />
        //            <add name="XmlMinificationModule"
        //                type="WebMarkupMin.AspNet4.HttpModules.XmlMinificationModule, WebMarkupMin.AspNet4.HttpModules" />
        //            <add name="HttpCompressionModule"
        //                type="WebMarkupMin.AspNet4.HttpModules.HttpCompressionModule, WebMarkupMin.AspNet4.HttpModules" />
        //        </modules
        //    </system.webServer>
        //</configuration>
        #endregion

        public static void Configure(WebMarkupMinConfiguration configuration)
        {
            if (AppSettingManager.IsLocale)
                return;

            #region Configuration
            configuration.AllowMinificationInDebugMode = true;
            configuration.AllowCompressionInDebugMode = true;
            configuration.DisablePoweredByHttpHeaders = true;
            configuration.DisableMinification = false;
            configuration.DisableCompression = false;
            configuration.MaxResponseSize = -1; //Maximum size of the response (in bytes), in excess of which disables the minification of markup. If this property is set equal to -1, then checking of the response size is not performed.
            #endregion

            #region Html Minification
            //HtmlMinificationManager.Current.CssMinifierFactory = new KristensenCssMinifierFactory();
            //HtmlMinificationManager.Current.JsMinifierFactory = new CrockfordJsMinifierFactory();
            //HtmlMinificationManager.Current.SupportedMediaTypes = //text/html;
            //HtmlMinificationManager.Current.IncludedPages
            //HtmlMinificationManager.Current.ExcludedPages = new IUrlMatcher[] {
            //    new ExactUrlMatcher("/contact"),
            //    new RegexUrlMatcher(@"^/minifiers/x(?:ht)?ml-minifier$"),
            //    new WildcardUrlMatcher("/minifiers/x*ml-minifier"),
            //};
            var htmlMinificationSettings = HtmlMinificationManager.Current.MinificationSettings;
            htmlMinificationSettings.RemoveHttpProtocol­FromAttributes = false;
            htmlMinificationSettings.RemoveHttpsProtocol­FromAttributes = false;
            htmlMinificationSettings.WhitespaceMinificationMode = WhitespaceMinificationMode.Medium; //Aggressive
            htmlMinificationSettings.RemoveHtmlComments = true;
            htmlMinificationSettings.RemoveHtmlComments­FromScriptsAndStyles = true;
            htmlMinificationSettings.RemoveCdataSections­FromScriptsAndStyles = true;
            htmlMinificationSettings.UseShortDoctype = true;
            htmlMinificationSettings.PreserveCase = false;
            htmlMinificationSettings.UseMetaCharsetTag = true;
            htmlMinificationSettings.EmptyTagRenderMode = HtmlEmptyTagRenderMode.NoSlash;
            htmlMinificationSettings.RemoveOptionalEndTags = false; //default true;
            htmlMinificationSettings.PreservableOptionalTagList = ""; //Comma-separated list of names of optional tags, which should not be removed (e.g. "li, rb, rtc, rt, rp").
            htmlMinificationSettings.RemoveTagsWithoutContent = false; //Flag for whether to remove tags without content, except for textarea, tr, th and td tags, and tags with class, id, name, role, src and custom attributes.
            htmlMinificationSettings.CollapseBooleanAttributes = true; //Flag for whether to remove values from boolean attributes (for example, checked="checked" is transforms to checked).
            htmlMinificationSettings.AttributeQuotesRemovalMode = HtmlAttributeQuotesRemovalMode.Html5; //HTML attribute quotes removal mode. Can take the following values: KeepQuotes.Keep quotes. - Html4.Removes a quotes in accordance with standard HTML 4.X. - Html5.Removes a quotes in accordance with standard HTML5.
            htmlMinificationSettings.RemoveEmptyAttributes = true;
            htmlMinificationSettings.RemoveRedundantAttributes = false; //<script language="javascript" …> <form method="get" …> <input type="text" …>
            htmlMinificationSettings.RemoveJsTypeAttributes = true; //Flag for whether to remove type="text/javascript" attributes from script tags.
            htmlMinificationSettings.RemoveCssTypeAttributes = true; //Flag for whether to remove type="text/css" attributes from style and link tags.
            //htmlMinificationSettings.PreservableAttributeList = "";
            /*
            Comma-separated list of string representations of attribute expressions, that define what attributes can not be removed (e.g. "form[method=get i], input[type], [xmlns]"). Attribute expressions somewhat similar to the CSS Attribute Selectors. There are six varieties of the attribute expressions:
            [attributeName]
            tagName[attributeName]
            [attributeName=attributeValue]
            tagName[attributeName=attributeValue]
            [attributeName=attributeValue i]
            tagName[attributeName=attributeValue i]
            */
            htmlMinificationSettings.RemoveJsProtocol­FromAttributes = true; //javascript: from event attributes.
            htmlMinificationSettings.MinifyEmbeddedCssCode = true;
            htmlMinificationSettings.MinifyInlineCssCode = true;
            htmlMinificationSettings.MinifyEmbeddedJsCode = true;
            htmlMinificationSettings.MinifyInlineJsCode = true;
            htmlMinificationSettings.ProcessableScriptTypeList = ""; //Comma-separated list of types of script tags, that are processed by minifier (e.g. "text/html, text/ng-template"). Currently only supported the KnockoutJS, Kendo UI MVVM and AngularJS 1.X views.
            htmlMinificationSettings.MinifyKnockout­BindingExpressions = false;
            htmlMinificationSettings.MinifyAngular­BindingExpressions = false;
            htmlMinificationSettings.CustomAngularDirectiveList = ""; //Comma-separated list of names of custom AngularJS 1.X directives (e.g. "myDir, btfCarousel"), that contain expressions. If value of the MinifyAngularBindingExpressions property equal to true, then the expressions in custom directives will be minified.
            htmlMinificationSettings.RemoveHtmlComments = true;
            htmlMinificationSettings.RemoveHtmlComments = true;
            htmlMinificationSettings.RemoveHtmlComments = true;
            #endregion

            #region Xhtml Minification
            //XhtmlMinificationManager.Current.CssMinifierFactory = new KristensenCssMinifierFactory();
            //XhtmlMinificationManager.Current.JsMinifierFactory = new CrockfordJsMinifierFactory();
            //XhtmlMinificationManager.Current.SupportedMediaTypes = //text/html, application/xhtml+xml;
            //XhtmlMinificationManager.Current.IncludedPages
            //XhtmlMinificationManager.Current.ExcludedPages = new IUrlMatcher[] {
            //    new ExactUrlMatcher("/contact"),
            //    new RegexUrlMatcher(@"^/minifiers/x(?:ht)?ml-minifier$"),
            //    new WildcardUrlMatcher("/minifiers/x*ml-minifier"),
            //};
            var xhtmlMinificationSettings = XhtmlMinificationManager.Current.MinificationSettings;
            xhtmlMinificationSettings.RemoveHttpProtocol­FromAttributes = false;
            xhtmlMinificationSettings.RemoveHttpsProtocol­FromAttributes = false;
            xhtmlMinificationSettings.WhitespaceMinificationMode = WhitespaceMinificationMode.Medium; //Aggressive
            xhtmlMinificationSettings.RemoveHtmlComments = true;
            xhtmlMinificationSettings.RemoveHtmlComments­FromScriptsAndStyles = true;
            xhtmlMinificationSettings.UseShortDoctype = true;
            xhtmlMinificationSettings.UseMetaCharsetTag = true;
            xhtmlMinificationSettings.RemoveTagsWithoutContent = false; //Flag for whether to remove tags without content, except for textarea, tr, th and td tags, and tags with class, id, name, role, src and custom attributes.
            xhtmlMinificationSettings.RemoveEmptyAttributes = true;
            xhtmlMinificationSettings.RemoveRedundantAttributes = false; //<script language="javascript" …> <form method="get" …> <input type="text" …>
            //xhtmlMinificationSettings.PreservableAttributeList = "";
            /*
            Comma-separated list of string representations of attribute expressions, that define what attributes can not be removed (e.g. "form[method=get i], input[type], [xmlns]"). Attribute expressions somewhat similar to the CSS Attribute Selectors. There are six varieties of the attribute expressions:
            [attributeName]
            tagName[attributeName]
            [attributeName=attributeValue]
            tagName[attributeName=attributeValue]
            [attributeName=attributeValue i]
            tagName[attributeName=attributeValue i]
            */
            xhtmlMinificationSettings.RemoveJsProtocol­FromAttributes = true; //javascript: from event attributes.
            xhtmlMinificationSettings.MinifyEmbeddedCssCode = true;
            xhtmlMinificationSettings.MinifyInlineCssCode = true;
            xhtmlMinificationSettings.MinifyEmbeddedJsCode = true;
            xhtmlMinificationSettings.MinifyInlineJsCode = true;
            xhtmlMinificationSettings.ProcessableScriptTypeList = ""; //Comma-separated list of types of script tags, that are processed by minifier (e.g. "text/html, text/ng-template"). Currently only supported the KnockoutJS, Kendo UI MVVM and AngularJS 1.X views.
            xhtmlMinificationSettings.MinifyKnockout­BindingExpressions = false;
            xhtmlMinificationSettings.MinifyAngular­BindingExpressions = false;
            xhtmlMinificationSettings.CustomAngularDirectiveList = ""; //Comma-separated list of names of custom AngularJS 1.X directives (e.g. "myDir, btfCarousel"), that contain expressions. If value of the MinifyAngularBindingExpressions property equal to true, then the expressions in custom directives will be minified.
            xhtmlMinificationSettings.RemoveHtmlComments = true;
            xhtmlMinificationSettings.RemoveHtmlComments = true;
            xhtmlMinificationSettings.RemoveHtmlComments = true;
            #endregion

            #region Xml Minification
            //XmlMinificationManager.Current.SupportedMediaTypes = //application/xml, text/xml, application/xml-dtd, application/xslt+xml, application/rss+xml, application/atom+xml, application/rdf+xml, application/soap+xml, application/wsdl+xml, image/svg+xml, application/mathml+xml, application/voicexml+xml, application/srgs+xml;
            //XmlMinificationManager.Current.IncludedPages
            //XmlMinificationManager.Current.ExcludedPages = new IUrlMatcher[] {
            //    new ExactUrlMatcher("/contact"),
            //    new RegexUrlMatcher(@"^/minifiers/x(?:ht)?ml-minifier$"),
            //    new WildcardUrlMatcher("/minifiers/x*ml-minifier"),
            //};
            var xmlMinificationSettings = XmlMinificationManager.Current.MinificationSettings;
            xmlMinificationSettings.MinifyWhitespace = true;
            xmlMinificationSettings.RemoveXmlComments = true;
            xmlMinificationSettings.RenderEmptyTagsWithSpace = false;
            xmlMinificationSettings.CollapseTagsWithoutContent = true;
            #endregion

            #region Other Configuration
            //DefaultCssMinifierFactory.Current = new YuiCssMinifierFactory();
            //DefaultCssMinifierFactory.Current = new MsAjaxCssMinifierFactory();
            //DefaultJsMinifierFactory.Current = new MsAjaxJsMinifierFactory();
            //HttpCompressionManager.Current.CompressorFactories = new List<ICompressorFactory>
            //{
            //    new DeflateCompressorFactory(),
            //    new GZipCompressorFactory()
            //};
            #endregion
        }

        public static void AddWebMarkupMinFilters(GlobalFilterCollection filters)
        {
            if (AppSettingManager.IsLocale)
                return;
            filters.Add(new CompressContentAttribute());
            filters.Add(new MinifyHtmlAttribute());
            filters.Add(new MinifyXmlAttribute());
        }
    }
}
