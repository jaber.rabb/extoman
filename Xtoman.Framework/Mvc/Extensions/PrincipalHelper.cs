﻿using Microsoft.AspNet.Identity;
using System.Security.Claims;

namespace Xtoman.Framework.Mvc
{
    public static class PrincipalHelper
    {
        public static string GetFullName(this System.Security.Principal.IPrincipal user, bool ifNullUserName = false)
        {
            var fullNameClaim = ((ClaimsIdentity)user.Identity).FindFirst("FullName");
            if (fullNameClaim != null)
                return fullNameClaim.Value;
            return ifNullUserName ? user.Identity.GetUserName() : "";
        }
    }
}
