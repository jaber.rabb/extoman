﻿using System;
using Xtoman.Domain.Models;
using Xtoman.Utility;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    /// <summary>
    /// Select list helper
    /// </summary>
    public static class CategorySelectListHelper
    {
        /// <summary>
        /// Get category list
        /// </summary>
        /// <param name="categoryService">Category service</param>
        /// <returns>Category list</returns>
        public static Dictionary<int, string> GetCategoryList(List<Category> categories)
        {
            return categories.ToDictionary(c => c.Id, c => c.GetFormattedBreadCrumb(categories));
        }

        /// <summary>
        /// Get formatted category breadcrumb 
        /// </summary>
        /// <param name="category">Category</param>
        /// <param name="allCategories">All categories</param>
        /// <param name="separator">Separator</param>
        /// <param name="languageId">Language identifier for localization</param>
        /// <returns>Formatted breadcrumb</returns>
        public static string GetFormattedBreadCrumb(this Category category,
            IList<Category> allCategories,
            string separator = ">>", int languageId = 0)
        {
            string result = string.Empty;

            var breadcrumb = GetCategoryBreadCrumb(category, allCategories);
            for (int i = 0; i <= breadcrumb.Count - 1; i++)
            {
                var categoryName = breadcrumb[i].Name;
                result = result.HasValue() ? $"{result} {separator} {categoryName}" : categoryName;
            }

            return result;
        }

        /// <summary>
        /// Get category breadcrumb 
        /// </summary>
        /// <param name="category">Category</param>
        /// <param name="allCategories">All categories</param>
        /// <returns>Category breadcrumb </returns>
        public static IList<Category> GetCategoryBreadCrumb(this Category category,
            IList<Category> allCategories)
        {
            CheckHelper.NotNull(category, nameof(category));
            var result = new List<Category>();

            //used to prevent circular references
            var alreadyProcessedCategoryIds = new List<int>();

            while (category != null && //not null
                !alreadyProcessedCategoryIds.Contains(category.Id)) //prevent circular references
            {
                result.Add(category);

                alreadyProcessedCategoryIds.Add(category.Id);

                category = (from c in allCategories
                                //where c.Id == category.ParentId
                            select c).FirstOrDefault();
            }
            result.Reverse();
            return result;
        }
    }
}