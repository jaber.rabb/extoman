﻿namespace Xtoman.Framework.Mvc
{
    public enum NotifyType
    {
        Success,
        Error,
        Info
    }
}
