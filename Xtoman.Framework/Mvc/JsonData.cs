﻿using System.Web.Mvc;

namespace Xtoman.Framework.Mvc
{
    public class JsonData : JsonResult
    {
        public string Script { get; set; }
        public string Html { get; set; }
        public bool Success { get; set; }
    }
}