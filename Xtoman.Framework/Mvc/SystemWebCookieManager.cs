﻿using System;
using System.Web;
using Microsoft.Owin;
using Microsoft.Owin.Infrastructure;
using Xtoman.Utility;

namespace Xtoman.Framework.Mvc
{
    /// <summary>
    /// source-1: http://katanaproject.codeplex.com/wikipage?title=System.Web%20response%20cookie%20integration%20issues
    /// source-2: http://appetere.com/post/owinresponse-cookies-not-set-when-remove-an-httpresponse-cookie
    /// source-3: http://stackoverflow.com/questions/20737578/asp-net-sessionid-owin-cookies-do-not-send-to-browser
    /// </summary>
    public class SystemWebCookieManager : ICookieManager
    {
        public string GetRequestCookie(IOwinContext context, string key)
        {
            CheckHelper.NotNull(context, nameof(context));

            var webContext = context.Get<HttpContextBase>(typeof(HttpContextBase).FullName);
            var cookie = webContext.Request.Cookies[key];
            return cookie?.Value;
        }

        public void AppendResponseCookie(IOwinContext context, string key, string value, CookieOptions options)
        {
            CheckHelper.NotNull(context, nameof(context));
            CheckHelper.NotNull(options, nameof(options));

            var webContext = context.Get<HttpContextBase>(typeof(HttpContextBase).FullName);

            var cookie = new HttpCookie(key, value)
            {
                Secure = options.Secure,
                HttpOnly = options.HttpOnly
            };

            if (options.Domain.HasValue())
                cookie.Domain = options.Domain;
            if (options.Path.HasValue())
                cookie.Path = options.Path;
            if (options.Expires.HasValue)
                cookie.Expires = options.Expires.Value;

            webContext.Response.AppendCookie(cookie);
        }

        public void DeleteCookie(IOwinContext context, string key, CookieOptions options)
        {
            CheckHelper.NotNull(context, nameof(context));
            CheckHelper.NotNull(options, nameof(options));

            AppendResponseCookie(
                context,
                key,
                string.Empty,
                new CookieOptions
                {
                    Path = options.Path,
                    Domain = options.Domain,
                    Expires = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc),
                });
        }
    }
}
