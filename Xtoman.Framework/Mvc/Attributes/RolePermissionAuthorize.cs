﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xtoman.Framework.DependencyResolution;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Framework.Mvc.Attributes
{
    public class RolePermissionAuthorize : AuthorizeAttribute
    {
        private string superAdminRoleName { get; set; }
        public RolePermissionAuthorize(string SuperAdminRoleName = null)
        {
            if (!SuperAdminRoleName.HasValue())
                superAdminRoleName = SuperAdminRoleName;
            else
                superAdminRoleName = "Admin";
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var allowedControllers = new List<string>() { "Home", "Account", "Manage" };
            base.OnAuthorization(filterContext);
            var currentControllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var currentActionName = filterContext.ActionDescriptor.ActionName;
            var userId = HttpContext.Current.User.Identity.GetUserId<int>();
            if (userId != 0 && !allowedControllers.Any(x => x.Equals(currentControllerName, System.StringComparison.InvariantCultureIgnoreCase)))
            {
                if (!filterContext.ActionDescriptor.IsDefined
                    (typeof(AllowAnonymousAttribute), true) &&
                  !filterContext.ActionDescriptor.ControllerDescriptor.IsDefined
                    (typeof(AllowAnonymousAttribute), true))
                {
                    var _roleManager = IoC.Container.GetInstance<IAppRoleManager>();

                    var userRoles = _roleManager.FindUserRoles(userId);
                    var isAdmin = userRoles.Any(x => x.Name.Equals("Admin", System.StringComparison.InvariantCultureIgnoreCase));

                    if (!isAdmin)
                    {
                        var isPermissioned = userRoles.Any(x => x.Permissions.Any(p => p.ControllerName == currentControllerName + "Controller" && p.ControllerAction == currentActionName));
                        if (!isPermissioned)
                        {
                            new HttpException(404, $"Current = '{currentControllerName} | {currentActionName}'").LogError();
                            filterContext.Result = new AccessDeniedResult();
                        }
                    }
                }

            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new AccessDeniedResult();
            base.HandleUnauthorizedRequest(filterContext);
        }

        public class AccessDeniedResult : ViewResult
        {
            public AccessDeniedResult()
            {
                ViewName = "AccessDenied";
            }
        }
    }
}
