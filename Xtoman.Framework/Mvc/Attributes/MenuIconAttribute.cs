﻿using System;

namespace Xtoman.Framework.Mvc.Attributes
{
    /// <summary>
    /// Specifies the display icon class for a controller in menu
    /// </summary>
    public class MenuIconAttribute : Attribute
    {
        public MenuIconAttribute(string iconClass)
        {
            IconClassValue = iconClass;
        }

        /// Gets the display name for a property, event, or public void method that takes
        /// no arguments stored in this attribute.
        ///
        /// Returns:
        /// The display icon class.
        public virtual string IconClass => IconClassValue;

        /// <summary>
        /// Get or sets the display icon class.
        ///
        /// Returns:
        /// The display icon class.
        /// </summary>
        protected string IconClassValue { get; set; }
    }
}
