﻿using System;

namespace Xtoman.Framework.Mvc
{
    /// <summary>
    /// Attribute indicating that entered values should not be trimmed
    /// </summary>
    public class NoTrimAttribute : Attribute
    {
    }
}
