﻿using Xtoman.Utility;
using System.ComponentModel.DataAnnotations;

namespace Xtoman.Framework.Mvc
{
    public class NationalCodeAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var nationalCode = value as string;

            if (nationalCode.HasValue(true) && nationalCode.IsNationalCode())
                return ValidationResult.Success;

            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }
    }
}
