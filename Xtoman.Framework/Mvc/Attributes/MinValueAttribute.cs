﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc.Attributes
{
    public class MinValueAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly int _intMinValue;
        private readonly decimal _decimalMinValue;
        private readonly float _floatMinValue;
        private readonly double _doubleMinValue;

        public MinValueAttribute(int minValue)
        {
            _intMinValue = minValue;
        }

        public MinValueAttribute(decimal minValue)
        {
            _decimalMinValue = minValue;
        }

        public MinValueAttribute(float minValue)
        {
            _floatMinValue = minValue;
        }

        public MinValueAttribute(double minValue)
        {
            _doubleMinValue = minValue;
        }

        public override bool IsValid(object value)
        {
            switch (value)
            {
                default:
                case int Int:
                    return (int)value >= _intMinValue;
                case decimal Decimal:
                    return Decimal >= _decimalMinValue;
                case float Float:
                    return Float >= _floatMinValue;
                case double Double:
                    return Double >= _doubleMinValue;
            }
        }

        public override string FormatErrorMessage(string name)
        {
            return $"عدد وارد شده برای {name} از حداقل مقدار کمتر است.";
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRule
            {
                ErrorMessage = String.IsNullOrEmpty(ErrorMessage) ? FormatErrorMessage(metadata.DisplayName) : ErrorMessage,
                ValidationType = "minvalue"
            };
        }
    }
}
