﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Xtoman.Framework.Mvc
{
    public class PersianDateTimeAttribute : RegularExpressionAttribute
    {
        public new string ErrorMessage { get; set; }

        public PersianDateTimeAttribute(string errorMessage)
            : base(@"^(13\d{2}|[1-9]\d)/(1[012]|0?[1-9])/([12]\d|3[01]|0?[1-9]) ([01][0-9]|2[0-3]):([0-5]?[0-9])(:([0-5]?[0-9]))?$")
        {
            ErrorMessage = errorMessage;
        }

        public override bool IsValid(object value)
        {
            if (value == null) return true;
            return Regex.IsMatch((value /*as PersianDateTime*/).ToString(), Pattern);
        }

        public override string FormatErrorMessage(string name)
        {
            return string.IsNullOrEmpty(ErrorMessage) ? string.Format("تاریخ {0} را بدرستی وارد کنید", name) : ErrorMessage;
        }
    }
}