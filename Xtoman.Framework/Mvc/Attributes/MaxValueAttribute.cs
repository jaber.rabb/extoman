﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Xtoman.Framework.Mvc.Attributes
{
    public class MaxValueAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly int _intMaxValue;
        private readonly decimal _decimalMaxValue;
        private readonly float _floatMaxValue;
        private readonly double _doubleMaxValue;

        public MaxValueAttribute(int maxValue)
        {
            _intMaxValue = maxValue;
        }

        public MaxValueAttribute(decimal maxValue)
        {
            _decimalMaxValue = maxValue;
        }

        public MaxValueAttribute(float maxValue)
        {
            _floatMaxValue = maxValue;
        }

        public MaxValueAttribute(double maxValue)
        {
            _doubleMaxValue = maxValue;
        }

        public override bool IsValid(object value)
        {
            switch (value)
            {
                default:
                case int Int:
                    return (int)value <= _intMaxValue;
                case decimal Decimal:
                    return Decimal <= _decimalMaxValue;
                case float Float:
                    return Float <= _floatMaxValue;
                case double Double:
                    return Double <= _doubleMaxValue;
            }
        }

        public override string FormatErrorMessage(string name)
        {
            return $"عدد وارد شده برای {name} از حداکثر مقدار بیشتر است.";
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRule
            {
                ErrorMessage = String.IsNullOrEmpty(ErrorMessage) ? FormatErrorMessage(metadata.DisplayName) : ErrorMessage,
                ValidationType = "maxvalue"
            };
        }
    }
}
