﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Blockchain;
using Xtoman.Framework.DependencyResolution;
using Xtoman.Framework.Mvc;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Framework
{
    [HandleError]
    public abstract class BaseController : Controller
    {
        private int? _fromId;
        private int? _toId;
        private string _ipAddress;
        private string _ipLocationCountryIso;
        private string _browser;
        //CF-IPCountry HTTP header. CloudFlare Country Finder
        public string DomainName
        {
            get
            {
                return CommonUtility.Domain;
            }
        }
        public string Browser
        {
            get
            {
                if (!_browser.HasValue())
                {
                    var browser = Request.Headers["user-agent"];
                    if (!browser.HasValue())
                    {
                        browser = Request.UserAgent;
                    }
                    _browser = browser;
                }
                return _browser;
            }
        }
        public string IPAddress
        {
            get
            {
                if (!_ipAddress.HasValue())
                {
                    var ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (!ipAddress.HasValue())
                    {
                        ipAddress = Request.ServerVariables["REMOTE_ADDR"];
                    }
                    _ipAddress = ipAddress;
                }
                return _ipAddress;
            }
        }
        public string IPCountryIsoCode
        {
            get
            {
                if (!_ipLocationCountryIso.HasValue())
                {
                    if (IPAddress.HasValue())
                        _ipLocationCountryIso = GeoIP.GetIPCountryIsoCode(IPAddress);
                }
                return _ipLocationCountryIso;
            }
        }
        public bool IsFromIran
        {
            get
            {
                if (IPCountryIsoCode.HasValue())
                    return IPCountryIsoCode.Equals("IR", StringComparison.InvariantCultureIgnoreCase);

                return true;
            }
        }
        public int? FromId
        {
            get
            {
                if (!_fromId.HasValue)
                {
                    if (Request.Cookies["exchangeFrom"] != null)
                    {
                        _fromId = Request.Cookies["exchangeFrom"].Value.ToInt();
                    }
                    else
                    {
                        return new int?();
                    }
                }
                return _fromId.Value;
            }
            set
            {
                if (value.HasValue)
                {
                    Response.Cookies["exchangeFrom"].Value = value.Value.ToString();
                    Response.Cookies["exchangeFrom"].Expires = DateTime.UtcNow.AddDays(7);
                    _fromId = value;
                }
            }
        }
        public int? ToId
        {
            get
            {
                if (!_toId.HasValue)
                {
                    if (Request.Cookies["exchangeTo"] != null)
                    {
                        _toId = Request.Cookies["exchangeTo"].Value.ToInt();
                    }
                    else
                    {
                        return new int?();
                    }
                }
                return _toId.Value;
            }
            set
            {
                if (value.HasValue)
                {
                    Response.Cookies["exchangeTo"].Value = value.Value.ToString();
                    Response.Cookies["exchangeTo"].Expires = DateTime.UtcNow.AddDays(7);
                    _toId = value;
                }
            }
        }

        private string _referrer;
        private int? _referrerId;
        public int? ReferrerId
        {
            get
            {
                if (!_referrerId.HasValue)
                {
                    _referrerId = Referrer.HasValue() ? Referrer.ToLower().Replace("x", "").ToInt() : new int?();
                }
                return _referrerId;
            }
        }
        public string Referrer
        {
            get
            {
                if (!_referrer.HasValue())
                {
                    if (Request.Cookies["XomanRef"] != null && Request.Cookies["XomanRef"]["id"] != null)
                    {
                        _referrer = Request.Cookies["XomanRef"]["id"].ToString();
                    }
                    else
                    {
                        return "";
                    }
                }
                return _referrer;
            }
            set
            {
                if (value.HasValue())
                {
                    Response.Cookies["XomanRef"]["id"] = value;
                    Response.Cookies["XomanRef"].Expires = DateTime.UtcNow.AddDays(3);
                    _referrer = value;
                }
            }
        }
        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception is HttpAntiForgeryException)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        { "action", "Login" },
                        { "controller", "Account" }
                    });

                filterContext.ExceptionHandled = true;
                filterContext.Exception.LogError();
            }
            else
            {
                //filterContext.Result = new RedirectToRouteResult(
                //new RouteValueDictionary
                //{
                //                    { "action", "Error" },
                //                    { "controller", "ServerError" }
                //});

                //filterContext.ExceptionHandled = true;
                //filterContext.Exception.LogError();
                base.OnException(filterContext);
            }
        }

        protected void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                if (error == "Incorrect password.")
                {
                    ModelState.AddModelError("", "کلمه عبور وارد شده اشتباه است.");
                }
                else
                {
                    ModelState.AddModelError("", error);
                }
            }
        }

        protected override ViewResult View(IView view, object model)
        {
            this.ViewBag.IsFromIran = IsFromIran;
            return base.View(view, model);
        }

        /// <summary>
        /// Render partial view to string
        /// </summary>
        /// <returns>Result</returns>
        public virtual string RenderPartialViewToString()
        {
            return RenderPartialViewToString(null, null);
        }
        /// <summary>
        /// Render partial view to string
        /// </summary>
        /// <param name="viewName">View name</param>
        /// <returns>Result</returns>
        public virtual string RenderPartialViewToString(string viewName)
        {
            return RenderPartialViewToString(viewName, null);
        }
        /// <summary>
        /// Render partial view to string
        /// </summary>
        /// <param name="model">Model</param>
        /// <returns>Result</returns>
        public virtual string RenderPartialViewToString(object model)
        {
            return RenderPartialViewToString(null, model);
        }
        /// <summary>
        /// Render partial view to string
        /// </summary>
        /// <param name="viewName">View name</param>
        /// <param name="model">Model</param>
        /// <returns>Result</returns>
        public virtual string RenderPartialViewToString(string viewName, object model)
        {
            //Original source code: http://craftycodeblog.com/2010/05/15/asp-net-mvc-render-partial-view-to-string/
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        /// <summary>
        /// Display success notification
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="persistForTheNextRequest">A value indicating whether a message should be persisted for the next request</param>
        protected virtual void InfoNotification(string message, bool persistForTheNextRequest = true)
        {
            AddNotification(NotifyType.Info, message, persistForTheNextRequest);
        }
        /// <summary>
        /// Display success notification
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="persistForTheNextRequest">A value indicating whether a message should be persisted for the next request</param>
        protected virtual void SuccessNotification(string message, bool persistForTheNextRequest = true)
        {
            AddNotification(NotifyType.Success, message, persistForTheNextRequest);
        }
        /// <summary>
        /// Display error notification
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="persistForTheNextRequest">A value indicating whether a message should be persisted for the next request</param>
        protected virtual void ErrorNotification(string message, bool persistForTheNextRequest = true)
        {
            AddNotification(NotifyType.Error, message, persistForTheNextRequest);
        }
        /// <summary>
        /// Display error notification
        /// </summary>
        /// <param name="exception">Exception</param>
        /// <param name="persistForTheNextRequest">A value indicating whether a message should be persisted for the next request</param>
        /// <param name="logException">A value indicating whether exception should be logged</param>
        protected virtual void ErrorNotification(Exception exception, bool persistForTheNextRequest = true, bool logException = true)
        {
            if (logException)
                exception.LogError();
            AddNotification(NotifyType.Error, exception.Message, persistForTheNextRequest);
        }
        /// <summary>
        /// Display notification
        /// </summary>
        /// <param name="type">Notification type</param>
        /// <param name="message">Message</param>
        /// <param name="persistForTheNextRequest">A value indicating whether a message should be persisted for the next request</param>
        protected virtual void AddNotification(NotifyType type, string message, bool persistForTheNextRequest)
        {
            string dataKey = string.Format("nop.notifications.{0}", type);
            if (persistForTheNextRequest)
            {
                if (TempData[dataKey] == null)
                    TempData[dataKey] = new List<string>();
                ((List<string>)TempData[dataKey]).Add(message);
            }
            else
            {
                if (ViewData[dataKey] == null)
                    ViewData[dataKey] = new List<string>();
                ((List<string>)ViewData[dataKey]).Add(message);
            }
        }

        /// <summary>
        /// Add locales for localizable entities
        /// </summary>
        /// <typeparam name="TLocalizedModelLocal">Localizable model</typeparam>
        /// <param name="languageService">Language service</param>
        /// <param name="locales">Locales</param>
        protected virtual Task AddLocalesAsync<TLocalizedModelLocal>(ILanguageService languageService, IList<TLocalizedModelLocal> locales) where TLocalizedModelLocal : ILocalizedModelLocal
        {
            return AddLocalesAsync(languageService, locales, null);
        }
        /// <summary>
        /// Add locales for localizable entities
        /// </summary>
        /// <typeparam name="TLocalizedModelLocal">Localizable model</typeparam>
        /// <param name="languageService">Language service</param>
        /// <param name="locales">Locales</param>
        /// <param name="configure">Configure action</param>
        protected virtual async Task AddLocalesAsync<TLocalizedModelLocal>(ILanguageService languageService, IList<TLocalizedModelLocal> locales, Action<TLocalizedModelLocal, int> configure) where TLocalizedModelLocal : ILocalizedModelLocal
        {
            var list = await languageService.TableNoTracking.Where(x => x.LanguageCode != "fa").ToListAsync();
            foreach (var language in list)
            {
                var locale = Activator.CreateInstance<TLocalizedModelLocal>();
                locale.LanguageId = language.Id;

                configure?.Invoke(locale, locale.LanguageId);

                locales.Add(locale);
            }
        }

        protected virtual async Task AddLocalesAsync<TModel, TLocalizedModelLocal>(ILanguageService languageService, TModel model, IList<TLocalizedModelLocal> locales)
            where TModel : BaseEntity, IBaseEntity, ILocalizedEntity
            where TLocalizedModelLocal : ILocalizedModelLocal
        {
            var list = await languageService.TableNoTracking.Where(x => x.LanguageCode != "fa").ToListAsync();
            foreach (var language in list)
            {
                //var locale = Activator.CreateInstance<TLocalizedModelLocal>();
                var locale = await model.GetLocalizedAsync<TModel, TLocalizedModelLocal>(language.Id);
                locale.LanguageId = language.Id;
                locales.Add(locale);
            }
        }

        protected virtual async Task AddLocalesWithUrlsAsync<TModel, TLocalizedModelLocal>(ILanguageService languageService, TModel model, IList<TLocalizedModelLocal> locales)
            where TModel : BaseEntity, IBaseEntity, ILocalizedEntity
            where TLocalizedModelLocal : ILocalizedModelLocal, ILocalizedUrlHistoryModel
        {
            var list = await languageService.TableNoTracking.Where(x => x.LanguageCode != "fa").ToListAsync();
            foreach (var language in list)
            {
                //var locale = Activator.CreateInstance<TLocalizedModelLocal>();
                var locale = await model.GetLocalizedAsync<TModel, TLocalizedModelLocal>(language.Id);
                ((ILocalizedModelLocal)locale).LanguageId = language.Id;
                locale.OriginalUrl = locale.Url;
                locales.Add(locale);
            }
        }

        protected T CreateController<T>() where T : BaseController
        {
            var controller = IoC.Container.GetInstance<T>();
            controller.ControllerContext = new ControllerContext(Request.RequestContext, controller);
            return controller;
        }

        protected HttpStatusCodeResult LogAndReturnResult(HttpStatusCode statusCod, string message = "")
        {
            if (message.HasValue() && message != "1") new Exception(message).LogError();
            return new HttpStatusCodeResult(HttpStatusCode.OK);  // OK = 200
        }

        public virtual string ImageSrcFromThumbnails(List<MediaThumbnail> thumbnails, string sizeName)
        {
            return thumbnails.Where(x => x.MediaThumbnailSize.Name == sizeName).Select(x => x.Url).FirstOrDefault();
        }

        public void LogMessage(string message)
        {
            new Exception(message).LogError();
        }

        protected ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        protected BlockcypherNetwork GetNetwork(string coin)
        {
            coin = coin.ToUpper();
            switch (coin)
            {
                case "BTC":
                default:
                    return BlockcypherNetwork.Bitcoin;
                case "DOGE":
                    return BlockcypherNetwork.Dogecoin;
                case "LTC":
                    return BlockcypherNetwork.Litecoin;
                case "DASH":
                    return BlockcypherNetwork.Dash;
            }
        }
    }
}
