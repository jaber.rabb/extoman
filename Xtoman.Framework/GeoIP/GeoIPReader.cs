﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Web.Hosting;
using Xtoman.Framework.DependencyResolution;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Framework
{
    public static class GeoIP
    {
        public static string GetIPCountryName(string ipStr)
        {
            var data = GetIPCountryData(ipStr);
            return data.Country.Names.En;
        }

        public static string GetIPCountryIsoCode(string ipStr)
        {
            var key = $"Xtoman.IPInfo.Iso.{ipStr}";
            var _cacheManager = IoC.Container.GetInstance<ICacheManager>();

            return _cacheManager.Get(key, () => {
                var data = GetIPCountryData(ipStr);
                if (data != null && data.Country != null)
                    return data.Country.IsoCode;
                return "";
            });
        }

        public static GeoIpModel GetIPCountryData(string ipStr)
        {
            var file = HostingEnvironment.MapPath(@"~/App_Data/GeoLite2-Country.mmdb");
            using (var reader = new MaxMind.Db.Reader(file))
            {
                var ip = IPAddress.Parse(ipStr);
                var data = reader.Find<Dictionary<string, object>>(ip).Serialize().Deserialize<GeoIpModel>();
                return data;
            }
        }

        public partial class GeoIpModel
        {
            [JsonProperty("continent")]
            public GeoIpContinent Continent { get; set; }

            [JsonProperty("country")]
            public GeoIpCountry Country { get; set; }

            [JsonProperty("registered_country")]
            public GeoIpCountry RegisteredCountry { get; set; }
        }

        public partial class GeoIpContinent
        {
            [JsonProperty("code")]
            public string Code { get; set; }

            [JsonProperty("geoname_id")]
            public long GeonameId { get; set; }

            [JsonProperty("names")]
            public GeoIpNames Names { get; set; }
        }

        public partial class GeoIpNames
        {
            [JsonProperty("de")]
            public string De { get; set; }

            [JsonProperty("en")]
            public string En { get; set; }

            [JsonProperty("es")]
            public string Es { get; set; }

            [JsonProperty("fr")]
            public string Fr { get; set; }

            [JsonProperty("ja")]
            public string Ja { get; set; }

            [JsonProperty("pt-BR")]
            public string PtBr { get; set; }

            [JsonProperty("ru")]
            public string Ru { get; set; }

            [JsonProperty("zh-CN")]
            public string ZhCn { get; set; }
        }

        public partial class GeoIpCountry
        {
            [JsonProperty("geoname_id")]
            public long GeonameId { get; set; }

            [JsonProperty("is_in_european_union")]
            public bool IsInEuropeanUnion { get; set; }

            [JsonProperty("iso_code")]
            public string IsoCode { get; set; }

            [JsonProperty("names")]
            public GeoIpNames Names { get; set; }
        }
    }
}
