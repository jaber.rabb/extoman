﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DelegateDecompiler.EntityFramework;
using PagedList;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Xtoman.Framework.Mvc;
using Xtoman.Utility;

namespace Xtoman.Framework.Core
{
    public static class AutoMapperExtensions
    {
        public static void ConfigureAutoMapper(this IMapperConfigurationExpression config)
        {
            var alltypes = Assembly.GetCallingAssembly().GetTypes();
            var viewModelTypes = alltypes.Where(type => type.IsClass && type.BaseType != null
            && type.BaseType.IsGenericType && type.BaseType.GetGenericTypeDefinition() == typeof(BaseViewModel<,>))
            .Select(type =>
            {
                var arguments = type.BaseType.GetGenericArguments();
                var interfaces = type.GetInterfaces();
                var localizedInterface = interfaces.SingleOrDefault(p => p.IsGenericType && p.GetGenericTypeDefinition() == typeof(ILocalizedModel<>));
                return new
                {
                    ViewModel = arguments[0],
                    Entity = arguments[1],
                    Locale = localizedInterface?.GetGenericArguments()[0]
                };
            }).ToList();

            foreach (var type in viewModelTypes)
            {
                config.CreateMap(type.Entity, type.ViewModel).ReverseMap();
                if (type.Locale != null)
                    config.CreateMap(type.Locale, type.Entity).ReverseMap();
            }
        }


        public static async Task<PagedList.IPagedList<TDestination>> ProjectToPagedListAsync<TDestination>(this IQueryable queryable, int pageNumber, int pageSize)
            where TDestination : BaseViewModel
        {
            var skip = (pageNumber - 1) * pageSize;
            var query = queryable.ProjectTo<TDestination>().DecompileAsync().OrderByDescending(x => x.Id);
            var totalCount = await query.CountAsync();
            var list = await query.Skip(skip).Take(pageSize).ToListAsync();
            return new StaticPagedList<TDestination>(list, pageNumber, pageSize, totalCount);
        }

        public static async Task<PagedList.IPagedList<TDestination>> ProjectToPagedListAsync<TDestination>(this IQueryable queryable, Expression<Func<TDestination, object>> orderByDescKeySelector, int pageNumber, int pageSize)
    where TDestination : BaseViewModel
        {
            var skip = (pageNumber - 1) * pageSize;
            var query = queryable.ProjectTo<TDestination>().DecompileAsync().OrderByDescending(x => x.Id).ThenBy(orderByDescKeySelector);
            var totalCount = await query.CountAsync();
            var list = await query.Skip(skip).Take(pageSize).ToListAsync();
            return new StaticPagedList<TDestination>(list, pageNumber, pageSize, totalCount);
        }

        public static async Task<PagedList.IPagedList<TDestination>> ProjectToPagedListAsync<TDestination>(this IQueryable queryable, int pageNumber, int pageSize, Func<TDestination, Task> func)
            where TDestination : BaseViewModel
        {
            var skip = (pageNumber - 1) * pageSize;
            var query = queryable.ProjectTo<TDestination>().DecompileAsync().OrderByDescending(x => x.Id);
            var totalCount = query.Count();
            var list = query.Skip(skip).Take(pageSize).ToList();
            for (int i = 0; i < list.Count; i++)
                await func(list[i]);
            return new StaticPagedList<TDestination>(list, pageNumber, pageSize, totalCount);
        }
    }

}
