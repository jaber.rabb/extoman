// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultRegistry.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Xtoman.Framework.DependencyResolution
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.Owin.Security;
    using Microsoft.Owin.Security.Infrastructure;
    using Microsoft.Owin.Security.OAuth;
    using StructureMap;
    using StructureMap.Web;
    using System;
    using System.Data.Entity;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Web;
    using Xtoman.Data;
    using Xtoman.Domain.Models;
    using Xtoman.Domain.Settings;
    using Xtoman.Framework.WebApi.Providers;
    using Xtoman.Framework.WebApi.Token;
    using Xtoman.Jobs;
    using Xtoman.Service;
    using Xtoman.Utility.Caching;

    public class DefaultRegistry : Registry
    {
        public DefaultRegistry()
        {
            Scan(
                scan =>
                {
                    scan.Assembly(typeof(PostService).Assembly);
                    scan.Assembly(typeof(BaseEntity).Assembly);
                    scan.Assembly(typeof(MessageJobs).Assembly);
                    scan.WithDefaultConventions();
                    scan.With(new RepositoryConvention());
                    scan.With(new SettingConvention());
                    scan.With(new ControllerConvention());
                });

            For<IIdentity>().Use(() => GetIdentity());
            For<IUnitOfWork>().HybridHttpOrThreadLocalScoped().Use<AppDbContext>();
            //.Ctor<string>("connectionString")
            //.Is("Data Source=(local);Initial Catalog=TestDbIdentity;Integrated Security = true");
            For<AppDbContext>().HybridHttpOrThreadLocalScoped().Use(context => (AppDbContext)context.GetInstance<IUnitOfWork>());
            For<DbContext>().HybridHttpOrThreadLocalScoped().Use(context => (AppDbContext)context.GetInstance<IUnitOfWork>());
            For(typeof(IRepository<>)).HybridHttpOrThreadLocalScoped().Use(typeof(RepositoryBase<>));
            For<ICacheManager>().HybridHttpOrThreadLocalScoped().Use<MemoryCacheManager>();

            #region Identity
            For<IUserStore<AppUser, int>>().HybridHttpOrThreadLocalScoped().Use<AppUserStore>();
            For<IRoleStore<AppRole, int>>().HybridHttpOrThreadLocalScoped().Use<RoleStore<AppRole, int, AppUserRole>>();
            For<IAuthenticationManager>().Use(() => HttpContext.Current.GetOwinContext().Authentication);
            For<IAppSignInManager>().HybridHttpOrThreadLocalScoped().Use<AppSignInManager>();
            For<IAppRoleManager>().HybridHttpOrThreadLocalScoped().Use<AppRoleManager>();
            // map same interface to different concrete classes
            For<IIdentityMessageService>().Use<SmsService>();
            For<IIdentityMessageService>().Use<EmailService>();
            For<IAppUserManager>().HybridHttpOrThreadLocalScoped()
                .Use<AppUserManager>()
                .Ctor<IIdentityMessageService>("smsService").Is<SmsService>()
                .Ctor<IIdentityMessageService>("emailService").Is<EmailService>()
                .Setter(userManager => userManager.SmsService).Is<SmsService>()
                .Setter(userManager => userManager.EmailService).Is<EmailService>();
            For<AppUserManager>().HybridHttpOrThreadLocalScoped().Use(context => (AppUserManager)context.GetInstance<IAppUserManager>());
            For<IAppRoleStore>().HybridHttpOrThreadLocalScoped().Use<AppRoleStore>();
            For<IAppUserStore>().HybridHttpOrThreadLocalScoped().Use<AppUserStore>();
            For<ICurrentUser>().HybridHttpOrThreadLocalScoped().Use<CurrentUser>();
            #endregion

            #region Setting
            For<BlogSettings>().Use(context => context.GetInstance<SettingService>().LoadSetting<BlogSettings>());
            For<CacheSettings>().Use(context => context.GetInstance<SettingService>().LoadSetting<CacheSettings>());
            For<LocalizationSettings>().Use(context => context.GetInstance<SettingService>().LoadSetting<LocalizationSettings>());
            For<PaymentSettings>().Use(context => context.GetInstance<SettingService>().LoadSetting<PaymentSettings>());
            For<MessageSettings>().Use(context => context.GetInstance<SettingService>().LoadSetting<MessageSettings>());
            For<CommentSettings>().Use(context => context.GetInstance<SettingService>().LoadSetting<CommentSettings>());
            #endregion

            #region WebApi
            Policies.SetAllProperties(setterConvention =>
            {
                // For WebAPI ActionFilter Dependency Injection
                setterConvention.OfType<Func<IAppUserManager>>();
                setterConvention.OfType<Func<IApiTokenService>>();
            });
            // we only need one instance of this provider
            For<IOAuthAuthorizationServerProvider>().Singleton().Use<AppOAuthProvider>();
            For<IAuthenticationTokenProvider>().Singleton().Use<RefreshTokenProvider>();
            #endregion
        }

        private static IIdentity GetIdentity()
        {
            if (HttpContext.Current?.User != null)
                return HttpContext.Current.User.Identity;
            return ClaimsPrincipal.Current?.Identity;
        }
    }
}