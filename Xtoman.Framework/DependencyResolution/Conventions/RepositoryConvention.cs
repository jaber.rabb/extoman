﻿using StructureMap;
using StructureMap.Graph;
using StructureMap.Graph.Scanning;
using StructureMap.TypeRules;
using StructureMap.Web;
using System.Linq;
using Xtoman.Data;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Framework.DependencyResolution
{
    public class RepositoryConvention : IRegistrationConvention
    {
        public void ScanTypes(TypeSet types, Registry registry)
        {
            var list = types.FindTypes(TypeClassification.Concretes | TypeClassification.Closed)
                .Where(p => p.CanBeCreated() && (p.CanBeCastTo(typeof(IRepository<>)) || p.CanBeCastTo(typeof(WebServiceManager)))).ToList();
            foreach (var type in list)
            {
                type.GetInterfaces().ToList().ForEach(itype => registry.For(itype).HybridHttpOrThreadLocalScoped().Use(type));
            }
        }
    }
}
