﻿using StructureMap;
using StructureMap.Graph;
using StructureMap.Graph.Scanning;
using StructureMap.TypeRules;
using StructureMap.Web;
using System;
using System.Linq;

namespace Xtoman.Framework.DependencyResolution
{
    public class HybridHttpOrThreadLocalScopedConvention : IRegistrationConvention
    {
        public Type[] Types { get; set; }

        public HybridHttpOrThreadLocalScopedConvention(params Type[] types)
        {
            Types = types;
        }

        public void ScanTypes(TypeSet types, Registry registry)
        {
            var list = types.FindTypes(TypeClassification.Concretes | TypeClassification.Closed)
                .Where(p => p.CanBeCreated() && Types.Any(q => p.CanBeCastTo(q)));
            foreach (var type in list)
            {
                type.GetInterfaces().ToList().ForEach(itype => registry.For(itype).HybridHttpOrThreadLocalScoped().Use(type));
            }
        }
    }
}
