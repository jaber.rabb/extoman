﻿using StructureMap;
using StructureMap.Graph;
using StructureMap.Graph.Scanning;
using StructureMap.TypeRules;
using System.Linq;
using Xtoman.Domain.Settings;
using Xtoman.Service;

namespace Xtoman.Framework.DependencyResolution
{
    public class SettingConvention : IRegistrationConvention
    {
        public void ScanTypes(TypeSet types, Registry registry)
        {
            var list = types.FindTypes(TypeClassification.Concretes | TypeClassification.Closed)
                .Where(p => p.CanBeCreated() && p.CanBeCastTo(typeof(ISettings))).ToList();
            foreach (var type in list)
            {
                registry.For(type).Use(context => context.GetInstance<ISettingService>().LoadSetting(type));
            }
        }
    }
}
