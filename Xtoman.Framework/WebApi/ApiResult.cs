﻿namespace Xtoman.Framework.WebApi
{
    public class ApiResult
    {
        public string Error { get; set; }
        public ApiStatusCode Status { get; set; }
    }

    public class ApiResult<T> : ApiResult /* where T : new()*/
    {
        //public ApiResult()
        //{
        //    Data = new T();
        //}
        public T Data { get; set; }
    }
}
