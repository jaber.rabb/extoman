﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Framework.WebApi
{
    public enum ApiStatusCode
    {
        [Display(Name = "عملیات با موفقیت انجام شد")]
        Success = 0,
        [Display(Name = "ورودی نادرست است")]
        InvalidParameter = 1,
        [Display(Name = "خطایی در سرور رخ داده است")]
        InternalServerError = 2,
        [Display(Name = "درخواست شما نادرست است")]
        BadRequest = 3,
        [Display(Name = "یافت نشد")]
        NotFound = 4
    }
}
