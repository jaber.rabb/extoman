﻿using Xtoman.Utility;
using System;
using System.Diagnostics.Contracts;
using System.Web.Http.Dispatcher;

namespace Xtoman.Framework.WebApi.Services
{
    //config.Services.Replace(typeof(IHttpControllerTypeResolver), new BaseApiControllerTypeResolver());
    public class BaseApiControllerTypeResolver : DefaultHttpControllerTypeResolver
    {
        public BaseApiControllerTypeResolver() : base(IsHttpEndpoint)
        {
        }

        internal static bool IsHttpEndpoint(Type type)
        {
            CheckHelper.NotNull(type, nameof(type));

            //Contract.Assert(type != null);
            //return
            //type != null &&
            //type.IsClass &&
            //type.IsVisible &&
            //!type.IsAbstract &&
            //typeof(IHttpController).IsAssignableFrom(type) /*&& HasValidControllerName(type)*/;

            return
             type.IsClass
             && type.IsVisible
             && !type.IsAbstract
            && typeof(BaseApiController).IsAssignableFrom(type);
        }

        internal static bool HasValidControllerName(Type controllerType)
        {
            Contract.Assert(controllerType != null);
            string controllerSuffix = DefaultHttpControllerSelector.ControllerSuffix;
            return controllerType.Name.Length > controllerSuffix.Length && controllerType.Name.EndsWith(controllerSuffix, StringComparison.OrdinalIgnoreCase);
        }
    }

    //config.Services.Replace(typeof(IAssembliesResolver), new MyAssemblyResolver());
    //public class MyAssembliesResolver : DefaultAssembliesResolver
    //{
    //    public override ICollection<Assembly> GetAssemblies()
    //    {
    //        ICollection<Assembly> baseAssemblies = base.GetAssemblies();
    //        var controllersAssembly = Assembly.LoadFrom("c:/myAssymbly.dll");
    //        baseAssemblies.Add(controllersAssembly);
    //        return baseAssemblies;
    //    }
    //}
}
