﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Xtoman.Framework.DependencyResolution
{
    public class IoCFilterProvider : ActionDescriptorFilterProvider
    {
        public new IEnumerable<FilterInfo> GetFilters(HttpConfiguration configuration, HttpActionDescriptor actionDescriptor)
        {
            var filters = base.GetFilters(configuration, actionDescriptor);

            foreach (var filter in filters)
            {
                IoC.Container.BuildUp(filter.Instance);
                yield return filter;
            }
        }
    }
}
