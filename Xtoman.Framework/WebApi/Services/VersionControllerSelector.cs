﻿using Xtoman.Utility;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace Xtoman.Framework.WebApi.Services
{
    //config.Services.Replace(typeof(IHttpControllerSelector), new VersionControllerSelector(config));
    public class VersionControllerSelector : DefaultHttpControllerSelector
    {
        public VersionControllerSelector(HttpConfiguration configuration) : base(configuration)
        {
        }

        public override string GetControllerName(HttpRequestMessage request)
        {
            var controllerName = base.GetControllerName(request);
            var version = GetUriVersioning(request);

            var versionControllerName = version == 0 ? $"{controllerName}v1" : $"{controllerName}v{version}";
            if (GetControllerMapping().TryGetValue(versionControllerName, out HttpControllerDescriptor descriptor))
                return versionControllerName;

            versionControllerName = controllerName;
            if (GetControllerMapping().TryGetValue(versionControllerName, out descriptor))
                return versionControllerName;


            throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.NotFound, $"No HTTP resource was found that matches the URI {request.RequestUri} for version number {version}"));
        }

        private int GetUriVersioning(HttpRequestMessage request)
        {
            var routeData = request.GetRouteData();
            if (routeData != null)
            {
                var versionString = routeData.GetRouteVariable<string>("version");
                if (versionString != null)
                {
                    if (!string.IsNullOrWhiteSpace(versionString))
                    {
                        if (int.TryParse(versionString, out int intVersion))
                        {
                            return intVersion;
                        }
                    }
                }
            }
            return 0;
        }
    }
}
