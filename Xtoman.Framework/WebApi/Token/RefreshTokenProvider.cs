﻿using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Framework.WebApi.Token
{
    public class RefreshTokenProvider : IAuthenticationTokenProvider
    {
        private readonly Func<IApiTokenService> _apiTokenService;
        private readonly IJwtConfiguration _configuration;

        public RefreshTokenProvider(Func<IApiTokenService> apiTokenService, IJwtConfiguration configuration)
        {
            //apiTokenService.CheckArgumentNull(nameof(apiTokenService));
            _apiTokenService = apiTokenService;
            _configuration = configuration;
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            CreateAsync(context).RunSynchronously();
        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var refreshTokenId = Guid.NewGuid().ToString("n");

            var now = DateTime.UtcNow;
            var ownerUserId = context.Ticket.Identity.FindFirst(ClaimTypes.UserData).Value;
            var token = new ApiToken
            {
                UserId = int.Parse(ownerUserId),
                // Refresh token handles should be treated as secrets and should be stored hashed
                RefreshTokenIdHash = CryptographyHelper.ToSHA256Hash(refreshTokenId),
                RefreshTokenExpiresUtc = now.AddMinutes(_configuration.RefreshTokenExpirationMinutes),
                AccessTokenExpirationDateTime = now.AddMinutes(_configuration.ExpirationMinutes)
            };

            context.Ticket.Properties.IssuedUtc = now;
            context.Ticket.Properties.ExpiresUtc = token.RefreshTokenExpiresUtc;

            token.RefreshToken = context.SerializeTicket();

            var service = _apiTokenService();
            await service.CreateApiToken(token);
            await service.DeleteExpiredTokens();

            context.SetToken(refreshTokenId);
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            ReceiveAsync(context).RunSynchronously();
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var service = _apiTokenService();
            var hashedTokenId = CryptographyHelper.ToSHA256Hash(context.Token);
            var refreshToken = await service.FindTokenByHash(hashedTokenId);
            if (refreshToken != null)
            {
                context.DeserializeTicket(refreshToken.RefreshToken);
                await service.DeleteToken(hashedTokenId);
            }
        }
    }

}
