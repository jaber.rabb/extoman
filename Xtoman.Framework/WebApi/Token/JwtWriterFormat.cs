﻿using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;
using Xtoman.Utility;

namespace Xtoman.Framework.WebApi.Token
{
    public class JwtWriterFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private readonly IJwtConfiguration _config;
        public JwtWriterFormat(IJwtConfiguration config)
        {
            _config = config; /*IoC.Container.GetInstance<IJwtConfiguration>()*/
        }

        public string Protect(AuthenticationTicket data)
        {
            CheckHelper.NotNull(data, nameof(data));

            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            RSAParameters keyParams;
            using (var rsa = new RSACryptoServiceProvider(1024))
            {
                try
                {
                    keyParams = rsa.ExportParameters(true);
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
            RsaSecurityKey key = new RsaSecurityKey(keyParams);
            var signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

            var token = new JwtSecurityToken(
                _config.Issuer, _config.AllowedAudiences[0],
                data.Identity.Claims, issued.Value.UtcDateTime, 
                expires.Value.UtcDateTime, 
                signingCredentials
            );

            var result = new JwtSecurityTokenHandler().WriteToken(token);
            return result;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}