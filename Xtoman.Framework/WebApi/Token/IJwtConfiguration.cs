﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Jwt;
using System;
using System.Collections.Generic;
using System.ServiceModel.Security.Tokens;

namespace Xtoman.Framework.WebApi.Token
{
    public interface IJwtConfiguration
    {
        bool AllowInsecureHttp { get; set; }
        string Issuer { get; set; }
        string Secret { get; set; }
        string TokenPath { get; set; }
        string AuthorizePath { get; set; }
        int ExpirationMinutes { get; set; }
        int RefreshTokenExpirationMinutes { get; set; }
        PathString TokenEndpointPath { get; set; }
        PathString AuthorizeEndpointPath { get; set; }
        TimeSpan AccessTokenExpireTimeSpan { get; set; }
        //ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; set; }
        //IOAuthAuthorizationServerProvider Provider { get; set; }
        //IAuthenticationTokenProvider RefreshTokenProvider { get; set; }
        AuthenticationMode AuthenticationMode { get; set; }
        string[] AllowedAudiences { get; set; }
        IEnumerable<IssuedSecurityTokenProvider> IssuerSecurityTokenProviders { get; set; }
    }
}
