﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Service;

namespace Xtoman.Framework.WebApi.Providers
{
    public class AppOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly Func<IAppUserManager> _applicationUserManager;

        public AppOAuthProvider(Func<IAppUserManager> applicationUserManager)
        {
            _applicationUserManager = applicationUserManager;
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            //var user = await _applicationUserManager().FindAsync(context.UserName, context.Password);
            var user = await _applicationUserManager().FindByEmailOrUsernameOrPhoneAsync(context.UserName, context.Password);
            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                context.Rejected();
                return;
            }

            var identity = SetClaimsIdentity(context, user);
            context.Validated(identity);
        }

        private ClaimsIdentity SetClaimsIdentity(OAuthGrantResourceOwnerCredentialsContext context, AppUser user)
        {
            var identity = new ClaimsIdentity(authenticationType: "JWT");
            identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
            identity.AddClaim(new Claim(ClaimTypes.UserData, user.Id.ToString()));

            var roles = user.Roles.Select(p => p.Role.Name);
            foreach (var role in roles)
                identity.AddClaim(new Claim(ClaimTypes.Role, role));

            return identity;
        }
    }
}
