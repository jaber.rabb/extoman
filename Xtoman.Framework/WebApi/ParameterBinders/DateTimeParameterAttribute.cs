﻿using System;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Xtoman.Framework.WebApi.ParameterBinders
{
    public class DateTimeParameterAttribute : ParameterBindingAttribute
    {
        public string DateFormat { get; set; }

        public bool ReadFromQueryString { get; set; }


        public override HttpParameterBinding GetBinding(HttpParameterDescriptor parameter)
        {
            if (parameter.ParameterType == typeof(DateTime?))
            {
                var binding = new DateTimeParameterBinding(parameter)
                {
                    DateFormat = DateFormat,
                    ReadFromQueryString = ReadFromQueryString
                };
                return binding;
            }

            return parameter.BindAsError("Type DateTime? Expected.");
        }
    }
}
