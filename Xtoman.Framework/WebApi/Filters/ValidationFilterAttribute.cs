﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Xtoman.Framework.WebApi.Filters
{
    public class ValidationFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ModelState.IsValid)
            {
                var result = new ApiResult
                {
                    Status = ApiStatusCode.InvalidParameter,
                    Error = string.Join(" | ", actionContext.ModelState.SelectMany(p => p.Value.Errors).Select(p => p.ErrorMessage))
                };
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, result);
                //actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.OK, actionContext.ModelState);
            }
        }
    }
}
