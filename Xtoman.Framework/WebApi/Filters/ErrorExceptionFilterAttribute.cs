﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace Xtoman.Framework.WebApi.Filters
{
    public class ErrorExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            HttpResponseMessage response;
            if (actionExecutedContext.Exception is ErrorException)
            {
                var ex = actionExecutedContext.Exception as ErrorException;
                var result = new ApiResult()
                {
                    Status = ex.Status,
                    Error = ex.Error
                };
                response = actionExecutedContext.Request.CreateResponse(HttpStatusCode.OK, result);

                //var configuration = actionExecutedContext.ActionContext.ControllerContext.Configuration; //actionExecutedContext.ActionContext.RequestContext.Configuration
                //var negotiator = configuration.Services.GetContentNegotiator().Negotiate(typeof(ApiResult), actionExecutedContext.Request, configuration.Formatters);
                //response = new HttpResponseMessage(HttpStatusCode.OK) { Content = new ObjectContent<ApiResult>(result, negoResult.Formatter, negoResult.MediaType) };
                //response = actionExecutedContext.Request.CreateResponse(HttpStatusCode.OK, result, JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                var result = new ApiResult()
                {
                    Status = ApiStatusCode.InternalServerError,
                    Error = actionExecutedContext.Exception.Message
                };
                response = actionExecutedContext.Request.CreateResponse(HttpStatusCode.OK, result);
            }
            actionExecutedContext.Response = response;
        }
    }
}

namespace Xtoman.Framework.WebApi
{
    public class ErrorException : Exception
    {
        public ErrorException(ApiStatusCode status, string error) : base(error)
        {
            Status = status;
            Error = error;
        }

        public ErrorException(ApiStatusCode status, string error, Exception innerException) : base(error, innerException)
        {
            Status = status;
            Error = error;
        }

        public ApiStatusCode Status { get; set; }
        public string Error { get; set; }
    }

    public static class ErrorExceptionExtentions
    {
        public static ErrorException ToErrorException(this Exception exception, ApiStatusCode status, string error)
        {
            return new ErrorException(status, error, exception);
        }
    }
}