﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Xtoman.Framework.DependencyResolution;
using Xtoman.Service;

namespace Xtoman.Framework.WebApi.Authorization
{
    public class BasicAuthorizeAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Authorization == null)
            {
                UnAuthorizeResponse(actionContext);
                return;
            }

            var param = actionContext.Request.Headers.Authorization.Parameter;
            var decodedCridential = Encoding.UTF8.GetString(Convert.FromBase64String(param));
            var splitedArray = decodedCridential.Split(':');
            if (splitedArray.Length == 2)
            {
                var userName = splitedArray[0];
                var password = splitedArray[1];
                var userService = IoC.Container.GetInstance<UserService>();
                var userManager = IoC.Container.GetInstance<AppUserManager>();
                var userId = userService.GetUserIdByUsernameOrEmailOrPhoneAsync(userName).Result;

                var genericIdentity = new GenericIdentity(userId.ToString());
                var genericPrincipal = new GenericPrincipal(genericIdentity, null);
                
                Thread.CurrentPrincipal = genericPrincipal;
                return;
            }
            //if (splitedArray[0] == "username" && splitedArray[1] == "password")
            //{
            //    //Read from database
            //    const int userId = 1;
            //    var genericIdentity = new GenericIdentity(userId.ToString());
            //    var genericPrincipal = new GenericPrincipal(genericIdentity, null);
            //    Thread.CurrentPrincipal = genericPrincipal;
            //    return;
            //}
            UnAuthorizeResponse(actionContext);
        }

        private void UnAuthorizeResponse(HttpActionContext actionContext)
        {
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
        }
    }
}
