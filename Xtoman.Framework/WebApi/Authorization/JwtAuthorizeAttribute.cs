﻿using Xtoman.Service;
using System;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Xtoman.Framework.WebApi.Authorization
{
    public class JwtAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Using Func here, creates transient IApplicationUserManager's
        /// </summary>
        public Func<IAppUserManager> ApplicationUserManager { set; get; }

        /// <summary>
        /// Using Func here, creates transient IApiTokenService's
        /// </summary>
        public Func<IApiTokenService> ApiTokenService { set; get; }

        public override /*async*/ void OnAuthorization(HttpActionContext actionContext)
        {
            if (skipAuthorization(actionContext))
                return;

            var accessToken = actionContext.Request.Headers.Authorization?.Parameter;
            if (string.IsNullOrWhiteSpace(accessToken) || accessToken.Equals("undefined", StringComparison.OrdinalIgnoreCase))
            {
                // null token
                HandleUnauthorizedRequest(actionContext);
                return;
            }

            var claimsIdentity = actionContext.RequestContext.Principal.Identity as ClaimsIdentity;
            if (claimsIdentity?.Claims == null || !claimsIdentity.Claims.Any())
            {
                // this is not our issued token
                HandleUnauthorizedRequest(actionContext);
                //actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                return;
            }

            //var userId = claimsIdentity.FindFirst(ClaimTypes.UserData).Value;
            //var rowVersionClaim = claimsIdentity.FindFirst(ClaimTypes.SerialNumber);
            //if (rowVersionClaim == null)
            //{
            //    // this is not our issued token
            //    HandleUnauthorizedRequest(actionContext);
            //    return;
            //}

            //if (ApplicationUserManager == null)
            //    throw new NullReferenceException($"{nameof(ApplicationUserManager)} is null. Make sure ioc.Policies.SetAllProperties is configured and also IFilterProvider is replaced with SmWebApiFilterProvider.");

            //var rowVersion = await ApplicationUserManager().GetApiRowVersion(int.Parse(userId));
            //if (rowVersion != rowVersionClaim.Value)
            //{
            //    // user has changed its password/roles/stat/IsActive
            //    HandleUnauthorizedRequest(actionContext);
            //    return;
            //}

            //if (ApiTokenService == null)
            //    throw new NullReferenceException($"{nameof(ApiTokenService)} is null. Make sure ioc.Policies.SetAllProperties is configured and also IFilterProvider is replaced with SmWebApiFilterProvider.");

            //if (!await ApiTokenService().IsValidToken(accessToken, int.Parse(userId)))
            //{
            //    // this is not our issued token
            //    HandleUnauthorizedRequest(actionContext);
            //    return;
            //}

            base.OnAuthorization(actionContext);
        }

        private static bool skipAuthorization(HttpActionContext actionContext)
        {
            return actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Count > 0
                || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Count > 0;
        }
    }
}
