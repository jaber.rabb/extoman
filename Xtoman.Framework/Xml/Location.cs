﻿using System;
using System.Collections.Generic;

namespace Xtoman.Framework.Xml
{
    public class Location
    {
        public string Url { get; set; }
        public DateTime? LastModified { get; set; }
        public ChangeFreq? ChangeFrequency { get; set; }
        public float? Priority { get; set; }
        public List<Image> Images { get; set; }
    }
}
