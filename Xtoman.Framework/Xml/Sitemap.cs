﻿using System.Collections.Generic;

namespace Xtoman.Framework.Xml
{
    public class Sitemap
    {
        public Sitemap()
        {
            Locations = new List<Location>();
        }

        public List<Location> Locations { get; set; }
    }
}
