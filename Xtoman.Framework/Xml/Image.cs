﻿namespace Xtoman.Framework.Xml
{
    public class Image
    {
        public string Url { get; set; }
        public string Caption { get; set; }
        public string GeoLocation { get; set; }
        public string Title { get; set; }
    }
}
