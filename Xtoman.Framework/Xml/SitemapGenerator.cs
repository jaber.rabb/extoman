﻿using Xtoman.Utility;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace Xtoman.Framework.Xml
{
    public class SitemapGenerator
    {
        private readonly Sitemap _sitemap;
        private /*readonly*/ bool _cData;
        private XmlTextWriter _writer;
        private const string DateFormat = "yyyy-MM-ddTHH:mm:ss+03:30"; //"r"; //"yyyy-MM-dd";

        public SitemapGenerator(Sitemap sitemap, bool cData = false)
        {
            _cData = cData;
            _sitemap = sitemap;
        }

        public string Generate()
        {
            var hasImagesAll = _sitemap.Locations.Any(p => p.Images.Count > 0);
            using (var stream = new MemoryStream())
            {
                using (_writer = new XmlTextWriter(stream, Encoding.UTF8))
                {
                    _writer.Formatting = Formatting.Indented;
                    _writer.WriteStartDocument();
                    _writer.WriteStartElement("urlset");
                    _writer.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
                    _writer.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                    _writer.WriteAttributeString("xsi:schemaLocation", "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");
                    if (hasImagesAll)
                        _writer.WriteAttributeString("xmlns:image", "http://www.google.com/schemas/sitemap-image/1.1");

                    foreach (var item in _sitemap.Locations)
                    {
                        var hasImages = item.Images.Count > 0;
                        _writer.WriteStartElement("url");
                        const string loc = "sss";// XmlHelper.XmlEncode(item.Url);
                        _writer.WriteElementString("loc", loc);
                        if (item.ChangeFrequency.HasValue)
                            _writer.WriteElementString("changefreq", item.ChangeFrequency.ToString().ToLowerInvariant());
                        if (item.LastModified.HasValue)
                            _writer.WriteElementString("lastmod", item.LastModified.Value.ToString(DateFormat));
                        if (item.Priority.HasValue)
                            _writer.WriteElementString("priority", item.Priority.ToString());
                        if (hasImages)
                        {
                            _writer.WriteStartElement("image:image");
                            foreach (var image in item.Images)
                            {
                                //const string SpecialChars = @"<>&";
                                //_cData = image.Caption.Contains(SpecialChars);

                                _writer.WriteElementString("image:loc", image.Url);
                                if (image.Caption.HasValue())
                                {
                                    if (_cData)
                                        _writer.WriteElementCData("image:caption", image.Caption);
                                    else
                                        _writer.WriteElementString("image:caption", image.Caption);
                                }
                                if (image.Title.HasValue())
                                {
                                    if (_cData)
                                        _writer.WriteElementCData("image:title", image.Title);
                                    else
                                        _writer.WriteElementString("image:title", image.Title);
                                }
                                if (image.GeoLocation.HasValue())
                                {
                                    if (_cData)
                                        _writer.WriteElementCData("image:geo_location", image.GeoLocation);
                                    else
                                        _writer.WriteElementString("image:geo_location", image.GeoLocation);
                                }
                            }
                            _writer.WriteEndElement();
                        }
                        _writer.WriteEndElement();
                    }

                    _writer.WriteEndElement();
                }
                return Encoding.UTF8.GetString(stream.ToArray());
            }
        }
    }
}
