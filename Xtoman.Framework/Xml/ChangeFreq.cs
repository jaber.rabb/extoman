﻿namespace Xtoman.Framework.Xml
{
    public enum ChangeFreq
    {
        Always,
        Hourly,
        Daily,
        Weekly,
        Monthly,
        Yearly,
        Never
    }
}
