﻿using QRCoder;
using System;
using System.Drawing;
using System.Net;

namespace Xtoman.Framework
{
    public static class QRGenerator
    {
        public static string GetImageBase64String(string text)
        {
            var qrGenerator = new QRCodeGenerator();
            var qrCodeData = qrGenerator.CreateQrCode(text, QRCodeGenerator.ECCLevel.Q);
            var qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            return "data:image/jpeg;base64, " + Convert.ToBase64String(qrCodeImage.ToByte());
        }
        public static byte[] ToByte(this Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        [Obsolete("It will or must lock the thread, find another way")]
        public static string GetImageBase64StringFromUrl(string qrcode_url)
        {
            var result = "";
            using (var client = new WebClient())
            {
                using (var stream = client.OpenRead(qrcode_url))
                {
                    var bitmap = new Bitmap(stream);
                    result = Convert.ToBase64String(bitmap.ToByte());
                }
            }
            return result;
        }
    }
}
