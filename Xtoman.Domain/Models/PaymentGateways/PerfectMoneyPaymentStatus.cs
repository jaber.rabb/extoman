﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    public class PerfectMoneyPaymentStatus : BaseEntity
    {
        public PerfectMoneyPaymentStatus()
        {
            StatusDate = DateTime.UtcNow;
        }
        public string PAYEE_ACCOUNT { get; set; }
        public string PAYMENT_ID { get; set; }
        public string PAYMENT_AMOUNT { get; set; }
        public string PAYMENT_UNITS { get; set; }
        public string PAYMENT_BATCH_NUM { get; set; }
        public string PAYER_ACCOUNT { get; set; }
        public string TIMESTAMPGMT { get; set; }
        public string V2_HASH { get; set; }
        public string USERNAME { get; set; }
        public string BAGGAGE_FIELDS { get; set; }
        public DateTime StatusDate { get; set; }
    }

    public class PerfectMoneyPaymentStatusConfig : EntityTypeConfiguration<PerfectMoneyPaymentStatus>
    {
        public PerfectMoneyPaymentStatusConfig()
        {
        }
    }
}
