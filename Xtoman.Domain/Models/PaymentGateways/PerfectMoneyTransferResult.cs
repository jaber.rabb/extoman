﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    public class PerfectMoneyTransferResult : BaseEntity
    {
        public string ERROR { get; set; }
        public string Payee_Account_Name { get; set; }
        public string Payer_Account { get; set; }
        public string Payee_Account { get; set; }
        public string PAYMENT_AMOUNT { get; set; }
        public string PAYMENT_BATCH_NUM { get; set; }
        public string PAYMENT_ID { get; set; }
        public string code { get; set; }
        public string Period { get; set; }
    }

    public class PerfectMoneyTransferResultConfig : EntityTypeConfiguration<PerfectMoneyTransferResult>
    {
        public PerfectMoneyTransferResultConfig()
        {
        }
    }
}
