﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class BankResponseStatus : BaseEntity
    {
        #region Bank
        public BankGatewayType? BankGatewayType { get; set; }
        /// <summary>
        /// Mellat: saleOrderId, Saman: ResNum
        /// </summary>
        public long BankOrderId { get; set; }
        /// <summary>
        /// Mellat: ResCode, Saman: State
        /// </summary>
        public int? BankResponseCode { get; set; }
        /// <summary>
        /// Mellat: RefId
        /// </summary>
        public string BankRefId { get; set; }
        /// <summary>
        /// Mellat: SaleReferenceId, Saman: RefNum
        /// </summary>
        public string BankSaleReferenceId { get; set; }
        /// <summary>
        /// Mellat: cardHolderInfo
        /// </summary>
        public string BankCardHolderInfo { get; set; }
        /// <summary>
        /// Mellat: cardHolderPan
        /// </summary>
        public string BankCardHolderPan { get; set; }
        #endregion

        public int OrderId { get; set; }
        public virtual Order Order { get; set; }
    }

    public class BankResponseStatusConfig : EntityTypeConfiguration<BankResponseStatus>
    {
        public BankResponseStatusConfig()
        {
        }
    }
}
