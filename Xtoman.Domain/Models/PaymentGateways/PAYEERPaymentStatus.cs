﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    public class PAYEERPaymentStatus : BaseEntity
    {
        public PAYEERPaymentStatus()
        {
            StatusDate = DateTime.UtcNow;
        }
        public string m_operation_id { get; set; } // Internal number of payment
        public string m_operation_ps { get; set; } // Method of payment	
        public string m_operation_date { get; set; } // Date and time of formation of operation
        public string m_operation_pay_date { get; set; } // Date and time of performance of payment
        public string m_shop { get; set; } // Merchant identifier
        public string m_orderid { get; set; } // Payment identifier
        public string m_amount { get; set; } // Amount of payment
        public string m_curr { get; set; } // Payment currency
        public string m_desc { get; set; } // Payment description
        public string m_status { get; set; } // Payment status
        public string m_sign { get; set; } // Signature
        public DateTime StatusDate { get; set; }
    }

    public class PAYEERPaymentStatusConfig : EntityTypeConfiguration<PAYEERPaymentStatus>
    {
        public PAYEERPaymentStatusConfig()
        {
        }
    }
}
