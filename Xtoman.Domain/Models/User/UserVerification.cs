﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class UserVerification : BaseEntity
    {
        public UserVerification()
        {
            InsertDate = DateTime.UtcNow;
        }
        public new long Id { get; set; }
        public int? InsertUserId { get; set; }
        public DateTime InsertDate { get; set; }
        public UserVerificationType Type { get; set; }
        public VerificationStatus Status { get; set; }
        public string Description { get; set; }

        public int? MediaId { get; set; }

        public int UserId { get; set; }
        public virtual AppUser User { get; set; }
        public virtual AppUser InsertUser { get; set; }
        public virtual Media Media { get; set; }
    }

    public class UserVerificationConfig : EntityTypeConfiguration<UserVerification>
    {
        public UserVerificationConfig()
        {
            //HasRequired(p => p.User).WithMany(p => p.UserVerifications).HasForeignKey(p => p.UserId);
        }
    }
}
