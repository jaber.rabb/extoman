﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    [Serializable]
    public class Permission : BaseEntity
    {
        public string Name { get; set; }
        public string ControllerName { get; set; }
        public string ControllerAction { get; set; }
        private ICollection<AppRole> _roles { get; set; }
        public virtual ICollection<AppRole> Roles
        {
            get { return _roles ?? (_roles = new List<AppRole>()); }
            protected set { _roles = value; }
        }

        public class PermissionConfig : EntityTypeConfiguration<Permission>
        {
            public PermissionConfig()
            {
            }
        }
    }
}
