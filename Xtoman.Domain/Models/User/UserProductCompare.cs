﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class UserProductCompare : BaseEntity
    {
        public UserProductCompare()
        {
            Date = DateTime.UtcNow;
        }
        public string AnonymousId { get; set; }
        public DateTime Date { get; set; }

        public int UserId { get; set; }
        public int ProductId { get; set; }
        public int CategoryId { get; set; }

        public virtual AppUser User { get; set; }
        public virtual Product Product { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }
    }

    public class UserCompareConfig : EntityTypeConfiguration<UserProductCompare>
    {
        public UserCompareConfig()
        {
            HasRequired(p => p.User).WithMany(p => p.ProductCompares).HasForeignKey(p => p.UserId);
            HasRequired(p => p.Product).WithMany(p => p.ProductCompares).HasForeignKey(p => p.ProductId);
            HasRequired(p => p.ProductCategory).WithMany(p => p.ProductCompares).HasForeignKey(p => p.CategoryId);
        }
    }
}
