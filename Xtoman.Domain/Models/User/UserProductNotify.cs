﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class UserProductNotify : BaseEntity
    {
        public UserProductNotify()
        {
            Date = DateTime.UtcNow;
            IsNotified = false;
        }
        public bool IsNotified { get; set; }
        public DateTime Date { get; set; }
        public DateTime? NotifyDate { get; set; }

        public int UserId { get; set; }
        public int ProductId { get; set; }
        public int ProductAttributeId { get; set; }

        // Navigation properties
        public virtual AppUser User { get; set; }
        public virtual Product Product { get; set; }
        public virtual ProductAttribute ProductAttribute { get; set; }
    }

    public class UserProductNotifyConfig : EntityTypeConfiguration<UserProductNotify>
    {
        public UserProductNotifyConfig()
        {
            HasRequired(p => p.User).WithMany(p => p.ProductNotifies).HasForeignKey(p => p.UserId);
            HasRequired(p => p.Product).WithMany(p => p.Notifies).HasForeignKey(p => p.ProductId);
            HasRequired(p => p.ProductAttribute).WithMany(p => p.Notifies).HasForeignKey(p => p.ProductAttributeId);
        }
    }
}
