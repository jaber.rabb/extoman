﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class UserBankCard : AuditEntity
    {
        public UserBankCard()
        {
            VerificationStatus = VerificationStatus.Pending;
        }
        public string CardNumber { get; set; }
        public string BankName { get; set; }
        public string ShebaNumber { get; set; }
        public string BankAccountNumber { get; set; }
        public VerificationStatus VerificationStatus { get; set; }
        public DateTime? VerifyDate { get; set; }
        public string VerifyDescription { get; set; }

        public int UserId { get; set; }
        public virtual AppUser User { get; set; }

        public long? FinnotechHistoryId { get; set; }
        public virtual FinnotechResponseCardInfo FinnotechHistory{ get; set; }
    }

    public class UserBankCardConfig : EntityTypeConfiguration<UserBankCard>
    {
        public UserBankCardConfig()
        {
            HasRequired(p => p.User).WithMany(p => p.UserBankCards).HasForeignKey(p => p.UserId);
            HasOptional(p => p.FinnotechHistory).WithOptionalPrincipal().Map(x => x.MapKey("FinnotechHistoryId"));
            Property(p => p.CardNumber).HasMaxLength(19);
        }
    }
}
