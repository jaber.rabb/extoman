﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    [Serializable]
    public class AppRoleForCache : IBaseCachingEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public List<Permission> Permissions { get; set; }
    }
    public class AppRole : IdentityRole<int, AppUserRole>, IBaseEntity
    {
        public AppRole() { }
        public AppRole(string roleName)
        {
            Name = roleName;
        }

        public string DisplayName { get; set; }
        private ICollection<Permission> _permissions { get; set; }
        public virtual ICollection<Permission> Permissions
        {
            get { return _permissions ?? (_permissions = new List<Permission>()); }
            protected set { _permissions = value; }
        }
    }

    public class AppRoleConfig : EntityTypeConfiguration<AppRole>
    {
        public AppRoleConfig()
        {
            Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            HasMany(p => p.Permissions).WithMany(p => p.Roles);
        }
    }
}
