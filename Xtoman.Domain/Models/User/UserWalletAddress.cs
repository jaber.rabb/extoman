﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class UserWalletAddress : BaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string PayentMemoDescriptionTagId { get; set; }

        public int CurrencyTypeId { get; set; }
        public virtual CurrencyType CurrencyType { get; set; }
    }

    public class UserWalletAddressConfig : EntityTypeConfiguration<UserWalletAddress>
    {
        public UserWalletAddressConfig()
        {
            HasRequired(p => p.CurrencyType).WithMany().HasForeignKey(p => p.CurrencyTypeId);
        }
    }
}
