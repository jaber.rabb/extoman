﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class UserBadge : BaseEntity
    {
        public UserBadge()
        {
            EarnDateUtc = DateTime.UtcNow;
        }
        public DateTime EarnDateUtc { get; set; }
        public int BadgeId { get; set; }
        public int UserId { get; set; }

        //Navigation properties
        public virtual Badge Badge { get; set; }
        public virtual AppUser User { get; set; }
    }

    public class UserBadgeConfig : EntityTypeConfiguration<UserBadge>
    {
        public UserBadgeConfig()
        {
            HasRequired(x => x.Badge).WithMany(x => x.UserBadges).HasForeignKey(x => x.BadgeId).WillCascadeOnDelete(true);
            HasRequired(x => x.User).WithMany(x => x.Badges).HasForeignKey(x => x.UserId);
        }
    }
}
