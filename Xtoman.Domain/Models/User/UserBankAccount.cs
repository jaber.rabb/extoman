﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class UserBankAccount : BaseEntity
    {
        public BankType BankType { get; set; }
        public string CardNumber { get; set; }
        public string ShebaNumber { get; set; }
        public int? MediaId { get; set; }
        public virtual Media Media { get; set; }
        public VerificationStatus Status { get; set; }
    }

    public class UserBankAccountConfig : EntityTypeConfiguration<UserBankAccount>
    {
        public UserBankAccountConfig()
        {
            HasOptional(c => c.Media).WithMany().HasForeignKey(p => p.MediaId);
        }
    }
}