﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class AppUser : IdentityUser<int, AppUserLogin, AppUserRole, AppUserClaim>, IBaseEntity
    {
        public AppUser()
        {
            RegDate = DateTime.UtcNow;
            IsSuspended = false;
            //IsBuyLimited = true;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool? Gender { get; set; } // false 0 = male | true 1 = female
        public int? ReferrerId { get; set; }
        public DateTime RegDate { get; set; }
        public bool IsSuspended { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public string IdentityVerificationDescription { get; set; }
        public VerificationStatus IdentityVerificationStatus { get; set; }
        public DateTime? IdentityVerificationVerifyDate { get; set; }
        public string Telephone { get; set; }
        public string NationalCode { get; set; }
        public string FatherName { get; set; }
        public VerificationStatus TelephoneConfirmationStatus { get; set; }
        public string TelephoneConfirmDescription { get; set; }
        public int? MediaId { get; set; }
        public int? VerificationMediaId { get; set; }
        //2Factor google authenticator
        public bool IsGoogleAuthenticatorEnabled { get; set; }
        public string GoogleAuthenticatorSecretKey { get; set; }

        public bool IsPhisher { get; set; } // Detected by phishing attacks

        //public long? TelegramChatId { get; set; }

        public bool IsBuyLimited { get; set; }
        //Virtual ignored property
        [IgnoreChangeLog]
        public string FullName => FirstName + " " + LastName;

        //public long? FinnotechHistoryId { get; set; }

        //Private properties
        private ICollection<Finnotech> _finnotechs;
        private ICollection<UserBadge> _badges;
        private ICollection<AppUser> _referrals;
        private ICollection<UserNotification> _userNotifications;
        private ICollection<Order> _orders;
        private ICollection<UserNotifyAlert> _notifyAlerts;
        private ICollection<UserIncome> _incomes;
        private ICollection<UserWithdraw> _withdraws;
        private ICollection<UserBankCard> _userBankCards;
        private ICollection<UserVerification> _userVerifications;
        private ICollection<UserLoginHistory> _loginHistories;

        private ICollection<UserProductNotify> _productNotifies;
        private ICollection<Product> _favoriteProducts;
        private ICollection<UserProductCompare> _productCompares;
        private ICollection<UserProductVisit> _productVisits;
        private ICollection<ProductComment> _productComments;
        private ICollection<ProductOrderReturnRequest> _returnRequests;

        //Navigation properties
        public virtual AppUser Referrer { get; set; }
        public virtual Media Media { get; set; }
        public virtual Media VerificationMedia { get; set; }
        public virtual ICollection<Finnotech> Finnotechs
        {
            get { return _finnotechs ?? (_finnotechs = new List<Finnotech>()); }
            protected set { _finnotechs = value; }
        }

        public virtual ICollection<UserBadge> Badges
        {
            get { return _badges ?? (_badges = new List<UserBadge>()); }
            protected set { _badges = value; }
        }
        public virtual ICollection<AppUser> Referrals
        {
            get { return _referrals ?? (_referrals = new List<AppUser>()); }
            protected set { _referrals = value; }
        }
        public virtual ICollection<UserNotification> UserNotifications
        {
            get { return _userNotifications ?? (_userNotifications = new List<UserNotification>()); }
            protected set { _userNotifications = value; }
        }

        public virtual ICollection<Order> Orders
        {
            get { return _orders ?? (_orders = new List<Order>()); }
            protected set { _orders = value; }
        }

        public virtual ICollection<UserIncome> Incomes
        {
            get { return _incomes ?? (_incomes = new List<UserIncome>()); }
            protected set { _incomes = value; }
        }

        public virtual ICollection<UserWithdraw> Withdraws
        {
            get { return _withdraws ?? (_withdraws = new List<UserWithdraw>()); }
            protected set { _withdraws = value; }
        }

        public virtual ICollection<UserNotifyAlert> NotifyAlerts
        {
            get { return _notifyAlerts ?? (_notifyAlerts = new List<UserNotifyAlert>()); }
            protected set { _notifyAlerts = value; }
        }

        public virtual ICollection<UserBankCard> UserBankCards
        {
            get { return _userBankCards ?? (_userBankCards = new List<UserBankCard>()); }
            protected set { _userBankCards = value; }
        }

        public virtual ICollection<UserVerification> UserVerifications
        {
            get { return _userVerifications ?? (_userVerifications = new List<UserVerification>()); }
            protected set { _userVerifications = value; }
        }

        public virtual ICollection<UserLoginHistory> LoginHistories
        {
            get { return _loginHistories ?? (_loginHistories = new List<UserLoginHistory>()); }
            protected set { _loginHistories = value; }
        }

        public virtual ICollection<UserProductCompare> ProductCompares
        {
            get { return _productCompares ?? (_productCompares = new List<UserProductCompare>()); }
            protected set { _productCompares = value; }
        }

        public virtual ICollection<Product> FavoriteProducts
        {
            get { return _favoriteProducts ?? (_favoriteProducts = new List<Product>()); }
            protected set { _favoriteProducts = value; }
        }

        public virtual ICollection<UserProductNotify> ProductNotifies
        {
            get { return _productNotifies ?? (_productNotifies = new List<UserProductNotify>()); }
            protected set { _productNotifies = value; }
        }

        public virtual ICollection<UserProductVisit> ProductVisits
        {
            get { return _productVisits ?? (_productVisits = new List<UserProductVisit>()); }
            protected set { _productVisits = value; }
        }

        public virtual ICollection<ProductComment> ProductComments
        {
            get { return _productComments ?? (_productComments = new List<ProductComment>()); }
            protected set { _productComments = value; }
        }

        public virtual ICollection<ProductOrderReturnRequest> ReturnRequests
        {
            get { return _returnRequests ?? (_returnRequests = new List<ProductOrderReturnRequest>()); }
            protected set { _returnRequests = value; }
        }
    }

    public class AppUserConfig : EntityTypeConfiguration<AppUser>
    {
        public AppUserConfig()
        {
            HasMany(p => p.FavoriteProducts).WithMany(p => p.UserFavorites);
            HasMany(p => p.Finnotechs).WithOptional(p => p.ForUser).HasForeignKey(p => p.ForUserId);
            HasOptional(c => c.VerificationMedia).WithMany().HasForeignKey(p => p.VerificationMediaId);
            HasOptional(c => c.Referrer).WithMany(p => p.Referrals).HasForeignKey(c => c.ReferrerId);
            HasOptional(c => c.Media).WithMany().HasForeignKey(p => p.MediaId);
            Ignore(p => p.FullName);
        }
    }
}
