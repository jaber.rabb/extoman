﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class UserProductVisit : BaseEntity
    {
        public UserProductVisit()
        {
            Date = DateTime.UtcNow;
        }
        public DateTime Date { get; set; }
        public int UserId { get; set; }
        public int ProductId { get; set; }

        // Navigation properties
        public virtual AppUser User { get; set; }
        public virtual Product Product { get; set; }
    }

    public class UserProductVisitConfig : EntityTypeConfiguration<UserProductVisit>
    {
        public UserProductVisitConfig()
        {
            HasRequired(x => x.User).WithMany(x => x.ProductVisits).HasForeignKey(x => x.UserId);
            HasRequired(x => x.Product).WithMany(x => x.Visits).HasForeignKey(x => x.ProductId);
        }
    }
}
