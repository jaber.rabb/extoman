﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class UserNotifyAlert : BaseEntity
    {
        public UserNotifyAlert()
        {
            InsertDate = DateTime.UtcNow;
        }
        public NotifyAlertType Type { get; set; }
        public decimal Value { get; set; }
        public bool SendViaEmail { get; set; }
        public bool SendViaSMS { get; set; }
        public bool SendViaTelegram { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime? AlertedDate { get; set; }

        public int? ExchangeTypeId { get; set; }
        public int? BaseCurrencyId { get; set; }
        public int UserId { get; set; }

        public virtual AppUser User { get; set; }
        public virtual ExchangeType ExchangeType { get; set; }
        public virtual CurrencyType BaseCurrency { get; set; }
    }

    public class UserNotifyAlertConfig : EntityTypeConfiguration<UserNotifyAlert>
    {
        public UserNotifyAlertConfig()
        {
            HasRequired(p => p.User).WithMany(p => p.NotifyAlerts).HasForeignKey(p => p.UserId);
            HasOptional(p => p.ExchangeType).WithMany(p => p.NotifyAlerts).HasForeignKey(p => p.ExchangeTypeId);
            HasOptional(p => p.BaseCurrency).WithMany(p => p.NotifyAlerts).HasForeignKey(p => p.BaseCurrencyId);
        }
    }
}
