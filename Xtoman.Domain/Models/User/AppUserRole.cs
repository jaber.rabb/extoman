﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class AppUserRole : IdentityUserRole<int>
    {
        public virtual AppRole Role { get; set; }
        public virtual AppUser User { get; set; }
    }

    public class AppUserRoleConfig : EntityTypeConfiguration<AppUserRole>
    {
        public AppUserRoleConfig()
        {
            HasRequired(x => x.Role).WithMany(x => x.Users).HasForeignKey(x => x.RoleId);
            HasRequired(x => x.User).WithMany(x => x.Roles).HasForeignKey(x => x.UserId);
        }
    }
}
