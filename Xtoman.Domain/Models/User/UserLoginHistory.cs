﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class UserLoginHistory : BaseEntity
    {
        public UserLoginHistory()
        {
            Date = DateTime.UtcNow;
        }

        public string Browser { get; set; }
        public string IPAddress { get; set; }
        public byte OwinSignInStatus { get; set; }
        public DateTime Date { get; set; }
        public int UserId { get; set; }
        public virtual AppUser User { get; set; }
    }

    public class UserLoginHistoryConfig : EntityTypeConfiguration<UserLoginHistory>
    {
        public UserLoginHistoryConfig()
        {
            HasRequired(p => p.User).WithMany(p => p.LoginHistories).HasForeignKey(p => p.UserId);
        }
    }
}
