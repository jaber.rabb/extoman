﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class UserNotification : BaseEntity
    {
        public UserNotification()
        {
            Date = DateTime.UtcNow;
        }
        public string Title { get; set; }
        public string Text { get; set; }
        public bool IsRead { get; set; }
        public DateTime Date { get; set; }
        public NotificationType NotificationType { get; set; }

        public int UserId { get; set; }
        public virtual AppUser User { get; set; }
    }

    public class UserNotificationConfig : EntityTypeConfiguration<UserNotification>
    {
        public UserNotificationConfig()
        {
            HasRequired(x => x.User).WithMany(x => x.UserNotifications).HasForeignKey(x => x.UserId);
        }
    }
}
