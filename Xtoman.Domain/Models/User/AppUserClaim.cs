﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class AppUserClaim : IdentityUserClaim<int>
    {
    }

    public class AppUserClaimConfig : EntityTypeConfiguration<AppUserClaim>
    {
        public AppUserClaimConfig()
        {
            Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
