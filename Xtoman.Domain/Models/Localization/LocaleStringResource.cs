﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    /// <summary>
    /// Represents a locale string resource
    /// </summary>
    public partial class LocaleStringResource : BaseEntity
    {
        /// <summary>
        /// Gets or sets the resource name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the resource value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the language identifier
        /// </summary>
        public int LanguageId { get; set; }

        /// <summary>
        /// Gets or sets the language
        /// </summary>
        public virtual Language Language { get; set; }
    }

    public class LocaleStringResourceConfig : EntityTypeConfiguration<LocaleStringResource>
    {
        public LocaleStringResourceConfig()
        {
            Property(l => l.Name).IsRequired().HasMaxLength(200);
            Property(l => l.Value).IsRequired();
            HasRequired(p => p.Language).WithMany(p => p.LocaleStringResources).HasForeignKey(p => p.LanguageId);
        }
    }
}
