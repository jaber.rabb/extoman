﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    public class Language : BaseEntity
    {
        public Language()
        {
            Countries = new HashSet<Country>();
        }
        public string Name { get; set; }
        public string LanguageCode { get; set; }

        public virtual ICollection<Country> Countries { get; set; }
        public virtual ICollection<LocaleStringResource> LocaleStringResources { get; set; }
    }

    public class LanguageConfig : EntityTypeConfiguration<Language>
    {
        public LanguageConfig()
        {
            Property(l => l.Name).IsRequired().HasMaxLength(50);
            Property(l => l.LanguageCode).IsRequired().HasMaxLength(3);
        }
    }
}
