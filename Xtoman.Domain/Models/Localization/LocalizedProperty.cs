﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    [Serializable]
    public class LocalizedPropertyForCaching : IBaseCachingEntity
    {
        public int Id { get; set; }
        public int EntityId { get; set; }
        public int LanguageId { get; set; }
        public string LocaleKeyGroup { get; set; }
        public string LocaleKey { get; set; }
        public string LocaleValue { get; set; }
    }

    public class LocalizedProperty : BaseEntity
    {
        public int EntityId { get; set; } //105
        public string LocaleKeyGroup { get; set; } //News
        public string LocaleKey { get; set; } //Title
        public string LocaleValue { get; set; } //asdusadh askd alskdosh
        public int LanguageId { get; set; } //en: 2

        public virtual Language Language { get; set; }
    }

    public class LocalizedPropertyConfig : EntityTypeConfiguration<LocalizedProperty>
    {
        public LocalizedPropertyConfig()
        {
            HasRequired(x => x.Language).WithMany().HasForeignKey(x => x.LanguageId);
        }
    }
}
