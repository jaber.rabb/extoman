﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    [AttributeUsage(AttributeTargets.Property)]
    public class LocalizedPropertyAttribute : Attribute
    {
    }
}
