﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Currency : BaseEntity
    {
        public string Name { get; set; }
        public string Symbol { get; set; }
        public string CustomFormating { get; set; }
        public int Order { get; set; }
        public virtual Country Country { get; set; }
    }

    public class CurrencyConfig : EntityTypeConfiguration<Currency>
    {
        public CurrencyConfig()
        {
            HasRequired(x => x.Country).WithOptional(x => x.Currency);
            Property(c => c.Name).IsRequired().HasMaxLength(50);
            Property(c => c.CustomFormating).IsOptional().HasMaxLength(50);
        }
    }
}
