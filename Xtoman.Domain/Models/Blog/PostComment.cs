﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class PostComment : Comment
    {
        public int PostId { get; set; }
        public virtual Post Post { get; set; }
    }

    public class PostCommentConfig : EntityTypeConfiguration<PostComment>
    {
        public PostCommentConfig()
        {
            HasRequired(x => x.Post).WithMany(x => x.Comments).HasForeignKey(x => x.PostId).WillCascadeOnDelete(true);
        }
    }
}
