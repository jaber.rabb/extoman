﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Post : Content
    {
        public Post()
        {
            AllowComments = true;
        }

        [LocalizedProperty]
        public string TagNames { get; set; }
        public PostType PostType { get; set; }
        public bool IsPin { get; set; }
        public int? MediaId { get; set; }
        public int ApprovedCommentsCount { get; set; }

        //Private properties
        private ICollection<PostComment> _comments;
        private ICollection<PostTag> _tags;
        private ICollection<PostCategory> _categories;
        private ICollection<Media> _medias;

        //Navigation properties
        public virtual Media Media { get; set; }
        public virtual ICollection<Media> Medias
        {
            get { return _medias ?? (_medias = new List<Media>()); }
            set { _medias = value; }
        }
        public virtual ICollection<PostComment> Comments
        {
            get { return _comments ?? (_comments = new List<PostComment>()); }
            set { _comments = value; }
        }
        public virtual ICollection<PostTag> Tags
        {
            get { return _tags ?? (_tags = new List<PostTag>()); }
            set { _tags = value; }
        }
        public virtual ICollection<PostCategory> Categories
        {
            get { return _categories ?? (_categories = new List<PostCategory>()); }
            set { _categories = value; }
        }
    }

    public class PostConfig : EntityTypeConfiguration<Post>
    {
        public PostConfig()
        {
            HasOptional(p => p.Media).WithMany().HasForeignKey(p => p.MediaId);
            HasMany(x => x.Comments).WithRequired(x => x.Post).WillCascadeOnDelete(true);
            HasMany(x => x.Tags).WithMany(x => x.Posts);
            HasMany(x => x.Categories).WithMany(x => x.Posts);
            HasMany(x => x.Medias).WithMany(x => x.Posts);
        }
    }
}