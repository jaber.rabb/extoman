﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public partial class Tag : BaseEntity, ILocalizedEntity
    {
        [LocalizedProperty]
        public string Name { get; set; }

        [LocalizedProperty]
        public string Url { get; set; }
    }
    public class TagConfig : EntityTypeConfiguration<Tag>
    {
        public TagConfig()
        {
        }
    }
}
