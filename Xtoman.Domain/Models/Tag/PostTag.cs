﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class PostTag : Tag, ILocalizedEntity, IUrlHistoryEntity
    {
        public bool IsPin { get; set; }

        //Private properties
        private ICollection<Post> _posts;

        //Navigation properties
        public virtual ICollection<Post> Posts
        {
            get { return _posts ?? (_posts = new List<Post>()); }
            set { _posts = value; }
        }
    }

    public class PostTagConfig : EntityTypeConfiguration<PostTag>
    {
        public PostTagConfig()
        {
        }
    }
}
