﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class TelegramBotLog : BaseEntity
    {
        public long ChatId { get; set; }
        public string BotName { get; set; }
        public string Operation { get; set; }
        public string ChannelUser { get; set; }
        public long ChannelChatId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? UserId { get; set; }
        public virtual AppUser User { get; set; }
    }

    public class TelegramBotLogConfig : EntityTypeConfiguration<TelegramBotLog>
    {
        public TelegramBotLogConfig()
        {
            HasOptional(p => p.User).WithMany().HasForeignKey(p => p.UserId);
        }
    }

}
