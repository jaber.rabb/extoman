﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class OrderNote : AuditEntity
    {
        public string Note { get; set; }
        public bool DisplayToCustomer { get; set; }
        public long OrderId { get; set; }

        //Navigation properties
        public virtual Order Order { get; set; }
    }

    public class OrderNoteConfig : EntityTypeConfiguration<OrderNote>
    {
        public OrderNoteConfig()
        {
            HasRequired(p => p.Order).WithMany(p => p.OrderNotes).HasForeignKey(p => p.OrderId);
        }
    }
}
