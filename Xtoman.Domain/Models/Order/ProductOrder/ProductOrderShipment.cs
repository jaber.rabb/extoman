﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductOrderShipment : AuditEntity
    {
        public new long Id { get; set; }
        public decimal? TotalWeight { get; set; }
        public DateTime? ShippedDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string TrackingNumber { get; set; }
        public long OrderId { get; set; }
        public int CarrierId { get; set; }

        //Private properties
        private ICollection<ProductOrderShipmentItem> _shipmentItems;

        //Navigation property
        public virtual ProductOrder ProductOrder { get; set; }
        public virtual Carrier Carrier { get; set; }
        public virtual ICollection<ProductOrderShipmentItem> ShipmentItems
        {
            get { return _shipmentItems ?? (_shipmentItems = new List<ProductOrderShipmentItem>()); }
            protected set { _shipmentItems = value; }
        }
    }

    public class ProductOrderShipmentConfig : EntityTypeConfiguration<ProductOrderShipment>
    {
        public ProductOrderShipmentConfig()
        {
            HasRequired(p => p.ProductOrder).WithMany(p => p.OrderShipments).HasForeignKey(p => p.OrderId);
            HasRequired(p => p.Carrier).WithMany(p => p.OrderShipments).HasForeignKey(p => p.CarrierId);
        }
    }
}
