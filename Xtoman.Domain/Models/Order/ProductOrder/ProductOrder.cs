﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductOrder : Order
    {
        public ProductOrder()
        {
            OrderStatus = OrderStatus.Pending;
            ShippingStatus = ShippingStatus.NotShipped;
            PaymentStatus = PaymentStatus.NotPaid;
            PaymentType = PaymentType.Online;
            Guid = Guid.NewGuid();
        }

        #region Status
        public ShippingStatus ShippingStatus { get; set; }
        #endregion

        #region Price
        public decimal TotalDiscount { get; set; }
        public decimal TotalTax { get; set; }
        public decimal TotalShippingPrice { get; set; }
        public decimal TotalGiftingPrice { get; set; }
        public decimal TotalPackingPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalPayPrice { get; set; }
        #endregion

        #region Gift
        public bool IsGift { get; set; }
        public string GiftMessage { get; set; }
        #endregion

        //Private properties
        private ICollection<ProductOrderItem> _orderItems;
        private ICollection<ProductOrderShipment> _orderShipments;

        //Navigation properties 
        //public virtual OrderReturnRequest OrderReturnRequest { get; set; }
        public virtual ICollection<ProductOrderItem> OrderItems
        {
            get { return _orderItems ?? (_orderItems = new List<ProductOrderItem>()); }
            set { _orderItems = value; }
        }

        public virtual ICollection<ProductOrderShipment> OrderShipments
        {
            get { return _orderShipments ?? (_orderShipments = new List<ProductOrderShipment>()); }
            set { _orderShipments = value; }
        }
    }

    public class ProductOrderConfig : EntityTypeConfiguration<ProductOrder>
    {
        public ProductOrderConfig()
        {
            ToTable("ProductOrders");
        }
    }
}
