﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductOrderItem : BaseEntity
    {
        public new long Id { get; set; }
        public int Quanity { get; set; }

        public long OrderId { get; set; }
        public int ProductId { get; set; }
        public int ProductAttributeId { get; set; }
        public long? ShipmentItemId { get; set; }

        public decimal Price { get; set; }
        public decimal SellPrice { get; set; }

        //Private properties
        private ICollection<ProductOrderReturnRequest> _returnRequests;

        //Navigation properties
        public virtual ProductOrder ProductOrder { get; set; }
        public virtual Product Product { get; set; }
        public virtual ProductAttribute ProductAttribute { get; set; }
        public virtual ProductOrderShipmentItem ShipmentItem { get; set; }
        public virtual ICollection<ProductOrderReturnRequest> ReturnRequests
        {
            get { return _returnRequests ?? (_returnRequests = new List<ProductOrderReturnRequest>()); }
            protected set { _returnRequests = value; }
        }
    }

    public class ProductOrderItemConfig : EntityTypeConfiguration<ProductOrderItem>
    {
        public ProductOrderItemConfig()
        {
            HasRequired(p => p.Product).WithMany(p => p.OrderItems).HasForeignKey(p => p.ProductId);
            HasRequired(p => p.ProductOrder).WithMany(p => p.OrderItems).HasForeignKey(p => p.OrderId);
            HasRequired(p => p.ProductAttribute).WithMany(p => p.OrderItems).HasForeignKey(p => p.ProductAttributeId);
            HasOptional(p => p.ShipmentItem).WithRequired(p => p.OrderItem);
        }
    }
}
