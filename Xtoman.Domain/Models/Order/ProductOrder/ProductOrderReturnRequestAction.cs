﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductOrderReturnRequestAction : BaseEntity, ILocalizedEntity
    {
        [LocalizedProperty]
        public string Name { get; set; }
        public int DisplayOrder { get; set; }

        //Private properties
        private ICollection<ProductOrderReturnRequest> _returnRequests;

        //Navigation properties
        public ICollection<ProductOrderReturnRequest> ReturnRequests
        {
            get { return _returnRequests ?? (_returnRequests = new List<ProductOrderReturnRequest>()); }
            set { _returnRequests = value; }
        }
    }

    public class ProductReturnRequestActionConfig : EntityTypeConfiguration<ProductOrderReturnRequestAction>
    {
        public ProductReturnRequestActionConfig()
        {
        }
    }
}