﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductOrderReturnRequest : AuditEntity
    {
        public int Quantity { get; set; }
        public string ReasonForReturn { get; set; }
        public ReturnRequestStatus ReturnRequestStatus { get; set; }

        public string StaffNotes { get; set; }
        public string UserComments { get; set; }
 
        public int UserId { get; set; }
        public long OrderItemId { get; set; }
        public int RequestedActionId { get; set; }

        //Navigation properties
        public virtual AppUser User { get; set; }
        public virtual ProductOrderItem OrderItem { get; set; }
        public virtual ProductOrderReturnRequestAction RequestedAction { get; set; }
    }

    public class ReturnRequestConfig : EntityTypeConfiguration<ProductOrderReturnRequest>
    {
        public ReturnRequestConfig()
        {
            HasRequired(p => p.User).WithMany(p => p.ReturnRequests).HasForeignKey(p => p.UserId);
            HasRequired(p => p.OrderItem).WithMany(p => p.ReturnRequests).HasForeignKey(p => p.OrderItemId);
            HasRequired(p => p.RequestedAction).WithMany(p => p.ReturnRequests).HasForeignKey(p => p.RequestedActionId);
        }
    }
}
