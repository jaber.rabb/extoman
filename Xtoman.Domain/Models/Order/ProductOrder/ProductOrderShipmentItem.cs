﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductOrderShipmentItem : BaseEntity
    {
        public new long Id { get; set; }
        public int Quantity { get; set; }
        public long OrderItemId { get; set; }
        public long OrderShipmentId { get; set; }
        public virtual ProductOrderShipment OrderShipment { get; set; }
        public virtual ProductOrderItem OrderItem { get; set; }
    }

    public class ProductOrderShipmentItemConfig : EntityTypeConfiguration<ProductOrderShipmentItem>
    {
        public ProductOrderShipmentItemConfig()
        {
            HasRequired(p => p.OrderItem).WithOptional(p => p.ShipmentItem);
            HasRequired(p => p.OrderShipment).WithMany(p => p.ShipmentItems).HasForeignKey(p => p.OrderShipmentId);
        }
    }
}
