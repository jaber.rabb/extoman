﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class OrderRefund : BaseEntity
    {
        public OrderRefund()
        {
            RefundDate = DateTime.UtcNow;
        }
        public DateTime RefundDate { get; set; }
        public DateTime? RefundMoneyDate { get; set; }
        public string Description { get; set; }

        public decimal Amount { get; set; }

        public virtual Order Order { get; set; }
    }

    public class OrderRefundConfig : EntityTypeConfiguration<OrderRefund>
    {
        public OrderRefundConfig()
        {
            HasRequired(x => x.Order).WithOptional(x => x.Refund);
        }
    }
}
