﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ExchangeOrderWalletAddress : BaseEntity
    {
        public string Address { get; set; }
        public string MemoTagPaymentId { get; set; }
        public ECurrencyType Type { get; set; }
        public long ExchangeOrderId { get; set; }
        public virtual ExchangeOrder ExchangeOrder { get; set; }
    }

    public class ExchangeOrderWalletAddressConfig : EntityTypeConfiguration<ExchangeOrderWalletAddress>
    {
        public ExchangeOrderWalletAddressConfig()
        {

        }
    }
}
