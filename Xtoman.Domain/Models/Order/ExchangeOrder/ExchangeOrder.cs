﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ExchangeOrder : Order
    {
        #region Pay
        public decimal PaidAmount { get; set; }
        public string PayAddress { get; set; }
        public string PayMemoTagPaymentId { get; set; }
        public string PayTransactionId { get; set; }
        public string PayPaymentGatewayId { get; set; }
        public WalletApiType PayWalletApiType { get; set; }
        public DateTime? PayTimeEnd { get; set; }
        //public TimeSpan PayTimeLeft => PayTimeEnd.HasValue ? PayTimeEnd.Value - DateTime.UtcNow : TimeSpan.FromMinutes(30);
        //public string PayQRUrl { get; set; }
        //public string MemoQRUrl { get; set; }
        //public int PayConfirmsNeed { get; set; }
        //public string PayStatusUrl { get; set; }
        #endregion

        #region Receive
        public decimal ReceiveAmount { get; set; }
        public string ReceiveAddress { get; set; }
        public string ReceiveMemoTagPaymentId { get; set; }
        public string ReceiveTransactionCode { get; set; }
        public DateTime? ReceiveDate { get; set; }
        public string ReceivePayerAccountNo { get; set; }
        public decimal EstimatedReceiveAmount { get; set; }
        #endregion

        #region Other
        public decimal? CurrencyPriceInDollar { get; set; }
        public int DollarPriceInToman { get; set; }
        public int EuroPriceInToman { get; set; }
        public decimal BTCPriceInUSD { get; set; }
        public decimal BTCPriceInEUR { get; set; }
        public decimal BTCValueOfOrder { get; set; }
        public decimal ExchangeExtraFeePercentInTime { get; set; }
        #endregion

        public int ExchangeId { get; set; }
        public virtual ExchangeType Exchange { get; set; }


        //public long? WithdrawId { get; set; }

        private ICollection<Withdraw> _orderWithdraws;
        public virtual ICollection<Withdraw> OrderWithdraws
        {
            get { return _orderWithdraws ?? (_orderWithdraws = new List<Withdraw>()); }
            protected set { _orderWithdraws = value; }
        }

        private ICollection<Trade> _orderTrades;
        public virtual ICollection<Trade> OrderTrades
        {
            get { return _orderTrades ?? (_orderTrades = new List<Trade>()); }
            protected set { _orderTrades = value; }
        }

    }

    public class ExchangeOrderConfig : EntityTypeConfiguration<ExchangeOrder>
    {
        public ExchangeOrderConfig()
        {
            HasRequired(p => p.Exchange).WithMany(p => p.UserExchanges).HasForeignKey(p => p.ExchangeId);
            Property(p => p.ReceiveAmount).HasPrecision(21, 8);
            Property(p => p.PaidAmount).HasPrecision(21, 8);
            Property(p => p.EstimatedReceiveAmount).HasPrecision(21, 8);
            ToTable("ExchangeOrders");
        }
    }
}
