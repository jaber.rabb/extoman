﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Order : AuditEntity
    {
        public Order()
        {
            Guid = Guid.NewGuid();
            PaymentStatus = PaymentStatus.NotPaid;
        }
        public new long Id { get; set; }
        public Guid Guid { get; set; }
        public PaymentType PaymentType { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public DateTime? CancelDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public string Description { get; set; }
        public bool IsFromOmniTether { get; set; }

        #region Pricing
        public decimal PayAmount { get; set; }
        public decimal PriceAmount { get; set; }
        public decimal GatewayFee { get; set; }
        public ECurrencyType PaymentECurrencyType { get; set; }
        public string Errors { get; set; }
        #endregion
        public int BankResponseId { get; set; }
        public int UserId { get; set; }
        public virtual AppUser User { get; set; }
        public virtual UserIncome UserIncome { get; set; }
        public virtual OrderRefund Refund { get; set; }
        public virtual OrderReview Review { get; set; }
        public virtual BankResponseStatus BankResponse { get; set; }

        private ICollection<OrderNote> _orderNotes;

        public virtual ICollection<OrderNote> OrderNotes
        {
            get { return _orderNotes ?? (_orderNotes = new List<OrderNote>()); }
            set { _orderNotes = value; }
        }

        public string GetOrderName()
        {
            if (this is ExchangeOrder)
                return "مبادله ارز الکترونیکی";
            throw new ArgumentException("نوع سفارش نامشخص است!");
        }
            
        public ExchangeOrder GetExchangeOrderType()
        {
            if (this is ExchangeOrder)
                return this as ExchangeOrder;
            throw new ArgumentException("Order type is not ExchangeOrder");
        }
    }

    public class OrderConfig : EntityTypeConfiguration<Order>
    {
        public OrderConfig()
        {
            HasRequired(p => p.User).WithMany(p => p.Orders).HasForeignKey(p => p.UserId);
            HasOptional(p => p.BankResponse).WithRequired(p => p.Order).WillCascadeOnDelete(false);
            HasOptional(p => p.UserIncome).WithRequired(p => p.Order).WillCascadeOnDelete(false);
            Property(p => p.PayAmount).HasPrecision(21, 8);
            Property(p => p.GatewayFee).HasPrecision(21, 8);
            Property(p => p.PriceAmount).HasPrecision(21, 8);
            //Property(p => p.OrderTypeName);
        }
    }
}
