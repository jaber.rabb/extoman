﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class OrderReview : BaseEntity
    {
        public new long Id { get; set; }
        [Display(Name = "متن نظر")]
        public string Text { get; set; }
        [Display(Name = "امتیاز")]
        public byte VoteRate { get; set; }
        [Display(Name = "وضعیت نمایش")]
        public Approvement Visibility { get; set; }

        public virtual Order Order { get; set; }
    }

    public class OrderReviewConfig : EntityTypeConfiguration<OrderReview>
    {
        public OrderReviewConfig()
        {
            HasRequired(x => x.Order).WithOptional(x => x.Review);
        }
    }
}
