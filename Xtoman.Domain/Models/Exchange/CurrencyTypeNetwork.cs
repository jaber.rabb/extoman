﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class CurrencyTypeNetwork : BaseEntity
    {
        public CurrencyTypeNetwork()
        {
            DepositEnable = true;
            WithdrawEnable = true;
        }
        public bool IsDefault { get; set; }

        public bool DepositEnable { get; set; }
        public bool WithdrawEnable { get; set; }
        public decimal WithdrawFee { get; set; }
        public decimal WithdrawMinimum { get; set; }
        public decimal WithdrawMaximum { get; set; }

        public int CurrencyTypeId { get; set; }
        public virtual CurrencyType CurrencyType { get; set; }

        public int BlockchainNetworkId { get; set; }
        public virtual BlockchainNetwork BlockchainNetwork { get; set; }
    }

    public class CurrencyTypeNetworkConfig : EntityTypeConfiguration<CurrencyTypeNetwork>
    {
        public CurrencyTypeNetworkConfig()
        {
            Property(p => p.WithdrawFee).HasPrecision(21, 8);
            Property(p => p.WithdrawMinimum).HasPrecision(21, 8);
            Property(p => p.WithdrawMaximum).HasPrecision(21, 8);
            HasRequired(p => p.CurrencyType).WithMany(p => p.Networks).HasForeignKey(p => p.CurrencyTypeId);
            HasRequired(p => p.BlockchainNetwork).WithMany(p => p.CurrencyTypes).HasForeignKey(p => p.BlockchainNetworkId);
        }
    }
}
