﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ExchangeType : BaseEntity
    {
        public int FromCurrencyId { get; set; }
        public int ToCurrencyId { get; set; }
        public bool Disabled { get; set; }
        public bool Invisible { get; set; }
        public decimal ExtraFeePercent { get; set; }
        public ExchangeFiatCoinType ExchangeFiatCoinType { get; set; }
        public string Description { get; set; }
        public virtual CurrencyType FromCurrency { get; set; }
        public virtual CurrencyType ToCurrency { get; set; }

        //private ICollection<ExchangeTypeRate> _exchangeTypeRates;
        private ICollection<ExchangeOrder> _userExchanges;
        private ICollection<UserNotifyAlert> _notifyAlerts;

        //public virtual ICollection<ExchangeTypeRate> ExchangeTypeRates
        //{
        //    get { return _exchangeTypeRates ?? (_exchangeTypeRates = new List<ExchangeTypeRate>()); }
        //    protected set { _exchangeTypeRates = value; }
        //}

        public virtual ICollection<ExchangeOrder> UserExchanges
        {
            get { return _userExchanges ?? (_userExchanges = new List<ExchangeOrder>()); }
            protected set { _userExchanges = value; }
        }

        public virtual ICollection<UserNotifyAlert> NotifyAlerts
        {
            get { return _notifyAlerts ?? (_notifyAlerts = new List<UserNotifyAlert>()); }
            protected set { _notifyAlerts = value; }
        }
    }

    public class ExchangeTypeConfig : EntityTypeConfiguration<ExchangeType>
    {
        public ExchangeTypeConfig()
        {
            HasRequired(p => p.FromCurrency).WithMany(p => p.ExchangeTypesFrom).HasForeignKey(p => p.FromCurrencyId);
            HasRequired(p => p.ToCurrency).WithMany(p => p.ExchangeTypesTo).HasForeignKey(p => p.ToCurrencyId);
        }
    }
}
