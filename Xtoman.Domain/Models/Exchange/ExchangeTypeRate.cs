﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    [IgnoreChangeLog]
    public class ExchangeTypeRate : BaseEntity
    {
        public ExchangeTypeRate()
        {
            InsertDate = DateTime.UtcNow;
        }
        public new long Id { get; set; }
        public decimal RateIncludeExtraFee { get; set; }
        public decimal Rate { get; set; }
        public decimal ExtraFeePercent { get; set; }
        public int EuroPriceInToman { get; set; }
        public int DollarPriceInToman { get; set; }
        public bool IsManual { get; set; }
        public DateTime InsertDate { get; set; }
        public int ExchangeTypeId { get; set; }
        public virtual ExchangeType ExchangeType { get; set; }
    }

    public class ExchangeTypeRateConfig : EntityTypeConfiguration<ExchangeTypeRate>
    {
        public ExchangeTypeRateConfig()
        {
            HasRequired(p => p.ExchangeType).WithMany(p => p.ExchangeTypeRates).HasForeignKey(p => p.ExchangeTypeId);
            Property(p => p.RateIncludeExtraFee).HasPrecision(24, 15);
            Property(p => p.Rate).HasPrecision(24, 15);
        }
    }
}
