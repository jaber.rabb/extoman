﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class BlockchainNetwork : BaseEntity
    {
        public string Name { get; set; }
        public string Network { get; set; }
        public string AddressRegex { get; set; }
        public string MemoRegex { get; set; }
        public string MemoName { get; set; }
        public string SpecialTips { get; set; }
        public bool IsAvailable { get; set; }
        public int MinConfirm { get; set; }

        private ICollection<CurrencyTypeNetwork> _currencyTypes;
        public virtual ICollection<CurrencyTypeNetwork> CurrencyTypes
        {
            get { return _currencyTypes ?? (_currencyTypes = new List<CurrencyTypeNetwork>()); }
            protected set { _currencyTypes = value; }
        }
    }

    public class BlockchainNetworkConfig : EntityTypeConfiguration<BlockchainNetwork>
    {
        public BlockchainNetworkConfig()
        {
        }
    }
}
