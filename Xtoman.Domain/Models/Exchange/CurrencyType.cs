﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    [Serializable]
    public class CurrencyTypeForCaching : IBaseCachingEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AlternativeName { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string UnitSign { get; set; }
        public bool UnitSignIsAfter { get; set; }
        public decimal? Available { get; set; }
        public ECurrencyType Type { get; set; }
        public decimal MinimumReceiveAmount { get; set; }
        public decimal Step { get; set; }
        public bool IsFiat { get; set; }
        public bool IsERC20 { get; set; }
        public byte MaxPrecision { get; set; }
        public string AddressRegEx { get; set; }
        public decimal? TransactionFee { get; set; }

        public int ConfirmsNeed { get; set; }
        public string ChangellyName { get; set; }
        public string CoinMarketCapId { get; set; }

        public bool HasMemo { get; set; }
        public string MemoName { get; set; }
        public string MemoRegEx { get; set; }
        public string MemoDescription { get; set; }

        public bool Disabled { get; set; }
        public bool Invisible { get; set; }
        public int? MediaId { get; set; }
    }

    public class CurrencyType : BaseEntity
    {
        public CurrencyType()
        {
            Step = 1;
        }
        public string Name { get; set; }
        public string AlternativeName { get; set; }
        public string Description { get; set; }
        public string UnitSign { get; set; }
        public bool UnitSignIsAfter { get; set; }
        public decimal? Available { get; set; }
        public ECurrencyType Type { get; set; }
        public decimal MinimumReceiveAmount { get; set; }
        public decimal Step { get; set; }
        public bool Disabled { get; set; }
        public bool IsERC20 { get; set; }
        public bool Invisible { get; set; }
        public int? MediaId { get; set; }
        public string Url { get; set; }
        public bool IsFiat { get; set; }
        public byte MaxPrecision { get; set; }
        public string AddressRegEx { get; set; }
        public decimal? TransactionFee { get; set; }

        public int ConfirmsNeed { get; set; }
        public string ChangellyName { get; set; }
        public string CoinMarketCapId { get; set; }

        public bool HasMemo { get; set; }
        public string MemoName { get; set; }
        public string MemoRegEx { get; set; }
        public string MemoDescription { get; set; }

        private ICollection<ExchangeType> _exchangeTypesFrom;
        private ICollection<ExchangeType> _exchangeTypesTo;
        private ICollection<UserNotifyAlert> _notifyAlerts;
        private ICollection<CurrencyTypeNetwork> _networks;

        public virtual Media Media { get; set; }
        public virtual ICollection<ExchangeType> ExchangeTypesFrom
        {
            get { return _exchangeTypesFrom ?? (_exchangeTypesFrom = new List<ExchangeType>()); }
            protected set { _exchangeTypesFrom = value; }
        }
        public virtual ICollection<ExchangeType> ExchangeTypesTo
        {
            get { return _exchangeTypesTo ?? (_exchangeTypesTo = new List<ExchangeType>()); }
            protected set { _exchangeTypesTo = value; }
        }
        public virtual ICollection<UserNotifyAlert> NotifyAlerts
        {
            get { return _notifyAlerts ?? (_notifyAlerts = new List<UserNotifyAlert>()); }
            protected set { _notifyAlerts = value; }
        }

        public virtual ICollection<CurrencyTypeNetwork> Networks
        {
            get { return _networks ?? (_networks = new List<CurrencyTypeNetwork>()); }
            protected set { _networks = value; }
        }
    }

    public class CurrencyTypeConfig : EntityTypeConfiguration<CurrencyType>
    {
        public CurrencyTypeConfig()
        {
            Property(p => p.Available).HasPrecision(21, 8);
            Property(p => p.MinimumReceiveAmount).HasPrecision(21, 8);
            Property(p => p.Step).HasPrecision(21, 8);
            Property(p => p.TransactionFee).HasPrecision(21, 8);
            HasOptional(p => p.Media).WithMany().HasForeignKey(p => p.MediaId);
        }
    }
}
