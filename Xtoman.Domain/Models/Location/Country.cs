﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    public class Country : Location
    {
        public Country()
        {
            ProvincesStates = new HashSet<ProvinceState>();
            Languages = new HashSet<Language>();
        }
        public string IsoCode { get; set; }

        //Private properties
        private ICollection<ProvinceState> _provincesStates;
        private ICollection<Language> _languages;

        //Navigation properties
        public virtual Currency Currency { get; set; }
        public virtual ICollection<ProvinceState> ProvincesStates
        {
            get { return _provincesStates ?? (_provincesStates = new List<ProvinceState>()); }
            set { _provincesStates = value; }
        }
        public virtual ICollection<Language> Languages
        {
            get { return _languages ?? (_languages = new List<Language>()); }
            set { _languages = value; }
        }
    }
    public class CountryConfig : EntityTypeConfiguration<Country>
    {
        public CountryConfig()
        {
            Property(c => c.IsoCode).IsRequired().HasMaxLength(3);
            HasMany(c => c.ProvincesStates).WithRequired(x => x.Country);
            HasMany(c => c.Languages).WithMany(x => x.Countries);
        }
    }
}
