﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    public class Location : BaseEntity
    {
        public string Name { get; set; }
    }
    public class LocationConfig : EntityTypeConfiguration<Location>
    {
        public LocationConfig()
        {
            Property(l => l.Name).IsRequired().HasMaxLength(100);
        }
    }
}
