﻿using System.Collections.Generic;

namespace Xtoman.Domain.Models
{
    public class City : Location
    {
        public int ProvinceStateId { get; set; }

        // Private properties
        public virtual ProvinceState ProvinceState { get; set; }
        private ICollection<Supplier> _suppliers;
        private ICollection<Address> _addresses;
        private ICollection<CarrierPrice> _carrierPrices;

        // Navigation properties
        public ICollection<Supplier> Suppliers
        {
            get { return _suppliers ?? (_suppliers = new List<Supplier>()); }
            protected set { _suppliers = value; }
        }
        public ICollection<Address> Addresses
        {
            get { return _addresses ?? (_addresses = new List<Address>()); }
            protected set { _addresses = value; }
        }
        public ICollection<CarrierPrice> CarrierPrices
        {
            get { return _carrierPrices ?? (_carrierPrices = new List<CarrierPrice>()); }
            protected set { _carrierPrices = value; }
        }
    }

}
