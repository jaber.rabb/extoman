﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProvinceState : Location
    {
        public virtual int CountryId { get; set; }
        public virtual Country Country { get; set; }
    }

    public class ProvinceStateConfig : EntityTypeConfiguration<ProvinceState>
    {
        public ProvinceStateConfig()
        {
            HasRequired(x => x.Country).WithMany(x => x.ProvincesStates).HasForeignKey(x => x.CountryId);
        }
    }
}
