﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum TradeStatus : byte
    {
        [Display(Name = "در صف انتظار")]
        Queue = 0,
        [Display(Name = "درخواست ارسال شد")]
        Requested = 1,
        [Display(Name = "کمبود موجودی")]
        InsufficientFund = 2,
        [Display(Name = "ناموفق")]
        Failed = 4,
        [Display(Name = "انجام شد")]
        Completed = 8,
        [Display(Name = "نامشخص")]
        Voided = 16,
        [Display(Name = "تقریبا کامل شده")]
        PartiallyFilled = 32,
        [Display(Name = "دستی انجام شد")]
        CompletedManually = 64
    }
}
