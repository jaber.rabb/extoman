﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum BankGatewayType : byte
    {
        [Display(Name = "درگاه به پرداخت (ملت)", GroupName = "به-پرداخت")]
        BehPardakht = 0,
        [Display(Name = "درگاه سپ (سامان)", GroupName = "سامان")]
        Sep = 1,
        //[Display(Name = "آسان پرداخت", GroupName = "آسان-پرداخت")]
        //AsanPardakht = 2,
        //[Display(Name = "درگاه سداد (ملی)", GroupName = "سداد")]
        //Sadad = 4,
        [Display(Name = "درگاه پی دات آی آر", GroupName = "پی-دات-آی-آر")]
        Payir = 8,
        [Display(Name = "درگاه وندار", GroupName = "وندار")]
        Vandar = 16
    }
}
