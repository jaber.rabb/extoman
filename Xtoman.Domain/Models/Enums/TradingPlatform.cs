﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum TradingPlatform
    {
        [Display(Name = "هیچکدام")]
        None = 0,
        [Display(Name = "بیتفینکس Bitfinex")]
        Bitfinex = 1,
        [Display(Name = "بایننس Binance")]
        Binance = 2,
        [Display(Name = "جی داکس GDAX")]
        GDAX = 3,
        [Display(Name = "هیت بی تی سی HitBTC")]
        HitBTC = 4,
        [Display(Name = "پلونی اکس Poloniex")]
        Poloniex = 5,
        [Display(Name = "بیتترکس Bittrex")]
        Bittrex = 6,
        [Display(Name = "سی اکس CEX")]
        CEX = 7,
        [Display(Name = "کرکن Kraken")]
        Kraken = 8
    }
}
