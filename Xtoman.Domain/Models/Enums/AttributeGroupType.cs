﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum AttributeGroupType : byte
    {
        [Display(Name = "دکمه رادیویی")]
        RadioButton = 0,
        [Display(Name = "منوی کشویی")]
        DropDown = 1,
        [Display(Name = "انتخاب رنگ")]
        ColorPicker = 2
    }
}
