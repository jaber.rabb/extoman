﻿namespace Xtoman.Domain.Models
{
    public enum QueuedMessageType : byte
    {
        Email,
        SMS
    }
}
