﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum ExchangeRateType
    {
        [Display(Name = "تنطیمات پیش فرض سیستم")]
        SystemDefault,
        [Display(Name = "درصد اضافه")]
        ExtraFeePercent,
        [Display(Name = "میانگین مقایسه")]
        CompetitionAverage,
        [Display(Name = "کمترین حد مقایسه")]
        CompetitionLowest,
        [Display(Name = "بیشترین حد مقایسه")]
        CompetitionHighest,
        [Display(Name = "میانگین مقایسه + درصد اضافه")]
        CompetitionAveragePlusExtraFeePercent,
        [Display(Name = "کمترین حد مقایسه + درصد اضافه")]
        CompetitionLowestPlusExtraFeePercent,
        [Display(Name = "بیشترین حد مقایسه + درصد اضافه")]
        CompetitionHighestPlusExtraFeePercent
    }
}
