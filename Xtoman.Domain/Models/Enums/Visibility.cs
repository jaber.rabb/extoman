﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum Approvement : byte
    {
        [Display(Name = "در انتظار تایید", GroupName = "warning")]
        Pending = 0,
        [Display(Name = "تایید شده", GroupName = "success")]
        Aproved = 1,
        [Display(Name = "تایید نشده", GroupName = "danger")]
        NotAproved = 2
    }
}
