﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum WithdrawStatus : byte
    {
        [Display(Name = "در حال انجام", GroupName = "warning")]
        Processing = 0,
        [Display(Name = "ناموفق", GroupName = "danger")]
        Failure = 1,
        [Display(Name = "انجام شد", GroupName = "success")]
        Completed = 2,
        [Display(Name = "نامشخص", GroupName = "danger")]
        Voided = 4,
        [Display(Name = "لغو شد", GroupName = "danger")]
        Canceled = 8
    }
}
