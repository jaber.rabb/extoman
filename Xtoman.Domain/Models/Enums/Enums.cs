﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Xtoman.Domain.Models
{
    public enum PostType : byte
    {
        Standard,
        Image,
        Video,
        Quote
    }
    public enum MediaFileTypes : byte
    {
        Image,
        Video,
        PDF,
        PowerPoint
    }
    public enum NotificationType : byte
    {
        Info,
        Message,
        Alert
    }
}