﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum ExchangeFiatCoinType
    {
        [Display(Name = "رمز ارز به رمز ارز")]
        CoinToCoin = 0,
        [Display(Name = "رمز ارز به دلار، یورو یا درهم")]
        CoinToFiatUSD = 1,
        [Display(Name = "رمز ارز به تومان")]
        CoinToFiatToman = 2,
        [Display(Name = "دلار، یورو یا درهم به رمز ارز")]
        FiatUSDToCoin = 3,
        [Display(Name = "تومان به رمز ارز")]
        FiatTomanToCoin = 4,
        [Display(Name = "دلار، یورو یا درهم به تومان")]
        FiatUSDEuroDirhamToFiatToman = 5,
        [Display(Name = "تومان به دلار، یورو یا درهم")]
        FiatTomanToFiatUSDEuroDirham = 6,
        [Display(Name = "دلار، یورو یا درهم به دلار، یورو یا درهم")]
        FiatUSDEuroDirhamToFiatUSDEuroDirham = 7
    }
}
