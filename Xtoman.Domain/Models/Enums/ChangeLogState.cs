﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    public enum ChangeLogState
    {
        Added,
        Modified,
        SoftDeleted,
        HardDeleted
    }
}
