﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum PaidPaymentFor
    {
        [Display(Name = "شارژ ترید")]
        TradingDeposit,
        [Display(Name = "برداشت شخصی")]
        PersonalWithdraw,
        [Display(Name = "تبلیغات")]
        Advertisement,
        [Display(Name = "حقوق پرسنل")]
        Salary,
        [Display(Name = "خرید ارز")]
        BuyingCurrency,
        [Display(Name = "پرداخت قبض")]
        PayingBills,
        [Display(Name = "شارژ پنل اس ام اس")]
        ChargingSMSPanel
    }
}
