﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum NotifyAlertType
    {
        [Display(Name = "کاهش نرخ مبادله")]
        ExchangeRateBelow,
        [Display(Name = "افزایش نرخ مبادله")]
        ExchangeRateUpper,
        [Display(Name = "موجود شدن")]
        ValueAvailable
    }
}
