﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum ContractType
    {
        [Display(Name = "تسویه از قبل")]
        PrePaid = 0,
        [Display(Name = "تسویه با هر ترید")]
        PayAsGo = 1,
        [Display(Name = "تسویه روزانه")]
        DailyPay = 2,
        [Display(Name = "تسویه هفتگی")]
        WeeklyPay = 3,
        [Display(Name = "تسویه ماهانه")]
        MonthlyPay = 4
    }
}
