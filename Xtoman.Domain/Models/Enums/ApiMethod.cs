﻿namespace Xtoman.Domain.Models
{
    public enum ApiMethod
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}
