﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum SupportDepartment : byte
    {
        [Display(Name = "اطلاعات")]
        Info = 0,
        [Display(Name = "مشکلات پرداخت")]
        Payments = 1,
        [Display(Name = "مشکلات ورود/ثبت نام")]
        LoginRegister = 2,
        [Display(Name = "مشکلات فنی")]
        Technical = 4,
        [Display(Name = "پیشنهادات")]
        Suggestions = 8,
        [Display(Name = "مشکلات دیگر")]
        Other = 16
    }
}
