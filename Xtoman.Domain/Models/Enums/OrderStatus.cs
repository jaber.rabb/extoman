﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum OrderStatus : byte
    {
        [Display(Name = "در حال انتظار", GroupName = "warning")]
        Pending = 0,

        [Display(Name = "در حال انجام", GroupName = "info")]
        Processing = 1,

        [Display(Name = "تکمیل شده", GroupName = "success")]
        Complete = 2,

        [Display(Name = "کنسل شده", GroupName = "danger")]
        Canceled = 4,

        [Display(Name = "ناقص", GroupName = "danger")]
        InComplete = 8,

        [Display(Name = "استرداد انجام شد", GroupName = "info")]
        RefundComplete = 16,

        [Display(Name = "خطا در عملیات تبدیل", GroupName = "danger")]
        TradeFailed = 32,

        [Display(Name = "خطا در عملیات واریز", GroupName = "danger")]
        WithdrawFailed = 64,

        [Display(Name = "استرداد وجه به کارت", GroupName = "warning")]
        RefundNotValidCard = 128,
    }
}
