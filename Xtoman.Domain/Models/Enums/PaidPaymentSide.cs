﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum PaidPaymentSide
    {
        [Display(Name = "هیچکدام")]
        None,
        [Display(Name = "بدهکار")]
        Debtor,
        [Display(Name = "بستانکار")]
        Creditor
    }
}
