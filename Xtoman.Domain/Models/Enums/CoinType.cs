﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum CoinType
    {
        [Display(Name = "Bitcoin", ShortName = "BTC", GroupName = "#f79c2e")]
        Bitcoin = 4,
        [Display(Name = "Ethereum", ShortName = "ETH", GroupName = "#5580a3")]
        Ethereum = 5,
        [Display(Name = "Ethereum Classic", ShortName = "ETC", GroupName = "#669073")]
        EthereumClassic = 6,
        [Display(Name = "Monero", ShortName = "XMR", GroupName = "#f26822")]
        Monero = 7,
        [Display(Name = "Litecoin", ShortName = "LTC", GroupName = "#ccc")]
        Litecoin = 8,
        [Display(Name = "Bitcoin cash", ShortName = "BCH", GroupName = "#f79c2e")]
        BitcoinCash = 9,
        [Display(Name = "Dash", ShortName = "DASH", GroupName = "#1c75bc")]
        Dash = 10,
        [Display(Name = "NEO", ShortName = "NEO", GroupName = "#50b70a")]
        NEO = 11,
        [Display(Name = "Zcash", ShortName = "ZEC", GroupName = "#febf34")]
        Zcash = 12,
        [Display(Name = "Ripple", ShortName = "XRP", GroupName = "#000000")]
        Ripple = 13,
        [Display(Name = "Factom", ShortName = "FCT", GroupName = "#000000")]
        Factom = 14,
        [Display(Name = "Dogecoin", ShortName = "DOGE", GroupName = "#000000")]
        Dogecoin = 15
    }
}