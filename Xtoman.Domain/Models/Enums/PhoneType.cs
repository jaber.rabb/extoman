﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum PhoneType
    {
        [Display(Name = "تلفن")]
        Phone,
        [Display(Name = "فکس")]
        Fax,
        [Display(Name = "موبایل")]
        Mobile
    }
}
