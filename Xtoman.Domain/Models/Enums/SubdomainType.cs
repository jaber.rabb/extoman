﻿namespace Xtoman.Domain.Models
{
    public enum SubdomainType : byte
    {
        Web = 0,
        Admin = 1,
        Api = 2,
        Beta = 4,
        Telegram = 8
    }
}
