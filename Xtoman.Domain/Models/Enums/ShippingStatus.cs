﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum ShippingStatus : byte
    {
        [Display(Name = "ارسال نشده")]
        NotShipped = 0,
        [Display(Name = "آماده برای ارسال")]
        ReadyForShipment = 1,
        [Display(Name = "ارسال شده در چند پارت")]
        PartiallyShipped = 2,
        [Display(Name = "ارسال شده")]
        Shipped = 4,
        [Display(Name = "تحویل شده")]
        Delivered = 8
    }
}
