﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum BankType
    {
        [Display(Name = "بانک پارسیان")]
        Parsian,
        [Display(Name = "بانک ملت")]
        Mellat,
        [Display(Name = "بانک ملی")]
        Melli,
        [Display(Name = "بانک سپه")]
        Sepah,
        [Display(Name = "پست بانک")]
        Post,
        [Display(Name = "بانک توسعه صادرات")]
        ToseSaderat,
        [Display(Name = "بانک صنعت و معدن")]
        SanatoMadan,
        [Display(Name = "بانک کشاورزی")]
        Keshavarzi,
        [Display(Name = "بانک مسکن")]
        Maskan,
        [Display(Name = "بانک توسعه تعاون")]
        ToseTaavon,
        [Display(Name = "بانک اقتصاد نوین")]
        EghtesadNovin,
        [Display(Name = "بانک پاسارگاد")]
        Pasargad,
        [Display(Name = "بانک تجارت")]
        Tejarat,
        [Display(Name = "بانک رفاه کارگران")]
        RefahKargaran,
        [Display(Name = "بانک سامان")]
        Saman,
        [Display(Name = "بانک سرمایه")]
        Sarmaye,
        [Display(Name = "بانک سینا")]
        Sina,
        [Display(Name = "بانک صادرات")]
        Saderat,
        [Display(Name = "بانک کارآفرین")]
        Karafarin,
        [Display(Name = "بانک دی")]
        Dey,
        [Display(Name = "بانک انصار")]
        Ansar,
        [Display(Name = "بانک ایران زمین")]
        IranZamin,
        [Display(Name = "بانک گردشگری")]
        Gardeshgari,
        [Display(Name = "بانک حکمت ایرانیان")]
        HekmatIranian,
        [Display(Name = "بانک قوامین")]
        Ghavamin,
        [Display(Name = "بانک شهر")]
        Shahr,
        [Display(Name = "بانک خاورمیانه")]
        Khavarmiane,
        [Display(Name = "قرض الحسنه مهر ایران")]
        MehreIran,
        [Display(Name = "بانک قرض الحسنه رسالت")]
        Resalat,
        [Display(Name = "سایر بانک ها")]
        Other
    }
}
