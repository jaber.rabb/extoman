﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum SpecificationSearchFilter : byte
    {
        [Display(Name = "بدون فیلتر")]
        None = 0,
        [Display(Name = "نوار کنار")]
        SideBar = 1,
        [Display(Name = "جستجوی پیشرفته")]
        AdvancedSearch = 2,
        [Display(Name = "جستجوی پیشرفته و نوار کنار")]
        Both = 4
    }
}
