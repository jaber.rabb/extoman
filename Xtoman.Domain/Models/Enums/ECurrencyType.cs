﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Utility;

namespace Xtoman.Domain.Models
{
    public enum ECurrencyType
    {
        [Display(Name = "Shetab", ShortName = "TOMAN", GroupName = "#333333", Prompt = "تومان")]
        Shetab = 0,
        [Display(Name = "PAYEER", ShortName = "PAYEER", GroupName = "#0099de", Prompt = "پییر")]
        PAYEER = 1,
        [Display(Name = "PerfectMoney", ShortName = "PM", GroupName = "#f01010", Prompt = "پرفکت مانی")]
        PerfectMoney = 2,
        [Display(Name = "WebMoney", ShortName = "WM", GroupName = "#036cb5", Prompt = "وب مانی")]
        WebMoney = 3,
        [Display(Name = "Bitcoin", ShortName = "BTC", GroupName = "#f79c2e", Prompt = "بیت کوین")]
        Bitcoin = 4,
        [Display(Name = "Ethereum", ShortName = "ETH", GroupName = "#5580a3", Prompt = "اتریوم")]
        Ethereum = 5,
        [Display(Name = "Ethereum Classic", ShortName = "ETC", GroupName = "#669073", Prompt = "اتریوم کلاسیک")]
        EthereumClassic = 6,
        [Display(Name = "Monero", ShortName = "XMR", GroupName = "#f26822", Prompt = "مونرو")]
        Monero = 7,
        [Display(Name = "Litecoin", ShortName = "LTC", GroupName = "#ccc", Prompt = "لایت کوین")]
        Litecoin = 8,
        [Display(Name = "Bitcoin cash", ShortName = "BCH", GroupName = "#f79c2e", Prompt = "بیت کوین کش")]
        BitcoinCash = 9,
        [Display(Name = "Dash", ShortName = "DASH", GroupName = "#1c75bc", Prompt = "دش")]
        Dash = 10,
        [Display(Name = "NEO", ShortName = "NEO", GroupName = "#50b70a", Prompt = "نئو")]
        NEO = 11,
        [Display(Name = "Zcash", ShortName = "ZEC", GroupName = "#febf34", Prompt = "زد کش")]
        Zcash = 12,
        [Display(Name = "Ripple", ShortName = "XRP", GroupName = "#000000", Prompt = "ریپل")]
        Ripple = 13,
        [Display(Name = "Factom", ShortName = "FCT", GroupName = "#000000", Prompt = "فکتوم")]
        Factom = 14,
        [Display(Name = "Dogecoin", ShortName = "DOGE", GroupName = "#000000", Prompt = "داج کوین")]
        Dogecoin = 15,
        [Display(Name = "US Dollar", ShortName = "USD", GroupName = "#000000", Prompt = "دلار")]
        USDollar = 16,
        [Display(Name = "Tether US Dollar", ShortName = "USDT", GroupName = "#000000", Prompt = "دلار تتر")]
        Tether = 17,
        [Display(Name = "Bitcoin Gold", ShortName = "BTG", GroupName = "#000000", Prompt = "بیت کوین گلد")]
        BitcoinGold = 18,
        [Display(Name = "IOTA", ShortName = "IOTA", GroupName = "#000000", Prompt = "آی او تا")]
        IOTA = 19,
        [Display(Name = "Qtum", ShortName = "QTUM", GroupName = "#000000", Prompt = "کوانتوم")]
        QTUM = 20,
        [Display(Name = "Cardano", ShortName = "ADA", GroupName = "#000000", Prompt = "کاردانو")]
        Cardano = 21,
        [Display(Name = "Ark", ShortName = "ARK", GroupName = "#000000", Prompt = "آرک")]
        Ark = 22,
        [Display(Name = "Stellar", ShortName = "XLM", GroupName = "#000000", Prompt = "استلار")]
        Stellar = 23,
        [Display(Name = "Lisk", ShortName = "LSK", GroupName = "#000000", Prompt = "لیسک")]
        Lisk = 24,
        [Display(Name = "Nano", ShortName = "NANO", GroupName = "#000000", Prompt = "نانو")]
        Nano = 25,
        [Display(Name = "Verge", ShortName = "XVG", GroupName = "#000000", Prompt = "ورج")]
        Verge = 26,
        [Display(Name = "Stratis", ShortName = "STRAT", GroupName = "#000000", Prompt = "استراتیس")]
        Stratis = 27,
        [Display(Name = "Steem", ShortName = "STEEM", GroupName = "#000000", Prompt = "استیم")]
        Steem = 28,
        [Display(Name = "Waves", ShortName = "WAVES", GroupName = "#000000", Prompt = "ویوز")]
        Waves = 29,
        [Display(Name = "Komodo", ShortName = "KMD", GroupName = "#000000", Prompt = "کومودو")]
        Komodo = 30,
        [Display(Name = "BitShares", ShortName = "BTS", GroupName = "#000000", Prompt = "بیت شیرز")]
        BitShares = 31,
        [Display(Name = "Neblio", ShortName = "NEBL", GroupName = "#000000", Prompt = "نبلیو")]
        Neblio = 32,
        [Display(Name = "NAV Coin", ShortName = "NAV", GroupName = "#000000", Prompt = "نو کوین")]
        NAVCoin = 33,
        [Display(Name = "PIVX", ShortName = "PIVX", GroupName = "#000000", Prompt = "پیویکس")]
        PIVX = 34,
        [Display(Name = "NEM", ShortName = "XEM", GroupName = "#000000", Prompt = "ان ای ام")]
        NEM = 35,
        [Display(Name = "Euro", ShortName = "EUR", GroupName = "#000000", Prompt = "یورو")]
        Euro = 36,
        [Display(Name = "Dirham", ShortName = "AED", GroupName = "#000000", Prompt = "درهم")]
        Dirham = 37,
        [Display(Name = "PerfectMoney Voucher", ShortName = "ev-PM", GroupName = "#000000", Prompt = "ووچر پرفکت مانی")]
        PerfectMoneyVoucher = 38,
        [Display(Name = "Decred", ShortName = "DCR", GroupName = "#000000", Prompt = "دیکرد")]
        Decred = 39,
        [Display(Name = "EOS", ShortName = "EOS", GroupName = "#000000", Prompt = "ای او اس")]
        EOS = 40,
        [Display(Name = "TRON", ShortName = "TRX", GroupName = "#000000", Prompt = "ترون")]
        Tron = 41,
        [Display(Name = "ICON", ShortName = "ICX", GroupName = "#000000", Prompt = "آیکون")]
        Icon = 42,
        [Display(Name = "Nebulas", ShortName = "NAS", GroupName = "#000000", Prompt = "نبیولاس")]
        Nebulas = 43,
        [Display(Name = "Siacoin", ShortName = "SC", GroupName = "#000000", Prompt = "سیاکوین")]
        Siacoin = 44,
        [Display(Name = "Bitcoin Diamond", ShortName = "BCD", GroupName = "#000000", Prompt = "بیت کوین دایموند")]
        BitcoinDiamond = 45,
        [Display(Name = "ZCoin", ShortName = "XZC", GroupName = "#000000", Prompt = "زد کوین")]
        ZCoin = 46,
        [Display(Name = "Ravencoin", ShortName = "RVN", GroupName = "#000000", Prompt = "ریون کوین")]
        Ravencoin = 47,
        [Display(Name = "Horizen", ShortName = "ZEN", GroupName = "#000000", Prompt = "هوریزن")]
        Horizen = 48,
        //Tokens
        [Display(Name = "Chainlink", ShortName = "LINK", GroupName = "#000000", Prompt = "چینلینک")]
        Chainlink = 49,
        [Display(Name = "Basic Attention Token", ShortName = "BAT", GroupName = "#000000", Prompt = "بیسیک اتنشن توکن")]
        BasicAttentionToken = 50,
        [Display(Name = "OmiseGO", ShortName = "OMG", GroupName = "#000000", Prompt = "اومیسه گو")]
        OmiseGO = 51,
        [Display(Name = "0x", ShortName = "ZRX", GroupName = "#000000", Prompt = "زیرو اکس")]
        ZeroX = 52,
        [Display(Name = "IOST", ShortName = "IOST", GroupName = "#000000", Prompt = "آی او اس تی")]
        IOST = 53,
        [Display(Name = "Enjin Coin", ShortName = "ENJ", GroupName = "#000000", Prompt = "انجین کوین")]
        EnjinCoin = 54,
        [Display(Name = "Binance Coin", ShortName = "BNB", GroupName = "#ebb42e", Prompt = "بایننس کوین")]
        BinanceCoin = 55,
        [Display(Name = "Theta", ShortName = "THETA", GroupName = "#25cfd4", Prompt = "تتا")]
        Theta = 56,
        [Display(Name = "Theta Fuel", ShortName = "TFUEL", GroupName = "#ff7811", Prompt = "تتا فیول")]
        TFuel = 57,
        [Display(Name = "Aeternity", ShortName = "AE", GroupName = "#de3f6b", Prompt = "ایترنیتی")]
        Aeternity = 58,
        [Display(Name = "Ontology", ShortName = "ONT", GroupName = "#32a4be", Prompt = "اونتولوژی")]
        Ontology = 59
    }

    public static class ECurrencyTypeHelper
    {
        public static bool IsFiat(this ECurrencyType eCurrencyType)
        {
            switch (eCurrencyType)
            {
                case ECurrencyType.Shetab:
                case ECurrencyType.PAYEER:
                case ECurrencyType.PerfectMoney:
                case ECurrencyType.WebMoney:
                case ECurrencyType.PerfectMoneyVoucher:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsERC20(this ECurrencyType eCurrencyType)
        {
            switch (eCurrencyType)
            {
                case ECurrencyType.Chainlink:
                case ECurrencyType.BasicAttentionToken:
                case ECurrencyType.OmiseGO:
                case ECurrencyType.ZeroX:
                //case ECurrencyType.IOST:
                case ECurrencyType.EnjinCoin:
                case ECurrencyType.Tether:
                    return true;
                default:
                    return false;
            }
        }

        public static string BlockExplorerAddress(ECurrencyType eCurrencyType)
        {
            switch (eCurrencyType)
            {
                case ECurrencyType.Bitcoin:
                    return "https://live.blockcypher.com/btc/address";
                case ECurrencyType.Ethereum:
                    return "https://www.etherchain.org/account";
                case ECurrencyType.EthereumClassic:
                    return "http://gastracker.io/addr/";
                case ECurrencyType.Monero:
                    break;
                case ECurrencyType.Litecoin:
                    return "https://live.blockcypher.com/ltc/address";
                case ECurrencyType.BitcoinCash:
                    return "https://explorer.bitcoin.com/bch/address";
                case ECurrencyType.Dash:
                    return "https://live.blockcypher.com/dash/address";
                case ECurrencyType.NEO:
                    return "https://neotracker.io/address";
                case ECurrencyType.Zcash:
                    return "https://zcashnetwork.info/address";
                case ECurrencyType.Ripple:
                    return "https://bithomp.com/explorer";
                case ECurrencyType.Factom:
                    break;
                case ECurrencyType.Dogecoin:
                    return "https://live.blockcypher.com/doge/address";
                case ECurrencyType.Tether:
                    return "https://omniexplorer.info/address";
                case ECurrencyType.BitcoinGold:
                    return "https://btgexplorer.com/address";
                case ECurrencyType.IOTA:
                    return "https://thetangle.org/address";
                case ECurrencyType.QTUM:
                    return "https://explorer.qtum.org/address";
                case ECurrencyType.Cardano:
                    return "https://cardanoexplorer.com/address";
                case ECurrencyType.Ark:
                    return "https://explorer.ark.io/wallets";
                case ECurrencyType.Stellar:
                    return "https://stellarchain.io/address";
                case ECurrencyType.Lisk:
                    return "https://explorer.lisk.io/address";
                case ECurrencyType.Nano:
                    return "https://www.nanode.co/account";
                case ECurrencyType.Verge:
                    return "https://verge-blockchain.info/address";
                case ECurrencyType.Stratis:
                    return "https://cryptobe.com/address";
                case ECurrencyType.Steem:
                    break;
                case ECurrencyType.Waves:
                    return "https://wavesexplorer.com/address";
                case ECurrencyType.Komodo:
                    break;
                case ECurrencyType.BitShares:
                    break;
                case ECurrencyType.Neblio:
                    break;
                case ECurrencyType.NAVCoin:
                    break;
                case ECurrencyType.PIVX:
                    break;
                case ECurrencyType.NEM:
                    break;
                case ECurrencyType.Decred:
                    return "https://mainnet.decred.org/address";
                case ECurrencyType.EOS:
                    return "https://www.eosx.io/account";
                case ECurrencyType.Tron:
                    return "https://tronscan.org/#/address";
                case ECurrencyType.Icon:
                    break;
                case ECurrencyType.Nebulas:
                    break;
                case ECurrencyType.Siacoin:
                    break;
                case ECurrencyType.BitcoinDiamond:
                    break;
                case ECurrencyType.Horizen:
                    return "https://explorer.zensystem.io/address";
                case ECurrencyType.ZCoin:
                    return "https://explorer.zcoin.io/address";
                case ECurrencyType.Ravencoin:
                    return "https://ravencoin.network/address";
                case ECurrencyType.Chainlink:
                case ECurrencyType.BasicAttentionToken:
                case ECurrencyType.OmiseGO:
                case ECurrencyType.ZeroX:
                case ECurrencyType.IOST:
                case ECurrencyType.EnjinCoin:
                    return "https://ethplorer.io/address";
                case ECurrencyType.BinanceCoin:
                    return "https://explorer.binance.org/address";
                case ECurrencyType.Aeternity:
                    return "https://explorer.aeternity.io/account/transactions";
                case ECurrencyType.Theta:
                case ECurrencyType.TFuel:
                    return "https://explorer.thetatoken.org/account";
                case ECurrencyType.Ontology:
                    return "https://explorer.ont.io/address";
                default:
                    break;
            }
            return "";
        }

        public static string BlockExplorerTransaction(ECurrencyType eCurrencyType)
        {
            switch (eCurrencyType)
            {
                case ECurrencyType.Bitcoin:
                    return "https://live.blockcypher.com/btc/tx";
                case ECurrencyType.Ethereum:
                    return "https://www.etherscan.io/tx";
                case ECurrencyType.EthereumClassic:
                    return "http://gastracker.io/tx/";
                case ECurrencyType.Monero:
                    break;
                case ECurrencyType.Litecoin:
                    return "https://live.blockcypher.com/ltc/tx";
                case ECurrencyType.BitcoinCash:
                    return "https://explorer.bitcoin.com/bch/tx";
                case ECurrencyType.Dash:
                    return "https://live.blockcypher.com/dash/tx";
                case ECurrencyType.NEO:
                    return "https://neotracker.io/tx";
                case ECurrencyType.Zcash:
                    return "https://zcashnetwork.info/tx";
                case ECurrencyType.Ripple:
                    return "https://bithomp.com/explorer";
                case ECurrencyType.Factom:
                    break;
                case ECurrencyType.Dogecoin:
                    return "https://live.blockcypher.com/doge/tx";
                case ECurrencyType.Tether:
                    return "https://omniexplorer.info/tx";
                case ECurrencyType.BitcoinGold:
                    return "https://btgexplorer.com/tx";
                case ECurrencyType.IOTA:
                    return "https://thetangle.org/tx";
                case ECurrencyType.QTUM:
                    return "https://explorer.qtum.org/tx";
                case ECurrencyType.Cardano:
                    return "https://cardanoexplorer.com/tx";
                case ECurrencyType.Ark:
                    return "https://explorer.ark.io/tx";
                case ECurrencyType.Stellar:
                    return "https://stellarchain.io/tx";
                case ECurrencyType.Lisk:
                    return "https://explorer.lisk.io/tx";
                case ECurrencyType.Nano:
                    return "https://www.nanode.co/tx";
                case ECurrencyType.Verge:
                    return "https://verge-blockchain.info/tx";
                case ECurrencyType.Stratis:
                    return "https://cryptobe.com/tx";
                case ECurrencyType.Steem:
                    break;
                case ECurrencyType.Waves:
                    return "https://wavesexplorer.com/tx";
                case ECurrencyType.Komodo:
                    break;
                case ECurrencyType.BitShares:
                    break;
                case ECurrencyType.Neblio:
                    break;
                case ECurrencyType.NAVCoin:
                    break;
                case ECurrencyType.PIVX:
                    break;
                case ECurrencyType.NEM:
                    break;
                case ECurrencyType.Decred:
                    return "https://mainnet.decred.org/tx";
                case ECurrencyType.EOS:
                    return "https://www.eosx.io/tx";
                case ECurrencyType.Tron:
                    return "https://tronscan.org/#/transaction";
                case ECurrencyType.Icon:
                    break;
                case ECurrencyType.Nebulas:
                    break;
                case ECurrencyType.Siacoin:
                    break;
                case ECurrencyType.BitcoinDiamond:
                    break;
                case ECurrencyType.Horizen:
                    return "https://explorer.zensystem.io/tx";
                case ECurrencyType.ZCoin:
                    return "https://explorer.zcoin.io/tx";
                case ECurrencyType.Ravencoin:
                    return "https://ravencoin.network/tx";
                case ECurrencyType.Chainlink:
                case ECurrencyType.BasicAttentionToken:
                case ECurrencyType.OmiseGO:
                case ECurrencyType.ZeroX:
                case ECurrencyType.IOST:
                case ECurrencyType.EnjinCoin:
                    return "https://ethplorer.io/tx";
                case ECurrencyType.BinanceCoin:
                    return "https://explorer.binance.org/tx";
                case ECurrencyType.Aeternity:
                    return "https://explorer.aeternity.io/transactions";
                case ECurrencyType.Theta:
                case ECurrencyType.TFuel:
                    return "https://explorer.thetatoken.org/txs";
                case ECurrencyType.Ontology:
                    return "https://explorer.ont.io/transaction";
                default:
                    break;
            }
            return "";
        }

        public static ECurrencyType? ToECurrencyAccountType(string currency)
        {
            if (currency.HasValue())
                currency = currency.ToLower().Replace("-", " ").Replace("_", " ");

            switch (currency)
            {
                case "تومان":
                case "ریال":
                case "شتاب":
                case "toman":
                case "shetab":
                case "rial":
                case "شتاب تومان ریال":
                    return ECurrencyType.Shetab;
                case "payeer":
                case "پییر":
                case "پاییر":
                    return ECurrencyType.PAYEER;
                case "pm":
                case "perfectmoney":
                case "perfect money":
                case "perfect-money":
                case "پرفکت مانی":
                    return ECurrencyType.PerfectMoney;
                case "ev-pm":
                case "ووچر پرفکت مانی":
                case "perfectmoneyvoucher":
                case "perfect-money-voucher":
                case "perfect money voucher":
                case "perfectmoney-voucher":
                case "perfectmoney voucher":
                    return ECurrencyType.PerfectMoneyVoucher;
                case "wm":
                case "webmoney":
                case "wmtransfer":
                case "web money":
                case "وب مانی":
                    return ECurrencyType.WebMoney;
                case "btc":
                case "bitcoin":
                case "بیت کوین":
                    return ECurrencyType.Bitcoin;
                case "eth":
                case "ethereum":
                case "ایتریوم":
                case "اتریوم":
                    return ECurrencyType.Ethereum;
                case "etc":
                case "ethereumclassic":
                case "ethereum classic":
                case "ایتریوم کلاسیک":
                case "اتریوم کلاسیک":
                    return ECurrencyType.EthereumClassic;
                case "ltc":
                case "litecoin":
                case "لایت کوین":
                    return ECurrencyType.Litecoin;
                case "xmr":
                case "monero":
                case "مونرو":
                    return ECurrencyType.Monero;
                case "bch":
                case "bchabc":
                case "bitcoincash":
                case "bitcoincashabc":
                case "bitcoin cash":
                case "bitcoin cash abc":
                case "bcash":
                case "بیت کوین کش":
                    return ECurrencyType.BitcoinCash;
                case "dsh":
                case "dash":
                case "دش":
                    return ECurrencyType.Dash;
                case "neo":
                case "نیو":
                case "نئو":
                    return ECurrencyType.NEO;
                case "zec":
                case "zcash":
                case "زد کش":
                case "زدکش":
                    return ECurrencyType.Zcash;
                case "xrp":
                case "ripple":
                case "ریپل":
                    return ECurrencyType.Ripple;
                case "fct":
                case "factom":
                case "فکتوم":
                case "فاکتوم":
                    return ECurrencyType.Factom;
                case "doge":
                case "dogecoin":
                case "دوج":
                case "دج":
                case "دوج کوین":
                case "دج کوین":
                    return ECurrencyType.Dogecoin;
                case "usdt":
                case "tether":
                case "تتر":
                    return ECurrencyType.Tether;
                case "cardano":
                case "ada":
                case "کاردانو":
                    return ECurrencyType.Cardano;
                case "iota":
                case "miota":
                case "آی او تا":
                    return ECurrencyType.IOTA;
                case "qtum":
                case "کوانتوم":
                    return ECurrencyType.QTUM;
                case "ark":
                case "آرک":
                    return ECurrencyType.Ark;
                case "bts":
                case "bitshares":
                case "بیت شیرز":
                    return ECurrencyType.BitShares;
                case "kmd":
                case "komodo":
                case "کومودو":
                    return ECurrencyType.Komodo;
                case "lisk":
                case "lsk":
                case "لیسک":
                    return ECurrencyType.Lisk;
                case "نانو":
                case "nano":
                    return ECurrencyType.Nano;
                case "nav":
                case "navcoin":
                case "nav-coin":
                case "nav coin":
                case "نو کوین":
                    return ECurrencyType.NAVCoin;
                case "nebl":
                case "neblio":
                case "نبلیو":
                    return ECurrencyType.Neblio;
                case "pivx":
                case "پیویکس":
                    return ECurrencyType.PIVX;
                case "steem":
                case "استیم":
                    return ECurrencyType.Steem;
                case "XLM":
                case "stellar":
                case "استلار":
                    return ECurrencyType.Stellar;
                case "strat":
                case "stratis":
                case "استراتیس":
                    return ECurrencyType.Stratis;
                case "xvg":
                case "verge":
                case "ورج":
                    return ECurrencyType.Verge;
                case "waves":
                case "ویوز":
                    return ECurrencyType.Waves;
                case "nem":
                case "xem":
                case "ان ای ام":
                case "ان-ای-ام":
                    return ECurrencyType.NEM;
                case "decred":
                case "dcr":
                case "دیکرد":
                    return ECurrencyType.Decred;
                case "eos":
                case "ای-او-اس":
                case "ای او اس":
                    return ECurrencyType.EOS;
                case "tron":
                case "trx":
                case "ترون":
                    return ECurrencyType.Tron;
                case "icx":
                case "icon":
                case "آیکون":
                    return ECurrencyType.Icon;
                case "nas":
                case "nebulas":
                case "نبیولاس":
                    return ECurrencyType.Nebulas;
                case "sc":
                case "siacoin":
                case "سیاکوین":
                case "sia-coin":
                case "سیا-کوین":
                case "sia coin":
                case "سیا کوین":
                    return ECurrencyType.Siacoin;
                case "bcd":
                case "bitcoindiamond":
                case "bitcoin-diamond":
                case "bitcoin diamond":
                case "بیت کوین دایموند":
                case "بیت-کوین-دایموند":
                    return ECurrencyType.BitcoinDiamond;
                case "xzc":
                case "zcoin":
                case "زدکوین":
                case "زد-کوین":
                    return ECurrencyType.ZCoin;
                case "هوریزن":
                case "horizen":
                case "zen":
                    return ECurrencyType.Horizen;
                case "rvn":
                case "ریون کوین":
                case "ریون-کوین":
                case "ravencoin":
                    return ECurrencyType.Ravencoin;
                case "chainlink":
                case "link":
                case "چینلینک":
                    return ECurrencyType.Chainlink;
                case "بیسیک-اتنشن-توکن":
                case "بیسیک اتنشن توکن":
                case "bat":
                case "basicattentiontoken":
                case "basic-attention-token":
                case "basic attention token":
                    return ECurrencyType.BasicAttentionToken;
                case "omg":
                    return ECurrencyType.OmiseGO;
                case "0x":
                case "zrx":
                case "zerox":
                case "زیرو-اکس":
                case "زیرو اکس":
                    return ECurrencyType.ZeroX;
                case "آی او اس تی":
                case "آی-او-اس-تی":
                case "iost":
                    return ECurrencyType.IOST;
                case "enj":
                case "enjin coin":
                case "enjincoin":
                case "انجین-کوین":
                case "انجین کوین":
                    return ECurrencyType.EnjinCoin;
                case "bnb":
                case "binance coin":
                case "binancecoin":
                case "بایننس-کوین":
                case "بایننس کوین":
                    return ECurrencyType.BinanceCoin;
                case "ae":
                case "aeternity":
                case "ایترنیتی":
                    return ECurrencyType.Aeternity;
                case "theta":
                case "تتا":
                    return ECurrencyType.Theta;
                case "tfuel":
                case "theta fuel":
                case "theta-fuel":
                case "تی-فیول":
                case "تی فیول":
                    return ECurrencyType.TFuel;
                case "ont":
                case "ontology":
                case "اونتولوژی":
                    return ECurrencyType.Ontology;
            }
            return null;
        }
        public static string ToBinanceAsset(this ECurrencyType type)
        {
            switch (type)
            {
                case ECurrencyType.BitcoinCash:
                    return "BCH";
                default:
                    return type.ToDisplay(DisplayProperty.ShortName).ToUpper();
            }
        }

        public static string ToKrakenAsset(this ECurrencyType type)
        {
            switch (type)
            {
                case ECurrencyType.Bitcoin:
                    return "XXBT";
                case ECurrencyType.Ethereum:
                    return "XETH";
                case ECurrencyType.EthereumClassic:
                    return "XETC";
                case ECurrencyType.Monero:
                    return "XXMR";
                case ECurrencyType.Litecoin:
                    return "XLTC";
                case ECurrencyType.BitcoinCash:
                    return "BCH";
                case ECurrencyType.Dash:
                    return "DASH";
                case ECurrencyType.Zcash:
                    return "XZEC";
                case ECurrencyType.Ripple:
                    return "XXRP";
                case ECurrencyType.Tether:
                    return "USDT";
                case ECurrencyType.Stellar:
                    return "XXLM";
                case ECurrencyType.Dogecoin:
                    return "XXDG";
                default:
                    return type.ToDisplay(DisplayProperty.ShortName);
            }
        }
    }
}
