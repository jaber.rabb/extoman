﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum VerificationStatus : byte
    {
        [Display(Name = "ارسال نشده", GroupName = "#337ab7")]
        NotSent = 0,
        [Display(Name = "در انتظار تایید", GroupName = "#ff6633")]
        Pending = 1,
        [Display(Name = "تایید شده", GroupName = "#00a86b")]
        Confirmed = 2,
        [Display(Name = "تایید نشده", GroupName = "#b43b2c")]
        NotConfirmed = 4
    }
}