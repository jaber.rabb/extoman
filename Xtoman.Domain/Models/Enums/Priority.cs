﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum Priority : byte
    {
        [Display(Name = "کم", ShortName = "success", GroupName = "#00b19d")]
        Low = 0,
        [Display(Name = "معمولی", ShortName = "primary", GroupName = "#3bafda")]
        Medium = 1,
        [Display(Name = "مهم", ShortName = "pink", GroupName = "#f76397")]
        High = 2
    }
}
