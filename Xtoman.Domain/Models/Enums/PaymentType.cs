﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum PaymentType : byte
    {
        [Display(Name = "کارت به کارت")]
        CardToCard = 0,

        [Display(Name = "دستی")]
        Manually = 1,

        [Display(Name = "اینترنتی")]
        Online = 2
    }
}
