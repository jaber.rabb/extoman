﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum UserVerificationType : byte
    {
        [Display(Name = "ایمیل")]
        Email = 0,
        [Display(Name = "موبایل")]
        Mobile = 1,
        [Display(Name = "تصویر سلفی با مدارک")]
        Identity = 2,
        [Display(Name = "کارت ملی")]
        NationalCard = 3,
        [Display(Name = "کارت بانکی")]
        BankCard = 4,
        [Display(Name = "تصویر کارت بانکی")]
        BankCardImage = 5,
        [Display(Name = "تلفن ثابت")]
        Telephone = 6,
        [Display(Name = "فینوتک")]
        FinnotechVerify = 7
    }
}