﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum WithdrawFromType
    {
        [Display(Name = "ایکس تومن")]
        XtomanWallet = 0,
        [Display(Name = "وب سرویس")]
        ApiWallet = 1,
        [Display(Name = "پلتفرم ترید")]
        TradingPlatform = 2,
        [Display(Name = "کیف پول شخصی")]
        Personal = 3,
        [Display(Name = "حساب بانکی")]
        BankAccount = 4
    }
}
