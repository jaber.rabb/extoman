﻿namespace Xtoman.Domain.Models
{
    public enum ExchangeApiType : byte
    {
        None = 0,
        Changelly = 1,
        CoinPayments = 2,
        CoinMarketCap = 4,
        TGJU = 8,
        ArzLive = 16,
        NerkhAPI = 32,
        Binance = 64,
        NavasanAPI = 128
    }
}
