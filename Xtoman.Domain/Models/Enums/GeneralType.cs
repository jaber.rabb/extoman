﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum GeneralType : byte
    {
        [Display(Name = "پست")]
        Post = 0,
        [Display(Name = "خبر")]
        News = 1,
        [Display(Name = "محصول")]
        Product = 2,
    }
}
