﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum WalletApiType
    {
        [Display(Name = "هیچکدام")]
        None = 0,
        [Display(Name = "CoinPayments")]
        CoinPayments = 1,
        [Display(Name = "BitGo")]
        BitGo = 2,
        [Display(Name = "LocalBitcoins")]
        LocalBitcoins = 3,
        [Display(Name = "Binance")]
        Binance = 4,
        [Display(Name = "Kraken")]
        Kraken = 5,
        [Display(Name = "وندار")]
        Vandar = 6,
        [Display(Name = "Pay.ir")]
        PayIR = 7
    }
}
