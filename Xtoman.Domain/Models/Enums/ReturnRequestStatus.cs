﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum ReturnRequestStatus: byte
    {
        [Display(Name = "در حال انتظار")]
        Pending = 1,
        [Display(Name = "دریافت شده")]
        Received = 2,
        [Display(Name = "بازگشت تایید شد")]
        ReturnAuthorized = 4,
        [Display(Name = "محصول تعمیر شد")]
        ItemsRepaired = 8,
        [Display(Name = "وجه پرداختی محصول بازگشت داده شد")]
        ItemsRefunded = 16,
        [Display(Name = "درخواست رد شد")]
        RequestRejected = 32,
        [Display(Name = "کنسل شد")]
        Cancelled = 64,
    }
}
