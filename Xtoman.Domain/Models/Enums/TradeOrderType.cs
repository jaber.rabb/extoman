﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum TradeOrderType
    {
        [Display(Name = "قیمت مارکت")]
        Market,
        [Display(Name = "قیمت تعیین شده")]
        Limit
    }
}
