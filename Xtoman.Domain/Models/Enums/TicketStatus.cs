﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum TicketStatus : byte
    {
        [Display(Name = "در انتظار پاسخ", ShortName = "warning")]
        WaitingReply = 0,
        [Display(Name = "پاسخ داده شد", ShortName = "info")]
        Answered = 1,
        [Display(Name = "بسته", ShortName = "success")]
        Closed = 2,
        [Display(Name = "در انتظار پاسخ کاربر", ShortName = "warning")]
        WaitingUserReply = 4
    }
}
