﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum SpecificationType
    {
        [Display(Name = "متن")]
        Text,
        [Display(Name = "چک باکس")]
        CheckBox,
        [Display(Name = "لیست")]
        List
    }
}
