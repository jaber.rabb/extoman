﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum PaymentStatus : byte
    {
        [Display(Name = "پرداخت نشده", GroupName = "danger")]
        NotPaid = 0,

        [Display(Name = "در حال پرداخت", GroupName = "info")]
        InPayment = 1,

        [Display(Name = "پرداخت شده", GroupName = "success")]
        Paid = 2,

        [Display(Name = "استرداد", GroupName = "primary")]
        Refunded = 4,

        [Display(Name = "نا معلوم", GroupName = "default")]
        Voided = 8,

        [Display(Name = "انصراف", GroupName = "warning")]
        Canceled = 16,

        [Display(Name = "بازبینی", GroupName = "warning")]
        NotVerified = 32,

        [Display(Name = "استرداد وجه", GroupName = "primary")]
        MoneyRefunded = 64,

        [Display(Name = "شماره کارت احراز نشده", GroupName = "danger")]
        CardNotValid = 128
    }
}
