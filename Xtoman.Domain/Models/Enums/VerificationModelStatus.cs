﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum VerificationModelStatus
    {
        [Display(Name = "مدارک ارسال نشده", GroupName = "#337ab7")]
        IdentityNotSent,
        [Display(Name = "در انتظار تایید مدارک", GroupName = "#ff6633")]
        IdentityPending,
        [Display(Name = "مدارک تایید شده", GroupName = "#00a86b")]
        IdentityConfirmed,
        [Display(Name = "مدارک تایید نشده", GroupName = "#b43b2c")]
        IdentityNotConfirmed,

        [Display(Name = "تلفن وارد نشده", GroupName = "#337ab7")]
        TelephoneNotEntered,
        [Display(Name = "منتظر تماس تلفنی", GroupName = "#ff6633")]
        TelephonePending,
        [Display(Name = "تلفن تایید نشده", GroupName = "#b43b2c")]
        TelephoneNotConfirmed,
        [Display(Name = "تلفن تایید شده", GroupName = "#00a86b")]
        TelephoneConfirmed,

        [Display(Name = "موبایل وارد نشده", GroupName = "#337ab7")]
        PhoneNumberNotEntered,
        [Display(Name = "موبایل تایید نشده", GroupName = "#b43b2c")]
        PhoneNumberNotConfirmed,
        [Display(Name = "موبایل تایید شده", GroupName = "#00a86b")]
        PhoneNumberConfirmed,

        [Display(Name = "ایمیل وارد نشده", GroupName = "#337ab7")]
        EmailNotEntered,
        [Display(Name = "ایمیل تایید نشده", GroupName = "#b43b2c")]
        EmailNotConfirmed,
        [Display(Name = "ایمیل تایید شده", GroupName = "#00a86b")]
        EmailConfirmed,

        [Display(Name = "هیج شماره کارتی وارد نشده", GroupName = "#337ab7")]
        BankCardNotEntered,
        [Display(Name = "منتظر تایید کارت بانکی", GroupName = "#ff6633")]
        BankCardPending,
        [Display(Name = "کارت بانکی تایید نشده", GroupName = "#b43b2c")]
        BankCardNotConfirmed,
        [Display(Name = "کارت بانکی تایید شده", GroupName = "#00a86b")]
        BankCardConfirmed,

        [Display(Name = "تایید کامل", GroupName = "#00a86b")]
        Confirmed
    }
}
