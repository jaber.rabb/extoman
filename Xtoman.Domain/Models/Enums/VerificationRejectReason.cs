﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum VerificationRejectReason
    {
        [Display(Name = "سلفی نیست", Description = "مدارک باید در کنار صورت باشد و به صورت سلفی عکس گرفته شود")]
        NoSelfie,
        [Display(Name = "متن یادداشت نشده", Description = "عبارت جهت احراز هویت در سایت ایکس تومن روی کاغذ یادداشت نشده است")]
        NoLetter,
        [Display(Name = "نا هماهنگی نام با مدارک", Description = "نام و نام خانوادگی با مدارک همخوانی ندارد")]
        MismatchName,
        [Display(Name = "نا هماهنگی مدارک با کارت بانکی", Description = "نام و نام خانوادگی صاحب کارت بانکی با مدارک همخوانی ندارد")]
        MismatchCard,
        [Display(Name = "تصویر ناخوانا است", Description = "نوشته های تصویر خوانا نیست یا تار است")]
        Blur
    }
}
