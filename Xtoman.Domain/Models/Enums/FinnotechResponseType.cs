﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Models
{
    public enum FinnotechResponseType
    {
        [Display(Name = "دریافت موجودی")]
        FinnotechResponseBalance,
        [Display(Name = "دریافت اطلاعات کارت بانکی")]
        FinnotechResponseCardInfo,
        [Display(Name = "صحت سنجی کارت ملی")]
        FinnotechResponseIdVerification,
        [Display(Name = "گزارش وضعیت انتقال پایا")]
        FinnotechResponsePayaReport,
        [Display(Name = "انتقال وجه")]
        FinnotechResponseTransferTo,
        [Display(Name = "تطابق کد ملی با شماره موبایل")]
        FinnotechResponseMobileVerify,
        [Display(Name = "دریافت شماره شبا")]
        FinnotechResponseIBANDetail
    }
}
