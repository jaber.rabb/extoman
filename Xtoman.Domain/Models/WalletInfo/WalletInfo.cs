﻿namespace Xtoman.Domain.Models
{
    public class WalletInfo : BaseEntity
    {
        public string Name { get; set; }
        public string AltName { get; set; }
        
    }
}
