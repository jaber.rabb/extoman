﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class SupplierPhone : BaseEntity
    {
        public string Number { get; set; }
        public PhoneType PhoneType { get; set; }
        public int SupplierId { get; set; }
        public virtual Supplier Supplier { get; set; }
    }

    public class SupplierPhoneConfig : EntityTypeConfiguration<SupplierPhone>
    {
        public SupplierPhoneConfig()
        {
            HasRequired(p => p.Supplier).WithMany(p => p.Phones).HasForeignKey(p => p.SupplierId);
        }
    }
}
