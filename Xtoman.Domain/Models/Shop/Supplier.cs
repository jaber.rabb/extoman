﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Supplier : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }

        //Private properties
        private ICollection<SupplierAddress> _addresses;
        private ICollection<SupplierPhone> _phones;
        private ICollection<Product> _products;

        //Navigation properties
        public virtual ICollection<SupplierAddress> Addresses
        {
            get { return _addresses ?? (_addresses = new List<SupplierAddress>()); }
            protected set { _addresses = value; }
        }
        public virtual ICollection<SupplierPhone> Phones
        {
            get { return _phones ?? (_phones = new List<SupplierPhone>()); }
            protected set { _phones = value; }
        }
        public virtual ICollection<Product> Products
        {
            get { return _products ?? (_products = new List<Product>()); }
            protected set { _products = value; }
        }
    }

    public class SupplierConfig : EntityTypeConfiguration<Supplier>
    {
        public SupplierConfig()
        {
            HasMany(p => p.Addresses).WithRequired(p => p.Supplier).HasForeignKey(p => p.SupplierId).WillCascadeOnDelete(true);
            HasMany(p => p.Phones).WithRequired(p => p.Supplier).HasForeignKey(p => p.SupplierId).WillCascadeOnDelete(true);
        }
    }
}
