﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    [Serializable]
    public class CarrierPriceForCaching
    {
        public decimal MinWeight { get; set; }
        public decimal MaxWeight { get; set; }
        public decimal Price { get; set; }
        public long PostalCodeFrom { get; set; }
        public long PostalCodeTo { get; set; }
    }

    public class CarrierPrice : BaseEntity
    {
        public decimal MinWeight { get; set; }
        public decimal MaxWeight { get; set; }
        public decimal Price { get; set; }
        public long PostalCodeFrom { get; set; }
        public long PostalCodeTo { get; set; }

        public int CityId { get; set; }
        public int CarrierId { get; set; }

        //Navigation Properties
        public virtual City City { get; set; }
        public virtual Carrier Carrier { get; set; }
    }

    public class CarrierPriceConfig : EntityTypeConfiguration<CarrierPrice>
    {
        public CarrierPriceConfig()
        {
            HasRequired(p => p.City).WithMany(p => p.CarrierPrices).HasForeignKey(p => p.CityId);
            HasRequired(p => p.Carrier).WithMany(p => p.Prices).HasForeignKey(p => p.CarrierId);
        }
    }
}
