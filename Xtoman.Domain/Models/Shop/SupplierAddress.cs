﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    public class SupplierAddress : Address
    {
        public int SupplierId { get; set; }

        public virtual Supplier Supplier { get; set; }
    }

    public class SupplierAddressConfig : EntityTypeConfiguration<SupplierAddress>
    {
        public SupplierAddressConfig()
        {
            HasRequired(p => p.Supplier).WithMany(p => p.Addresses).HasForeignKey(p => p.SupplierId);
        }
    }
}
