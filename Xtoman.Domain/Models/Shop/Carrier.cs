﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class CarrierForCaching : IBaseCachingEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsFree { get; set; }
        public bool IsPayInPlace { get; set; }
        public string ArriveDays { get; set; }
        public string CityName { get; set; }
        private List<CarrierPriceForCaching> Prices { get; set; }
    }

    public class Carrier : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsFree { get; set; }
        public bool IsPayInPlace { get; set; }
        public string ArriveDays { get; set; }

        //Private properties
        private ICollection<CarrierPrice> _prices;
        private ICollection<ProductCarrier> _productCarriers;
        private ICollection<ProductOrderShipment> _orderShipments;

        //Navigation properties
        public virtual ICollection<ProductCarrier> ProductCarriers
        {
            get { return _productCarriers ?? (_productCarriers = new List<ProductCarrier>()); }
            protected set { _productCarriers = value; }
        }
        public virtual ICollection<CarrierPrice> Prices
        {
            get { return _prices ?? (_prices = new List<CarrierPrice>()); }
            protected set { _prices = value; }
        }
        public virtual ICollection<ProductOrderShipment> OrderShipments
        {
            get { return _orderShipments ?? (_orderShipments = new List<ProductOrderShipment>()); }
            set { _orderShipments = value; }
        }
    }

    public class CarrierConfig : EntityTypeConfiguration<Carrier>
    {
        public CarrierConfig()
        {
        }
    }
}
