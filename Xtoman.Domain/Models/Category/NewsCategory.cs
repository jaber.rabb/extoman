﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class NewsCategory : Category
    {
        private ICollection<News> _newses;
        //Navigation properties
        public virtual ICollection<News> Newses
        {
            get { return _newses ?? (_newses = new List<News>()); }
            protected set { _newses = value; }
        }
    }

    public class NewsCategoryConfig : EntityTypeConfiguration<NewsCategory>
    {
        public NewsCategoryConfig()
        {
        }
    }
}
