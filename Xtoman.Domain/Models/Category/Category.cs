﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Category : AuditEntity, ILocalizedEntity, IUrlHistoryEntity
    {
        [LocalizedProperty]
        public string Name { get; set; }
        [LocalizedProperty]
        public string Url { get; set; }
        [LocalizedProperty]
        public string Description { get; set; }
        [LocalizedProperty]
        public string MetaKeywords { get; set; }
        [LocalizedProperty]
        public string MetaDescription { get; set; }
        public int Order { get; set; }
        public string ColorHex { get; set; }
        public int? ParentCategoryId { get; set; } // For parent

        private ICollection<CategoryRate> _rates;
        public virtual ICollection<CategoryRate> Rates
        {
            get { return _rates ?? (_rates = new List<CategoryRate>()); }
            protected set { _rates = value; }
        }

        //Navigation property
        public virtual Category ParentCategory { get; set; } //Parent category
    }

    public class CategoryConfig : EntityTypeConfiguration<Category>
    {
        public CategoryConfig()
        {
            HasOptional(x => x.ParentCategory).WithMany().HasForeignKey(x => x.ParentCategoryId);
            HasMany(p => p.Rates).WithRequired(p => p.Category).HasForeignKey(p => p.CategoryId).WillCascadeOnDelete(true);
        }
    }
}
