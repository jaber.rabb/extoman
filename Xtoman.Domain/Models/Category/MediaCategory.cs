﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class MediaCategory : BaseEntity
    {
        public string Name { get; set; }

        //Private properties
        private ICollection<MediaCategoryMedia> _medias;

        //Navigation properties
        public virtual ICollection<MediaCategoryMedia> Medias
        {
            get { return _medias ?? (_medias = new List<MediaCategoryMedia>()); }
            protected set { _medias = value; }
        }
    }

    public class MediaCategoryConfig : EntityTypeConfiguration<MediaCategory>
    {
        public MediaCategoryConfig()
        {
        }
    }
}
