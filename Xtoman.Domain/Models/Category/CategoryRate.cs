﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class CategoryRate : BaseEntity
    {
        public string Name { get; set; }

        public int CategoryId { get; set; }

        // Private properties
        private ICollection<ProductCommentRate> _rateList;

        // Navigation properties
        public virtual Category Category { get; set; }
        public virtual ICollection<ProductCommentRate> RatesList
        {
            get { return _rateList ?? (_rateList = new List<ProductCommentRate>()); }
            protected set { _rateList = value; }
        }
    }

    public class ProductCategoryRateConfig : EntityTypeConfiguration<CategoryRate>
    {
        public ProductCategoryRateConfig()
        {
            HasRequired(p => p.Category).WithMany(p => p.Rates).HasForeignKey(p => p.CategoryId);
        }
    }
}
