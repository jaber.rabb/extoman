﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    [Serializable]
    public class ProductCategoryForCaching : IBaseCachingEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public int Order { get; set; }
        public string ColorHex { get; set; }
        public int? ParentCategoryId { get; set; } // For parent

        //Navigation property
        //public ProductCategoryForCaching ParentCategory { get; set; } //Parent category
    }

    public class ProductCategory : Category
    {
        public virtual new ProductCategory ParentCategory { get; set; } //Parent category

        // Private properties
        private ICollection<Product> _products;
        private ICollection<Specification> _specifications;
        private ICollection<UserProductCompare> _compares;

        // Navigation properties
        public virtual ICollection<Product> Products
        {
            get { return _products ?? (_products = new List<Product>()); }
            protected set { _products = value; }
        }
        public virtual ICollection<Specification> Specifications
        {
            get { return _specifications ?? (_specifications = new List<Specification>()); }
            protected set { _specifications = value; }
        }
        public virtual ICollection<UserProductCompare> ProductCompares
        {
            get { return _compares ?? (_compares = new List<UserProductCompare>()); }
            protected set { _compares = value; }
        }
    }

    public class ProductCategoryConfig : EntityTypeConfiguration<ProductCategory>
    {
        public ProductCategoryConfig()
        {
            HasMany(p => p.Specifications).WithMany(x => x.Categories);
            HasMany(p => p.ProductCompares).WithRequired(p => p.ProductCategory).HasForeignKey(p => p.CategoryId).WillCascadeOnDelete(true);
            HasOptional(x => x.ParentCategory).WithMany().HasForeignKey(x => x.ParentCategoryId);
        }
    }
}
