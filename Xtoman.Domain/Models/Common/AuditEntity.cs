﻿using System;

namespace Xtoman.Domain.Models
{
    public abstract class AuditEntity : BaseEntity
    {
        public AuditEntity()
        {
            IsDelete = false;
        }

        public int? InsertUserId { get; set; }
        public int? UpdateUserId { get; set; }
        public int? DeleteUserId { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? DeleteDate { get; set; }
        public string InsertUserIpAddress { get; set; }
        public string UpdateUserIpAddress { get; set; }
        public string DeleteUserIpAddress { get; set; }
        public bool IsDelete { get; set; }

        public virtual AppUser InsertUser { get; set; }
        public virtual AppUser UpdateUser { get; set; }
        public virtual AppUser DeleteUser { get; set; }
    }
}
