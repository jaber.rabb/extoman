﻿namespace Xtoman.Domain.Models
{
    public interface IBaseEntity
    {
        /// <summary>
        /// Gets or sets the entity identifier
        /// </summary>
        int Id { get; set; }
    }
}
