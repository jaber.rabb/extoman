﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    [Serializable]
    public class UrlHistoryForCaching : IBaseCachingEntity
    {
        public int Id { get; set; }
        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public int? LanguageId { get; set; }
        public string Url { get; set; }
    }

    public class UrlHistory : BaseEntity
    {
        public string Url { get; set; }
        public string EntityName { get; set; }
        public int EntityId { get; set; }
        public int? LanguageId { get; set; }
        public int? InsertUserId { get; set; }
        public DateTime? InsertDate { get; set; }

        public virtual Language Language { get; set; }
        public virtual AppUser InsertUser { get; set; }
    }

    public class UrlHistoryConfig : EntityTypeConfiguration<UrlHistory>
    {
        public UrlHistoryConfig()
        {
            Property(x => x.EntityName).HasMaxLength(50).IsRequired();
            Property(x => x.Url).HasMaxLength(300).IsRequired();

            HasRequired(x => x.Language).WithMany().HasForeignKey(x => x.LanguageId);
            HasRequired(x => x.InsertUser).WithMany().HasForeignKey(x => x.InsertUserId);
        }
    }
}
