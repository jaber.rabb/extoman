﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Comment : AuditEntity
    {
        public string Text { get; set; }
        public int? ParentCommentId { get; set; }
        public byte Rating { get; set; }
        public int? ConfirmUserId { get; set; }
        public bool? IsConfirm { get; set; }
        public DateTime? ConfirmDate { get; set; }
        public bool IsAutenticated { get; set; }
        public string AnonymousId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }

        //Navigation properties
        public virtual Comment Parent { get; set; }
    }

    public class CommentConfig : EntityTypeConfiguration<Comment>
    {
        public CommentConfig()
        {
            HasOptional(x => x.Parent).WithMany().HasForeignKey(x => x.ParentCommentId);
        }
    }
}
