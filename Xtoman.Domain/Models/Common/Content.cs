﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Content : AuditEntity, IUrlHistoryEntity, ILocalizedEntity
    {
        public Content()
        {
            Enabled = true;
        }
        [LocalizedProperty]
        public string Title { get; set; }
        [LocalizedProperty]
        public string Summary { get; set; }
        [LocalizedProperty]
        public string Text { get; set; }
        public DateTime? PublishDate { get; set; }
        [LocalizedProperty]
        public string Url { get; set; }

        [LocalizedProperty]
        public string MetaDescription { get; set; }
        [LocalizedProperty]
        public string MetaKeywords { get; set; }
        [LocalizedProperty]
        public string FocusKeyword { get; set; }

        public bool AllowComments { get; set; }
        public int VisitCount { get; set; }

        public bool Enabled { get; set; }
    }

    public class ContentConfig : EntityTypeConfiguration<Content>
    {
        public ContentConfig()
        {
            Property(x => x.Title).HasMaxLength(200).IsRequired();
            Property(x => x.Url).HasMaxLength(300).IsRequired();
            Property(x => x.Summary).HasMaxLength(1000);
            Property(x => x.MetaKeywords).HasMaxLength(300);
            Property(x => x.MetaDescription).HasMaxLength(500);
            Property(x => x.FocusKeyword).HasMaxLength(50);
        }
    }
}
