﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    public abstract partial class BaseEntity : IBaseEntity
    {
        public int Id { get; set; }
    }
}
