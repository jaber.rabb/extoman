﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public partial class Address : BaseEntity
    {
        public string AddressName { get; set; }
        public string OwnerName { get; set; }
        public string Mobile { get; set; }
        public string Tel { get; set; }
        public string PostalAddress { get; set; }
        public string PostalCode { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }

        //Navigation properties
        public int CityId { get; set; }
        public virtual City City { get; set; }
    }

    public class AddressConfig : EntityTypeConfiguration<Address>
    {
        public AddressConfig()
        {
            Property(p => p.Lat).HasPrecision(18, 15);
            Property(p => p.Lng).HasPrecision(18, 15);
            HasRequired(x => x.City).WithMany(x => x.Addresses).HasForeignKey(x => x.CityId);
        }
    }
}
