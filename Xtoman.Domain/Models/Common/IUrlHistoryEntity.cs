﻿namespace Xtoman.Domain.Models
{
    public interface IUrlHistoryEntity
    {
        string Url { get; set; }
    }
}
