﻿namespace Xtoman.Domain.Models
{
    public interface IBaseCachingEntity
    {
        int Id { get; set; }
    }
}
