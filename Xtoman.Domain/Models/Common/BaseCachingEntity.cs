﻿namespace Xtoman.Domain.Models
{
    public abstract partial class BaseCachingEntity : IBaseCachingEntity
    {
        public int Id { get; set; }
    }
}
