﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductPrice : BaseEntity
    {
        public ProductPrice()
        {
            PriceDate = DateTime.UtcNow;
        }

        public DateTime PriceDate { get; set; }
        public decimal Amount { get; set; }
        public decimal SuppliedPrice { get; set; }
        public int ProductId { get; set; }
        public int? ProductAttributeId { get; set; }

        //Navigation properties
        public virtual Product Product { get; set; }
        public virtual ProductAttribute ProductAttribute { get; set; }
    }

    public class PriceConfig : EntityTypeConfiguration<ProductPrice>
    {
        public PriceConfig()
        {
            HasRequired(p => p.Product).WithMany(p => p.Prices).HasForeignKey(p => p.ProductId);
            HasOptional(p => p.ProductAttribute).WithMany(p => p.PricesDifference).HasForeignKey(p => p.ProductAttributeId);
        }
    }
}
