﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductAttributeCombination : BaseEntity
    {
        public int ProductAttributeId { get; set; }
        public int ShopAttributeId { get; set; }

        public virtual ProductAttribute ProductAttribute { get; set; }
        public virtual ShopAttribute ShopAttribute { get; set; }
    }

    public class ProductAttributeCombinationConfig : EntityTypeConfiguration<ProductAttributeCombination>
    {
        public ProductAttributeCombinationConfig()
        {
            HasRequired(p => p.ProductAttribute).WithMany(p => p.Combinations).HasForeignKey(p => p.ProductAttributeId);
            HasRequired(p => p.ShopAttribute).WithMany(p => p.ProductAttributeCombinations).HasForeignKey(p => p.ShopAttributeId);
        }
    }
}
