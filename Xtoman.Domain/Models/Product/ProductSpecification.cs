﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductSpecification : BaseEntity
    {
        public string Value { get; set; }
        public int ProductId { get; set; }
        public int SpecificationId { get; set; }

        //Navigation properties
        public virtual Product Product { get; set; }
        public virtual Specification Specification { get; set; }
    }

    public class ProductSpecificationConfig : EntityTypeConfiguration<ProductSpecification>
    {
        public ProductSpecificationConfig()
        {
            HasRequired(p => p.Product).WithMany(p => p.Specifications).HasForeignKey(p => p.ProductId);
            HasRequired(p => p.Specification).WithMany(p => p.Products).HasForeignKey(p => p.SpecificationId);
        }
    }
}
