﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class SpecificationDefault : BaseEntity
    {
        public string Text { get; set; }
        public int SpecificationId { get; set; }

        //Navigation properties
        public virtual Specification Specification { get; set; }
    }

    public class SpecificationDefaultConfig : EntityTypeConfiguration<SpecificationDefault>
    {
        public SpecificationDefaultConfig()
        {
            HasRequired(p => p.Specification).WithMany(p => p.Defaults).HasForeignKey(p => p.SpecificationId);
        }
    }
}
