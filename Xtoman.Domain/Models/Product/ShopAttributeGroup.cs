﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ShopAttributeGroup : BaseEntity
    {
        public string Name { get; set; }
        public bool IsColor { get; set; }
        public AttributeGroupType Type { get; set; }
        public int Position { get; set; }

        //Private properties
        private ICollection<ShopAttribute> _shopAttributes;
        //Navigation properties
        public virtual ICollection<ShopAttribute> ShopAttributes
        {
            get { return _shopAttributes ?? (_shopAttributes = new List<ShopAttribute>()); }
            protected set { _shopAttributes = value; }
        }
    }

    public class ShopAttributeGroupConfig : EntityTypeConfiguration<ShopAttributeGroup>
    {
        public ShopAttributeGroupConfig()
        {

        }
    }
}
