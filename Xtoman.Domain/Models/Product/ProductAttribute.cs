﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductAttribute : BaseEntity
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public int Width { get; set; } // cm difference - 0 if is default
        public int Height { get; set; } // cm difference - 0 if is default
        public int Depth { get; set; } // cm difference - 0 if is default
        public int Weight { get; set; } // gram difference - 0 if is default
        public int InStock { get; set; }
        public bool IsDefault { get; set; }
        public int? MediaId { get; set; }
        public string Url { get; set; }

        //Private properties
        private List<ProductPrice> _pricesDifference;
        private List<ProductAttributeCombination> _combinations;
        private ICollection<UserProductNotify> _notifies;
        private ICollection<ProductOrderItem> _orderItems;
        private ICollection<CartItem> _cartProducts;

        //Navigation properties
        public virtual Product Product { get; set; }
        public virtual Media Media { get; set; }
        public virtual List<ProductPrice> PricesDifference
        {
            get { return _pricesDifference ?? (_pricesDifference = new List<ProductPrice>()); }
            protected set { _pricesDifference = value; }
        } // 0 row(null) if is default
        public virtual List<ProductAttributeCombination> Combinations {
            get { return _combinations ?? (_combinations = new List<ProductAttributeCombination>()); }
            protected set { _combinations = value; }
        }
        public virtual ICollection<UserProductNotify> Notifies
        {
            get { return _notifies ?? (_notifies = new List<UserProductNotify>()); }
            protected set { _notifies = value; }
        }
        public virtual ICollection<ProductOrderItem> OrderItems
        {
            get { return _orderItems ?? (_orderItems = new List<ProductOrderItem>()); }
            protected set { _orderItems = value; }
        }
        public virtual ICollection<CartItem> CartProducts
        {
            get { return _cartProducts ?? (_cartProducts = new List<CartItem>()); }
            protected set { _cartProducts = value; }
        }
    }

    public class ProductAttributeConfig : EntityTypeConfiguration<ProductAttribute>
    {
        public ProductAttributeConfig()
        {
            HasOptional(p => p.Media).WithMany().HasForeignKey(p => p.MediaId);
            HasRequired(p => p.Product).WithMany(p => p.Attributes).HasForeignKey(p => p.ProductId);
        }
    }
}
