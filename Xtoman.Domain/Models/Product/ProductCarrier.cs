﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductCarrier : BaseEntity
    {
        public int ProductId { get; set; }
        public int CarrierId { get; set; }

        //Navigation properties
        public virtual Carrier Carrier { get; set; }
        public virtual Product Product { get; set; }
    }

    public class ProductCarrierConfig : EntityTypeConfiguration<ProductCarrier>
    {
        public ProductCarrierConfig()
        {
            HasRequired(p => p.Product).WithMany(p => p.Carriers).HasForeignKey(p => p.ProductId);
            HasRequired(p => p.Carrier).WithMany(p => p.ProductCarriers).HasForeignKey(p => p.CarrierId);
        }
    }
}
