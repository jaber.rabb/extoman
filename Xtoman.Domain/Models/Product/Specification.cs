﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Specification : BaseEntity
    {
        public string Name { get; set; }
        public SpecificationType Type { get; set; }
        public SpecificationSearchFilter Filter { get; set; }

        public int GroupId { get; set; }

        //Private properties
        private ICollection<ProductSpecification> _products;
        private ICollection<SpecificationDefault> _defaults;
        private ICollection<ProductCategory> _categories;

        //Navigation properties
        public SpecificationGroup Group { get; set; }
        public virtual ICollection<ProductSpecification> Products
        {
            get { return _products ?? (_products = new List<ProductSpecification>()); }
            protected set { _products = value; }
        }
        public virtual ICollection<SpecificationDefault> Defaults
        {
            get { return _defaults ?? (_defaults = new List<SpecificationDefault>()); }
            protected set { _defaults = value; }
        }
        public virtual ICollection<ProductCategory> Categories
        {
            get { return _categories ?? (_categories = new List<ProductCategory>()); }
            protected set { _categories = value; }
        }
    }

    public class SpecificationConfig : EntityTypeConfiguration<Specification>
    {
        public SpecificationConfig()
        {
            HasMany(p => p.Categories).WithMany(p => p.Specifications);
            HasRequired(p => p.Group).WithMany(p => p.Specifications).HasForeignKey(p => p.GroupId);
            HasMany(p => p.Defaults).WithRequired(p => p.Specification).HasForeignKey(p => p.SpecificationId).WillCascadeOnDelete(true);
            HasMany(p => p.Products).WithRequired(p => p.Specification).HasForeignKey(p => p.SpecificationId).WillCascadeOnDelete(true);
        }
    }
}
