﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class RelatedProduct : BaseEntity
    {
        public int ProductId1 { get; set; }
        public int ProductId2 { get; set; }
        public int DisplayOrder { get; set; }

        public virtual Product Product1 { get; set; }
        public virtual Product Product2 { get; set; }
    }

    public class RelatedProductConfig : EntityTypeConfiguration<RelatedProduct>
    {
        public RelatedProductConfig()
        {
            HasRequired(p => p.Product1).WithMany().HasForeignKey(p => p.ProductId1);
            HasRequired(p => p.Product2).WithMany().HasForeignKey(p => p.ProductId2);
        }
    }
}
