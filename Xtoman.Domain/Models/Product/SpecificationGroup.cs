﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class SpecificationGroup : BaseEntity
    {
        public string Name { get; set; }

        //Private properties
        private ICollection<Specification> _specifications;

        //Navigation properties
        public virtual ICollection<Specification> Specifications
        {
            get { return _specifications ?? (_specifications = new List<Specification>()); }
            protected set { _specifications = value; }
        }
    }

    public class SpecificationGroupConfig : EntityTypeConfiguration<SpecificationGroup>
    {
        public SpecificationGroupConfig()
        {
        }
    }
}
