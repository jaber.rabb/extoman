﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductGift : BaseEntity
    {
        public int GiftProductId { get; set; }

        public int ProductId { get; set; }
        //Navigation property
        public virtual Product Product { get; set; }
        public virtual Product GiftProduct { get; set; }
    }

    public class ProductGiftConfig : EntityTypeConfiguration<ProductGift>
    {
        public ProductGiftConfig()
        {
            HasRequired(p => p.Product).WithMany(p => p.Gifts).HasForeignKey(p => p.ProductId);
            HasRequired(p => p.GiftProduct);
        }
    }
}
