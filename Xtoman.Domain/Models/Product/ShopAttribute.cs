﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ShopAttribute : BaseEntity
    {
        public string ColorHex { get; set; }
        public string Name { get; set; }
        //public int Position { get; set; }
        public int GroupId { get; set; }

        //Private properties
        public virtual ShopAttributeGroup Group { get; set; }
        private ICollection<ProductAttributeCombination> _productAttributeCombinations;

        //Navigation properties
        public virtual ICollection<ProductAttributeCombination> ProductAttributeCombinations
        {
            get { return _productAttributeCombinations ?? (_productAttributeCombinations = new List<ProductAttributeCombination>()); }
            protected set { _productAttributeCombinations = value; }
        }
    }

    public class ShopAttributeConfig : EntityTypeConfiguration<ShopAttribute>
    {
        public ShopAttributeConfig()
        {
            HasRequired(p => p.Group).WithMany(p => p.ShopAttributes).HasForeignKey(p => p.GroupId);
        }
    }
}
