﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Brand : AuditEntity, ILocalizedEntity, IUrlHistoryEntity
    {
        public string Name { get; set; }
        public string AlternativeName { get; set; }
        public string Country { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public int? MediaId { get; set; }

        //Private properties
        private ICollection<Product> _products;

        //Navigation properties
        public virtual Media Media { get; set; }
        public virtual ICollection<Product> Products
        {
            get { return _products ?? (_products = new List<Product>()); }
            protected set { _products = value; }
        }
    }

    public class BrandConfig : EntityTypeConfiguration<Brand>
    {
        public BrandConfig()
        {
            HasOptional(p => p.Media).WithMany().HasForeignKey(p => p.MediaId);
        }
    }
}
