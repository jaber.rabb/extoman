﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductQuanityDiscount : BaseEntity
    {
        public int MinQuanity { get; set; }
        public int DiscountPercent { get; set; }

        public int ProductId { get; set; }

        //Navigation properties
        public virtual Product Product { get; set; }
    }
    
    public class ProductQuanityDiscountConfig : EntityTypeConfiguration<ProductQuanityDiscount>
    {
        public ProductQuanityDiscountConfig()
        {
            HasRequired(p => p.Product).WithMany(p => p.QuanityDiscounts).HasForeignKey(p => p.ProductId);
        }
    }
}
