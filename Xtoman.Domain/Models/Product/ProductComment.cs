﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductComment : BaseEntity
    {
        public ProductComment()
        {
            Date = DateTime.UtcNow;
        }
        public int ParentId { get; set; }
        public string Text { get; set; }
        public float Rate { get; set; }
        public bool IsBuyer { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public DateTime Date { get; set; }

        public int UserId { get; set; }
        public int ProductId { get; set; }

        // Navigation properties
        public virtual AppUser User { get; set; }
        public virtual Product Product { get; set; }

        private ICollection<ProductCommentRate> _rateList;
        public virtual ICollection<ProductCommentRate> RatesList
        {
            get { return _rateList ?? (_rateList = new List<ProductCommentRate>()); }
            protected set { _rateList = value; }
        }
    }

    public class ProductCommentConfig : EntityTypeConfiguration<ProductComment>
    {
        public ProductCommentConfig()
        {
            HasRequired(x => x.User).WithMany(x => x.ProductComments).HasForeignKey(x => x.UserId);
            HasRequired(x => x.Product).WithMany(x => x.Comments).HasForeignKey(x => x.ProductId);
            HasMany(p => p.RatesList).WithRequired(p => p.ProductComment).HasForeignKey(p => p.ProductCommentId).WillCascadeOnDelete(true);
        }
    }
}