﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductCommentRate : BaseEntity
    {
        public int RateValue { get; set; }

        public int ProductCommentId { get; set; }
        public int CategoryRateId { get; set; }

        //Navigation properties
        public virtual ProductComment ProductComment { get; set; }
        public virtual CategoryRate CategoryRate { get; set; }
    }

    public class ProductCommentRateConfig : EntityTypeConfiguration<ProductCommentRate>
    {
        public ProductCommentRateConfig()
        {
            HasRequired(x => x.ProductComment).WithMany(x => x.RatesList).HasForeignKey(x => x.ProductCommentId);
            HasRequired(x => x.CategoryRate).WithMany(x => x.RatesList).HasForeignKey(x => x.CategoryRateId);
        }
    }
}
