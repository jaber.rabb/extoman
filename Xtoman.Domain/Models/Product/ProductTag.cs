﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductTag : BaseEntity
    {
        public string Name { get; set; }
        public string Url { get; set; }

        //Private properties
        private ICollection<Product> _products;

        //Navigation properties
        public ICollection<Product> Products
        {
            get { return _products ?? (_products = new List<Product>()); }
            protected set { _products = value; }
        }
    }
    
    public class ProductTagConfig : EntityTypeConfiguration<ProductTag>
    {
        public ProductTagConfig()
        {
            HasMany(x => x.Products).WithMany(x => x.Tags);
        }
    }
}
