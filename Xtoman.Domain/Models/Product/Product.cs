﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Product : AuditEntity, IUrlHistoryEntity, ILocalizedEntity
    {
        [LocalizedProperty]
        public string Name { get; set; }
        public string AlternateName { get; set; }
        [LocalizedProperty]
        public string Url { get; set; }
        [LocalizedProperty]
        public string Summary { get; set; }
        [LocalizedProperty]
        public string Description { get; set; }
        [LocalizedProperty]
        public string MetaDescription { get; set; }
        [LocalizedProperty]
        public string MetaKeywords { get; set; }
        [LocalizedProperty]
        public string FocusKeyword { get; set; }
        public int VisitCount { get; set; }
        public float Rate { get; set; }
        public DateTime? PublishDate { get; set; }
        public DateTime? ReleaseDate { get; set; } // Coming soon products
        public int Width { get; set; } // cm
        public int Height { get; set; } // cm
        public int Depth { get; set; } // cm
        public int Weight { get; set; } // gram
        public int InStock { get; set; }
        public int? MediaId { get; set; } // media id
        public int DiscountOffPercent { get; set; }
        public bool ProductionStopped { get; set; }
        public string Waranty { get; set; }

        public int BrandId { get; set; }

        //Private properties
        private ICollection<ProductPrice> _prices;
        private ICollection<Media> _medias;
        private ICollection<ProductAttribute> _attributes;
        private ICollection<ProductCategory> _categories;
        private ICollection<ProductTag> _tags;
        private ICollection<ProductSpecification> _specifications;
        private ICollection<ProductSpecialDiscount> _specialDiscounts;
        private ICollection<ProductComment> _comments;
        private ICollection<AppUser> _favorites;
        private ICollection<ProductGift> _gifts;
        private ICollection<Supplier> _suppliers;
        private ICollection<ProductCarrier> _carriers;
        private ICollection<UserProductVisit> _visits;
        private ICollection<UserProductNotify> _notifies;
        private ICollection<UserProductCompare> _compares;
        private ICollection<RelatedProduct> _relatedProducts;
        private ICollection<CartItem> _cartProducts;
        private ICollection<ProductQuanityDiscount> _quanityDiscounts;
        private ICollection<ProductOrderItem> _orderItems;

        //Navigation properties
        public virtual Brand Brand { get; set; }
        public virtual Media Media { get; set; }
        public virtual ICollection<ProductPrice> Prices
        {
            get { return _prices ?? (_prices = new List<ProductPrice>()); }
            protected set { _prices = value; }
        }
        public virtual ICollection<Media> Medias
        {
            get { return _medias ?? (_medias = new List<Media>()); }
            protected set { _medias = value; }
        }
        public virtual ICollection<ProductAttribute> Attributes
        {
            get { return _attributes ?? (_attributes = new List<ProductAttribute>()); }
            protected set { _attributes = value; }
        } // 0 row (null) if no attribute
        public virtual ICollection<ProductCategory> Categories
        {
            get { return _categories ?? (_categories = new List<ProductCategory>()); }
            protected set { _categories = value; }
        }
        public virtual ICollection<ProductTag> Tags
        {
            get { return _tags ?? (_tags = new List<ProductTag>()); }
            protected set { _tags = value; }
        }
        public virtual ICollection<ProductSpecification> Specifications
        {
            get { return _specifications ?? (_specifications = new List<ProductSpecification>()); }
            protected set { _specifications = value; }
        }
        public virtual ICollection<ProductSpecialDiscount> SpecialDiscounts
        {
            get { return _specialDiscounts ?? (_specialDiscounts = new List<ProductSpecialDiscount>()); }
            protected set { _specialDiscounts = value; }
        }
        public virtual ICollection<ProductComment> Comments
        {
            get { return _comments ?? (_comments = new List<ProductComment>()); }
            protected set { _comments = value; }
        }
        public virtual ICollection<AppUser> UserFavorites
        {
            get { return _favorites ?? (_favorites = new List<AppUser>()); }
            protected set { _favorites = value; }
        }
        public virtual ICollection<ProductGift> Gifts
        {
            get { return _gifts ?? (_gifts = new List<ProductGift>()); }
            protected set { _gifts = value; }
        }
        public virtual ICollection<Supplier> Suppliers
        {
            get { return _suppliers ?? (_suppliers = new List<Supplier>()); }
            protected set { _suppliers = value; }
        }
        public virtual ICollection<ProductCarrier> Carriers
        {
            get { return _carriers ?? (_carriers = new List<ProductCarrier>()); }
            protected set { _carriers = value; }
        }
        public virtual ICollection<UserProductVisit> Visits
        {
            get { return _visits ?? (_visits = new List<UserProductVisit>()); }
            protected set { _visits = value; }
        }
        public virtual ICollection<UserProductNotify> Notifies
        {
            get { return _notifies ?? (_notifies = new List<UserProductNotify>()); }
            protected set { _notifies = value; }
        }
        public virtual ICollection<UserProductCompare> ProductCompares
        {
            get { return _compares ?? (_compares = new List<UserProductCompare>()); }
            protected set { _compares = value; }
        }
        public virtual ICollection<RelatedProduct> RelatedProducts
        {
            get { return _relatedProducts ?? (_relatedProducts = new List<RelatedProduct>()); }
            protected set { _relatedProducts = value; }
        }
        public virtual ICollection<CartItem> CartProducts
        {
            get { return _cartProducts ?? (_cartProducts = new List<CartItem>()); }
            protected set { _cartProducts = value; }
        }
        public virtual ICollection<ProductQuanityDiscount> QuanityDiscounts
        {
            get { return _quanityDiscounts ?? (_quanityDiscounts = new List<ProductQuanityDiscount>()); }
            protected set { _quanityDiscounts = value; }
        }
        public virtual ICollection<ProductOrderItem> OrderItems
        {
            get { return _orderItems ?? (_orderItems = new List<ProductOrderItem>()); }
            protected set { _orderItems = value; }
        }
    }

    public class ProductConfig : EntityTypeConfiguration<Product>
    {
        public ProductConfig()
        {
            Property(x => x.Name).IsRequired();
            Property(x => x.Url).IsRequired();
            HasMany(p => p.Categories).WithMany(p => p.Products);
            HasMany(x => x.Medias).WithMany(x => x.Products);
            HasMany(x => x.Tags).WithMany(x => x.Products);
            HasMany(p => p.UserFavorites).WithMany(p => p.FavoriteProducts);
            HasRequired(p => p.Brand).WithMany(p => p.Products).HasForeignKey(p => p.BrandId);
            HasOptional(p => p.Media).WithMany().HasForeignKey(p => p.MediaId);
            HasMany(p => p.Attributes).WithRequired(p => p.Product).WillCascadeOnDelete(true);
            HasMany(p => p.Prices).WithRequired(p => p.Product).WillCascadeOnDelete(true);
            HasMany(p => p.Specifications).WithRequired(p => p.Product).WillCascadeOnDelete(true);
            HasMany(p => p.SpecialDiscounts).WithRequired(p => p.Product).WillCascadeOnDelete(true);
            HasMany(p => p.Visits).WithRequired(p => p.Product).WillCascadeOnDelete(true);
            HasMany(p => p.Notifies).WithRequired(p => p.Product).WillCascadeOnDelete(true);
            HasMany(p => p.Gifts).WithRequired(p => p.Product).WillCascadeOnDelete(true);
            HasMany(p => p.Comments).WithRequired(p => p.Product).WillCascadeOnDelete(true);
            HasMany(p => p.Carriers).WithRequired(p => p.Product).WillCascadeOnDelete(true);
            HasMany(p => p.CartProducts).WithRequired(p => p.Product).WillCascadeOnDelete(true);
            HasMany(p => p.ProductCompares).WithRequired(p => p.Product).WillCascadeOnDelete(true);
            HasMany(p => p.QuanityDiscounts).WithRequired(p => p.Product).WillCascadeOnDelete(true);
            HasMany(p => p.OrderItems).WithRequired(p => p.Product).WillCascadeOnDelete(true);
        }
    }
}
