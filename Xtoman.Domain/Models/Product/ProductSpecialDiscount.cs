﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ProductSpecialDiscount : BaseEntity
    {
        public int DiscountPercent { get; set; }
        public DateTime DiscountEndDate { get; set; }
        public DateTime DiscountStartDate { get; set; }

        public int ProductId { get; set; }

        //Navigation properties
        public virtual Product Product { get; set; }
    }

    public class ProductSpecialDiscountConfig : EntityTypeConfiguration<ProductSpecialDiscount>
    {
        public ProductSpecialDiscountConfig()
        {
            HasRequired(p => p.Product).WithMany(p => p.SpecialDiscounts).HasForeignKey(p => p.ProductId);
        }
    }
}
