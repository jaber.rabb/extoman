﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    [Serializable]
    public class CommissionForCache : BaseCachingEntity
    {
        public string Name { get; set; }
        public decimal MinValue { get; set; }
        public decimal Percent { get; set; }
    }
    public class Commission : DiscountCommission
    {
    }

    public class CommissionConfig : EntityTypeConfiguration<Commission>
    {
        public CommissionConfig()
        {

        }
    }
}
