﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    [Serializable]
    public class DiscountForCache : BaseCachingEntity
    {
        public string Name { get; set; }
        public decimal MinValue { get; set; }
        public decimal Percent { get; set; }
    }
    public class Discount : DiscountCommission
    {
    }

    public class DiscountConfig : EntityTypeConfiguration<Discount>
    {
        public DiscountConfig()
        {

        }
    }
}
