﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class UserWithdraw : BaseEntity
    {
        public UserWithdraw()
        {
            Date = DateTime.UtcNow;
        }
        public decimal Value { get; set; }
        public string ReceiveAddress { get; set; }
        public WithdrawStatus Status { get; set; }
        public DateTime Date { get; set; }

        public int UserId { get; set; }
        public virtual AppUser User { get; set; }
    }

    public class UserWithdrawConfig : EntityTypeConfiguration<UserWithdraw>
    {
        public UserWithdrawConfig()
        {
            HasRequired(p => p.User).WithMany(p => p.Withdraws).HasForeignKey(p => p.UserId);
        }
    }
}
