﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class UserIncome : BaseEntity
    {
        public UserIncome()
        {
            Date = DateTime.UtcNow;
        }
        public int Value { get; set; } // Value must be in toman
        public DateTime Date { get; set; }

        public int UserId { get; set; }
        public virtual AppUser User { get; set; }

        public Order Order { get; set; }
    }

    public class UserIncomeConfig : EntityTypeConfiguration<UserIncome>
    {
        public UserIncomeConfig()
        {
            HasRequired(p => p.User).WithMany(p => p.Incomes).HasForeignKey(p => p.UserId);
            HasRequired(p => p.Order).WithOptional(p => p.UserIncome);
        }
    }
}
