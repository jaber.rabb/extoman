﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    [Serializable]
    public class DiscountCommissionForCache : BaseCachingEntity
    {
        public string Name { get; set; }
        public decimal MinValue { get; set; }
        public decimal Percent { get; set; }
    }
    public class DiscountCommission : BaseEntity
    {
        public string Name { get; set; }
        /// <summary>
        /// <para>Value in toman for Discount</para> 
        /// or
        /// <para>Value of Income in BTC for Referral commission</para>
        /// </summary>
        public decimal MinValue { get; set; }
        public decimal Percent { get; set; }
    }

    public class DiscountCommissionConfig : EntityTypeConfiguration<DiscountCommission>
    {
        public DiscountCommissionConfig()
        {
        }
    }
}