﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class QueuedMessage : BaseEntity
    {
        public new long Id { get; set; }
        public string To { get; set; }
        public string ToName { get; set; }
        public string Text { get; set; }
        public string Subject { get; set; }
        public QueuedMessageType Type { get; set; }
        public bool IsSent { get; set; }
        public byte TryCount { get; set; }

        public int? EmailAccountId { get; set; }
        public virtual EmailAccount EmailAccount { get; set; }
    }

    public class QueuedMessageConfig : EntityTypeConfiguration<QueuedMessage>
    {
        public QueuedMessageConfig()
        {
            HasOptional(p => p.EmailAccount).WithMany().HasForeignKey(p => p.EmailAccountId);
        }
    }
}
