﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    [Serializable]
    public class NewsForCaching : BaseCachingEntity
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public bool Emergency { get; set; }
        public string NewsCategoryName { get; set; }
        public DateTime? PublishDate { get; set; }
        public string Url { get; set; }
    }
    public class News : Content
    {
        public bool Emergency { get; set; }
        public int NewsCategoryId { get; set; }
        public virtual NewsCategory Category { get; set; }
    }

    public class NewsConfig : EntityTypeConfiguration<News>
    {
        public NewsConfig()
        {
            HasRequired(x => x.Category).WithMany(x => x.Newses).HasForeignKey(x => x.NewsCategoryId);
        }
    }
}
