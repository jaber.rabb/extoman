﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class SupportTicketReply : AuditEntity
    {
        public string Text { get; set; }
        public bool IsSupportReply { get; set; }

        public int SupportTicketId { get; set; }
        public virtual SupportTicket SupportTicket { get; set; }
    }

    public class SupportTicketReplyConfig : EntityTypeConfiguration<SupportTicketReply>
    {
        public SupportTicketReplyConfig()
        {
            HasRequired(x => x.SupportTicket).WithMany(x => x.Replies).HasForeignKey(x => x.SupportTicketId);
        }
    }
}