﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    public class Page : Content
    {
        public PageType PageType { get; set; }
    }

    public class PageConfig : EntityTypeConfiguration<Page>
    {
        public PageConfig()
        {
        }
    }
}
