﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Badge : AuditEntity, ILocalizedEntity
    {
        [LocalizedProperty]
        public string Title { get; set; }
        public int XP { get; set; }
        [LocalizedProperty]
        public string Description { get; set; }
        public int MediaId { get; set; }

        private ICollection<UserBadge> _userBadges;

        //Navigation properties
        public virtual ICollection<UserBadge> UserBadges
        {
            get { return _userBadges ?? (_userBadges = new List<UserBadge>()); }
            protected set { _userBadges = value; }
        }
    }

    public class BadgeConfig : EntityTypeConfiguration<Badge>
    {
        public BadgeConfig()
        {
        }
    }
}
