﻿namespace Xtoman.Domain.Models
{
    using System;
    using System.Data.Entity.ModelConfiguration;

    [Serializable]
    public class FAQCached : BaseCachingEntity
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public string GroupName { get; set; }
        public string Url { get; set; }
    }

    public class FAQ : BaseEntity, ILocalizedEntity
    {
        [LocalizedProperty]
        public string Question { get; set; }
        [LocalizedProperty]
        public string Answer { get; set; }
        [LocalizedProperty]
        public string GroupName { get; set; }
        public string Url { get; set; }
    }

    public class FAQConfig : EntityTypeConfiguration<FAQ>
    {
        public FAQConfig()
        {

        }
    }
}
