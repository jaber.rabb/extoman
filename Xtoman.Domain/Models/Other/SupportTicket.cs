﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class SupportTicket : AuditEntity
    {
        public SupportTicket()
        {
            Status = TicketStatus.WaitingReply;
            LastDate = DateTime.UtcNow;
        }
        public string Subject { get; set; }
        public string Text { get; set; }
        public string EmailOrPhoneNumber { get; set; }
        public string UserName { get; set; }
        public Priority Priority { get; set; }
        public TicketStatus Status { get; set; }
        public SupportDepartment Department { get; set; }
        public DateTime LastDate { get; set; }
        //Private properties
        private ICollection<SupportTicketReply> _replies;

        //Navigation properties
        public virtual ICollection<SupportTicketReply> Replies
        {
            get { return _replies ?? (_replies = new List<SupportTicketReply>()); }
            protected set { _replies = value; }
        }
    }

    public class SupportTicketConfig : EntityTypeConfiguration<SupportTicket>
    {
        public SupportTicketConfig()
        {
            Property(l => l.EmailOrPhoneNumber).IsRequired();
            Property(l => l.Subject).IsRequired();
            Property(l => l.Text).IsRequired();
        }
    }
}
