﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Models
{
    public class MessageTemplate : BaseEntity, ILocalizedEntity
    {
        public string Name { get; set; }

        [LocalizedProperty]
        public string Subject { get; set; }

        [LocalizedProperty]
        public string Body { get; set; }
    }

    public class MessageTemplateConfig : EntityTypeConfiguration<MessageTemplate>
    {
        public MessageTemplateConfig()
        {
        }
    }
}
