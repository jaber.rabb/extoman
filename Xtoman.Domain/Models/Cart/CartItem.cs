﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class CartItem : AuditEntity
    {
        public new long Id { get; set; }
        public int Quanity { get; set; }

        public long CartId { get; set; }
        public int ProductId { get; set; }
        public int ProductAttributeId { get; set; }

        //Navigation properties
        public virtual Cart Cart { get; set; }
        public virtual Product Product { get; set; }
        public virtual ProductAttribute ProductAttribute { get; set; }
    }

    public class CartItemConfig : EntityTypeConfiguration<CartItem>
    {
        public CartItemConfig()
        {
            HasRequired(p => p.Cart).WithMany(p => p.CartProducts).HasForeignKey(p => p.CartId);
            HasRequired(p => p.Product).WithMany(p => p.CartProducts).HasForeignKey(p => p.ProductId);
            HasRequired(p => p.ProductAttribute).WithMany(p => p.CartProducts).HasForeignKey(p => p.ProductAttributeId);
        }
    }
}
