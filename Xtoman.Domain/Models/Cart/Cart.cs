﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Cart : AuditEntity
    {
        public new long Id { get; set; }

        //Private property
        private ICollection<CartItem> _cartProducts;
        //Navigation property
        public virtual ICollection<CartItem> CartProducts {
            get { return _cartProducts ?? (_cartProducts = new List<CartItem>()); }
            protected set { _cartProducts = value; }
        }
    }

    public class CartConfig : EntityTypeConfiguration<Cart>
    {
        public CartConfig()
        {
        }
    }
}
