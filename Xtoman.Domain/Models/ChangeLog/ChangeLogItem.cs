﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ChangeLogDetail : BaseEntity
    {
        public new long Id { get; set; }
        public string PropertyName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public int ChangeLogId { get; set; }

        public virtual ChangeLog Log { get; set; }
    }
    public class ChangeLogDetailmConfig : EntityTypeConfiguration<ChangeLogDetail>
    {
        public ChangeLogDetailmConfig()
        {
            Property(x => x.PropertyName).HasMaxLength(50).IsRequired();
            //Property(x => x.OldValue).IsRequired();
            //Property(x => x.NewValue).IsRequired();
            HasRequired(x => x.Log).WithMany(x => x.Details).HasForeignKey(x => x.ChangeLogId);
        }
    }
}
