﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ChangeLog : BaseEntity
    {
        public string EntityName { get; set; }
        public int PrimaryKeyValue { get; set; }
        public DateTime DateChanged { get; set; }
        public ChangeLogState State { get; set; }
        public int? UserId { get; set; }
        public int? LanguageId { get; set; }

        //Private properties
        private ICollection<ChangeLogDetail> _details;

        //Navigation properties
        public virtual AppUser User { get; set; }
        public virtual Language Language { get; set; }
        public virtual ICollection<ChangeLogDetail> Details {
            get { return _details ?? (_details = new List<ChangeLogDetail>()); }
            set { _details = value; }
        }
    }

    public class ChangeLogConfig : EntityTypeConfiguration<ChangeLog>
    {
        public ChangeLogConfig()
        {
            Property(x => x.EntityName).HasMaxLength(50).IsRequired();
            HasOptional(x => x.User).WithMany().HasForeignKey(x => x.UserId);
            HasOptional(x => x.Language).WithMany().HasForeignKey(x => x.LanguageId);
        }
    }
}
