﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class FinnotechResponseIdVerification : Finnotech
    {
        #region Id Verification
        public string NationalId { get; set; }
        public string BirthDate { get; set; }
        public string Status { get; set; }
        public string FullName { get; set; }
        public int? FullNameSimilarity { get; set; }
        public string FirstName { get; set; }
        public int? FirstNameSimilarity { get; set; }
        public string LastName { get; set; }
        public int? LastNameSimilarity { get; set; }
        public string FatherName { get; set; }
        public int? FatherNameSimilarity { get; set; }
        public string DeathStatus { get; set; }
        #endregion
    }

    public class FinnotechResponseIdVerificationConfig : EntityTypeConfiguration<FinnotechResponseIdVerification>
    {
        public FinnotechResponseIdVerificationConfig()
        {
            ToTable("FinnotechIdVerifications");
        }
    }
}
