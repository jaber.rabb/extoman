﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class FinnotechResponseMobileVerify : Finnotech
    {
        public bool IsValid { get; set; }
    }

    public class FinnotechResponseMobileVerifyConfig : EntityTypeConfiguration<FinnotechResponseMobileVerify>
    {
        public FinnotechResponseMobileVerifyConfig()
        {
            ToTable("FinnotechResponseMobileVerify");
        }
    }
}
