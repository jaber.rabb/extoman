﻿using System.Data.Entity.ModelConfiguration;
using Xtoman.Domain.WebServices.Payment.Finnotech;

namespace Xtoman.Domain.Models
{
    public class FinnotechResponseBalance : Finnotech
    {
        public double CurrentBalance { get; set; }
        public double AvailableBalance { get; set; }
        public double EffectiveBalance { get; set; }
        public FinnotechState State { get; set; }
        public string Number { get; set; }
    }

    public class FinnotechResponseBalanceConfig : EntityTypeConfiguration<FinnotechResponseBalance>
    {
        public FinnotechResponseBalanceConfig()
        {
            ToTable("FinnotechBalance");
        }
    }
}
