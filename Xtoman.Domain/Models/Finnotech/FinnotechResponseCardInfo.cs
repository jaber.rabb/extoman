﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class FinnotechResponseCardInfo : Finnotech
    {
        #region Card Information
        public string DestCard { get; set; }
        public string Name { get; set; }
        public string Result { get; set; }
        public string Description { get; set; }
        public string DoTime { get; set; }
        #endregion

        public int? UserBankCardId { get; set; }
        public virtual UserBankCard UserBankCard { get; set; }
    }

    public class FinnotechResponseCardInfoConfig : EntityTypeConfiguration<FinnotechResponseCardInfo>
    {
        public FinnotechResponseCardInfoConfig()
        {
            HasOptional(p => p.UserBankCard).WithOptionalPrincipal().Map(x => x.MapKey("UserBankCardId"));
            ToTable("FinnotechCardInfos");
        }
    }
}
