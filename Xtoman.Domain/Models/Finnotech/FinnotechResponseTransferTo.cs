﻿using System.Data.Entity.ModelConfiguration;
using Xtoman.Domain.WebServices.Payment.Finnotech;

namespace Xtoman.Domain.Models
{
    public class FinnotechResponseTransferTo : Finnotech
    {
        public long Amount { get; set; }
        public string Description { get; set; }
        public string DestinationFirstname { get; set; }
        public string DestinationLastname { get; set; }
        public string DestinationNumber { get; set; }
        public string InquiryDate { get; set; }
        public long InquirySequence { get; set; }
        public string InquiryTime { get; set; }
        public long PaymentNumber { get; set; }
        public string RefCode { get; set; }
        public string SourceFirstname { get; set; }
        public string SourceLastname { get; set; }
        public string SourceNumber { get; set; }
        public string State { get; set; }
        public FinnotechTransferType? TypeEnum { get; set; }
}

    public class FinnotechResponseTransferToConfig : EntityTypeConfiguration<FinnotechResponseTransferTo>
    {
        public FinnotechResponseTransferToConfig()
        {
            ToTable("FinnotechTransferTo");
        }
    }
}
