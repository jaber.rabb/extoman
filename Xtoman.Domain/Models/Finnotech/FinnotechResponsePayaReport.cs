﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class FinnotechResponsePayaReport : Finnotech
    {
        public long Count { get; set; }
        public string State { get; set; }
        public List<FinnotechResponsePayaTransaction> Transactions { get; set; }
    }

    public class FinnotechResponsePayaReportConfig : EntityTypeConfiguration<FinnotechResponsePayaReport>
    {
        public FinnotechResponsePayaReportConfig()
        {
            HasMany(p => p.Transactions).WithRequired(p => p.Finnotech);
            ToTable("FinnotechPayaReport");
        }
    }
}
