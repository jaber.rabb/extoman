﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Finnotech : AuditEntity
    {
        public Finnotech()
        {
            Enum.TryParse(GetType().Name, true, out FinnotechResponseType FinnotechResponseType);
            FinType = FinnotechResponseType;
        }
        public new long Id { get; set; }

        #region Base Response
        public string Code { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public string TrackId { get; set; }
        public FinnotechResponseType FinType { get; private set; }
        #endregion

        //public int? ForUserId { get; set; }
        public int? ForUserId { get; set; }
        public AppUser ForUser { get; set; }
    }

    public class FinnotechConfig : EntityTypeConfiguration<Finnotech>
    {
        public FinnotechConfig()
        {
            HasOptional(p => p.ForUser).WithMany(p => p.Finnotechs).HasForeignKey(p => p.ForUserId); 
        }
    }
}
