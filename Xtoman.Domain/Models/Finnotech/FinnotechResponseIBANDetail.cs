﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class FinnotechResponseIBANDetail : Finnotech
    {
        public string Iban { get; set; }
        public string BankName { get; set; }
        public string Deposit { get; set; }
        public string Card { get; set; }
        public string DepositStatus { get; set; }
        public string DepositDescription { get; set; }
        public string DepositComment { get; set; }
        public string DepositOwner { get; set; }
        public string AlertCode { get; set; }
    }

    public class FinnotechResponseIBANDetailConfig : EntityTypeConfiguration<FinnotechResponseIBANDetail>
    {
        public FinnotechResponseIBANDetailConfig()
        {
            ToTable("FinnotechIBANDetails");
        }
    }
}
