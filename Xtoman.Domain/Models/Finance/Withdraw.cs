﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Withdraw : BaseEntity
    {
        public Withdraw()
        {
            InsertDate = DateTime.UtcNow;
        }
        public new long Id { get; set; }
        public string ApiWithdrawId { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public WithdrawFromType WithdrawType { get; set; }
        public ECurrencyType ECurrencyType { get; set; }
        public TradingPlatform? TradingPlatform { get; set; }
        public WalletApiType? WalletApiType { get; set; }
        public WithdrawStatus Status { get; set; }
        public DateTime InsertDate { get; set; }
        public string TransactionId { get; set; }

        public long? OrderId { get; set; }
        public virtual ExchangeOrder Order { get; set; }
    }

    public class WithdrawConfig : EntityTypeConfiguration<Withdraw>
    {
        public WithdrawConfig()
        {
            HasOptional(p => p.Order).WithMany(p => p.OrderWithdraws).HasForeignKey(p => p.OrderId);
            Property(p => p.Amount).HasPrecision(21, 8);
            Property(p => p.Fee).HasPrecision(21, 8);
        }
    }
}
