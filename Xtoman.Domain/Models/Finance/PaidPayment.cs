﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class PaidPayment : BaseEntity
    {
        public PaidPayment()
        {
            PayDate = DateTime.UtcNow;
            InsertDate = DateTime.UtcNow;
        }
        public decimal Amount { get; set; } // مقدار
        public decimal AmountInUSD { get; set; } // مقدار معادل دلار
        public decimal AmountInToman { get; set; } // مقدار معادل تومان
        public ECurrencyType Type { get; set; }
        public string Description { get; set; }
        public int InsertUserId { get; set; }
        public int? ForUserId { get; set; }
        public DateTime PayDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string TransactionNumber { get; set; }
        public string ReceiverAddress { get; set; }
        public string ReceiveMemo { get; set; }
        public PaidPaymentFor For { get; set; }
        public int? BankAccountId { get; set; }
        public WalletApiType? WalletType { get; set; }
        public PaidPaymentSide Side { get; set; }

        public virtual BankAccount BankAccount { get; set; }
        public virtual AppUser InsertUser { get; set; }
        public virtual AppUser ForUser { get; set; }
    }

    public class PaidPaymentConfig : EntityTypeConfiguration<PaidPayment>
    {
        public PaidPaymentConfig()
        {
            HasOptional(p => p.BankAccount).WithMany(p => p.PaidPayments).HasForeignKey(p => p.BankAccountId);
            HasRequired(p => p.InsertUser).WithMany().HasForeignKey(p => p.InsertUserId);
            HasOptional(p => p.ForUser).WithMany().HasForeignKey(p => p.ForUserId);
        }
    }
}
