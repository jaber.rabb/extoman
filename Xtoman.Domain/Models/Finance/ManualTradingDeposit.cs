﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ManualTradingDeposit : BaseEntity
    {
        public decimal Amount { get; set; }
        public ECurrencyType Type { get; set; }
        public TradingPlatform TradingPlatform { get; set; }
        public int CommissionPercent { get; set; }
        public ContractType ContractType { get; set; }

        public int? ContractorUserId { get; set; }
        public virtual AppUser ContractorUser { get; set; }
    }

    public class ManualTradingDepositConfig : EntityTypeConfiguration<ManualTradingDeposit>
    {
        public ManualTradingDepositConfig()
        {
            HasOptional(p => p.ContractorUser).WithMany().HasForeignKey(p => p.ContractorUserId);
            Property(p => p.Amount).HasPrecision(21, 8);
            Property(p => p.Amount).HasPrecision(21, 8);
        }
    }
}
