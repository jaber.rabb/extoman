﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class BankAccount : BaseEntity
    {
        public string Name { get; set; }
        public string AccountNumber { get; set; }
        public string BankName { get; set; }
        public string Sheba { get; set; }
        public string CardNumber { get; set; }

        private ICollection<PaidPayment> _paidPayments;
        public virtual ICollection<PaidPayment> PaidPayments
        {
            get { return _paidPayments ?? (_paidPayments = new List<PaidPayment>()); }
            protected set { _paidPayments = value; }
        }
    }

    public class BankAccountConfig : EntityTypeConfiguration<BankAccount>
    {
        public BankAccountConfig()
        {

        }
    }
}
