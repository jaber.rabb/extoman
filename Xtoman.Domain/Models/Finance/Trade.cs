﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class Trade : BaseEntity
    {
        public Trade()
        {
            Date = DateTime.UtcNow;
        }
        public new long Id { get; set; }
        public string ApiTradeId { get; set; }
        public bool IsManual { get; set; }
        public TradingPlatform TradingPlatform { get; set; }
        public TradeStatus TradeStatus { get; set; }
        public TradeOrderType? TradeOrderType { get; set; }
        public DateTime Date { get; set; }
        public decimal Price { get; set; }

        #region From
        public decimal FromAmount { get; set; }
        public decimal FromUSDAmount { get; set; }
        public decimal FromTomanAmount { get; set; }
        public ECurrencyType FromCryptoType { get; set; }
        #endregion

        #region To
        public decimal ToAmount { get; set; }
        public decimal ToAmountInUSD { get; set; }
        public decimal ToAmountInToman { get; set; }
        public ECurrencyType ToCryptoType { get; set; }
        #endregion

        #region Fee
        public decimal TradingFee { get; set; }
        public decimal TradingFeeInToman { get; set; }
        public decimal TradingFeeInUSD { get; set; }
        public ECurrencyType TradingFeeType { get; set; }
        #endregion

        #region Additional Fee || Contractor Fee
        public decimal AdditionalFee { get; set; }
        public decimal AdditionalFeeInToman { get; set; }
        public decimal AdditionalFeeInUSD { get; set; }
        public ECurrencyType AdditionalFeeType { get; set; }
        #endregion

        public int? ContractorUserId { get; set; }
        public virtual AppUser ContractorUser { get; set; }
        public long? ExchangeOrderId { get; set; }
        public virtual ExchangeOrder Order { get; set; }
    }

    public class TradeConfig : EntityTypeConfiguration<Trade>
    {
        public TradeConfig()
        {
            HasOptional(p => p.Order).WithMany(p => p.OrderTrades).HasForeignKey(p => p.ExchangeOrderId);
            HasOptional(p => p.ContractorUser).WithMany().HasForeignKey(p => p.ContractorUserId);
            Property(p => p.AdditionalFee).HasPrecision(21, 8);
            Property(p => p.AdditionalFeeInUSD).HasPrecision(21, 8);
            Property(p => p.FromAmount).HasPrecision(21, 8);
            Property(p => p.FromUSDAmount).HasPrecision(21, 8);
            Property(p => p.ToAmount).HasPrecision(21, 8);
            Property(p => p.ToAmountInUSD).HasPrecision(21, 8);
            Property(p => p.TradingFee).HasPrecision(21, 8);
            Property(p => p.TradingFeeInUSD).HasPrecision(21, 8);
        }
    }
}
