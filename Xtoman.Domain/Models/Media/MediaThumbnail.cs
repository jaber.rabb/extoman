﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class MediaThumbnail : BaseEntity
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public string Url { get; set; }

        public int MediaId { get; set; }
        public int MediaThumbnailSizeId { get; set; }
        //Navigation properties
        public virtual Media Media { get; set; }
        public virtual MediaThumbnailSize MediaThumbnailSize { get; set; }
    }

    public class MediaThumbnailConfig : EntityTypeConfiguration<MediaThumbnail>
    {
        public MediaThumbnailConfig()
        {
            HasRequired(p => p.MediaThumbnailSize).WithMany().HasForeignKey(p => p.MediaThumbnailSizeId);
            HasRequired(p => p.Media).WithMany(p => p.Thumbnails).HasForeignKey(p => p.MediaId);
        }
    }
}
