﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class MediaCategoryMedia : BaseEntity
    {
        public int MediaId { get; set; }
        public int MediaCategoryId { get; set; }

        //Navigation properties
        public virtual Media Media { get; set; }
        public virtual MediaCategory MediaCategory { get; set; }
    }

    public class MediaCategoryMediaConfig : EntityTypeConfiguration<MediaCategoryMedia>
    {
        public MediaCategoryMediaConfig()
        {
            HasRequired(p => p.Media).WithMany(p => p.Categories).HasForeignKey(p => p.MediaId);
            HasRequired(p => p.MediaCategory).WithMany(p => p.Medias).HasForeignKey(p => p.MediaCategoryId);
        }
    }
}
