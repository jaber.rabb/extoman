﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using Xtoman.Utility;

namespace Xtoman.Domain.Models
{
    public class Media : BaseEntity
    {
        public Media()
        {
            AddDate = DateTime.UtcNow;
        }
        public string Name { get; set; }
        public string Url { get; set; }
        public DateTime AddDate { get; set; }
        public MediaType Type { get; set; }
        public string Alt { get; set; }
        public int? InsertUserId { get; set; }

        //Private properties
        private ICollection<MediaCategoryMedia> _categories;
        private ICollection<MediaThumbnail> _thumbnails;
        private ICollection<Post> _posts;
        private ICollection<Product> _products;

        //Navigation properties
        public virtual ICollection<MediaCategoryMedia> Categories
        {
            get { return _categories ?? (_categories = new List<MediaCategoryMedia>()); }
            protected set { _categories = value; }
        }
        public virtual ICollection<MediaThumbnail> Thumbnails
        {
            get { return _thumbnails ?? (_thumbnails = new List<MediaThumbnail>()); }
            protected set { _thumbnails = value; }
        }
        public virtual ICollection<Post> Posts
        {
            get { return _posts ?? (_posts = new List<Post>()); }
            set { _posts = value; }
        }
        public virtual ICollection<Product> Products
        {
            get { return _products ?? (_products = new List<Product>()); }
            set { _products = value; }
        }
    }

    public class MediaConfig : EntityTypeConfiguration<Media>
    {
        public MediaConfig()
        {
        }
    }
}
