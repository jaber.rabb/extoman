﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    [Serializable]
    public class MediaThumbnailSizeCache : BaseCachingEntity
    {
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool Watermark { get; set; }
        public bool Crop { get; set; }
    }
    public class MediaThumbnailSize : BaseEntity
    {
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool Watermark { get; set; }
        public bool Crop { get; set; }
    }

    public class MediaThumbnailSizeConfig : EntityTypeConfiguration<MediaThumbnailSize>
    {
        public MediaThumbnailSizeConfig()
        {
        }
    }
}
