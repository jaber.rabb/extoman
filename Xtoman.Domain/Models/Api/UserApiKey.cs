﻿using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models.Api
{
    public class UserApiKey : AuditEntity
    {
        public string Key { get; set; }
        public string Secret { get; set; }
        public int UserId { get; set; }
        public AppUser User { get; set; }
    }

    public class UserApiKeyConfig : EntityTypeConfiguration<UserApiKey>
    {
        public UserApiKeyConfig()
        {
            HasRequired(p => p.User).WithMany().HasForeignKey(p => p.UserId);
        }
    }
}
