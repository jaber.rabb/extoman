﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    public class ApiToken : BaseEntity
    {
        public new long Id { get; set; }
        public string AccessTokenHash { get; set; }
        public DateTime AccessTokenIssuedDateTime { get; set; }
        public DateTime AccessTokenExpirationDateTime { get; set; }
        public string RefreshTokenIdHash { get; set; }
        public DateTime RefreshTokenExpiresUtc { get; set; }
        public string RefreshToken { get; set; }
        public int UserId { get; set; }

        //Private properties
        private ICollection<ApiResult> _apiResults;

        //Navigation properties
        public virtual AppUser User { get; set; }
        public virtual ICollection<ApiResult> ApiResults
        {
            get { return _apiResults ?? (_apiResults = new List<ApiResult>()); }
            set { _apiResults = value; }
        }
    }

    public class ApiTokenConfig : EntityTypeConfiguration<ApiToken>
    {
        public ApiTokenConfig()
        {
            HasRequired(p => p.User).WithMany().HasForeignKey(p => p.UserId);
        }
    }
}
