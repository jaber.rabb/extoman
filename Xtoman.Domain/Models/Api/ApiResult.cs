﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Xtoman.Domain.Models
{
    [IgnoreChangeLog]
    public class ApiResult : BaseEntity
    {
        public string RequestContent { get; set; }
        public string ResponseContent { get; set; }
        public DateTime InsertDate { get; set; }
        public bool IsApiOutput { get; set; }
        public long? ApiTokenId { get; set; }

        public virtual ApiToken ApiToken { get; set; }
    }

    public class ApiResultConfig : EntityTypeConfiguration<ApiResult>
    {
        public ApiResultConfig()
        {
            HasOptional(p => p.ApiToken).WithMany(p => p.ApiResults).HasForeignKey(p => p.ApiTokenId);
        }
    }
}
