﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Blockchain
{
    public class BlockcypherFeature
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("last_transition_height")]
        public long LastTransitionHeight { get; set; }

        [JsonProperty("last_transition_hash")]
        public string LastTransitionHash { get; set; }
    }
}
