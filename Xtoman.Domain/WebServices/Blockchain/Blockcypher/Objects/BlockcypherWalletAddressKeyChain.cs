﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Blockchain
{
    public class BlockcypherWalletAddressKeyChain : BlockcypherAddressKeychain
    {
        /// <summary>
        /// User token associated with this wallet.
        /// </summary>
        [JsonProperty("token")]
        public string Token { get; set; }

        /// <summary>
        /// Name of the wallet.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// List of addresses associated with this wallet.
        /// </summary>
        [JsonProperty("addresses")]
        public List<string> Addresses { get; set; }
    }
}
