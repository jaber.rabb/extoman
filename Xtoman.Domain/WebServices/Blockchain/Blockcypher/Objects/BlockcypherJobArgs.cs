﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.Blockchain
{
    /// <summary>
    /// A JobArgs represents the query parameters of a particular analytics job, used when Creating an Analytics Job and returned within a Job. Note that the required and optional arguments can change depending on the engine you’re using; for more specifics check the Analytics Engine and Parameters section.
    /// </summary>
    public class BlockcypherJobArgs
    {
        /// <summary>
        /// Address hash this job is querying.
        /// </summary>
        [JsonProperty("address")]
        public string Address { get; set; }

        /// <summary>
        /// Minimal/threshold value (in satoshis) to query.
        /// </summary>
        [JsonProperty("value_threshold")]
        public int ValueThreshold { get; set; }

        /// <summary>
        /// Limit of results to return.
        /// </summary>
        [JsonProperty("limit")]
        public int Limit { get; set; }

        /// <summary>
        /// Beginning of time range to query.
        /// </summary>
        [JsonProperty("start")]
        public DateTimeOffset Start { get; set; }

        /// <summary>
        /// End of time range to query.
        /// </summary>
        [JsonProperty("end")]
        public DateTimeOffset End { get; set; }

        /// <summary>
        /// Degree of connectiveness to query.
        /// </summary>
        [JsonProperty("degree")]
        public int Degree { get; set; }

        /// <summary>
        /// IP address and port, of the form “0.0.0.0:80”. Ideally an IP and port combination found from another API lookup (for example, relayed_by from the Transaction Hash Endpoint)
        /// </summary>
        [JsonProperty("source")]
        public string Source { get; set; }
    }
}
