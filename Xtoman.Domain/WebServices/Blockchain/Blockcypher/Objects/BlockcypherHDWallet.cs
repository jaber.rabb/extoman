﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Blockchain
{
    /// <summary>
    /// An HDWallet contains addresses derived from a single seed. Like normal wallets, it can be used interchangeably with all the Address API endpoints, and in many places that require addresses, like when Creating Transactions.
    /// </summary>
    public class BlockcypherHDWallet
    {
        /// <summary>
        /// User token associated with this HD wallet.
        /// </summary>
        [JsonProperty("token")]
        public string Token { get; set; }

        /// <summary>
        /// Name of the HD wallet.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// List of HD chains associated with this wallet, each containing HDAddresses. A single chain is returned if the wallet has no subchains.
        /// </summary>
        [JsonProperty("chains")]
        public List<BlockcypherHDChain> Chains { get; set; }

        /// <summary>
        /// true for HD wallets, not present for normal wallets.
        /// </summary>
        [JsonProperty("hd")]
        public bool HD { get; set; }

        /// <summary>
        /// The extended public key all addresses in the HD wallet are derived from. It’s encoded in BIP32 format
        /// </summary>
        [JsonProperty("extended_public_key")]
        public string ExtendedPublicKey { get; set; }

        /// <summary>
        /// (Optional) returned for HD wallets created with subchains.
        /// </summary>
        [JsonProperty("subchain_indexes")]
        public List<long> SubchainIndexes { get; set; }
    }
}
