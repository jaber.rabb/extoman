﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.Blockchain
{
    /// <summary>
    /// A Job represents an analytics query set up through the Analytics API.
    /// </summary>
    public class BlockcypherJob
    {
        /// <summary>
        /// The token that created this job.
        /// </summary>
        [JsonProperty("token")]
        public string Token { get; set; }

        /// <summary>
        /// The engine used for the job query.
        /// </summary>
        [JsonProperty("analytics_engine")]
        public string AnalyticsEngine { get; set; }

        /// <summary>
        /// The time this job was created.
        /// </summary>
        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }

        /// <summary>
        /// (Optional) When this job was completed; only present on complete jobs.
        /// </summary>
        [JsonProperty("completed_at")]
        public DateTimeOffset? CompletedAt { get; set; }

        /// <summary>
        /// true if this job is finished processing, false otherwise
        /// </summary>
        [JsonProperty("finished")]
        public bool Finished { get; set; }

        /// <summary>
        /// true if this job has begun processing, false otherwise.
        /// </summary>
        [JsonProperty("started")]
        public bool Started { get; set; }

        /// <summary>
        /// Unique identifier for this job, used to get job status and results.
        /// </summary>
        [JsonProperty("ticket")]
        public string Ticket { get; set; }

        /// <summary>
        /// (Optional) URL to query job results; only present on complete jobs.
        /// </summary>
        [JsonProperty("result_path")]
        public Uri ResultPath { get; set; }

        /// <summary>
        /// Query arguments for this job.
        /// </summary>
        [JsonProperty("args")]
        public BlockcypherJobArgs Args { get; set; }
    }
}
