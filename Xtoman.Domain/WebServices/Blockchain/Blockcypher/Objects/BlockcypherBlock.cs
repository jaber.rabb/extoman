﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Blockchain
{
    /// <summary>
    /// A Block represents the current state of a particular block from a Blockchain. Typically returned from the Block Hash and Block Height endpoints.
    /// </summary>
    public partial class BlockcypherBlock
    {
        /// <summary>
        /// The hash of the block; in Bitcoin, the hashing function is SHA256(SHA256(block))
        /// </summary>
        [JsonProperty("hash")]
        public string Hash { get; set; }

        /// <summary>
        /// The height of the block in the blockchain; i.e., there are height earlier blocks in its blockchain.
        /// </summary>
        [JsonProperty("height")]
        public long Height { get; set; }

        /// <summary>
        /// The name of the blockchain represented, in the form of $COIN.$CHAIN
        /// </summary>
        [JsonProperty("chain")]
        public string Chain { get; set; }

        /// <summary>
        /// The total number of satoshis transacted in this block.
        /// </summary>
        [JsonProperty("total")]
        public long Total { get; set; }

        /// <summary>
        /// The total number of fees—in satoshis—collected by miners in this block.
        /// </summary>
        [JsonProperty("fees")]
        public long Fees { get; set; }

        /// <summary>
        /// Block version
        /// </summary>
        [JsonProperty("ver")]
        public long Ver { get; set; }

        /// <summary>
        /// Recorded time at which block was built. Note: Miners rarely post accurate clock times.
        /// </summary>
        [JsonProperty("time")]
        public DateTimeOffset Time { get; set; }

        /// <summary>
        /// The time BlockCypher’s servers receive the block. Our servers’ clock is continuously adjusted and accurate.
        /// </summary>
        [JsonProperty("received_time")]
        public DateTimeOffset ReceivedTime { get; set; }

        /// <summary>
        /// Address of the peer that sent BlockCypher’s servers this block.
        /// </summary>
        [JsonProperty("relayed_by")]
        public string RelayedBy { get; set; }

        /// <summary>
        /// The block-encoded difficulty target.
        /// </summary>
        [JsonProperty("bits")]
        public long Bits { get; set; }

        /// <summary>
        /// The number used by a miner to generate this block.
        /// </summary>
        [JsonProperty("nonce")]
        public long Nonce { get; set; }

        /// <summary>
        /// Number of transactions in this block.
        /// </summary>
        [JsonProperty("n_tx")]
        public long NTx { get; set; }

        /// <summary>
        /// The hash of the previous block in the blockchain.
        /// </summary>
        [JsonProperty("prev_block")]
        public string PrevBlock { get; set; }

        /// <summary>
        /// The Merkle root of this block.
        /// </summary>
        [JsonProperty("mrkl_root")]
        public string MrklRoot { get; set; }

        /// <summary>
        /// An array of transaction hashes in this block. By default, only 20 are included.
        /// </summary>
        [JsonProperty("txids")]
        public List<string> Txids { get; set; }

        /// <summary>
        /// The depth of the block in the blockchain; i.e., there are depth later blocks in its blockchain.
        /// </summary>
        [JsonProperty("depth")]
        public long Depth { get; set; }

        /// <summary>
        /// The BlockCypher URL to query for more information on the previous block.
        /// </summary>
        [JsonProperty("prev_block_url")]
        public Uri PrevBlockUrl { get; set; }

        /// <summary>
        /// The base BlockCypher URL to receive transaction details. To get more details about specific transactions, you must concatenate this URL with the desired transaction hash(es).
        /// </summary>
        [JsonProperty("tx_url")]
        public Uri TxUrl { get; set; }

        /// <summary>
        /// (Optional) Raw size of block (including header and all transactions) in bytes. Not returned for bitcoin blocks earlier than height 389104.
        /// </summary>
        [JsonProperty("size")]
        public long? Size { get; set; }
    }
}
