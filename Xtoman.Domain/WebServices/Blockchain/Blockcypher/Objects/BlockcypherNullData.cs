﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Blockchain
{
    /// <summary>
    /// A NullData Object is used exclusively by our Data Endpoint to embed small pieces of data on the blockchain. If your data is over 40 bytes, it cannot be embedded into the blockchain and will return an error
    /// </summary>
    public class BlockcypherNullData
    {
        /// <summary>
        /// The string representing the data to embed, can be either hex-encoded or plaintext.
        /// </summary>
        [JsonProperty("data")]
        public string Data { get; set; }

        /// <summary>
        /// (Optional) The encoding of your data, can be either string (for plaintext) or hex (for hex-encoded). If not set, defaults to hex.
        /// </summary>
        [JsonProperty("encoding")]
        public string Encoding { get; set; }

        /// <summary>
        /// (Optional) Your BlockCypher API token, can either be included here or as a URL Parameter in your request.
        /// </summary>
        [JsonProperty("token")]
        public string Token { get; set; }

        /// <summary>
        /// (Optional) The hash of the transaction containing your data; only part of return object.
        /// </summary>
        [JsonProperty("hash")]
        public string Hash { get; set; }
    }
}
