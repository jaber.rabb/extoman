﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Blockchain
{
    /// <summary>
    /// A MicroTX represents a streamlined—and typically much lower value—microtransaction, one which BlockCypher can sign for you if you send your private key. MicroTXs can also be signed on the client-side without ever sending your private key. You’ll find these objects used in the Microtransaction API.
    /// </summary>
    public class BlockcypherMicroTX
    {
        /// <summary>
        /// Hex-encoded public key from which you’re sending coins.
        /// </summary>
        [JsonProperty("from_pubkey")]
        public string FromPubkey { get; set; }

        /// <summary>
        /// The target address to which you’re sending coins.
        /// </summary>
        [JsonProperty("to_address")]
        public string ToAddress { get; set; }

        /// <summary>
        /// Value you’re sending/you’ve sent in satoshis.
        /// </summary>
        [JsonProperty("value_satoshis")]
        public long ValueSatoshis { get; set; }

        /// <summary>
        /// Your BlockCypher API token
        /// </summary>
        [JsonProperty("token")]
        public string Token { get; set; }

        /// <summary>
        /// (Optional) Hex-encoded signatures for you to send back after having received (and signed) tosign.
        /// </summary>
        [JsonProperty("signatures")]
        public List<string> Signatures { get; set; }

        /// <summary>
        /// (Optional) Hex-encoded data for you to sign after initiating the microtransaction. Sent in reply to a microtransaction generated using from_pubkey/a public key.
        /// </summary>
        [JsonProperty("tosign")]
        public List<string> Tosign { get; set; }

        /// <summary>
        /// (Optional) Partial list of inputs that will be used with this transaction. Inputs themsleves are heavily pared down, see cURL sample. Only returned when using from_pubkey.
        /// </summary>
        [JsonProperty("inputs")]
        public List<BlockcypherTxInput> Inputs { get; set; }

        /// <summary>
        /// (Optional) Partial list of outputs that will be used with this transaction. Outputs themselves are heavily pared down, see cURL sample. Only returned when using from_pubkey.
        /// </summary>
        [JsonProperty("outputs")]
        public List<BlockcypherTxOutput> Outputs { get; set; }

        /// <summary>
        /// (Optional) BlockCypher’s optimally calculated fees for this MicroTX to guarantee swift 99% confirmation, only returned when using from_pubkey. BlockCypher pays these fees for the first 8,000 microtransactions, but like regular transactions, it is deducted from the source address thereafter.
        /// </summary>
        [JsonProperty("fees")]
        public long Fees { get; set; }

        /// <summary>
        /// (Optional) Address BlockCypher will use to send back your change. If not set, defaults to the address from which the coins were originally sent. While not required, we recommend that you set a change address.
        /// </summary>
        [JsonProperty("change_address")]
        public string ChangeAddress { get; set; }

        /// <summary>
        /// (Optional) If not set, defaults to true, which means the API will wait for BlockCypher to guarantee the transaction, using our Confidence Factor. The guarantee usually takes around 8 seconds. If manually set to false, the Microtransaction endpoint will return as soon as the transaction is broadcast.
        /// </summary>
        [JsonProperty("wait_guarantee")]
        public bool WaitGuarantee { get; set; }

        /// <summary>
        /// (Optional) The hash of the finalized transaction, once sent.
        /// </summary>
        [JsonProperty("hash")]
        public string Hash { get; set; }
    }
}
