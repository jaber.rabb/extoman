﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.Blockchain
{
    /// <summary>
    /// An Event represents a WebHooks or WebSockets-based notification request, as detailed in the Events & Hooks section of the documentation.
    /// </summary>
    public class BlockcypherEvent
    {
        /// <summary>
        /// Identifier of the event; generated when a new request is created.
        /// </summary>
        [JsonProperty("id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Type of event; can be unconfirmed-tx, new-block, confirmed-tx, tx-confirmation, double-spend-tx, tx-confidence.
        /// </summary>
        [JsonProperty("event")]
        public string Event { get; set; }

        /// <summary>
        /// (Optional) Only objects with a matching hash will be sent. The hash can either be for a block or a transaction.
        /// </summary>
        [JsonProperty("hash")]
        public string Hash { get; set; }

        /// <summary>
        /// (Optional) Only transactions associated with the given wallet will be sent; can use a regular or HD wallet name. If used, requires a user token.
        /// </summary>
        [JsonProperty("wallet_name")]
        public string WalletName { get; set; }

        /// <summary>
        /// (Optional) Only transactions associated with the given address will be sent. A wallet name can also be used instead of an address, which will then match on any address in the wallet.
        /// </summary>
        [JsonProperty("address")]
        public string Address { get; set; }

        /// <summary>
        /// (Optional) Used in concert with the tx-confirmation event type to set the number of confirmations desired for which to receive an update. You’ll receive an updated TX for every confirmation up to this amount. The maximum allowed is 10; if not set, it will default to 6.
        /// </summary>
        [JsonProperty("confirmations")]
        public long? Confirmations { get; set; }

        /// <summary>
        /// (Optional) Used in concert with the tx-confidence event type to set the minimum confidence for which you’ll receive a notification. You’ll receive a TX once this threshold is met. Will accept any float between 0 and 1, exclusive; if not set, defaults to 0.99.
        /// </summary>
        [JsonProperty("confidence")]
        public double Confidence { get; set; }

        /// <summary>
        /// (Optional) Only transactions with an output script of the provided type will be sent. The recognized types of scripts are: pay-to-pubkey-hash, pay-to-multi-pubkey-hash, pay-to-pubkey, pay-to-script-hash, null-data (sometimes called OP_RETURN), empty or unknown.
        /// </summary>
        [JsonProperty("script")]
        public string Script { get; set; }

        /// <summary>
        /// (Optional) Required if wallet_name is used, though generally we advise users to include it (as they can reach API throttling thresholds rapidly).
        /// </summary>
        [JsonProperty("token")]
        public string Token { get; set; }

        /// <summary>
        /// (Optional) Callback URL for this Event’s WebHook; not applicable for WebSockets usage.
        /// </summary>
        [JsonProperty("url")]
        public Uri Url { get; set; }

        /// <summary>
        /// Number of errors when attempting to POST to callback URL; not applicable for WebSockets usage.
        /// </summary>
        [JsonProperty("callback_errors")]
        public long CallbackErrors { get; set; }
    }
}
