﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Blockchain
{
    /// <summary>
    /// An array of HDChains are included in every HDWallet and returned from the Get Wallet, Get Wallet Addresses and Derive Address in Wallet endpoints.
    /// </summary>
    public class BlockcypherHDChain
    {
        /// <summary>
        /// Array of HDAddresses associated with this subchain.
        /// </summary>
        [JsonProperty("chain_addresses")]
        public List<BlockcypherHDAddress> ChainAddresses { get; set; }

        /// <summary>
        /// (Optional) Index of the subchain, returned if the wallet has subchains.
        /// </summary>
        [JsonProperty("index")]
        public int? Index { get; set; }
    }
}
