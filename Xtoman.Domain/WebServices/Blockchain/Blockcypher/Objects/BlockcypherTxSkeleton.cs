﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Blockchain
{
    public class BlockcypherTxSkeleton
    {
        [JsonProperty("tx")]
        public BlockcypherTx Tx { get; set; }

        [JsonProperty("tosign")]
        public List<string> Tosign { get; set; }

        [JsonProperty("errors")]
        public List<BlockcypherError> Errors { get; set; }

        [JsonProperty("signatures")]
        public List<string> Signatures { get; set; }

        [JsonProperty("pubkeys")]
        public List<string> Pubkeys { get; set; }
    }
}
