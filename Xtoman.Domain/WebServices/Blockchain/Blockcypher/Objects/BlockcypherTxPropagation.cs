﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices.Blockchain
{
    /// <summary>
    /// By operating a well-connected node, we collect a lot of information about how transactions propagate; for example, our Confidence Factor relies on this connectivity. With this endpoint, you can leverage our connectivity to get an approximation of a transaction’s location of origin.
    /// </summary>
    public class BlockcypherTxPropagation
    {
        /// <summary>
        /// The hash of the transaction you queried.
        /// </summary>
        [JsonProperty("transaction")]
        public string Transaction { get; set; }

        /// <summary>
        /// An object containing latitude and longitude floats representing the first location to broadcast this transaction to BlockCypher.
        /// </summary>
        [JsonProperty("first_location")]
        public BlockcypherAggregatedOrigin FirstLocation { get; set; }

        /// <summary>
        /// (Optional) The name of the city closest to the first location. If no nearby city can be found, this is not returned.
        /// </summary>
        [JsonProperty("first_city")]
        public string FirstCity { get; set; }

        /// <summary>
        /// The name of the country containing the first_location.
        /// </summary>
        [JsonProperty("first_country")]
        public string FirstCountry { get; set; }

        /// <summary>
        /// An object containing latitude and longitude floats representing BlockCypher’s best guess of likely origin of this transaction, based on the radius of the smallest circle containing the first peer from which we detect this transaction, the average of the first five peers, and the average of the first ten peers.
        /// </summary>
        [JsonProperty("aggregated_origin")]
        public BlockcypherAggregatedOrigin AggregatedOrigin { get; set; }

        /// <summary>
        /// The radius (in meters) of the smallest circle containing the first peer from which we detect this transaction, the average of the first five peers, and the average of the first ten peers. In a general sense, this represents an approximate confidence interval in our calculated aggregated_origin; the smaller the radius, the more confidence in our aggregated_origin assessment.
        /// </summary>
        [JsonProperty("aggregated_origin_radius")]
        public long AggregatedOriginRadius { get; set; }

        /// <summary>
        /// The timestamp when BlockCypher first received this transaction.
        /// </summary>
        [JsonProperty("first_received")]
        public DateTimeOffset FirstReceived { get; set; }
    }

    public class BlockcypherAggregatedOrigin
    {
        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [JsonProperty("longitude")]
        public double Longitude { get; set; }
    }
}
