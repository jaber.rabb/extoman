﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Blockchain
{
    /// <summary>
    /// A AddressForwardCallback object represents the payload delivered to the optional callback_url in a AddressForward request.
    /// </summary>
    public class BlockcypherAddressForwardCallback
    {
        /// <summary>
        /// Amount sent to the destination address, in satoshis.
        /// </summary>
        [JsonProperty("value")]
        public long Value { get; set; }

        /// <summary>
        /// The intermediate address to which the was originally sent.
        /// </summary>
        [JsonProperty("input_address")]
        public string InputAddress { get; set; }

        /// <summary>
        /// The final destination address to which the will eventually be sent.
        /// </summary>
        [JsonProperty("destination")]
        public string Destination { get; set; }

        /// <summary>
        /// The transaction hash representing the initial to the input_address.
        /// </summary>
        [JsonProperty("input_transaction_hash")]
        public string InputTransactionHash { get; set; }

        /// <summary>
        /// The transaction hash of the generated transaction that forwards the from the input_address to the destination.
        /// </summary>
        [JsonProperty("transaction_hash")]
        public string TransactionHash { get; set; }
    }
}
