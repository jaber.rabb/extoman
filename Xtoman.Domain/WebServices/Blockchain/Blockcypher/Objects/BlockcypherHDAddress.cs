﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Blockchain
{
    /// <summary>
    /// An HD Address object contains an address and its BIP32 HD path (location of the address in the HD tree). It also contains the hex-encoded public key when returned from the Derive Address in Wallet endpoint.
    /// </summary>
    public class BlockcypherHDAddress
    {
        /// <summary>
        /// Standard address representation.
        /// </summary>
        [JsonProperty("address")]
        public string Address { get; set; }

        /// <summary>
        /// The BIP32 path of the HD address.
        /// </summary>
        [JsonProperty("path")]
        public string Path { get; set; }

        /// <summary>
        /// optional Contains the hex-encoded public key if returned by Derive Address in Wallet endpoint.
        /// </summary>
        [JsonProperty("public")]
        public string Public { get; set; }
    }
}
