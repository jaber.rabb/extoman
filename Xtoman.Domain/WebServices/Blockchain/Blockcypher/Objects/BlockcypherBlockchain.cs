﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.Blockchain
{
    /// <summary>
    /// A Blockchain represents the current state of a particular blockchain from the Coin/Chain (Ex: btc/main) resources that BlockCypher supports. Typically returned from the Chain API endpoint.
    /// </summary>
    public partial class BlockcypherBlockchain
    {
        /// <summary>
        /// The name of the blockchain represented, in the form of $COIN.$CHAIN.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// The current height of the blockchain; i.e., the number of blocks in the blockchain.
        /// </summary>
        [JsonProperty("height")]
        public long Height { get; set; }

        /// <summary>
        /// The hash of the latest confirmed block in the blockchain; in Bitcoin, the hashing function is SHA256(SHA256(block)).
        /// </summary>
        [JsonProperty("hash")]
        public string Hash { get; set; }

        /// <summary>
        /// The time of the latest update to the blockchain; typically when the latest block was added.
        /// </summary>
        [JsonProperty("time")]
        public DateTimeOffset Time { get; set; }

        /// <summary>
        /// The BlockCypher URL to query for more information on the latest confirmed block; returns a Block.
        /// </summary>
        [JsonProperty("latest_url")]
        public Uri LatestUrl { get; set; }

        /// <summary>
        /// The hash of the second-to-latest confirmed block in the blockchain.
        /// </summary>
        [JsonProperty("previous_hash")]
        public string PreviousHash { get; set; }

        /// <summary>
        /// The BlockCypher URL to query for more information on the second-to-latest confirmed block; returns a Block.
        /// </summary>
        [JsonProperty("previous_url")]
        public Uri PreviousUrl { get; set; }

        /// <summary>
        /// N/A, will be deprecated soon.
        /// </summary>
        [JsonProperty("peer_count")]
        public long PeerCount { get; set; }

        /// <summary>
        /// Number of unconfirmed transactions in memory pool (likely to be included in next block).
        /// </summary>
        [JsonProperty("unconfirmed_count")]
        public long UnconfirmedCount { get; set; }

        /// <summary>
        /// A rolling average of the fee (in satoshis) paid per kilobyte for transactions to be confirmed within 1 to 2 blocks.
        /// </summary>
        [JsonProperty("high_fee_per_kb")]
        public long HighFeePerKb { get; set; }

        /// <summary>
        /// A rolling average of the fee (in satoshis) paid per kilobyte for transactions to be confirmed within 3 to 6 blocks.
        /// </summary>
        [JsonProperty("medium_fee_per_kb")]
        public long MediumFeePerKb { get; set; }

        /// <summary>
        /// A rolling average of the fee (in satoshis) paid per kilobyte for transactions to be confirmed in 7 or more blocks.
        /// </summary>
        [JsonProperty("low_fee_per_kb")]
        public long LowFeePerKb { get; set; }

        /// <summary>
        /// (Optional) The current height of the latest fork to the blockchain; when no competing blockchain fork present, not returned with endpoints that return Blockchains.
        /// </summary>
        [JsonProperty("last_fork_height")]
        public long? LastForkHeight { get; set; }

        /// <summary>
        /// (Optional) The hash of the latest confirmed block in the latest fork of the blockchain; when no competing blockchain fork present, not returned with endpoints that return Blockchains.
        /// </summary>
        [JsonProperty("last_fork_hash")]
        public string LastForkHash { get; set; }
    }
}
