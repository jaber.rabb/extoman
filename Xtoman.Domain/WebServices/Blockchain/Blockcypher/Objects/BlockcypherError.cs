﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Blockchain
{
    public class BlockcypherError
    {
        [JsonProperty("error")]
        public string Error { get; set; }
    }
}
