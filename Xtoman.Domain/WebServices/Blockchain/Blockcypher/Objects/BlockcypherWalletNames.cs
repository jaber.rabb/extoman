﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Blockchain
{
    public partial class BlockcypherWalletNames
    {
        [JsonProperty("wallet_names")]
        public List<string> WalletNames { get; set; }
    }

}
