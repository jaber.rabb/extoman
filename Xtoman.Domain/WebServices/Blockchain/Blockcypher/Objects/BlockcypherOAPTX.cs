﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Blockchain
{
    public class BlockcypherOAPTX
    {
        /// <summary>
        /// Version of Open Assets Protocol transaction. Typically 1.
        /// </summary>
        [JsonProperty("ver")]
        public long Ver { get; set; }

        /// <summary>
        /// Unique indentifier associated with this asset; can be used to query other transactions associated with this asset.
        /// </summary>
        [JsonProperty("assetid")]
        public string Assetid { get; set; }

        /// <summary>
        /// This transaction’s unique hash; same as the underlying transaction on the asset’s parent blockchain.
        /// </summary>
        [JsonProperty("hash")]
        public string Hash { get; set; }

        /// <summary>
        /// (Optional) Time this transaction was confirmed; only returned for confirmed transactions.
        /// </summary>
        [JsonProperty("confirmed")]
        public DateTimeOffset? Confirmed { get; set; }

        /// <summary>
        /// Time this transaction was received.
        /// </summary>
        [JsonProperty("received")]
        public DateTimeOffset Received { get; set; }

        /// <summary>
        /// (Optional) Associated hex-encoded metadata with this transaction, if it exists.
        /// </summary>
        [JsonProperty("oap_meta")]
        public string OapMeta { get; set; }

        /// <summary>
        /// true if this is an attempted double spend; false otherwise.
        /// </summary>
        [JsonProperty("double_spend")]
        public bool DoubleSpend { get; set; }

        /// <summary>
        /// Array of input data, which can be seen explicitly in the cURL example. Very similar to array of TXInputs, but with values related to assets instead of satoshis.
        /// </summary>
        [JsonProperty("inputs")]
        public List<BlockcypherOAPTXInput> Inputs { get; set; }

        /// <summary>
        /// Array of output data, which can be seen explicitly in the cURL example. Very similar to array of TXOutputs, but with values related to assets instead of satoshis.
        /// </summary>
        [JsonProperty("outputs")]
        public List<BlockcypherOAPTXOutput> Outputs { get; set; }
    }

    /// <summary>
    /// Very similar to array of TXInputs, but with values related to assets instead of satoshis.
    /// </summary>
    public class BlockcypherOAPTXInput
    {
        /// <summary>
        /// The previous transaction hash where this input was an output. Not present for coinbase transactions.
        /// </summary>
        [JsonProperty("prev_hash")]
        public string PrevHash { get; set; }

        /// <summary>
        /// The index of the output being spent within the previous transaction. Not present for coinbase transactions.
        /// </summary>
        [JsonProperty("output_index")]
        public long OutputIndex { get; set; }

        /// <summary>
        /// The address associated with this transaction input/output.
        /// </summary>
        [JsonProperty("address")]
        public string Address { get; set; }

        /// <summary>
        /// The value of the output being spent within the previous transaction. Not present for coinbase transactions.
        /// </summary>
        [JsonProperty("output_value")]
        public long OutputValue { get; set; }
    }

    /// <summary>
    /// Very similar to array of TXOutputs, but with values related to assets instead of satoshis.
    /// </summary>
    public class BlockcypherOAPTXOutput
    {
        /// <summary>
        /// The address associated with this transaction input/output. 
        /// </summary>
        [JsonProperty("address")]
        public string Address { get; set; }

        /// <summary>
        /// Value in this transaction output, in satoshis.
        /// </summary>
        [JsonProperty("value")]
        public long Value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("original_output_index")]
        public long OriginalOutputIndex { get; set; }
    }
}
