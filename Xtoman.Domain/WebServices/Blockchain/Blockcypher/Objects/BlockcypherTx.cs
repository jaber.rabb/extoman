﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Blockchain
{
    /// <summary>
    /// A TX represents the current state of a particular transaction from either a Block within a Blockchain, or an unconfirmed transaction that has yet to be included in a Block. Typically returned from the Unconfirmed Transactions and Transaction Hash endpoints.
    /// </summary>
    public partial class BlockcypherTx
    {
        /// <summary>
        /// The hash of the Block that contains this transaction.
        /// </summary>
        [JsonProperty("block_hash")]
        public string BlockHash { get; set; }

        /// <summary>
        /// Height of the block that contains this transaction. If this is an unconfirmed transaction, it will equal -1.
        /// </summary>
        [JsonProperty("block_height")]
        public long BlockHeight { get; set; }

        /// <summary>
        /// The hash of the transaction. While reasonably unique, using hashes as identifiers may be unsafe.
        /// </summary>
        [JsonProperty("hash")]
        public string Hash { get; set; }

        /// <summary>
        /// Array of bitcoin public addresses involved in the transaction.
        /// </summary>
        [JsonProperty("addresses")]
        public List<string> Addresses { get; set; }

        /// <summary>
        /// The total number of satoshis exchanged in this transaction.
        /// </summary>
        [JsonProperty("total")]
        public long Total { get; set; }

        /// <summary>
        /// The total number of fees—in satoshis—collected by miners in this transaction.
        /// </summary>
        [JsonProperty("fees")]
        public long Fees { get; set; }

        /// <summary>
        /// The size of the transaction in bytes.
        /// </summary>
        [JsonProperty("size")]
        public long Size { get; set; }

        /// <summary>
        /// The likelihood that this transaction will make it to the next block; reflects the preference level miners have to include this transaction. Can be high, medium or low.
        /// </summary>
        [JsonProperty("preference")]
        public string Preference { get; set; }

        /// <summary>
        /// Address of the peer that sent BlockCypher’s servers this transaction.
        /// </summary>
        [JsonProperty("relayed_by")]
        public string RelayedBy { get; set; }

        [JsonProperty("confirmed")]
        public DateTimeOffset Confirmed { get; set; }

        /// <summary>
        /// Time this transaction was received by BlockCypher’s servers.
        /// </summary>
        [JsonProperty("received")]
        public DateTimeOffset Received { get; set; }

        /// <summary>
        /// Version number, typically 1 for Bitcoin transactions.
        /// </summary>
        [JsonProperty("ver")]
        public long Ver { get; set; }

        /// <summary>
        /// Time when transaction can be valid. Can be interpreted in two ways: if less than 500 million, refers to block height. If more, refers to Unix epoch time.
        /// </summary>
        [JsonProperty("lock_time")]
        public long LockTime { get; set; }

        /// <summary>
        /// true if this is an attempted double spend; false otherwise.
        /// </summary>
        [JsonProperty("double_spend")]
        public bool DoubleSpend { get; set; }

        /// <summary>
        /// Total number of inputs in the transaction.
        /// </summary>
        [JsonProperty("vin_sz")]
        public long VinSz { get; set; }

        /// <summary>
        /// Total number of outputs in the transaction.
        /// </summary>
        [JsonProperty("vout_sz")]
        public long VoutSz { get; set; }

        /// <summary>
        /// Number of subsequent blocks, including the block the transaction is in. Unconfirmed transactions have 0 confirmations.
        /// </summary>
        [JsonProperty("confirmations")]
        public long Confirmations { get; set; }

        /// <summary>
        /// TXInput Array, limited to 20 by default.
        /// </summary>
        [JsonProperty("inputs")]
        public List<BlockcypherTxInput> Inputs { get; set; }

        /// <summary>
        /// TXOutput Array, limited to 20 by default.
        /// </summary>
        [JsonProperty("outputs")]
        public List<BlockcypherTxOutput> Outputs { get; set; }
    }
}
