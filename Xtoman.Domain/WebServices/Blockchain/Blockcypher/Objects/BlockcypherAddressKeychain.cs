﻿using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Blockchain
{
    /// <summary>
    /// An AddressKeychain represents an associated collection of public and private keys alongside their respective public address. Generally returned and used with the Generate Address Endpoint.
    /// </summary>
    public class BlockcypherAddressKeychain
    {
        /// <summary>
        /// Standard address representation.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Hex-encoded Public key.
        /// </summary>
        public string Public { get; set; }

        /// <summary>
        /// Hex-encoded Private key.
        /// </summary>
        public string Private { get; set; }

        /// <summary>
        /// Wallet import format, a common encoding for the private key.
        /// </summary>
        public string Wif { get; set; }

        /// <summary>
        /// (Optional) Array of public keys to provide to generate a multisig address.
        /// </summary>
        public List<string> PubKeys { get; set; }

        /// <summary>
        /// (Optional) If generating a multisig address, the type of multisig script; typically “multisig-n-of-m”, where n and m are integers.
        /// </summary>
        public string ScriptType { get; set; }

        /// <summary>
        /// (Optional) If generating an OAP address, this represents the parent blockchain’s underlying address (the typical address listed above).
        /// </summary>
        public string OriginalAddress { get; set; }

        /// <summary>
        /// (Optional) The OAP address, if generated using the Generate Asset Address Endpoint.
        /// </summary>
        public string OapAddress { get; set; }
    }
}
