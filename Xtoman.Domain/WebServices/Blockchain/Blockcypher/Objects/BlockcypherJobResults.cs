﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Blockchain
{
    public class BlockcypherJobResults
    {
        /// <summary>
        /// Current page of results.
        /// </summary>
        [JsonProperty("page")]
        public long Page { get; set; }

        /// <summary>
        /// true if there are more results in a separate page; false otherwise.
        /// </summary>
        [JsonProperty("more")]
        public bool More { get; set; }

        /// <summary>
        /// (Optional) URL to get the next page of results; only present if there are more results to show.
        /// </summary>
        [JsonProperty("next_page")]
        public Uri NextPage { get; set; }

        /// <summary>
        /// Results of analytics job; structure of results are dependent on engine-type of query, but are generally either strings of address hashes or JSON objects.
        /// </summary>
        [JsonProperty("results")]
        public List<BlockcypherJobResult> Results { get; set; }
    }

    /// <summary>
    /// Results of analytics job; structure of results are dependent on engine-type of query, but are generally either strings of address hashes or JSON objects.
    /// </summary>
    public class BlockcypherJobResult
    {
        [JsonProperty("DstAddr")]
        public string DstAddr { get; set; }

        [JsonProperty("SrcAddr")]
        public string SrcAddr { get; set; }

        [JsonProperty("TxHash")]
        public string TxHash { get; set; }

        [JsonProperty("Value")]
        public long Value { get; set; }
    }
}
