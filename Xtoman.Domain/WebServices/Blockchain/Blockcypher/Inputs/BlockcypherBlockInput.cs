﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Blockchain
{
    public class BlockcypherBlockInput
    {
        /// <summary>
        /// Filters response to only include transaction hashes after txstart in the block.
        /// </summary>
        [JsonProperty("txstart")]
        public int? TxStart { get; set; }

        /// <summary>
        /// Filters response to only include a maximum of limit transactions hashes in the block. Maximum value allowed is 500.
        /// </summary>
        [JsonProperty("limit")]
        public int? Limit { get; set; }
    }
}
