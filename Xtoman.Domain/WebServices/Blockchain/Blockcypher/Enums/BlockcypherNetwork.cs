﻿namespace Xtoman.Domain.WebServices.Blockchain
{
    public enum BlockcypherNetwork
    {
        Bitcoin,
        Litecoin,
        Dogecoin,
        Dash
    }
}
