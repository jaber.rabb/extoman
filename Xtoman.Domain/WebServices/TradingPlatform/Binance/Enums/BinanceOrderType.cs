﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    /// <summary>
    /// Different types of an order.
    /// </summary>
    public enum BinanceOrderType
    {
        [Display(Name = "Limit (محدود)")]
        LIMIT,
        [Display(Name = "Market (بازار)")]
        MARKET
    }
}
