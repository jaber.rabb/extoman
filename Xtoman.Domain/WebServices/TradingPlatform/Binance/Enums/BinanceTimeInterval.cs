﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    /// <summary>
    /// Time interval for the candlestick.
    /// </summary>
    public enum BinanceTimeInterval
    {
        [Display(Name = "1m")]
        Minutes_1,
        [Display(Name = "3m")]
        Minutes_3,
        [Display(Name = "5m")]
        Minutes_5,
        [Display(Name = "15m")]
        Minutes_15,
        [Display(Name = "30m")]
        Minutes_30,
        [Display(Name = "1h")]
        Hours_1,
        [Display(Name = "2h")]
        Hours_2,
        [Display(Name = "4h")]
        Hours_4,
        [Display(Name = "6h")]
        Hours_6,
        [Display(Name = "8h")]
        Hours_8,
        [Display(Name = "12h")]
        Hours_12,
        [Display(Name = "1d")]
        Days_1,
        [Display(Name = "3d")]
        Days_3,
        [Display(Name = "1w")]
        Weeks_1,
        [Display(Name = "1M")]
        Months_1
    }
}
