﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    /// <summary>
    /// Different sides of an order.
    /// </summary>
    public enum BinanceOrderSide
    {
        [Display(Name = "خرید")]
        BUY,
        [Display(Name = "فروش")]
        SELL
    }
}
