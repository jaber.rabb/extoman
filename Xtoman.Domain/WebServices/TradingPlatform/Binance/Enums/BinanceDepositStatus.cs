﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public enum BinanceDepositStatus
    {
        [Display(Name = "در حال انتظار")]
        Pending = 0,
        [Display(Name = "انجام شد")]
        Success = 1
    }
}
