﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public enum BinanceErrors
    {
        [Display(Name = "An unknown error occured while processing the request.")]
        UNKNOWN = -1000,
        [Display(Name = "Internal error; unable to process your request. Please try again.")]
        DISCONNECTED = -1001,
        [Display(Name = "You are not authorized to execute this request.")]
        UNAUTHORIZED = -1002,
        [Display(Name = "Too many requests.")]
        TOO_MANY_REQUESTS = -1003,
        [Display(Name = "An unexpected response was received from the message bus. Execution status unknown.")]
        UNEXPECTED_RESP = -1006,
        [Display(Name = "Timeout waiting for response from backend server. Send status unknown; execution status unknown.")]
        TIMEOUT = -1007,
        [Display(Name = "Unsupported order combination.")]
        UNKNOWN_ORDER_COMPOSITION = -1014,
        [Display(Name = "Too many new orders.")]
        TOO_MANY_ORDERS = -1015,
        [Display(Name = "This service is no longer available.")]
        SERVICE_SHUTTING_DOWN = -1016,
        [Display(Name = "This operation is not supported.")]
        UNSUPPORTED_OPERATION = -1020,
        [Display(Name = "Timestamp for this request is outside of the recvWindow or Timestamp for this request was 1000ms ahead of the server's time.")]
        INVALID_TIMESTAMP = -1021,
        [Display(Name = "Signature for this request is not valid.")]
        INVALID_SIGNATURE = -1022,
        [Display(Name = "Illegal characters found in a parameter.")]
        ILLEGAL_CHARS = -1100,
        [Display(Name = "Too many parameters sent for this endpoint or Duplicate values for a parameter detected.")]
        TOO_MANY_PARAMETERS = -1101,
        [Display(Name = "A mandatory parameter was not sent, was empty/null, or malformed.")]
        MANDATORY_PARAM_EMPTY_OR_MALFORMED = -1102,
        [Display(Name = "An unknown parameter was sent.")]
        UNKNOWN_PARAM = -1103,
        [Display(Name = "Not all sent parameters were read.")]
        UNREAD_PARAMETERS = -1104,
        [Display(Name = "A parameter was empty.")]
        PARAM_EMPTY = -1105,
        [Display(Name = "A parameter was sent when not required.")]
        PARAM_NOT_REQUIRED = -1106,
        [Display(Name = "Precision is over the maximum defined for this asset.")]
        BAD_PRECISION = -1111,
        [Display(Name = "No orders on book for symbol.")]
        NO_DEPTH = -1112,
        [Display(Name = "TimeInForce parameter sent when not required.")]
        TIF_NOT_REQUIRED = -1114,
        [Display(Name = "Invalid timeInForce.")]
        INVALID_TIF = -1115,
        [Display(Name = "Invalid orderType.")]
        INVALID_ORDER_TYPE = -1116,
        [Display(Name = "Invalid side.")]
        INVALID_SIDE = -1117,
        [Display(Name = "New client order ID was empty.")]
        EMPTY_NEW_CL_ORD_ID = -1118,
        [Display(Name = "Original client order ID was empty.")]
        EMPTY_ORG_CL_ORD_ID = -1119,
        [Display(Name = "Invalid interval.")]
        BAD_INTERVAL = -1120,
        [Display(Name = "Invalid symbol.")]
        BAD_SYMBOL = -1121,
        [Display(Name = "This listenKey does not exist.")]
        INVALID_LISTEN_KEY = -1125,
        [Display(Name = "Lookup interval is too big.")]
        MORE_THAN_XX_HOURS = -1127,
        [Display(Name = "Combination of optional parameters invalid.")]
        OPTIONAL_PARAMS_BAD_COMBO = -1128,
        [Display(Name = "Invalid data sent for a parameter.")]
        INVALID_PARAMETER = -1130,
        [Display(Name = "New order rejected.")]
        NEW_ORDER_REJECTED = -2010,
        [Display(Name = "Cancel rejected.")]
        CANCEL_REJECTED = -2011,
        [Display(Name = "Order does not exist.")]
        NO_SUCH_ORDER = -2013,
        [Display(Name = "API-key format invalid.")]
        BAD_API_KEY_FMT = -2014,
        [Display(Name = "Invalid API-key, IP, or permissions for action.")]
        REJECTED_MBX_KEY = -2015,
        [Display(Name = "No trading window could be found for the symbol. Try ticker/24hrs instead.")]
        NO_TRADING_WINDOW = -2016,
        [Display(Name = "Filter failure")]
        FILTER_FAILURE = -9
    }
}
