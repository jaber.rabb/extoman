﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public enum BinanceSystemStatus
    {
        Normal = 0,
        Maintenance = 1
    }
}
