﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public enum BinanceTradeStatus : byte
    {
        [Display(Name = "New")]
        NEW = 0,
        [Display(Name = "Partially filled")]
        PARTIALLY_FILLED = 1,
        [Display(Name = "Filled")]
        FILLED = 2,
        [Display(Name = "Canceled")]
        CANCELED = 4,
        [Display(Name = "Pending cancel")]
        PENDING_CANCEL = 8,
        [Display(Name = "Rejected")]
        REJECTED = 16,
        [Display(Name = "Expired")]
        EXPIRED = 32,
        [Display(Name = "Voided")]
        VOIDED = 64
    }
}
