﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public enum BinanceWithdrawStatus
    {
        [Display(Name = "ایمیل ارسال شد")]
        EmailSent = 0,
        [Display(Name = "کنسل شد")]
        Cancelled = 1,
        [Display(Name = "در انتظار تایید")]
        AwaitingApproval = 2,
        [Display(Name = "رد شد")]
        Rejected = 3,
        [Display(Name = "در حال انجام")]
        Processing = 4,
        [Display(Name = "خطا")]
        Failure = 5,
        [Display(Name = "انجام شد")]
        Completed = 6
    }
}
