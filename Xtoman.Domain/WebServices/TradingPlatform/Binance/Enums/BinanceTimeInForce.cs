﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    /// <summary>
    /// Different Time in force of an order.
    /// </summary>
    public enum BinanceTimeInForce
    {
        GTC,
        IOC
    }
}
