﻿using System;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceException : Exception
    {
        public BinanceException(string message) : base(message)
        {
        }
    }
}
