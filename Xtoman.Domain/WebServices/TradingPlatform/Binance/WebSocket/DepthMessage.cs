﻿using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceDepthMessage
    {
        public string EventType { get; set; }
        public long EventTime { get; set; }
        public string Symbol { get; set; }
        public int UpdateId { get; set; }
        public IEnumerable<BinanceOrderBookOffer> Bids { get; set; }
        public IEnumerable<BinanceOrderBookOffer> Asks { get; set; }
    }
}
