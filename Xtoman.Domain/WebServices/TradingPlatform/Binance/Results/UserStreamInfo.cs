﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceUserStreamInfo
    {
        [JsonProperty("listenKey")]
        public string ListenKey { get; set; }
    }
}
