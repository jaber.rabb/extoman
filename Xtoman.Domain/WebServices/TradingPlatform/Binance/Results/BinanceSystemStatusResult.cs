﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceSystemStatusResult
    {
        [JsonProperty("status")]
        public byte Status { get; set; }

        [JsonProperty("msg")]
        public string Msg { get; set; }

        [JsonIgnore]
        public BinanceSystemStatus StatusEnum => (BinanceSystemStatus)Status;
    }
}
