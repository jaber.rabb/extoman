﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceCoinInfo
    {
        [JsonProperty("coin")]
        public string Coin { get; set; }

        [JsonProperty("depositAllEnable")]
        public bool DepositAllEnable { get; set; }

        [JsonProperty("free")]
        public decimal Free { get; set; } // String Decimal

        [JsonProperty("freeze")]
        public decimal Freeze { get; set; } // String Decimal

        [JsonProperty("ipoable")]
        public decimal Ipoable { get; set; } // String Decimal

        [JsonProperty("ipoing")]
        public decimal Ipoing { get; set; } // String Decimal

        [JsonProperty("isLegalMoney")]
        public bool IsLegalMoney { get; set; }

        [JsonProperty("locked")]
        public decimal Locked { get; set; } // String Decimal

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("storage")]
        public decimal Storage { get; set; } // String Decimal

        [JsonProperty("trading")]
        public bool Trading { get; set; }

        [JsonProperty("withdrawAllEnable")]
        public bool WithdrawAllEnable { get; set; }

        [JsonProperty("withdrawing")]
        public decimal Withdrawing { get; set; } // String Decimal

        [JsonProperty("networkList")]
        public List<BinanceNetwork> NetworkList { get; set; }
    }
}
