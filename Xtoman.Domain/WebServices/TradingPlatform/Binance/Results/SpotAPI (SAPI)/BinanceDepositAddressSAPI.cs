﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceDepositAddressSAPI
    {
        public string Address { get; set; }
        public string Coin { get; set; }
        public string Tag { get; set; }
        public string Url { get; set; }
    }
}
