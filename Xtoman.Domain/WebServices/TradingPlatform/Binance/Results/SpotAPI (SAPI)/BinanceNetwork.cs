﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceNetwork
    {
        [JsonProperty("addressRegex")]
        public string AddressRegex { get; set; }

        [JsonProperty("coin")]
        public string Coin { get; set; }

        [JsonProperty("depositDesc")]
        public string DepositDesc { get; set; } // shown only when "depositEnable" is false.

        [JsonProperty("depositEnable")]
        public bool DepositEnable { get; set; }

        [JsonProperty("isDefault")]
        public bool IsDefault { get; set; }

        [JsonProperty("memoRegex")]
        public string MemoRegex { get; set; }

        [JsonProperty("minConfirm")]
        public int MinConfirm { get; set; } // min number for balance confirmation

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("network")]
        public string Network { get; set; }

        [JsonProperty("resetAddressStatus")]
        public bool ResetAddressStatus { get; set; }

        [JsonProperty("specialTips")]
        public string SpecialTips { get; set; } // For memo 

        [JsonProperty("unLockConfirm")]
        public int UnLockConfirm { get; set; } // confirmation number for balance unlock 

        [JsonProperty("withdrawDesc")]
        public string WithdrawDesc { get; set; } // shown only when "withdrawEnable" is false.

        [JsonProperty("withdrawEnable")]
        public bool WithdrawEnable { get; set; }

        [JsonProperty("withdrawFee")]
        public decimal WithdrawFee { get; set; } // string decimal

        [JsonProperty("withdrawMin")]
        public decimal WithdrawMin { get; set; } // string decimal

        [JsonProperty("withdrawMax")]
        public decimal WithdrawMax { get; set; }
    }
}
