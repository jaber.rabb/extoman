﻿using System.Collections.Generic;
using System.Linq;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceBalanceResultModel
    {
        public decimal TotalFreeBTCValue => Assets.Select(p => p.FreeBTCValue).DefaultIfEmpty(0).Sum();
        public decimal TotalFreeUSDValue => Assets.Select(p => p.FreeUSDValue).DefaultIfEmpty(0).Sum();
        public decimal TotalLockedBTCValue => Assets.Select(p => p.LockedBTCValue).DefaultIfEmpty(0).Sum();
        public decimal TotalLockedUSDValue => Assets.Select(p => p.LockedUSDValue).DefaultIfEmpty(0).Sum();
        public decimal TotalBTCValue => TotalFreeBTCValue + TotalLockedBTCValue;
        public decimal TotalUSDValue => TotalFreeUSDValue + TotalLockedUSDValue;
        public List<BinanceBalanceItemResultModel> Assets { get; set; }
    }

    public class BinanceBalanceItemResultModel
    {
        public string Name { get; set; }
        public decimal Free { get; set; }
        public decimal Locked { get; set; }
        public decimal FreeBTCValue { get; set; }
        public decimal FreeUSDValue { get; set; }
        public decimal LockedBTCValue { get; set; }
        public decimal LockedUSDValue { get; set; }
        public decimal UnitPriceInUSD { get; set; }
        public decimal UnitPriceInBTC { get; set; }
    }
}
