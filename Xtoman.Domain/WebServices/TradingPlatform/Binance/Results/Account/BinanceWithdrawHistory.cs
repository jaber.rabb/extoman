﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceWithdrawHistory
    {
        [JsonProperty("withdrawList")]
        public List<BinanceWithdraw> WithdrawList { get; set; }
        [JsonProperty("success")]
        public bool Success { get; set; }
    }

    public class BinanceWithdraw
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("amount")]
        public decimal Amount { get; set; }
        [JsonProperty("address")]
        public string Address { get; set; }
        [JsonProperty("txId")]
        public string TxId { get; set; }
        [JsonProperty("asset")]
        public string Asset { get; set; }
        [JsonProperty("applyTime")]
        public long ApplyTime { get; set; }
        /// <summary>
        /// (0:Email Sent - 1:Cancelled - 2:Awaiting Approval - 3:Rejected - 4:Processing - 5:Failure - 6:Completed)
        /// </summary>
        [JsonProperty("status")]
        public byte Status { get; set; }
        [JsonIgnore]
        public BinanceWithdrawStatus StatusEnum => (BinanceWithdrawStatus)Status;

        [JsonIgnore]
        public string OrderId { get; set; }
    }
}
