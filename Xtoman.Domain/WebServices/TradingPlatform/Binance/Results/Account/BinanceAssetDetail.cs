﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceAssetsDetails
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("assetDetail")]
        public Dictionary<string, BinanceAssetDetail> Assets { get; set; }
    }

    public class BinanceAssetDetail
    {
        [JsonProperty("minWithdrawAmount")]
        public decimal MinWithdrawAmount { get; set; }

        [JsonProperty("depositStatus")]
        public bool DepositStatus { get; set; }

        [JsonProperty("withdrawFee")]
        public decimal WithdrawFee { get; set; }

        [JsonProperty("withdrawStatus")]
        public bool WithdrawStatus { get; set; }

        [JsonProperty("depositTip")]
        public string DepositTip { get; set; }

        [JsonProperty("withdrawTip")]
        public string WithdrawTip { get; set; }
    }
}
