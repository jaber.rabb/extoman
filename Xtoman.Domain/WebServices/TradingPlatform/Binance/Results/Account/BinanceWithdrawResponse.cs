﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceWithdrawResponse
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("msg")]
        public string Msg { get; set; }
        [JsonProperty("success")]
        public bool Success { get; set; }
    }
}
