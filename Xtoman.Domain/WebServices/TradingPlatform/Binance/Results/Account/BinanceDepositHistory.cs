﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceDeposit
    {
        [JsonProperty("insertTime")]
        public long InsertTime { get; set; }
        [JsonProperty("amount")]
        public decimal Amount { get; set; }
        [JsonProperty("asset")]
        public string Asset { get; set; }
        /// <summary>
        /// 0 = Pending & 1 = Success
        /// </summary>
        [JsonProperty("status")]
        public byte Status { get; set; }
        [JsonProperty("address")]
        public string Address { get; set; }
        [JsonProperty("addressTag")]
        public string AddressTag { get; set; }
        [JsonProperty("txId")]
        public string TxId { get; set; }

        [JsonIgnore]
        public BinanceDepositStatus StatusEnum => (BinanceDepositStatus)Status;
    }

    public class BinanceDepositHistory
    {
        [JsonProperty("depositList")]
        public List<BinanceDeposit> DepositList { get; set; }
        [JsonProperty("success")]
        public bool Success { get; set; }
    }

}
