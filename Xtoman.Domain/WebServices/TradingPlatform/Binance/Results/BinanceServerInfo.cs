﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceServerInfo
    {
        [JsonProperty("serverTime")]
        public long ServerTime { get; set; }
    }
}
