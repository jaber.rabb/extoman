﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceAveragePrice
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("mins")]
        public int Mins { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("price")]
        public decimal Price { get; set; }
    }
}
