﻿using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceOrderBook
    {
        public long LastUpdateId { get; set; }
        public IEnumerable<BinanceOrderBookOffer> Bids { get; set; }
        public IEnumerable<BinanceOrderBookOffer> Asks { get; set; }
    }
}
