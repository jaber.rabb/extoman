﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public class BinanceOrderBookOffer
    {
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
    }
}
