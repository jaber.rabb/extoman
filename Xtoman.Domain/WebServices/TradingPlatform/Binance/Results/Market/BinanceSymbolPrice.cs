﻿using System;
using Newtonsoft.Json;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    public static class BinanceSymbolHelper
    {
        public static string[] quoteAssets = new string[] { "USDT", "PAX", "TUSD", "USDC", "EUR", "NGN", "RUB", "TRY", "USDS", "BTC", "ETH", "BNB", "BUSD", "TRX", "XRP", "ZAR", "BKRW", "IDRT" };
        public static string GetPair(string symbol)
        {
            if (symbol.Length == 6)
                return symbol.RemoveRight(3) + "_" + symbol.RemoveLeft(3);
            if (symbol.Length >= 5)
            {
                var quoteAsset = GetQuoteAsset(symbol);
                if (quoteAsset.HasValue())
                    return symbol.RemoveRight(quoteAsset.Length) + "_" + quoteAsset;
            }
            return "";
        }

        private static string GetQuoteAsset(string symbol)
        {
            var defaultQuote = "";
            symbol = symbol.ToUpper();
            foreach (var quote in quoteAssets)
                if (symbol.Contains(quote) && symbol.EndsWith(quote))
                    defaultQuote = quote;

            if (defaultQuote.HasValue())
                return defaultQuote;

            new ArgumentException($"Symbol is wrong! ({symbol})").LogError();
            return "";
        }
    }
    public class BinanceSymbolPrice
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        [JsonProperty("price")]
        public decimal Price { get; set; }

        private string _pair;
        [JsonIgnore]
        public string Pair => _pair ?? (_pair = BinanceSymbolHelper.GetPair(Symbol));
    }
}
