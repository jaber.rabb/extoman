﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    [Serializable]
    public class BinanceTradingRules
    {
        [JsonProperty("timezone")]
        public string Timezone { get; set; }
        [JsonProperty("serverTime")]
        public long ServerTime { get; set; }
        [JsonProperty("rateLimits")]
        public List<BinanceRateLimit> RateLimits { get; set; }
        [JsonProperty("symbols")]
        public List<BinanceSymbol> Symbols { get; set; }
    }
}
