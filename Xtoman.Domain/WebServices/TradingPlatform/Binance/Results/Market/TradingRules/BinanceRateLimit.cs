﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.TradingPlatform.Binance
{
    [Serializable]
    public class BinanceRateLimit
    {
        [JsonProperty("rateLimitType")]
        public string RateLimitType { get; set; }
        [JsonProperty("interval")]
        public string Interval { get; set; }
        [JsonProperty("limit")]
        public int Limit { get; set; }
    }
}
