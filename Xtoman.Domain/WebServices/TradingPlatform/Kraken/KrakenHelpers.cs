﻿using System;
using Xtoman.Domain.Models;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public static class KrakenHelpers
    {
        public static ECurrencyType? GetKrakenAssetEcurrencyType(string krakenAsset)
        {
            switch (krakenAsset)
            {
                case "BCH":
                    return ECurrencyType.BitcoinCash;
                case "DASH":
                    return ECurrencyType.Dash;
                case "XETC":
                    return ECurrencyType.EthereumClassic;
                case "XETH":
                    return ECurrencyType.Ethereum;
                case "XLTC":
                    return ECurrencyType.Litecoin;
                case "XBT":
                case "XXBT":
                    return ECurrencyType.Bitcoin;
                case "XXDG":
                    return ECurrencyType.Dogecoin;
                case "XXMR":
                    return ECurrencyType.Monero;
                case "XXRP":
                    return ECurrencyType.Ripple;
                case "XZEC":
                    return ECurrencyType.Zcash;
                case "ZUSD":
                    return ECurrencyType.USDollar;
                case "ZEUR":
                    return ECurrencyType.Euro;
                case "USDT":
                    return ECurrencyType.Tether;
            }
            new Exception($"Kraken asset '{krakenAsset}' not found in ECurrencyType").LogError();
            return null;
        }
    }
}
