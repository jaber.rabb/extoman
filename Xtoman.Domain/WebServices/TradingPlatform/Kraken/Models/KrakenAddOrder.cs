﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenAddOrder
    {
        /// <summary>
        /// Order description info.
        /// </summary>
        [JsonProperty("descr")]
        public KrakenOrderDescription Description { get; set; }

        /// <summary>
        /// Array of transaction ids for order (if order was added successfully).
        /// </summary>
        [JsonProperty("txid")]
        public List<string> TransactionIds { get; set; }
    }
}
