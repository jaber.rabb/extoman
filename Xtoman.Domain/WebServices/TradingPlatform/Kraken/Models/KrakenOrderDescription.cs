﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenOrderDescription
    {
        /// <summary>
        /// Order description.
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// Conditional close order description (if conditional close set).
        /// </summary>
        public string Close { get; set; }
    }
}
