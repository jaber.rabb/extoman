﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenOrderBook
    {
        /// <summary>
        /// Ask side array of array entries(0 = price, 1 = volume, 3 = timestamp)
        /// </summary>
        [JsonProperty("asks")]
        public List<decimal[]> Asks { get; set; }

        /// <summary>
        /// Bid side array of array entries(0 = price, 1 = volume, 3 = timestamp)
        /// </summary>
        [JsonProperty("bids")]
        public List<decimal[]> Bids { get; set; }
    }
}
