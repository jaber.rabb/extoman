﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenTicker
    {
        /// <summary>
        /// Ask decimal array (0 = price, 1 = whole lot volume, 2 = lot volume)
        /// </summary>
        [JsonProperty("a")]
        public decimal[] Ask { get; set; }

        /// <summary>
        /// Bid decimal array (0 = price, 1 = whole lot volume, 2 = lot volume)
        /// </summary>
        [JsonProperty("b")]
        public decimal[] Bid { get; set; }

        /// <summary>
        /// Last trade closed decimal array (0 = price, 1 = lot volume)
        /// </summary>
        [JsonProperty("c")]
        public decimal[] LastClosed { get; set; }

        /// <summary>
        /// Volume decimal array (0 = today, 1 = last 24 hours)
        /// </summary>
        [JsonProperty("v")]
        public decimal[] Volume { get; set; }

        /// <summary>
        /// Volume weighted average price decimal array (0 = today, 1 = last 24 hours)
        /// </summary>
        [JsonProperty("p")]
        public decimal[] VolumeWeightedAveragePrice { get; set; }

        /// <summary>
        /// Number of trades int array (0 = today, 1 = last 24 hours),
        /// </summary>
        [JsonProperty("t")]
        public long[] NumberOfTrades { get; set; }

        /// <summary>
        /// Low decimal array (0 = today, 1 = last 24 hours)
        /// </summary>
        [JsonProperty("l")]
        public decimal[] Low { get; set; }

        /// <summary>
        /// High decimal array (0 = today, 1 = last 24 hours)
        /// </summary>
        [JsonProperty("h")]
        public decimal[] High { get; set; }

        /// <summary>
        /// Today's opening price
        /// </summary>
        [JsonProperty("o")]
        public decimal OpeningPriceOfTheDay { get; set; }
    }
}
