﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenDepositAddress
    {
        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("tag")]
        public string Tag { get; set; }

        [JsonProperty("expiretm")]
        public long ExpireTime { get; set; }

        [JsonProperty("new")]
        public bool New { get; set; }
    }
}