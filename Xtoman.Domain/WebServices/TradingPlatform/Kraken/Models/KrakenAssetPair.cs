﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenAssetPair
    {
        [JsonProperty("altname")]
        public string AltName { get; set; }

        [JsonProperty("aclass_base")]
        public string AClassBase { get; set; }

        [JsonProperty("base")]
        public string Base { get; set; }

        [JsonProperty("aclass_quote")]
        public string AClassQuote { get; set; }

        [JsonProperty("quote")]
        public string Quote { get; set; }

        [JsonProperty("lot")]
        public string Lot { get; set; }

        [JsonProperty("pair_decimals")]
        public byte PairDecimals { get; set; }

        [JsonProperty("lot_decimals")]
        public byte LotDecimals { get; set; }

        [JsonProperty("lot_multiplier")]
        public byte LotMultiplier { get; set; }

        [JsonProperty("leverage_buy")]
        public List<byte> LeverageBuy { get; set; }

        [JsonProperty("leverage_sell")]
        public List<byte> LeverageSell { get; set; }

        [JsonProperty("fees")]
        public List<List<decimal>> Fees { get; set; }

        [JsonProperty("fees_maker")]
        public List<List<decimal>> FeesMaker { get; set; }

        [JsonProperty("fee_volume_currency")]
        public string FeeVolumeCurrency { get; set; }

        [JsonProperty("margin_call")]
        public long MarginCall { get; set; }

        [JsonProperty("margin_stop")]
        public long MarginStop { get; set; }
    }
}
