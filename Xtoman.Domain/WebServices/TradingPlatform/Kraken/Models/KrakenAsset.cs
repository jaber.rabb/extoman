﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenAsset
    {
        [JsonProperty("aclass")]
        public string AClass { get; set; }

        [JsonProperty("altname")]
        public string AltName { get; set; }

        [JsonProperty("decimals")]
        public byte Decimals { get; set; }

        [JsonProperty("display_decimals")]
        public byte DisplayDecimals { get; set; }
    }
}
