﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenDepositStatus
    {
        /// <summary>
        /// name of the deposit method used
        /// </summary>
        [JsonProperty("method")]
        public string Method { get; set; }

        /// <summary>
        /// asset class
        /// </summary>
        [JsonProperty("aclass")]
        public string AssetClass { get; set; }

        /// <summary>
        /// asset X-ISO4217-A3 code
        /// </summary>
        [JsonProperty("asset")]
        public string Asset { get; set; }

        /// <summary>
        /// reference id
        /// </summary>
        [JsonProperty("refid")]
        public string RefId { get; set; }

        /// <summary>
        /// method transaction id
        /// </summary>
        [JsonProperty("txid")]
        public string TransactionId { get; set; }

        /// <summary>
        /// method transaction information
        /// </summary>
        [JsonProperty("info")]
        public string Info { get; set; }

        /// <summary>
        /// amount deposited
        /// </summary>
        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        /// <summary>
        /// fees paid
        /// </summary>
        [JsonProperty("fee")]
        public decimal Fee { get; set; }

        /// <summary>
        /// unix timestamp when request was made
        /// </summary>
        [JsonProperty("time")]
        public long Time { get; set; }

        /// <summary>
        /// status of deposit: INITIAL, PENDING, SETTLED, SUCCESS, FAILURE, PARTIAL
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("status-prop")]
        public KrakenTransactionStatusProperties StatusProperties { get; set; }
    }

    public class KrakenTransactionStatusProperties
    {

    }
}
