﻿using Newtonsoft.Json;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    [JsonConverter(typeof(JArrayToObjectConverter))]
    public class KrakenTrade
    {
        public decimal Price { get; set; }

        public decimal Volume { get; set; }

        public double Time { get; set; }

        public string Side { get; set; }

        public string Type { get; set; }

        public string Misc { get; set; }
    }
}
