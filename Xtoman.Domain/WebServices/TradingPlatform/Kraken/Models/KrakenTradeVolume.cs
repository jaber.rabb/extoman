﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenTradeVolume
    {
        /// <summary>
        /// Volume currency.
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// Current discount volume.
        /// </summary>
        public decimal Volume { get; set; }

        /// <summary>
        /// Fee tier info (if requested).
        /// </summary>
        public Dictionary<string, KrakenFeeInfo> Fees { get; set; }

        /// <summary>
        /// Maker fee tier info (if requested) for any pairs on maker/taker schedule.
        /// </summary>
        public Dictionary<string, KrakenFeeInfo> FeesMaker { get; set; }
    }
}