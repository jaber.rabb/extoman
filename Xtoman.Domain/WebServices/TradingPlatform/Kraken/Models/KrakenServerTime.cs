﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenServerTime
    {
        [JsonProperty("unixtime")]
        public long Unixtime { get; set; }

        [JsonProperty("rfc1123")]
        public string Rfc1123 { get; set; }
    }
}
