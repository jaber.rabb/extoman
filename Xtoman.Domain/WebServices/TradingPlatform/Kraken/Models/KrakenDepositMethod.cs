﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenDepositMethod
    {
        /// <summary>
        /// Name of deposit method
        /// </summary>
        [JsonProperty("method")]
        public string Method { get; set; }
        /// <summary>
        /// Maximum net amount that can be deposited right now, or false if no limit
        /// </summary>
        [JsonProperty("limit")]
        public bool Limit { get; set; }
        /// <summary>
        /// Amount of fees that will be paid
        /// </summary>
        [JsonProperty("fee")]
        public decimal? Fee { get; set; }
        /// <summary>
        /// Whether or not method has an address setup fee (optional)
        /// </summary>
        [JsonProperty("address-setup-fee")]
        public decimal? AddressSetupFee { get; set; }

        /// <summary>
        /// Unknown property
        /// </summary>
        [JsonProperty("gen-address")]
        public bool GenerateAddress { get; set; }
    }
}
