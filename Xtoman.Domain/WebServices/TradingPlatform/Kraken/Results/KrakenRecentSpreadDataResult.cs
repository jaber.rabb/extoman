﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenRecentSpreadDataResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public List<string[]> Result { get; set; }
    }
}
