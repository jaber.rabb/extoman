﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenLedgersQueryResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public Dictionary<string, KrakenLedgerInfo> Result { get; set; }
    }
}
