﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenOpenOrdersResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public Dictionary<string, Dictionary<string, KrakenOrder>> Result { get; set; }
    }
}
