﻿using Newtonsoft.Json;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenRecentTradesResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public TimestampedDictionary<string, KrakenTrade[]> Result { get; set; }
    }
}
