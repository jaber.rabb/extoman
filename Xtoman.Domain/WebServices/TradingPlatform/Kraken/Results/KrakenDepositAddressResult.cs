﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenDepositAddressResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public List<KrakenDepositAddress> Result { get; set; }
    }
}
