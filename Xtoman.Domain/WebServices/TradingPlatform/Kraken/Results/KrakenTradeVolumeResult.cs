﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenTradeVolumeResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public KrakenTradeVolume Result { get; set; }
    }
}
