﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenAssetPairsResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public Dictionary<string, KrakenAssetPair> Result { get; set; }
    }
}
