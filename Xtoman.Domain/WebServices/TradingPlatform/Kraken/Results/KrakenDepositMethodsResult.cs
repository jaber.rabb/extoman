﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    [Serializable]
    public class KrakenDepositMethodsResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public List<KrakenDepositMethod> Result { get; set; }
    }
}
