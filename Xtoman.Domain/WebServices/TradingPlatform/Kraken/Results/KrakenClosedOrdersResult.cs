﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenClosedOrdersResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public KrakenClosedCountedResult Result { get; set; }
    }

    public class KrakenClosedCountedResult
    {
        [JsonProperty("closed")]
        public Dictionary<string, KrakenOrder> Closed { get; set; }

        [JsonProperty("count")]
        public int Count { get; set; }
    }
}
