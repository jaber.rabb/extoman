﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenLedgersInfoResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public KrakenLedgersInfoResultLedgers Result { get; set; }
    }

    public class KrakenLedgersInfoResultLedgers
    {
        public int Count { get; set; }

        [JsonProperty("ledger")]
        public Dictionary<string, KrakenLedgerInfo> Ledgers { get; set; }
    }
}
