﻿using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    [Serializable]
    public class KrakenBaseResult
    {
        public virtual KrakenGeneralResultType ResultType { get; set; }

        //Set only if ResultType = error
        [JsonProperty("error")]
        public ReadOnlyCollection<KrakenErrorString> Errors { get; set; }

        //Set only if ResultType = exception
        [JsonIgnore]
        public Exception Exception { get; set; }
    }
}
