﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenOpenPositionsResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public Dictionary<string, KrakenPositionInfo> Result { get; set; }
    }
}
