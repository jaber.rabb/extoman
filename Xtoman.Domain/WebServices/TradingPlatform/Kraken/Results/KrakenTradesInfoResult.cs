﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenTradesInfoResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public Dictionary<string, KrakenTradeInfo> Result { get; set; }
    }
}