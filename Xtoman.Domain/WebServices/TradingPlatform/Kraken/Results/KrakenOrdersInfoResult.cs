﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenOrdersInfoResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public Dictionary<string, KrakenOrder> Result { get; set; }
    }
}
