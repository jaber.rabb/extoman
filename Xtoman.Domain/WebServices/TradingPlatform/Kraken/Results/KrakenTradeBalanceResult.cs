﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenTradeBalanceResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public KrakenTradeBalance Result { get; set; }
    }
}