﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenCancelOrderResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public KrakenCancelOrder Result { get; set; }
    }
}
