﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenTradesHistoryResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public KrakenTradeInfo Result { get; set; }
    }
}
