﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenAddOrderResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public KrakenAddOrder Result { get; set; }
    }
}
