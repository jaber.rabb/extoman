﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenBalanceResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public Dictionary<string, decimal> Result { get; set; }
    }
}
