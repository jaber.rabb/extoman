﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenOrderResult : KrakenBaseResult
    {
        //public new KrakenOrderResultType ResultType { get; set; }
        public KrakenOrder Order { get; set; }
    }
}
