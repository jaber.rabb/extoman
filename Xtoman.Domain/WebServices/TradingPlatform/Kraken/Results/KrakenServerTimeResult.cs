﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenServerTimeResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public KrakenServerTime Result { get; set; }
    }
}
