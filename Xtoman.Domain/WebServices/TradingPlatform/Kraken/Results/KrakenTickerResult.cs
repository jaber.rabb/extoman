﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenTickerResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public Dictionary<string, KrakenTicker> Result { get; set; }
    }
}
