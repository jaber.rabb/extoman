﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenOrderBookResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public Dictionary<string, KrakenOrderBook> Result { get; set; }
    }
}
