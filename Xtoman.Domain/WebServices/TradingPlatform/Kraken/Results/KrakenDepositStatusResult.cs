﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenDepositStatusResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public List<KrakenDepositStatus> Result { get; set; }
    }
}
