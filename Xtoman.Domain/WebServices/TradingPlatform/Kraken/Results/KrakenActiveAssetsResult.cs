﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public class KrakenActiveAssetsResult : KrakenBaseResult
    {
        [JsonProperty("result")]
        public Dictionary<string, KrakenAsset> Result { get; set; }
    }
}
