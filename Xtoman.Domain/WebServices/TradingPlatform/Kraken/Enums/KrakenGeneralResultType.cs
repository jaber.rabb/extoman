﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public enum KrakenGeneralResultType
    {
        [Display(Name = "Error")]
        error,
        [Display(Name = "Exception")]
        exception,
        [Display(Name = "Success")]
        success
    }
}
