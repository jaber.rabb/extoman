﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public enum KrakenOrderFlag
    {
        /// <summary>
        /// Volume in quote currency (not available for leveraged orders)
        /// </summary>
        [Display(Name = "viqc", Description = "Volume in quote currency (not available for leveraged orders)")]
        VolumeInQuoteCurrency,
        /// <summary>
        /// Prefer fee in base currency
        /// </summary>
        [Display(Name = "fcib", Description = "Prefer fee in base currency")]
        PreferFeeInBaseCurrency,
        /// <summary>
        /// Prefer fee in quote currency
        /// </summary>
        [Display(Name = "fciq", Description = "Prefer fee in quote currency")]
        PreferFeeInQuoteCurrency,
        /// <summary>
        /// No market price protection
        /// </summary>
        [Display(Name = "nompp", Description = "No market price protection")]
        NoMarketPriceProtection,
        /// <summary>
        /// Post only order (available when ordertype = limit)
        /// </summary>
        [Display(Name = "post", Description = "Post only order (available when ordertype = limit)")]
        PostOnlyOrder
    }
}
