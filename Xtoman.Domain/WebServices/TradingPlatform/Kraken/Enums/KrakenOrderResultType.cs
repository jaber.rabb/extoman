﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public enum KrakenOrderResultType
    {
        [Display(Name = "Error")]
        error,
        [Display(Name = "Exception")]
        exception,
        [Display(Name = "Order not found!")]
        order_not_found,
        [Display(Name = "Success")]
        success,
    }
}
