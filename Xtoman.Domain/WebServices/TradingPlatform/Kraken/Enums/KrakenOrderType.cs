﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public enum KrakenOrderType
    {
        [Display(Name = "market")]
        Market,
        [Display(Name = "limit")]
        Limit,
        [Display(Name = "stop-loss")]
        StopLoss,
        [Display(Name = "take-profit")]
        TakeProfit,
        [Display(Name = "stop-loss-profit")]
        StopLossProfit,
        [Display(Name = "stop-loss-profit-limit")]
        StopLossProfitLimit,
        [Display(Name = "stop-loss-limit")]
        StopLossLimit,
        [Display(Name = "take-profit-limit")]
        TakeProfitLimit,
        [Display(Name = "trailing-stop")]
        TrailingStop,
        [Display(Name = "trailing-stop-limit")]
        TrailingStopLimit,
        [Display(Name = "stop-loss-and-limit")]
        StopLossAndLimit,
        [Display(Name = "settle-position")]
        SettlePosition
    }

    public static class KrakenOrderTypeExtension
    {
        public static TradeOrderType ToTradeOrderType(this KrakenOrderType orderType)
        {
            switch (orderType)
            {
                case KrakenOrderType.Market:
                    return TradeOrderType.Market;
                case KrakenOrderType.Limit:
                    return TradeOrderType.Limit;
            }
            throw new Exception("Kraken Order Type not defined!");
        }

        public static KrakenOrderType ToKrakenOrderType(this TradeOrderType? orderType)
        {
            if (orderType.HasValue)
                switch (orderType)
                {
                    case TradeOrderType.Market:
                        return KrakenOrderType.Market;
                    case TradeOrderType.Limit:
                        return KrakenOrderType.Limit;
                }

            throw new Exception("Order Type not defined or is null!");
        }
    }
}
