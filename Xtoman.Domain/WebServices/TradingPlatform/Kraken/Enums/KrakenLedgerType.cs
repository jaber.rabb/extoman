﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public enum KrakenLedgerType
    {
        /// <summary>
        /// all (default)
        /// </summary>
        [Display(Name = "all")]
        All,
        /// <summary>
        /// deposit
        /// </summary>
        [Display(Name = "deposit")]
        Deposit,
        /// <summary>
        /// withdrawal
        /// </summary>
        [Display(Name = "withdrawal")]
        Withdrawal,
        /// <summary>
        /// trade
        /// </summary>
        [Display(Name = "trade")]
        Trade,
        /// <summary>
        /// margin
        /// </summary>
        [Display(Name = "margin")]
        Margin
    }
}
