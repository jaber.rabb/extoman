﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public enum KrakenOrderSide
    {
        [Display(Name = "buy")]
        Buy,
        [Display(Name = "sell")]
        Sell
    }
}
