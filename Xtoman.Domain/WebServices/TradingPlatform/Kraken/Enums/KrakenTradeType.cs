﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Kraken
{
    public enum KrakenTradeType
    {
        /// <summary>
        /// all = all types (default)
        /// </summary>
        [Display(Name = "all")]
        All,
        /// <summary>
        /// any position = any position (open or closed)
        /// </summary>
        [Display(Name = "any position")]
        AnyPosition,
        /// <summary>
        /// closed position = positions that have been closed
        /// </summary>
        [Display(Name = "closed position")]
        ClosedPosition,
        /// <summary>
        /// closing position = any trade closing all or part of a position
        /// </summary>
        [Display(Name = "closing position")]
        ClosingPosition,
        /// <summary>
        /// no position = non-positional trades
        /// </summary>
        [Display(Name = "no position")]
        NoPosition
    }
}
