﻿namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public enum HitBTCTransactionType
    {
        /// <summary>
        /// crypto withdraw transaction
        /// </summary>
        payout,
        /// <summary>
        /// crypto deposit transaction
        /// </summary>
        payin,
        deposit,
        withdraw,
        bankToExchange,
        exchangeToBank
    }
}
