﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public enum HitBTCOrderStatus
    {
        [Display(Name = "New")]
        neworder,
        [Display(Name = "Suspended")]
        suspended,
        [Display(Name = "Partialy Filled")]
        partiallyFilled,
        [Display(Name = "Filled")]
        filled,
        [Display(Name = "Canceled")]
        canceled,
        [Display(Name = "Expired")]
        expired
    }
}
