﻿namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public enum HitBTCTransferType
    {
        bankToExchange,
        exchangeToBank
    }
}
