﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public enum HitBTCTimeInForce
    {
        /// <summary>
        /// Good till cancel. GTC order won't close until it is filled. 
        /// </summary>
        [Display(Name = "Good till cancel", Description = "GTC order won't close until it is filled.")]
        GTC,
        /// <summary>
        ///  An immediate or cancel order is an order to buy or sell that must be executed immediately, and any portion of the order that cannot be immediately filled is cancelled.
        /// </summary>
        [Display(Name = "Immediate or cancel order", Description = "An immediate or cancel order is an order to buy or sell that must be executed immediately, and any portion of the order that cannot be immediately filled is cancelled.")]
        IOC,
        /// <summary>
        /// Fill or kill is a type of time-in-force designation used in securities trading that instructs a brokerage to execute a transaction immediately and completely or not at all.
        /// </summary>
        [Display(Name = "Fill or kill", Description = "Fill or kill is a type of time-in-force designation used in securities trading that instructs a brokerage to execute a transaction immediately and completely or not at all. ")]
        FOK,
        /// <summary>
        /// keeps the order active until the end of the trading day in UTC. 
        /// </summary>
        [Display(Name = "Day", Description = "keeps the order active until the end of the trading day in UTC. ")]
        Day,
        /// <summary>
        /// Good till date specified in expireTime
        /// </summary>
        [Display(Name = "Good till date", Description = "Good till date specified in expireTime")]
        GTD
    }
}
