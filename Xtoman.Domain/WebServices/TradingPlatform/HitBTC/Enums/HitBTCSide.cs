﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public enum HitBTCSide
    {
        [Display(Name = "Sell")]
        sell,
        [Display(Name = "Buy")]
        buy
    }
}
