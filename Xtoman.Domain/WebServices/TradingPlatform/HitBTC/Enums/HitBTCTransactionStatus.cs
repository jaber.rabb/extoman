﻿namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public enum HitBTCTransactionStatus
    {
        pending,
        failed,
        success
    }
}
