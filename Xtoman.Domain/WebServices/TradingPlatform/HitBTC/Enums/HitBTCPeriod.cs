﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public enum HitBTCPeriod
    {
        [Display(Name = "M1")]
        Minute_1,
        [Display(Name = "M3")]
        Minute_3,
        [Display(Name = "M5")]
        Minute_5,
        [Display(Name = "M30")]
        Minute_30,
        [Display(Name = "H1")]
        Hour_1,
        [Display(Name = "H4")]
        Hour_4,
        [Display(Name = "D1")]
        Day_1,
        [Display(Name = "D7")]
        Day_7,
        [Display(Name = "1M")]
        Month_1
    }
}
