﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public enum HitBTCOrderType
    {
        [Display(Name = "Limit")]
        limit,
        [Display(Name = "Market")]
        market,
        [Display(Name = "Stop-Limit")]
        stopLimit,
        [Display(Name = "Stop-Market")]
        stopMarket
    }
}
