﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public enum HitBTCSort
    {
        [Display(Name = "Ascending")]
        ASC,
        [Display(Name = "Descending")]
        DESC
    }
}
