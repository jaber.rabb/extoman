﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public class HitBTCBalance : HitBTCError
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        /// <summary>
        /// <para>For Trading Balance: Amount available for trading or transfer to main account</para>
        /// <para>For Account Balance: Amount available for withdraw or transfer to trading account</para>
        /// </summary>
        [JsonProperty("available")]
        public decimal Available { get; set; }

        /// <summary>
        /// <para>For Trading Balance: Amount reserved for active orders or incomplete transfers to main account</para>
        /// <para>For Account Balance: Amount reserved for incomplete transactions</para>
        /// </summary>
        [JsonProperty("reserved")]
        public decimal Reserved { get; set; }
    }
}
