﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public class HitBTCOrderBook : HitBTCError
    {
        [JsonProperty("ask")]
        public List<HitBTCAskBid> Ask { get; set; }
        [JsonProperty("bid")]
        public List<HitBTCAskBid> Bid { get; set; }
    }
}
