﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public class HitBTCOrder : HitBTCError
    {
        /// <summary>
        /// Unique identifier for Order as assigned by exchange
        /// </summary>
        [JsonProperty("id")]
        public long Id { get; set; }

        /// <summary>
        /// Unique identifier for Order as assigned by trader. Uniqueness must be guaranteed within a single trading day, including all active orders.
        /// </summary>
        [JsonProperty("clientOrderId")]
        public string ClientOrderId { get; set; }

        /// <summary>
        /// Trading symbol
        /// </summary>
        [JsonProperty("symbol")]
        public string Symbol { get; set; }

        /// <summary>
        /// sell | buy
        /// </summary>
        [JsonProperty("side")]
        public string Side { get; set; }
        [JsonIgnore]
        public HitBTCSide SideEnum => (HitBTCSide)Enum.Parse(typeof(HitBTCSide), Side);

        /// <summary>
        /// new, suspended, partiallyFilled, filled, canceled, expired
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonIgnore]
        public HitBTCOrderStatus StatusEnum => (HitBTCOrderStatus)Enum.Parse(typeof(HitBTCOrderStatus),
            Status.Equals("new", StringComparison.InvariantCultureIgnoreCase) ? "neworder" : Status);

        /// <summary>
        /// limit, market, stopLimit, stopMarket
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonIgnore]
        public HitBTCOrderType TypeEnum => (HitBTCOrderType)Enum.Parse(typeof(HitBTCOrderType), Type);

        /// <summary>
        /// Time in force is a special instruction used when placing a trade to indicate how long an order will remain active before it is executed or expires 
        /// <para>GTC - Good till cancel.GTC order won't close until it is filled. </para>
        /// <para>IOC - An immediate or cancel order is an order to buy or sell that must be executed immediately, and any portion of the order that cannot be immediately filled is cancelled.</para>
        /// <para>FOK - Fill or kill is a type of time-in-force designation used in securities trading that instructs a brokerage to execute a transaction immediately and completely or not at all.</para>
        /// <para>Day - keeps the order active until the end of the trading day in UTC.</para>
        /// <para>GTD - Good till date specified in expireTime.</para>
        /// </summary>
        [JsonProperty("timeInForce")]
        public string TimeInForce { get; set; }
        /// <summary>
        /// Time in force is a special instruction used when placing a trade to indicate how long an order will remain active before it is executed or expires 
        /// <para>GTC - Good till cancel.GTC order won't close until it is filled. </para>
        /// <para>IOC - An immediate or cancel order is an order to buy or sell that must be executed immediately, and any portion of the order that cannot be immediately filled is cancelled.</para>
        /// <para>FOK - Fill or kill is a type of time-in-force designation used in securities trading that instructs a brokerage to execute a transaction immediately and completely or not at all.</para>
        /// <para>Day - keeps the order active until the end of the trading day in UTC.</para>
        /// <para>GTD - Good till date specified in expireTime.</para>
        /// </summary>
        [JsonIgnore]
        public HitBTCTimeInForce TimeInForceEnum { get; set; }

        /// <summary>
        /// Order quantity
        /// </summary>
        [JsonProperty("quantity")]
        public decimal Quantity { get; set; }

        /// <summary>
        /// Order price
        /// </summary>
        [JsonProperty("price")]
        public decimal? Price { get; set; }

        /// <summary>
        /// Cumulative executed quantity
        /// </summary>
        [JsonProperty("cumQuantity")]
        public decimal CumQuantity { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTime? UpdatedAt { get; set; }

        [JsonProperty("stopPrice")]
        public decimal? StopPrice { get; set; }

        [JsonProperty("expireTime")]
        public DateTime? ExpireTime { get; set; }
    }
}
