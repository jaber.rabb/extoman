﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public class HitBTCAskBid
    {
        [JsonProperty("price")]
        public decimal Price { get; set; }
        [JsonProperty("size")]
        public decimal Size { get; set; }
    }
}
