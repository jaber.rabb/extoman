﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public class HitBTCDepositAddress : HitBTCError
    {
        /// <summary>
        /// Address for deposit
        /// </summary>
        [JsonProperty("address")]
        public string Address { get; set; }

        /// <summary>
        /// Optional additional parameter. Required for deposit if persist
        /// </summary>
        [JsonProperty("paymentId")]
        public string PaymentId { get; set; }
    }
}
