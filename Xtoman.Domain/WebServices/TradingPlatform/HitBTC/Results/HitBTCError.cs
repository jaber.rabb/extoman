﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public class HitBTCError
    {
        [JsonProperty("Error")]
        public HitBTCErrorResult Error { get; set; }
    }

    public class HitBTCErrorResult
    {
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
