﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public class HitBTCTrade : HitBTCError
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("price")]
        public decimal Price { get; set; }
        [JsonProperty("quantity")]
        public decimal Quantity { get; set; }
        [JsonProperty("side")]
        public string Side { get; set; }
        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }
    }
}
