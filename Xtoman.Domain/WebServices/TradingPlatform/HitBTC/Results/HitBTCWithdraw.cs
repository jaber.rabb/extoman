﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public class HitBTCWithdrawTransfer : HitBTCError
    {
        /// <summary>
        /// Unique identifier for Transaction as assigned by exchange
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
