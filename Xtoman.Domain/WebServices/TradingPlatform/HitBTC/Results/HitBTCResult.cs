﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public class HitBTCResult : HitBTCError
    {
        /// <summary>
        /// True of request completed
        /// </summary>
        [Display(Name = "result")]
        public bool Result { get; set; }
    }
}
