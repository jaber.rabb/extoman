﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public class HitBTCTransactionHistory : HitBTCError
    {
        /// <summary>
        /// Unique identifier for Transaction as assigned by exchange
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Is the internal index value that represents when the entry was updated
        /// </summary>
        [JsonProperty("index")]
        public long Index { get; set; }

        /// <summary>
        /// Currency identifier.
        /// </summary>
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("fee")]
        public decimal? Fee { get; set; }

        [JsonProperty("networkFee")]
        public decimal? NetworkFee { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("paymentId")]
        public string PaymentId { get; set; }

        /// <summary>
        /// Transaction hash
        /// </summary>
        [JsonProperty("hash")]
        public string Hash { get; set; }

        /// <summary>
        /// One of: payout - crypto withdraw transaction, payin - crypto deposit transaction, deposit, withdraw, bankToExchange, exchangeToBank
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonIgnore]
        public HitBTCTransactionStatus StatusEnum => (HitBTCTransactionStatus)Enum.Parse(typeof(HitBTCTransactionStatus), Status);

        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonIgnore]
        public HitBTCTransactionType TypeEnum => (HitBTCTransactionType)Enum.Parse(typeof(HitBTCTransactionType), Type);

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTime? UpdatedAt { get; set; }
    }
}
