﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public class HitBTCCurrency : HitBTCError
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("fullName")]
        public string FullName { get; set; }
        [JsonProperty("crypto")]
        public bool Crypto { get; set; }
        [JsonProperty("payinEnabled")]
        public bool PayinEnabled { get; set; }
        [JsonProperty("payinPaymentId")]
        public bool PayinPaymentId { get; set; }
        [JsonProperty("payinConfirmations")]
        public int PayinConfirmations { get; set; }
        [JsonProperty("payoutEnabled")]
        public bool PayoutEnabled { get; set; }
        [JsonProperty("payoutIsPaymentId")]
        public bool PayoutIsPaymentId { get; set; }
        [JsonProperty("transferEnabled")]
        public bool TransferEnabled { get; set; }
    }
}
