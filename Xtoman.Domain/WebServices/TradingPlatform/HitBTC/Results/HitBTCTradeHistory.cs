﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public class HitBTCTradeHistory : HitBTCError
    {
        /// <summary>
        /// Unique identifier for Trade as assigned by exchange
        /// </summary>
        [JsonProperty("id")]
        public long Id { get; set; }

        /// <summary>
        /// Unique identifier for Order as assigned by trader.
        /// </summary>
        [JsonProperty("clientOrderId")]
        public string ClientOrderId { get; set; }

        /// <summary>
        /// Unique identifier for Order as assigned by exchange
        /// </summary>
        [JsonProperty("orderId")]
        public long OrderId { get; set; }

        /// <summary>
        /// Trading symbol
        /// </summary>
        [JsonProperty("symbol")]
        public string Symbol { get; set; }

        /// <summary>
        /// sell buy
        /// </summary>
        [JsonProperty("side")]
        public string Side { get; set; }
        [JsonIgnore]
        public HitBTCSide SideEnum => (HitBTCSide)Enum.Parse(typeof(HitBTCSide), Side);

        /// <summary>
        /// Trade quantity
        /// </summary>
        [JsonProperty("quantity")]
        public decimal Quantity { get; set; }

        /// <summary>
        /// Trade price
        /// </summary>
        [JsonProperty("price")]
        public decimal? Price { get; set; }

        /// <summary>
        /// Trade commission. Could be negative – reward. Fee currency see in symbol config
        /// </summary>
        [JsonProperty("fee")]
        public decimal Fee { get; set; }

        /// <summary>
        /// Trade timestamp
        /// </summary>
        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }
    }
}
