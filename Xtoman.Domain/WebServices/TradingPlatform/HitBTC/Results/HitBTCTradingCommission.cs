﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.HitBTC
{
    public class HitBTCTradingCommission : HitBTCError
    {
        /// <summary>
        /// Fee to those who take that liquidity takeLiquidityRate (The market taker)
        /// </summary>
        [JsonProperty("takeLiquidityRate")]
        public string TakeLiquidityRate { get; set; }

        /// <summary>
        /// Fee to those who provide liquidity provideLiquidityRate (The market maker)
        /// </summary>
        [JsonProperty("provideLiquidityRate")]
        public string ProvideLiquidityRate { get; set; }
    }
}
