﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexBalancesRequest : BitfinexGenericRequest
    {
        public BitfinexBalancesRequest(string nonce)
        {
            this.nonce = nonce;
            this.request = "/v1/balances";
        }
    }
}
