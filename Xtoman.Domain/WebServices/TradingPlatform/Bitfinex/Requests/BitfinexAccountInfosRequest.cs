﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexAccountInfosRequest : BitfinexGenericRequest
    {
        public BitfinexAccountInfosRequest(string nonce)
        {
            this.nonce = nonce;
            this.request = "/v1/account_infos";
        }
    }
}
