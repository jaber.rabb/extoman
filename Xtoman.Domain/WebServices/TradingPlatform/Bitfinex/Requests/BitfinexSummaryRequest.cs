﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexSummaryRequest : BitfinexGenericRequest
    {
        public BitfinexSummaryRequest(string nonce)
        {
            this.nonce = nonce;
            this.request = "/v1/summary";
        }
    }
}
