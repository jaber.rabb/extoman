﻿using System.Collections;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexGenericRequest
    {
        public string request;
        public string nonce;
        public ArrayList options = new ArrayList();
    }
}
