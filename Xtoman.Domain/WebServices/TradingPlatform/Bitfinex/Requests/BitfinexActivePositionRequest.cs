﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexActivePositionRequest : BitfinexGenericRequest
    {
        public BitfinexActivePositionRequest(string nonce)
        {
            this.nonce = nonce;
            this.request = "/v1/positions";
        }
    }
}
