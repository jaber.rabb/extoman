﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexOrderStatusRequest : BitfinexGenericRequest
    {
        public long order_id;
        public BitfinexOrderStatusRequest(string nonce, long orderId)
        {
            this.nonce = nonce;
            this.order_id = orderId;
            this.request = "/v1/order/status";
        }
    }
}
