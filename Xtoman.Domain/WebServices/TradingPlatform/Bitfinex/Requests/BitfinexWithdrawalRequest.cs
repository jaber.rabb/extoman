﻿using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexWithdrawalRequest : BitfinexGenericRequest
    {
        public string withdraw_type;
        public string walletselected;
        public decimal amount;
        public string address;
        public string payment_id;
        public string account_name;
        public string account_number;
        public string swift;
        public string bank_name;
        public string bank_address;
        public string bank_city;
        public string bank_country;
        public string detail_payment;
        public string expressWire;
        public string intermediary_bank_name;
        public string intermediary_bank_address;
        public string intermediary_bank_city;
        public string intermediary_bank_country;
        public string intermediary_bank_account;
        public string intermediary_bank_swift;

        public BitfinexWithdrawalRequest(string nonce, BitfinexWithdrawalInput input)
        {
            this.nonce = nonce;
            this.withdraw_type = input.WithdrawType.ToDisplay(DisplayProperty.Prompt);
            this.walletselected = input.WalletSelected.ToString().ToLower();
            this.amount = input.Amount;
            this.address = input.Address;
            this.payment_id = input.PaymentId;
            this.account_name = input.AccountName;
            this.account_number = input.AccountNumber;
            this.swift = input.Swift;
            this.bank_name = input.BankName;
            this.bank_address = input.BankAddress;
            this.bank_city = input.BankCity;
            this.bank_country = input.BankCountry;
            this.detail_payment = input.DetailPayment;
            this.expressWire = input.ExpressWire;
            this.intermediary_bank_name = input.IntermediaryBankName;
            this.intermediary_bank_address = input.IntermediaryBankAddress;
            this.intermediary_bank_city = input.IntermediaryBankCity;
            this.intermediary_bank_country = input.IntermediaryBankCountry;
            this.intermediary_bank_account = input.IntermediaryBankAccount;
            this.intermediary_bank_swift = input.IntermediaryBankSwift;
            this.request = "/v1/withdraw";
        }
    }
}
