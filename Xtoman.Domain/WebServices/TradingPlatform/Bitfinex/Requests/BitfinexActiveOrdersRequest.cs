﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexActiveOrdersRequest : BitfinexGenericRequest
    {
        public BitfinexActiveOrdersRequest(string nonce)
        {
            this.nonce = nonce;
            this.request = "/v1/orders";
        }
    }
}
