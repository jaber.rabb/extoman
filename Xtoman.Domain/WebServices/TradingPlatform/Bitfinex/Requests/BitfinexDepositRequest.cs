﻿using Newtonsoft.Json;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexDepositRequest : BitfinexGenericRequest
    {
        [JsonProperty("method")]
        public string method;
        public string wallet_name;
        public int renew;

        public BitfinexDepositRequest(string nonce, BitfinexMethod method, BitfinexWalletType walletType, int renew)
        {
            this.nonce = nonce;
            this.method = method.ToDisplay(DisplayProperty.Prompt);
            this.wallet_name = walletType.ToString().ToLower();
            this.renew = renew;
            this.request = "/v1/deposit/new";
        }
    }
}
