﻿using System;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexBalanceHistoryRequest : BitfinexGenericRequest
    {
        public string currency;
        public string since;
        public string until;
        public int limit;
        public string wallet;

        public BitfinexBalanceHistoryRequest(string nonce, BitfinexMethod currency, DateTime sinceUTC, DateTime untilUTC, int limit, BitfinexWalletType walletType)
        {
            this.nonce = nonce;
            this.currency = currency.ToDisplay(DisplayProperty.ShortName);
            this.since = sinceUTC.ConvertToUnixTimestamp().ToString();
            this.until = untilUTC.ConvertToUnixTimestamp().ToString();
            this.limit = limit;
            this.wallet = walletType.ToString().ToLower();
            this.request = "/v1/history";
        }
    }
}
