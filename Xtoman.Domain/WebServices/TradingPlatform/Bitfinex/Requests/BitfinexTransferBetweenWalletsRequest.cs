﻿using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexTransferBetweenWalletsRequest : BitfinexGenericRequest
    {
        public string walletfrom;
        public string walletto;
        public string currency;
        public decimal amount;

        public BitfinexTransferBetweenWalletsRequest(string nonce, BitfinexWalletType fromWalletType, BitfinexWalletType toWalletType, BitfinexMethod currency, decimal amount)
        {
            this.nonce = nonce;
            this.walletfrom = fromWalletType.ToString().ToLower();
            this.walletto = toWalletType.ToString().ToLower();
            this.currency = currency.ToDisplay(DisplayProperty.ShortName);
            this.amount = amount;
            this.request = "/v1/transfer";
        }
    }
}
