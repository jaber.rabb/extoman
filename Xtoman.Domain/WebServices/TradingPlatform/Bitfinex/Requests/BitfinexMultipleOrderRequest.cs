﻿using System.Collections.Generic;
using System.Linq;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexMultipleOrderRequest : BitfinexGenericRequest
    {
        public object orders;

        public BitfinexMultipleOrderRequest(string nonce, List<BitfinexOrderInput> newOrders)
        {
            this.nonce = nonce;
            this.orders = newOrders.Select(p =>
                    new {
                        symbol = p.Symbol.ToString(),
                        amount = p.Amount,
                        price = p.Price,
                        exchange = p.Exchange.ToString().ToLower(),
                        side = p.Side.ToString().ToLower(),
                        type = p.Type.ToDisplay(DisplayProperty.Prompt)
                    })
                .ToArray();
            this.request = "/v1/order/new/multi";
        }
    }
}
