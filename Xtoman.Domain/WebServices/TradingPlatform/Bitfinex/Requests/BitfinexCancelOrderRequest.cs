﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexCancelOrderRequest : BitfinexGenericRequest
    {
        public long order_id;
        public BitfinexCancelOrderRequest(string nonce, long orderId)
        {
            this.nonce = nonce;
            this.order_id = orderId;
            this.request = "/v1/order/cancel";
        }
    }
}
