﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexMarginInformationRequest : BitfinexGenericRequest
    {
        public BitfinexMarginInformationRequest(string nonce)
        {
            this.nonce = nonce;
            this.request = "/v1/margin_infos";
        }
    }
}
