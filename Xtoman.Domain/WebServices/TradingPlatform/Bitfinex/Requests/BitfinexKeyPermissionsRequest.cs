﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexKeyPermissionsRequest : BitfinexGenericRequest
    {
        public BitfinexKeyPermissionsRequest(string nonce)
        {
            this.nonce = nonce;
            this.request = "/v1/key_info";
        }
    }
}
