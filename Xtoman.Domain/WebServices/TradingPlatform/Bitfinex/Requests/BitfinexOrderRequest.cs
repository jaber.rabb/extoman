﻿using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexOrderRequest : BitfinexGenericRequest
    {
        public string symbol;
        public float amount;
        public float price;
        public string side;
        public string type;
        public string exchange;
        //public bool is_hidden = false;
        //public bool is_postonly = false;
        //public int use_all_available = 0;
        //public bool ocoorder = false;
        //public float buy_price_oco = 0;
        //public float sell_price_oco = 0;
        public BitfinexOrderRequest(string nonce, BitfinexSymbol symbol, float amount, float price, BitfinexOrderExchange exchange, BitfinexOrderSide side, BitfinexOrderType type)
        {
            this.symbol = symbol.ToString();
            this.amount = amount;
            this.price = price;
            this.exchange = exchange.ToString().ToLower();
            this.side = side.ToString().ToLower();
            this.type = type.ToDisplay(DisplayProperty.Prompt);
            this.nonce = nonce;
            this.request = "/v1/order/new";
        }
    }
}
