﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexCancelAllOrdersRequest : BitfinexGenericRequest
    {
        public BitfinexCancelAllOrdersRequest(string nonce)
        {
            this.nonce = nonce;
            this.request = "/v1/order/cancel/all";
        }
    }
}
