﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexAccountFeesRequest : BitfinexGenericRequest
    {
        public BitfinexAccountFeesRequest(string nonce)
        {
            this.nonce = nonce;
            this.request = "/v1/account_fees";
        }
    }
}
