﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public enum BitfinexMethod
    {
        [Display(Prompt = "usd", ShortName = "USD")]
        USDollar,
        [Display(Prompt = "bitcoin", ShortName = "BTC")]
        Bitcoin,
        [Display(Prompt = "litecoin", ShortName = "LTC")]
        Litecoin,
        [Display(Prompt = "ethereum", ShortName = "ETH")]
        Ethereum,
        [Display(Prompt = "tetheruso", ShortName = "USD")] //"tetheruso" deposit method works only for verified accounts
        Tether,
        [Display(Prompt = "ethereumc", ShortName = "ETC")]
        EthereumClassic,
        [Display(Prompt = "zcash", ShortName = "ZEC")]
        ZCash,
        [Display(Prompt = "monero", ShortName = "XMR")]
        Monero,
        [Display(Prompt = "iota", ShortName = "IOT")]
        IOTA,
        [Display(Prompt = "bcash", ShortName = "BCH")]
        BitcoinCash,
        [Display(Prompt = "neo", ShortName = "NEO")]
        NEO,
        [Display(Prompt = "ripple", ShortName = "XRP")]
        Ripple,
        [Display(Prompt = "dash", ShortName = "DSH")]
        Dash,
        [Display(Prompt = "bitcoingold", ShortName = "BTG")]
        BitcoinGold,
        [Display(Prompt = "qtum", ShortName = "QTM")]
        Qtum,
        [Display(Prompt = "omisego", ShortName = "OMG")]
        OmiseGo,
        [Display(Prompt = "qash", ShortName = "QSH")]
        QASH,
        [Display(Prompt = "santiment", ShortName = "SAN")]
        Santiment,
        [Display(Prompt = "eos", ShortName = "EOS")]
        EOS,
        [Display(Prompt = "metaverseetp", ShortName = "ETP")]
        MetaverseETP,
        [Display(Prompt = "aventus", ShortName = "AVT")]
        Aventus,
        [Display(Prompt = "eidoo", ShortName = "EDO")]
        Eidoo,
        [Display(Prompt = "data", ShortName = "DAT")]
        StreamrDATAcoin,
        [Display(Prompt = "yoyow", ShortName = "YYW")]
        YOYOW
    }
}