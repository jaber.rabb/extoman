﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public enum BitfinexOrderSide
    {
        Buy,
        Sell
    }
}
