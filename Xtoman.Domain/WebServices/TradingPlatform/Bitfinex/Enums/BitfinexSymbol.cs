﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public enum BitfinexSymbol
    {
        [Display(Name = "btcusd", Description = "Bitcoin/US Dollar")]
        BTCUSD,
        [Display(Name = "btceur", Description = "Bitcoin/Euro")]
        BTCEUR,
        [Display(Name = "ltcusd", Description = "Litecoin/US Dollar")]
        LTCUSD,
        [Display(Name = "ltcbtc", Description = "Litecoin/Bitcoin")]
        LTCBTC,
        [Display(Name = "ethusd", Description = "Ethereum/US Dollar")]
        ETHUSD,
        [Display(Name = "ethbtc", Description = "Ethereum/Bitcoin")]
        ETHBTC,
        [Display(Name = "etcusd", Description = "Ethereum/US Dollar")]
        ETCUSD,
        [Display(Name = "etcbtc", Description = "Ethereum/Bitcoin")]
        ETCBTC,
        [Display(Name = "rrtusd", Description = "Recovery Right Tokens/US Dollar")]
        RRTUSD,
        [Display(Name = "rrtbtc", Description = "Recovery Right Tokens/Bitcoin")]
        RRTBTC,
        [Display(Name = "zecusd", Description = "ZCash/US Dollar")]
        ZECUSD,
        [Display(Name = "zecbtc", Description = "ZCash/Bitcoin")]
        ZECBTC,
        [Display(Name = "xmrusd", Description = "Monero/US Dollar")]
        XMRUSD,
        [Display(Name = "xmrbtc", Description = "Monero/Bitcoin")]
        XMRBTC,
        [Display(Name = "dshusd", Description = "Dash/US Dollar")]
        DASHUSD,
        [Display(Name = "dshbtc", Description = "Dash/Bitcoin")]
        DASHBTC,
        [Display(Name = "xrpusd", Description = "Ripple/US Dollar")]
        XRPUSD,
        [Display(Name = "xrpbtc", Description = "Ripple/Bitcoin")]
        XRPBTC,
        [Display(Name = "iotusd", Description = "IOTA/US Dollar")]
        IOTAUSD,
        [Display(Name = "iotbtc", Description = "IOTA/Bitcoin")]
        IOTABTC,
        [Display(Name = "ioteth", Description = "IOTA/Ethereum")]
        IOTAETH,
        [Display(Name = "eosusd", Description = "EOS/US Dollar")]
        EOSUSD,
        [Display(Name = "eosbtc", Description = "EOS/Bitcoin")]
        EOSBTC,
        [Display(Name = "eoseth", Description = "EOS/Ethereum")]
        EOSETH,
        [Display(Name = "sanusd", Description = "Santiment Network Token/US Dollar")]
        SANUSD,
        [Display(Name = "sanbtc", Description = "Santiment Network Token/Bitcoin")]
        SANBTC,
        [Display(Name = "saneth", Description = "Santiment Network Token/Ethereum")]
        SANETH,
        [Display(Name = "omgusd", Description = "OmiseGO/US Dollar")]
        OMGUSD,
        [Display(Name = "omgbtc", Description = "OmiseGO/Bitcoin")]
        OMGBTC,
        [Display(Name = "omgeth", Description = "OmiseGO/Ethereum")]
        OMGETH,
        [Display(Name = "bchusd", Description = "Bitcoin Cash/US Dollar")]
        BCHUSD,
        [Display(Name = "bchbtc", Description = "Bitcoin Cash/Bitcoin")]
        BCHBTC,
        [Display(Name = "bcheth", Description = "Bitcoin Cash/Ethereum")]
        BCHETH,
        [Display(Name = "neousd", Description = "NEO/US Dollar")]
        NEOUSD,
        [Display(Name = "neobtc", Description = "NEO/Bitcoin")]
        NEOBTC,
        [Display(Name = "neoeth", Description = "NEO/Ethereum")]
        NEOETH,
        [Display(Name = "etpusd", Description = "Metaverse ETP/US Dollar")]
        ETPUSD,
        [Display(Name = "etpbtc", Description = "Metaverse ETP/Bitcoin")]
        ETPBTC,
        [Display(Name = "etpeth", Description = "Metaverse ETP/Ethereum")]
        ETPETH,
        [Display(Name = "qtmusd", Description = "QTUM/US Dollar")]
        QTUMUSD,
        [Display(Name = "qtmbtc", Description = "QTUM/Bitcoin")]
        QTUMBTC,
        [Display(Name = "qtmeth", Description = "QTUM/Ethereum")]
        QTUMETH,
        [Display(Name = "avtusd", Description = "Aventus/US Dollar")]
        AVTUSD,
        [Display(Name = "avtbtc", Description = "Aventus/Bitcoin")]
        AVTBTC,
        [Display(Name = "avteth", Description = "Aventus/Ethereum")]
        AVTETH,
        [Display(Name = "edousd", Description = "Eidoo/US Dollar")]
        EDOUSD,
        [Display(Name = "edobtc", Description = "Eidoo/Bitcoin")]
        EDOBTC,
        [Display(Name = "edoeth", Description = "Eidoo/Ethereum")]
        EDOETH,
        [Display(Name = "btgusd", Description = "Bitcoin Gold/US Dollar")]
        BTGUSD,
        [Display(Name = "btgbtc", Description = "Bitcoin Gold/Bitcoin")]
        BTGBTC,
        [Display(Name = "datusd", Description = "Streamr DATAcoin/US Dollar")]
        DATAUSD,
        [Display(Name = "datbtc", Description = "Streamr DATAcoin/Bitcoin")]
        DATABTC,
        [Display(Name = "dateth", Description = "Streamr DATAcoin/Ethereum")]
        DATAETH,
        [Display(Name = "qshusd", Description = "QASH/US Dollar")]
        QASHUSD,
        [Display(Name = "qshbtc", Description = "QASH/Bitcoin")]
        QASHBTC,
        [Display(Name = "qsheth", Description = "QASH/Ethereum")]
        QASHETH,
        [Display(Name = "yywusd", Description = "YOYOW/US Dollar")]
        YYWUSD,
        [Display(Name = "yywbtc", Description = "YOYOW/Bitcoin")]
        YYWBTC,
        [Display(Name = "yyweth", Description = "YOYOW/Ethereum")]
        YYWETH
    }
}
