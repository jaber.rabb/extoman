﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public enum BitfinexOrderType
    {
        [Display(Prompt = "market")]
        MarginMarket,
        [Display(Prompt = "limit")]
        MarginLimit,
        [Display(Prompt = "stop")]
        MarginStop,
        [Display(Prompt = "trailing-stop")]
        MarginTrailingStop,
        [Display(Prompt = "fill-or-kill")]
        MarginFillOrKill,
        [Display(Prompt = "exchange market")]
        ExchangeMarket,
        [Display(Prompt = "exchange limit")]
        ExchangeLimit,
        [Display(Prompt = "exchange stop")]
        ExchangeStop,
        [Display(Prompt = "exchange trailing-stop")]
        ExchangeTrailingStop,
        [Display(Prompt = "exchange fill-or-kill")]
        ExchangeFillOrKill
    }
}
