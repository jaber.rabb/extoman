﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public enum BitfinexWalletType
    {
        Trading,
        Exchange,
        Deposit
    }
}
