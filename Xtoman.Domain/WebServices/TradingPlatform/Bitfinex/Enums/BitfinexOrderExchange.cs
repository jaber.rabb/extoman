﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public enum BitfinexOrderExchange
    {
        Bitfinex,
        Bitstamp,
        All
    }
}
