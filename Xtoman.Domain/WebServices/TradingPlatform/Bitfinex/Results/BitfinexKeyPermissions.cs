﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexKeyPermissions
    {
        [JsonProperty("account")]
        public BitfinexKeyPermissionItem Account { get; set; }
        [JsonProperty("history")]
        public BitfinexKeyPermissionItem History { get; set; }
        [JsonProperty("orders")]
        public BitfinexKeyPermissionItem Orders { get; set; }
        [JsonProperty("positions")]
        public BitfinexKeyPermissionItem Positions { get; set; }
        [JsonProperty("funding")]
        public BitfinexKeyPermissionItem Funding { get; set; }
        [JsonProperty("wallets")]
        public BitfinexKeyPermissionItem Wallets { get; set; }
        [JsonProperty("withdraw")]
        public BitfinexKeyPermissionItem Withdraw { get; set; }
    }
}
