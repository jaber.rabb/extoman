﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexSummary
    {
        [JsonProperty("trade_vol_30d")]
        public List<BitfinexTradeVol30d> TradeVol30days { get; set; }
        [JsonProperty("funding_profit_30d")]
        public List<BitfinexFundingProfit30d> FundingProfit30days { get; set; }
        [JsonProperty("maker_fee")]
        public decimal MakerFee { get; set; }
        [JsonProperty("taker_fee")]
        public decimal TakerFee { get; set; }
    }
}
