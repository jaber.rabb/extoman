﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexTradeVol30d
    {
        [JsonProperty("curr")]
        public string Currency { get; set; }
        [JsonProperty("vol")]
        public decimal Volume { get; set; }
    }
}
