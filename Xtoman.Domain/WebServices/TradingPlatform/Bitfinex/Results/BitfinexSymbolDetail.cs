﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexSymbolDetail
    {
        [JsonProperty("pair")]
        public string Pair { get; set; }
        [JsonProperty("price_precision")]
        public int PricePrecision { get; set; }
        [JsonProperty("initial_margin")]
        public decimal InitialMargin { get; set; }
        [JsonProperty("minimum_margin")]
        public decimal MinimumMargin { get; set; }
        [JsonProperty("maximum_order_size")]
        public decimal MaximumOrderSize { get; set; }
        [JsonProperty("minimum_order_size")]
        public decimal MinimumOrderSize { get; set; }
        [JsonProperty("expiration")]
        public string Expiration { get; set; }
    }
}
