﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexFundingbook
    {
        [JsonProperty("bids")]
        public List<BitfinexFundingbookAskBid> Bids { get; set; }
        [JsonProperty("asks")]
        public List<BitfinexFundingbookAskBid> Asks { get; set; }
    }
}
