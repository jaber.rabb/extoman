﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexOrderbook
    {
        [JsonProperty("bids")]
        public List<BitfinexOrderbookAskBid> Bids { get; set; }
        [JsonProperty("asks")]
        public List<BitfinexOrderbookAskBid> Asks { get; set; }
    }
}
