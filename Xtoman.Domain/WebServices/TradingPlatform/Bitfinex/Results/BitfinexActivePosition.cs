﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexActivePosition
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("base")]
        public string Base { get; set; }
        [JsonProperty("amount")]
        public string Amount { get; set; }
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }
        [JsonProperty("swap")]
        public string Swap { get; set; }
        [JsonProperty("pl")]
        public string Pl { get; set; }
    }
}
