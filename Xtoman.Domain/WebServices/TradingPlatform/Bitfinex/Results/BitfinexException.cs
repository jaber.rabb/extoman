﻿using System.Net;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexException : WebException
    {
        public BitfinexException(WebException ex, string bitfinexMessage) :
            base(bitfinexMessage, ex)
        {
        }
    }
}
