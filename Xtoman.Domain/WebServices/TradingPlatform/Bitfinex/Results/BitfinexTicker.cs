﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexTicker
    {
        [JsonProperty("mid")]
        public string Mid { get; set; }
        [JsonProperty("bid")]
        public string Bid { get; set; }
        [JsonProperty("ask")]
        public string Ask { get; set; }
        [JsonProperty("last_price")]
        public string LastPrice { get; set; }
        [JsonProperty("low")]
        public string Low { get; set; }
        [JsonProperty("high")]
        public string High { get; set; }
        [JsonProperty("volume")]
        public string Volume { get; set; }
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }
    }
}
