﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexNewOrder : BitfinexOrderStatus
    {
        [JsonProperty("order_id")]
        public long OrderId { get; set; }
    }
}
