﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexResult
    {
        /// <summary>
        /// “success” or “error”
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }
        /// <summary>
        /// Success or error message
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
