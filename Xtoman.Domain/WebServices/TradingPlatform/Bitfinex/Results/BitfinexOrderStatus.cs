﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexOrderStatus
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        /// <summary>
        /// The symbol name the order belongs to
        /// </summary>
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        /// <summary>
        /// “bitfinex”, "all", "bitstamp"
        /// </summary>
        [JsonProperty("exchange")]
        public string Exchange { get; set; }
        /// <summary>
        /// The price the order was issued at (can be null for market orders)
        /// </summary>
        [JsonProperty("price")]
        public decimal? Price { get; set; }
        /// <summary>
        /// The average price at which this order as been executed so far. 0 if the order has not been executed at all
        /// </summary>
        [JsonProperty("avg_execution_price")]
        public decimal AverageExecutionPrice { get; set; }
        /// <summary>
        /// Either “buy” or “sell”
        /// </summary>
        [JsonProperty("side")]
        public string Side { get; set; }
        /// <summary>
        /// Either “market” / “limit” / “stop” / “trailing-stop” / “exchange market” / ...
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// The timestamp the order was submitted
        /// </summary>
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }
        /// <summary>
        /// Could the order still be filled?
        /// </summary>
        [JsonProperty("is_live")]
        public string IsLive { get; set; }
        /// <summary>
        /// Has the order been cancelled?
        /// </summary>
        [JsonProperty("is_cancelled")]
        public string IsCancelled { get; set; }
        /// <summary>
        /// Is the order hidden?
        /// </summary>
        [JsonProperty("is_hidden")]
        public bool IsHidden { get; set; }
        /// <summary>
        /// If the order is an OCO order, the ID of the linked order. Otherwise, null
        /// </summary>
        [JsonProperty("oco_order")]
        public long? OcoOrder { get; set; }
        /// <summary>
        /// For margin only true if it was forced by the system
        /// </summary>
        [JsonProperty("was_forced")]
        public string WasForced { get; set; }
        /// <summary>
        /// How much of the order has been executed so far in its history?
        /// </summary>
        [JsonProperty("executed_amount")]
        public decimal ExecutedAmount { get; set; }
        /// <summary>
        /// How much is still remaining to be submitted?
        /// </summary>
        [JsonProperty("remaining_amount")]
        public decimal RemainingAmount { get; set; }
        /// <summary>
        /// What was the order originally submitted for?
        /// </summary>
        [JsonProperty("original_amount")]
        public decimal OriginalAmount { get; set; }
    }
}
