﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexStatsItem
    {
        [JsonProperty("period")]
        public int Period { get; set; }
        [JsonProperty("volume")]
        public decimal Volume { get; set; }
    }
}
