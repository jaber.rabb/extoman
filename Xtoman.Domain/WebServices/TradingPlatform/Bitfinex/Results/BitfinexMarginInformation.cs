﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexMarginInformation
    {
        /// <summary>
        /// The USD value of all your trading assets (based on last prices)
        /// </summary>
        [JsonProperty("margin_balance")]
        public decimal MarginBalance { get; set; }
        /// <summary>
        /// Your tradable balance in USD (the maximum size you can open on leverage for this pair)
        /// </summary>
        [JsonProperty("tradable_balance")]
        public decimal TradableBalance { get; set; }
        /// <summary>
        /// The unrealized profit/loss of all your open positions
        /// </summary>
        [JsonProperty("unrealized_pl")]
        public decimal UnrealizedProfitLoss { get; set; }
        /// <summary>
        /// The margin funding used by all your open positions
        /// </summary>
        [JsonProperty("unrealized_swap")]
        public decimal UnrealizedSwap { get; set; }
        /// <summary>
        /// Your net value (the USD value of your trading wallet, including your margin balance, your unrealized Profit/Loss and margin funding)
        /// </summary>
        [JsonProperty("net_value")]
        public decimal NetValue { get; set; }
        /// <summary>
        /// The minimum net value to maintain in your trading wallet, under which all of your positions are fully liquidated
        /// </summary>
        [JsonProperty("required_margin")]
        public decimal RequiredMargin { get; set; }
        /// <summary>
        /// Leverage
        /// </summary>
        [JsonProperty("leverage")]
        public decimal Leverage { get; set; }
        /// <summary>
        /// The maintenance margin (% of the USD value of all of your open positions in the current pair to maintain)
        /// </summary>
        [JsonProperty("margin_requirement")]
        public decimal MarginRequirement { get; set; }
        /// <summary>
        /// The list of margin limits for each pair. The array gives you the following information, for each pair
        /// </summary>
        [JsonProperty("margin_limits")]
        public List<BitfinexMarginLimit> MarginLimits { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
