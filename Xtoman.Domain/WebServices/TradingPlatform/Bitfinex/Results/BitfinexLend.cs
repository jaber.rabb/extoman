﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexLend
    {
        /// <summary>
        /// Average rate of total funding received at fixed rates, ie past Flash Return Rate annualized
        /// </summary>
        [JsonProperty("rate")]
        public decimal Rate { get; set; }
        /// <summary>
        /// Total amount of open margin funding in the given currency
        /// </summary>
        [JsonProperty("amount_lent")]
        public decimal AmountLent { get; set; }
        /// <summary>
        /// Total amount of open margin funding used in a margin position in the given currency
        /// </summary>
        [JsonProperty("amount_used")]
        public decimal AmountUsed { get; set; }
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }
    }
}
