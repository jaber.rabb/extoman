﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexCancelAllOrders
    {
        public string message;
        public BitfinexCancelAllOrders(string message)
        {
            this.message = message;
        }
    }
}
