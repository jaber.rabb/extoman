﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexTrade
    {
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }
        [JsonProperty("tid")]
        public long Tid { get; set; }
        [JsonProperty("price")]
        public decimal Price { get; set; }
        [JsonProperty("amount")]
        public decimal Amount { get; set; }
        [JsonProperty("exchange")]
        public string Exchange { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
