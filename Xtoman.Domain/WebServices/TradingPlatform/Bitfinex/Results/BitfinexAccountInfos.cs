﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexAccountInfos
    {
        [JsonProperty("maker_fees")]
        public string MakerFees { get; set; }
        [JsonProperty("taker_fees")]
        public string TakerFees { get; set; }
        [JsonProperty("fees")]
        public List<BitfinexFee> Fees { get; set; }
    }
}
