﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexMarginLimit
    {
        [JsonProperty("on_pair")]
        public string OnPair { get; set; }
        [JsonProperty("initial_margin")]
        public decimal InitialMargin { get; set; }
        [JsonProperty("margin_requirement")]
        public decimal MarginRequirement { get; set; }
        [JsonProperty("tradable_balance")]
        public decimal TradableBalance { get; set; }
    }
}
