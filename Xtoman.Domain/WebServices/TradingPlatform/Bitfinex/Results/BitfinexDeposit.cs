﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexDeposit
    {
        /// <summary>
        /// “success” or “error”
        /// </summary>
        [JsonProperty("result")]
        public string Result { get; set; }
        [JsonProperty("method")]
        public string Method { get; set; }
        [JsonProperty("currency")]
        public string Currency { get; set; }
        /// <summary>
        /// The deposit address (or error message if result = “error”)
        /// </summary>
        [JsonProperty("address")]
        public string Address { get; set; }
    }
}
