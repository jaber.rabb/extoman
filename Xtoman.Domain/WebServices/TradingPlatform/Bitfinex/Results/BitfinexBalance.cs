﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexBalance
    {
        /// <summary>
        /// “trading”, “deposit” or “exchange”
        /// </summary>
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        /// <summary>
        /// Example: "btc", "usd", "eth", ...
        /// </summary>
        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }
        /// <summary>
        /// How much balance of this currency in this wallet
        /// </summary>
        [JsonProperty(PropertyName = "amount")]
        public decimal Amount { get; set; }
        /// <summary>
        /// How much X there is in this wallet that is available to trade
        /// </summary>
        [JsonProperty(PropertyName = "available")]
        public decimal Available { get; set; }
    }
}
