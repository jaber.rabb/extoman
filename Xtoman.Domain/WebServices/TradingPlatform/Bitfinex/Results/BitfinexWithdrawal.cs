﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexWithdrawal : BitfinexResult
    {
        /// <summary>
        /// ID of the withdrawal (0 if unsuccessful)
        /// </summary>
        [JsonProperty("withdrawal_id")]
        public long WithdrawalId { get; set; }
    }
}
