﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexKeyPermissionItem
    {
        [JsonProperty("read")]
        public bool? Read { get; set; }
        [JsonProperty("write")]
        public bool? Write { get; set; }
    }
}
