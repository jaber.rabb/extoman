﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexFee
    {
        [JsonProperty("pairs")]
        public string Pairs { get; set; }
        [JsonProperty("maker_fees")]
        public string MakerFees { get; set; }
        [JsonProperty("taker_fees")]
        public string TakerFees { get; set; }
    }
}
