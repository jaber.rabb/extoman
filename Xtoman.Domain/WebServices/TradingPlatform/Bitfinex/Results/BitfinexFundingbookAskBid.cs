﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexFundingbookAskBid
    {
        [JsonProperty("rate")]
        public string Rate { get; set; }
        [JsonProperty("amount")]
        public decimal Amount { get; set; }
        /// <summary>
        /// Minimum period for the margin funding contract
        /// </summary>
        [JsonProperty("period")]
        public int Period { get; set; }
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }
        /// <summary>
        /// “Yes” if the offer is at Flash Return Rate, “No” if the offer is at fixed rate
        /// </summary>
        [JsonProperty("frr")]
        public string Frr { get; set; }
        [JsonIgnore]
        public bool IsFlashReturnRate => Frr == "Yes" || Frr == "yes" ? true : false;
        [JsonIgnore]
        public bool IsFixedRate => Frr == "No" || Frr == "no" ? true : false;
    }
}
