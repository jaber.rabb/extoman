﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexDepositWithdrawalHistory
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("txid")]
        public long Txid { get; set; }
        [JsonProperty("currency")]
        public string Currency { get; set; }
        [JsonProperty("method")]
        public string Method { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("amount")]
        public decimal Amount { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("address")]
        public string Address { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }
        [JsonProperty("timestamp_created")]
        public string TimestampCreated { get; set; }
        [JsonProperty("fee")]
        public decimal Fee { get; set; }
    }
}
