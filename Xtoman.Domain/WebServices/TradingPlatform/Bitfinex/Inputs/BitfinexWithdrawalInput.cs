﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexWithdrawalInput
    {
        /// <summary>
        /// Required: 
        /// can be one of the following ['bitcoin', 'litecoin', 'ethereum', 'ethereumc', 'mastercoin', 'zcash', 'monero', 'wire', 'dash', 'ripple', 'eos', 'neo', 'aventus', 'qtum', 'eidoo']
        /// </summary>
        public BitfinexMethod WithdrawType { get; set; }
        /// <summary>
        /// The wallet to withdraw from, can be “trading”, “exchange”, or “deposit”.
        /// </summary>
        public BitfinexWalletType WalletSelected { get; set; }
        /// <summary>
        /// Amount to withdraw.
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// (1) Destination address for withdrawal.
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Optional hex string to identify a Monero transaction
        /// </summary>
        public string PaymentId { get; set; }
        /// <summary>
        /// Account name
        /// </summary>
        public string AccountName { get; set; }
        /// <summary>
        /// Account number
        /// </summary>
        public string AccountNumber { get; set; }
        /// <summary>
        /// The SWIFT code for your bank.
        /// </summary>
        public string Swift { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string BankCity { get; set; }
        public string BankCountry { get; set; }
        /// <summary>
        /// Message to beneficiary
        /// </summary>
        public string DetailPayment { get; set; }
        /// <summary>
        /// “1” to submit an express wire withdrawal, “0” or omit for a normal withdrawal
        /// </summary>
        public string ExpressWire { get; set; }
        public string IntermediaryBankName { get; set; }
        public string IntermediaryBankAccount { get; set; }
        public string IntermediaryBankCountry { get; set; }
        public string IntermediaryBankCity { get; set; }
        public string IntermediaryBankAddress { get; set; }
        public string IntermediaryBankSwift { get; set; }
    }
}
