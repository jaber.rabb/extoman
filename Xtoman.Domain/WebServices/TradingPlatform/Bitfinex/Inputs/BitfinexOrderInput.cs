﻿namespace Xtoman.Domain.WebServices.TradingPlatform.Bitfinex
{
    public class BitfinexOrderInput
    {
        public BitfinexSymbol Symbol { get; set; }
        public float Amount { get; set; }
        public float Price { get; set; }
        public BitfinexOrderExchange Exchange { get; set; }
        public BitfinexOrderSide Side { get; set; }
        public BitfinexOrderType Type { get; set; }
    }
}
