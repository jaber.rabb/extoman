﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public class FinnotechCardInformation : FinnotechBaseResponse
    {
        /// <summary>
        /// destCard: شماره کارت به صورت xxxx-xxxx-xxxx-{چهار رقم آخر شماره کارت}
        /// </summary>
        [JsonProperty("destCard")]
        public string DestCard { get; set; }

        /// <summary>
        /// name: نام صاحب کارت
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// result: کد نتیجه فراخوانی سرویس، برای اطلاع از مقادیر این فیلد به ResultText مراجعه کنید
        /// </summary>
        [JsonProperty("result")]
        public string Result { get; set; }

        /// <summary>
        /// نتیجه فراخوانی سرویس
        /// </summary>
        public string ResultText => FinnotechCardResponseCode.ToText(Result);

        /// <summary>
        /// description: توضیحات تکمیلی در مورد نتیجه
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// doTime: زمان انجام تراکنش به شمسی
        /// </summary>
        [JsonProperty("doTime")]
        public string DoTime { get; set; }

        /// <summary>
        /// trackId: کد پیگیری، اگر ارسال شده باشد همان مقدار برگردانده میشود و در غیر اینصورت یک رشته تصادفی تولید و برگردانده میشود
        /// </summary>
        [JsonProperty("trackId")]
        public Guid TrackId { get; set; }
    }
}
