﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public class FinnotechBaseResponse
    {
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("farsi_message")]
        public string FarsiMessage { get; set; }
        public int StatusCode { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonIgnore]
        public long HistoryId { get; set; }
    }
}
