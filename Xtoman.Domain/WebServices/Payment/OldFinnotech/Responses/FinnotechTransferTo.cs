﻿using System;
using Newtonsoft.Json;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public class FinnotechTransferTo : FinnotechBaseResponse
    {
        [JsonProperty("amount")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Amount { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("destinationFirstname")]
        public string DestinationFirstname { get; set; }

        [JsonProperty("destinationLastname")]
        public string DestinationLastname { get; set; }

        [JsonProperty("destinationNumber")]
        public string DestinationNumber { get; set; }

        [JsonProperty("inquiryDate")]
        public string InquiryDate { get; set; }

        [JsonProperty("inquirySequence")]
        public long InquirySequence { get; set; }

        [JsonProperty("inquiryTime")]
        public string InquiryTime { get; set; }

        [JsonProperty("paymentNumber")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long PaymentNumber { get; set; }

        [JsonProperty("refCode")]
        public string RefCode { get; set; }

        [JsonProperty("sourceFirstname")]
        public string SourceFirstname { get; set; }

        [JsonProperty("sourceLastname")]
        public string SourceLastname { get; set; }

        [JsonProperty("sourceNumber")]
        public string SourceNumber { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("trackId")]
        public string TrackId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        public FinnotechTransferType? TypeEnum => FinnotechTransferTypeHelper.GetByString(Type);
    }
}
