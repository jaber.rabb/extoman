﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public class FinnotechAuthentication : FinnotechBaseResponse
    {
        [JsonProperty("access_token")]
        public FinnotechAccessToken AccessToken { get; set; }

        /// <summary>
        /// مقدار این فیلد برابر Bearer خواهد بود
        /// </summary>
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
    }

    public class FinnotechAccessToken
    {
        /// <summary>
        /// توکنی که برای فراخوانی سرویس ها استفاده میشود
        /// </summary>
        [JsonProperty("value")]
        public string Value { get; set; }

        /// <summary>
        /// عمر توکن به میلی ثانیه
        /// </summary>
        [JsonProperty("lifeTime")]
        public long LifeTime { get; set; }

        /// <summary>
        /// زمان ساخت توکن به صورت تاریخ شمسی YYYYMMDDHHmmss
        /// </summary>
        [JsonProperty("creationDate")]
        public string CreationDate { get; set; }

        /// <summary>
        /// اگر عمر توکن تمام شود با استفاده از این توکن و فراخوانی سرویس توکن قادر خواهید بود توکن جدید دریافت نمایید
        /// </summary>
        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }
    }
}
