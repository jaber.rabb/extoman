﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public class FinnotechIdVerification : FinnotechBaseResponse
    {
        /// <summary>
        /// کد ملی فردی که میخواهید نام او را استعلام کنید
        /// </summary>
        [JsonProperty("nid")]
        [Display(Name = "کد ملی")]
        public string NationalId { get; set; }

        /// <summary>
        /// تاریخ تولد به صورت yyyy/mm/dd شمسی
        /// </summary>
        [JsonProperty("birthDate")]
        [Display(Name = "تاریخ تولد")]
        public string BirthDate { get; set; }

        /// <summary>
        /// وضعیت فراخوانی که میتواند یکی از موارد DONE|FAILED باشد
        /// </summary>
        [JsonProperty("status")]
        [Display(Name = "وضعیت فراخوانی")]
        public string Status { get; set; }

        /// <summary>
        /// نام و نام خانوادگی که میخواهید صحت آن را استعلام کنید
        /// </summary>
        [JsonProperty("fullName")]
        [Display(Name = "نام کامل")]
        public string FullName { get; set; }

        /// <summary>
        /// میزان شباهت نام کامل ارسال شده با نام صاحب کد ملی، مقدار عددی بین ۰ تا ۱۰۰ است
        /// </summary>
        [JsonProperty("fullNameSimilarity")]
        [Display(Name = "تشابه نام کامل")]
        public int? FullNameSimilarity { get; set; }

        /// <summary>
        /// نام صاحب کد ملی که میخواهید صحت آن را استعلام کنید
        /// </summary>
        [JsonProperty("firstName")]
        [Display(Name = "نام")]
        public string FirstName { get; set; }

        /// <summary>
        /// نام صاحب کدملی که میخواهید صحت آن را استعلام کنید، مقدار عددی بین ۰ تا ۱۰۰ است
        /// </summary>
        [JsonProperty("firstNameSimilarity")]
        [Display(Name = "تشابه نام")]
        public int? FirstNameSimilarity { get; set; }

        /// <summary>
        /// نام خانوادگی صاحب کد ملی که میخواهید صحت آن را استعلام کنید
        /// </summary>
        [JsonProperty("lastName")]
        [Display(Name = "تشابه نام خانوادگی")]
        public string LastName { get; set; }

        /// <summary>
        /// میزان شباهت نام خانوادگی صاحب کد ملی با نام خانوادگی ارسال شده، مقدار عددی بین ۰ تا ۱۰۰ است
        /// </summary>
        [JsonProperty("lastNameSimilarity")]
        public int? LastNameSimilarity { get; set; }

        /// <summary>
        /// نام و نام خانوادگی که میخواهید صحت آن را استعلام کنید
        /// </summary>
        [JsonProperty("fatherName")]
        public string FatherName { get; set; }

        /// <summary>
        /// میزان شباهت نام پدر صاحب کد ملی با نام پدر ارسال شده، مقدار عددی بین ۰ تا ۱۰۰ است
        /// </summary>
        [JsonProperty("fatherNameSimilarity")]
        [Display(Name = "تشابه نام پدر")]
        public int? FatherNameSimilarity { get; set; }

        /// <summary>
        /// وضعیت حیات که یکی از مقادیر زنده|مرده را دارد
        /// </summary>
        [JsonProperty("deathStatus")]
        [Display(Name = "وضعیت حیات")]
        public string DeathStatus { get; set; }

        /// <summary>
        /// کد پیگیری، اگر ارسال شده باشد همان مقدار برگردانده میشود و در غیر اینصورت یک رشته تصادفی تولید و برگردانده میشود
        /// </summary>
        [JsonProperty("trackId")]
        [Display(Name = "کد پیگیری")]
        public string TrackId { get; set; }
    }
}
