﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public class FinnotechPayaReport : FinnotechBaseResponse
    {
        [JsonProperty("transactions")]
        public List<FinnotechPayaTransaction> Transactions { get; set; }

        [JsonProperty("count")]
        public long Count { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
    }

    public class FinnotechPayaTransaction
    {
        [JsonProperty("transactionId")]
        public string TransactionId { get; set; }

        [JsonProperty("transactionDate")]
        public string TransactionDate { get; set; }

        [JsonProperty("traceId")]
        public string TraceId { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("senderName")]
        public string SenderName { get; set; }

        [JsonProperty("senderIban")]
        public string SenderIban { get; set; }

        [JsonProperty("senderBank")]
        public string SenderBank { get; set; }

        [JsonProperty("receiverIban")]
        public string ReceiverIban { get; set; }

        [JsonProperty("receiverName")]
        public string ReceiverName { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }

        [JsonProperty("payaCycle")]
        public string PayaCycle { get; set; }

        [JsonProperty("cycleTime")]
        public string CycleTime { get; set; }

        [JsonProperty("payId")]
        public string PayId { get; set; }

        [JsonProperty("returnCausality")]
        public string ReturnCausality { get; set; }

        [JsonProperty("returnId")]
        public string ReturnId { get; set; }

        [JsonProperty("rejected")]
        public string Rejected { get; set; }

        [JsonProperty("adDate")]
        public DateTimeOffset AdDate { get; set; }
    }
}
