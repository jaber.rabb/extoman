﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public class FinnotechIdDetail : FinnotechBaseResponse
    {
        [JsonProperty("message")]
        public new FinnotechIdDetailMessage Message { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("trackId")]
        public string TrackId { get; set; }
    }

    public class FinnotechIdDetailMessage
    {
        [JsonProperty("birthdate")]
        public string Birthdate { get; set; }

        [JsonProperty("deathStatus")]
        public string DeathStatus { get; set; }

        [JsonProperty("firstname")]
        public string Firstname { get; set; }

        [JsonProperty("lastname")]
        public string Lastname { get; set; }

        [JsonProperty("fatherName")]
        public string FatherName { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("nationalId")]
        public string NationalId { get; set; }

        [JsonProperty("idNo")]
        public long IdNo { get; set; }

        [JsonProperty("idSerie")]
        public string IdSerie { get; set; }

        [JsonProperty("idSerial")]
        public long IdSerial { get; set; }

        [JsonProperty("base64BinanryImage")]
        public string Base64BinanryImage { get; set; }

        [JsonProperty("operationStatus")]
        public bool OperationStatus { get; set; }

        [JsonProperty("message")]
        public string MessageMessage { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
    }
}
