﻿using Newtonsoft.Json;
using System;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public class FinnotechBalance : FinnotechBaseResponse
    {
        /// <summary>
        /// مانده واقعی حساب
        /// </summary>
        [JsonProperty("currentBalance")]
        public double CurrentBalance { get; set; }

        /// <summary>
        /// مانده قابل برداشت
        /// </summary>
        [JsonProperty("availableBalance")]
        public double AvailableBalance { get; set; }

        /// <summary>
        /// مانده
        /// </summary>
        [JsonProperty("effectiveBalance")]
        public double EffectiveBalance { get; set; }

        /// <summary>
        /// کد پیگیری، اگر ارسال شده باشد همان مقدار برگردانده میشود و در غیر اینصورت یک رشته تصادفی تولید و برگردانده میشود
        /// </summary>
        [JsonProperty("trackId")]
        public string TrackId { get; set; }

        /// <summary>
        /// وضعیت فراخوانی سرویس
        /// <para>DONE: فراخوانی موفق سرویس</para>
        /// <para>FAILED: فراخوانی ناموفق سرویس</para>
        /// </summary>
        [JsonProperty("state")]
        public string State { get; set; }

        [JsonIgnore]
        public FinnotechState StateEnum => State.HasValue() ? State.Equals("DONE", StringComparison.InvariantCultureIgnoreCase) ? FinnotechState.DONE : FinnotechState.FAILED : FinnotechState.FAILED;

        /// <summary>
        /// شماره حساب
        /// </summary>
        [JsonProperty("number")]
        public string Number { get; set; }
    }
}
