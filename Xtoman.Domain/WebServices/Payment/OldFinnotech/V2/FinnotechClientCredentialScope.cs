﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.Payment.Finnotech.V2
{
    public enum FinnotechClientCredentialScope
    {
        [Display(Name = "card:shahkar:verify", Description = "برای انطباق کد ملی و شماره موبایل از این سرویس استفاده کنید.")]
        CardMobileVerify,
        [Display(Name = "oak:iban-inquiry:get", Description = "سرویس اطلاعات شبا")]
        IBANInfo,
        [Display(Name = "card:information:get", Description = "برای استعلام شماره کارت های عضو شتاب از این سرویس استفاده کنید.")]
        CardInfo,
        [Display(Name = "oak:nid-verification:get", Description = "صحت سنجی کد ملی، این سرویس کد ملی، تارخ تولد و نام را دریافت میکند و شباهت نام ارسال شده را با نام واقعی صاحب کد ملی برمیگرداند. نام را هم به صورت یک فیلد و هم به صورت نام و نام خانوادگی جدا میتوانید بفرستید. این سرویس از ساعت ۲۰:۰۰ تا ساعت ۷:۰۰ غیر فعال است . در صورتی که در این بازه سرویس را فراخوانی کنید با خطای 'شما در بازه زمانی تعریف شده قرار ندارید' مواجه خواهید شد .")]
        IdCardVerification
    }
}
