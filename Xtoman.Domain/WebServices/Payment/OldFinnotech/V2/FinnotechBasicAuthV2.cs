﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.Finnotech.V2
{
    public class FinnotechBasicAuthV2
    {
        public FinnotechBasicAuthV2()
        {
            grant_type = "client_credentials";
        }
        /// <summary>
        ///  این مقدار باید برابر client_credentials قرار گیرد
        /// </summary>
        [JsonProperty("grant_type")]
        public string grant_type { get; set; }

        /// <summary>
        /// کد ملی ۱۰ رقمی شخص فراخوانی کننده که باید به کلاینت دسترسی داشته باشد
        /// </summary>
        [JsonProperty("nid")]
        public string nid { get; set; }

        /// <summary>
        /// لیست اسکوپ(ها)یی که قصد فراخوانی آن را دارید. اسکوپ‌ها با ',' از هم جدا شوند
        /// </summary>
        [JsonProperty("scopes")]
        public string scopes { get; set; }
    }

    public class FinnotechBasicAuthRefreshTokenV2
    {
        public FinnotechBasicAuthRefreshTokenV2()
        {
            grant_type = "refresh_token";
            token_type = "CLIENT-CREDENTIAL";
        }
        /// <summary>
        /// این مقدار باید برابر refresh_token قرار گیرد
        /// </summary>
        [JsonProperty("grant_type")]
        public string grant_type { get; set; }

        /// <summary>
        /// نوع توکن (CLIENT-CREDENTIAL یا CODE)
        /// </summary>
        [JsonProperty("token_type")]
        public string token_type { get; set; }

        [JsonProperty("refresh_token")]
        public string refresh_token { get; set; }
    }
}
