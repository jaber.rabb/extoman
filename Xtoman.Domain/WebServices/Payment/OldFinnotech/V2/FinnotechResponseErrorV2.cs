﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.Finnotech.V2
{
    public class FinnotechResponseErrorV2
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }
    }
}
