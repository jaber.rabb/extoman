﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Xtoman.Domain.WebServices.Payment.Finnotech.V2
{
    public class FinnotechResponseV2
    {
        /// <summary>
        /// DONE: فراخوانی موفق سرویس, FAILED: فراخوانی ناموفق سرویس
        /// </summary>
        [JsonProperty("status")]
        [JsonConverter(typeof(StringEnumConverter))]
        public FinnotechState Status { get; set; }

        /// <summary>
        /// جزییات خطا در صورت بروز خطا        
        /// </summary>
        [JsonProperty("error")]
        public FinnotechResponseErrorV2 Error { get; set; }

        /// <summary>
        /// کد پیگیری، اگر ارسال شده باشد همان مقدار برگردانده میشود و در غیر اینصورت یک رشته تصادفی تولید و برگردانده میشود
        /// </summary>
        [JsonProperty("trackId")]
        public string TrackId { get; set; }

        /// <summary>
        /// آی دی ردیف در دیتابیس ایکس تومن بعد از فراخوانی متد و افزودن به دیتا بیس اضافه می شود.
        /// </summary>
        [JsonIgnore]
        public long HistoryId { get; set; }

        /// <summary>
        /// کد پاسخ وضعیت درخواست متد: 200 موفق، 400 و 403 خطا
        /// </summary>
        [JsonIgnore]
        public int StatusCode { get; set; }
    }
    public class FinnotechResponseV2<T> : FinnotechResponseV2
    {
        /// <summary>
        /// نتیجه درصورتی که فراخوانی متد ناموفق باشد خالی است.
        /// </summary>
        [JsonProperty("result")]
        public T Result { get; set; }
    }
}
