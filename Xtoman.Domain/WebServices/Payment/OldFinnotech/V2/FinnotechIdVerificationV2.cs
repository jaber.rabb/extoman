﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.Finnotech.V2
{
    public class FinnotechIdVerificationV2
    {
        /// <summary>
        /// کد ملی فردی که میخواهید نام او را استعلام کنید
        /// </summary>
        [JsonProperty("nationalCode")]
        public string NationalCode { get; set; }

        /// <summary>
        /// تاریخ تولد به صورت yyyy/mm/dd
        /// </summary>
        [JsonProperty("birthDate")]
        public string BirthDate { get; set; }

        /// <summary>
        /// نام و نام خانوادگی که میخواهید صحت آن را استعلام کنید
        /// </summary>
        [JsonProperty("fullName")]
        public string FullName { get; set; }

        /// <summary>
        /// میزان شباهت نام کامل ارسال شده با نام صاحب کد ملی، مقدار عددی بین ۰ تا ۱۰۰ است
        /// </summary>
        [JsonProperty("fullNameSimilarity")]
        public int? FullNameSimilarity { get; set; }

        /// <summary>
        /// نام صاحب کد ملی که میخواهید صحت آن را استعلام کنید
        /// </summary>
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// نام صاحب کدملی که میخواهید صحت آن را استعلام کنید، مقدار عددی بین ۰ تا ۱۰۰ است
        /// </summary>
        [JsonProperty("firstNameSimilarity")]
        public int? FirstNameSimilarity { get; set; }

        /// <summary>
        /// نام خانوادگی صاحب کد ملی که میخواهید صحت آن را استعلام کنید
        /// </summary>
        [JsonProperty("lastName")]
        public string LastName { get; set; }

        /// <summary>
        /// میزان شباهت نام خانوادگی صاحب کد ملی با نام خانوادگی ارسال شده، مقدار عددی بین ۰ تا ۱۰۰ است
        /// </summary>
        [JsonProperty("lastNameSimilarity")]
        public int? LastNameSimilarity { get; set; }

        /// <summary>
        /// نام و نام خانوادگی که میخواهید صحت آن را استعلام کنید
        /// </summary>
        [JsonProperty("fatherName")]
        public string FatherName { get; set; }

        /// <summary>
        /// میزان شباهت نام پدر صاحب کد ملی با نام پدر ارسال شده، مقدار عددی بین ۰ تا ۱۰۰ است
        /// </summary>
        [JsonProperty("fatherNameSimilarity")]
        public int? FatherNameSimilarity { get; set; }

        /// <summary>
        /// وضعیت حیات که یکی از مقادیر زنده|مرده را دارد
        /// </summary>
        [JsonProperty("deathStatus")]
        public string DeathStatus { get; set; }
    }
}
