﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.Finnotech.V2
{
    public class FinnotechAuthenticationV2
    {
        /// <summary>
        /// توکنی که برای فراخوانی سرویس‌ها استفاده میشود
        /// </summary>
        [JsonProperty("value")]
        public string Value { get; set; }

        /// <summary>
        /// آرایه ای از اسکوپ‌های مجاز توکن
        /// </summary>
        [JsonProperty("scopes")]
        public List<string> Scopes { get; set; }

        /// <summary>
        /// عمر توکن به میلی ثانیه
        /// </summary>
        [JsonProperty("lifeTime")]
        public long LifeTime { get; set; }

        /// <summary>
        /// زمان ساخت توکن به صورت تاریخ شمسی YYYYMMDDHHmmss
        /// </summary>
        [JsonProperty("creationDate")]
        public string CreationDate { get; set; }

        /// <summary>
        /// اگر عمر توکن تمام شود با استفاده از این توکن و فراخوانی سرویس توکن قادر خواهید بود توکن جدید دریافت نمایید
        /// </summary>
        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }
    }
}
