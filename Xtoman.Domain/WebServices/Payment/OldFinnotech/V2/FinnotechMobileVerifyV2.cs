﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.Finnotech.V2
{
    public class FinnotechMobileVerifyV2
    {
        /// <summary>
        /// نتیجه انطباق کد ملی و شماره موبایل، در صورتی که شماره موبایل ارسالی متعلق به کد ملی ارسال شده باشد، صحیح خواهد بود.
        /// </summary>
        [JsonProperty("isValid")]
        public bool IsValid { get; set; }
    }
}
