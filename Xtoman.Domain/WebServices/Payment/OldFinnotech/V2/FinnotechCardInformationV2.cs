﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.Finnotech.V2
{
    public class FinnotechCardInformationV2
    {
        /// <summary>
        /// شماره کارت به صورت xxxx-xxxx-xxxx-{چهار رقم آخر شماره کارت}
        /// </summary>
        [JsonProperty("destCard")]
        public string DestCard { get; set; }

        /// <summary>
        /// نام صاحب کارت
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// نتیجه فراخوانی سرویس، برای اطلاع از مقادیر این فیلد به اینجا مراجعه کنید
        /// </summary>
        [JsonProperty("result")]
        public string Result { get; set; }

        /// <summary>
        /// نتیجه فراخوانی سرویس
        /// </summary>
        public string ResultText => FinnotechCardResponseCode.ToText(Result);

        /// <summary>
        /// توضیحات تکمیلی در مورد نتیجه
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// زمان انجام تراکنش
        /// </summary>
        [JsonProperty("doTime")]
        public string DoTime { get; set; }
    }
}
