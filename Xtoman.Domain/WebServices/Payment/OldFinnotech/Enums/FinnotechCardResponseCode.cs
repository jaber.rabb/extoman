﻿namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public static class FinnotechCardResponseCode
    {
        public static string ToText(string code)
        {
            switch (code)
            {
                case "0":
                    return "موفق‬";
                case "01":
                    return "صادر کننده‌ی کارت از انجام تراکنش صرف نظر کرد";
                case "02":
                    return "با توجه به شرایط خاص به وجود آمده به صادرکننده کارت رجوع شود";
                case "03":
                    return "پذیرنده فروشگاهی نامعتبر است";
                case "04":
                    return "کارت توسط دستگاه ضبط شده است";
                case "05":
                    return "به تراکنش رسیدگی نشد";
                case "06":
                    return "خطا";
                case "07":
                    return "به دلیل شرایط خاص کارت توسط دستگاه ضبط شود";
                case "08":
                    return "با تشخیص هویت دارنده کارت، تراکنش موفق میباشد‬";
                case "09":
                    return "درخواست در حال پردازش می‌باشد";
                case "12":
                    return "تراکنش معتبر نیست‬";
                case "13":
                    return "مبلغ معتبر نیست";
                case "14":
                    return "شماره کارت معتبر نیست‬";
                case "15":
                    return "صادر کننده کارت نامعتبر است‬";
                case "17":
                    return "انصراف مشتری";
                case "18":
                    return "اختلاف مشتری";
                case "19":
                    return "تراکنش مجددا ارسال شود‬";
                case "20":
                    return "پاسخ معتبر نیست";
                case "21":
                    return "عملیاتی انجام نپذیرفت‬";
                case "22":
                    return "مشکوک به اختلال در عملکرد";
                case "23":
                    return "کارمزد ارسالی پذیرنده غیر قابل قبول است";
                case "24":
                    return "عملیات فایل توسط دریافت کننده پشتیبانی نمی شود";
                case "25":
                    return "تراکنش اصلی یافت نشد";
                case "30":
                    return "قالب پیام دارای اشکال است";
                case "31":
                    return "پذیرنده توسط سوئیچ پشتیبانی نمی‌گردد";
                case "33":
                    return "کارت منقضی شده - ضبط کارت";
                case "34":
                    return "احتمال سوء استفاده - ضبط کارت‬";
                case "36":
                    return "کارت دارای محدودیت می باشد";
                case "38":
                    return "تعداد دفعات مجاز تکرار رمز مشتری به پایان رسید";
                case "39":
                    return "کارت حساب اعتباری ندارد";
                case "40":
                    return "عملیات درخواستی پشتیبانی نمی گردد‬";
                case "41":
                    return "کارت مفقودی می باشد";
                case "42":
                    return "حساب عمومی وجود ندارد‬";
                case "43":
                    return "کارت مفقودی می‌باشد";
                case "44":
                    return "کارت حساب سرمایه گذاری ندارد‬";
                case "51":
                    return "موجودی حساب کافی نیست";
                case "52":
                    return "کارت حساب جاری ندارد";
                case "53":
                    return "حساب پس انداز موجود نیست‬";
                case "54":
                    return "تاریخ انقضای کارت سپری شده است‬";
                case "55":
                    return "رمز اشتباه است";
                case "56":
                    return "کارت نامعتبر است‬";
                case "57":
                    return "دارنده کارت مجاز به انجام تراکنش نیست‬";
                case "58":
                    return "پایانه مجاز به انجام تراکنش نیست‬";
                case "59":
                    return "کارت مظنون به تقلب است‬";
                case "61":
                    return "مبلغ تراکنش پیش از حد مجاز می‌باشد";
                case "62":
                    return "کارت محدود شده است";
                case "63":
                    return "نقض موارد امنیتی";
                case "65":
                    return "تعداد درخواست تراکنش بیش از حد محاز می‬‌باشد";
                case "66":
                    return "‫مبلغ از سقف دوره ای برداشت نقدی بیشتر است";
                case "67":
                    return "کارت توسط دستگاه ضبط گردد";
                case "68":
                    return "پاسخ در فرصت مشخص شده دریافت نشد‬";
                case "69":
                    return "تعداد دفعات مجاز تکرار رمز مشتری به پایان رسید";
                case "75":
                    return "تعداد دفعات ورود رمز غلط بیش از حد مجاز می‌باشد";
                case "76":
                    return "مبلغ درخواست شده نامعتبر است";
                case "77":
                    return "تاریخ روز بیزینسی نامعتبر است";
                case "78":
                    return "کارت غیر فعال است‬";
                case "79":
                    return "حساب نامعتبر است";
                case "80":
                    return "تراکنش لغو شد";
                case "83":
                    return "ارائه دهنده خدمات پرداخت یا سامانه شاپرک اعلام Off Sign";
                case "84":
                    return "صادر کننده خارج از سرویس است";
                case "86":
                    return "موسسه ارسال کننده، شاپرک یا مقصد تراکنش در حالت SignOff";
                case "90":
                    return "سامانه مقصد تراکنش در حال انجام عملیات پایان روز می باشد";
                case "91":
                    return "صادر کننده یا سویچ در حال انجام عملیات هستند";
                case "92":
                    return "موسسه مالی یا شبکه ارتباطی جهت مسیر یابی در درسترس نیست";
                case "93":
                    return "امکان تکمیل تراکنش وجود ندارد";
                case "94":
                    return "تراکنش تکراری است";
                case "96":
                    return "بروز خطای سیستمی در انجام تراکنش";
                case "99":
                    return "عدم ارسال موفق SMS";
                case "111":
                case "112":
                case "113":
                case "115":
                    return "نام کاربری یا رمز عبور نامعتبر است";
                case "114":
                    return "آی پی نامعتبر است";
                case "116":
                    return "کابر غیر فعال است";
                case "117":
                    return "کاربر دسترسی ندارد";
                case "118":
                    return "به دلیل انجام تراکنش نامعتبر کابر غیر فعال است";
                case "119":
                    return "شماره پیگیری نامعتبر است";
                case "120":
                    return "ایندکس کارت نامعتبر است";
                case "121":
                    return "شناسه قبض نامعتبر است";
                case "122":
                    return "شناسه پرداخت نامعتبر است";
                case "123":
                    return "شناسه قبض یا شناسه پرداخت نامعتبر است";
                case "124":
                    return "مبلغ قبض نامعتبر است";
                case "125":
                    return "نوع قبض پشتیبانی نمی شود";
                case "126":
                    return "طول رمز عبور نامعتبر است";
                case "128":
                    return "شماره پیگیری یافت نشده";
                case "129":
                    return "شماره پیگیری با نام کاربری همخوانی ندارد";
                case "130":
                    return "شماره پیگیری با شماره همراه همخوانی ندارد";
                case "131":
                    return "مبلغ نامعتبر است";
                case "132":
                    return "شناسه سرویس نامعتبر است";
                case "133":
                    return "شماره پیگیری تکراری است";
                case "134":
                    return "رمز نگاری نامعتبر است";
                case "135":
                    return "شماره کارت مقصد نامعتبر است";
                case "136":
                    return "اطلاعات کارت نامعتبر است";
                case "137":
                    return "اطلاعات کارت نامعتبر است";
                case "138":
                    return "جمع مبالغ سرویس ها نامعتبر است";
                case "139":
                    return "شماره کارت نامعتبر می باشد";
                case "140":
                    return "تعداد سرویس ها از حد مجاز گذشته است";
                case "141":
                    return "شناسه سرویس ها تکراری است";
                case "142":
                    return "شماره همراه نامعتبر است";
                case "998":
                    return "پاسخی از کانال دریافت نشد";
                case "999":
                    return "خطای کلی سامانه";
                default:
                    return "نامشخص";
            }
        }
    }
}
