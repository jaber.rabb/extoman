﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public enum FinnotechTransferType
    {
        [Display(Name = "داخلی")]
        Internal,
        [Display(Name = "پایا و ساتنا")]
        Paya
    }

    public static class FinnotechTransferTypeHelper
    {
        public static FinnotechTransferType? GetByString(string type)
        {
            type = type.HasValue() ? type.ToLower() : "";
            switch (type)
            {
                case "internal":
                    return FinnotechTransferType.Internal;
                case "paya":
                    return FinnotechTransferType.Paya;
                default:
                    return null;
            }
        }
    }
}
