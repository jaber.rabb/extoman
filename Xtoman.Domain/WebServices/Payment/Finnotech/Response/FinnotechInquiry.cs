﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public class FinnotechInquiry : FinnotechResult
    {
        [JsonProperty("status")]
        public FinnotechState Status { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("transactionAmount")]
        public long TransactionAmount { get; set; }

        [JsonProperty("trackId")]
        public string TrackId { get; set; }

        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("tariffName")]
        public string TariffName { get; set; }

        [JsonProperty("scopeName")]
        public string ScopeName { get; set; }

        [JsonProperty("serviceName")]
        public string ServiceName { get; set; }

        [JsonProperty("uid")]
        public string Uid { get; set; }

        [JsonProperty("inquiry_date")]
        public string InquiryDate { get; set; }

        [JsonProperty("inquiry_time")]
        public string InquiryTime { get; set; }

        [JsonProperty("inquiry_sequence")]
        public string InquirySequence { get; set; }
    }
}
