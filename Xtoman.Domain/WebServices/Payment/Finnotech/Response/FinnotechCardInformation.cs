﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public class FinnotechCardInformation : FinnotechResult
    {
        [JsonProperty("destCard")]
        public string DestCard { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("result")]
        public string Result { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("doTime")]
        public string DoTime { get; set; }
    }
}
