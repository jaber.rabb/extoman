﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public class FinnotechIBANByCardNumber : FinnotechResult
    {
        [JsonProperty("IBAN")]
        public string Iban { get; set; }

        [JsonProperty("bankName")]
        public string BankName { get; set; }

        [JsonProperty("deposit")]
        public string Deposit { get; set; }

        [JsonProperty("card")]
        public string Card { get; set; }

        [JsonProperty("depositStatus")]
        public string DepositStatus { get; set; }

        [JsonProperty("depositDescription")]
        public string DepositDescription { get; set; }

        [JsonProperty("depositComment")]
        public string DepositComment { get; set; }

        [JsonProperty("depositOwners")]
        public List<FinnotechDepositOwner> DepositOwners { get; set; }

        [JsonProperty("alertCode")]
        public string AlertCode { get; set; }
    }

    public class FinnotechDepositOwner
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }
    }
}
