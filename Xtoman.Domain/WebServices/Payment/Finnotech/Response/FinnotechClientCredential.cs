﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public class FinnotechClientCredential : FinnotechResult
    {
        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("scopes")]
        public List<string> Scopes { get; set; }

        [JsonProperty("lifeTime")]
        public long LifeTime { get; set; }

        [JsonProperty("creationDate")]
        public string CreationDate { get; set; }

        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }
    }

    public class FinnotechRefreshClientCredential : FinnotechClientCredential
    {
        [JsonProperty("userId")]
        public string UserNationalCode { get; set; }
    }
}
