﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public class FinnotechRefreshToken : FinnotechResult
    {
        [JsonProperty("scopes")]
        public List<string> Scopes { get; set; }

        [JsonProperty("monthlyCallLimitation")]
        public long MonthlyCallLimitation { get; set; }

        [JsonProperty("maxAmountPerTransaction")]
        public long MaxAmountPerTransaction { get; set; }

        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("creationDate")]
        public string CreationDate { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("bank")]
        public string Bank { get; set; }

        [JsonProperty("lifeTime")]
        public long LifeTime { get; set; }

        [JsonProperty("deposits")]
        public List<string> Deposits { get; set; }

        [JsonProperty("clientId")]
        public string ClientId { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }
    }
}
