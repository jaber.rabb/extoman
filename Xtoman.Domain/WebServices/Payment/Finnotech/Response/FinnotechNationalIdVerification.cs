﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public class FinnotechNationalIdVerification : FinnotechResult
    {
        [JsonProperty("nationalCode")]
        public string NationalCode { get; set; }

        [JsonProperty("birthDate")]
        public string BirthDate { get; set; }

        [JsonProperty("status")]
        [JsonConverter(typeof(StringEnumConverter))]
        public FinnotechState Status { get; set; }

        [JsonProperty("fullName")]
        public string FullName { get; set; }

        [JsonProperty("fullNameSimilarity")]
        public int? FullNameSimilarity { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("firstNameSimilarity")]
        public int? FirstNameSimilarity { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("lastNameSimilarity")]
        public int? LastNameSimilarity { get; set; }

        [JsonProperty("fatherName")]
        public string FatherName { get; set; }

        [JsonProperty("fatherNameSimilarity")]
        public int? FatherNameSimilarity { get; set; }

        [JsonProperty("deathStatus")]
        public string DeathStatus { get; set; }
    }
}
