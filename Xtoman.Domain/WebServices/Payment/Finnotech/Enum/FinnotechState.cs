﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public enum FinnotechState
    {
        [Display(Name = "موفق")]
        DONE,
        [Display(Name = "ناموفق")]
        FAILED,
        [Display(Name = "در حال انجام")]
        PENDING,
        [Display(Name = "نامشخص")]
        UNKNOWN
    }
}
