﻿namespace Xtoman.Domain.WebServices.Payment.Finnotech
{
    public enum FinnotechAuthType
    {
        ClientCredential,
        AuthorizationCode
    }
}
