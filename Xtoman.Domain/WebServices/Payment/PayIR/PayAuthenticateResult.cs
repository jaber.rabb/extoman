﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.PayIR
{
    public class PayAuthenticateResult : PayCashoutBaseResult
    {
        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
