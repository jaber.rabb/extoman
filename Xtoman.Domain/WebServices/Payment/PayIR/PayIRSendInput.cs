﻿namespace Xtoman.Domain.WebServices.Payment.PayIR
{
    public class PayIRSendInput : PayIRBaseRequest
    {
#pragma warning disable IDE1006 // Naming Styles
        /// <summary>
        /// مبلغ تراکنش به صورت ریالی و بزرگتر یا مساوی 1000
        /// </summary>
        public int amount { get; set; }
        /// <summary>
        /// آدرس بازگشتی به صورت urlencode ، که باید با آدرس درگاه پرداخت تایید شده در Pay.ir بر روی یک دامنه باشد
        /// </summary>
        public string redirect { get; set; }
        /// <summary>
        /// شماره موبایل پرداخت کننده جهت نمایش لیست کارت های بانکی پرداخت کننده
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// شماره فاکتور - اختیاری
        /// </summary>
        public string factorNumber { get; set; }
        /// <summary>
        /// توضیحات (اختیاری ، حداکثر 255 کاراکتر)
        /// </summary>
        public string description { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }
}
