﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.PayIR
{
    public class PayIRSendResult : PayIRResult
    {
        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }
    }
}
