﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.PayIR
{
    public class PayCashoutRequestResult : PayCashoutBaseResult
    {
        [JsonProperty("data")]
        public PayCashoutRequestData Data { get; set; }
    }

    public class PayCashoutRequestData
    {
        [JsonProperty("cashout_id")]
        public long CashoutId { get; set; }

        [JsonProperty("status_code")]
        public byte StatusCode { get; set; }

        [JsonProperty("status_label")]
        public string StatusLabel { get; set; }
    }
}
