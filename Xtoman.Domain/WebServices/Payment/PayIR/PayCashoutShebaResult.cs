﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.PayIR
{
    public class PayCashoutShebaResult : PayCashoutBaseResult
    {
        [JsonProperty("data")]
        public PayCashoutShebaData Data { get; set; }
    }

    public class PayCashoutShebaData
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("bank")]
        public string Bank { get; set; }

        [JsonProperty("acc")]
        public string Acc { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
