﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices.Payment.PayIR
{
    public class PayCashoutStatusResult : PayCashoutBaseResult
    {
        [JsonProperty("data")]
        public PayCashoutStatusResultData Data { get; set; }
    }

    public class PayCashoutStatusResultData
    {
        [JsonProperty("cashout_id")]
        public string CashoutId { get; set; } // long

        [JsonProperty("status_code")]
        public byte StatusCode { get; set; } // 1 = Pending, 4 = Completed

        [JsonProperty("status_label")]
        public string StatusLabel { get; set; }

        [JsonProperty("transfer_status")]
        public string TransferStatus { get; set; }

        [JsonProperty("deposit_referrer")]
        public string DepositReferrer { get; set; }
    }
}
