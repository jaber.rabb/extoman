﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.PayIR
{
    public class PayCashoutBaseResult
    {
        [JsonProperty("status")]
        public byte Status { get; set; }
        [JsonProperty("message")]
        public string[] Messages { get; set; }
        [JsonProperty("error")]
        public string Error { get; set; }
    }
}
