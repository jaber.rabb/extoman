﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.PayIR
{
    public partial class PayCashoutTransactionsResult : PayCashoutBaseResult
    {
        [JsonProperty("data")]
        public PayCashoutTransactionsData Data { get; set; }
    }

    public class PayCashoutTransactionsData
    {
        [JsonProperty("transactions")]
        public PayCashoutTransactions Transactions { get; set; }
    }

    public class PayCashoutTransactions
    {
        [JsonProperty("current_page")]
        public long CurrentPage { get; set; }

        [JsonProperty("data")]
        public List<PayCashoutTransactionsDataListItem> Data { get; set; }

        [JsonProperty("first_page_url")]
        public Uri FirstPageUrl { get; set; }

        [JsonProperty("from")]
        public long From { get; set; }

        [JsonProperty("next_page_url")]
        public Uri NextPageUrl { get; set; }

        [JsonProperty("path")]
        public Uri Path { get; set; }

        [JsonProperty("per_page")]
        public long PerPage { get; set; }

        [JsonProperty("prev_page_url")]
        public object PrevPageUrl { get; set; }

        [JsonProperty("to")]
        public long To { get; set; }
    }

    public class PayCashoutTransactionsDataListItem
    {
        [JsonProperty("transaction_id")]
        public long TransactionId { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }

        [JsonProperty("transaction_type")]
        public long TransactionType { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("balance")]
        public long Balance { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("details")]
        public PayCashoutTransactionDetails Details { get; set; }

        [JsonProperty("created")]
        public DateTime Created { get; set; }

        [JsonProperty("from")]
        public PayCashoutTransactionFrom From { get; set; }

        [JsonProperty("to")]
        public PayCashoutTransactionTo To { get; set; }

        [JsonProperty("transaction")]
        public PayCashoutTransactionDetail Transaction { get; set; }

        [JsonProperty("wallet")]
        public PayCashoutTransactionWallet Wallet { get; set; }
    }

    public class PayCashoutTransactionDetails
    {
        [JsonProperty("website")]
        public Uri Website { get; set; }

        [JsonProperty("factor_number")]
        public string FactorNumber { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }
    }

    public class PayCashoutTransactionFrom
    {
        [JsonProperty("card_number")]
        public string CardNumber { get; set; }
    }

    public class PayCashoutTransactionTo
    {
        [JsonProperty("user_id")]
        public long UserId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("bank_name")]
        public string BankName { get; set; }
        [JsonProperty("bank_sheba")]
        public string BankSheba { get; set; }
        [JsonProperty("bank_account_number")]
        public string BankAccountNumber { get; set; }
    }

    public class PayCashoutTransactionDetail
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("amount_to_pay")]
        public long AmountToPay { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }

        [JsonProperty("ip")]
        public string Ip { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }
    }

    public class PayCashoutTransactionWallet
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
