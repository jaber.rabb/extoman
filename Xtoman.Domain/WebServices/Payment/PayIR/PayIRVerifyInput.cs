﻿namespace Xtoman.Domain.WebServices.Payment.PayIR
{
    public class PayIRVerifyInput : PayIRBaseRequest
    {
#pragma warning disable IDE1006 // Naming Styles
        /// <summary>
        /// token دریافتی از مرحله callback
        /// </summary>
        public string token { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }
}
