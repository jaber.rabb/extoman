﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.PayIR
{
    public class PayCashoutBalanceResult : PayCashoutBaseResult
    {
        [JsonProperty("data")]
        public PayCashoutBalanceData Data { get; set; }
    }

    public class PayCashoutBalanceData
    {
        [JsonProperty("wallet")]
        public string Wallet { get; set; }

        [JsonProperty("balance")]
        public long? Balance { get; set; }

        [JsonIgnore]
        public long? BalanceToman => Balance.HasValue && Balance.Value > 0 ? Balance.Value / 10 : new long?();

        [JsonProperty("cashoutable_amount")]
        public long? CashoutableAmount { get; set; }

        [JsonIgnore]
        public long? CashoutableAmountToman => CashoutableAmount.HasValue && CashoutableAmount.Value > 0 ? CashoutableAmount.Value / 10 : new long?();
    }
}
