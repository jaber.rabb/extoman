﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.PayIR
{
    /// <summary>
    /// "status" : 1,
    ///"amount" : مبلغ تراکنش,
    ///"transId" : شماره تراکنش,
    ///"factorNumber" : شماره فاکتور,
    ///"mobile" : شماره موبایل,
    ///"description" : توضیحات,
    ///"cardNumber" : شماره کارت,
    ///"message" : OK,
    /// </summary>
    public class PayIRVerifyResult : PayIRResult
    {
        [JsonProperty("amount")]
        public int Amount { get; set; }
        [JsonProperty("transId")]
        public string TransactionId { get; set; }
        [JsonProperty("factorNumber")]
        public string FactorNumber { get; set; }
        [JsonProperty("mobile")]
        public string Mobile { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("cardNumber")]
        public string CardNumber { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
