﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Payment.PayIR
{
    public class PayIRBaseRequest
    {
        public PayIRBaseRequest()
        {
            api = AppSettingManager.PayIR_ApiId;
        }
#pragma warning disable IDE1006
        /// <summary>
        /// API Key دریافتی شما از Pay.ir
        /// </summary>
        public string api { get; }
#pragma warning restore IDE1006 // Naming Styles


        [JsonIgnore]
        private string RequestBody => JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
        public string GetQueryString()
        {
            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(RequestBody);
            return dict.ToQueryParameters();
        }
    }
}
