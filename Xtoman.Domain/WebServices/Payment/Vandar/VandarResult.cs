﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices
{
    public class VandarResult
    {
        [JsonProperty(PropertyName = "status")]
        public byte Status { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("errors")]
        public List<string> Errors { get; set; }
    }
}
