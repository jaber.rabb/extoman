﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class VandarVerifyInput : VandarBaseRequest
    {
#pragma warning disable IDE1006 // Naming Styles
        public string token { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }
}
