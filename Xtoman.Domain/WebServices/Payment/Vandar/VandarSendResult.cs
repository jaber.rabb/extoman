﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices
{
    public class VandarSendResult : VandarResult
    {
        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }
    }
}
