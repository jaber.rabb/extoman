﻿using System;
using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.Vandar
{
    public class VandarBusinessBalance : VandarBase
    {
        [JsonProperty("data")]
        public VandarBusinessBalanceData Data { get; set; }
    }

    public class VandarBusinessBalanceData
    {
        [JsonProperty("wallet")]
        public long Wallet { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }
    }
}
