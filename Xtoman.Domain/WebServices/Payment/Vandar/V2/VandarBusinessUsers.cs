﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.Vandar
{
    public partial class VandarBusinessUsers : VandarBase
    {
        [JsonProperty("data")]
        public VandarBusinessUserData Data { get; set; }
    }

    public partial class VandarBusinessUserData
    {
        [JsonProperty("users")]
        public List<VandarBusinessUser> Users { get; set; }
    }

    public partial class VandarBusinessUser
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("avatar")]
        public Uri Avatar { get; set; }

        [JsonProperty("role")]
        public string Role { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("role_id")]
        public long RoleId { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
