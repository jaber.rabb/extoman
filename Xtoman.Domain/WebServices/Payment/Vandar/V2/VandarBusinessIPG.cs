﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.Vandar
{
    public class VandarBusinessIPG : VandarBase
    {
        [JsonProperty("data")]
        public VandarBusinessIPGData Data { get; set; }
    }

    public class VandarBusinessIPGData
    {
        [JsonProperty("ipg")]
        public VandarBusinessIPGItem IPG { get; set; }
    }

    public class VandarBusinessIPGItem
    {
        [JsonProperty("urls")]
        public List<Uri> Urls { get; set; }

        [JsonProperty("ips")]
        public List<string> Ips { get; set; }

        [JsonProperty("api_key")]
        public string ApiKey { get; set; }

        [JsonProperty("wage")]
        public bool Wage { get; set; }

        [JsonProperty("vip")]
        public bool Vip { get; set; }

        [JsonProperty("payment")]
        public bool Payment { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("transactions_count")]
        public long TransactionsCount { get; set; }

        [JsonProperty("transactions_sum")]
        public long TransactionsSum { get; set; }
    }
}
