﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.Vandar
{
    public partial class VandarBusiness : VandarBase
    {
        [JsonProperty("data")]
        public VandarBusinessData Data { get; set; }
    }
    public partial class VandarBusinessList : VandarBase
    {
        [JsonProperty("data")]
        public List<VandarBusinessData> Data { get; set; }
    }

    public partial class VandarBusinessData
    {
        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("business_name")]
        public string BusinessName { get; set; }

        [JsonProperty("business_name_fa")]
        public string BusinessNameFa { get; set; }

        [JsonProperty("national_code", NullValueHandling = NullValueHandling.Ignore)]
        public string NationalCode { get; set; }

        [JsonProperty("business_type")]
        public string BusinessType { get; set; }

        [JsonProperty("phone_number")]
        public string PhoneNumber { get; set; }

        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("wallet")]
        public long Wallet { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("today_transactions")]
        public long TodayTransactions { get; set; }

        [JsonProperty("today_settlements")]
        public long TodaySettlements { get; set; }

        [JsonProperty("active", NullValueHandling = NullValueHandling.Ignore)]
        public long? Active { get; set; }

        [JsonProperty("legal_business_name")]
        public string LegalBusinessName { get; set; }

        [JsonProperty("national_id")]
        public string NationalId { get; set; }

        [JsonProperty("city_id")]
        public string CityId { get; set; }

        [JsonProperty("mcc_code")]
        public string MccCode { get; set; }

        [JsonProperty("statusBox", NullValueHandling = NullValueHandling.Ignore)]
        public VandarBusinessStatusBox StatusBox { get; set; }

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public long? Status { get; set; }

        [JsonProperty("role_name", NullValueHandling = NullValueHandling.Ignore)]
        public string RoleName { get; set; }

        [JsonProperty("role", NullValueHandling = NullValueHandling.Ignore)]
        public string Role { get; set; }

        [JsonProperty("permissions", NullValueHandling = NullValueHandling.Ignore)]
        public List<string> Permissions { get; set; }
    }

    public class VandarBusinessStatusBox
    {
        [JsonProperty("payment_required")]
        public bool PaymentRequired { get; set; }

        [JsonProperty("national_card_photo")]
        public string NationalCardPhoto { get; set; }

        [JsonProperty("official_Newspaper")]
        public string OfficialNewspaper { get; set; }

        [JsonProperty("introduction_letter")]
        public string IntroductionLetter { get; set; }
    }
}
