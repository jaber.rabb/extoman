﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.Vandar
{
    public class VandarBusinessIBANs : VandarBase
    {
        [JsonProperty("data")]
        public VandarBusinessIBANData Data { get; set; }
    }

    public class VandarBusinessIBANData
    {
        [JsonProperty("ibans")]
        public List<VandarBusinessIBAN> Ibans { get; set; }
    }

    public class VandarBusinessIBAN
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("IBAN")]
        public string IBAN { get; set; }

        [JsonProperty("bank_name")]
        public string BankName { get; set; }

        [JsonProperty("account_number")]
        public string AccountNumber { get; set; }

        [JsonProperty("account_description")]
        public string AccountDescription { get; set; }

        [JsonProperty("account_owner")]
        public List<VandarIBANAccountOwner> AccountOwner { get; set; }
    }

    public class VandarIBANAccountOwner
    {
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }
    }
}
