﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices.Payment.Vandar
{
    public class VandarBusinessTransactions : VandarBase
    {
        [JsonProperty("data")]
        public VandarBusinessTransactionData Data { get; set; }
    }

    public class VandarBusinessTransactionData
    {
        [JsonProperty("current_page")]
        public long CurrentPage { get; set; }

        [JsonProperty("data")]
        public List<VandarBusinessTransaction> Data { get; set; }

        [JsonProperty("first_page_url")]
        public Uri FirstPageUrl { get; set; }

        [JsonProperty("from")]
        public long From { get; set; }

        [JsonProperty("last_page")]
        public long LastPage { get; set; }

        [JsonProperty("last_page_url")]
        public Uri LastPageUrl { get; set; }

        [JsonProperty("next_page_url")]
        public Uri NextPageUrl { get; set; }

        [JsonProperty("path")]
        public Uri Path { get; set; }

        [JsonProperty("per_page")]
        public long PerPage { get; set; }

        [JsonProperty("prev_page_url")]
        public string PrevPageUrl { get; set; }

        [JsonProperty("to")]
        public long To { get; set; }

        [JsonProperty("total")]
        public long Total { get; set; }
    }

    public partial class VandarBusinessTransaction
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("wage")]
        public long Wage { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonIgnore]
        public VandarTransactionStatus StatusEnum => (VandarTransactionStatus)Status;

        [JsonProperty("ref_id")]
        public string RefId { get; set; }

        [JsonProperty("tracking_code")]
        public string TrackingCode { get; set; }

        [JsonProperty("card_number")]
        public string CardNumber { get; set; }

        [JsonProperty("cid")]
        public string Cid { get; set; }

        [JsonProperty("verified")]
        public long Verified { get; set; }

        [JsonProperty("channel")]
        public string Channel { get; set; }

        [JsonProperty("payment_date")]
        public string PaymentDate { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("wallet")]
        public long Wallet { get; set; }

        [JsonProperty("result")]
        public string Result { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("factorNumber")]
        public string FactorNumber { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("callback_url")]
        public string CallbackUrl { get; set; }

        [JsonProperty("form_title")]
        public string FormTitle { get; set; }

        [JsonProperty("payer")]
        public VandarBusinessTransactionPayer Payer { get; set; }

        [JsonProperty("receiver")]
        public VandarBusinessTransactionReceiver Receiver { get; set; }
    }

    public partial class VandarBusinessTransactionPayer
    {
        [JsonProperty("ip")]
        public string Ip { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }

    public partial class VandarBusinessTransactionReceiver
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("IBAN")]
        public string Iban { get; set; }

        [JsonProperty("bank_name")]
        public string BankName { get; set; }
    }
}
