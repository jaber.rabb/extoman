﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.Vandar
{
    public class VandarBusinessSettlementStore : VandarBase
    {
        [JsonProperty("data")]
        public VandarBusinessSettlementStoreData Data { get; set; }
    }

    public class VandarBusinessSettlementStoreData
    {
        [JsonProperty("settlement")]
        public Dictionary<string, VandarBusinessSettlementItem> Settlement { get; set; }
    }

    public class VandarBusinessSettlementItem
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("transaction_id")]
        public long TransactionId { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("payment_number")]
        public long PaymentNumber { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("wallet")]
        public long Wallet { get; set; }

        [JsonProperty("settlement_date")]
        public DateTimeOffset SettlementDate { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("iban_id")]
        public string IbanId { get; set; }
    }
}
