﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.Vandar
{
    public class VandarBase
    {
        [JsonProperty("status")]
        public long Status { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("error")]
        public string Error { get; set; }
    }
}
