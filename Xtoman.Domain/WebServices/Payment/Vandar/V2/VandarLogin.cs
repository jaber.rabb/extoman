﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.Payment.Vandar
{
    public partial class VandarLogin : VandarBase
    {
        [JsonProperty("data")]
        public VandarLoginData Data { get; set; }
    }

    public partial class VandarLoginData
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("user")]
        public VandarUser User { get; set; }
    }

    public partial class VandarUser
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("fname")]
        public string Fname { get; set; }

        [JsonProperty("lname")]
        public string Lname { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("national_code")]
        public string NationalCode { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("address")]
        public object Address { get; set; }

        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }

        [JsonProperty("phone_number")]
        public object PhoneNumber { get; set; }

        [JsonProperty("wallet")]
        public long Wallet { get; set; }

        [JsonProperty("deductible_amount")]
        public long DeductibleAmount { get; set; }

        [JsonProperty("birthdate")]
        public string Birthdate { get; set; }

        [JsonProperty("birthdate_year")]
        public long BirthdateYear { get; set; }

        [JsonProperty("birthdate_month")]
        public long BirthdateMonth { get; set; }

        [JsonProperty("birthdate_day")]
        public long BirthdateDay { get; set; }

        [JsonProperty("telegram_chat_id")]
        public long TelegramChatId { get; set; }

        [JsonProperty("meta")]
        public VandarMeta Meta { get; set; }

        [JsonProperty("statusBox")]
        public VandarStatusBox StatusBox { get; set; }

        [JsonProperty("invitation")]
        public object Invitation { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("created_at")]
        public VandarDateTime CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public VandarDateTime UpdatedAt { get; set; }
    }

    public partial class VandarDateTime
    {
        [JsonProperty("date")]
        public DateTimeOffset Date { get; set; }

        [JsonProperty("timezone_type")]
        public long TimezoneType { get; set; }

        [JsonProperty("timezone")]
        public string Timezone { get; set; }
    }

    public partial class VandarMeta
    {
        [JsonProperty("onboarding")]
        public bool Onboarding { get; set; }

        [JsonProperty("notification")]
        public VandarNotification Notification { get; set; }
    }

    public partial class VandarNotification
    {
        [JsonProperty("transaction")]
        public VandarAuth Transaction { get; set; }

        [JsonProperty("settlement")]
        public VandarAuth Settlement { get; set; }

        [JsonProperty("ticket")]
        public VandarAuth Ticket { get; set; }

        [JsonProperty("auth")]
        public VandarAuth Auth { get; set; }
    }

    public partial class VandarAuth
    {
        [JsonProperty("sms")]
        public bool Sms { get; set; }

        [JsonProperty("email")]
        public bool Email { get; set; }

        [JsonProperty("telegram")]
        public bool Telegram { get; set; }

        [JsonProperty("notif")]
        public bool Notif { get; set; }
    }

    public partial class VandarStatusBox
    {
        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("personal_information")]
        public string PersonalInformation { get; set; }
    }
}
