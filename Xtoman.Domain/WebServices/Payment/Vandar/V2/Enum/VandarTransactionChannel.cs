﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.Payment.Vandar
{
    public enum VandarTransactionChannel
    {
        [Display(Name = "پرداخت اینترنتی")]
        IPG,
        [Display(Name = "تسویه حساب")]
        Form
    }
}
