﻿namespace Xtoman.Domain.WebServices.Payment.Vandar
{
    public enum VandarTransactionStatus
    {
        Done = 2,
        Unknown = 0,
        Unsuccessful = -1,
        Successful = 1,
        Pending = -2,
        Canceled = -4,
        Failed = -8
    }
}
