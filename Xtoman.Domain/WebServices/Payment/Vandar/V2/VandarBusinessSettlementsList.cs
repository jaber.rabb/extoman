﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.Vandar
{
    public class VandarBusinessSettlementsList : VandarBase
    {
        [JsonProperty("data")]
        public VandarBusinessSettlementsListData Data { get; set; }
    }

    public class VandarBusinessSettlementsListData
    {
        [JsonProperty("wallet")]
        public long Wallet { get; set; }

        [JsonProperty("deductible_amount")]
        public long DeductibleAmount { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("settlements")]
        public VandarBusinessSettlementsData Settlements { get; set; }
    }

    public class VandarBusinessSettlementsData
    {
        [JsonProperty("current_page")]
        public long CurrentPage { get; set; }

        [JsonProperty("data")]
        public List<VandarBusinessSettlement> Data { get; set; }

        [JsonProperty("first_page_url")]
        public Uri FirstPageUrl { get; set; }

        [JsonProperty("from")]
        public long From { get; set; }

        [JsonProperty("last_page")]
        public long LastPage { get; set; }

        [JsonProperty("last_page_url")]
        public Uri LastPageUrl { get; set; }

        [JsonProperty("next_page_url")]
        public string NextPageUrl { get; set; }

        [JsonProperty("path")]
        public Uri Path { get; set; }

        [JsonProperty("per_page")]
        public long PerPage { get; set; }

        [JsonProperty("prev_page_url")]
        public string PrevPageUrl { get; set; }

        [JsonProperty("to")]
        public long To { get; set; }

        [JsonProperty("total")]
        public long Total { get; set; }
    }

    public class VandarBusinessSettlement
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("transaction_id")]
        public long TransactionId { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("payment_number")]
        public long PaymentNumber { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
        public VandarTransactionStatus StatusEnum => GetVandarTransactionStatus(Status);

        private VandarTransactionStatus GetVandarTransactionStatus(string status)
        {
            switch (status)
            {
                case "PENDING":
                    return VandarTransactionStatus.Pending;
                case "CANCELED":
                    return VandarTransactionStatus.Canceled;
                case "DONE":
                    return VandarTransactionStatus.Successful;
                case "FAILED":
                    return VandarTransactionStatus.Failed;
                default:
                    return VandarTransactionStatus.Unknown;
            }
        }

        [JsonProperty("wallet")]
        public long Wallet { get; set; }

        [JsonProperty("settlement_date")]
        public DateTimeOffset SettlementDate { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("iban_id")]
        public string IbanId { get; set; }
    }
}
