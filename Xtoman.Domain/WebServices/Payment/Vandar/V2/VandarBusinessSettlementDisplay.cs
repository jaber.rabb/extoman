﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.Vandar
{
    public class VandarBusinessSettlementDisplay : VandarBase
    {
        [JsonProperty("data")]
        public VandarBusinessSettlementDisplayData Data { get; set; }
    }

    public class VandarBusinessSettlementDisplayData
    {
        [JsonProperty("settlement")]
        public VandarBusinessSettlementItem Settlement { get; set; }
    }
}
