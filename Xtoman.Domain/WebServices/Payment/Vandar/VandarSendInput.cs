﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class VandarSendInput : VandarBaseRequest
    {
#pragma warning disable IDE1006 // Naming Styles
        /// <summary>
        /// مبلغ تراکنش به صورت ریالی و بزرگتر یا مساوی 1000
        /// </summary>
        public int amount { get; set; }
        /// <summary>
        /// باید با آدرس درگاه پرداخت تایید شده در وندار بر روی یک دامنه باشد
        /// </summary>
        public string callback_url { get; set; }
        /// <summary>
        /// شماره موبایل (اختیاری ، جهت نمایش کارت های خریدار به ایشان و نمایش درگاه موبایلی )
        /// </summary>
        public string mobile_number { get; set; }
        /// <summary>
        /// شماره فاکتور شما (اختیاری)
        /// </summary>
        public string factorNumber { get; set; }
        /// <summary>
        /// توضیحات (اختیاری ، حداکثر 255 کاراکتر)
        /// </summary>
        [MaxLength(255)]
        public string description { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }
}
