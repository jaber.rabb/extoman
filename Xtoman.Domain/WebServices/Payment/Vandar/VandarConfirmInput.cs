﻿namespace Xtoman.Domain.WebServices
{
    public class VandarConfirmInput : VandarBaseRequest
    {
#pragma warning disable IDE1006 // Naming Styles
        public string token { get; set; }
        public bool confirm { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }
}
