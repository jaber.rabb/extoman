﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices
{
    public class VandarCallback : VandarResult
    {
        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("transId")]
        public string TransId { get; set; }

        [JsonProperty("refnumber")]
        public string Refnumber { get; set; }

        [JsonProperty("trackingCode")]
        public string TrackingCode { get; set; }

        [JsonProperty("factorNumber")]
        public string FactorNumber { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("cardNumber")]
        public string CardNumber { get; set; }

        [JsonProperty("CID")]
        public string Cid { get; set; }

        [JsonProperty("paymentDate")]
        public string PaymentDate { get; set; }
    }
}
