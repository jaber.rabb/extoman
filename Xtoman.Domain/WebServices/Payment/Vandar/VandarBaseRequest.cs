﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices
{
    public class VandarBaseRequest
    {
        public VandarBaseRequest()
        {
            api_key = AppSettingManager.Vandar_ApiKey;
        }
#pragma warning disable IDE1006
        /// <summary>
        /// API Key دریافتی شما از Vandar.io
        /// </summary>
        public string api_key { get; }
#pragma warning restore IDE1006 // Naming Styles

        [JsonIgnore]
        private string RequestBody => JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
        public string GetQueryString()
        {
            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(RequestBody);
            return dict.ToQueryParameters();
        }
    }
}
