﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices
{
    public class VandarVerifyResult : VandarResult
    {
        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("transId")]
        public string TransId { get; set; }

        [JsonProperty("factorNumber")]
        public string FactorNumber { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("cardNumber")]
        public string CardNumber { get; set; }

        [JsonProperty("paymentDate")]
        public DateTimeOffset PaymentDate { get; set; }
    }
}
