﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public enum CreateTransferStatus
    {
        [Display(Name = "Transfer created, waiting for email confirmation.")]
        Transfer_created_waiting_email_confirmation = 0,
        [Display(Name = "Transfer created with no email confirmation needed.")]
        Transfer_created_with_no_email_confirmation = 1
    }

    public enum CreateWithdrawalStatus
    {
        [Display(Name = "Withdrawal created, waiting for email confirmation.")]
        Withdrawal_created_waiting_email_confirmation = 0,
        [Display(Name = "Withdrawal created with no email confirmation needed.")]
        Withdrawal_created_with_no_email_confirmation = 1
    }

    public enum WithdrawalInfoStatus
    {
        [Display(Name = "Cancelled", ShortName = "لغو شده")]
        Cancelled = -1,
        [Display(Name = "Waiting for email confirmation", ShortName = "در انتظار تایید ایمیل")]
        Waiting_for_email_confirmation = 0,
        [Display(Name = "Pending", ShortName = "در حال انجام")]
        Pending = 1,
        [Display(Name = "Complete", ShortName = "انجام شده")]
        Complete = 2
    }

    public enum ConversionInfoStatus
    {
        [Display(Name = "Complete")]
        Complete = 2,
        [Display(Name = "Funds Sent")]
        FundsSent = 1,
        [Display(Name = "Pending")]
        Pending = 0,
        [Display(Name = "Cancelled")]
        Cancelled = -1,
        [Display(Name = "Cancelled: Below ShapeShift Minimum")]
        Cancelled_Below_ShapeShift_Minimum = -2,
        [Display(Name = "Cancelled: Above ShapeShift Minimum")]
        Cancelled_Above_ShapeShift_Minimum = -3
    }
}
