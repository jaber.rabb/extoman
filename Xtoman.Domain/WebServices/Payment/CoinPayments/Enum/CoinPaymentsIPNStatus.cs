﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    /// <summary>
    /// For future-proofing your IPN handler you can use the following rules: 
    /// Lower than 0 = Failures/Errors | 
    /// 0-99 = Payment is Pending in some way | 
    /// >=100 = Payment completed successfully | 
    /// IMPORTANT: You should never ship/release your product until the status is >= 100 OR == 2 (Queued for nightly payout)!
    /// </summary>
    public enum CoinPaymentsIPNStatus
    {
        [Display(Description = "PayPal Refund or Reversal")]
        PayPalRefundOrReversal = -2,
        [Display(Description = "Cancelled / Timed Out")]
        CancelledOrTimedOut = -1,
        [Display(Description = "Waiting for buyer funds")]
        WaitingForBuyerFunds = 0,
        [Display(Description = "We have confirmed coin reception from the buyer")]
        WeHaveConfirmedCoinReceptionFromTheBuyer = 1,
        [Display(Description = "Queued for nightly payout (if you have the Payout Mode for this coin set to Nightly)")]
        QueuedForNightlyPayout = 2,
        [Display(Description = "PayPal Pending (eChecks or other types of holds)")]
        PayPalPending = 3,
        [Display(Description = "Payment Complete. We have sent your coins to your payment address or 3rd party payment system reports the payment complete")]
        PaymentComplete = 100
    }
}
