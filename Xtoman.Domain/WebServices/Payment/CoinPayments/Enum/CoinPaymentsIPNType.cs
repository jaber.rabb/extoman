﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public enum CoinPaymentsIPNType
    {
        None,
        Deposit,
        Withdrawal,
        Simple,
        Button,
        Cart,
        Donation,
        Api
    }
}
