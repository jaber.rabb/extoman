﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public enum CoinPaymentsCurrency
    {
        [Display(Name = "US Dollar", ShortName = "USD")]
        USD,
        [Display(Name = "Euro", ShortName = "EUR")]
        EUR,
        [Display(Name = "British Pound", ShortName = "GBP")]
        GBP,
        [Display(Name = "Bitcoin", ShortName = "BTC")]
        Bitcoin,
        [Display(Name = "Litecoin", ShortName = "LTC")]
        Litecoin,
        [Display(Name = "Ethereum", ShortName = "ETH")]
        Ethereum,
        [Display(Name = "EthereumClassic", ShortName = "ETC")]
        EthereumClassic,
        [Display(Name = "Dash", ShortName = "DASH")]
        Dash,
        [Display(Name = "Dogecoin", ShortName = "DOGE")]
        Dogecoin,
        [Display(Name = "Namecoin", ShortName = "NMC")]
        Namecoin,
        [Display(Name = "TetherUSD", ShortName = "USDT")]
        TetherUSD,
        [Display(Name = "Monero", ShortName = "XMR")]
        Monero,
        [Display(Name = "ZCash", ShortName = "ZEC")]
        ZCash,
        [Display(Name = "NEO", ShortName = "NEO")]
        NEO,
        [Display(Name = "Ripple", ShortName = "XRP")]
        Ripple,
        [Display(Name = "Bitcoin Cash", ShortName = "BCH")]
        BitcoinCash,
        [Display(Name = "Tron", ShortName = "TRX")]
        Tron,
        [Display(Name = "Verge", ShortName = "XVG")]
        Verge,
        [Display(Name = "NEM", ShortName = "XEM")]
        NEM,
        [Display(Name = "ZCoin", ShortName = "XZC")]
        ZCoin,
        [Display(Name = "Komodo", ShortName = "KMD")]
        Komodo,
        [Display(Name = "Stratis", ShortName = "STRAT")]
        Stratis,
        [Display(Name = "Lisk", ShortName = "LSK")]
        Lisk,
        [Display(Name = "Decred", ShortName = "DCR")]
        Decred,
        [Display(Name = "Nav Coin", ShortName = "NAV")]
        NavCoin,
        [Display(Name = "Steem", ShortName = "STEEM")]
        Steem,
        [Display(Name = "Waves", ShortName = "WAVES")]
        Waves,
        [Display(Name = "Pivx", ShortName = "PIVX")]
        Pivx,
        [Display(Name = "EOS", ShortName = "EOS")]
        EOS,
        [Display(Name = "Ravencoin", ShortName = "RVN")]
        Ravencoin,
        [Display(Name = "Horizen", ShortName = "ZEN")]
        Horizen,
        [Display(Name = "Binance Coin", ShortName = "BNB")]
        BinanceCoin
    }

    public static class CoinPaymentsCurrencyHelper
    {
        public static string ToCoinPaymentsCurrency(this ECurrencyType eCurrencyType)
        {
            switch (eCurrencyType)
            {
                default:
                    return eCurrencyType.ToDisplay(DisplayProperty.ShortName);
            }
        }

        /// <summary>
        /// Give Symbol
        /// </summary>
        /// <param name="nameCode"></param>
        /// <returns></returns>
        public static CoinPaymentsCurrency? ToCoinPaymentsCurrency(this string nameCode)
        {
            nameCode = nameCode.ToUpper().Replace(" ", "");
            switch (nameCode)
            {
                case "BCH":
                case "BCC":
                case "BCHABC":
                    return CoinPaymentsCurrency.BitcoinCash;
                default:
                    return nameCode.ToEnumFromDisplayShortName(CoinPaymentsCurrency.USD);
            }
            throw new ArgumentException("Enum not found");
        }
    }
}
