﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CoinPaymentsCreateMassWithdrawalResult : CoinPaymentsResult
    {
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public new List<CreateMassWithdrawal> result { get; set; }
    }

    public class CoinPaymentsCreateWithdrawalResult : CoinPaymentsResult
    {
        [JsonProperty("result")]
        public new CreateWithdrawal result { get; set; }
    }

    public class CreateMassWithdrawal : CreateWithdrawal
    {
        public string error { get; set; }
    }

    public class CreateWithdrawal
    {
        /// <summary>
        /// The CoinPayments withdrawal ID. (This is not a coin network TX ID.)
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// status = 0 or 1. 0 = Withdrawal created, waiting for email confirmation. 1 = Withdrawal created with no email confirmation needed.
        /// </summary>
        public int status { get; set; }
        public CreateWithdrawalStatus statusEnum => (CreateWithdrawalStatus)status;
        public decimal amount { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
