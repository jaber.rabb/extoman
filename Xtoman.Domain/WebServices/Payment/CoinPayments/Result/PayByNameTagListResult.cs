﻿using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CoinPaymentsPayByNameTagListResult : CoinPaymentsResult
    {
        public new List<PayByNameTagList> result { get; set; }
    }

    public class PayByNameTagList
    {
        /// <summary>
        /// This is the unique identifier of the tag in the system. This is the identifier you will use with the 'update_pbn_tag' and 'claim_pbn_tag' API calls.
        /// </summary>
        public string tagid { get; set; }
        /// <summary>
        /// This is the $PayByName tag. An empty string means the tag is unclaimed. (Note that the tags do not have a $ at the front.)
        /// </summary>
        public string pbntag { get; set; }
        /// <summary>
        /// The time (Unix timestamp) of when the tag expires.
        /// </summary>
        public long time_expires { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
