﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CoinPaymentsAddressResult : CoinPaymentsResult
    {
        public new CoinPaymentsAddress result { get; set; }
    }

    public class CoinPaymentsAddress
    {
        /// <summary>
        /// The address to deposit the selected coin into your CoinPayments Wallet.
        /// </summary>
        public string address { get; set; }
        /// <summary>
        /// NXT Only: The pubkey to attach the 1st time you send to the address to activate it.
        /// </summary>
        public string pubkey { get; set; }
        /// <summary>
        /// For coins needing a destination tag, payment ID, etc. (like Ripple or Monero) to set for depositing into your CoinPayments Wallet.
        /// </summary>
        public string dest_tag { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
