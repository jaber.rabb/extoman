﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CoinPaymentsConversionInformationResult : CoinPaymentsResult
    {
        public new ConversionInformation result { get; set; }
    }

    public class ConversionInformation
    {
        public long time_created { get; set; }
        public ConversionInfoStatus status { get; set; }
        public string status_text { get; set; }
        public string coin1 { private get; set; }
        public string coin2 { private get; set; }
        public decimal amount_sent { get; set; }
        public decimal amount_sentf { get; set; }
        public decimal received { get; set; }
        public decimal receivedf { get; set; }

        [JsonIgnore]
        public CoinPaymentsCurrency Coin1 => coin1.ToCoinPaymentsCurrency().Value;
        [JsonIgnore]
        public CoinPaymentsCurrency Coin2 => coin2.ToCoinPaymentsCurrency().Value;
    }
#pragma warning restore IDE1006 // Naming Styles
}
