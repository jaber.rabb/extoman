﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CoinPaymentsWithdrawalHistoryResult : CoinPaymentsResult
    {
        public new List<WithdrawalHistory> result { get; set; }
    }

    public class WithdrawalHistory
    {
        /// <summary>
        /// This withdrawal ID.
        /// </summary>
        [Display(Name = "ردیف")]
        public string id { get; set; }
        /// <summary>
        /// The time the withdrawal request was submitted.
        /// </summary>
        [Display(Name = "تاریخ ثبت")]
        public long time_created { get; set; }
        /// <summary>
        /// The status of the withdrawal
        /// </summary>
        [Display(Name = "وضعیت")]
        public WithdrawalInfoStatus status { get; set; }
        /// <summary>
        /// The status of the withdrawal in text format.
        /// </summary>
        [Display(Name = "متن وضعیت")]
        public string status_text { get; set; }
        /// <summary>
        /// The ticker symbol of the coin for the withdrawal.
        /// </summary>
        [Display(Name = "نوع ارز")]
        public string coin { get; set; }
        /// <summary>
        /// The amount of the withdrawal (in Satoshis).
        /// </summary>
        [Display(Name = "مقدار به ساتوشی")]
        public decimal amount { get; set; }
        /// <summary>
        /// The amount of the withdrawal (as a floating point number).
        /// </summary>
        [Display(Name = "مقدار")]
        public string amountf { get; set; }
        /// <summary>
        /// The address the withdrawal was sent to. (only in response if status == Completed)
        /// </summary>
        [Display(Name = "آدرس")]
        public string send_address { get; set; }
        /// <summary>
        /// The destination tag/payment ID/etc. the withdrawal was sent to. (only in response if coin supports destination tags/payment IDs/etc.)
        /// </summary>
        [Display(Name = "تگ آدرس")]
        public string send_address_dest_tag { get; set; }
        /// <summary>
        /// The coin TX ID of the send. (only in response if status == Completed)
        /// </summary>
        [Display(Name = "کد تراکنش")]
        public string send_txid { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
