﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CoinPaymentsCreateTransactionResult : CoinPaymentsResult
    {
        public new CreateTransaction result { get; set; }
    }

    public class CreateTransaction
    {
        /// <summary>
        /// The amount for the buyer to send in the destination currency (currency2).
        /// </summary>
        public decimal amount { get; set; }
        /// <summary>
        /// The address the buyer needs to send the coins to.
        /// </summary>
        public string address { get; set; }
        /// <summary>
        /// The CoinPayments.net transaction ID.
        /// </summary>
        public string txn_id { get; set; }
        /// <summary>
        /// The number of confirms needed for the transaction to be complete.
        /// </summary>
        public int confirms_needed { get; set; }
        /// <summary>
        /// How long the buyer has to send the coins and have them be confirmed in seconds.
        /// </summary>
        public int timeout { get; set; }
        /// <summary>
        /// A URL where the buyer can view the payment progress and leave feedback for you.
        /// </summary>
        public string status_url { get; set; }
        /// <summary>
        /// A URL to a QR code you can display for buyer's paying with a QR supporting wallet.
        /// </summary>
        public string qrcode_url { get; set; }
        /// <summary>
        /// For coins needing a destination tag, payment ID, etc. (like Ripple or Monero) to set for depositing into your CoinPayments Wallet.
        /// </summary>
        public string dest_tag { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
