﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CoinPaymentsPayByNameProfileInfoResult : CoinPaymentsResult
    {
        public new PayByNameProfileInfo result { get; set; }
    }

    public class PayByNameProfileInfo
    {
        /// <summary>
        ///  This is the $PayByName tag in the same case as the owner entered it. It is recommended to display the tag this way versus how it was entered by a viewing user.
        /// </summary>
        public string pbntag { get; set; }
        /// <summary>
        /// This is the owner's merchant ID. It can be used to send transfers or payments to the owner.
        /// </summary>
        public string merchant { get; set; }
        /// <summary>
        /// This is the owner's name (may be a store name, nickname, real name, etc.)
        /// </summary>
        public string profile_name { get; set; }
        /// <summary>
        /// This is the owner's website URL.
        /// </summary>
        public string profile_url { get; set; }
        /// <summary>
        /// This is the owner's email.
        /// </summary>
        public string profile_email { get; set; }
        /// <summary>
        /// The URL of the owner's profile picture.
        /// </summary>
        public string profile_image { get; set; }
        /// <summary>
        /// The time (Unix timestamp) of when the user signed up for CoinPayments.
        /// </summary>
        public long member_since { get; set; }
        /// <summary>
        /// The owners current feedback. The 'percent' field with either be a percent as seen or 'No Rating' if the user has no feedback.
        /// </summary>
        public PayByNameProfileInfoFeedback feedback { get; set; }
    }

    public class PayByNameProfileInfoFeedback
    {
        /// <summary>
        /// Positive
        /// </summary>
        public int pos { get; set; }
        /// <summary>
        /// Negative
        /// </summary>
        public int neg { get; set; }
        public string neut { get; set; }
        public int total { get; set; }
        /// <summary>
        /// Percent with %
        /// </summary>
        public string percent { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
