﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CoinPaymentsCoinBalancesResult : CoinPaymentsResult
    {
        [JsonProperty("result")]
        public new Dictionary<string, CoinBalanceItem> result { get; set; }
        //public new CoinBalances result { get; set; }
    }

    public class CoinBalances
    {
        /// <summary>
        /// Euro
        /// </summary>
        public CoinBalanceItem EUR { get; set; }
        /// <summary>
        /// British pound
        /// </summary>
        public CoinBalanceItem GBP { get; set; }
        /// <summary>
        /// US Dollar
        /// </summary>
        public CoinBalanceItem USD { get; set; }
        /// <summary>
        /// Bitcoin
        /// </summary>
        public CoinBalanceItem BTC { get; set; }
        /// <summary>
        /// Litecoin
        /// </summary>
        public CoinBalanceItem LTC { get; set; }
        /// <summary>
        /// Bitcoin cash
        /// </summary>
        public CoinBalanceItem BCH { get; set; }
        /// <summary>
        /// NEO
        /// </summary>
        public CoinBalanceItem NEO { get; set; }
        /// <summary>
        /// Ethereum
        /// </summary>
        public CoinBalanceItem ETH { get; set; }
        /// <summary>
        /// Ethereum classic
        /// </summary>
        public CoinBalanceItem ETC { get; set; }
        /// <summary>
        /// Monero
        /// </summary>
        public CoinBalanceItem XMR { get; set; }
        /// <summary>
        /// ZCash
        /// </summary>
        public CoinBalanceItem ZEC { get; set; }
        /// <summary>
        /// Namecoin
        /// </summary>
        public CoinBalanceItem NMC { get; set; }
        /// <summary>
        /// Dash
        /// </summary>
        public CoinBalanceItem DASH { get; set; }
        /// <summary>
        /// Tether
        /// </summary>
        public CoinBalanceItem USDT { get; set; }
        /// <summary>
        /// Dogecoin
        /// </summary>
        public CoinBalanceItem DOGE { get; set; }
        /// <summary>
        /// Ripple
        /// </summary>
        public CoinBalanceItem XRP { get; set; }
        /// <summary>
        /// KOMODO
        /// </summary>
        public CoinBalanceItem KMD { get; set; }
        /// <summary>
        /// Decred
        /// </summary>
        public CoinBalanceItem DCR { get; set; }
        /// <summary>
        /// Lisk
        /// </summary>
        public CoinBalanceItem LSK { get; set; }
        /// <summary>
        /// PIVX
        /// </summary>
        public CoinBalanceItem PIVX { get; set; }
        /// <summary>
        /// QTUM
        /// </summary>
        public CoinBalanceItem QTUM { get; set; }
        /// <summary>
        /// STRAT
        /// </summary>
        public CoinBalanceItem STRAT { get; set; }
        /// <summary>
        /// TRX
        /// </summary>
        public CoinBalanceItem TRX { get; set; }
        /// <summary>
        /// WAVES
        /// </summary>
        public CoinBalanceItem WAVES { get; set; }
        /// <summary>
        /// NEM
        /// </summary>
        public CoinBalanceItem XEM { get; set; }
        /// <summary>
        /// Verge
        /// </summary>
        public CoinBalanceItem XVG { get; set; }
        /// <summary>
        /// NAV Coin
        /// </summary>
        public CoinBalanceItem NAV { get; set; }
    }

    public class CoinBalanceItem
    {
        /// <summary>
        /// The coin balance as an integer in Satoshis.
        /// </summary>
        public decimal balance { get; set; }
        /// <summary>
        /// The coin balance as a floating point number.
        /// </summary>
        public decimal balancef { get; set; }

        /// <summary>
        /// available or not
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// online or not
        /// </summary>
        public string coin_status { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
