﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.WebServices.Payment.CoinPayments;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class IPNGenericResult
    {
        /// <summary>
        /// 1.0
        /// </summary>
        [Required]
        public string ipn_version { get; set; }
        /// <summary>
        /// Currently: 'simple, 'button', 'cart', 'donation', 'deposit', 'withdrawal', or 'api'	
        /// </summary>
        [Required]
        public string ipn_type { get; set; }
        /// <summary>
        /// Currently: 'hmac'
        /// </summary>
        [Required]
        public string ipn_mode { get; set; }
        /// <summary>
        /// The unique identifier of this IPN
        /// </summary>
        [Required]
        public string ipn_id { get; set; }
        /// <summary>
        /// Your merchant ID (you can find this on the My Account page).
        /// </summary>
        [Required]
        public string merchant { get; set; }
        public CoinPaymentsIPNType? IPNType => _ipnType ?? (_ipnType = IPNTypeHelper.GetIPNType(ipn_type));
        private CoinPaymentsIPNType? _ipnType;
    }
    public class IPNDepositResult : IPNGenericResult
    {
        /// <summary>
        /// Coin address the payment was received on.
        /// </summary>
        [Required]
        public string address { get; set; }
        /// <summary>
        /// For coins that use an extra tag it will include it here. For example Ripple Destination Tag, Monero Payment ID, etc.
        /// </summary>
        [Required]
        public string dest_tag { get; set; }
        /// <summary>
        /// The coin transaction ID of the payment.
        /// </summary>
        [Required]
        public string txn_id { get; set; }
        /// <summary>
        /// Numeric status of the payment, currently 0 = pending and 100 = confirmed/complete. For future proofing you should use the same logic as Payment Statuses.
        /// IMPORTANT: You should never ship/release your product until the status is >= 100	
        /// </summary>
        [Required]
        public int status { get; set; }
        /// <summary>
        /// A text string describing the status of the payment. (useful for displaying in order comments)
        /// </summary>
        [Required]
        public string status_text { get; set; }
        /// <summary>
        /// The coin the buyer paid with.
        /// </summary>
        [Required]
        public string currency { get; set; }
        /// <summary>
        /// The number of confirms the payment has.
        /// </summary>
        [Required]
        public int confirms { get; set; }
        /// <summary>
        /// The total amount of the payment
        /// </summary>
        [Required]
        public decimal amount { get; set; }
        /// <summary>
        /// The total amount of the payment in Satoshis
        /// </summary>
        [Required]
        public decimal amounti { get; set; }
        /// <summary>
        /// Optional: The fee deducted by CoinPayments (only sent when status >= 100)	
        /// </summary>
        public decimal? fee { get; set; }
        /// <summary>
        /// Optional: The fee deducted by CoinPayments in Satoshis (only sent when status >= 100)	
        /// </summary>
        public decimal? feei { get; set; }

        public bool SuccessStatusLax(decimal receivedAmount)
        {
            if (receivedAmount > 0)
            {
                // we trust Coinpayment so as soon as they receive funds (status == 1)
                // and email customer ont their end that funds are received - we release product
                // otherwise Coinpayment would email customer that transaction is complete
                // and there would be 10 minute delay until those funds are forwarded to our wallets
                // bad customer experience
                return status >= 100 || status == 2 || status == 1;
            }
            return false;
        }
    }
    public class IPNWithdrawalResult : IPNGenericResult
    {
        /// <summary>
        /// The ID of the withdrawal ('id' field returned from 'create_withdrawal'.)
        /// </summary>
        [Required]
        public string id { get; set; }
        /// <summary>
        /// Numeric status of the withdrawal, currently 0 = waiting email confirmation, 1 = pending, and 2 = sent/complete.	
        /// </summary>
        [Required]
        public int status { get; set; }
        /// <summary>
        /// A text string describing the status of the withdrawal. 
        /// </summary>
        [Required]
        public string status_text { get; set; }
        /// <summary>
        /// Coin address the withdrawal was sent to.
        /// </summary>
        [Required]
        public string address { get; set; }
        /// <summary>
        /// The coin transaction ID of the withdrawal.
        /// </summary>
        public string txn_id { get; set; }
        /// <summary>
        /// The coin of the withdrawal.
        /// </summary>
        [Required]
        public string currency { get; set; }
        /// <summary>
        /// The total amount of the withdrawal
        /// </summary>
        [Required]
        public decimal amount { get; set; }
        /// <summary>
        /// The total amount of the withdrawal in Satoshis
        /// </summary>
        [Required]
        public decimal amounti { get; set; }
    }
    public class IPNAPIResult : IPNGenericResult
    {
        /// <summary>
        /// The status of the payment. See Payment Statuses for details.
        /// </summary>
        [Required]
        public int status { get; set; }
        /// <summary>
        /// A text string describing the status of the payment. (useful for displaying in order comments)
        /// </summary>
        [Required]
        public string status_text { get; set; }
        /// <summary>
        /// The unique ID of the payment. 
        /// Your IPN handler should be able to handle a TransactionId composed of any combination of a-z, A-Z, 0-9, and - up to 128 characters long for future proofing.
        /// </summary>
        [Required]
        public string txn_id { get; set; }
        /// <summary>
        /// The original currency/coin submitted.
        /// </summary>
        [Required]
        public string currency1 { get; set; }
        /// <summary>
        /// The coin the buyer paid with.
        /// </summary>
        [Required]
        public string currency2 { get; set; }
        /// <summary>
        /// The amount of the payment in your original currency/coin.
        /// </summary>
        [Required]
        public string amount1 { get; set; }
        /// <summary>
        /// The amount of the payment in the buyer's coin.
        /// </summary>
        [Required]
        public string amount2 { get; set; }
        /// <summary>
        /// The fee on the payment in the buyer's selected coin.
        /// </summary>
        [Required]
        public string fee { get; set; }
        /// <summary>
        /// Optional: The name of the buyer.
        /// </summary>
        public string buyer_name { get; set; }
        /// <summary>
        /// Optional: Buyer's email address.
        /// </summary>
        public string email { get; set; }
        /// <summary>
        /// Optional: The name of the item that was purchased.
        /// </summary>
        public string item_name { get; set; }
        /// <summary>
        /// Optional: This is a passthru variable for your own use.	
        /// </summary>
        public string item_number { get; set; }
        /// <summary>
        /// Optional: This is a passthru variable for your own use.
        /// </summary>
        public string invoice { get; set; }
        /// <summary>
        /// Optional: This is a passthru variable for your own use.
        /// </summary>
        public string custom { get; set; }
        /// <summary>
        /// Optional: The TX ID of the payment to the merchant. Only included when 'status' >= 100 and if the payment mode is set to ASAP or Nightly or if the payment is PayPal Passthru.
        /// </summary>
        public string send_tx { get; set; }
        /// <summary>
        /// Optional: The amount of currency2 received at the time the IPN was generated.
        /// </summary>
        public string received_amount { get; set; }
        /// <summary>
        /// The number of confirms of ReceivedAmount at the time the IPN was generated.
        /// </summary>
        public string received_confirms { get; set; }

        public bool SuccessStatus()
        {
            return status >= 100 || status == 2;
        }

        public bool SuccessStatusLax(decimal mustReceiveAmount, decimal receivedAmount)
        {
            if (receivedAmount > 0)
            {
                if (mustReceiveAmount == receivedAmount)
                {
                    // we trust Coinpayment so as soon as they receive funds (status == 1)
                    // and email customer ont their end that funds are received - we release product
                    // otherwise Coinpayment would email customer that transaction is complete
                    // and there would be 10 minute delay until those funds are forwarded to our wallets
                    // bad customer experience
                    return status >= 100 || status == 2 || status == 1;
                }
            }
            return false;
        }
    }
#pragma warning restore IDE1006 // Naming Styles
}

public static class IPNTypeHelper
{
    public static CoinPaymentsIPNType GetIPNType(string ipnTypeString)
    {
        ipnTypeString = ipnTypeString.ToLower().Trim();
        switch (ipnTypeString)
        {
            case "deposit":
                return CoinPaymentsIPNType.Deposit;
            case "withdrawal":
                return CoinPaymentsIPNType.Withdrawal;
            case "api":
                return CoinPaymentsIPNType.Api;
            case "button":
                return CoinPaymentsIPNType.Button;
            case "cart":
                return CoinPaymentsIPNType.Cart;
            case "donation":
                return CoinPaymentsIPNType.Donation;
            case "simple":
                return CoinPaymentsIPNType.Simple;
            default:
                return CoinPaymentsIPNType.None;
        }
    }
}
