﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CoinPaymentsConvertCoinsResult : CoinPaymentsResult
    {
        public new ConvertCoins result { get; set; }
    }

    public class ConvertCoins
    {
        public string id { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
