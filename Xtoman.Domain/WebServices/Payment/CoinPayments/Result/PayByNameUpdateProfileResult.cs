﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CoinPaymentsPayByNameUpdateProfileResult : CoinPaymentsResult
    {
        /// <summary>
        /// Null
        /// </summary>
        public new object result { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
