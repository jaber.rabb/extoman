﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class CoinPaymentsCheckout
    {
        public string currency { get; set; }
        public long amount { get; set; }
        public long test { get; set; }
        public string item_number { get; set; }
        public string item_name { get; set; }
        public object[] details { get; set; }
        public string invoice { get; set; }
        public string custom { get; set; }
        public string ipn_url { get; set; }
    }

    public class CoinPaymentsWithdrawalInformationResult : CoinPaymentsResult
    {
        public new WithdrawalInformation result { get; set; }
    }

    public class WithdrawalInformation
    {
        /// <summary>
        /// The time the withdrawal request was submitted.
        /// </summary>
        public long time_created { get; set; }
        /// <summary>
        /// The status of the withdrawal 
        /// </summary>
        public WithdrawalInfoStatus status { get; set; }
        /// <summary>
        /// The status of the withdrawal in text format.
        /// </summary>
        public string status_text { get; set; }
        /// <summary>
        /// The ticker symbol of the coin for the withdrawal.
        /// </summary>
        public string coin { private get; set; }
        /// <summary>
        /// The amount of the withdrawal (in Satoshis).
        /// </summary>
        public decimal amount { get; set; }
        /// <summary>
        /// The amount of the withdrawal (as a floating point number).
        /// </summary>
        public string amountf { get; set; }
        /// <summary>
        /// The address the withdrawal was sent to. (only in response if status == Completed)
        /// </summary>
        public string send_address { get; set; }
        /// <summary>
        /// The coin TX ID of the send. (only in response if status == Completed)
        /// </summary>
        public string send_txid { get; set; }

        /// <summary>
        /// The ticker symbol of the coin for the withdrawal.
        /// </summary>
        [JsonIgnore]
        public CoinPaymentsCurrency Coin => coin.ToCoinPaymentsCurrency().Value;
    }
}
