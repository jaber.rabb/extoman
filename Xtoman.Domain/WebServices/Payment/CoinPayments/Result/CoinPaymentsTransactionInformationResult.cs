﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class CoinPaymentsTransactionInformationResult : CoinPaymentsResult
    {
        public new TransactionInformation result { get; set; }
    }

    public class TransactionInformation
    {
        public long time_created { get; set; }
        public long time_expires { get; set; }
        public long status { get; set; }
        public string status_text { get; set; }
        public string type { get; set; }
        public string coin { get; set; }
        public long amount { get; set; }
        public string amountf { get; set; }
        public long received { get; set; }
        public string receivedf { get; set; }
        public long recv_confirms { get; set; }
        public string payment_address { get; set; }
        public long time_completed { get; set; }
        public CoinPaymentsCheckout checkout { get; set; }
        public object[] shipping { get; set; }
    }
}
