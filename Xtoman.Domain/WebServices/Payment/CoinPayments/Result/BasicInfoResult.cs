﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CoinPaymentsBasicInfoResult : CoinPaymentsResult
    {
        public new BasicInfo result { get; set; }
    }

    public class BasicInfo
    {
        public string username { get; set; }
        public string merchant_id { get; set; }
        public string email { get; set; }
        /// <summary>
        /// May be blank if not set!!!
        /// </summary>
        public string public_name { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
