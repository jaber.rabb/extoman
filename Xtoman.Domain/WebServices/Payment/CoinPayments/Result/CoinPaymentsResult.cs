﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CoinPaymentsResult
    {
        [JsonProperty("error")]
        public string error { get; set; }
        [JsonProperty("result")]
        public object result { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
