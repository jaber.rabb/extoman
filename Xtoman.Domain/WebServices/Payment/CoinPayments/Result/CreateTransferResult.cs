﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CoinPaymentsCreateTransferResult : CoinPaymentsResult
    {
        public new CreateTransfer result { get; set; }
    }

    public class CreateTransfer
    {
        /// <summary>
        /// The CoinPayments transfer/withdrawal ID. (This is not a coin network TX ID.)
        /// </summary>
        public string id { get; set; }
        public CreateTransferStatus status { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
