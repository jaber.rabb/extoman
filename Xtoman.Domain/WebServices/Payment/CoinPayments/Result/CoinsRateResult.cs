﻿using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    [Serializable]
    public class CoinPaymentsCoinsRateResult : CoinPaymentsResult
    {
        public new Dictionary<string, CoinsRateItem> result { get; set; }
    }

    [Serializable]
    public class CoinsRateItem
    {
        public string is_fiat { get; set; }
        public decimal rate_btc { get; set; }
        public long last_update { get; set; }
        public string name { get; set; }
        public int confirms { get; set; }
        public decimal tx_fee { get; set; }
        public string status { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
