﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class CoinBalancesRequest : CoinPaymentsGenericRequest
    {
        public CoinBalancesRequest()
        {
            cmd = "balances";
        }
    }
}
