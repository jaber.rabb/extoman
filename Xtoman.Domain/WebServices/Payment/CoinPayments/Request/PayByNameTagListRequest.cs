﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class PayByNameTagListRequest : CoinPaymentsGenericRequest
    {
        public PayByNameTagListRequest()
        {
            this.cmd = "get_pbn_list";
        }
    }
}
