﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class CreateTransactionRequest : CoinPaymentsGenericRequest
    {
        public decimal amount;
        public string currency1;
        public string currency2;
        public string address;
        public string buyer_email;
        public string buyer_name;
        public string item_name;
        public string item_number;
        public string invoice;
        public string custom;
        public string ipn_url;
        public CreateTransactionRequest(decimal amount, string currency1, string currency2, string custom, string ipn_url)
        {
            this.cmd = "create_transaction";
            this.amount = amount;
            this.currency1 = currency1;
            this.currency2 = currency2;
            this.custom = custom;
            this.ipn_url = ipn_url;
        }
        public CreateTransactionRequest(decimal amount, string buyerEmail, string currency1, string currency2, string custom, string ipn_url)
        {
            this.cmd = "create_transaction";
            this.amount = amount;
            this.currency1 = currency1;
            this.currency2 = currency2;
            this.custom = custom;
            this.ipn_url = ipn_url;
            this.buyer_email = buyerEmail;
        }
        public CreateTransactionRequest(CreateTransactionInput input)
        {
            this.cmd = "create_transaction";
            this.amount = input.amount;
            this.currency1 = input.currency1;
            this.currency2 = input.currency2;
            this.address = input.address;
            this.buyer_email = input.buyer_email;
            this.buyer_name = input.buyer_name;
            this.item_name = input.item_name;
            this.item_number = input.item_number;
            this.invoice = input.invoice;
            this.custom = input.custom;
            this.ipn_url = input.ipn_url;
        }
    }
}
