﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class CoinsRateRequest : CoinPaymentsGenericRequest
    {
        public CoinsRateRequest()
        {
            this.cmd = "rates";
        }
    }
}
