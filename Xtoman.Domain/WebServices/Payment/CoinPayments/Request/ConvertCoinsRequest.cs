﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class ConvertCoinsRequest : CoinPaymentsGenericRequest
    {
        public decimal amount;
        public string from;
        public string to;
        public string address;
        public string dest_tag;
        public ConvertCoinsRequest(ConvertCoinsInput input)
        {
            this.cmd = "convert";
            this.amount = input.amount;
            this.from = input.from;
            this.to = input.to;
            this.address = input.address;
            this.amount = input.amount;
        }
    }
}
