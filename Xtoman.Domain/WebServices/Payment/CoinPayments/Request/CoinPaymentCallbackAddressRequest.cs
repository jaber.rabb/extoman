﻿using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class CoinPaymentCallbackAddressRequest : CoinPaymentsGenericRequest
    {
        public string currency;
        public string ipn_url;
        public CoinPaymentCallbackAddressRequest(CoinPaymentsCurrency currency, string ipn_url)
        {
            this.cmd = "get_callback_address";
            this.currency = currency.ToDisplay(DisplayProperty.ShortName);
            this.ipn_url = ipn_url;
        }

        public CoinPaymentCallbackAddressRequest(string currency, string ipn_url)
        {
            this.cmd = "get_callback_address";
            this.currency = currency;
            this.ipn_url = ipn_url;
        }
    }
}
