﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class CoinPaymentsGenericRequest
    {
        public string cmd { get; internal set; }
        public string version => "1";
        public string key => AppSettingManager.CoinPayments_PublicKey;
        [JsonIgnore]
        private string RequestBody => JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
        public string GetQueryString()
        {
            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(RequestBody);
            return dict.ToQueryParameters();
        }
    }
}
