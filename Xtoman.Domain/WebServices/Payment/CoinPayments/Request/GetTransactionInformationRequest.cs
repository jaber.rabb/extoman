﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class GetTransactionInformationRequest : CoinPaymentsGenericRequest
    {
        public string txid;
        public int full;
        public GetTransactionInformationRequest(string txid, bool fullResult = false)
        {
            this.cmd = "get_tx_info";
            this.txid = txid;
            this.full = fullResult ? 1 : 0;
        }
    }
}
