﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class PayByNameProfileInfoRequest : CoinPaymentsGenericRequest
    {
        public string pbntag;

        public PayByNameProfileInfoRequest(string pbntag)
        {
            this.cmd = "get_pbn_info";
            this.pbntag = pbntag;
        }
    }
}
