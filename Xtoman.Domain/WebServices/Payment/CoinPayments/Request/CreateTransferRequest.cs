﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class CreateTransferRequest : CoinPaymentsGenericRequest
    {
        public decimal amount;
        public string currency;
        public string merchant;
        public string pbntag;
        public byte auto_confirm;
        public CreateTransferRequest(CreateTransferInput input)
        {
            this.cmd = "create_transfer";
            this.amount = input.amount;
            this.currency = input.currency;
            this.merchant = input.merchant;
            this.pbntag = input.pbntag;
            this.auto_confirm = input.auto_confirm;
        }
    }
}
