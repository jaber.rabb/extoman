﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class PayByNameUpdateProfileRequest : CoinPaymentsGenericRequest
    {
        public string tagid;
        public string name;
        public string email;
        public string url;
        public string image;
        public PayByNameUpdateProfileRequest(PayByNameUpdateProfileInput input)
        {
            this.cmd = "update_pbn_tag";
            this.tagid = input.tagid;
            this.name = input.name;
            this.email = input.email;
            this.url = input.url;
            this.image = input.image;
        }
    }
}
