﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class ConversionInformationRequest : CoinPaymentsGenericRequest
    {
        public string id;
        public ConversionInformationRequest(string id)
        {
            cmd = "get_conversion_info";
            this.id = id;
        }
    }
}
