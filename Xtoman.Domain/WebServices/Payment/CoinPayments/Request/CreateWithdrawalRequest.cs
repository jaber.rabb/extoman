﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class CreateWithdrawalRequest : CoinPaymentsGenericRequest
    {
        public decimal amount;
        public string currency;
        public string currency2;
        public string address;
        public string pbntag;
        public string dest_tag;
        public string ipn_url;
        public byte auto_confirm;
        public string note;
        public int add_tx_fee;
        public CreateWithdrawalRequest(CreateWithdrawalInput input, bool? addTransactionFee = null)
        {
            this.cmd = "create_withdrawal";
            this.amount = input.amount;
            this.currency = input.currency;
            this.currency2 = input.currency2;
            this.address = input.address;
            this.pbntag = input.pbntag;
            this.dest_tag = input.dest_tag;
            this.ipn_url = input.ipn_url;
            this.auto_confirm = input.auto_confirm;
            this.note = input.note;
            if (addTransactionFee.HasValue && addTransactionFee.Value)
                this.add_tx_fee = 1;
            else
                this.add_tx_fee = 0;
        }
    }
}
