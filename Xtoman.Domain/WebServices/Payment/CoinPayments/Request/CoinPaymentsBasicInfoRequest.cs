﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class CoinPaymentsBasicInfoRequest : CoinPaymentsGenericRequest
    {
        public CoinPaymentsBasicInfoRequest()
        {
            cmd = "get_basic_info";
        }
    }
}
