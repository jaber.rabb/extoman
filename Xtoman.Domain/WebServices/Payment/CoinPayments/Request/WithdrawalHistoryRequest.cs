﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class WithdrawalHistoryRequest : CoinPaymentsGenericRequest
    {
        public int? limit { get; set; }
        public int? start { get; set; }
        public int? newer { get; set; }
        public WithdrawalHistoryRequest(WithdrawalHistoryInput input)
        {
            this.cmd = "get_withdrawal_history";
            this.limit = input.limit;
            this.start = input.start;
            this.newer = input.newer;
        }
    }
}
