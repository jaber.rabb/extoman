﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class PayByNameTagClaimRequest : CoinPaymentsGenericRequest
    {
        public string tagid;
        public string name;
        public PayByNameTagClaimRequest(PayByNameTagClaimInput input)
        {
            this.cmd = "claim_pbn_tag";
            this.tagid = input.tagid;
            this.name = input.name;
        }
    }
}
