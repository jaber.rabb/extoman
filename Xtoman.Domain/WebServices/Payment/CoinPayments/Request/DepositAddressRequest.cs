﻿using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class DepositAddressRequest : CoinPaymentsGenericRequest
    {
        public string currency;
        public DepositAddressRequest(CoinPaymentsCurrency currency)
        {
            this.cmd = "get_deposit_address";
            this.currency = currency.ToDisplay(DisplayProperty.ShortName);
        }
    }
}
