﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class WithdrawalInformationRequest : CoinPaymentsGenericRequest
    {
        public string id;
        public WithdrawalInformationRequest(string id)
        {
            this.cmd = "get_withdrawal_info";
            this.id = id;
        }
    }
}
