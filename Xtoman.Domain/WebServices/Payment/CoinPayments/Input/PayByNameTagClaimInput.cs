﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class PayByNameTagClaimInput
    {
        /// <summary>
        /// The tag's unique ID (obtained from PayByNameTagList Method).
        /// </summary>
        public string tagid { get; set; }
        /// <summary>
        /// Name for the tag; for example a value of 'Apple' would be the PayByName tag $Apple. Make sure to use the case you want the tag displayed with.
        /// </summary>
        public string name { get; set; }
    }
}
