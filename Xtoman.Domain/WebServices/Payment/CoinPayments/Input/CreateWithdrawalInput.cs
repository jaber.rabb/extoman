﻿using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CreateWithdrawalInput
    {
        /// <summary>
        /// The amount of the withdrawal in the currency.
        /// </summary>
        public decimal amount { get; set; }
        /// <summary>
        /// The cryptocurrency to withdraw. (BTC, LTC, etc.)
        /// </summary>
        public string currency => CoinPaymentsCurrency.ToDisplay(DisplayProperty.ShortName);
        /// <summary>
        /// (Optional) Currency to use to to withdraw 'amount' worth of 'currency2' in 'currency' coin. This is for exchange rate calculation only and will not convert coins or change which currency is withdrawn. For example, to withdraw 1.00 USD worth of BTC you would specify 'currency'='BTC', 'currency2'='USD', and 'amount'='1.00'
        /// </summary>
        public string currency2 => CoinPaymentsCurrency2.HasValue ? CoinPaymentsCurrency2.ToDisplay(DisplayProperty.ShortName) : string.Empty;
        /// <summary>
        /// The address to send the funds to, either this OR pbntag must be specified. Remember: this must be an address in currency's network.
        /// </summary>
        public string address { get; set; }
        /// <summary>
        /// The $PayByName tag to send the withdrawal to, either this OR address must be specified. This will also override any destination tag specified.
        /// </summary>
        public string pbntag { get; set; }
        /// <summary>
        /// (Optional) The destination tag to use for the withdrawal (for Ripple.)
        /// </summary>
        public string dest_tag { get; set; }
        /// <summary>
        /// (Optional) URL for your IPN callbacks. If not set it will use the IPN URL in your Edit Settings page if you have one set.
        /// </summary>
        public string ipn_url { get; set; }
        /// <summary>
        /// (Optional) If set to 1, withdrawal will complete without email confirmation.
        /// </summary>
        public byte auto_confirm { get; set; }
        /// <summary>
        /// (Optional) This lets you set the note for the withdrawal.
        /// </summary>
        public string note { get; set; }

        /// <summary>
        /// The cryptocurrency to withdraw. (BTC, LTC, etc.)
        /// </summary>
        public CoinPaymentsCurrency CoinPaymentsCurrency { private get; set; }
        /// <summary>
        /// (Optional) Currency to use to to withdraw 'amount' worth of 'currency2' in 'currency' coin. This is for exchange rate calculation only and will not convert coins or change which currency is withdrawn. For example, to withdraw 1.00 USD worth of BTC you would specify 'currency'='BTC', 'currency2'='USD', and 'amount'='1.00'
        /// </summary>
        public CoinPaymentsCurrency? CoinPaymentsCurrency2 { private get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
