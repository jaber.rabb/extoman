﻿using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class ConvertCoinsInput
    {
        /// <summary>
        /// The amount convert in the 'from' currency below.	
        /// </summary>
        public decimal amount { get; set; }
        /// <summary>
        /// The cryptocurrency in your Coin Wallet to convert from. (BTC, LTC, etc.)	
        /// </summary>
        public string from => fromCurrency.ToDisplay(DisplayProperty.ShortName);
        /// <summary>
        /// The cryptocurrency to convert to. (BTC, LTC, etc.)	
        /// </summary>
        public string to => toCurrency.ToDisplay(DisplayProperty.ShortName);
        /// <summary>
        /// (Optional) The address to send the funds to. If blank or not included the coins will go to your CoinPayments Wallet.	
        /// </summary>
        public string address { get; set; }
        /// <summary>
        /// (Optional) The destination tag to use for the withdrawal (for Ripple.) If 'address' is not included this has no effect.
        /// </summary>
        public string dest_tag { get; set; }
        /// <summary>
        /// The cryptocurrency in your Coin Wallet to convert from. (BTC, LTC, etc.)	
        /// </summary>
        public CoinPaymentsCurrency fromCurrency { private get; set; }
        /// <summary>
        /// The cryptocurrency to convert to. (BTC, LTC, etc.)	
        /// </summary>
        public CoinPaymentsCurrency toCurrency { private get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
