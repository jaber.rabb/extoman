﻿using Newtonsoft.Json;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CreateTransactionInput
    {
        /// <summary>
        /// The amount of the transaction in the original currency (currency1).
        /// </summary>
        public decimal amount { get; set; }
        /// <summary>
        /// The original currency of the transaction.
        /// </summary>
        public string currency1 => CoinPaymentsCurrency1.ToDisplay(DisplayProperty.ShortName);
        /// <summary>
        /// The currency the buyer will be sending. For example if your products are priced in USD but you are receiving BTC, you would use currency1=USD and currency2=BTC. currency1 and currency2 can be set to the same thing if you don't need currency conversion.	
        /// </summary>
        public string currency2 => CoinPaymentsCurrency2.ToDisplay(DisplayProperty.ShortName);
        /// <summary>
        /// (Optional) Optionally set the address to send the funds to (if not set will use the settings you have set on the 'Coins Acceptance Settings' page). Remember: this must be an address in currency2's network.
        /// </summary>
        public string address { get; set; }
        /// <summary>
        /// (Recommended Optional) Optionally (but highly recommended) set the buyer's email address. This will let us send them a notice if they underpay or need a refund. We will not add them to our mailing list or spam them or anything like that.
        /// </summary>
        public string buyer_email { get; set; }
        /// <summary>
        /// (Optional) Optionally set the buyer's name for your reference.
        /// </summary>
        public string buyer_name { get; set; }
        /// <summary>
        /// (Optional) Item name for your reference, will be on the payment information page and in the IPNs for the transaction.
        /// </summary>
        public string item_name { get; set; }
        /// <summary>
        /// (Optional) Item number for your reference, will be on the payment information page and in the IPNs for the transaction.
        /// </summary>
        public string item_number { get; set; }
        /// <summary>
        /// (Optional) Another field for your use, will be on the payment information page and in the IPNs for the transaction.
        /// </summary>
        public string invoice { get; set; }
        /// <summary>
        /// (Optional) Another field for your use, will be on the payment information page and in the IPNs for the transaction.
        /// </summary>
        public string custom { get; set; }
        /// <summary>
        /// (Optional) URL for your IPN callbacks. If not set it will use the IPN URL in your Edit Settings page if you have one set.
        /// </summary>
        public string ipn_url { get; set; }
        /// <summary>
        /// The original currency of the transaction.
        /// </summary>
        [JsonIgnore]
        public CoinPaymentsCurrency CoinPaymentsCurrency1 { private get; set; }
        /// <summary>
        /// The currency the buyer will be sending. For example if your products are priced in USD but you are receiving BTC, you would use currency1=USD and currency2=BTC. currency1 and currency2 can be set to the same thing if you don't need currency conversion.	
        /// </summary>
        [JsonIgnore]
        public CoinPaymentsCurrency CoinPaymentsCurrency2 { private get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
