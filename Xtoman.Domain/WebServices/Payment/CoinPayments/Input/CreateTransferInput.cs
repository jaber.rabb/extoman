﻿using Newtonsoft.Json;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
#pragma warning disable IDE1006 // Naming Styles
    public class CreateTransferInput
    {
        /// <summary>
        /// The amount of the transfer in the currency.
        /// </summary>
        public decimal amount { get; set; }
        /// <summary>
        /// The cryptocurrency to withdraw. (BTC, LTC, etc.)
        /// </summary>
        public string currency => CoinPaymentsCurrency.ToDisplay(DisplayProperty.ShortName);
        /// <summary>
        /// The merchant ID to send the funds to, either this OR pbntag must be specified. Remember: this is a merchant ID and not a username.
        /// </summary>
        public string merchant { get; set; }
        /// <summary>
        /// The $PayByName tag to send the funds to, either this OR merchant must be specified.
        /// </summary>
        public string pbntag { get; set; }
        /// <summary>
        /// If set to 1, withdrawal will complete without email confirmation.
        /// </summary>
        public byte auto_confirm { get; set; }
        /// <summary>
        /// The cryptocurrency to withdraw. (BTC, LTC, etc.)
        /// </summary>
        [JsonIgnore]
        public CoinPaymentsCurrency CoinPaymentsCurrency { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
