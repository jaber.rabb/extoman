﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class WithdrawalHistoryInput
    {
        /// <summary>
        /// The maximum number of withdrawals to return from 1-100. (default: 25)
        /// </summary>
        public int? limit { get; set; }
        /// <summary>
        /// What withdrawals # to start from (for iteration/pagination.) (default: 0, starts with your newest withdrawals.)	
        /// </summary>
        public int? start { get; set; }
        /// <summary>
        /// Return withdrawals submitted at the given Unix timestamp or later. (default: 0)
        /// </summary>
        public int? newer { get; set; }
    }
}
