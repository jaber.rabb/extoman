﻿namespace Xtoman.Domain.WebServices.Payment.CoinPayments
{
    public class PayByNameUpdateProfileInput
    {
        /// <summary>
        /// The tag's unique ID (obtained from PayByNameTagList Method).
        /// </summary>
        public string tagid { get; set; }
        /// <summary>
        /// (Optional) Name for the profile. If field is not supplied the current name will be unchanged.
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// (Optional) Name for the profile. If field is not supplied the current name will be unchanged.
        /// </summary>
        public string email { get; set; }
        /// <summary>
        /// (Optional) Website URL for the profile. If field is not supplied the current URL will be unchanged.	
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// (Optional) HTTP POST with a JPG or PNG image 250KB or smaller. This is an actual "multipart/form-data" file POST and not a URL to a file. If field is not supplied the current image will be unchanged.
        /// </summary>
        public string image { get; set; }
    }
}
