﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Domain.WebServices
{
    public class AutoPayResult
    {
        public bool Status { get; set; }
        public string Text { get; set; }
        public string TransactionCode { get; set; }
    }
}
