﻿using System;
using System.Collections.Generic;
using Xtoman.Domain.Models;

namespace Xtoman.Domain.WebServices
{
    public class CoinPaymentAddressResult
    {
        public string Address { get; set; }
        public string MemoTagPaymentId { get; set; }
        public string PubKey { get; set; }
    }

    public class CoinPaymentFullAddressResult
    {
        public string Id { get; set; }
        public string Address { get; set; }
        public string MemoTagPaymentId { get; set; }
        public DateTime TimeEnd { get; set; }
        public string QRUrl { get; set; }
        public string StatusUrl { get; set; }
        public int ConfirmsNeed { get; set; }
        public WalletApiType ApiType { get; set; }
        public string PubKey { get; set; }
    }

    public class BalanceResult
    {
        public BalanceResult()
        {
            Balances = new List<BalanceResultItem>();
        }
        public List<BalanceResultItem> Balances { get; set; }
        //public string PerfectMoneyBalance { get; set; }
        //public string PAYEERBalance { get; set; }
        //public string WebMoneyBalance { get; set; }
    }

    public class BalanceResultItem
    {
        public ECurrencyType Type { get; set; }
        public decimal Balance { get; set; }
        public WalletApiType WalletApiType { get; set; }
    }
}
