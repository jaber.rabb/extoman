﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices.Payment.Mellat
{
#pragma warning disable IDE1006 // Naming Styles
    public class PayRequestResult
    {
        public string RefId { get; set; }
        public int ResCode { get; set; }
        public long saleOrderId { get; set; }
        public long saleReferenceId { get; set; }
        public MellatResponseMessage ResponseMessage { get; set; }
    }
#pragma warning restore IDE1006 // Naming Styles
}
