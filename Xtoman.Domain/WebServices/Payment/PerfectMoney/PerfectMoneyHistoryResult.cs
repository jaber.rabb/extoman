﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PerfectMoneyHistoryResult
    {
        public string Batch { get; set; }
        public DateTime Time { get; set; }
        public string Type { get; set; }
        public string Currency { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public string Payer_Account { get; set; }
        public string Payee_Account  { get; set; }
        public string Payment_ID  { get; set; }
        public string Memo { get; set; }
    }
}
