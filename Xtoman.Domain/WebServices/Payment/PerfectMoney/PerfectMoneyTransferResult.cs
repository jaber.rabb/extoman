﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PerfectMoneyTransferResult : PerfectMoneyBaseResult
    {
        public string Payee_Account_Name { get; set; }
        public string Payer_Account { get; set; }
        public string Payee_Account { get; set; }
        public string PAYMENT_AMOUNT { get; set; }
        public string PAYMENT_BATCH_NUM { get; set; }
        public string PAYMENT_ID { get; set; }
    }
}
