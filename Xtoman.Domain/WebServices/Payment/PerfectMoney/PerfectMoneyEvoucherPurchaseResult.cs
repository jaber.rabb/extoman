﻿namespace Xtoman.Domain.WebServices
{
    public class PerfectMoneyEvoucherPurchaseResult : PerfectMoneyBaseResult
    {
        public string PayerAccount { get; set; }
        public double PaymentAmount { get; set; }
        public string PaymentBatchNumber { get; set; }
        public string VoucherNumber { get; set; }
        public string VoucherCode { get; set; }
        public double VoucherAmount { get; set; }
    }
}
