﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PerfectMoneyBalanceResult : PerfectMoneyBaseResult
    {
        public List<PerfectMoneyBalanceItem> Accounts { get; set; }
    }

    public class PerfectMoneyBalanceItem
    {
        public string WalletNumber { get; set; }
        public decimal Balance { get; set; }
    }
}
