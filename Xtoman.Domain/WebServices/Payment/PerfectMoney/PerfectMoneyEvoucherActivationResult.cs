﻿namespace Xtoman.Domain.WebServices
{
    public class PerfectMoneyEvoucherActivationResult : PerfectMoneyBaseResult
    {
        public string VoucherNumber { get; set; }
        public double VoucherAmount { get; set; }
        public string VoucherAmountCurrency { get; set; }
        public string PayeeAccount { get; set; }
        public string PaymentBatchNumber { get; set; }
    }
}
