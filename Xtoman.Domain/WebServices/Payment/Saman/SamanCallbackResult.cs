﻿namespace Xtoman.Domain.WebServices.Payment.Saman
{
    public class SamanCallbackResult
    {
        public string State { get; set; }
        public string RefNum { get; set; }
        public string ResNum { get; set; }
    }
}
