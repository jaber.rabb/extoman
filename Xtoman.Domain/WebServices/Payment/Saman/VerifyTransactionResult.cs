﻿using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Payment.Saman
{
    public class VerifyTransactionResult
    {
        public double _amount;
        public int AmountToman
        {
            get => (_amount / 10).ToInt();
            set => _amount = value * 10;
        }

        public SamanMessageStatus Status { get; set; }
    }
}
