﻿using System.ComponentModel;

namespace Xtoman.Domain.WebServices.Payment.Saman
{
    public enum SamanMessageStatus
    {
        [Description("خطا در تراکنش از طرف خریدار")]
        BuyerProblem = -99,
        [Description("IP Address فروشنده نا معتبر است.")]
        InvalidIP = -18,
        [Description("برگشت زدن جزیی تراکنش مجاز نمی باشد.")]
        PartialRefundNotAllowed = -17,
        [Description("خطای داخلی سیستم")]
        InertnalError = -16,
        [Description("مبلغ برگشتی به صورت اعشاری داده شده است.")]
        InvalidRefundAmountDecimal = -15,
        [Description("چنین تراکنشی تعریف نشده است.")]
        TransactonNotDefind = -14,
        [Description("مبلغ برگشتی برای برگشت جزئی بیش از مبلغ برگشت نخورده ی رسید دیجیتالی است.")]
        InvalidRefundAmount = -13,
        [Description("مبلغ برگشتی منفی است.")]
        InvalidRefundAmountNegative = -12,
        [Description("طول ورودی ها کمتر از حد مجاز است.")]
        ShorterInput = -11,
        [Description("رسید دیجیتالی به صورت Base64 نیست )حاوی کارکترهای غیرمجاز است(.")]
        InvalidRefNum = -10,
        [Description("وجود کارکترهای غیرمجاز در مبلغ برگشتی.")]
        InvalidCharacterRefundAmount = -9,
        [Description("طول ورودی ها بیشتر از حد مجاز است")]
        LongerInput = -8,
        [Description("رسید دیجیتالی تهی است.")]
        RefNumIsEmpty = -7,
        [Description("سند قبلا برگشت کامل یافته است.")]
        AlreadyRefunded = -6,
        [Description("Merchant Authentication Failed ) کلمه عبور یا کد فروشنده اشتباه است(")]
        AuthenticationFailed = -4,
        [Description("ورودی ها حاوی کارکترهای غیرمجاز می باشند.")]
        InvalidCharacterInput = -3,
        [Description("خطای در پردازش اطلاعات ارسالی(شکل در یکی از ورودیرا و ناموفق بودن فراخوانی متد برگشت تراکنش)")]
        InvalidInput = -1,
        [Description("تراكنش با موفقیت انجام شد")]
        Success = 0
    }
}
