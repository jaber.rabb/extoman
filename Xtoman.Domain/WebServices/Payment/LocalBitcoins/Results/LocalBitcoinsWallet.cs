﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.LocalBitcoins
{
    public class LocalBitcoinsWalletBalance : LocalBitcoinsResult
    {
        [JsonProperty("data")]
        public LocalBitcoinsWalletBalanceData Data { get; set; }
    }

    public class LocalBitcoinsWallet : LocalBitcoinsResult
    {
        [JsonProperty("data")]
        public LocalBitcoinsWalletData Data { get; set; }
    }

    public class LocalBitcoinsWalletBalanceData
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("total")]
        public LocalBitcoinsWalletTotal Total { get; set; }

        [JsonProperty("receiving_address")]
        public string ReceivingAddress { get; set; }
    }

    public class LocalBitcoinsWalletData : LocalBitcoinsWalletBalanceData
    {
        [JsonProperty("old_address_list")]
        public List<LocalBitcoinsWalletOldAddress> OldAddressList { get; set; }

        [JsonProperty("sent_transactions_30d")]
        public List<LocalBitcoinsTransaction> SentTransactions30Days { get; set; }

        [JsonProperty("received_transactions_30d")]
        public List<LocalBitcoinsTransaction> ReceivedTransactions30Days { get; set; }
    }

    public class LocalBitcoinsTransaction
    {
        [JsonProperty("txid")]
        public string Txid { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("tx_type")]
        public long TxType { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
    }

    public class LocalBitcoinsWalletTotal
    {
        [JsonProperty("balance")]
        public string Balance { get; set; }

        [JsonProperty("sendable")]
        public string Sendable { get; set; }
    }

    public class LocalBitcoinsWalletOldAddress
    {
        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("received")]
        public string Received { get; set; }
    }
}
