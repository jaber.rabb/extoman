﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.Payment.LocalBitcoins
{
    public class LocalBitcoinsAccountInfo : LocalBitcoinsResult
    {
        [JsonProperty("data")]
        public LocalBitcoinsAccountInfoData Data { get; set; }
    }

    public class LocalBitcoinsAccountInfoData
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("trading_partners_count")]
        public long TradingPartnersCount { get; set; }

        [JsonProperty("feedbacks_unconfirmed_count")]
        public long FeedbacksUnconfirmedCount { get; set; }

        [JsonProperty("trade_volume_text")]
        public string TradeVolumeText { get; set; }

        [JsonProperty("has_common_trades")]
        public bool HasCommonTrades { get; set; }

        [JsonProperty("confirmed_trade_count_text")]
        public string ConfirmedTradeCountText { get; set; }

        [JsonProperty("blocked_count")]
        public long BlockedCount { get; set; }

        [JsonProperty("feedback_score")]
        public long FeedbackScore { get; set; }

        [JsonProperty("feedback_count")]
        public long FeedbackCount { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("trusted_count")]
        public long TrustedCount { get; set; }

        [JsonProperty("identity_verified_at")]
        public DateTime? IdentityVerifiedAt { get; set; }

        [JsonProperty("real_name_verifications_trusted")]
        public long RealNameVerificationsTrusted { get; set; }

        [JsonProperty("real_name_verifications_untrusted")]
        public long RealNameVerificationsUntrusted { get; set; }

        [JsonProperty("real_name_verifications_rejected")]
        public long RealNameVerificationsRejected { get; set; }
    }
}
