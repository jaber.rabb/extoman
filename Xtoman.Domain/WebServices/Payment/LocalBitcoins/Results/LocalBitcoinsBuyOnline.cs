﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Payment.LocalBitcoins
{
    public class LocalBitcoinsBuySellOnline
    {
        [JsonProperty("pagination")]
        public LocalBitcoinsPagination Pagination { get; set; }

        [JsonProperty("data")]
        public LocalBitcoinsBuySellOnlineData Data { get; set; }
    }

    public class LocalBitcoinsBuySellOnlineData
    {
        [JsonProperty("ad_list")]
        public List<LocalBitcoinsAdList> AdList { get; set; }

        [JsonProperty("ad_count")]
        public long AdCount { get; set; }
    }

    public class LocalBitcoinsAdList
    {
        [JsonProperty("data")]
        public LocalBitcoinsAdListData Data { get; set; }

        [JsonProperty("actions")]
        public LocalBitcoinsBuySellOnlineActions Actions { get; set; }
    }

    public class LocalBitcoinsAdListData
    {
        [JsonProperty("profile")]
        public LocalBitcoinsBuySellOnlineProfile Profile { get; set; }

        [JsonProperty("require_feedback_score")]
        public long RequireFeedbackScore { get; set; }

        [JsonProperty("hidden_by_opening_hours")]
        public bool HiddenByOpeningHours { get; set; }

        [JsonProperty("trade_type")]
        public string TradeType { get; set; }

        [JsonProperty("ad_id")]
        public long AdId { get; set; }

        [JsonProperty("temp_price")]
        public decimal? TempPrice { get; set; }

        [JsonProperty("bank_name")]
        public string BankName { get; set; }

        [JsonProperty("payment_window_minutes")]
        public long PaymentWindowMinutes { get; set; }

        [JsonProperty("trusted_required")]
        public bool TrustedRequired { get; set; }

        [JsonProperty("min_amount")]
        public decimal? MinAmount { get; set; }

        [JsonProperty("visible")]
        public bool Visible { get; set; }

        [JsonProperty("require_trusted_by_advertiser")]
        public bool RequireTrustedByAdvertiser { get; set; }

        [JsonProperty("temp_price_usd")]
        public decimal TempPriceUsd { get; set; }

        [JsonProperty("lat")]
        public decimal? Lat { get; set; }

        [JsonProperty("age_days_coefficient_limit")]
        public decimal AgeDaysCoefficientLimit { get; set; }

        [JsonProperty("is_local_office")]
        public bool IsLocalOffice { get; set; }

        [JsonProperty("first_time_limit_btc")]
        public decimal? FirstTimeLimitBtc { get; set; }

        [JsonProperty("atm_model")]
        public string AtmModel { get; set; }

        [JsonProperty("track_max_amount")]
        public bool? TrackMaxAmount { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("location_string")]
        public string LocationString { get; set; }

        [JsonProperty("countrycode")]
        public string Countrycode { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("limit_to_fiat_amounts")]
        public string LimitToFiatAmounts { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("max_amount")]
        public decimal? MaxAmount { get; set; }

        [JsonProperty("lon")]
        public decimal? Lon { get; set; }

        [JsonProperty("sms_verification_required")]
        public bool SmsVerificationRequired { get; set; }

        [JsonProperty("require_trade_volume")]
        public decimal RequireTradeVolume { get; set; }

        [JsonProperty("online_provider")]
        public string OnlineProvider { get; set; }

        [JsonProperty("max_amount_available")]
        public decimal MaxAmountAvailable { get; set; }

        [JsonProperty("msg")]
        public string Message { get; set; }

        [JsonProperty("require_identification")]
        public bool RequireIdentification { get; set; }

        [JsonProperty("email")]
        public object Email { get; set; }

        [JsonProperty("volume_coefficient_btc")]
        public decimal VolumeCoefficientBtc { get; set; }
    }

    public class LocalBitcoinsBuySellOnlineProfile
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("feedback_score")]
        public long FeedbackScore { get; set; }

        [JsonProperty("trade_count")]
        public string TradeCount { get; set; }

        [JsonProperty("last_online")]
        public DateTime LastOnline { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class LocalBitcoinsBuySellOnlineActions
    {
        [JsonProperty("public_view")]
        public string PublicView { get; set; }
    }
}
