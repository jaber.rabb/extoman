﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.LocalBitcoins
{
    public class LocalBitcoinsResult
    {
        [JsonProperty("error")]
        public LocalBitcoinsError Error { get; set; }
    }

    public class LocalBitcoinsError
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("error_code")]
        public int ErrorCode { get; set; }

        [JsonProperty("error_lists")]
        public object ErrorLists { get; set; }
    }
}
