﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.LocalBitcoins
{
    public class LocalBitcoinsWalletSend : LocalBitcoinsResult
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("data")]
        public LocalBitcoinsWalletSendData Data { get; set; }
    }

    public class LocalBitcoinsWalletSendData
    {
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
