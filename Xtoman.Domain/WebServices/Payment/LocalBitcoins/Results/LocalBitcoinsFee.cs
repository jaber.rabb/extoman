﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Payment.LocalBitcoins
{
    public class LocalBitcoinsFee : LocalBitcoinsResult
    {
        [JsonProperty("data")]
        public LocalBitcoinsFeeData Data { get; set; }
    }

    public class LocalBitcoinsFeeData
    {
        [JsonProperty("deposit_fee")]
        public decimal DepositFee { get; set; }

        [JsonProperty("outgoing_fee")]
        public decimal OutgoingFee { get; set; }
    }
}
