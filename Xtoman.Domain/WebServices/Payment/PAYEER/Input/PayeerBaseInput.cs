﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PayeerBaseInput
    {
        /// <summary>
        /// action type, examples: transfer, balance, checkUser, getExchangeRate, initOutput, output, getPaySystems, historyInfo, shopOrderInfo, history, merchant
        /// </summary>
        public string action { get; set; }
    }
}
