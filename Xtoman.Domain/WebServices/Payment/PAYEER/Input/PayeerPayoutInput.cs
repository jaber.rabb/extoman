﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PayeerPayoutInput
    {
        /// <summary>
        /// ID of selected payment system, example: 1136053
        /// </summary>
        public string ps { get; set; }
        /// <summary>
        /// amount withdrawn (the amount deposited will be calculated automatically, factoring in all fees from the recipient), example: 1.00
        /// </summary>
        public string sumIn { get; set; }
        /// <summary>
        /// currency with which the withdrawal will be performed, example: USD, EUR, RUB
        /// </summary>
        public string curIn { get; set; }
        /// <summary>
        /// (Optional) amount deposited (the amount withdrawn will be calculated automatically, factoring in all fees from the sender), example: 1.00
        /// </summary>
        public string sumOut { get; set; }
        /// <summary>
        /// deposit currency (if the withdrawal currency is different from the deposit currency, the conversion will be performed automatically based on the Payeer system’s exchange rate when the transfer takes place), example: USD, EUR, RUB
        /// </summary>
        public string curOut { get; set; }
        /// <summary>
        /// recipient's account number in the selected payment system (ps), example: P1000441
        /// </summary>
        public string param_ACCOUNT_NUMBER { get; set; }
    }
}
