﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PayeerAutomaticExchangeRateInput
    {
        /// <summary>
        /// select currencies for conversion rates, example: N or Y
        /// </summary>
        public string output { get; set; }
    }
}
