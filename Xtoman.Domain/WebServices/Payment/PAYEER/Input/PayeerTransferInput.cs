﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PayeerTransferInput
    {
        /// <summary>
        /// currency with which the withdrawal will be performed, example: USD, EUR, RUB
        /// </summary>
        public string curIn { get; set; }
        /// <summary>
        /// amount withdrawn (the amount deposited will be calculated automatically, factoring in all fees from the recipient), example: 1.00
        /// </summary>
        public decimal sum { get; set; }
        /// <summary>
        /// deposit currency (if the withdrawal currency is different from the deposit currency, the conversion will be performed automatically based on the Payeer system’s exchange rate when the transfer takes place), example: USD, EUR, RUB
        /// </summary>
        public string curOut { get; set; }
        /// <summary>
        /// (Optional) amount deposited (the amount withdrawn will be calculated automatically, factoring in all fees from the sender), example: 1.00
        /// </summary>
        public string sumOut { get; set; }
        /// <summary>
        /// user’s Payeer account number or email address (if the email address was not registered before the transfer, the registration will be performed automatically), example: P1000000 or test@mail.com
        /// </summary>
        public string to { get; set; }
        /// <summary>
        /// (Optional) comments on the transfer (this should preferably show the transaction ID in your accounting system), example: Transfer #1365
        /// </summary>
        public string comment { get; set; }
        /// <summary>
        /// (Optional) if the value "Y" is set for this parameter, the user will not be able to see the sender of the transfer (required for hiding the sender's wallet for security reasons), example: Y
        /// </summary>
        public string anonim { get; set; }
        /// <summary>
        /// (Optional) activation of transaction protection (to have the transfer deposited into the recipient’s account you will have to enter the protectCode protection code within protectCode days; otherwise the transfer will be canceled and the funds will remain in the sender's account), example: Y
        /// </summary>
        public string protect { get; set; }
        /// <summary>
        /// (Optional) protection period: 1–30 days, example: 1-30
        /// </summary>
        public int protectPeriod { get; set; }
        /// <summary>
        /// (Optional) protection code, example: 12345
        /// </summary>
        public int protectCode { get; set; }
    }
}
