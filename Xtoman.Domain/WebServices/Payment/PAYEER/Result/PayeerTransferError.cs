﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public enum PayeerTransferError
    {
        [Display(Name = "Transfer succeeded", Description = "Transfer succeeded")]
        noError = 0,
        [Display(Name = "Insufficient funds", Description = "Insufficient funds for transfer")]
        balanceError = 1,
        [Display(Name = "Insufficient funds (Limited)", Description = "insufficient funds for transfer given limitations on the account")]
        balanceError000 = 2,
        [Display(Name = "Cannot transfer funds to yourself", Description = "you cannot transfer funds to yourself (when the withdrawal currency and deposit currency are the same)")]
        transferHimselfForbidden = 3,
        [Display(Name = "Transfer error", Description = "transfer error, try again later")]
        transferError = 4,
        [Display(Name = "Currency exchange porhibited for now", Description = "automatic exchange of currency input for currency output is temporarily prohibited")]
        convertForbidden = 5,
        [Display(Name = "Entered transfer protection period is wrong", Description = "error in entered transfer protection period it most be a number from 1 to 30")]
        protectDay_1_30 = 6,
        [Display(Name = "Protection code not presented for the period", Description = "the protection code cannot be blank when protection is active")]
        protectCodeNotEmpty = 7,
        [Display(Name = "The send and receive amounts cannot be zero", Description = "the amounts sent and received cannot be zero")]
        sumNotNull = 8,
        [Display(Name = "The send amount cannot be negative", Description = "the amount sent cannot be negative")]
        sumInNotMinus = 9,
        [Display(Name = "The receive amount cannot be negative", Description = "the amount received cannot be negative")]
        sumOutNotMinus = 10,
        [Display(Name = "Sender not found", Description = "sender not found")]
        fromError = 11,
        [Display(Name = "Receiver is not correct", Description = "recipient entered incorrectly")]
        toError = 12,
        [Display(Name = "Withdrawal currency is not supported", Description = "withdrawal currency not supported")]
        curInError = 13,
        [Display(Name = "Reception currency is not supported", Description = "reception currency not supported")]
        curOutError = 14,
        [Display(Name = "Issues have arisen regarding your activity", Description = "Issues have arisen regarding your activity. Please contact Technical Support")]
        outputHold = 15,
        [Display(Name = "Transfer to customers in certain countries is prohibited", Description = "Transfer to customers in certain countries is prohibited")]
        transferToForbiddenCountry = 16
    }
}
