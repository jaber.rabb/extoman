﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PayeerAutomaticExchangeRateResult : PayeerBaseResult
    {
        public PayeerRateItem rate { get; set; }
    }

    public class PayeerRateItem
    {
        [JsonProperty("RUB/USD")]
        public string RUB_USD { get; set; }
        [JsonProperty("RUB/RUB")]
        public string RUB_RUB { get; set; }
        [JsonProperty("RUB/EUR")]
        public string RUB_EUR { get; set; }
        [JsonProperty("USD/USD")]
        public string USD_USD { get; set; }
        [JsonProperty("USD/RUB")]
        public string USD_RUB { get; set; }
        [JsonProperty("USD/EUR")]
        public string USD_EUR { get; set; }
        [JsonProperty("EUR/USD")]
        public string EUR_USD { get; set; }
        [JsonProperty("EUR/RUB")]
        public string EUR_RUB { get; set; }
    }
}
