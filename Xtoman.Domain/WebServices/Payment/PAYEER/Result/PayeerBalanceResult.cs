﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PayeerBalanceResult : PayeerBaseResult
    {
        public PayeerBalanceItemModel balance { get; set; }
    }

    public class PayeerBalanceItemModel
    {
        public PayeerCurrencyBalanceItemModel BTC { get; set; }
        public PayeerCurrencyBalanceItemModel EUR { get; set; }
        public PayeerCurrencyBalanceItemModel RUB { get; set; }
        public PayeerCurrencyBalanceItemModel USD { get; set; }
    }

    public class PayeerCurrencyBalanceItemModel
    {
        /// <summary>
        /// balance
        /// </summary>
        public decimal BUDGET { get; set; }
        //public string BUDGET { get; set; }
        /// <summary>
        /// balance minus transactions in progress
        /// </summary>
        public string DOSTUPNO { get; set; }
        /// <summary>
        /// Balance minus transactions in progress, minus funds that could be blocked by the system (to be used as the main balance for the wallet; for example, when checking the balance before a withdrawal)
        /// </summary>
        public string DOSTUPNO_SYST { get; set; }
    }
}
