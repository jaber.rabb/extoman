﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PayeerAvailablePaymentSystemsResult : PayeerBaseResult
    {
        public List<object> list { get; set; }
    }

    public class PayeerAvailablePaymentSystemsGateCommission
    {
        public string USD { get; set; }
        public string RUB { get; set; }
        public string EUR { get; set; }
    }
}
