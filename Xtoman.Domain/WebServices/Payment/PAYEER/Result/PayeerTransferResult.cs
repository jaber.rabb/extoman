﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PayeerTransferResult : PayeerBaseResult
    {
        /// <summary>
        /// Transaction number historyId > 0
        /// </summary>
        public string historyId { get; set; }
    }
}
