﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PayeerAccountExistResult : PayeerBaseResult
    {
        /// <summary>
        /// user’s account number in the format P1000000
        /// </summary>
        public string user { get; set; }
    }
}
