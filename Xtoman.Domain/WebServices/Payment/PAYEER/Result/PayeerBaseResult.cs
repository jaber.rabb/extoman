﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PayeerBaseResult
    {
        /// <summary>
        /// Errors
        /// </summary>
        public List<string> errors { get; set; }
        /// <summary>
        /// 0 => Authorization successful, 1 => Authorization error
        /// </summary>
        public string auth_error { get; set; }
    }
}
