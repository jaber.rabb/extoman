﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public enum PayeerPayoutError
    {
        [Display(Name = "Payout succeeded", Description = "Payout succeeded")]
        noError = 0,
        [Display(Name = "Withdrawal currency not supported", Description = "Withdrawal currency not supported")]
        curIn_invalid = 1,
        [Display(Name = "Deposit currency not supported", Description = "Deposit currency not supported")]
        curOut_invalid = 2,
        [Display(Name = "Automatic exchange is temporarily prohibited", Description = "Automatic exchange of currency in for currency out is temporarily prohibited")]
        This_type_of_exchange_is_not_possible = 3,
        [Display(Name = "Invalid parameter format", Description = "invalid parameter format")]
        invalid_format = 4,
        [Display(Name = "Payout method is disabled or does not exist", Description = "Payout method is disabled or does not exist")]
        pay_sys_no_isset = 5,
        [Display(Name = "Non-existent deposit currency", Description = "Non-existent deposit currency")]
        cur_no_pay_sys = 6,
        [Display(Name = "Amount received is below the minimum", Description = "Amount received is below the minimum")]
        sum_less_min = 7,
        [Display(Name = "Amount received is above the maximum", Description = "Amount received is above the maximum")]
        sum_more_max = 8,
        [Display(Name = "Issues have arisen regarding your activity.", Description = "Issues have arisen regarding your activity. Please contact Technical Support.")]
        output_block = 9,
        [Display(Name = "Insufficient funds for withdrawal", Description = "Insufficient funds for withdrawal")]
        balans_no = 10,
        [Display(Name = "Insufficient funds for withdrawal given limitations on the account", Description = "Insufficient funds for withdrawal given limitations on the account")]
        balans_no_hold = 11,
        [Display(Name = "No reserve for the creation of a payout", Description = "No reserve for the creation of a payout")]
        balance_sys_no = 12
    }
}
