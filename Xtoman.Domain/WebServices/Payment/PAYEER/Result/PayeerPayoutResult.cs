﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PayeerPayoutResult : PayeerBaseResult
    {
        public OutputParams outputParams { get; set; }
        public int? historyId { get; set; }
    }

    public class OutputParams
    {
        public string sumIn { get; set; }
        public string curIn { get; set; }
        public string curOut { get; set; }
        public string ps { get; set; }
        public string sumOut { get; set; }
    }
}
