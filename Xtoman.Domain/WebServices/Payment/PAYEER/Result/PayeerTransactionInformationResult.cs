﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices
{
    public class PayeerTransactionInformationResult : PayeerBaseResult
    {
        public PayeerTransactionInfoItem info { get; set; }
    }

    public class PayeerTransactionInfoItem
    {
        /// <summary>
        /// Transaction ID (HistoryId), example: 197941397
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// Date/time of creation in the format DD.MM.YYYY HH:MM:SS, example: 14.01.2015 14:00:00
        /// </summary>
        public string dateCreate { get; set; }
        /// <summary>
        /// transaction type, transfer => transfer, input => deposit, output => withdrawal
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// Transaction status : 
        /// execute => complete (final status),
        /// process => in progress (changes to "execute", "cancel", or "hold"),
        /// cancel => canceled (final status),
        /// wait => expected (for example, when waiting for a payment) (changes to "execute", "cancel", or "hold"),
        /// hold => on hold (changes to "execute" or "cancel"),
        /// black_list => the transaction has been stopped because it was on the blacklist filter (can change to "execute", "cancel", or "hold")
        /// </summary>
        public string status { get; set; }
        /// <summary>
        /// sender's account number, example: P1000000, null
        /// </summary>
        public string from { get; set; }
        /// <summary>
        /// Amount withdrawn
        /// </summary>
        public string sumIn { get; set; }
        /// <summary>
        /// Withdrawal currency, example: USD, EUR, RUB
        /// </summary>
        public string curIn { get; set; }
        /// <summary>
        /// Recipient's account number, example: P1000001, null
        /// </summary>
        public string to { get; set; }
        /// <summary>
        /// Amount received, example: 1
        /// </summary>
        public string sumOut { get; set; }
        /// <summary>
        /// Deposit currency, example: USD, EUR, RUB
        /// </summary>
        public string curOut { get; set; }
        /// <summary>
        /// Payeer’s fee, example: 0.01
        /// </summary>
        public string comSite { get; set; }
        /// <summary>
        /// Gateway fee, example: 0.01
        /// </summary>
        public object comGate { get; set; }
        /// <summary>
        /// Exchange rate for auto-conversion, example: 0
        /// </summary>
        public string exchangeCourse { get; set; }
        /// <summary>
        /// Transaction protection for transfer, example: Y, N
        /// </summary>
        public string protect { get; set; }
        /// <summary>
        /// protection code, example: 12345
        /// </summary>
        public object protectCode { get; set; }
        /// <summary>
        /// Number of days for protection, example: 7
        /// </summary>
        public object protectDay { get; set; }
        /// <summary>
        /// comments, example: test
        /// </summary>
        public object comment { get; set; }
        /// <summary>
        /// payment system ID, example: 1265
        /// </summary>
        public object psId { get; set; }
        /// <summary>
        /// Is api, example: Y, N
        /// </summary>
        public string isApi { get; set; }
        /// <summary>
        /// Error code
        /// </summary>
        public string error { get; set; }
        /// <summary>
        /// Exchange marker, example: Y, N
        /// </summary>
        public string isExchange { get; set; }
        public List<object> arExchange { get; set; }
        [JsonProperty("params")]
        public List<object> parameters { get; set; }
    }
}
