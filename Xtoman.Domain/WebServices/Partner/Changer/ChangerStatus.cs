﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.Partner.Changer
{
    public enum ChangerStatus
    {
        /// <summary>
        /// We haven't received a payment yet.
        /// </summary>
        [Display(Name = "پرداخت نشده")]
        New,
        /// <summary>
        /// Your exchange is in our processing queue.
        /// </summary>

        [Display(Name = "در حال پردازش")]
        Processing,
        /// <summary>
        /// Your exchange has been succesfully processed.
        /// </summary>

        [Display(Name = "تکمیل شده")]
        Processed,
        /// <summary>
        ///  Your exchange has been denied and refunded to your payer account/address.
        /// </summary>

        [Display(Name = "لغو شده")]
        Denied,

        [Display(Name = "نامشخص")]
        Unknown
    }
}
