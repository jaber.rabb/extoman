﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Partner.Changer
{
    public enum ChangerECurrency
    {
        [Display(Name = "پرفکت مانی", ShortName = "PMUSD", Prompt = "pm_USD")]
        PerfectMoney,
        [Display(Name = "ووچر پرفکت مانی", ShortName = "PMUSDVoucher", Prompt = "pmvoucher_USD")]
        PerfectMoneyVoucher,
        [Display(Name = "پاییر", ShortName = "PAYEERUSD", Prompt = "payeer_USD")]
        PAYEER,
        [Display(Name = "ادوکش", ShortName = "AdVUSD", Prompt = "advcash_USD")]
        AdvCash,
        [Display(Name = "بیت کوین", ShortName = "BTC", Prompt = "bitcoin_BTC")]
        Bitcoin,
        [Display(Name = "اتریوم", ShortName = "ETH", Prompt = "ethereum_ETH")]
        Ethereum,
        [Display(Name = "بیت کوین کش", ShortName = "BCH", Prompt = "bitcoincash_BCH")]
        BitcoinCash,
        [Display(Name = "داج کوین", ShortName = "DOGE", Prompt = "dogecoin_DOGE")]
        Dogecoin,
        [Display(Name = "دش", ShortName = "DASH", Prompt = "dash_DASH")]
        Dash,
        [Display(Name = "زد کش", ShortName = "ZEC", Prompt = "zcash_ZEC")]
        Zcash,
        [Display(Name = "لایت کوین", ShortName = "LTC", Prompt = "litecoin_LTC")]
        Litecoin,
        [Display(Name = "اتریوم کلاسیک", ShortName = "ETC", Prompt = "ethereumclassic_ETC")]
        EthereumClassic,
        [Display(Name = "آوگور", ShortName = "REP", Prompt = "augur_REP")]
        Augur,
        [Display(Name = "گولم", ShortName = "GNT", Prompt = "golem_GNT")]
        Golem,
        [Display(Name = "لیسک", ShortName = "LSK", Prompt = "lisk_LSK")]
        Lisk
    }

    public static class ChangerECurrencyHelper
    {
        public static ECurrencyType? ToECurrencyType(this ChangerECurrency changerECurrency)
        {
            var sign = changerECurrency.ToDisplay(DisplayProperty.ShortName);
            return ECurrencyTypeHelper.ToECurrencyAccountType(sign);
        }

        public static string ToChangerString(this ChangerECurrency changerECurrency)
        {
            return changerECurrency.ToDisplay(DisplayProperty.Prompt);
        }

        public static ECurrencyType? ToECurrencyTypeFromChanger(this string changerECurrencyString)
        {
            ChangerECurrency? changerECurrencyType = null;
            foreach (ChangerECurrency item in Enum.GetValues(typeof(ChangerECurrency)))
            {
                if (item.ToDisplay(DisplayProperty.Prompt) == changerECurrencyString)
                    changerECurrencyType = item;
            }
            if (changerECurrencyType.HasValue)
                return ToECurrencyType(changerECurrencyType.Value);

            return null;
        }

        public static ChangerECurrency? ToChangerECurrency(this ECurrencyType eCurrencyType)
        {
            switch (eCurrencyType)
            {
                case ECurrencyType.PAYEER:
                    return ChangerECurrency.PAYEER;
                case ECurrencyType.PerfectMoney:
                    return ChangerECurrency.PerfectMoney;
                case ECurrencyType.PerfectMoneyVoucher:
                    return ChangerECurrency.PerfectMoneyVoucher;
                case ECurrencyType.Bitcoin:
                    return ChangerECurrency.Bitcoin;
                case ECurrencyType.Ethereum:
                    return ChangerECurrency.Ethereum;
                case ECurrencyType.EthereumClassic:
                    return ChangerECurrency.EthereumClassic;
                case ECurrencyType.Litecoin:
                    return ChangerECurrency.Litecoin;
                case ECurrencyType.BitcoinCash:
                    return ChangerECurrency.BitcoinCash;
                case ECurrencyType.Dash:
                    return ChangerECurrency.Dash;
                case ECurrencyType.Zcash:
                    return ChangerECurrency.Zcash;
                case ECurrencyType.Dogecoin:
                    return ChangerECurrency.Dogecoin;
                case ECurrencyType.Lisk:
                    return ChangerECurrency.Lisk;
                default:
                    return null;
            }
        }
    }
}
