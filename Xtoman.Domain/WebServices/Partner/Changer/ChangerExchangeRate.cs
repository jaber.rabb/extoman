﻿using Newtonsoft.Json;
using Xtoman.Domain.Models;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Partner.Changer
{
    public class ChangerPair
    {
        /// <summary>
        /// The e-currency/cryptocurrency you want to send.
        /// </summary>
        [JsonProperty("send")]
        public string Send { get; set; }
        /// <summary>
        /// The e-currency/cryptocurrency you want to receive.
        /// </summary>
        [JsonProperty("receive")]
        public string Receive { get; set; }

        public ECurrencyType? SendECurrency => Send.ToECurrencyTypeFromChanger();
        public ECurrencyType? ReceiveECurrency => Receive.ToECurrencyTypeFromChanger();
    }

    public class ChangerLimits
    {
        /// <summary>
        /// Minimum amount of send you can exchange. Lower amounts will not be processed and will be refunded to the payer address/account.
        /// </summary>
        [JsonProperty("min_amount")]
        public decimal MinimumAmount { get; set; }
        /// <summary>
        /// Maximum amount of send you can exchange. Higher amounts will not be processed and will be refunded to the payer address/account.
        /// </summary>
        [JsonProperty("max_amount")]
        public decimal MaximumAmount { get; set; }
    }

    public class ChangerExchangeRate
    {
        [JsonProperty("pair")]
        public ChangerPair Pair { get; set; }
        /// <summary>
        /// Will return the exchange rate for the selected pair with your account's discount level applied, if you are using the authenticated mode.
        /// </summary>
        [JsonProperty("rate")]
        public decimal Rate { get; set; }
        /// <summary>
        /// Optional: If you specified the amount argument, this returns the amount you will receive or have to send.
        /// </summary>
        [JsonProperty("receive_amount")]
        public decimal Receiveamount { get; set; }
    }

    public class ChangerExchangeLimit
    {
        [JsonProperty("pair")]
        public ChangerPair Pair { get; set; }
        [JsonProperty("limits")]
        public ChangerLimits Limits { get; set; }
    }

    public class ChangerMakeExchange : ChangerExchangeLimit
    {
        /// <summary>
        /// UUID of your exchange request, required later for payment confirmation and status checking.
        /// </summary>
        [JsonProperty("exchange_id")]
        public string ExchangeId { get; set; }
        /// <summary>
        /// The amount you should send in order to receive the amount shown in the returned ReceiveAmount.
        /// </summary>
        [JsonProperty("send_amount")]
        public decimal SendAmount { get; set; }

        /// <summary>
        /// Will return the exchange rate with your account's discount level applied, if you are using the authenticated mode. Locked for 10 minutes, as explained in the expiration.
        /// </summary>
        [JsonProperty("rate")]
        public decimal Rate { get; set; }

        /// <summary>
        /// The amount you will receive if the amount you send is exactly what is returned in the SendAmount.
        /// </summary>
        [JsonProperty("receive_amount")]
        public decimal ReceiveAmount { get; set; }

        /// <summary>
        /// The account/address the exchange will be processed to. Make sure it belongs to you: transactions are irreversible.
        /// </summary>
        [JsonProperty("receiver_id")]
        public string ReceiverAddress { get; set; }

        /// <summary>
        /// The account/address the payment must be sent to.
        /// </summary>
        [JsonProperty("payee")]
        public string PayToAddress { get; set; }

        /// <summary>
        /// Returned for Monero exchanges: will output the Payment ID that must be used when sending the payment.
        /// </summary>
        [JsonProperty("payment_id")]
        public string PayToTagId { get; set; }

        /// <summary>
        /// When this value is true, you will need to For digital e-currencies like OKPay and Perfect Money, we will need your payment Batch no. (i.e. Transaction ID) in order to recognize your payment associated with the exchange. You need to use MakeExchangeBatchNumberAsync method <seealso cref=""/>.
        /// </summary>
        [JsonProperty("batch_required")]
        public bool IsBatchRequired { get; set; }

        /// <summary>
        /// UNIX timestamp of your exchange order's expiration. If a payment is received after the expiration, it will be processed at the current exchange rate.
        /// </summary>
        [JsonProperty("expiration")]
        public int Expiration { get; set; }
    }

    public class ChangerMakeExchangeBatch
    {
        /// <summary>
        /// Returns true if successful or false otherwise.
        /// </summary>
        [JsonProperty("success")]
        public bool Success { get; set; }

        /// <summary>
        /// UUID of your exchange.
        /// </summary>
        [JsonProperty("exchange_id")]
        public string ExchangeId { get; set; }

        /// <summary>
        /// The amount you've sent.
        /// </summary>
        [JsonProperty("send_amount")]
        public decimal SendAmount { get; set; }

        /// <summary>
        /// The exchange rate applied to your exchange.
        /// </summary>
        [JsonProperty("rate")]
        public decimal Rate { get; set; }

        /// <summary>
        /// The exact amount that will be sent to your receiver_id account/address.
        /// </summary>
        [JsonProperty("receive_amount")]
        public decimal ReceiveAmount { get; set; }

        /// <summary>
        /// The account/address whom the exchange will be sent to.
        /// </summary>
        [JsonProperty("receiver_id")]
        public string ReceiverAddress { get; set; }
    }

    public class ChangerExchangeStatus : ChangerMakeExchangeBatch
    {
        [JsonProperty("pair")]
        public ChangerPair Pair { get; set; }

        /// <summary>
        /// Your exchange status, it can be any of these: 
        /// <para>new: We haven't received a payment yet.</para>
        /// <para>processing: Your exchange is in our processing queue.</para>
        /// <para>processed: Your exchange has been succesfully processed.</para>
        /// <para>denied: Your exchange has been denied and refunded to your payer account/address.</para>
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }

        public ChangerStatus StatusEnum => Status.ToEnum(ChangerStatus.Unknown);

        /// <summary>
        /// UNIX timestamp representation of the moment we've received your exchange request.
        /// </summary>
        [JsonProperty("exchange_time")]
        public int ExchangeTime { get; set; }

        /// <summary>
        /// null if your exchange hasn't been processed yet.
        /// Integer: UNIX timestamp of the moment your exchange has been processed
        /// </summary>
        [JsonProperty("processed_time")]
        public int? ProcessedTime { get; set; }

        /// <summary>
        /// null if your exchange hasn't been processed yet.
        /// String: Transaction hash or batch no.of the outgoing payment sent to your ReceiverAddress.
               /// </summary>
        [JsonProperty("batch_out")]
        public string BatchOut { get; set; }
    }
}
