﻿using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Partner.Changelly
{
    public class GetStatusResult
    {
#pragma warning disable IDE1006 // Naming Styles
        public string jsonrpc { get; set; }
        public string id { get; set; }
        public string result { get; set; }
#pragma warning restore IDE1006 // Naming Styles
        public ChangellyStatus EnumResult => result.ToEnum<ChangellyStatus>(0);
    }
}
