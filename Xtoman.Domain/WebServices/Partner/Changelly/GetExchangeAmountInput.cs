﻿namespace Xtoman.Domain.WebServices.Partner.Changelly
{
    public class GetExchangeAmountInput
    {
        public string From { get; set; }
        public string To { get; set; }
        public decimal Amount { get; set; }
    }
}
