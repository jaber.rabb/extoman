﻿using System;

namespace Xtoman.Domain.WebServices.Partner.Changelly
{
    public class CreateTransactionResponseResult
    {
#pragma warning disable IDE1006 // Naming Styles
        public string jsonrpc { get; set; }
        public string id { get; set; }
        public CreateTransactionResult result { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }

    public class CreateTransactionResult
    {
#pragma warning disable IDE1006 // Naming Styles
        public string id { get; set; }
        public decimal apiExtraFee { get; set; }
        public decimal changellyFee { get; set; }
        public string payinExtraId { get; set; }
        public string status { get; set; }
        public string currencyFrom { get; set; }
        public string currencyTo { get; set; }
        public decimal amountTo { get; set; }
        public string payinAddress { get; set; }
        public string payoutAddress { get; set; }
        public DateTime createdAt { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }
}