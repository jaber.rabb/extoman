﻿namespace Xtoman.Domain.WebServices.Partner.Changelly
{
    public class GetDecimalAmountResult
    {
#pragma warning disable IDE1006 // Naming Styles
        public string jsonrpc { get; set; }
        public string id { get; set; }
        public decimal result { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }
}
