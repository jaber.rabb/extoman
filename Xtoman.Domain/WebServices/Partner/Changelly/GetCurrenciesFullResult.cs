﻿using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Partner.Changelly
{
    public class GetCurrenciesFullResult
    {
#pragma warning disable IDE1006 // Naming Styles
        public string jsonrpc { get; set; }
        public string id { get; set; }
        public List<CurrencyFullItem> result { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }

    public class CurrencyFullItem
    {
#pragma warning disable IDE1006 // Naming Styles
        public string name { get; set; }
        public string fullName { get; set; }
        public bool enabled { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }
}
