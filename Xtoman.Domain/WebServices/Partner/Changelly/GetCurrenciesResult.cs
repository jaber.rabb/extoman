﻿using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Partner.Changelly
{
    public class GetCurrenciesResult
    {
#pragma warning disable IDE1006 // Naming Styles
        public string jsonrpc { get; set; }
        public string id { get; set; }
        public List<string> result { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }
}
