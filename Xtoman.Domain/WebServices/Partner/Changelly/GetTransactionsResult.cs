﻿using System.Collections.Generic;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Partner.Changelly
{
    public class TransactionResult
    {
#pragma warning disable IDE1006 // Naming Styles
        public string id { get; set; }
        public long createdAt { get; set; }
        public long payinConfirmations { get; set; }
        public string status { get; set; }
        public string currencyFrom { get; set; }
        public string currencyTo { get; set; }
        public string payinAddress { get; set; }
        public string payinExtraId { get; set; }
        public string payinHash { get; set; }
        public string payoutAddress { get; set; }
        public string payoutExtraId { get; set; }
        public string payoutHash { get; set; }
        public string amountFrom { get; set; }
        public string amountTo { get; set; }
        public decimal networkFee { get; set; }
        public decimal changellyFee { get; set; }
        public decimal apiExtraFee { get; set; }
        public ChangellyStatus EnumStatus => status.ToEnum<ChangellyStatus>(0);
#pragma warning restore IDE1006 // Naming Styles
    }

    public class GetTransactionResult
    {
#pragma warning disable IDE1006 // Naming Styles
        public string jsonrpc { get; set; }
        public string id { get; set; }
        public List<TransactionResult> result { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }
}
