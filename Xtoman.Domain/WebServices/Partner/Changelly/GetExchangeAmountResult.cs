﻿using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Partner.Changelly
{
    public class ExchangeAmount
    {
#pragma warning disable IDE1006 // Naming Styles
        public string from { get; set; }
        public string to { get; set; }
        public decimal amount { get; set; }
        public decimal result { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }

    public class GetExchangeAmountResult
    {
#pragma warning disable IDE1006 // Naming Styles
        public string jsonrpc { get; set; }
        public string id { get; set; }
        public List<ExchangeAmount> result { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }
}
