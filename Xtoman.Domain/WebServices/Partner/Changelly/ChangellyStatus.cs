﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.Partner.Changelly
{
    public enum ChangellyStatus
    {
        [Display(Name = "در انتظار پرداخت", Description = "Waiting for user payment")]
        waiting,
        [Display(Name = "در انتظار تایید", Description = "Your transaction is in a mempool and waits to be confirmed.")]
        confirming,
        [Display(Name = "در حال تبدیل", Description = "Your payment is received and being exchanged via our partner.")]
        exchanging,
        [Display(Name = "در حال ارسال", Description = "Money is sending to the recipient address.")]
        sending,
        [Display(Name = "اتمام", Description = "Money is successfully sent to the recipient address.")]
        finished,
        [Display(Name = "ناموفق", Description = "Transaction has failed. In most cases, the amount was less than the minimum.")]
        failed,
        [Display(Name = "باز پرداخت شده", Description = "Exchange was failed and coins were refunded to user's wallet. The wallet address should be provided by user.")]
        refunded
    }
}
