﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices.Partner.Changelly
{
    public class GetTransactionsInput
    {
        public GetTransactionsInput()
        {
            Limit = 10;
            Offset = 10;
        }
        /// <summary>
        /// Optional
        /// </summary>
        public string Currency { get; set; }
        /// <summary>
        /// Optional
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Optional
        /// </summary>
        public string ExtraId { get; set; }
        public int Limit { get; set; }
        public int Offset { get; set; }
    }
}
