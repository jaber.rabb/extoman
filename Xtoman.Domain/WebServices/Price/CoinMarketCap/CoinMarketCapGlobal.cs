﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.Price.CoinMarketCap
{
    public class CoinMarketCapGlobal
    {
        [JsonProperty(PropertyName = "total_market_cap_usd")]
        public long Total_market_cap_usd { get; set; }
        [JsonProperty(PropertyName = "total_24h_volume_usd")]
        public long Total_24h_volume_usd { get; set; }
        [JsonProperty(PropertyName = "bitcoin_percentage_of_market_cap")]
        public double Bitcoin_percentage_of_market_cap { get; set; }
        [JsonProperty(PropertyName = "active_currencies")]
        public int Active_currencies { get; set; }
        [JsonProperty(PropertyName = "active_assets")]
        public int Active_assets { get; set; }
        [JsonProperty(PropertyName = "active_markets")]
        public int Active_markets { get; set; }
        [JsonProperty(PropertyName = "last_updated")]
        public long Last_updated { get; set; }

        [JsonIgnore]
        public DateTime LastUpdateDateTime => new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(Last_updated);
    }
}
