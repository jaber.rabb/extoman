﻿using Newtonsoft.Json;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Price.Nerkh
{
    public partial class NerkhPrice
    {
        [JsonProperty("min")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Min { get; set; }

        [JsonProperty("max")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Max { get; set; }

        [JsonProperty("current")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Current { get; set; }

        public long MinToman => Min / 10;
        public long MaxToman => Max / 10;
        public long CurrentToman => Current / 10;
    }
}
