﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Price.Nerkh
{
    public class NerkhCurrency
    {
        [JsonProperty("data")]
        public NerkhCurrencyData Data { get; set; }
    }

    public class NerkhCurrencyData
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("prices")]
        public Dictionary<string, NerkhPrice> Prices { get; set; }
    }
}
