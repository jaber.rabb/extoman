﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Price.Cryptonator
{
    public class CryptonatorTicker : CryptonatorBase
    {
        [JsonProperty("ticker")]
        public CryptonatorTickerObject Ticker { get; set; }
    }

    public class CryptonatorTickerObject
    {
        [JsonProperty("base")]
        public string Base { get; set; }

        [JsonProperty("target")]
        public string Target { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("volume")]
        public decimal Volume { get; set; }

        [JsonProperty("change")]
        public decimal Change { get; set; }
    }
}
