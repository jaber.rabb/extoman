﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Price.Cryptonator
{
    public class CryptonatorFullTicker : CryptonatorBase
    {
        [JsonProperty("ticker")]
        public CryptonatorFullTickerObject Ticker { get; set; }
    }

    public class CryptonatorFullTickerObject : CryptonatorTickerObject
    {
        [JsonProperty("markets")]
        public List<CryptonatorMarket> Markets { get; set; }
    }

    public class CryptonatorMarket
    {
        [JsonProperty("market")]
        public string MarketName { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("volume")]
        public decimal Volume { get; set; }
    }
}
