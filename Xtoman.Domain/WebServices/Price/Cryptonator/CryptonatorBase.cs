﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Price.Cryptonator
{
    public class CryptonatorBase
    {
        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }
    }
}
