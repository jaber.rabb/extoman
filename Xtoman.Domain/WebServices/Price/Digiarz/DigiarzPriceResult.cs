﻿using System;

namespace Xtoman.Domain.WebServices.Price.Digiarz
{
    public class DigiarzPriceResult
    {
        public DateTime time { get; set; }
        public DigiarzTypeResult BTC { get; set; }
        public DigiarzTypeResult ETH { get; set; }
        public DigiarzTypeResult ETC { get; set; }
        public DigiarzTypeResult XMR { get; set; }
        public DigiarzTypeResult XRP { get; set; }
        public DigiarzTypeResult ZEC { get; set; }
        public DigiarzTypeResult FCT { get; set; }
        public DigiarzTypeResult LTC { get; set; }
        public DigiarzTypeResult DOGE { get; set; }
        public DigiarzTypeResult DASH { get; set; }
    }

    public class DigiarzTypeResult
    {
        public string name { get; set; }
        public DigiarzRatesResult rates { get; set; }
    }

    public class DigiarzRatesResult
    {
        public DigiarzRateResult TMN { get; set; }
        public DigiarzRateResult USD { get; set; }
    }

    public class DigiarzRateResult
    {
        public string name { get; set; }
        public decimal rate { get; set; }
    }
}
