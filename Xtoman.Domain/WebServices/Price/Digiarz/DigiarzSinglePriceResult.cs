﻿using System;
using System.Globalization;

namespace Xtoman.Domain.WebServices.Price.Digiarz
{
    public class DigiarzBitcoinPriceResult
    {
        public string time { get; set; }
        public DateTime DateTime => DateTime.ParseExact(time, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        public DigiarzTypeResult BTC { get; set; }
    }

    public class DigiarzEthereumPriceResult
    {
        public string time { get; set; }
        public DateTime DateTime => DateTime.ParseExact(time, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        public DigiarzTypeResult ETH { get; set; }
    }

    public class DigiarzEthereumClassicPriceResult
    {
        public string time { get; set; }
        public DateTime DateTime => DateTime.ParseExact(time, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        public DigiarzTypeResult ETC { get; set; }
    }

    public class DigiarzMoneroPriceResult
    {
        public string time { get; set; }
        public DateTime DateTime => DateTime.ParseExact(time, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        public DigiarzTypeResult XMR { get; set; }
    }

    public class DigiarzRipplePriceResult
    {
        public string time { get; set; }
        public DateTime DateTime => DateTime.ParseExact(time, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        public DigiarzTypeResult XRP { get; set; }
    }

    public class DigiarzZCashPriceResult
    {
        public string time { get; set; }
        public DateTime DateTime => DateTime.ParseExact(time, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        public DigiarzTypeResult ZEC { get; set; }
    }

    public class DigiarzFactomPriceResult
    {
        public string time { get; set; }
        public DateTime DateTime => DateTime.ParseExact(time, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        public DigiarzTypeResult FCT { get; set; }
    }

    public class DigiarzLitecoinPriceResult
    {
        public string time { get; set; }
        public DateTime DateTime => DateTime.ParseExact(time, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        public DigiarzTypeResult LTC { get; set; }
    }

    public class DigiarzDogecoinPriceResult
    {
        public string time { get; set; }
        public DateTime DateTime => DateTime.ParseExact(time, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        public DigiarzTypeResult DOGE { get; set; }
    }

    public class DigiarzDashPriceResult
    {
        public string time { get; set; }
        public DateTime DateTime => DateTime.ParseExact(time, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
        public DigiarzTypeResult DASH { get; set; }
    }
}
