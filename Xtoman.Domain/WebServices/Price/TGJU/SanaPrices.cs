﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Price.TGJU
{
    public class SanaPrices
    {
        [JsonProperty("sana_buy_usd")]
        public SanaPriceDetail SanaBuyUsd { get; set; }

        [JsonProperty("sana_buy_eur")]
        public SanaPriceDetail SanaBuyEur { get; set; }

        [JsonProperty("sana_buy_aed")]
        public SanaPriceDetail SanaBuyAed { get; set; }

        [JsonProperty("sana_sell_usd")]
        public SanaPriceDetail SanaSellUsd { get; set; }

        [JsonProperty("sana_sell_eur")]
        public SanaPriceDetail SanaSellEur { get; set; }

        [JsonProperty("sana_sell_aed")]
        public SanaPriceDetail SanaSellAed { get; set; }
    }
}
