﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Price.TGJU
{
    public class SanaPriceDetail
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("price")]
        public string Price { get; set; }

        [JsonProperty("jdate")]
        public string Jdate { get; set; }

        [JsonProperty("gdate")]
        public string Gdate { get; set; }
    }
}
