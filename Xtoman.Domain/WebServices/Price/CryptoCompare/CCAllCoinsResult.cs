﻿using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Price.CryptoCompare
{
    public class CCAllCoinsResult : CCResponseResult
    {
        public new List<CCAllCoinsItem> Data { get; set; }
    }

    public class CCAllCoinsItem
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public string ImageUrl { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }
        public string CoinName { get; set; }
        public string FullName { get; set; }
        public string Algorithm { get; set; }
        public string ProofType { get; set; }
        public string FullyPremined { get; set; }
        public string TotalCoinSupply { get; set; }
        public string PreMinedValue { get; set; }
        public string TotalCoinsFreeFloat { get; set; }
        public string SortOrder { get; set; }
        public bool Sponsored { get; set; }
    }
}
