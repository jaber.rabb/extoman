﻿using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Price.CryptoCompare
{
    public class CCResponseResult
    {
        public string Response { get; set; }
        public string Message { get; set; }
        public int? Type { get; set; }
        public bool? Aggregated { get; set; }
        public List<object> Data { get; set; }
        public string Path { get; set; }
        public string ErrorsSummary { get; set; }
    }
}
