﻿namespace Xtoman.Domain.WebServices.Price.CryptoCompare
{
    public class CCPriceResult : CCResponseResult
    {
        public decimal? BTC { get; set; }
        public decimal? USD { get; set; }
        public decimal? IRR { get; set; }
    }
}
