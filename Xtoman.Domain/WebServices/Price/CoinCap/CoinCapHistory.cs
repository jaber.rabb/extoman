﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Price.CoinCap
{
    public class CoinCapHistory
    {
        [JsonProperty("market_cap")]
        public List<decimal[]> MarketCap { get; set; }

        [JsonProperty("price")]
        public List<decimal[]> Price { get; set; }

        [JsonProperty("volume")]
        public List<decimal[]> Volume { get; set; }
    }
}
