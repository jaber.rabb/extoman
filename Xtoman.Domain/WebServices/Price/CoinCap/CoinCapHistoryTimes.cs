﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.Price.CoinCap
{
    public enum CoinCapHistoryTimes
    {
        [Display(Name = "1day")]
        OneDay,
        [Display(Name = "7day")]
        SevenDays,
        [Display(Name = "30day")]
        ThirtyDays,
        [Display(Name = "90day")]
        NintyDays,
        [Display(Name = "180day")]
        OneHundredAndEightyDays,
        [Display(Name = "360day")]
        ThreeHundredAndSixtyDays
    }
}
