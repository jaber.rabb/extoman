﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Price
{
    public partial class ArzwsArz
    {
        [JsonProperty("validationCheckResult")]
        public ArzwsValidationCheckResult ValidationCheckResult { get; set; }

        [JsonProperty("bazarExchange")]
        public List<ArzwsBazarExchange> BazarExchange { get; set; }
    }

    public partial class ArzwsBazarExchange
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("current")]
        public long Current { get; set; }

        [JsonProperty("maxVal")]
        public long MaxVal { get; set; }

        [JsonProperty("minVal")]
        public long MinVal { get; set; }

        [JsonProperty("differencePercentage")]
        public long DifferencePercentage { get; set; }

        [JsonProperty("differenceValue")]
        public long DifferenceValue { get; set; }

        [JsonProperty("lastTimeDifference")]
        public long LastTimeDifference { get; set; }

        [JsonProperty("insertDate")]
        public DateTimeOffset InsertDate { get; set; }

        [JsonProperty("dateSerial")]
        public long DateSerial { get; set; }

        public long CurrentToman => Current / 10;
        public long MinToman => MinVal / 10;
        public long MaxToman => MaxVal / 10;
    }

    public partial class ArzwsValidationCheckResult
    {
        [JsonProperty("isValid")]
        public bool IsValid { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("command")]
        public long Command { get; set; }
    }
}
