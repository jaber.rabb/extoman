﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Price.Navasan
{
    public class NavasanLatest
    {
        [JsonProperty("harat_naghdi_buy")]
        public NavasanLatestItem HaratNaghdiBuy { get; set; }

        [JsonProperty("harat_naghdi_sell")]
        public NavasanLatestItem HaratNaghdiSell { get; set; }

        [JsonProperty("sekkeh")]
        public NavasanLatestItem Sekkeh { get; set; }

        [JsonProperty("dirham_dubai")]
        public NavasanLatestItem DirhamDubai { get; set; }

        [JsonProperty("aed_sell")]
        public NavasanLatestItem AedSell { get; set; }

        [JsonProperty("usd_farda_buy")]
        public NavasanLatestItem UsdFardaBuy { get; set; }

        [JsonProperty("abshodeh")]
        public NavasanLatestItem Abshodeh { get; set; }

        [JsonProperty("usd_farda_sell")]
        public NavasanLatestItem UsdFardaSell { get; set; }

        [JsonProperty("usd_sell")]
        public NavasanLatestItem UsdSell { get; set; }

        [JsonProperty("usd_buy")]
        public NavasanLatestItem UsdBuy { get; set; }

        [JsonProperty("dolar_harat_sell")]
        public NavasanLatestItem DolarHaratSell { get; set; }

        [JsonProperty("18ayar")]
        public NavasanLatestItem The18Ayar { get; set; }

        [JsonProperty("bahar")]
        public NavasanLatestItem Bahar { get; set; }

        [JsonProperty("nim")]
        public NavasanLatestItem Nim { get; set; }

        [JsonProperty("rob")]
        public NavasanLatestItem Rob { get; set; }

        [JsonProperty("gerami")]
        public NavasanLatestItem Gerami { get; set; }

        [JsonProperty("dolar_soleimanie_sell")]
        public NavasanLatestItem DolarSoleimanieSell { get; set; }

        [JsonProperty("dolar_kordestan_sell")]
        public NavasanLatestItem DolarKordestanSell { get; set; }

        [JsonProperty("dolar_mashad_sell")]
        public NavasanLatestItem DolarMashadSell { get; set; }

        [JsonProperty("usd_shakhs")]
        public NavasanLatestItem UsdShakhs { get; set; }

        [JsonProperty("usd_sherkat")]
        public NavasanLatestItem UsdSherkat { get; set; }

        [JsonProperty("eur_hav")]
        public NavasanLatestItem EurHav { get; set; }

        [JsonProperty("gbp_hav")]
        public NavasanLatestItem GbpHav { get; set; }

        [JsonProperty("cad_hav")]
        public NavasanLatestItem CadHav { get; set; }

        [JsonProperty("aud_hav")]
        public NavasanLatestItem AudHav { get; set; }

        [JsonProperty("myr_hav")]
        public NavasanLatestItem MyrHav { get; set; }

        [JsonProperty("usd_pp")]
        public NavasanLatestItem UsdPp { get; set; }

        [JsonProperty("eur_pp")]
        public NavasanLatestItem EurPp { get; set; }

        [JsonProperty("cny_hav")]
        public NavasanLatestItem CnyHav { get; set; }

        [JsonProperty("try_hav")]
        public NavasanLatestItem TryHav { get; set; }

        [JsonProperty("jpy_hav")]
        public NavasanLatestItem JpyHav { get; set; }

        [JsonProperty("hav_cad_my")]
        public NavasanLatestItem HavCadMy { get; set; }

        [JsonProperty("hav_cad_cheque")]
        public NavasanLatestItem HavCadCheque { get; set; }

        [JsonProperty("hav_cad_cash")]
        public NavasanLatestItem HavCadCash { get; set; }

        [JsonProperty("mex_eur_buy")]
        public NavasanLatestItem MexEurBuy { get; set; }

        [JsonProperty("btc")]
        public NavasanLatestCoinItem Btc { get; set; }

        [JsonProperty("eth")]
        public NavasanLatestCoinItem Eth { get; set; }

        [JsonProperty("xrp")]
        public NavasanLatestCoinItem Xrp { get; set; }

        [JsonProperty("bch")]
        public NavasanLatestCoinItem Bch { get; set; }

        [JsonProperty("ltc")]
        public NavasanLatestCoinItem Ltc { get; set; }

        [JsonProperty("eos")]
        public NavasanLatestCoinItem Eos { get; set; }

        [JsonProperty("bnb")]
        public NavasanLatestCoinItem Bnb { get; set; }

        [JsonProperty("usdt")]
        public NavasanLatestCoinItem Usdt { get; set; }

        [JsonProperty("dash")]
        public NavasanLatestCoinItem Dash { get; set; }

        [JsonProperty("usd")]
        public NavasanLatestItem Usd { get; set; }

        [JsonProperty("eur")]
        public NavasanLatestItem Eur { get; set; }

        [JsonProperty("gbp")]
        public NavasanLatestItem Gbp { get; set; }

        [JsonProperty("jpy")]
        public NavasanLatestItem Jpy { get; set; }

        [JsonProperty("aud")]
        public NavasanLatestItem Aud { get; set; }

        [JsonProperty("cad")]
        public NavasanLatestItem Cad { get; set; }

        [JsonProperty("chf")]
        public NavasanLatestItem Chf { get; set; }

        [JsonProperty("ugx")]
        public NavasanLatestItem Ugx { get; set; }

        [JsonProperty("cny")]
        public NavasanLatestItem Cny { get; set; }

        [JsonProperty("myr")]
        public NavasanLatestItem Myr { get; set; }

        [JsonProperty("bgn")]
        public NavasanLatestItem Bgn { get; set; }

        [JsonProperty("pgk")]
        public NavasanLatestItem Pgk { get; set; }

        [JsonProperty("irr")]
        public NavasanLatestItem Irr { get; set; }

        [JsonProperty("gmd")]
        public NavasanLatestItem Gmd { get; set; }

        [JsonProperty("xcd")]
        public NavasanLatestItem Xcd { get; set; }

        [JsonProperty("htg")]
        public NavasanLatestItem Htg { get; set; }

        [JsonProperty("top")]
        public NavasanLatestItem Top { get; set; }

        [JsonProperty("vuv")]
        public NavasanLatestItem Vuv { get; set; }

        [JsonProperty("nok")]
        public NavasanLatestItem Nok { get; set; }

        [JsonProperty("isk")]
        public NavasanLatestItem Isk { get; set; }

        [JsonProperty("pkr")]
        public NavasanLatestItem Pkr { get; set; }

        [JsonProperty("lbp")]
        public NavasanLatestItem Lbp { get; set; }

        [JsonProperty("lyd")]
        public NavasanLatestItem Lyd { get; set; }

        [JsonProperty("awg")]
        public NavasanLatestItem Awg { get; set; }

        [JsonProperty("mwk")]
        public NavasanLatestItem Mwk { get; set; }

        [JsonProperty("cup")]
        public NavasanLatestItem Cup { get; set; }

        [JsonProperty("bwp")]
        public NavasanLatestItem Bwp { get; set; }

        [JsonProperty("rub")]
        public NavasanLatestItem Rub { get; set; }

        [JsonProperty("brl")]
        public NavasanLatestItem Brl { get; set; }

        [JsonProperty("azn")]
        public NavasanLatestItem Azn { get; set; }

        [JsonProperty("uyu")]
        public NavasanLatestItem Uyu { get; set; }

        [JsonProperty("stn")]
        public NavasanLatestItem Stn { get; set; }

        [JsonProperty("djf")]
        public NavasanLatestItem Djf { get; set; }

        [JsonProperty("mmk")]
        public NavasanLatestItem Mmk { get; set; }

        [JsonProperty("qar")]
        public NavasanLatestItem Qar { get; set; }

        [JsonProperty("clp")]
        public NavasanLatestItem Clp { get; set; }

        [JsonProperty("sek")]
        public NavasanLatestItem Sek { get; set; }

        [JsonProperty("ils")]
        public NavasanLatestItem Ils { get; set; }

        [JsonProperty("kgs")]
        public NavasanLatestItem Kgs { get; set; }

        [JsonProperty("crc")]
        public NavasanLatestItem Crc { get; set; }

        [JsonProperty("fjd")]
        public NavasanLatestItem Fjd { get; set; }

        [JsonProperty("cdf")]
        public NavasanLatestItem Cdf { get; set; }

        [JsonProperty("hnl")]
        public NavasanLatestItem Hnl { get; set; }

        [JsonProperty("lkr")]
        public NavasanLatestItem Lkr { get; set; }

        [JsonProperty("dkk")]
        public NavasanLatestItem Dkk { get; set; }

        [JsonProperty("aed")]
        public NavasanLatestItem Aed { get; set; }

        [JsonProperty("xaf")]
        public NavasanLatestItem Xaf { get; set; }

        [JsonProperty("tjs")]
        public NavasanLatestItem Tjs { get; set; }

        [JsonProperty("mga")]
        public NavasanLatestItem Mga { get; set; }

        [JsonProperty("lrd")]
        public NavasanLatestItem Lrd { get; set; }

        [JsonProperty("ssp")]
        public NavasanLatestItem Ssp { get; set; }

        [JsonProperty("lsl")]
        public NavasanLatestItem Lsl { get; set; }

        [JsonProperty("scr")]
        public NavasanLatestItem Scr { get; set; }

        [JsonProperty("kwd")]
        public NavasanLatestItem Kwd { get; set; }

        [JsonProperty("vnd")]
        public NavasanLatestItem Vnd { get; set; }

        [JsonProperty("egp")]
        public NavasanLatestItem Egp { get; set; }

        [JsonProperty("svc")]
        public NavasanLatestItem Svc { get; set; }

        [JsonProperty("ttd")]
        public NavasanLatestItem Ttd { get; set; }

        [JsonProperty("ghs")]
        public NavasanLatestItem Ghs { get; set; }

        [JsonProperty("mru")]
        public NavasanLatestItem Mru { get; set; }

        [JsonProperty("tzs")]
        public NavasanLatestItem Tzs { get; set; }

        [JsonProperty("mnt")]
        public NavasanLatestItem Mnt { get; set; }

        [JsonProperty("nzd")]
        public NavasanLatestItem Nzd { get; set; }

        [JsonProperty("ron")]
        public NavasanLatestItem Ron { get; set; }

        [JsonProperty("pen")]
        public NavasanLatestItem Pen { get; set; }

        [JsonProperty("jod")]
        public NavasanLatestItem Jod { get; set; }

        [JsonProperty("iqd")]
        public NavasanLatestItem Iqd { get; set; }

        [JsonProperty("cve")]
        public NavasanLatestItem Cve { get; set; }

        [JsonProperty("lak")]
        public NavasanLatestItem Lak { get; set; }

        [JsonProperty("dop")]
        public NavasanLatestItem Dop { get; set; }

        [JsonProperty("sar")]
        public NavasanLatestItem Sar { get; set; }

        [JsonProperty("try")]
        public NavasanLatestItem Try { get; set; }

        [JsonProperty("bdt")]
        public NavasanLatestItem Bdt { get; set; }

        [JsonProperty("pyg")]
        public NavasanLatestItem Pyg { get; set; }

        [JsonProperty("mad")]
        public NavasanLatestItem Mad { get; set; }

        [JsonProperty("xpf")]
        public NavasanLatestItem Xpf { get; set; }

        [JsonProperty("aoa")]
        public NavasanLatestItem Aoa { get; set; }

        [JsonProperty("yer")]
        public NavasanLatestItem Yer { get; set; }

        [JsonProperty("zar")]
        public NavasanLatestItem Zar { get; set; }

        [JsonProperty("idr")]
        public NavasanLatestItem Idr { get; set; }

        [JsonProperty("kzt")]
        public NavasanLatestItem Kzt { get; set; }

        [JsonProperty("bob")]
        public NavasanLatestItem Bob { get; set; }

        [JsonProperty("bzd")]
        public NavasanLatestItem Bzd { get; set; }

        [JsonProperty("kmf")]
        public NavasanLatestItem Kmf { get; set; }

        [JsonProperty("all")]
        public NavasanLatestItem All { get; set; }

        [JsonProperty("khr")]
        public NavasanLatestItem Khr { get; set; }

        [JsonProperty("czk")]
        public NavasanLatestItem Czk { get; set; }

        [JsonProperty("thb")]
        public NavasanLatestItem Thb { get; set; }

        [JsonProperty("php")]
        public NavasanLatestItem Php { get; set; }

        [JsonProperty("tmt")]
        public NavasanLatestItem Tmt { get; set; }

        [JsonProperty("afn")]
        public NavasanLatestItem Afn { get; set; }

        [JsonProperty("jmd")]
        public NavasanLatestItem Jmd { get; set; }

        [JsonProperty("gip")]
        public NavasanLatestItem Gip { get; set; }

        [JsonProperty("szl")]
        public NavasanLatestItem Szl { get; set; }

        [JsonProperty("npr")]
        public NavasanLatestItem Npr { get; set; }

        [JsonProperty("krw")]
        public NavasanLatestItem Krw { get; set; }

        [JsonProperty("bhd")]
        public NavasanLatestItem Bhd { get; set; }

        [JsonProperty("twd")]
        public NavasanLatestItem Twd { get; set; }

        [JsonProperty("uah")]
        public NavasanLatestItem Uah { get; set; }

        [JsonProperty("etb")]
        public NavasanLatestItem Etb { get; set; }

        [JsonProperty("srd")]
        public NavasanLatestItem Srd { get; set; }

        [JsonProperty("syp")]
        public NavasanLatestItem Syp { get; set; }

        [JsonProperty("ern")]
        public NavasanLatestItem Ern { get; set; }

        [JsonProperty("sos")]
        public NavasanLatestItem Sos { get; set; }

        [JsonProperty("wst")]
        public NavasanLatestItem Wst { get; set; }

        [JsonProperty("mur")]
        public NavasanLatestItem Mur { get; set; }

        [JsonProperty("huf")]
        public NavasanLatestItem Huf { get; set; }

        [JsonProperty("ngn")]
        public NavasanLatestItem Ngn { get; set; }

        [JsonProperty("rsd")]
        public NavasanLatestItem Rsd { get; set; }

        [JsonProperty("mkd")]
        public NavasanLatestItem Mkd { get; set; }

        [JsonProperty("sbd")]
        public NavasanLatestItem Sbd { get; set; }

        [JsonProperty("ang")]
        public NavasanLatestItem Ang { get; set; }

        [JsonProperty("mop")]
        public NavasanLatestItem Mop { get; set; }

        [JsonProperty("bam")]
        public NavasanLatestItem Bam { get; set; }

        [JsonProperty("dzd")]
        public NavasanLatestItem Dzd { get; set; }

        [JsonProperty("pln")]
        public NavasanLatestItem Pln { get; set; }

        [JsonProperty("hrk")]
        public NavasanLatestItem Hrk { get; set; }

        [JsonProperty("byn")]
        public NavasanLatestItem Byn { get; set; }

        [JsonProperty("ars")]
        public NavasanLatestItem Ars { get; set; }

        [JsonProperty("tnd")]
        public NavasanLatestItem Tnd { get; set; }

        [JsonProperty("bif")]
        public NavasanLatestItem Bif { get; set; }

        [JsonProperty("zmw")]
        public NavasanLatestItem Zmw { get; set; }

        [JsonProperty("gtq")]
        public NavasanLatestItem Gtq { get; set; }

        [JsonProperty("bnd")]
        public NavasanLatestItem Bnd { get; set; }

        [JsonProperty("sgd")]
        public NavasanLatestItem Sgd { get; set; }

        [JsonProperty("hkd")]
        public NavasanLatestItem Hkd { get; set; }

        [JsonProperty("amd")]
        public NavasanLatestItem Amd { get; set; }

        [JsonProperty("ves")]
        public NavasanLatestItem Ves { get; set; }

        [JsonProperty("bsd")]
        public NavasanLatestItem Bsd { get; set; }

        [JsonProperty("gnf")]
        public NavasanLatestItem Gnf { get; set; }

        [JsonProperty("gel")]
        public NavasanLatestItem Gel { get; set; }

        [JsonProperty("omr")]
        public NavasanLatestItem Omr { get; set; }

        [JsonProperty("cop")]
        public NavasanLatestItem Cop { get; set; }

        [JsonProperty("mxn")]
        public NavasanLatestItem Mxn { get; set; }

        [JsonProperty("mdl")]
        public NavasanLatestItem Mdl { get; set; }

        [JsonProperty("nio")]
        public NavasanLatestItem Nio { get; set; }

        [JsonProperty("gyd")]
        public NavasanLatestItem Gyd { get; set; }

        [JsonProperty("rwf")]
        public NavasanLatestItem Rwf { get; set; }

        [JsonProperty("sll")]
        public NavasanLatestItem Sll { get; set; }

        [JsonProperty("mvr")]
        public NavasanLatestItem Mvr { get; set; }

        [JsonProperty("inr")]
        public NavasanLatestItem Inr { get; set; }

        [JsonProperty("bbd")]
        public NavasanLatestItem Bbd { get; set; }

        [JsonProperty("xof")]
        public NavasanLatestItem Xof { get; set; }

        [JsonProperty("uzs")]
        public NavasanLatestItem Uzs { get; set; }

        [JsonProperty("pab")]
        public NavasanLatestItem Pab { get; set; }

        [JsonProperty("nad")]
        public NavasanLatestItem Nad { get; set; }

        [JsonProperty("sdg")]
        public NavasanLatestItem Sdg { get; set; }

        [JsonProperty("mzn")]
        public NavasanLatestItem Mzn { get; set; }

        [JsonProperty("kes")]
        public NavasanLatestItem Kes { get; set; }

        [JsonProperty("mro")]
        public NavasanLatestItem Mro { get; set; }

        [JsonProperty("mex_usd_sell")]
        public NavasanLatestItem MexUsdSell { get; set; }

        [JsonProperty("mex_usd_buy")]
        public NavasanLatestItem MexUsdBuy { get; set; }

        [JsonProperty("mex_eur_sell")]
        public NavasanLatestItem MexEurSell { get; set; }

        [JsonProperty("xau")]
        public NavasanLatestItem Xau { get; set; }

        [JsonProperty("bub_sekkeh")]
        public NavasanLatestItem BubSekkeh { get; set; }

        [JsonProperty("bub_bahar")]
        public NavasanLatestItem BubBahar { get; set; }

        [JsonProperty("bub_rob")]
        public NavasanLatestItem BubRob { get; set; }

        [JsonProperty("bub_18ayar")]
        public NavasanLatestItem Bub18Ayar { get; set; }

        [JsonProperty("bub_abshodeh")]
        public NavasanLatestItem BubAbshodeh { get; set; }

        [JsonProperty("bub_gerami")]
        public NavasanLatestItem BubGerami { get; set; }

        [JsonProperty("bub_nim")]
        public NavasanLatestItem BubNim { get; set; }

        [JsonProperty("usd_xau")]
        public NavasanLatestItem UsdXau { get; set; }

        [JsonProperty("mob_usd")]
        public NavasanLatestItem MobUsd { get; set; }

        [JsonProperty("mob_gbp")]
        public NavasanLatestItem MobGbp { get; set; }

        [JsonProperty("mob_eur")]
        public NavasanLatestItem MobEur { get; set; }

        [JsonProperty("mob_aed")]
        public NavasanLatestItem MobAed { get; set; }
    }
}
