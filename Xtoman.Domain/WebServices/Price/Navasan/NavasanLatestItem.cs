﻿using Newtonsoft.Json;
using System;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Price.Navasan
{
    public partial class NavasanLatestItem
    {
        [JsonProperty("value")]
        public long Value { get; set; }

        [JsonProperty("change")]
        public decimal Change { get; set; }

        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }

        [JsonProperty("date")]
        public DateTimeOffset Date { get; set; }
    }

    public partial class NavasanLatestCoinItem
    {
        [JsonProperty("value")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Value { get; set; }

        [JsonProperty("change")]
        public long Change { get; set; }

        [JsonProperty("timestamp")]
        public DateTimeOffset Timestamp { get; set; }

        [JsonProperty("date")]
        public string Date { get; set; }
    }

}
