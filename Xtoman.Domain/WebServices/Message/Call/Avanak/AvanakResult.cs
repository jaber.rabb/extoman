﻿namespace Xtoman.Domain.WebServices.Message.Call.Avanak
{
    public class AvanakResult
    {
        public bool Succeeded { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
