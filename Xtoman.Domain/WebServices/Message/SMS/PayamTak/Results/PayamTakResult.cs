﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.Message.SMS.PayamTak
{
    public class PayamTakResult
    {
        public string Value { get; set; }
        public int RetStatus { get; set; }
        [JsonIgnore]
        public PayamTakSendSMSStatus Status
        {
            get
            {
                try
                {
                    return (PayamTakSendSMSStatus)RetStatus;
                }
                catch (Exception)
                {
                    return PayamTakSendSMSStatus.Uknown;
                }
            }
        }
        public string StrRetStatus { get; set; }
        [JsonIgnore]
        public bool Success => StrRetStatus == "Ok";
    }
}
