﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.WebServices.Message.SMS.PayamTak
{
    public enum PayamTakSendSMSStatus
    {
        [Display(Name = "نامشخص")]
        Uknown = -150,
        [Display(Name = "نام کاربری یا رمز عبور اشتباه می باشد.")]
        InvalidUserNameOrPassword = 0,
        [Display(Name = "درخواست با موفقیت انجام شد.")]
        Success = 1,
        [Display(Name = "اعتبار کافی نمی باشد.")]
        InsufficentBalance = 2,
        [Display(Name = "محدودیت در ارسال روزانه")]
        DailyLimitReached = 3,
        [Display(Name = "محدودیت در حجم ارسال")]
        SendPerRequestLimitReached = 4,
        [Display(Name = "شماره فرستنده معتبر نمی باشد.")]
        SenderNumberIsWrong = 5,
        [Display(Name = "سامانه در حال بروزرسانی است.")]
        ServerInMaintenance = 6,
        [Display(Name = "متن حاوی کلمه فیلتر شده است.")]
        TextContainsFilteredWord = 7,
        [Display(Name = "لیست سیاه")]
        BlackList = 35
    }
}
