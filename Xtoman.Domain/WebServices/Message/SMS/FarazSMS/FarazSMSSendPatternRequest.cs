﻿using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Message.SMS.FarazSMS
{
    public class FarazSMSSendPatternRequest : FarazSMSRequestBase
    {
        public FarazSMSSendPatternRequest(string toNum, string patternCode, object inputData) : base()
        {
            this.op = "pattern";
            this.fromNum = AppSettingManager.PayamTak_FromPattern;
            this.toNum = new string[] { toNum };
            this.pattern_code = patternCode;
            this.input_data = new object[] { inputData };
        }
        public string fromNum;
        public string[] toNum;
        public string pattern_code;
        public object[] input_data;
    }

    public class FarazSMSGetCreditRequest : FarazSMSRequestBase
    {
        public FarazSMSGetCreditRequest() : base()
        {
            this.op = "credit";
        }
    }
}
