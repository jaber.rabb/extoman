﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Xtoman.Utility;

namespace Xtoman.Domain.WebServices.Message.SMS.FarazSMS
{
    public class FarazSMSRequestBase
    {
        public FarazSMSRequestBase()
        {
            this.uname = AppSettingManager.PayamTak_UserName;
            this.pass = AppSettingManager.PayamTak_Password;
        }
        public string op { get; set; }
        public string uname { get; set; }
        public string user { get { return uname; } set { uname = value; } }
        public string pass { get; set; }
    }
}
