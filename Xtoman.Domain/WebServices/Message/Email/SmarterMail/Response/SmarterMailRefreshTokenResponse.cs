﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.Message.Email.SmarterMail
{
    public class SmarterMailRefreshTokenResponse : SmarterMailBaseResponse
    {
        [JsonProperty("isImpersonating")]
        public bool IsImpersonating { get; set; }

        [JsonProperty("canViewPasswords")]
        public bool CanViewPasswords { get; set; }

        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }

        [JsonProperty("accessTokenExpiration")]
        public DateTime AccessTokenExpiration { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }
    }
}
