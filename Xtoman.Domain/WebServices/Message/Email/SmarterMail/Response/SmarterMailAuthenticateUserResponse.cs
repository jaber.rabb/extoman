﻿using Newtonsoft.Json;
using System;

namespace Xtoman.Domain.WebServices.Message.Email.SmarterMail
{
    [Serializable]
    public class SmarterMailAuthenticateUserResponse : SmarterMailBaseResponse
    {
        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("changePasswordNeeded")]
        public bool ChangePasswordNeeded { get; set; }

        [JsonProperty("displayWelcomeWizard")]
        public bool DisplayWelcomeWizard { get; set; }

        [JsonProperty("isAdmin")]
        public bool IsAdmin { get; set; }

        [JsonProperty("isDomainAdmin")]
        public bool IsDomainAdmin { get; set; }

        [JsonProperty("isLicensed")]
        public bool IsLicensed { get; set; }

        [JsonProperty("autoLoginToken")]
        public string AutoLoginToken { get; set; }

        [JsonProperty("autoLoginUrl")]
        public string AutoLoginUrl { get; set; }

        [JsonProperty("isImpersonating")]
        public bool IsImpersonating { get; set; }

        [JsonProperty("canViewPasswords")]
        public bool CanViewPasswords { get; set; }

        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }

        [JsonProperty("accessTokenExpiration")]
        public DateTime AccessTokenExpiration { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }
    }
}
