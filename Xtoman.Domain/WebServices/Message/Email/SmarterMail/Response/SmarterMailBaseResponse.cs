﻿using Newtonsoft.Json;

namespace Xtoman.Domain.WebServices.Message.Email.SmarterMail
{
    public class SmarterMailBaseResponse
    {
        /// <summary>
        /// If true the function call succeeded. [Required]
        /// </summary>
        [JsonProperty("success")]
        public bool Success { get; set; }

        /// <summary>
        /// enum (see <see cref="SmarterMailHttpStatusCode"/>)
        /// </summary>
        [JsonProperty("resultCode")]
        public SmarterMailHttpStatusCode ResultCode { get; set; }

        /// <summary>
        /// If success is false, this field tells you why the call failed.
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// Will sometimes contain additional info relating to a call.
        /// </summary>
        [JsonProperty("debugInfo")]
        public string DebugInfo { get; set; }
    }
}
