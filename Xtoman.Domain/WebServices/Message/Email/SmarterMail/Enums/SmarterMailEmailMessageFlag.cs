﻿namespace Xtoman.Domain.WebServices.Message.Email.SmarterMail
{
    public enum SmarterMailEmailMessageFlag
    {
        Read,
        Replied,
        Forwarded,
        Deleted,
        Flagged,
        Draft,
        Recent,
        HasAttachment,
        LinkedToTask
    }
}
