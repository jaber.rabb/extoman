﻿namespace Xtoman.Domain.WebServices.Message.Email.SmarterMail
{
    public enum SmarterMailMessagePriorities
    {
        High,
        Normal,
        Low
    }
}
