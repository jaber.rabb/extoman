﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.WebServices.Message.Email.SmarterMail
{
    public class SmarterMailAuthenticateUser
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("twoFactorCode")]
        public string TwoFactorCode { get; set; }

        [JsonProperty("twoFactorSetupGuid")]
        public string TwoFactorSetupGuid { get; set; }

        [JsonProperty("retrieveAutoLoginToken")]
        public bool? RetrieveAutoLoginToken { get; set; }

        [JsonProperty("autoLoginToken")]
        public string AutoLoginToken { get; set; }
    }
}
