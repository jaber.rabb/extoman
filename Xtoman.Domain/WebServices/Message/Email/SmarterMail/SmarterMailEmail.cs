﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Xtoman.Domain.WebServices.Message.Email.SmarterMail
{
    public class SmarterMailEmail
    {
        public SmarterMailEmail()
        {
            Date = DateTime.Now;
        }
        /// <summary>
        /// Bcc recipients.
        /// </summary>
        [JsonProperty("bcc")]
        public string BCC { get; set; }
        /// <summary>
        /// Cc recipients.
        /// </summary>
        [JsonProperty("cc")]
        public string CC { get; set; }

        /// <summary>
        /// From address.
        /// </summary>
        [JsonProperty("from")]
        public string From { get; set; }

        /// <summary>
        /// The date the email was received by the server.
        /// </summary>
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        /// <summary>
        /// Source of the from address. [alias, domainalias, smtpaccount, plusaddress]
        /// </summary>
        [JsonProperty("selectedFrom")]
        public string SelectedFrom { get; set; }

        /// <summary>
        /// enum (see <see cref="SmarterMailMessagePriorities"/>)
        /// </summary>
        [JsonProperty("priority")]
        public SmarterMailMessagePriorities Priority { get; set; }

        /// <summary>
        /// The reply-to address.
        /// </summary>
        [JsonProperty("replyTo")]
        public string ReplyTo { get; set; }

        /// <summary>
        /// The item's subject text.
        /// </summary>
        [JsonProperty("subject")]
        public string Subject { get; set; }

        /// <summary>
        /// To address. [Required]
        /// </summary>
        [JsonProperty("to")]
        public string To { get; set; }

        /// <summary>
        /// A plain text version of the email.
        /// </summary>
        [JsonProperty("messagePlainText")]
        public string MessagePlainText { get; set; }

        /// <summary>
        /// An HTML version of the email.
        /// </summary>
        [JsonProperty("messageHTML")]
        public string MessageHtml { get; set; }

        /// <summary>
        /// If true the email message flag is set.
        /// </summary>
        [JsonProperty("markForFollowup")]
        public bool MarkForFollowup { get; set; }

        /// <summary>
        /// The attachment's unique ID.
        /// </summary>
        [JsonProperty("attachmentGuid")]
        public Guid AttachmentGuid { get; set; }

        /// <summary>
        /// An identifier for the person sharing the item.
        /// </summary>
        [JsonProperty("ownerEmailAddress")]
        public string OwnerEmailAddress { get; set; }

        /// <summary>
        /// The name of the folder.
        /// </summary>
        [JsonProperty("folder")]
        public string Folder { get; set; }

        [JsonProperty("replyUid")]
        public long ReplyUid { get; set; }

        [JsonProperty("draftUid")]
        public long DraftUid { get; set; }

        /// <summary>
        /// The various email flags settings.
        /// </summary>
        [JsonProperty("actions")]
        public Dictionary<string, bool> Actions { get; set; }

        /// <summary>
        /// Flag used to determine if a read receipt is needed.
        /// </summary>
        [JsonProperty("readReceiptRequested")]
        public bool ReadReceiptRequested { get; set; }

        [JsonProperty("deliveryReceiptRequested")]
        public bool DeliveryReceiptRequested { get; set; }

        [JsonProperty("originalCidLinks")]
        public Dictionary<string, bool> OriginalCidLinks { get; set; }

        /// <summary>
        ///  A list of files that should be excluded from the email.
        /// </summary>
        [JsonProperty("excludeFiles")]
        public List<int> ExcludeFiles { get; set; }

        [JsonProperty("inlineToRemove")]
        public List<string> InlineToRemove { get; set; }

        [JsonProperty("attachedMessages")]
        public List<SmarterMailAttachedMessage> AttachedMessages { get; set; }
    }

    public class SmarterMailAttachedMessage
    {
        [JsonProperty("ownerEmailAddress")]
        public string OwnerEmailAddress { get; set; }

        [JsonProperty("folder")]
        public string Folder { get; set; }

        [JsonProperty("uid")]
        public long Uid { get; set; }
    }
}
