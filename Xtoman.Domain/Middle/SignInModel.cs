﻿using Xtoman.Domain.Models;

namespace Xtoman.Domain
{
    public class SignInModel
    {
        public string EmailOrPhone { get; set; }
        public string IPAddress { get; set; }
        public string Browser { get; set; }
        public string Password { get; set; }
        public bool IsPersistent { get; set; }
        public bool ShouldLockout { get; set; }
        public SubdomainType Subdomain { get; set; }
    }

    public class SignInModel2
    {
        public string EmailOrPhoneOrUserName { get; set; }
        public string IPAddress { get; set; }
        public string Browser { get; set; }
        public string Password { get; set; }
        public bool IsPersistent { get; set; }
        public bool ShouldLockout { get; set; }
        public SubdomainType Subdomain { get; set; }
    }
}
