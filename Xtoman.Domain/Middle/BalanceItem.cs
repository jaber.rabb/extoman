﻿using Xtoman.Domain.Models;

namespace Xtoman.Domain
{
    public class CurrencyAmountItem
    {
        public decimal Amount { get; set; }
        public ECurrencyType Type { get; set; }
    }

    public class BalanceItem : CurrencyAmountItem
    {
        public string Name { get; set; }
    }
}
