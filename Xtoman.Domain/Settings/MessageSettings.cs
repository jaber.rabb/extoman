﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Settings
{
    public class MessageSettings : ISettings
    {
        [Display(Name = "تعداد تلاش برای ارسال ایمیل")]
        public byte EmailSendingMaxTry { get; set; }
        [Display(Name = "تعداد تلاش برای ارسال پیامک")]
        public byte SMSSendingMaxTry { get; set; }
        [Display(Name = "ایمیل پیش فرض")]
        public int Default_EmailAccountId { get; set; }
        [Display(Name = "ایمیل دریافت کننده گزارشات")]
        public string ReportsReceiver_EmailAccount { get; set; }
        [Display(Name = "شماره موبایل دریافت کننده گزارشات")]
        public string ReportsReceiver_PhoneNumber { get; set; }

        #region Registration

        #region User
        [Display(Name = "ایمیل ثبت نام برای کاربر فرستاده شود؟")]
        public bool Email_User_Registration { get; set; }
        [Display(Name = "ایمیل ارسال کننده ثبت نام برای کاربر")]
        public int Email_Registration_EmailAccountId { get; set; }
        [Display(Name = "پیامک ثبت نام برای کاربر فرستاده شود؟")]
        public bool SMS_User_Registration { get; set; }
        #endregion
        #region Admin
        [Display(Name = "ایمیل ثبت نام برای مدیریت فرستاده شود؟")]
        public bool Email_Admin_Registration { get; set; }
        [Display(Name = "پیامک ثبت نام برای مدیریت فرستاده شود؟")]
        public bool SMS_Admin_Registration { get; set; }
        [Display(Name = "پیام تلگرامی ثبت نام برای مدیریت فرستاده شود؟")]
        public bool Telegram_Admin_Registration { get; set; }
        [Display(Name = "آی دی کانال یا شماره چت برای گزارش ثبت نام")]
        public string Telegram_Admin_Registration_ChannelId { get; set; }
        [Display(Name = "توکن ربات تلگرام برای گزارش ثبت نام")]
        public string Telegram_Admin_Registration_Token { get; set; }
        #endregion

        #endregion

        #region New Order

        #region User
        [Display(Name = "ایمیل ثبت سفارش جدید برای کاربر فرستاده شود؟")]
        public bool Email_User_NewOrder { get; set; }
        [Display(Name = "ایمیل ارسال کننده ثبت سفارش جدید")]
        public int Email_NewOrder_EmailAccountId { get; set; }
        [Display(Name = "پیامک ثبت سفارش جدید برای کاربر فرستاده شود؟")]
        public bool SMS_User_NewOrder { get; set; }
        #endregion
        #region Admin
        [Display(Name = "ایمیل ثبت سفارش جدید برای مدیریت فرستاده شود؟")]
        public bool Email_Admin_NewOrder { get; set; }
        [Display(Name = "پیامک ثبت سفارش جدید برای مدیریت فرستاده شود؟")]
        public bool SMS_Admin_NewOrder { get; set; }
        [Display(Name = "پیام تلگرامی ثبت سفارش جدید برای مدیریت فرستاده شود؟")]
        public bool Telegram_Admin_NewOrder { get; set; }
        [Display(Name = "آی دی کانال یا شماره چت برای گزارش ثبت سفارش جدید")]
        public string Telegram_Admin_NewOrder_ChannelId { get; set; }
        [Display(Name = "توکن ربات تلگرام برای گزارش ثبت سفارش جدید")]
        public string Telegram_Admin_NewOrder_Token { get; set; }
        #endregion

        #endregion

        #region User Verify Request

        #region User
        [Display(Name = "ایمیل درخواست احراز هویت برای کاربر فرستاده شود؟")]
        public bool Email_User_UserVerifyRequest { get; set; }
        [Display(Name = "ایمیل ارسال کننده درخواست احراز هویت")]
        public int Email_UserVerifyRequest_EmailAccountId { get; set; }
        [Display(Name = "پیامک درخواست احراز هویت برای کاربر فرستاده شود؟")]
        public bool SMS_User_UserVerifyRequest { get; set; }
        #endregion
        #region Admin
        [Display(Name = "ایمیل درخواست احراز هویت برای مدیریت فرستاده شود؟")]
        public bool Email_Admin_UserVerifyRequest { get; set; }
        [Display(Name = "پیامک درخواست احراز هویت برای مدیریت فرستاده شود؟")]
        public bool SMS_Admin_UserVerifyRequest { get; set; }
        [Display(Name = "پیام تلگرامی درخواست احراز هویت برای مدیریت فرستاده شود؟")]
        public bool Telegram_Admin_UserVerifyRequest { get; set; }
        [Display(Name = "آی دی کانال یا شماره چت برای گزارش درخواست احراز هویت")]
        public string Telegram_Admin_UserVerifyRequest_ChannelId { get; set; }
        [Display(Name = "توکن ربات تلگرام برای گزارش درخواست احراز هویت")]
        public string Telegram_Admin_UserVerifyRequest_Token { get; set; }
        #endregion

        #endregion

        #region User Verify Respond

        #region User
        [Display(Name = "ایمیل تایید احراز هویت برای کاربر فرستاده شود؟")]
        public bool Email_User_UserVerifyRespond { get; set; }
        [Display(Name = "ایمیل ارسال کننده تایید احراز هویت")]
        public int Email_UserVerifyRespond_EmailAccountId { get; set; }
        [Display(Name = "پیامک تایید احراز هویت برای کاربر فرستاده شود؟")]
        public bool SMS_User_UserVerifyRespond { get; set; }
        #endregion

        #endregion

        #region Forgot Password

        #region User
        [Display(Name = "Email user forgot password")]
        public bool Email_User_ForgotPassword { get; set; }
        [Display(Name = "Forgot password sender email account")]
        public int Email_ForgotPassword_EmailAccountId { get; set; }
        [Display(Name = "SMS user forgot password")]
        public bool SMS_User_ForgotPassword { get; set; }
        #endregion
        #region Admin
        [Display(Name = "Telegram admin forgot password")]
        public bool Telegram_Admin_ForgotPassword { get; set; }
        [Display(Name = "Telegram admin forgot password channel id")]
        public string Telegram_Admin_ForgotPassword_ChannelId { get; set; }
        [Display(Name = "Telegram admin forgot password api token")]
        public string Telegram_Admin_ForgotPassword_Token { get; set; }
        #endregion

        #endregion

        #region Support Ticket Submit

        #region User
        [Display(Name = "ایمیل ثبت تیکت پشتیبانی برای کاربر فرستاده شود؟")]
        public bool Email_User_SupportTicketSubmit { get; set; }
        [Display(Name = "پیامک ثبت تیکت پشتیبانی برای کاربر فرستاده شود؟")]
        public bool SMS_User_SupportTicketSubmit { get; set; }
        [Display(Name = "ایمیل ارسال کننده ثبت تیکت پشتیبانی")]
        public int Email_SupportTicketSubmit_EmailAccountId { get; set; }
        #endregion

        #region Admin
        [Display(Name = "ایمیل ثبت درخواست پشتیبانی برای مدیریت فرستاده شود؟")]
        public bool Email_Admin_SupportTicketSubmit { get; set; }
        [Display(Name = "پیامک ثبت درخواست پشتیبانی برای مدیریت فرستاده شود؟")]
        public bool SMS_Admin_SupportTicketSubmit { get; set; }
        [Display(Name = "پیام تلگرامی ثبت درخواست پشتیبانی برای مدیریت فرستاده شود؟")]
        public bool Telegram_Admin_SupportTicketSubmit { get; set; }
        [Display(Name = "آی دی کانال یا شماره چت برای گزارش ثبت درخواست پشتیبانی")]
        public string Telegram_Admin_SupportTicketSubmit_ChannelId { get; set; }
        [Display(Name = "توکن ربات تلگرام برای گزارش ثبت درخواست پشتیبانی")]
        public string Telegram_Admin_SupportTicketSubmit_Token { get; set; }
        #endregion

        #endregion

        #region Phone & Email Changed
        [Display(Name = "پیامک تغییر موبایل به کاربر فرستاده شود؟")]
        public bool SMS_User_PhoneNumberChanged { get; set; }
        [Display(Name = "ایمیل تغییر آدرس ایمیل به کاربر فرستاده شود؟")]
        public bool Email_User_EmailChanged { get; set; }
        [Display(Name = "ایمیل ارسال کننده تغییر ایمیل")]
        public int Email_EmailChanged_EmailAccountId { get; set; }
        #endregion
    }
}
