﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Settings
{
    public class PaymentSettings : ISettings
    {
        #region Shetab
        [Display(Name = "شماره کارت شتاب")]
        public string Shetab_CardNumber { get; set; }
        public string Shetab_Sheba { get; set; }
        #endregion

        //#region Behpardakht
        //[Display(Name = "کد پایانه بهپرداخت(ملت)")]
        //public int Behpardakht_TerminalCode { get; set; }
        //[Display(Name = "نام کاربری بهپرداخت(ملت)")]
        //public string Behpardakht_Username { get; set; }
        //[Display(Name = "رمز عبور بهپرداخت(ملت)")]
        //public string Behpardakht_Password { get; set; }
        //#endregion

        //#region Saman
        //[Display(Name = "رمز عبور سِپ(سامان)")]
        //public string Sep_Password { get; set; }
        //[Display(Name = "نام کاربری سِپ(سامان)")]
        //public string Sep_Username { get; set; }
        //[Display(Name = "کد پذیرنده سِپ(سامان)")]
        //public string Sep_MerchandId { get; set; }
        //#endregion

        //#region PerfectMoney
        //[Display(Name = "Member Id پرفکت مانی")]
        //public string PerfectMoney_MemberId { get; set; }
        //[Display(Name = "USD account number پرفکت مانی")]
        //public string PerfectMoney_USD_Account { get; set; }
        //[Display(Name = "passphere پرفکت مانی")]
        //public string PerfectMoney_Passphere { get; set; }
        //[Display(Name = "alt passphere پرفکت مانی")]
        //public string PerfectMoney_Alt_Passphere { get; set; }
        //#endregion

        //#region PAYEER
        //[Display(Name = "پییر merchant shop id")]
        //public string PAYEER_MerchantShopId { get; set; }
        //[Display(Name = "پییر secret key")]
        //public string PAYEER_SecretKey { get; set; }

        //[Display(Name = "پییر API ID")]
        //public string PAYEER_ApiId { get; set; }
        //[Display(Name = "پییر Account ID")]
        //public string PAYEER_AccountId { get; set; }
        //[Display(Name = "پییر API Secret Key")]
        //public string PAYEER_ApiSecretKey { get; set; }
        //#endregion

        //#region CoinPayments.net
        //[Display(Name = "کوین پیمنت Private Key")]
        //public string CoinPayments_PrivateKey { get; set; }
        //[Display(Name = "کوین پیمنت Public Key")]
        //public string CoinPayments_PublicKey { get; set; }
        //#endregion

        //#region Changelly
        //[Display(Name = "Changelly Key")]
        //public string Changelly_Key { get; set; }
        //[Display(Name = "Changelly Secret")]
        //public string Changelly_Secret { get; set; }
        //#endregion
        public decimal FirstOrderWithReferrerDiscountPercent { get; set; }
        public int FirstOrderWithReferrerDiscountAvailableDays { get; set; }
        public int USDPrice { get; set; }
        public int USDHighPrice { get; set; }
        public int USDLowPrice { get; set; }
        public int EURPrice { get; set; }
        public int EURHighPrice { get; set; }
        public int EURLowPrice { get; set; }
    }
}
