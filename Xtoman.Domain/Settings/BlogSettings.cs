﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Settings
{
    public class BlogSettings : ISettings
    {
        [Display(Name = "تعداد پست های صفحه وبلاگ")]
        public int BlogPageSize { get; set; }
        [Display(Name = "تعداد پست های پر بازدید")]
        public int PopularPostsCount { get; set; }
        [Display(Name = "تعداد پست های مرتبط")]
        public int RelatedPostsCount { get; set; }
        [Display(Name = "تعداد آخرین پست ها")]
        public int LatestPostsCount { get; set; }
        [Display(Name = "تعداد پرچسب ها در صفحه تکی پست")]
        public int SinglePageTagsCount { get; set; }
        [Display(Name = "تعداد برچسب ها در صفحه وبلاگ")]
        public int IndexTagsCount { get; set; }
        [Display(Name = "نمایش برچسب ها در صفحه وبلاگ؟")]
        public bool IndexTagsAreEnabled { get; set; }
        [Display(Name = "متا توضیحات وبلاگ")]
        public string BlogMetaDescription { get; set; }
        [Display(Name = "متا کلمات کلیدی وبلاگ")]
        public string BlogMetaKeywords { get; set; }
    }
}
