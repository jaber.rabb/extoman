﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Domain.Settings
{
    public class CommentSettings : ISettings
    {
        [Display(Name = "تعداد اولیه نمایش نظرات در صفحه پست")]
        public int PostCommentVisiblesCount { get; set; }
    }
}
