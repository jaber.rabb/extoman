﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Domain.Settings
{
    public class CacheSettings : ISettings
    {
        /// <summary>
        /// Gets or sets a value indicating cache on startup or not"
        /// </summary>
        [Display(Name = "Cache all in first request?")]
        public bool LoadAllOnStartup { get; set; }
        [Display(Name = "Default cache expire minutes")]
        public int DefaultCacheMinutes { get; set; }
    }
}
