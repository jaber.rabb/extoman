﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web2.ViewModels
{
    public class PostCategoryViewModel : BaseViewModel<PostCategoryViewModel, PostCategory>
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public int Order { get; set; }
        public string ColorHex { get; set; }
        public int? ParentCategoryId { get; set; } // For parent
    }
}