﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web2.ViewModels
{
    public class ReviewViewModel : BaseViewModel<ReviewViewModel, OrderReview>
    {
        public new long Id { get; set; }
        [Display(Name = "متن نظر")]
        public string Text { get; set; }
        [Display(Name = "امتیاز")]
        public byte VoteRate { get; set; }
        [Display(Name = "وضعیت نمایش")]
        public Approvement Visibility { get; set; }

        public string OrderUserUserName { get; set; }
        public string OrderUserFirstName { get; set; }
        public string OrderUserLastName { get; set; }
        public CurrencyViewModel OrderExchangeFromCurrency { get; set; }
        public CurrencyViewModel OrderExchangeToCurrency { get; set; }
    }
}