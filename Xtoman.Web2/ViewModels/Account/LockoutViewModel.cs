﻿using System;

namespace Xtoman.Web2.ViewModels
{
    public class LockoutViewModel
    {
        public DateTimeOffset EndDate { get; set; }
    }
}