﻿using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web2.ViewModels
{
    public class ExchangeTypeViewModel : BaseViewModel<ExchangeTypeViewModel, ExchangeType>
    {
        #region From
        public string FromCurrencyName { get; set; }
        public string FromCurrencyAlternativeName { get; set; }
        public string FromCurrencyUnitSign { get; set; }
        public int FromCurrencyId { get; set; }
        public decimal FromCurrencyMaximumAmount { get; set; }
        public decimal FromCurrencyMinimumAmount { get; set; }
        public decimal FromCurrencyStep { get; set; }
        public bool FromCurrencyDisabled { get; set; }
        public string FromCurrencyUrl { get; set; }
        public bool FromCurrencyHasMemo { get; set; }
        public string FromCurrencyMemoName { get; set; }
        public string FromCurrencyMemoRegEx { get; set; }
        public string FromCurrencyMemoDescription { get; set; }
        public ECurrencyType FromCurrencyType { get; set; }
        public int FromCurrencyConfirmsNeed { get; set; }
        #endregion

        #region To
        public string ToCurrencyName { get; set; }
        public string ToCurrencyAlternativeName { get; set; }
        public string ToCurrencyUnitSign { get; set; }
        public bool ToCurrencyIsFiat { get; set; }
        public int ToCurrencyId { get; set; }
        public decimal? ToCurrencyAvailable { get; set; }
        public decimal ToCurrencyMinimumAmount { get; set; }
        public decimal ToCurrencyMaximumAmount { get; set; }
        public decimal ToCurrencyStep { get; set; }
        public bool ToCurrencyDisabled { get; set; }
        public string ToCurrencyUrl { get; set; }
        public ECurrencyType ToCurrencyType { get; set; }
        public decimal? ToCurrencyTransactionFee { get; set; }

        #endregion

        #region Other
        public bool Disabled { get; set; }
        public decimal ExtraFeePercent { get; set; }
        public ExchangeFiatCoinType ExchangeFiatCoinType { get; set; }
        public MediaViewModel FromCurrencyMedia { get; set; }
        public MediaViewModel ToCurrencyMedia { get; set; }
        //public List<ExchangeTypeRateViewModel> ExchangeTypeRates { get; set; }
        public int UserExchangesCount { get; set; }
        #endregion
    }
}