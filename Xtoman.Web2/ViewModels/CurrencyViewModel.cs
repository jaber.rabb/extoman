﻿using System.Collections.Generic;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web2.ViewModels
{
    public class CurrencyViewModel : BaseViewModel<CurrencyViewModel, CurrencyType>
    {
        public bool IsFiat { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AlternativeName { get; set; }
        public string UnitSign { get; set; }
        public string ImageSrc { get; set; }
        public bool Disabled { get; set; }
        public decimal Min { get; set; }
        public decimal Step { get; set; }
        public string Url { get; set; }
        public decimal Max { get; set; }
        public ECurrencyType Type { get; set; }
        public decimal Price { get; set; }
        public decimal Change24 { get; set; }
    }

    public class CurrencySingleViewModel : BaseViewModel<CurrencySingleViewModel, CurrencyType>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string AlternativeName { get; set; }
        public string UnitSign { get; set; }
        public string ImageSrc { get; set; }
        public bool Disabled { get; set; }
        public decimal Min { get; set; }
        public decimal Step { get; set; }
        public string Url { get; set; }
        public decimal Max { get; set; }
        public bool IsFiat { get; set; }
        public ECurrencyType Type { get; set; }
        public List<ExchangeTypeViewModel> ExchangeTypesFrom { get; set; }
        public List<ExchangeTypeViewModel> ExchangeTypesTo { get; set; }
        public MediaViewModel Media { get; set; }
    }
}