﻿using System.Collections.Generic;
using Xtoman.Domain;

namespace Xtoman.Web2.ViewModels
{
    public class HomeViewModel
    {
        public int NewUsersCountPast24 { get; set; }
        public int OrdersCountPast24 { get; set; }
        public ExchangeTypeViewModel TopExchangePast24 { get; set; }
        public List<CurrencyViewModel> Currencies { get; set; }
        public List<BalanceItem> Balances { get; set; }
        public List<ReviewViewModel> Reviews { get; set; }
        public List<ShortPostViewModel> LatestPosts { get; set; }
    }
}