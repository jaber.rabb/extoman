﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain;
using Xtoman.Domain.Models;
using Xtoman.Framework;
using Xtoman.Service;
using Xtoman.Utility;
using Xtoman.Web2.ViewModels;

namespace Xtoman.Web2.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IPostService _postService;
        private readonly IOrderReviewService _reviewService;
        private readonly ICurrencyTypeService _currencyTypeService;
        private readonly IBalanceManager _balanceManager;
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly IUserService _userService;
        private readonly IExchangeTypeService _exchangeTypeService;

        public HomeController(
            IPostService postService,
            IOrderReviewService reviewService,
            ICurrencyTypeService currencyTypeService,
            IBalanceManager balanceManager,
            IExchangeOrderService exchangeOrderService,
            IUserService userService,
            IExchangeTypeService exchangeTypeService)
        {
            _postService = postService;
            _reviewService = reviewService;
            _currencyTypeService = currencyTypeService;
            _balanceManager = balanceManager;
            _exchangeOrderService = exchangeOrderService;
            _userService = userService;
            _exchangeTypeService = exchangeTypeService;
        }

        public async Task<ActionResult> Index()
        {
            var currencies = await _currencyTypeService.GetAllActiveCachedAsync();
            var currenciesModel = new List<CurrencyViewModel>();
            foreach (var currency in currencies)
                currenciesModel.Add(new CurrencyViewModel().FromEntity(currency));

            var past24 = DateTime.Now.AddDays(-1);
            var reviews = await _reviewService.TableNoTracking
                .Where(p => p.Text.Length > 15 && p.VoteRate >= 3)
                .OrderByDescending(p => p.Id).Take(9)
                .ProjectToListAsync<ReviewViewModel>();

            var past24OrdersCount = await _exchangeOrderService.TableNoTracking.Where(p => p.InsertDate >= past24 && p.PaymentStatus == PaymentStatus.Paid).CountAsync();
            var newUsersPast24 = await _userService.TableNoTracking.Where(p => p.RegDate >= past24).CountAsync();
            var topExchangePast24 = await _exchangeTypeService.TableNoTracking
                .Select(p => p.UserExchanges.Where(x => x.InsertDate >= past24 && x.PaymentStatus == PaymentStatus.Paid))
                .OrderByDescending(p => p.Count()).SelectMany(p => p.Select(x => x.Exchange)).ProjectToFirstOrDefaultAsync<ExchangeTypeViewModel>();

            var balances = AppSettingManager.IsLocale ? new List<BalanceItem>() {
                new BalanceItem() { Amount = 1.374845435m, Name = "Bitcoin", Type = ECurrencyType.Bitcoin },
                new BalanceItem() { Amount = 10.04632835m, Name = "Ethereum", Type = ECurrencyType.Ethereum },
                new BalanceItem() { Amount = 1000.936388m, Name = "Ripple", Type = ECurrencyType.Ripple },
                new BalanceItem() { Amount = 100000.4856m, Name = "Dogecoin", Type = ECurrencyType.Dogecoin },
            } : await _balanceManager.GetAll2Async();

            var now = DateTime.UtcNow;
            var model = new HomeViewModel()
            {
                Reviews = reviews,
                OrdersCountPast24 = past24OrdersCount,
                NewUsersCountPast24 = newUsersPast24,
                TopExchangePast24 = topExchangePast24,
                Balances = balances,
                Currencies = currenciesModel,
                LatestPosts = await _postService.TableNoTracking
                        .Where(p => p.PublishDate != null && p.PublishDate <= now)
                        .OrderByDescending(p => p.PublishDate)
                        .Take(3)
                        .ProjectToListAsync<ShortPostViewModel>()
            };
            return View(model);
        }
    }
}