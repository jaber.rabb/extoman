﻿using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain;
using Xtoman.Domain.Models;
using Xtoman.Framework;
using Xtoman.Framework.Mvc;
using Xtoman.Service;
using Xtoman.Utility;
using Xtoman.Web2.ViewModels;

namespace Xtoman.Web2.Controllers
{
    public class AccountController : BaseController
    {
        #region Properties
        private readonly IAppSignInManager _signInManager;
        private readonly IAppUserManager _userManager;
        private readonly IUserService _userService;
        #endregion

        #region Constructor
        public AccountController(
            IAppSignInManager signInManager,
            IAppUserManager userManager,
            IUserService userService)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _userService = userService;
        }
        #endregion

        #region Actions
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
                return View(model);

            if (!AppSettingManager.IsLocale)
            {
                var captchaResult = await HttpContext.ValidateCaptchaAsync("6LflMzoUAAAAACuYcwEmXd1jTfby3YKO5CDhC_h9");
                if (captchaResult != RecaptchaResponse.Success)
                {
                    ModelState.AddModelError("", "شما ربات هستید.");
                    return View(model);
                }
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var signInModel = new SignInModel2()
            {
                EmailOrPhoneOrUserName = model.EmailOrPhoneNumberOrUsername,
                Password = model.Password,
                IsPersistent = model.RememberMe,
                ShouldLockout = true,
                IPAddress = IPAddress,
                Browser = Browser,
                Subdomain = SubdomainType.Beta
            };
            var result = await _signInManager.EmailOrUsernameOrPhoneSignInAsync(signInModel);

            switch (result.signInStatus)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    var lockoutModel = new LockoutViewModel()
                    {
                        EndDate = await _userManager.GetLockoutEndDateAsync(result.user.Id)
                    };
                    return View("Lockout", lockoutModel);
                case SignInStatus.RequiresVerification:
                    var user2Factors = await _userManager.GetValidTwoFactorProvidersAsync(result.user.Id);
                    var provider = "";
                    if (user2Factors.Any(x => x.Equals("TelegramCode", System.StringComparison.InvariantCultureIgnoreCase)))// && result.user.TelegramChatId.HasValue())
                    {
                        // 1- Send Code via Telegram Bot

                        // 2- Go to VerifyPhoneCode Page 
                        provider = "TelegramCode";
                    }
                    else if (user2Factors.Any(x => x.Equals("GoogleAuthenticator", System.StringComparison.InvariantCultureIgnoreCase)))
                    {
                        // Go to VerifyGoogleAuthenticatorCode Page
                        provider = "GoogleAuthenticator";
                    }
                    else if (user2Factors.Any(x => x.Equals("PhoneCode")) && result.user.PhoneNumber.HasValue())
                    {
                        // 1- Send Code via SMS

                        // 2- Go to VerifyPhoneCode Page
                        provider = "PhoneCode";
                    }
                    else
                    {
                        // 3- Send Code via Email

                        // 3- Go to VerifyEmailCode Page
                        provider = "EmailCode";
                    }
                    return RedirectToAction("SendCode", new { Provider = provider, ReturnUrl = returnUrl, model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "کلمه عبور، ایمیل، موبایل یا نام کاربری وارد شده اشتباه است.");
                    return View(model);
            }
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await _signInManager.GetVerifiedUserIdAsync();
            if (userId == 0)
            {
                return HttpNotFound();
            }
            var userFactors = await _userManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            return View();
        }
        #endregion

        #region Helpers
        public async Task<int> GetUserIdByEmailOrPhoneOrUserNameAsync(string emailOrPhoneOrUserName)
        {
            var userId = await _userService.TableNoTracking.Where(p => p.Email == emailOrPhoneOrUserName
                                                               || p.UserName == emailOrPhoneOrUserName
                                                               || p.PhoneNumber == emailOrPhoneOrUserName)
                .Select(p => p.Id).SingleAsync();

            return userId;
        }
        #endregion
    }
}