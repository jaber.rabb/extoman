﻿using System.Web.Mvc;
using Xtoman.Framework;

namespace Xtoman.Web2.Controllers
{
    public class ErrorController : BaseController
    {
        public ActionResult Index()
        {
            return View("ServerError");
        }

        public ActionResult PageNotFound()
        {
            this.HttpContext.Response.StatusCode = 404;
            this.HttpContext.Response.TrySkipIisCustomErrors = true;
            return View();
        }

        public ActionResult ServerError()
        {
            this.HttpContext.Response.StatusCode = 500;
            this.HttpContext.Response.TrySkipIisCustomErrors = true;
            return View();
        }
    }
}