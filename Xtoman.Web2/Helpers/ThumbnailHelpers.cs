﻿using System.Collections.Generic;
using System.Linq;
using Xtoman.Domain.Models;
using Xtoman.Utility;
using Xtoman.Web2.ViewModels;

public static class ThumbnailHelpers
{
    public static string ImgSrc(this List<MediaThumbnailViewModel> thumbnails, string type, string defaultUrl = "")
    {
        var src = thumbnails?.Where(p => p.MediaThumbnailSizeName.Equals(type, System.StringComparison.InvariantCultureIgnoreCase)).Select(p => "https://static." + CommonUtility.Domain + p.Url.Replace("/static", "")).FirstOrDefault() ?? defaultUrl;
        return src;
    }

    public static string ImgSrc(this List<MediaThumbnailViewModel> thumbnails, string type, string subdomain, string defaultUrl = "")
    {
        var src = thumbnails?.Where(p => p.MediaThumbnailSizeName.Equals(type, System.StringComparison.InvariantCultureIgnoreCase)).Select(p => "https://static." + CommonUtility.Domain + p.Url.Replace("/static", "")).FirstOrDefault() ?? defaultUrl;
        return src;
    }

    public static string ImgSrc(this ICollection<MediaThumbnail> thumbnails, string type, string defaultUrl = "")
    {
        var src = thumbnails?.Where(p => p.MediaThumbnailSize.Name.Equals(type, System.StringComparison.InvariantCultureIgnoreCase)).Select(p => "https://static." + CommonUtility.Domain + p.Url.Replace("/static", "")).FirstOrDefault() ?? defaultUrl;
        return src;
    }

    public static string ImgSrc(this ICollection<MediaThumbnail> thumbnails, string type, string subdomain, string defaultUrl = "")
    {
        var src = thumbnails?.Where(p => p.MediaThumbnailSize.Name.Equals(type, System.StringComparison.InvariantCultureIgnoreCase)).Select(p => "https://static." + CommonUtility.Domain + p.Url.Replace("/static", "")).FirstOrDefault() ?? defaultUrl;
        return src;
    }
}