﻿using System;
using System.Web.Mvc;
using Xtoman.Utility;
public static class HtmlHelpers
{
    public static MvcHtmlString NavActionLink(this HtmlHelper htmlHelper, string text, string action, string controller, string iconClass, object routeValues = null)
    {
        var context = htmlHelper.ViewContext;
        if (context.Controller.ControllerContext.IsChildAction)
            context = htmlHelper.ViewContext.ParentActionViewContext;
        var currentRouteValues = context.RouteData.Values;
        var currentAction = currentRouteValues["action"].ToString();
        var currentController = currentRouteValues["controller"].ToString();
        var clss = "";
        if (currentAction.Equals(action, StringComparison.InvariantCultureIgnoreCase) &&
            currentController.Equals(controller, StringComparison.InvariantCultureIgnoreCase))
        {
            clss = " active";
        }
        clss = clss.Trim();
        var urlHelper = new UrlHelper(context.RequestContext);
        var aLink = String.Format("<a href=\"{0}\" class=\"nav-link {1}\">{2} {3}</a>",
            urlHelper.Action(action, controller, routeValues),
            clss,
            iconClass.HasValue() ? "<span class=\"" + iconClass + "\"></span>" : String.Empty,
            text);

        return new MvcHtmlString(aLink);
    }
}