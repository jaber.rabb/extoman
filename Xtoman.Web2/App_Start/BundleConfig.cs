﻿using System.Web;
using System.Web.Optimization;

namespace Xtoman.Web2
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/scripts/jquery").Include(
            //            "~/assets/js/jquery-3.4.1.min.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            #region Scripts
            bundles.Add(new ScriptBundle("~/scripts/main").Include(
                "~/assets/js/core.js",
                "~/assets/plugins/select2/js/select2.full.min.js",
                "~/assets/plugins/swiper/js/swiper.min.js",
                "~/assets/js/rater.min.js",
                "~/assets/js/main.js"));
            #endregion

            //bundles.Add(new ScriptBundle("~/scripts/main").Include(
            //          "~/assets/js/bootstrap.bundle.min.js",
            //          "~/assets/js/main.js"));

            bundles.Add(new StyleBundle("~/styles").Include(
                      //"~/assets/css/bootstrap.min.css",
                      "~/assets/css/fontiran.css",
                      "~/assets/css/fontawesome.css",
                      "~/assets/plugins/select2/css/select2.min.css",
                      "~/assets/plugins/swiper/css/swiper.min.css",
                      "~/assets/css/style.css"));
        }
    }
}
