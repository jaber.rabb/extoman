﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Xtoman.Web2.Startup))]
namespace Xtoman.Web2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
