﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Data
{
    public class RepositoryBase<T> : IRepository<T> where T : class, IBaseEntity
    {
        #region Fields

        private readonly IUnitOfWork _uow;
        private IDbSet<T> _entities;

        #endregion

        #region Properties
        /// <summary>
        /// Gets a table
        /// </summary>
        public virtual IQueryable<T> Table => Entities;

        /// <summary>
        /// Gets a table with "no tracking" enabled (EF feature) Use it only when you load record(s) only for read-only operations
        /// </summary>
        public virtual IQueryable<T> TableNoTracking => Entities.AsNoTracking();

        /// <summary>
        /// Entities
        /// </summary>
        public virtual IUnitOfWork Context => _uow;

        /// <summary>
        /// Entities
        /// </summary>
        public virtual IDbSet<T> Entities => _entities ?? (_entities = _uow.Set<T>());
        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="context">Object context</param>
        /// 
        public RepositoryBase(IUnitOfWork context)
        {
            _uow = context;
        }

        #endregion

        #region Methods
        public virtual T GetById(long id)
        {
            return Entities.Find(id);
        }

        public virtual T AddIfNotExists(Expression<Func<T, bool>> predicate, T entity)
        {
            var variable = Entities.FirstOrDefault(predicate);
            if (variable == null)
            {
                Insert(entity);
                return entity;
            }
            return variable;
        }

        public virtual void Insert(T entity)
        {
            Entities.Add(entity);
            _uow.SaveChanges();
        }

        public virtual void Insert(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
                Entities.Add(entity);
            _uow.SaveChanges();
        }

        public virtual void Update(T entity)
        {
            _uow.SaveChanges();
        }

        public virtual void Update(IEnumerable<T> entities)
        {
            _uow.SaveChanges();
        }

        public virtual void CleanUpdate(T entity)
        {
            _uow.CleanSaveChanges();
        }

        public virtual void CleanUpdate(IEnumerable<T> entities)
        {
            _uow.CleanSaveChanges();
        }

        public virtual void AddOrUpdate(params T[] entity)
        {
            Entities.AddOrUpdate(entity);
            _uow.SaveChanges();
        }

        public virtual void AddOrUpdate(Expression<Func<T, object>> identifierExpression, params T[] entity)
        {
            Entities.AddOrUpdate(identifierExpression, entity);
            _uow.SaveChanges();
        }

        public virtual bool Delete(T entity)
        {
            Entities.Remove(entity);
            return Convert.ToBoolean(_uow.SaveChanges());
        }

        public virtual bool Delete(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
                Entities.Remove(entity);
            return Convert.ToBoolean(_uow.SaveChanges());
        }

        public virtual bool Delete(int id)
        {
            try
            {
                var entity = Entities.Create();
                entity.Id = id;
                _uow.Entry(entity).State = EntityState.Deleted;
                _uow.ValidateOnSaveEnabled = false;
                return Convert.ToBoolean(_uow.SaveChanges());
            }
            catch
            {
                _uow.ValidateOnSaveEnabled = true;
                throw;
            }
            finally
            {
                _uow.ValidateOnSaveEnabled = true;
            }
        }
        #endregion

        #region Async Methods
        public virtual Task<T> GetByIdAsync(long id)
        {
            return Entities.FindAsync(id);
        }

        public virtual async Task<T> AddIfNotExistsAsync(Expression<Func<T, bool>> predicate, T entity)
        {
            var result = await AddIfNotExistsWithStatusAsync(predicate, entity);
            return result.Item1;
        }

        protected async Task<Tuple<T, bool>> AddIfNotExistsWithStatusAsync(Expression<Func<T, bool>> predicate, T entity)
        {
            var inserted = false;
            var variable = await Entities.FirstOrDefaultAsync(predicate);
            if (variable == null)
            {
                await InsertAsync(entity);
                inserted = true;
            }
            return new Tuple<T, bool>(variable ?? entity, inserted);
        }

        public virtual Task InsertAsync(T entity)
        {
            Entities.Add(entity);
            return _uow.SaveChangesAsync();
        }

        public virtual Task InsertAsync(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
                Entities.Add(entity);
            return _uow.SaveChangesAsync();
        }

        public virtual Task UpdateAsync(T entity)
        {
            return _uow.SaveChangesAsync();
        }

        public virtual Task UpdateAsync(IEnumerable<T> entities)
        {
            return _uow.SaveChangesAsync();
        }

        public virtual Task AddOrUpdateAsync(params T[] entity)
        {
            Entities.AddOrUpdate(entity);
            return _uow.SaveChangesAsync();
        }

        public virtual Task AddOrUpdateAsync(Expression<Func<T, object>> identifierExpression, params T[] entity)
        {
            Entities.AddOrUpdate(identifierExpression, entity);
            return _uow.SaveChangesAsync();
        }

        public virtual Task DeleteAsync(T entity)
        {
            Entities.Remove(entity);
            return _uow.SaveChangesAsync();
        }

        public virtual Task DeleteAsync(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
                Entities.Remove(entity);
            return _uow.SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(int id)
        {
            try
            {
                var entity = Entities.Create();
                entity.Id = id;
                _uow.Entry(entity).State = EntityState.Deleted;
                _uow.ValidateOnSaveEnabled = false;
                await _uow.SaveChangesAsync();
            }
            catch
            {
                _uow.ValidateOnSaveEnabled = true;
                throw;
            }
            finally
            {
                _uow.ValidateOnSaveEnabled = true;
            }
        }
        #endregion
    }
}
