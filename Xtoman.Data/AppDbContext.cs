﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using Xtoman.Data.Migration;
using Xtoman.Domain.Models;
using Xtoman.Utility;

namespace Xtoman.Data
{
    public class AppDbContext : IdentityDbContext<AppUser, AppRole, int, AppUserLogin, AppUserRole, AppUserClaim>, IUnitOfWork
    {
        #region Fields
        private static ConcurrentDictionary<Type, IEnumerable<string>> AuditFields;
        private static IEnumerable<string> IgnoreFields;
        private static IEnumerable<string> IgnoreTypes;
        private readonly Lazy<Func<IIdentity>> _identity;

        private class DynamicEntity
        {
            public int EntityId { get; set; }
            public string EntityName { get; set; }
            public string EntityField { get; set; }
            public string Value { get; set; }
            public int? LanguageId { get; set; }
            public string OriginalValue { get; set; }
            public string CurrentValue { get; set; }
            public EntityState State { get; set; }
        }
        #endregion

        #region Constractor
        public AppDbContext(Lazy<Func<IIdentity>>
            identity = null // For lazy loading -> Controller gets constructed before the HttpContext has been set by ASP.NET.
            )
            : this("XtomanDbConnection", identity)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AppDbContext, MigrationConfiguration>());
        }

        public AppDbContext(string nameOrConnectionString,
            Lazy<Func<IIdentity>> identity = null // For lazy loading -> Controller gets constructed before the HttpContext has been set by ASP.NET.
            )
            : base(nameOrConnectionString)
        {
            _identity = identity;
            IgnoreFields = typeof(AuditEntity).GetProperties().Select(p => p.Name).ToList();
            IgnoreTypes = ReflectionHelper.GetTypesAssignableFrom<IgnoreChangeLogAttribute>().Select(p => p.Name);
            if (AuditFields == null)
            {
                AuditFields = new ConcurrentDictionary<Type, IEnumerable<string>>();
                var baseType = typeof(IBaseEntity);

                var mappings = new ConcurrentDictionary<Type, MappingInfo>();
                var configTypes = baseType.Assembly.GetTypes().Where(type => ReflectionHelper.BaseTypeIsGeneric(type, typeof(EntityTypeConfiguration<>)));
                foreach (var type in configTypes)
                {
                    var configInstance = Activator.CreateInstance(type);
                    var mapping = ReflectionHelper.GetMappingInfo(configInstance);
                    mappings.TryAdd(type.BaseType.GetGenericArguments().ElementAt(0), mapping);
                }
                //var q = mappings.Keys.OrderBy(p => p.Name).ToList();

                var types = baseType.Assembly.GetTypes().Where(type => type.BaseType != null && baseType.IsAssignableFrom(type));
                foreach (var type in types)
                {
                    var properties = type.GetProperties()
                        .Where(p => !ReflectionHelper.IsEnumerable(p.PropertyType) && !ReflectionHelper.IsUserDefinedClass(p.PropertyType))
                        .Where(p =>
                        {
                            if (p.Name.Equals("Id", StringComparison.OrdinalIgnoreCase))
                                return false;
                            if (typeof(AuditEntity).IsAssignableFrom(type) && IgnoreFields.Contains(p.Name))
                                return false;
                            if (!mappings.TryGetValue(type, out MappingInfo mapping) || mapping.IgnoredProperties.Contains(p.Name))
                                return false;
                            if (p.GetCustomAttributes(typeof(IgnoreChangeLogAttribute), false).Length > 0)
                                return false;
                            return true;
                        }).Select(p => p.Name).ToList();

                    AuditFields.TryAdd(type, properties);
                }
            }

            //((IObjectContextAdapter) this).ObjectContext.ContextOptions.LazyLoadingEnabled = true;
        }
        #endregion

        #region DbSets
        public DbSet<ChangeLog> ChangeLogs { get; set; }
        public DbSet<LocalizedProperty> LocalizedProperties { get; set; }
        #endregion

        #region Methods
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Configurations.AddFromAssembly(typeof(LocaleStringResourceConfig).Assembly);
            base.OnModelCreating(modelBuilder);
            #region dynamically load all configuration
            //var configType = typeof(LanguageMap);   //any of your configuration classes here
            //var typesToRegister = Assembly.GetAssembly(configType).GetTypes()
            //.Where(type => !String.IsNullOrEmpty(type.Namespace))
            //.Where(type => type.BaseType != null && type.BaseType.IsGenericType &&
            //    type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
            //foreach (var type in typesToRegister)
            //{
            //    dynamic configurationInstance = Activator.CreateInstance(type);
            //    modelBuilder.Configurations.Add(configurationInstance);
            //}
            #endregion
        }

        public void MarkAsChanged<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            Entry(entity).State = EntityState.Modified;
        }

        public void MarkAsDeleted<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            Entry(entity).State = EntityState.Deleted;
        }

        public IEnumerable<TEntity> AddThisRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IBaseEntity
        {
            return ((DbSet<TEntity>)Set<TEntity>()).AddRange(entities);
        }

        public void ForceDatabaseInitialize()
        {
            Database.Initialize(true);
        }

        public new DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            return base.Entry(entity);
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class, IBaseEntity
        {
            return base.Set<TEntity>();
        }

        public new int SaveChanges()
        {
            return _doOperation(() =>
            {
                CleanString();
                AuditEntity();
                RemoveLocalizedProrperties();
                var changeLogs = SaveChangeLogs ? ChangeLogEntity() : null;

                var result = base.SaveChanges();
                if (changeLogs != null)
                {
                    changeLogs.ForEach(p =>
                    {
                        if (p.Key != null)
                            p.Value.PrimaryKeyValue = p.Key.Id;
                    });
                    ChangeLogs.AddRange(changeLogs.Select(p => p.Value));
                    base.SaveChanges();
                }
                return result;
            });
        }

        public int CleanSaveChanges()
        {
            return _doOperation(() =>
            {
                CleanString();
                RemoveLocalizedProrperties();
                return base.SaveChanges();
            });
        }

        public new Task<int> SaveChangesAsync()
        {
            return SaveChangesAsync(CancellationToken.None);
        }

        public new Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return _doOperation(async () =>
            {
                CleanString();
                AuditEntity();
                RemoveLocalizedProrperties();
                var changeLogs = SaveChangeLogs ? ChangeLogEntity() : null;

                var result = await base.SaveChangesAsync(cancellationToken);
                if (changeLogs != null)
                {
                    changeLogs.ForEach(p =>
                    {
                        if (p.Key != null)
                            p.Value.PrimaryKeyValue = p.Key.Id;
                    });
                    ChangeLogs.AddRange(changeLogs.Select(p => p.Value));
                    await base.SaveChangesAsync(cancellationToken);
                }
                return result;
            });
        }

        /// <summary>
        /// Execute stores procedure and load a list of entities at the end
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="commandText">Command text</param>
        /// <param name="parameters">Parameters</param>
        /// <returns>Entities</returns>
        public IList<TEntity> ExecuteStoredProcedureList<TEntity>(string commandText, params object[] parameters) where TEntity : class, IBaseEntity, new()
        {
            //add parameters to command
            if (parameters?.Length > 0)
            {
                for (int i = 0; i <= parameters.Length - 1; i++)
                {
                    var p = parameters[i] as DbParameter;
                    if (p == null)
                        throw new Exception("Parameter type not supported!");

                    commandText += i == 0 ? " " : ", ";

                    commandText += "@" + p.ParameterName;
                    if (p.Direction == ParameterDirection.InputOutput || p.Direction == ParameterDirection.Output)
                    {
                        //output parameter
                        commandText += " output";
                    }
                }
            }

            var result = Database.SqlQuery<TEntity>(commandText, parameters).ToList();

            //performance hack applied as described here - http://www.nopcommerce.com/boards/t/25483/fix-very-important-speed-improvement.aspx
            bool AutoDetectChanges = Configuration.AutoDetectChangesEnabled;
            try
            {
                Configuration.AutoDetectChangesEnabled = false;

                for (int i = 0; i < result.Count; i++)
                    result[i] = Attach(result[i]);
            }
            finally
            {
                Configuration.AutoDetectChangesEnabled = AutoDetectChanges;
            }

            return result;
        }

        /// <summary>
        /// Creates a raw SQL query that will return elements of the given generic type.  The type can be any type that has properties that match the names of the columns returned from the query, or can be a simple primitive type. The type does not have to be an entity type. The results of this query are never tracked by the context even if the type of object returned is an entity type.
        /// </summary>
        /// <typeparam name="TElement">The type of object returned by the query.</typeparam>
        /// <param name="sql">The SQL query string.</param>
        /// <param name="parameters">The parameters to apply to the SQL query string.</param>
        /// <returns>Result</returns>
        public IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters)
        {
            return Database.SqlQuery<TElement>(sql, parameters);
        }

        /// <summary>
        /// Executes the given DDL/DML command against the database.
        /// </summary>
        /// <param name="sql">The command string</param>
        /// <param name="doNotEnsureTransaction">false - the transaction creation is not ensured; true - the transaction creation is ensured.</param>
        /// <param name="timeout">Timeout value, in seconds. A null value indicates that the default value of the underlying provider will be used</param>
        /// <param name="parameters">The parameters to apply to the command string.</param>
        /// <returns>The result returned by the database after executing the command.</returns>
        public async Task<int> ExecuteSqlCommandAsync(string sql, bool doNotEnsureTransaction = false, int? timeout = null, params object[] parameters)
        {
            if (string.IsNullOrEmpty(sql))
                return 0;

            int? previousTimeout = null;
            if (timeout.HasValue)
            {
                //store previous timeout
                previousTimeout = ((IObjectContextAdapter)this).ObjectContext.CommandTimeout;
                ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = timeout;
            }

            var transactionalBehavior = doNotEnsureTransaction
                ? TransactionalBehavior.DoNotEnsureTransaction
                : TransactionalBehavior.EnsureTransaction;
            var result = await Database.ExecuteSqlCommandAsync(transactionalBehavior, sql, parameters);

            if (timeout.HasValue)
            {
                //Set previous timeout back
                ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = previousTimeout;
            }

            //return result
            return result;
        }

        public void Detach<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            //Entry(entity).State = EntityState.Detached;
            ((IObjectContextAdapter)this).ObjectContext.Detach(entity);
        }

        /// <summary>
        /// Attach an entity to the context or return an already attached entity (if it was already attached)
        /// </summary>
        /// <typeparam name="TEntity">TEntity</typeparam>
        /// <param name="entity">Entity</param>
        /// <returns>Attached entity</returns>
        public virtual TEntity Attach<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            //little hack here until Entity Framework really supports stored procedures
            //otherwise, navigation properties of loaded entities are not loaded until an entity is attached to the context
            var alreadyAttached = Set<TEntity>().Local.FirstOrDefault(x => x.Id == entity.Id);
            if (alreadyAttached == null)
            {
                //attach new entity
                Set<TEntity>().Attach(entity);
                return entity;
            }

            //entity is already loaded
            return alreadyAttached;
        }

        public void SaveChangesBulk()
        {
            _doOperation(() =>
            {
                CleanString();
                AuditEntity();
                RemoveLocalizedProrperties();
                this.BulkSaveChanges();
                return true;
            });
        }

        public async Task UpdateBulkAsync<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IBaseEntity
        {
            await _doOperation(async () =>
            {
                await this.BulkUpdateAsync(entities);
                return true;
            });
        }

        public async Task DeleteBulkAsync<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IBaseEntity
        {
            await _doOperation(async () =>
            {
                await this.BulkDeleteAsync(entities);
                return true;
            });
        }

        public async Task SaveChangesBulkAsync()
        {
            await _doOperation(async () =>
            {
                CleanString();
                AuditEntity();
                RemoveLocalizedProrperties();
                await this.BulkSaveChangesAsync();
                return true;
            });
        }
        #endregion

        #region Private Methods
        private List<KeyValuePair<IBaseEntity, ChangeLog>> ChangeLogEntity()
        {
            try
            {
                var identity = _identity?.Value();
                var userId = identity?.IsAuthenticated == true ? identity.GetUserId<int>() : (int?)null;
                if (!userId.HasValue || userId == 0)
                    userId = 1;

                var changeLogs = new List<KeyValuePair<IBaseEntity, ChangeLog>>();
                var now = DateTime.Now;
                var dynamicEntities = new List<DynamicEntity>();
                foreach (var item in ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
                {
                    var baseEntity = item.Entity as IBaseEntity;
                    if (baseEntity == null)
                        continue;
                    var type = baseEntity.GetUnproxiedEntityType();
                    if (IgnoreTypes.Contains(type.Name))
                        continue;

                    #region Collecting Dynamic Entities such as LocalizedProperty or UrlHistory
                    if (item.Entity is LocalizedProperty)
                    {
                        var entity = item.Cast<LocalizedProperty>();
                        var originalValue = entity.State == EntityState.Modified ? entity.Property(p => p.LocaleValue).OriginalValue : null;
                        var currentValue = entity.State != EntityState.Deleted ? entity.Property(p => p.LocaleValue).CurrentValue : null;
                        dynamicEntities.Add(new DynamicEntity
                        {
                            EntityId = entity.Entity.EntityId,
                            EntityName = entity.Entity.LocaleKeyGroup,
                            EntityField = entity.Entity.LocaleKey,
                            LanguageId = entity.Entity.LanguageId,
                            Value = entity.Entity.LocaleValue,
                            OriginalValue = originalValue,
                            CurrentValue = currentValue,
                            State = entity.State
                        });
                        continue;
                    }
                    if (item.Entity is UrlHistory)
                    {
                        var entity = item.Cast<UrlHistory>();
                        var originalValue = entity.State == EntityState.Modified ? entity.Property(p => p.Url).OriginalValue : null;
                        var currentValue = entity.State != EntityState.Deleted ? entity.Property(p => p.Url).CurrentValue : null;
                        dynamicEntities.Add(new DynamicEntity
                        {
                            EntityId = entity.Entity.EntityId,
                            EntityName = entity.Entity.EntityName,
                            EntityField = "Url",
                            LanguageId = entity.Entity.LanguageId,
                            Value = entity.Entity.Url,
                            OriginalValue = originalValue,
                            CurrentValue = currentValue,
                            State = entity.State
                        });
                        continue;
                    }
                    #endregion

                    #region Add ChangeLogs

                    var log = new ChangeLog()
                    {
                        EntityName = type.Name,
                        PrimaryKeyValue = baseEntity.Id,
                        DateChanged = now,
                        UserId = userId
                    };

                    if (item.State == EntityState.Deleted)
                    {
                        log.State = ChangeLogState.HardDeleted;
                        changeLogs.Add(new KeyValuePair<IBaseEntity, ChangeLog>(null, log));
                        continue;
                    }
                    else if (item.State == EntityState.Added)
                    {
                        log.State = ChangeLogState.Added;
                        foreach (var propName in AuditFields[type])
                        {
                            var currentValue = item.CurrentValues[propName]?.ToString();
                            log.Details.Add(new ChangeLogDetail
                            {
                                PropertyName = propName,
                                NewValue = currentValue
                            });
                        }
                        changeLogs.Add(new KeyValuePair<IBaseEntity, ChangeLog>(baseEntity, log));
                    }
                    else //Modified
                    {
                        var isSoftDelete = (item.Entity as AuditEntity)?.IsDelete ?? false;
                        log.State = isSoftDelete ? ChangeLogState.SoftDeleted : ChangeLogState.Modified;
                        foreach (var propName in AuditFields[type])
                        {
                            //var prop = item.Property(propName);
                            //if (prop.IsModified)
                            var originalValue = item.OriginalValues[propName]?.ToString();
                            var currentValue = item.CurrentValues[propName]?.ToString();
                            if (originalValue != currentValue)
                            {
                                log.Details.Add(new ChangeLogDetail
                                {
                                    PropertyName = propName,
                                    OldValue = originalValue,
                                    NewValue = currentValue
                                });
                            }
                        }
                        changeLogs.Add(new KeyValuePair<IBaseEntity, ChangeLog>(null, log));
                    }
                    #endregion
                }
                #region Save ChangeLogs of Dynamic Entities
                foreach (var groupByName in dynamicEntities.GroupBy(p => p.EntityName))
                {
                    foreach (var groupById in groupByName.GroupBy(p => p.EntityId))
                    {
                        foreach (var groupByLanguage in groupById.GroupBy(p => p.LanguageId))
                        {
                            var log = new ChangeLog()
                            {
                                EntityName = groupByName.Key,
                                PrimaryKeyValue = groupById.Key,
                                LanguageId = groupByLanguage.Key,
                                DateChanged = now,
                                UserId = userId,
                                State = ChangeLogState.Modified
                            };

                            if (groupByLanguage.All(x => x.State == EntityState.Deleted))
                            {
                                log.State = ChangeLogState.HardDeleted;
                                changeLogs.Add(new KeyValuePair<IBaseEntity, ChangeLog>(null, log));
                                continue;
                            }
                            else if (groupByLanguage.All(x => x.State == EntityState.Added))
                            {
                                log.State = ChangeLogState.Added;
                            }

                            foreach (var item in groupByLanguage)
                            {
                                log.Details.Add(new ChangeLogDetail
                                {
                                    OldValue = item.OriginalValue,
                                    NewValue = item.CurrentValue,
                                    PropertyName = item.EntityField
                                });
                            }
                            changeLogs.Add(new KeyValuePair<IBaseEntity, ChangeLog>(null, log));
                        }
                    }
                }
                #endregion

                return changeLogs;
            }
            catch (Exception) {}
            return null;
        }

        private void AuditEntity()
        {
            IIdentity identity = null;
            try
            {
                identity = _identity != null ? _identity?.Value() : null;
            }
            catch (Exception) {}
            try
            {
                var userId = identity != null && identity?.IsAuthenticated == true ? identity.GetUserId<int>() : (int?)null;
                if (!userId.HasValue || userId == 0)
                    userId = 1;
                var now = DateTime.UtcNow;
                foreach (var item in ChangeTracker.Entries<AuditEntity>().Where(p => p.State == EntityState.Added || p.State == EntityState.Modified))
                {
                    var entity = item.Entity;// as ItemEntity;
                                             //if (entity == null)
                                             //    continue;
                    switch (item.State)
                    {
                        case EntityState.Added:
                            if (!entity.InsertUserId.HasValue)
                                entity.InsertUserId = userId;
                            entity.InsertDate = now;
                            break;
                        case EntityState.Modified:
                            if (item.Entity.IsDelete)
                            {
                                if (!entity.DeleteUserId.HasValue)
                                    entity.DeleteUserId = userId;
                                entity.DeleteDate = now;
                            }
                            else
                            {
                                if (!entity.UpdateUserId.HasValue)
                                    entity.UpdateUserId = userId;
                                entity.UpdateDate = now;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
        }

        private void RemoveLocalizedProrperties()
        {
            foreach (var item in ChangeTracker.Entries<ILocalizedEntity>().Where(p => p.State == EntityState.Deleted))
            {
                var entity = item.Entity as IBaseEntity;
                var entityName = entity.GetUnproxiedEntityType().Name;
                var locales = LocalizedProperties.Where(p => p.LocaleKeyGroup == entityName && p.EntityId == entity.Id).ToList();
                LocalizedProperties.RemoveRange(locales);
            }
        }

        private void CleanString()
        {
            var changedEntities = ChangeTracker.Entries().Where(x => x.State == EntityState.Added || x.State == EntityState.Modified);
            foreach (var item in changedEntities)
            {
                if (item.Entity == null)
                    continue;
                var propertyInfos = item.Entity.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.CanRead && p.CanWrite && p.PropertyType == typeof(string));
                //var pr = new PropertyReflector();
                foreach (var propertyInfo in propertyInfos)
                {
                    var propName = propertyInfo.Name;
                    var val = (string)propertyInfo.GetValue(item.Entity, null); //pr.GetValue(item.Entity, propName);
                    if (!string.IsNullOrWhiteSpace(val))
                    {
                        var newVal = Utility.StringHelper.Fa2En(Utility.StringHelper.FixPersianChars(val));
                        if (newVal == val)
                            continue;
                        propertyInfo.SetValue(item.Entity, newVal, null);// pr.SetValue(item.Entity, propName, newVal);
                    }
                }
            }
        }

        private T _doOperation<T>(Func<T> func)
        {
            try
            {
                return func();
            }
            catch (DbEntityValidationException e)
            {
                var errors = ValidationHelper.GetValidationErrors(e);
                throw new DbEntityValidationException(string.Join("; ", errors), e.EntityValidationErrors);
            }
        }

        private async Task<T> _doOperation<T>(Func<Task<T>> func)
        {
            try
            {
                return await func();
            }
            catch (DbEntityValidationException e)
            {
                var errors = ValidationHelper.GetValidationErrors(e);
                throw new DbEntityValidationException(string.Join("; ", errors), e.EntityValidationErrors);
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether proxy creation setting is enabled (used in EF)
        /// </summary>
        public virtual bool ProxyCreationEnabled
        {
            get
            {
                return Configuration.ProxyCreationEnabled;
            }
            set
            {
                Configuration.ProxyCreationEnabled = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether auto detect changes setting is enabled (used in EF)
        /// </summary>
        public virtual bool AutoDetectChangesEnabled
        {
            get
            {
                return Configuration.AutoDetectChangesEnabled;
            }
            set
            {
                Configuration.AutoDetectChangesEnabled = value;
            }
        }

        /// <summary>
        /// Gets or sets a value LazyLoadingEnabled
        /// </summary>
        public virtual bool LazyLoadingEnabled
        {
            get
            {
                return Configuration.LazyLoadingEnabled;
            }
            set
            {
                Configuration.LazyLoadingEnabled = value;
            }
        }

        /// <summary>
        /// Gets or sets a value ValidateOnSaveEnabled
        /// </summary>
        public virtual bool ValidateOnSaveEnabled
        {
            get
            {
                return Configuration.ValidateOnSaveEnabled;
            }
            set
            {
                Configuration.ValidateOnSaveEnabled = value;
            }
        }

        public virtual bool SaveChangeLogs { get; set; } = true;
        #endregion
    }

}