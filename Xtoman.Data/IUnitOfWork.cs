﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Data
{
    //public interface IUnitOfWork : IDisposable
    //{
    //    void MarkAsChanged<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
    //    void MarkAsDeleted<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
    //    IEnumerable<TEntity> AddThisRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IBaseEntity;
    //    void ForceDatabaseInitialize();
    //    TEntity Attach<TEntity>(TEntity entity) where TEntity : class, IBaseEntity, new();

    //    /// <summary>
    //    /// Get Entry
    //    /// </summary>
    //    /// <typeparam name="TEntity">Entity type</typeparam>
    //    /// <param name="entity">entity</param>
    //    /// <returns>DbSet</returns>
    //    DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;

    //    /// <summary>
    //    /// Get DbSet
    //    /// </summary>
    //    /// <typeparam name="TEntity">Entity type</typeparam>
    //    /// <returns>DbSet</returns>
    //    IDbSet<TEntity> Set<TEntity>() where TEntity : class, IBaseEntity;
    //    /// <summary>
    //    /// Execute stores procedure and load a list of entities at the end
    //    /// </summary>
    //    /// <typeparam name="TEntity">Entity type</typeparam>
    //    /// <param name="commandText">Command text</param>
    //    /// <param name="parameters">Parameters</param>
    //    /// <returns>Entities</returns>
    //    IList<TEntity> ExecuteStoredProcedureList<TEntity>(string commandText, params object[] parameters) where TEntity : class, IBaseEntity, new();

    //    /// <summary>
    //    /// Creates a raw SQL query that will return elements of the given generic type.  The type can be any type that has properties that match the names of the columns returned from the query, or can be a simple primitive type. The type does not have to be an entity type. The results of this query are never tracked by the context even if the type of object returned is an entity type.
    //    /// </summary>
    //    /// <typeparam name="TElement">The type of object returned by the query.</typeparam>
    //    /// <param name="sql">The SQL query string.</param>
    //    /// <param name="parameters">The parameters to apply to the SQL query string.</param>
    //    /// <returns>Result</returns>
    //    IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters);

    //    /// <summary>
    //    /// Executes the given DDL/DML command against the database.
    //    /// </summary>
    //    /// <param name="sql">The command string</param>
    //    /// <param name="doNotEnsureTransaction">false - the transaction creation is not ensured; true - the transaction creation is ensured.</param>
    //    /// <param name="timeout">Timeout value, in seconds. A null value indicates that the default value of the underlying provider will be used</param>
    //    /// <param name="parameters">The parameters to apply to the command string.</param>
    //    /// <returns>The result returned by the database after executing the command.</returns>
    //    int ExecuteSqlCommand(string sql, bool doNotEnsureTransaction = false, int? timeout = null, params object[] parameters);

    //    /// <summary>
    //    /// Save changes
    //    /// </summary>
    //    /// <returns></returns>
    //    int SaveChanges();

    //    /// <summary>
    //    /// Save changes async
    //    /// </summary>
    //    /// <returns></returns>
    //    Task<int> SaveChangesAsync();

    //    /// <summary>
    //    /// Detach an entity
    //    /// </summary>
    //    /// <param name="entity">Entity</param>
    //    void Detach<TEntity>(TEntity entity) where TEntity : class, IBaseEntity, new();

    //    /// <summary>
    //    /// Gets or sets a value indicating whether proxy creation setting is enabled (used in EF)
    //    /// </summary>
    //    bool ProxyCreationEnabled { get; set; }

    //    /// <summary>
    //    /// Gets or sets a value indicating whether auto detect changes setting is enabled (used in EF)
    //    /// </summary>
    //    bool AutoDetectChangesEnabled { get; set; }

    //    /// <summary>
    //    /// Gets or sets a value LazyLoadingEnabled
    //    /// </summary>
    //    bool LazyLoadingEnabled { get; set; }

    //    /// <summary>
    //    /// Gets or sets a value ValidateOnSaveEnabled
    //    /// </summary>
    //    bool ValidateOnSaveEnabled { get; set; }
    //    bool SaveChangeLogs { get; set; }

    //    void SaveChangesBulk();
    //    void UpdateBulk<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IBaseEntity;
    //    void DeleteBulk<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IBaseEntity;

    //    Task SaveChangesBulkAsync();
    //}
    public interface IUnitOfWork : IDisposable
    {
        void MarkAsChanged<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
        void MarkAsDeleted<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
        IEnumerable<TEntity> AddThisRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IBaseEntity;
        void ForceDatabaseInitialize();

        /// <summary>
        /// Attach an entity
        /// </summary>
        /// <param name="entity">Entity</param>
        TEntity Attach<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;

        /// <summary>
        /// Detach an entity
        /// </summary>
        /// <param name="entity">Entity</param>
        void Detach<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;

        /// <summary>
        /// Get Entry
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="entity">entity</param>
        /// <returns>DbSet</returns>
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;

        /// <summary>
        /// Get DbSet
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <returns>DbSet</returns>
        IDbSet<TEntity> Set<TEntity>() where TEntity : class, IBaseEntity;

        /// <summary>
        /// Execute stores procedure and load a list of entities at the end
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="commandText">Command text</param>
        /// <param name="parameters">Parameters</param>
        /// <returns>Entities</returns>
        IList<TEntity> ExecuteStoredProcedureList<TEntity>(string commandText, params object[] parameters) where TEntity : class, IBaseEntity, new();

        /// <summary>
        /// Creates a raw SQL query that will return elements of the given generic type.  The type can be any type that has properties that match the names of the columns returned from the query, or can be a simple primitive type. The type does not have to be an entity type. The results of this query are never tracked by the context even if the type of object returned is an entity type.
        /// </summary>
        /// <typeparam name="TElement">The type of object returned by the query.</typeparam>
        /// <param name="sql">The SQL query string.</param>
        /// <param name="parameters">The parameters to apply to the SQL query string.</param>
        /// <returns>Result</returns>
        IEnumerable<TElement> SqlQuery<TElement>(string query, params object[] parameters);

        /// <summary>
        /// Executes the given DDL/DML command against the database.
        /// </summary>
        /// <param name="sql">The command string</param>
        /// <param name="doNotEnsureTransaction">false - the transaction creation is not ensured; true - the transaction creation is ensured.</param>
        /// <param name="timeout">Timeout value, in seconds. A null value indicates that the default value of the underlying provider will be used</param>
        /// <param name="parameters">The parameters to apply to the command string.</param>
        /// <returns>The result returned by the database after executing the command.</returns>
        Task<int> ExecuteSqlCommandAsync(string sql, bool doNotEnsureTransaction = false, int? timeout = null, params object[] parameters);

        /// <summary>
        /// Save changes
        /// </summary>
        /// <returns></returns>
        int SaveChanges();

        /// <summary>
        /// Save changes without audit and change log
        /// </summary>
        /// <returns></returns>
        int CleanSaveChanges();

        /// <summary>
        /// Save changes async
        /// </summary>
        /// <returns></returns>
        Task<int> SaveChangesAsync();

        /// <summary>
        /// Gets or sets a value indicating whether proxy creation setting is enabled (used in EF)
        /// </summary>
        bool ProxyCreationEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether auto detect changes setting is enabled (used in EF)
        /// </summary>
        bool AutoDetectChangesEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value LazyLoadingEnabled
        /// </summary>
        bool LazyLoadingEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value ValidateOnSaveEnabled
        /// </summary>
        bool ValidateOnSaveEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value SaveChangeLogs
        /// </summary>
        bool SaveChangeLogs { get; set; }

        void SaveChangesBulk();
        Task UpdateBulkAsync<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IBaseEntity;
        Task DeleteBulkAsync<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IBaseEntity;
        Task SaveChangesBulkAsync();
    }
}
