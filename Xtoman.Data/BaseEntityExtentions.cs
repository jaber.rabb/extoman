﻿using System;
using System.Data.Entity.Core.Objects;
using Xtoman.Domain.Models;

namespace Xtoman.Data
{
    public static class BaseEntityExtentions
    {
        public static Type GetUnproxiedEntityType(this IBaseEntity entity)
        {
            var userType = ObjectContext.GetObjectType(entity.GetType());
            return userType;
        }
    }
}
