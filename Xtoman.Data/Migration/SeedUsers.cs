﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using Xtoman.Domain.Models;

namespace Xtoman.Data.Migration
{
    public static class SeedUsers
    {
        public static void Execute(AppDbContext context)
        {
            var UserManager = new UserManager<AppUser, int>(new UserStore<AppUser, AppRole, int, AppUserLogin, AppUserRole, AppUserClaim>(context));
            IdentityResult roleResult, /*contractorRoleResult,*/ userResult, addToRoleResult;
            roleResult = AddNewRole("Admin", context);
            //contractorRoleResult = AddNewRole("Contractor", context);
            var adminUser = new AppUser
            {
                UserName = "MZiii",
                FirstName = "Mehdi",
                LastName = "Zamanian",
                PhoneNumber = "+989136588879",
                BirthDate = new DateTime(1989, 6, 4),
                Email = "zamanian.work@gmail.com",
                Gender = true,
                EmailConfirmed = true,
            };
            var mziii = UserManager.FindByName("MZiii");
            if (mziii == null)
                userResult = UserManager.Create(adminUser, "3ploverap");

            var mziiiId = UserManager.FindByName("MZiii").Id;
            if (!UserManager.IsInRole(mziiiId, "Admin"))
                addToRoleResult = UserManager.AddToRole(mziiiId, "Admin");

            //var contractorUser = new AppUser
            //{
            //    UserName = "m.amirhossein@gmail.com",
            //    FirstName = "Amirhossein",
            //    LastName = "Moradi",
            //    Email = "m.amirhossein@gmail.com",
            //    EmailConfirmed = true
            //};

            //var amirMoradi = UserManager.FindByName("m.amirhossein@gmail.com");
            //if (amirMoradi == null)
            //    userResult = UserManager.Create(amirMoradi, "123456");

            //var amirMoradiId = UserManager.FindByName("m.amirhossein@gmail.com").Id;
            //if (!UserManager.IsInRole(amirMoradiId, "Contractor"))
            //    addToRoleResult = UserManager.AddToRole(amirMoradiId, "Contractor");
        }
        private static IdentityResult AddNewRole(string roleName, AppDbContext context)
        {
            var RoleManager = new RoleManager<AppRole, int>(new RoleStore<AppRole, int, AppUserRole>(context));
            if (!RoleManager.RoleExists(roleName))
            {
                return RoleManager.Create(new AppRole(roleName));
            }
            return new IdentityResult("Exist");
        }
    }
}
