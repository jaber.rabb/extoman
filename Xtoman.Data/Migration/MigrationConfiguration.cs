﻿using System.Data.Entity.Migrations;

namespace Xtoman.Data.Migration
{
    public class MigrationConfiguration : DbMigrationsConfiguration<AppDbContext>
    {
        public MigrationConfiguration()
        {
            AutomaticMigrationDataLossAllowed = true;
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(AppDbContext context)
        {
            //SeedCurrencyTypes.Execute(context);
            //SeedDiscounts.Execute(context);
            //SeedCommissions.Execute(context);
            //SeedMediaThumbnailSizes.Execute(context);
            //SeedUsers.Execute(context);
            //SeedExchangeTypes.Execute(context);
            //SeedFAQs.Execute(context);
            //SeedEuroDollarCurrency.Execute(context);
            //SeedEuroDollarExchangeType.Execute(context);
            base.Seed(context);
        }
    }
}
