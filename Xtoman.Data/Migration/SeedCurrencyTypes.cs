﻿using System.Data.Entity.Migrations;
using Xtoman.Domain.Models;

namespace Xtoman.Data.Migration
{
    public static class SeedCurrencyTypes
    {
        public static void Execute(AppDbContext context)
        {
            var currencyTypesDbSet = context.Set<CurrencyType>();
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "شتاب",
            //    AlternativeName = "Shetab",
            //    Available = 5000000000,
            //    MinimumReceiveAmount = 200000,
            //    Type = ECurrencyType.Shetab,
            //    UnitSign = "تومان",
            //    UnitSignIsAfter = true,
            //    Url = "شتاب-تومان-ریال",
            //    IsFiat = true,
            //    MaxPrecision = 0,
            //    HasMemo = true,
            //    MemoName = "شماره شبا",
            //    MemoDescription = "<b>توجه</b><p>در یکی از قسمت های بالا <b>نام بانک</b> خود را نیز بنویسید. برای تسریع در واریز ترجیحا از بانک ملت استفاده نمایید.</p>",
            //    TransactionFee = 500
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "بیت کوین",
            //    AlternativeName = "Bitcoin",
            //    MinimumReceiveAmount = 0.01M,
            //    Type = ECurrencyType.Bitcoin,
            //    UnitSign = "BTC",
            //    UnitSignIsAfter = true,
            //    Url = "بیت-کوین",
            //    Step = 0.0001M,
            //    CoinMarketCapId = "bitcoin",
            //    ChangellyName = "btc",
            //    MaxPrecision = 8,
            //    AddressRegEx = "^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$",
            //    TransactionFee = 0.0005M,
            //    ConfirmsNeed = 2
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "پرفکت مانی",
            //    AlternativeName = "PerfectMoney",
            //    MinimumReceiveAmount = 1,
            //    Type = ECurrencyType.PerfectMoney,
            //    UnitSign = "PM USD",
            //    UnitSignIsAfter = true,
            //    Url = "پرفکت-مانی",
            //    IsFiat = true,
            //    MaxPrecision = 2,
            //    Disabled = true,
            //    TransactionFee = 1.99M
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "وب مانی",
            //    AlternativeName = "WebMoney",
            //    MinimumReceiveAmount = 1,
            //    Type = ECurrencyType.WebMoney,
            //    UnitSign = "WM USD",
            //    UnitSignIsAfter = true,
            //    Url = "وب-مانی",
            //    IsFiat = true,
            //    MaxPrecision = 2,
            //    Disabled = true
            //});
            currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            {
                Name = "چینلینک",
                AlternativeName = "Chainlink",
                MinimumReceiveAmount = 0.1M,
                Type = ECurrencyType.Chainlink,
                UnitSign = "LINK",
                UnitSignIsAfter = true,
                Url = "چینلینک",
                Step = 0.01M,
                CoinMarketCapId = "chainlink",
                MaxPrecision = 8,
                AddressRegEx = "^0x[a-fA-F0-9]{40}$",
                TransactionFee = 0.01M,
                ConfirmsNeed = 3,
                IsERC20 = true
            });
            currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            {
                Name = "بیسیک اتنشن توکن",
                AlternativeName = "Basic Attention Token",
                MinimumReceiveAmount = 0.1M,
                Type = ECurrencyType.BasicAttentionToken,
                UnitSign = "BAT",
                UnitSignIsAfter = true,
                Url = "بیسیک-اتنشن-توکن",
                Step = 0.01M,
                CoinMarketCapId = "basic-attention-token",
                MaxPrecision = 8,
                AddressRegEx = "^0x[a-fA-F0-9]{40}$",
                TransactionFee = 0.01M,
                ConfirmsNeed = 3,
                IsERC20 = true
            });
            currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            {
                Name = "اومیسه گو",
                AlternativeName = "OmiseGO",
                MinimumReceiveAmount = 0.1M,
                Type = ECurrencyType.OmiseGO,
                UnitSign = "LINK",
                UnitSignIsAfter = true,
                Url = "اومیسه-گو",
                Step = 0.01M,
                CoinMarketCapId = "omisego",
                MaxPrecision = 8,
                AddressRegEx = "^0x[a-fA-F0-9]{40}$",
                TransactionFee = 0.01M,
                ConfirmsNeed = 3,
                IsERC20 = true
            });
            currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            {
                Name = "زیرو اکس",
                AlternativeName = "0x",
                MinimumReceiveAmount = 0.1M,
                Type = ECurrencyType.ZeroX,
                UnitSign = "ZRX",
                UnitSignIsAfter = true,
                Url = "زیرو-اکس",
                Step = 0.01M,
                CoinMarketCapId = "0x",
                MaxPrecision = 8,
                AddressRegEx = "^0x[a-fA-F0-9]{40}$",
                TransactionFee = 0.01M,
                ConfirmsNeed = 3,
                IsERC20 = true
            });
            currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            {
                Name = "آی او اس توکن",
                AlternativeName = "IOST",
                MinimumReceiveAmount = 0.1M,
                Type = ECurrencyType.IOST,
                UnitSign = "IOST",
                UnitSignIsAfter = true,
                Url = "آی-او-اس-توکن",
                Step = 0.01M,
                CoinMarketCapId = "iostoken",
                MaxPrecision = 8,
                AddressRegEx = "^0x[a-fA-F0-9]{40}$",
                TransactionFee = 0.01M,
                ConfirmsNeed = 3,
                IsERC20 = true
            });
            currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            {
                Name = "انجین کوین",
                AlternativeName = "Enjin Coin",
                MinimumReceiveAmount = 0.1M,
                Type = ECurrencyType.EnjinCoin,
                UnitSign = "ENJ",
                UnitSignIsAfter = true,
                Url = "انجین-کوین",
                Step = 0.01M,
                CoinMarketCapId = "enjin-coin",
                MaxPrecision = 8,
                AddressRegEx = "^0x[a-fA-F0-9]{40}$",
                TransactionFee = 0.01M,
                ConfirmsNeed = 3,
                IsERC20 = true
            });
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "اتریوم کلاسیک",
            //    AlternativeName = "Ethereum Classic",
            //    MinimumReceiveAmount = 2,
            //    Type = ECurrencyType.EthereumClassic,
            //    UnitSign = "ETC",
            //    UnitSignIsAfter = true,
            //    Url = "اتریوم-کلاسیک",
            //    Step = 0.1M,
            //    CoinMarketCapId = "ethereum-classic",
            //    ChangellyName = "etc",
            //    MaxPrecision = 8,
            //    AddressRegEx = "^0x[a-fA-F0-9]{40}$",
            //    TransactionFee = 0.01M,
            //    ConfirmsNeed = 3
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "لایت کوین",
            //    AlternativeName = "Litecoin",
            //    MinimumReceiveAmount = 0.5M,
            //    Type = ECurrencyType.Litecoin,
            //    UnitSign = "LTC",
            //    UnitSignIsAfter = true,
            //    Url = "لایت-کوین",
            //    Step = 0.1M,
            //    CoinMarketCapId = "litecoin",
            //    ChangellyName = "ltc",
            //    MaxPrecision = 8,
            //    AddressRegEx = "^[LM3][a-km-zA-HJ-NP-Z1-9]{26,33}$",
            //    TransactionFee = 0.01M,
            //    ConfirmsNeed = 3
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "مونرو",
            //    AlternativeName = "Monero",
            //    MinimumReceiveAmount = 0.5M,
            //    Type = ECurrencyType.Monero,
            //    UnitSign = "XMR",
            //    UnitSignIsAfter = true,
            //    Url = "مونرو",
            //    Step = 0.01M,
            //    CoinMarketCapId = "monero",
            //    ChangellyName = "xmr",
            //    MaxPrecision = 8,
            //    AddressRegEx = "^4([0-9]|[A-B])(.){93}|^4([0-9]|[A-Z])(.){104}",
            //    HasMemo = true,
            //    MemoName = "Payment ID",
            //    MemoRegEx = "^([0-9a-fA-F]{16}|[0-9a-fA-F]{64})",
            //    MemoDescription = "<b>Payment ID چیست؟</b><p>در صورتی که برای کیف پول خود Payment ID انتخاب نموده اید باید آن را اینجا وارد نمایید در غیر این صورت خالی بگذارید.</p>",
            //    TransactionFee = 0.04M,
            //    ConfirmsNeed = 3
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "نئو",
            //    AlternativeName = "NEO",
            //    MinimumReceiveAmount = 1,
            //    Type = ECurrencyType.NEO,
            //    UnitSign = "NEO",
            //    UnitSignIsAfter = true,
            //    Url = "نئو",
            //    Step = 1,
            //    CoinMarketCapId = "neo",
            //    ChangellyName = "neo",
            //    MaxPrecision = 0,
            //    AddressRegEx = "^[A][a-km-zA-HJ-NP-Z1-9]{33}",
            //    TransactionFee = 0M, //TODO: Fix fee
            //    ConfirmsNeed = 10
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "زدکش",
            //    AlternativeName = "ZCash",
            //    MinimumReceiveAmount = 0.1M,
            //    Type = ECurrencyType.Zcash,
            //    UnitSign = "ZEC",
            //    UnitSignIsAfter = true,
            //    Url = "زدکش",
            //    Step = 0.01M,
            //    CoinMarketCapId = "zcash",
            //    ChangellyName = "zec",
            //    MaxPrecision = 8,
            //    TransactionFee = 0.005M,
            //    ConfirmsNeed = 3
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "بیت کوین کش",
            //    AlternativeName = "Bitcoin Cash",
            //    MinimumReceiveAmount = 0.05M,
            //    Type = ECurrencyType.BitcoinCash,
            //    UnitSign = "BCH",
            //    UnitSignIsAfter = true,
            //    Url = "بیت-کوین-کش",
            //    Step = 0.001M,
            //    CoinMarketCapId = "bitcoin-cash",
            //    ChangellyName = "bch",
            //    MaxPrecision = 8,
            //    TransactionFee = 0.001M,
            //    ConfirmsNeed = 3
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "دش",
            //    AlternativeName = "Dash",
            //    MinimumReceiveAmount = 0.05M,
            //    Type = ECurrencyType.Dash,
            //    UnitSign = "DASH",
            //    UnitSignIsAfter = true,
            //    Url = "دش",
            //    Step = 0.001M,
            //    CoinMarketCapId = "dash",
            //    ChangellyName = "dsh",
            //    MaxPrecision = 8,
            //    AddressRegEx = "^X[1-9A-HJ-NP-Za-km-z]{33}",
            //    TransactionFee = 0.002M,
            //    ConfirmsNeed = 3
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "پییر",
            //    AlternativeName = "PAYEER",
            //    MinimumReceiveAmount = 2,
            //    Type = ECurrencyType.PAYEER,
            //    UnitSign = "PAYEER USD",
            //    UnitSignIsAfter = true,
            //    Url = "پییر",
            //    IsFiat = true,
            //    MaxPrecision = 2,
            //    Disabled = true
            //});

            ////New Currency Types -> 1/18/2018
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "ریپل",
            //    AlternativeName = "Ripple",
            //    MinimumReceiveAmount = 30,
            //    Type = ECurrencyType.Ripple,
            //    UnitSign = "XRP",
            //    UnitSignIsAfter = true,
            //    Url = "ریپل",
            //    Step = 1,
            //    CoinMarketCapId = "ripple",
            //    ChangellyName = "xrp",
            //    MaxPrecision = 8,
            //    AddressRegEx = "^([r])([1-9A-HJ-NP-Za-km-z]{24,34})$",
            //    HasMemo = true,
            //    MemoName = "Destination Tag",
            //    MemoDescription = "<b>Destination Tag چیست؟</b><p>بعضی از کیف پول های ریپل دارای Destination Tag می باشد. در صورتی که کیف پول ریپل شما Destination Tag داشته باشد باید آن را اینجا وارد نمایید در غیر این صورت خالی بگذارید.</p><b>نکته بسیار مهم</b><p>در صورتی که به اشتباه Destination Tag را وارد نمایید تراکنش شما با مشکل مواجه می شود و مقدار ریپل شما از دست می رود و قابل بازگشت نیست.</p>",
            //    TransactionFee = 0.25M,
            //    ConfirmsNeed = 30
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "کاردانو",
            //    AlternativeName = "Cardano",
            //    MinimumReceiveAmount = 200,
            //    Type = ECurrencyType.Cardano,
            //    UnitSign = "ADA",
            //    UnitSignIsAfter = true,
            //    Url = "کاردانو",
            //    Step = 1,
            //    CoinMarketCapId = "cardano",
            //    ChangellyName = "ada",
            //    MaxPrecision = 8,
            //    HasMemo = false,
            //    TransactionFee = 1
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "کوانتوم",
            //    AlternativeName = "QTUM",
            //    MinimumReceiveAmount = 5,
            //    Type = ECurrencyType.QTUM,
            //    UnitSign = "QTUM",
            //    UnitSignIsAfter = true,
            //    Url = "کوانتوم",
            //    Step = 1,
            //    CoinMarketCapId = "qtum",
            //    ChangellyName = "qtum",
            //    MaxPrecision = 8,
            //    HasMemo = false,
            //    TransactionFee = 0.01M,
            //    ConfirmsNeed = 6
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "بیت کوین گلد",
            //    AlternativeName = "Bitcoin Gold",
            //    MinimumReceiveAmount = 0.5M,
            //    Type = ECurrencyType.BitcoinGold,
            //    UnitSign = "BTG",
            //    UnitSignIsAfter = true,
            //    Url = "بیت-کوین-گلد",
            //    Step = 0.1M,
            //    CoinMarketCapId = "bitcoin-gold",
            //    ChangellyName = "btg",
            //    MaxPrecision = 8,
            //    HasMemo = false,
            //    TransactionFee = 0.001M
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "آرک",
            //    AlternativeName = "Ark",
            //    MinimumReceiveAmount = 20,
            //    Type = ECurrencyType.Ark,
            //    UnitSign = "ARK",
            //    UnitSignIsAfter = true,
            //    Url = "آرک",
            //    Step = 1,
            //    CoinMarketCapId = "ark",
            //    ChangellyName = "ark",
            //    MaxPrecision = 8,
            //    HasMemo = false,
            //    TransactionFee = 0, //TODO: Fix fee
            //    //ConfirmsNeed = 
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "آی او تا",
            //    AlternativeName = "IOTA",
            //    MinimumReceiveAmount = 20,
            //    Type = ECurrencyType.IOTA,
            //    UnitSign = "IOTA",
            //    UnitSignIsAfter = true,
            //    Url = "آی-او-تا",
            //    Step = 1,
            //    CoinMarketCapId = "iota",
            //    ChangellyName = "iota",
            //    MaxPrecision = 8,
            //    HasMemo = false,
            //    TransactionFee = 0.5M,
            //    //ConfirmsNeed = 
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "استلار",
            //    AlternativeName = "Stellar",
            //    MinimumReceiveAmount = 150,
            //    Type = ECurrencyType.Stellar,
            //    UnitSign = "XLM",
            //    UnitSignIsAfter = true,
            //    Url = "استلار",
            //    Step = 1,
            //    CoinMarketCapId = "stellar",
            //    ChangellyName = "stellar",
            //    MaxPrecision = 8,
            //    TransactionFee = 0.01M,
            //    HasMemo = true,
            //    MemoName = "MEMO",
            //    MemoDescription = "<b>MEMO چیست؟</b><p>بعضی از کیف پول های استلار دارای MEMO می باشد. در صورتی که کیف پول استلار شما MEMO داشته باشد باید آن را اینجا وارد نمایید در غیر این صورت خالی بگذارید.</p><b>نکته بسیار مهم</b><p>در صورتی که به اشتباه MEMO را وارد نمایید تراکنش شما با مشکل مواجه می شود و مقدار استلار شما از دست می رود و قابل بازگشت نیست.</p>",
            //    AddressRegEx = "^[a-zA-Z0-9]{56}$",
            //    //ConfirmsNeed = 
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "لیسک",
            //    AlternativeName = "Lisk",
            //    MinimumReceiveAmount = 2.5M,
            //    Type = ECurrencyType.Lisk,
            //    UnitSign = "LSK",
            //    UnitSignIsAfter = true,
            //    Url = "لیسک",
            //    Step = 0.1M,
            //    CoinMarketCapId = "lisk",
            //    ChangellyName = "lisk",
            //    MaxPrecision = 8,
            //    HasMemo = false,
            //    TransactionFee = 0.1M,
            //    ConfirmsNeed = 10
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "نانو",
            //    AlternativeName = "Nano",
            //    MinimumReceiveAmount = 4,
            //    Type = ECurrencyType.Nano,
            //    UnitSign = "NANO",
            //    UnitSignIsAfter = true,
            //    Url = "نانو",
            //    Step = 0.1M,
            //    CoinMarketCapId = "nano",
            //    ChangellyName = "nano",
            //    MaxPrecision = 8,
            //    HasMemo = false,
            //    TransactionFee = 0.01M,
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "ورج",
            //    AlternativeName = "Verge",
            //    MinimumReceiveAmount = 850,
            //    Type = ECurrencyType.Verge,
            //    UnitSign = "XVG",
            //    UnitSignIsAfter = true,
            //    Url = "ورج",
            //    Step = 10,
            //    CoinMarketCapId = "verge",
            //    ChangellyName = "verge",
            //    MaxPrecision = 8,
            //    HasMemo = false,
            //    TransactionFee = 0.1M
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "استراتیس",
            //    AlternativeName = "Stratis",
            //    MinimumReceiveAmount = 5,
            //    Type = ECurrencyType.Stratis,
            //    UnitSign = "STRAT",
            //    UnitSignIsAfter = true,
            //    Url = "استراتیس",
            //    Step = 0.01M,
            //    CoinMarketCapId = "stratis",
            //    ChangellyName = "stratis",
            //    MaxPrecision = 8,
            //    HasMemo = false,
            //    TransactionFee = 0.1M
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "استیم",
            //    AlternativeName = "Steem",
            //    MinimumReceiveAmount = 15,
            //    Type = ECurrencyType.Steem,
            //    UnitSign = "STEEM",
            //    UnitSignIsAfter = true,
            //    Url = "استیم",
            //    Step = 0.01M,
            //    CoinMarketCapId = "steem",
            //    ChangellyName = "steem",
            //    MaxPrecision = 8,
            //    TransactionFee = 0.01M,
            //    HasMemo = true,
            //    MemoName = "MEMO",
            //    MemoDescription = "<b>MEMO چیست؟</b><p>بعضی از کیف پول های استیم دارای MEMO می باشد. در صورتی که کیف پول استیم شما MEMO داشته باشد باید آن را اینجا وارد نمایید در غیر این صورت خالی بگذارید.</p><b>نکته بسیار مهم</b><p>در صورتی که به اشتباه MEMO را وارد نمایید تراکنش شما با مشکل مواجه می شود و مقدار استیم شما از دست می رود و قابل بازگشت نیست.</p>",
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "ویوز",
            //    AlternativeName = "Waves",
            //    MinimumReceiveAmount = 7,
            //    Type = ECurrencyType.Waves,
            //    UnitSign = "WAVES",
            //    UnitSignIsAfter = true,
            //    Url = "ویوز",
            //    Step = 0.1M,
            //    CoinMarketCapId = "waves",
            //    ChangellyName = "waves",
            //    MaxPrecision = 8,
            //    HasMemo = false,
            //    TransactionFee = 0.002M
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "کومودو",
            //    AlternativeName = "Komodo",
            //    MinimumReceiveAmount = 7,
            //    Type = ECurrencyType.Komodo,
            //    UnitSign = "KMD",
            //    UnitSignIsAfter = true,
            //    Url = "کومودو",
            //    Step = 0.1M,
            //    CoinMarketCapId = "komodo",
            //    ChangellyName = "komodo",
            //    MaxPrecision = 8,
            //    HasMemo = false,
            //    TransactionFee = 0.002M
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "بیت شیرز",
            //    AlternativeName = "BitShares",
            //    MinimumReceiveAmount = 200,
            //    Type = ECurrencyType.BitShares,
            //    UnitSign = "BTS",
            //    UnitSignIsAfter = true,
            //    Url = "بیت-شیرز",
            //    Step = 1,
            //    CoinMarketCapId = "bitshares",
            //    ChangellyName = "bitshares",
            //    MaxPrecision = 8,
            //    TransactionFee = 1,
            //    HasMemo = true,
            //    MemoName = "MEMO",
            //    MemoDescription = "<b>MEMO چیست؟</b><p>بعضی از کیف پول های بیت شیرز دارای MEMO می باشد. در صورتی که کیف پول بیت شیرز شما MEMO داشته باشد باید آن را اینجا وارد نمایید در غیر این صورت خالی بگذارید.</p><b>نکته بسیار مهم</b><p>در صورتی که به اشتباه MEMO را وارد نمایید تراکنش شما با مشکل مواجه می شود و مقدار بیت شیرز شما از دست می رود و قابل بازگشت نیست.</p>",
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "نبلیو",
            //    AlternativeName = "Neblio",
            //    MinimumReceiveAmount = 4,
            //    Type = ECurrencyType.Neblio,
            //    UnitSign = "NEBL",
            //    UnitSignIsAfter = true,
            //    Url = "نبلیو",
            //    Step = 0.1M,
            //    CoinMarketCapId = "neblio",
            //    ChangellyName = "neblio",
            //    MaxPrecision = 8,
            //    TransactionFee = 1,
            //    HasMemo = false
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "نو کوین",
            //    AlternativeName = "NAV Coin",
            //    MinimumReceiveAmount = 30,
            //    Type = ECurrencyType.NAVCoin,
            //    UnitSign = "NAV",
            //    UnitSignIsAfter = true,
            //    Url = "نو-کوین",
            //    Step = 1,
            //    CoinMarketCapId = "nav-coin",
            //    ChangellyName = "nav",
            //    MaxPrecision = 8,
            //    TransactionFee = 0.2M,
            //    HasMemo = false
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "پیویکس",
            //    AlternativeName = "PIVX",
            //    MinimumReceiveAmount = 10,
            //    Type = ECurrencyType.PIVX,
            //    UnitSign = "PIVX",
            //    UnitSignIsAfter = true,
            //    Url = "پیویکس",
            //    Step = 0.1M,
            //    CoinMarketCapId = "pivx",
            //    ChangellyName = "pixv",
            //    MaxPrecision = 8,
            //    TransactionFee = 0.02M,
            //    HasMemo = false
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "تتر",
            //    AlternativeName = "Tether USDT",
            //    MinimumReceiveAmount = 50,
            //    Type = ECurrencyType.Tether,
            //    UnitSign = "USDT",
            //    UnitSignIsAfter = true,
            //    Url = "تتر",
            //    Step = 1,
            //    CoinMarketCapId = "tether",
            //    ChangellyName = "tether",
            //    MaxPrecision = 8,
            //    TransactionFee = 8.5M,
            //    HasMemo = false
            //});
            //currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            //{
            //    Name = "ان ای ام",
            //    AlternativeName = "NEM",
            //    MinimumReceiveAmount = 130,
            //    Type = ECurrencyType.NEM,
            //    UnitSign = "XEM",
            //    UnitSignIsAfter = true,
            //    Url = "ان-ای-ام",
            //    Step = 1,
            //    CoinMarketCapId = "nem",
            //    ChangellyName = "nem",
            //    MaxPrecision = 8,
            //    TransactionFee = 4,
            //    HasMemo = true,
            //    MemoName = "Message",
            //    MemoDescription = "<b>Message چیست؟</b><p>بعضی از کیف پول های ان ای ام دارای Message می باشد. در صورتی که کیف پول ان ای ام شما Message داشته باشد باید آن را اینجا وارد نمایید در غیر این صورت خالی بگذارید.</p><b>نکته بسیار مهم</b><p>در صورتی که به اشتباه Message را وارد نمایید تراکنش شما با مشکل مواجه می شود و مقدار ان ای ام شما از دست می رود و قابل بازگشت نیست.</p>",
            //});

            //New Currency Types -> 6/12/2019
            currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            {
                Name = "ای او اس",
                AlternativeName = "EOS",
                MinimumReceiveAmount = 3,
                Type = ECurrencyType.EOS,
                UnitSign = "EOS",
                UnitSignIsAfter = true,
                Url = "ای-او-اس",
                Step = 0.1m,
                CoinMarketCapId = "eos",
                ChangellyName = "eos",
                MaxPrecision = 4,
                TransactionFee = 0.1m,
                HasMemo = true,
                MemoName = "Memo",
                MemoDescription = "<b>Message چیست؟</b><p>بعضی از کیف پول های ای او اس دارای Memo می باشد. در صورتی که کیف پول ای او اس شما Memo داشته باشد باید آن را اینجا وارد نمایید در غیر این صورت خالی بگذارید.</p><b>نکته بسیار مهم</b><p>در صورتی که به اشتباه Memo را وارد نمایید تراکنش شما با مشکل مواجه می شود و مقدار ای او اس شما از دست می رود و قابل بازگشت نیست.</p>",
            });

            currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            {
                Name = "هوریزن",
                AlternativeName = "Horizen",
                MinimumReceiveAmount = 2,
                Type = ECurrencyType.Horizen,
                UnitSign = "ZEN",
                UnitSignIsAfter = true,
                Url = "هوریزن",
                Step = 0.1m,
                CoinMarketCapId = "zencash",
                ChangellyName = "zen",
                MaxPrecision = 8,
                TransactionFee = 0.002m,
                HasMemo = false,
            });

            currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            {
                Name = "زد کوین",
                AlternativeName = "ZCoin",
                MinimumReceiveAmount = 2,
                Type = ECurrencyType.ZCoin,
                UnitSign = "XZC",
                UnitSignIsAfter = true,
                Url = "زد-کوین",
                Step = 0.1m,
                CoinMarketCapId = "zcoin",
                ChangellyName = "zcoin",
                MaxPrecision = 8,
                TransactionFee = 0.02m,
                HasMemo = false,
            });

            currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            {
                Name = "ریون کوین",
                AlternativeName = "Ravencoin",
                MinimumReceiveAmount = 250,
                Type = ECurrencyType.Ravencoin,
                UnitSign = "RVN",
                UnitSignIsAfter = true,
                Url = "ریون-کوین",
                Step = 1,
                CoinMarketCapId = "ravencoin",
                ChangellyName = "ravencoin",
                MaxPrecision = 8,
                TransactionFee = 0.02m,
                HasMemo = false
            });

            context.SaveChanges();
        }
    }
}
