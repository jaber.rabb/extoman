﻿using System.Data.Entity.Migrations;
using System.Linq;
using Xtoman.Domain.Models;

namespace Xtoman.Data.Migration
{
    public class SeedEuroDollarCurrency
    {
        public static void Execute(AppDbContext context)
        {
            var currencyTypesDbSet = context.Set<CurrencyType>();

            #region CurrencyTypes
            currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            {
                Name = "دلار",
                AlternativeName = "US Dollar",
                Available = 0,
                MinimumReceiveAmount = 0,
                Type = ECurrencyType.USDollar,
                UnitSign = "دلار",
                UnitSignIsAfter = true,
                Url = "دلار",
                IsFiat = true,
                MaxPrecision = 0,
                Invisible = true
            });

            currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            {
                Name = "یورو",
                AlternativeName = "Euro",
                Available = 0,
                MinimumReceiveAmount = 0,
                Type = ECurrencyType.Euro,
                UnitSign = "یورو",
                UnitSignIsAfter = true,
                Url = "یورو",
                IsFiat = true,
                MaxPrecision = 0,
                Invisible = true
            });

            currencyTypesDbSet.AddOrUpdate(p => p.Name, new CurrencyType()
            {
                Name = "درهم",
                AlternativeName = "درهم",
                Available = 0,
                MinimumReceiveAmount = 0,
                Type = ECurrencyType.Dirham,
                UnitSign = "درهم",
                UnitSignIsAfter = true,
                Url = "درهم",
                IsFiat = true,
                MaxPrecision = 0,
                Invisible = true
            });

            context.SaveChanges();
            #endregion
        }
    }

    public class SeedEuroDollarExchangeType
    {
        public static void Execute(AppDbContext context)
        {
            var currencyTypesDbSet = context.Set<CurrencyType>();
            var shetabId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.Shetab).Select(p => p.Id).FirstOrDefault();
            var dollarId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.USDollar).Select(p => p.Id).FirstOrDefault();
            var euroId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.Euro).Select(p => p.Id).FirstOrDefault();
            var dirhamId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.Dirham).Select(p => p.Id).FirstOrDefault();

            #region ExchangeTypes
            var exchangeTypesDbSet = context.Set<ExchangeType>();

            #region From Toman
            //Dollar
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = dollarId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham,
                Invisible = true,
                ExtraFeePercent = 2
            });

            //Euro
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = euroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham,
                Invisible = true,
                ExtraFeePercent = 2
            });

            //Dirham
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = dirhamId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham,
                Invisible = true,
                ExtraFeePercent = 2
            });
            #endregion

            #region From US Dollar
            //Toman
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dollarId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman,
                Invisible = true,
                ExtraFeePercent = 2
            });

            //Euro
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dollarId,
                ToCurrencyId = euroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham,
                Invisible = true,
                ExtraFeePercent = 2
            });

            //Dirham
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dollarId,
                ToCurrencyId = dirhamId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham,
                Invisible = true,
                ExtraFeePercent = 2
            });
            #endregion

            #region From Euro
            //Toman
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = euroId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman,
                Invisible = true,
                ExtraFeePercent = 2
            });

            //US Dolalr
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = euroId,
                ToCurrencyId = dollarId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham,
                Invisible = true
            });

            //Dirham
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = euroId,
                ToCurrencyId = dirhamId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham,
                Invisible = true
            });
            #endregion

            #region From Dirham
            //Toman
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dirhamId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman,
                Invisible = true,
                ExtraFeePercent = 2
            });

            //US Dolalr
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dirhamId,
                ToCurrencyId = dollarId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham,
                Invisible = true
            });

            //Euro
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dirhamId,
                ToCurrencyId = euroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham,
                Invisible = true
            });
            #endregion

            context.SaveChanges();
            #endregion
        }
    }

}
