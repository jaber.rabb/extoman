﻿using System.Data.Entity.Migrations;
using Xtoman.Domain.Models;

namespace Xtoman.Data.Migration
{
    public static class SeedMediaThumbnailSizes
    {
        public static void Execute(AppDbContext context)
        {
            var mediaDbSet = context.Set<MediaThumbnailSize>();
            mediaDbSet.AddOrUpdate(p => p.Name, new MediaThumbnailSize()
            {
                Name = "post.small",
                Width = 150,
                Height = 100,
                Crop = true,
                Watermark = false,
            });
            mediaDbSet.AddOrUpdate(p => p.Name, new MediaThumbnailSize()
            {
                Name = "post.medium",
                Width = 466,
                Height = 231,
                Crop = true,
                Watermark = false
            });
            mediaDbSet.AddOrUpdate(p => p.Name, new MediaThumbnailSize()
            {
                Name = "post.large",
                Width = 850,
                Height = 420,
                Crop = true,
                Watermark = false
            });
            mediaDbSet.AddOrUpdate(p => p.Name, new MediaThumbnailSize()
            {
                Name = "verify.large",
                Width = 850,
                Height = 420,
                Crop = false,
                Watermark = false
            });
            context.SaveChanges();
        }
    }
}
