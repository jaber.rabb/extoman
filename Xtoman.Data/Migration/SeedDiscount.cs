﻿using System.Data.Entity.Migrations;
using Xtoman.Domain.Models;

namespace Xtoman.Data.Migration
{
    public static class SeedDiscounts
    {
        public static void Execute(AppDbContext context)
        {
            var discountDbSet = context.Set<Discount>();
            discountDbSet.AddOrUpdate(p => p.Name, new Discount() {
                Name = "مشتری جدید",
                MinValue = 0,
                Percent = 0
            });
            discountDbSet.AddOrUpdate(p => p.Name, new Discount()
            {
                Name = "مشتری ابتدایی",
                MinValue = 5000000, // Toman
                Percent = 2.5M
            });
            discountDbSet.AddOrUpdate(p => p.Name, new Discount()
            {
                Name = "مشتری جوان",
                MinValue = 25000000, // Toman
                Percent = 5
            });
            discountDbSet.AddOrUpdate(p => p.Name, new Discount()
            {
                Name = "مشتری پیشرفته",
                MinValue = 50000000, // Toman
                Percent = 7.5M
            });
            discountDbSet.AddOrUpdate(p => p.Name, new Discount()
            {
                Name = "مشتری ویژه",
                MinValue = 250000000, // Toman
                Percent = 10
            });
        }
    }
}
