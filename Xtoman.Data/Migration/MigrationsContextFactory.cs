﻿using System.Data.Entity.Infrastructure;

namespace Xtoman.Data.Migration
{
    public class MigrationsContextFactory : IDbContextFactory<AppDbContext>
    {
        public AppDbContext Create()
        {
            return new AppDbContext(null);
        }
    }
}
