﻿using System.Data.Entity.Migrations;
using System.Linq;
using Xtoman.Domain.Models;

namespace Xtoman.Data.Migration
{
    public class SeedExchangeTypes
    {
        public static void Execute(AppDbContext context)
        {
            var currencyTypesDbSet = context.Set<CurrencyType>();
            var exchangeTypesDbSet = context.Set<ExchangeType>();

            //fiats
            var shetabId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.Shetab).Select(p => p.Id).FirstOrDefault();
            var payeerId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.PAYEER).Select(p => p.Id).FirstOrDefault();
            var webMoneyId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.WebMoney).Select(p => p.Id).FirstOrDefault();
            var perfectMoneyId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.PerfectMoney).Select(p => p.Id).FirstOrDefault();

            //coins
            var moneroId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.Monero).Select(p => p.Id).FirstOrDefault();
            var bitcoinId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.Bitcoin).Select(p => p.Id).FirstOrDefault();
            var ethereumId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.Ethereum).Select(p => p.Id).FirstOrDefault();
            var litecoinId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.Litecoin).Select(p => p.Id).FirstOrDefault();
            var ethereumClassicId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.EthereumClassic).Select(p => p.Id).FirstOrDefault();

            //new added coins
            var neoId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.NEO).Select(p => p.Id).FirstOrDefault();
            var dashId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.Dash).Select(p => p.Id).FirstOrDefault();
            var zcashId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.Zcash).Select(p => p.Id).FirstOrDefault();
            var bitcoinCashId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.BitcoinCash).Select(p => p.Id).FirstOrDefault();

            //newest added coins
            var rippleId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.Ripple).Select(p => p.Id).FirstOrDefault();
            var cardanoId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.Cardano).Select(p => p.Id).FirstOrDefault();
            var qtumId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.QTUM).Select(p => p.Id).FirstOrDefault();
            var bitcoinGoldId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.BitcoinGold).Select(p => p.Id).FirstOrDefault();
            var arkId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.Ark).Select(p => p.Id).FirstOrDefault();
            var iotaId = currencyTypesDbSet.Where(p => p.Type == ECurrencyType.IOTA).Select(p => p.Id).FirstOrDefault();

            //System.IO.File.WriteAllText(@"C:\CurrencyTypes.txt", $"Bitcoin: {bitcoinId}\n Ethereum: {ethereumId}\n Ethereum Classic: {ethereumClassicId}\n Shetab: {shetabId}\n WebMoney: {webMoneyId}\n PerfectMoney: {perfectMoneyId}\n PAYEER: {payeerId}");

            #region From Shetab
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 18
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 28
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 28
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham,
                ExtraFeePercent = 25
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham,
                ExtraFeePercent = 28
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham,
                ExtraFeePercent = 25
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 27
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 27
            });
            //Neo
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 25
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 25
            });
            //Zcash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 25
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 25
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 25
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 25
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 25
            });
            //Bitcoin Gold
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 25
            });
            //Ark
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 25
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = shetabId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatTomanToCoin,
                ExtraFeePercent = 25
            });
            #endregion

            #region From Bitcoin
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman,
                ExtraFeePercent = -5
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 25
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 25
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 25
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Neo
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Zcash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ark
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From PerfectMoney
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman,
                ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham,
                ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham,
                ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Neo
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Zcash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = perfectMoneyId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From WebMoney
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman, ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham, ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham, ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Neo
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Zcash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = webMoneyId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From PAYEER
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman, ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham, ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham, ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Neo
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Zcash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin, ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //Ark
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = payeerId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.FiatUSDToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From Ethereum Classic
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman, ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Neo
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Zcash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumClassicId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From Ethereum
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman, ExtraFeePercent = -5
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Neo
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Zcash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = ethereumId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From Litecoin
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman, ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Neo
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Zcash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = litecoinId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From Monero
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman, ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Neo
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Zcash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = moneroId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From NEO
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman, ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Zcash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = neoId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From Dash
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman, ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //NEO
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Zcash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = dashId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From ZCash
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman, ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD, ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin, ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = zcashId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From Bitcoin Cash
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman,
                ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //NEO
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //ZCash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinCashId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            //New Exchange Types

            #region From Ripple
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman,
                ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //ZCash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = rippleId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From Cardano
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman,
                ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //ZCash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = cardanoId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From QTUM
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman,
                ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //ZCash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = qtumId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From Bitcoin Gold
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman,
                ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //ZCash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = bitcoinGoldId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From Ark
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman,
                ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //ZCash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //IOTA
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = arkId,
                ToCurrencyId = iotaId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            #region From IOTA
            //Shetab
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = shetabId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatToman,
                ExtraFeePercent = 7
            });
            //WebMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = webMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Ethereum Classic
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = ethereumClassicId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //PerfectMoney
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = perfectMoneyId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Bitcoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = bitcoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //PAYEER
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = payeerId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToFiatUSD,
                ExtraFeePercent = 7
            });
            //Ethereum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = ethereumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Litecoin
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = litecoinId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Monero
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = moneroId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = neoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Dash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = dashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Cash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = bitcoinCashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ripple
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = rippleId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Cardano
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = cardanoId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Qtum
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = qtumId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Bitcoin Gold 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = bitcoinGoldId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //Ark 
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = arkId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            //ZCash
            exchangeTypesDbSet.AddOrUpdate(p => new { p.FromCurrencyId, p.ToCurrencyId }, new ExchangeType()
            {
                FromCurrencyId = iotaId,
                ToCurrencyId = zcashId,
                ExchangeFiatCoinType = ExchangeFiatCoinType.CoinToCoin,
                ExtraFeePercent = 7
            });
            #endregion

            context.SaveChanges();
        }
    }
}