﻿using System.Data.Entity.Migrations;
using Xtoman.Domain.Models;

namespace Xtoman.Data.Migration
{
    public static class SeedCommissions
    {
        public static void Execute(AppDbContext context)
        {
            var commissionDbSet = context.Set<Commission>();
            commissionDbSet.AddOrUpdate(p => p.Name, new Commission()
            {
                Name = "پایه",
                MinValue = 0, // Income Value in BTC
                Percent = 3
            });
            commissionDbSet.AddOrUpdate(p => p.Name, new Commission()
            {
                Name = "جوان",
                MinValue = 0.01M, // Income Value in BTC
                Percent = 5
            });
            commissionDbSet.AddOrUpdate(p => p.Name, new Commission()
            {
                Name = "متوسط",
                MinValue = 0.05M, // Income Value in BTC
                Percent = 10
            });
            commissionDbSet.AddOrUpdate(p => p.Name, new Commission()
            {
                Name = "پیشرفته",
                MinValue = 0.1M, // Income Value in BTC
                Percent = 12
            });
            commissionDbSet.AddOrUpdate(p => p.Name, new Commission()
            {
                Name = "حرفه ای",
                MinValue = 0.5M, // Income Value in BTC
                Percent = 15
            });
            commissionDbSet.AddOrUpdate(p => p.Name, new Commission()
            {
                Name = "ویژه",
                MinValue = 1, // Income Value in BTC
                Percent = 20
            });
        }
    }
}
