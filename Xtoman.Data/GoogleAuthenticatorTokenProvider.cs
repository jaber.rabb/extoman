﻿using Base32;
using Microsoft.AspNet.Identity;
using OtpSharp;
using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Data
{
    public class GoogleAuthenticatorTokenProvider : IUserTokenProvider<AppUser, int>
    {
        public Task<string> GenerateAsync(string purpose, UserManager<AppUser, int> manager, AppUser user)
        {
            return Task.FromResult((string)null);
        }

        public Task<bool> ValidateAsync(string purpose, string token, UserManager<AppUser, int> manager, AppUser user)
        {
            long timeStepMatched = 0;

            var otp = new Totp(Base32Encoder.Decode(user.GoogleAuthenticatorSecretKey));
            bool valid = otp.VerifyTotp(token, out timeStepMatched, new VerificationWindow(2, 2));

            return Task.FromResult(valid);
        }

        public Task NotifyAsync(string token, UserManager<AppUser, int> manager, AppUser user)
        {
            return Task.FromResult(true);
        }

        public Task<bool> IsValidProviderForUserAsync(UserManager<AppUser, int> manager, AppUser user)
        {
            return Task.FromResult(user.IsGoogleAuthenticatorEnabled);
        }
    }
}
