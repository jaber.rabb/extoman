﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Data.Entity;
using Xtoman.Domain.Models;

namespace Xtoman.Data
{
    public interface IRepository<T> where T : class, IBaseEntity
    {
        #region Properties
        /// <summary>
        /// Gets a table
        /// </summary>
        IQueryable<T> Table { get; }

        /// <summary>
        /// Gets a table with "no tracking" enabled (EF feature) Use it only when you load record(s) only for read-only operations
        /// </summary>
        IQueryable<T> TableNoTracking { get; }

        /// <summary>
        /// Context
        /// </summary>
        IUnitOfWork Context { get; }

        /// <summary>
        /// Entities
        /// </summary>
        IDbSet<T> Entities { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Get entity by id
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Entity</returns>
        T GetById(long id);

        /// <summary>
        /// Get entity by identifier
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="entity"></param>
        /// <returns>Entity</returns>
        T AddIfNotExists(Expression<Func<T, bool>> predicate, T entity);
        /// <summary>
        /// Insert entity
        /// </summary>
        /// <param name="entity">Entity</param>
        void Insert(T entity);

        /// <summary>
        /// Insert entities
        /// </summary>
        /// <param name="entities">Entities</param>
        void Insert(IEnumerable<T> entities);

        /// <summary>
        /// Update entity
        /// </summary>
        /// <param name="entity">Entity</param>
        void Update(T entity);

        /// <summary>
        /// Update entities
        /// </summary>
        /// <param name="entities">Entities</param>
        void Update(IEnumerable<T> entities);

        /// <summary>
        /// Update entity
        /// </summary>
        /// <param name="entity">Entity</param>
        void CleanUpdate(T entity);

        /// <summary>
        /// Update entities
        /// </summary>
        /// <param name="entities">Entities</param>
        void CleanUpdate(IEnumerable<T> entities);

        /// <summary>
        /// AddOrUpdate entity
        /// </summary>
        /// <param name="entity">Entity</param>
        void AddOrUpdate(params T[] entity);
        //this IDbSet<TEntity> set, Expression<Func<TEntity, object>> identifierExpression, params TEntity[] entities

        /// <summary>
        /// AddOrUpdate entity
        /// </summary>
        /// <param name="identifierExpression"></param>
        /// <param name="entity">Entity</param>
        void AddOrUpdate(Expression<Func<T, object>> identifierExpression, params T[] entity);

        /// <summary>
        /// Delete entity by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(int id);

        /// <summary>
        /// Delete entity
        /// </summary>
        /// <param name="entity">Entity</param>
        bool Delete(T entity);

        /// <summary>
        /// Delete entities
        /// </summary>
        /// <param name="entities">Entities</param>
        bool Delete(IEnumerable<T> entities);
        #endregion

        #region Async Methods
        Task<T> GetByIdAsync(long id);
        Task<T> AddIfNotExistsAsync(Expression<Func<T, bool>> predicate, T entity);
        Task InsertAsync(T entity);
        Task InsertAsync(IEnumerable<T> entities);
        Task UpdateAsync(T entity);
        Task UpdateAsync(IEnumerable<T> entities);
        Task AddOrUpdateAsync(params T[] entity);
        Task AddOrUpdateAsync(Expression<Func<T, object>> identifierExpression, params T[] entity);
        Task DeleteAsync(T entity);
        Task DeleteAsync(IEnumerable<T> entities);
        Task DeleteAsync(int id);
        #endregion
    }
}
