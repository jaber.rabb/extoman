﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Data
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T">Entity class</typeparam>
    /// <typeparam name="S">Entity cache class</typeparam>
    public class CacheRepositoryBase<T, S> : RepositoryBase<T> where T : class, IBaseEntity where S : class, IBaseCachingEntity
    {
        #region Fields
        private readonly IUnitOfWork _uow;
        private readonly ICacheManager _cacheManager;
        private readonly CacheSettings _cacheSettings;
        private static readonly string CACHE_KEY_NAME = "Xtoman";
        private static readonly string TYPE_NAME = typeof(T).Name.ToLower();

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : id
        /// </remarks>
        protected string CACHE_KEY = $"{CACHE_KEY_NAME}.{TYPE_NAME}.{{0}}";

        /// <summary>
        /// Key for caching
        /// </summary>
        protected readonly string CACHE_ALL_KEY = $"{CACHE_KEY_NAME}.{TYPE_NAME}.all";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        protected readonly string CACHE_PATTERN_KEY = $"{CACHE_KEY_NAME}.{TYPE_NAME}.";
        #endregion

        #region Constractor
        public CacheRepositoryBase(IUnitOfWork context, ICacheManager cacheManager, CacheSettings cacheSettings) : base(context)
        {
            _uow = context;
            _cacheManager = cacheManager;
            _cacheSettings = cacheSettings;
        }
        #endregion

        public List<S> GetAllCached(Expression<Func<T, S>> selector)
        {
            if (_cacheManager.IsSet(CACHE_ALL_KEY))
                return _cacheManager.Get<List<S>>(CACHE_ALL_KEY);

            return _cacheManager.Get(CACHE_ALL_KEY, () =>
            {
                return TableNoTracking.Select(selector).ToList();
            });
        }

        public async Task<List<S>> GetAllCachedAsync(Expression<Func<T, S>> selector)
        {
            if (_cacheManager.IsSet(CACHE_ALL_KEY))
                return _cacheManager.Get<List<S>>(CACHE_ALL_KEY);

            return await _cacheManager.GetAsync(CACHE_ALL_KEY, async () =>
            {
                return await TableNoTracking.Select(selector).ToListAsync();
            });
        }

        public async Task<S> GetCachedByIdAsync(long id, Expression<Func<T,S>> selector)
        {
            var key = string.Format(CACHE_KEY, id);
            if (_cacheManager.IsSet(key))
                return _cacheManager.Get<S>(key);
            if (_cacheSettings.LoadAllOnStartup)
            {
                var list = await _cacheManager.GetAsync(CACHE_ALL_KEY, async () =>
                {
                    return await TableNoTracking.Select(selector).ToListAsync();
                });
                return _cacheManager.Get(key, () =>
                {
                    return list.SingleOrDefault(p => p.Id == id);
                });
            }
            else
            {
                return await _cacheManager.GetAsync(key, async () =>
                {
                    return await TableNoTracking.Where(p => p.Id == id).Select(selector).FirstOrDefaultAsync();
                });
            }
        }

        #region Methods
        public override bool Delete(int id)
        {
            var result = base.Delete(id);
            _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
            return result;
        }

        public override bool Delete(IEnumerable<T> entities)
        {
            var result = base.Delete(entities);
            _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
            return result;
        }

        public override bool Delete(T entity)
        {
            var result = base.Delete(entity);
            _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
            return result;
        }
        /// <summary>
        /// Add entity and remove all cache by pattern key
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns></returns>
        public override async Task InsertAsync(T entity)
        {
            await base.InsertAsync(entity);
            _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
        }

        /// <summary>
        /// Add entities many and remove all cache by pattern key
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns></returns>
        /// 
        public override async Task InsertAsync(IEnumerable<T> entities)
        {
            await base.InsertAsync(entities);
            _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
        }

        /// <summary>
        /// Add entity if not exist by predicate
        /// </summary>
        /// <param name="predicate">A function to test each element for a condition.</param>
        /// <param name="entity">Entity</param>
        /// <returns></returns>
        public override async Task<T> AddIfNotExistsAsync(Expression<Func<T, bool>> predicate, T entity)
        {
            var result = await base.AddIfNotExistsWithStatusAsync(predicate, entity);
            if (result.Item2)
                _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
            return result.Item1;
        }

        /// <summary>
        /// Update entity async and remove all cache by pattern key
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns></returns>
        public override async Task UpdateAsync(T entity)
        {
            await base.UpdateAsync(entity);
            _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
        }

        /// <summary>
        /// Update entities async and remove all cache by pattern key
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns></returns>
        public override async Task UpdateAsync(IEnumerable<T> entities)
        {
            await base.UpdateAsync(entities);
            _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
        }

        /// <summary>
        /// Add or update entity and remove all cache by pattern key
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns></returns>
        public override async Task AddOrUpdateAsync(params T[] entity)
        {
            await base.AddOrUpdateAsync(entity);
            _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
        }

        /// <summary>
        /// Add or update entity and remove all cache by pattern key
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <param name="identifierExpression">An expression specifying the properties that should be used when determining whether an Add or Update operation should be performed.</param>
        /// <returns></returns>
        public override async Task AddOrUpdateAsync(Expression<Func<T, object>> identifierExpression, params T[] entity)
        {
            await base.AddOrUpdateAsync(identifierExpression, entity);
            _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
        }

        /// <summary>
        /// Delete entity and remove all cache by pattern key
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns></returns>
        public override async Task DeleteAsync(T entity)
        {
            await base.DeleteAsync(entity);
            _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
        }

        /// <summary>
        /// Delete entities and remove all cache by pattern key
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public override async Task DeleteAsync(IEnumerable<T> entities)
        {
            await base.DeleteAsync(entities);
            _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
        }

        /// <summary>
        /// Delete entity by id and remove all cache by pattern key
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override async Task DeleteAsync(int id)
        {
            await base.DeleteAsync(id);
            _cacheManager.RemoveByPattern(CACHE_PATTERN_KEY);
        }
        #endregion
    }
}
