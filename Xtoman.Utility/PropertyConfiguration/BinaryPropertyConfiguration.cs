﻿namespace Xtoman.Utility.PropertyConfiguration
{
    public class BinaryPropertyConfiguration : LengthPropertyConfiguration
    {
        // <summary>
        // Gets or sets a value indicating whether the property is a row version in the
        // database.
        // </summary>
        public bool? IsRowVersion { get; set; }
    }
}
