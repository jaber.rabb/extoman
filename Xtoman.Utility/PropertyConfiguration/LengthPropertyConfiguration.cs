﻿namespace Xtoman.Utility.PropertyConfiguration
{
    public class LengthPropertyConfiguration : PrimitivePropertyConfiguration
    {
        // <summary>
        // Gets or sets a value indicating whether the property is fixed length.
        // </summary>
        public bool? IsFixedLength { get; set; }

        // <summary>
        // Gets or sets the maximum length of the property.
        // </summary>
        public int? MaxLength { get; set; }

        // <summary>
        // Gets or sets a value indicating whether the property allows the maximum
        // length supported by the database provider.
        // </summary>
        public bool? IsMaxLength { get; set; }
    }
}
