﻿namespace Xtoman.Utility.PropertyConfiguration
{
    public class StringPropertyConfiguration : LengthPropertyConfiguration
    {
        // <summary>
        // Gets or sets a value indicating whether the property supports Unicode string
        // content.
        // </summary>
        public bool? IsUnicode { get; set; }
    }
}
