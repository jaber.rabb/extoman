﻿namespace Xtoman.Utility.PropertyConfiguration
{
    public class DecimalPropertyConfiguration : PrimitivePropertyConfiguration
    {
        // <summary>
        // Gets or sets the precision of the property.
        // </summary>
        public byte? Precision { get; set; }

        // <summary>
        // Gets or sets the scale of the property.
        // </summary>
        public byte? Scale { get; set; }
    }
}
