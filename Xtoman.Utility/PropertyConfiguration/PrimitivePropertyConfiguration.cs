﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Core.Metadata.Edm;

namespace Xtoman.Utility.PropertyConfiguration
{
    public class PrimitivePropertyConfiguration
    {
        // <summary>
        // Gets a value indicating whether the property is optional.
        // </summary>
        public bool? IsNullable { get; set; }

        // <summary>
        // Gets or sets the concurrency mode to use for the property.
        // </summary>
        public ConcurrencyMode? ConcurrencyMode { get; set; }

        // <summary>
        // Gets or sets the pattern used to generate values in the database for the
        // property.
        // </summary>
        public DatabaseGeneratedOption? DatabaseGeneratedOption { get; set; }

        // <summary>
        // Gets or sets the type of the database column used to store the property.
        // </summary>
        public string ColumnType { get; set; }

        // <summary>
        // Gets or sets the name of the database column used to store the property.
        // </summary>
        public string ColumnName { get; set; }
    }
}
