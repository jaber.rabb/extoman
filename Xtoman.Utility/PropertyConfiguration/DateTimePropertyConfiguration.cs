﻿namespace Xtoman.Utility.PropertyConfiguration
{
    public class DateTimePropertyConfiguration : PrimitivePropertyConfiguration
    {
        // <summary>
        // Gets or sets the precision of the property.
        // </summary>
        public byte? Precision { get; set; }
    }
}
