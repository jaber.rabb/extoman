﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Xtoman.Utility
{
    public class DisplayNameContractResolver : DefaultContractResolver
    {
        public static readonly DisplayNameContractResolver Instance = new DisplayNameContractResolver();

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);

            //Get property display name from display attribute
            var attribute = member.GetAttribute<DisplayAttribute>();
            if (attribute != null)
                property.PropertyName = attribute.GetType().GetProperty(DisplayProperty.Name.ToString()).GetValue(attribute, null) as string;

            return property;
        }
    }
}
