﻿
namespace Xtoman.Utility
{
    public class ElmahEfInterceptor : EfExceptionsInterceptor
    {
        public ElmahEfInterceptor() : base(new ElmahEfExceptionsLogger())
        {
        }
    }
}