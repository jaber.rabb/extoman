﻿using System;
using System.Data.Common;
using System.Data.Entity.Infrastructure.Interception;

namespace Xtoman.Utility
{
    public class ElmahEfExceptionsLogger : IEfExceptionsLogger
    {
        /// <summary>
        /// Manually log errors using ELMAH
        /// </summary>
        public void LogException<TResult>(DbCommand command, DbCommandInterceptionContext<TResult> interceptionContext)
        {
            var ex = interceptionContext.OriginalException;
            if (ex == null)
                return;

            var sqlData = CommandDumper.LogSqlAndParameters(command, interceptionContext);
            var contextualMessage = $"{sqlData}{Environment.NewLine}OriginalException:{Environment.NewLine}{ex} {Environment.NewLine}";

            if (!string.IsNullOrWhiteSpace(contextualMessage))
                ex = new Exception(contextualMessage, new ElmahEfInterceptorException(ex.Message));

            ex.LogError();
        }
    }
}
