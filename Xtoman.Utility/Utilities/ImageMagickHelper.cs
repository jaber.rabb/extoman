﻿using ImageMagick;
using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;

namespace Xtoman.Utility
{
    public static class ImageMagickHelper
    {
        #region Public
        public static void Save(this MagickImage image, string savePath)
        {
            new MagickImage(image).Write(savePath);
        }

        public static MagickImage Save(string path)
        {
            return new MagickImage(path);
        }

        public static void MergeImages(string savePath, params string[] images)
        {
            using (var imagesCollection = new MagickImageCollection())
            {
                foreach (var item in images)
                    imagesCollection.Add(new MagickImage(item));

                using (var result = imagesCollection.Mosaic())
                    result.Write(savePath);
            }
        }

        public static void CreateGif(string path, params string[] images)
        {
            using (var collection = new MagickImageCollection())
            {
                foreach (var item in images)
                {
                    collection.Add(item);
                    collection[0].AnimationDelay = 100;
                    //collection[1].Flip();
                }

                var settings = new QuantizeSettings()
                {
                    Colors = 256
                };
                collection.Quantize(settings);
                // Optionally optimize the images (images should have the same size).
                collection.Optimize();
                collection.Write(path);
            }
        }
        #endregion

        #region Compress
        public static void CompressLossless(string path)
        {
            var optimizer = new ImageOptimizer()
            {
                OptimalCompression = true
            };
            optimizer.LosslessCompress(path);
        }

        public static void Compress(string path, int quality)
        {
            using (var image = new MagickImage(path))
            {
                image.Quality = quality;
                image.Write(path);
            }
        }

        public static MagickImage CompressReturn(string path, int quality)
        {
            var image = new MagickImage(path)
            {
                Quality = quality
            };
            return image;
        }

        public static MagickImage CompressReturn(this MagickImage image, int quality)
        {
            image.Quality = quality;
            return image;
        }

        public static void Compress(string path, QualityLevel quality = QualityLevel.q2Normal)
        {
            // Read from file
            using (var image = new MagickImage(path))
            {
                //image.CompressionMethod = CompressionMethod.Zip;
                image.Quality = System.Convert.ToInt32(quality);
                image.Write(path);
            }
        }

        public static dynamic CheckIsPhotoshop(object p)
        {
            throw new NotImplementedException();
        }

        public static void Compress(string path, string savePath, QualityLevel quality = QualityLevel.q2Normal)
        {
            // Read from file
            using (var image = new MagickImage(path))
            {
                //image.CompressionMethod = CompressionMethod.Zip;
                image.Quality = System.Convert.ToInt32(quality);
                image.Write(savePath);
            }
        }

        public static void Compress(this MagickImage image, string savePath, QualityLevel quality = QualityLevel.q2Normal)
        {
            //image.CompressionMethod = CompressionMethod.Zip;
            image.Quality = System.Convert.ToInt32(quality);
            image.Write(savePath);
        }

        public static void Compress(this MagickImage image, string savePath, int quality)
        {
            image.Quality = quality;
            image.Write(savePath);
        }

        public static MagickImage CompressReturn(string path, QualityLevel quality = QualityLevel.q2Normal)
        {
            // Read from file
            var image = new MagickImage(path)
            {
                //image.CompressionMethod = CompressionMethod.Zip;
                Quality = System.Convert.ToInt32(quality)
            };
            return image;
        }

        public static MagickImage CompressReturn(this MagickImage image, QualityLevel quality = QualityLevel.q2Normal)
        {
            image.Quality = System.Convert.ToInt32(quality);
            return image;
        }
        #endregion

        #region Watermark
        public static void Watermark(string path, string watermak, int x, int y)
        {
            using (var image = new MagickImage(path))
            {
                using (var watermark = new MagickImage(watermak))
                {
                    image.Composite(watermark, Gravity.Southeast, CompositeOperator.Over);
                    watermark.Evaluate(Channels.Alpha, EvaluateOperator.Divide, 4);
                    image.Composite(watermark, x, y, CompositeOperator.Over);
                }
                image.Write(path);
            }
        }

        public static void Watermark(string path, string watermak, string savePath, int x, int y)
        {
            using (var image = new MagickImage(path))
            {
                using (var watermark = new MagickImage(watermak))
                {
                    image.Composite(watermark, Gravity.Southeast, CompositeOperator.Over);
                    watermark.Evaluate(Channels.Alpha, EvaluateOperator.Divide, 4);
                    image.Composite(watermark, x, y, CompositeOperator.Over);
                }
                image.Write(savePath);
            }
        }

        public static void Watermark(this MagickImage image, string watermak, string savePath, int x, int y)
        {
            using (var watermark = new MagickImage(watermak))
            {
                image.Composite(watermark, Gravity.Southeast, CompositeOperator.Over);
                watermark.Evaluate(Channels.Alpha, EvaluateOperator.Divide, 4);
                image.Composite(watermark, x, y, CompositeOperator.Over);
            }
            image.Write(savePath);
        }

        public static MagickImage WatermarkReturn(this MagickImage image, string watermak, int x, int y)
        {
            using (var watermark = new MagickImage(watermak))
            {
                image.Composite(watermark, Gravity.Southeast, CompositeOperator.Over);
                watermark.Evaluate(Channels.Alpha, EvaluateOperator.Divide, 4);
                image.Composite(watermark, x, y, CompositeOperator.Over);
            }
            return image;
        }

        public static MagickImage WatermarkReturn(string path, string watermak, int x, int y)
        {
            var image = new MagickImage(path);
            using (var watermark = new MagickImage(watermak))
            {
                image.Composite(watermark, Gravity.Southeast, CompositeOperator.Over);
                watermark.Evaluate(Channels.Alpha, EvaluateOperator.Divide, 4);
                image.Composite(watermark, x, y, CompositeOperator.Over);
            }
            return image;
        }
        #endregion

        #region Convert
        public static byte[] Convert(string path)
        {
            // Read image from file
            using (var image = new MagickImage(path))
            {
                // Sets the output format to jpeg
                image.Format = MagickFormat.Jpeg;
                // Create byte array that contains a jpeg file
                return image.ToByteArray();
            }
        }

        public static byte[] Convert(this MagickImage imageM)
        {
            // Read image from file
            using (var image = new MagickImage(imageM))
            {
                // Sets the output format to jpeg
                image.Format = MagickFormat.Jpeg;
                // Create byte array that contains a jpeg file
                return image.ToByteArray();
            }
        }

        public static void Convert(string path, string savePath)
        {
            using (var image = new MagickImage(path))
            {
                image.Write(savePath);
            }
        }

        public static void Convert(this MagickImage image, string savePath)
        {
            image.Write(savePath);
        }

        public static void Convert(this MagickImage image, string savePath, MagickFormat format)
        {
            var digits = new[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            var path = string.Format("{0}\\{1}.{2}", Path.GetDirectoryName(savePath), Path.GetFileNameWithoutExtension(savePath), format.ToString().RemoveStr(digits).ToLower());
            image.Format = format;
            image.Write(path);
        }

        public static MagickImage ConvertReturn(this MagickImage image, MagickFormat format)
        {
            using (var memStream = new MemoryStream())
            {
                image.Format = format;
                image.Write(memStream, format);
                return new MagickImage(memStream);
            }
        }

        public static MagickImage ConvertReturn(string filePath, MagickFormat format)
        {
            using (var memStream = new MemoryStream())
            {
                var image = new MagickImage(filePath)
                {
                    Format = format
                };
                image.Write(memStream, format);
                return new MagickImage(memStream);
            }
        }

        public static void Convert(string filePath, string savePath, MagickFormat format)
        {
            var digits = new[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            var dir = Path.GetDirectoryName(savePath);
            var file = Path.GetFileNameWithoutExtension(savePath);
            var ext = format.ToString().RemoveStr(digits).ToLower();
            var path = $"{dir}\\{file}.{ext}";
            using (var memStream = new MemoryStream())
            {
                var image = new MagickImage(filePath)
                {
                    Format = format
                };
                image.Write(memStream);
                var convertedImage = new MagickImage(memStream);
                convertedImage.Write(path);
            }
        }

        public static void ConvertCMYKToRGB(string path)
        {
            // Uses sRGB.icm, eps/pdf produce better result when you set this before loading.
            var settings = new MagickReadSettings()
            {
                ColorSpace = ColorSpace.sRGB
            };

            // Create empty image 
            using (var image = new MagickImage())
            {
                // Reads the eps image, the specified settings tell Ghostscript to create an sRGB image
                image.Read("Snakeware.eps", settings);
                // Save image as tiff
                image.Write("Snakeware.tiff");
            }

            // Read image from file
            using (var image = new MagickImage("Snakeware.jpg"))
            {
                // Add a CMYK profile if your image does not contain a color profile.
                image.AddProfile(ColorProfile.USWebCoatedSWOP);

                // Adding the second profile will transform the colorspace from CMYK to RGB
                image.AddProfile(ColorProfile.SRGB);
                // Save image as png
                image.Write("Snakeware.png");
            }

            // Use custom color profile
            using (var image = new MagickImage("Snakeware.jpg"))
            {
                // First add a CMYK profile if your image does not contain a color profile.
                image.AddProfile(ColorProfile.USWebCoatedSWOP);

                // Adding the second profile will transform the colorspace from your custom icc profile
                image.AddProfile(new ColorProfile("YourProfile.icc"));
                // Save image as tiff
                image.Write("Snakeware.tiff");
            }
        }
        #endregion

        #region ResizeGif
        public static void ResizeGif(string path, int with, int height)
        {
            // Read from file
            using (var collection = new MagickImageCollection(path))
            {
                // This will remove the optimization and change the image to how it looks at that point during the animation.
                collection.Coalesce();
                // Resize each image in the collection to a width of 200. When zero is specified for the height
                // the height will be calculated with the aspect ratio.
                foreach (MagickImage image in collection)
                {
                    //image.Resize(200, 0);
                    image.Resize(with, height);
                }
                collection.Write(path);
            }
        }

        public static void ResizeGif(string path, int with, int height, string savePath)
        {
            // Read from file
            using (var collection = new MagickImageCollection(path))
            {
                // This will remove the optimization and change the image to how it looks at that point during the animation.
                collection.Coalesce();
                // Resize each image in the collection to a width of 200. When zero is specified for the height
                // the height will be calculated with the aspect ratio.
                foreach (MagickImage image in collection)
                {
                    //image.Resize(200, 0);
                    image.Resize(with, height);
                }
                collection.Write(savePath);
            }
        }
        #endregion

        #region Resize
        public static void Resize(string path, int width, int height, bool aspectRatio = true)
        {
            // Read from file
            using (var image = new MagickImage(path))
            {
                var size = new MagickGeometry(width, height)
                {
                    // This will resize the image to a fixed size without maintaining the aspect ratio.
                    // Normally an image will be resized to fit inside the specified size.
                    IgnoreAspectRatio = aspectRatio
                };
                image.Resize(size);
                image.Write(path);
            }
        }

        public static void Resize(string path, int width, int height, string savePath, bool aspectRatio = true)
        {
            using (var image = new MagickImage(path))
            {
                var size = new MagickGeometry(width, height)
                {
                    IgnoreAspectRatio = aspectRatio
                };
                image.Resize(size);
                image.Write(savePath);
            }
        }

        public static void Resize(this MagickImage image, int width, int height, string savePath, bool aspectRatio = true)
        {
            var size = new MagickGeometry(width, height)
            {
                IgnoreAspectRatio = aspectRatio
            };
            image.Resize(size);
            image.Write(savePath);
        }

        public static MagickImage ResizeReturn(string path, int width, int height, bool aspectRatio = true)
        {
            var image = new MagickImage(path);
            var size = new MagickGeometry(width, height)
            {
                IgnoreAspectRatio = aspectRatio
            };
            image.Resize(size);
            return image;
        }

        public static MagickImage ResizeReturn(this MagickImage image, int width, int height, bool aspectRatio = true)
        {
            var size = new MagickGeometry(width, height)
            {
                IgnoreAspectRatio = aspectRatio
            };
            image.Resize(size);
            return image;
        }



        public static void ResizeByWidth(string path, int width, bool aspectRatio = true)
        {
            // Read from file
            using (var image = new MagickImage(path))
            {
                var height = image.Height * width / image.Width;
                var size = new MagickGeometry(width, height)
                {
                    // This will resize the image to a fixed size without maintaining the aspect ratio.
                    // Normally an image will be resized to fit inside the specified size.
                    IgnoreAspectRatio = aspectRatio
                };
                image.Resize(size);
                image.Write(path);
            }
        }

        public static void ResizeByWidth(string path, int width, string savePath, bool aspectRatio = true)
        {
            using (var image = new MagickImage(path))
            {
                var height = image.Height * width / image.Width;
                var size = new MagickGeometry(width, height)
                {
                    IgnoreAspectRatio = aspectRatio
                };
                image.Resize(size);
                image.Write(savePath);
            }
        }

        public static void ResizeByWidth(this MagickImage image, int width, string savePath, bool aspectRatio = true)
        {
            var height = image.Height * width / image.Width;
            var size = new MagickGeometry(width, height)
            {
                IgnoreAspectRatio = aspectRatio
            };
            image.Resize(size);
            image.Write(savePath);
        }

        public static MagickImage ResizeByWidthReturn(string path, int width, bool aspectRatio = true)
        {
            var image = new MagickImage(path);
            var height = image.Height * width / image.Width;
            var size = new MagickGeometry(width, height)
            {
                IgnoreAspectRatio = aspectRatio
            };
            image.Resize(size);
            return image;
        }

        public static MagickImage ResizeByWidthReturn(this MagickImage image, int width, bool aspectRatio = true)
        {
            var height = image.Height * width / image.Width;
            var size = new MagickGeometry(width, height)
            {
                IgnoreAspectRatio = aspectRatio
            };
            image.Resize(size);
            return image;
        }


        public static void ResizeByHeight(string path, int height, bool aspectRatio = true)
        {
            // Read from file
            using (var image = new MagickImage(path))
            {
                var width = image.Width * height / image.Height;
                var size = new MagickGeometry(width, height)
                {
                    // This will resize the image to a fixed size without maintaining the aspect ratio.
                    // Normally an image will be resized to fit inside the specified size.
                    IgnoreAspectRatio = aspectRatio
                };
                image.Resize(size);
                image.Write(path);
            }
        }

        public static void ResizeByHeight(string path, int height, string savePath, bool aspectRatio = true)
        {
            using (var image = new MagickImage(path))
            {
                var width = image.Width * height / image.Height;
                var size = new MagickGeometry(width, height)
                {
                    IgnoreAspectRatio = aspectRatio
                };
                image.Resize(size);
                image.Write(savePath);
            }
        }

        public static void ResizeByHeight(this MagickImage image, int height, string savePath, bool aspectRatio = true)
        {
            var width = image.Width * height / image.Height;
            var size = new MagickGeometry(width, height)
            {
                IgnoreAspectRatio = aspectRatio
            };
            image.Resize(size);
            image.Write(savePath);
        }

        public static bool CheckIsPhotoshop(string path)
        {
            var strResult = "";
            var image = new MagickImage(path);
            foreach (var profileName in image.ProfileNames)
            {
                var x = image.GetProfile(profileName);
                strResult += " " + Encoding.UTF8.GetString(x.ToByteArray());
            }
            strResult = strResult.Trim().ToLower();
            if (strResult.Contains("iphone"))
            {
                return false;
            }
            else if (strResult.Contains("adobe") || strResult.Contains("photoshop"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static MagickImage ResizeByHeightReturn(string path, int height, bool aspectRatio = true)
        {
            var image = new MagickImage(path);
            var width = image.Width * height / image.Height;
            var size = new MagickGeometry(width, height)
            {
                IgnoreAspectRatio = aspectRatio
            };
            image.Resize(size);
            return image;
        }

        public static MagickImage ResizeByHeightReturn(this MagickImage image, int height, bool aspectRatio = true)
        {
            var width = image.Width * height / image.Height;
            var size = new MagickGeometry(width, height)
            {
                IgnoreAspectRatio = aspectRatio
            };
            image.Resize(size);
            return image;
        }
        #endregion

        #region Crop
        public static void Crop(string path, int x, int y, int width, int height)
        {
            using (var image = new MagickImage(path))
            {
                image.Crop(x, y, width, height);
                image.Write(path);
            }
        }

        public static void Crop(string path, int x, int y, int width, int height, string savePath)
        {
            using (var image = new MagickImage(path))
            {
                image.Crop(x, y, width, height);
                image.Write(savePath);
            }
        }

        public static void Crop(this MagickImage image, int x, int y, int width, int height, string savePath)
        {
            image.Crop(x, y, width, height);
            image.Write(savePath);
        }

        public static MagickImage CropReturn(string path, int x, int y, int width, int height)
        {
            var image = new MagickImage(path);
            image.Crop(x, y, width, height);
            return image;
        }

        public static MagickImage CropReturn(this MagickImage image, int x, int y, int width, int height)
        {
            image.Crop(x, y, width, height);
            return image;
        }
        #endregion

        #region ResizePercente
        public static void ResizePercente(string path, double percent)
        {
            // Read from file
            using (var image = new MagickImage(path))
            {
                image.Resize(new Percentage(percent));
                image.Write(path);
            }
        }

        public static void ResizePercente(string path, double percent, string savePath)
        {
            // Read from file
            using (var image = new MagickImage(path))
            {
                image.Resize(new Percentage(percent));
                image.Write(savePath);
            }
        }

        public static MagickImage ResizePercenteReturn(this MagickImage image, double percent)
        {
            image.Resize(new Percentage(percent));
            return image;
        }

        public static void ResizePercente(this MagickImage image, double percent, string savePath)
        {
            image.Resize(new Percentage(percent));
            image.Write(savePath);
        }
        #endregion

        public static void ResizeCrop(string path, int width, int height, string savePath, bool crop)
        {
            using (var image = new MagickImage(path))
            {
                if (image.Width > 1 && image.Height > 1)
                {
                    MagickGeometry size = new MagickGeometry
                    {
                        Width = width,
                        Height = height,
                        FillArea = true
                    };
                    image.Resize(size);
                    if (crop)
                        image.Crop(width, height);
                    image.Write(savePath);
                }
            }
        }

        public enum QualityLevel
        {
            x50 = 50,
            x60 = 60,
            x70 = 70,
            x80 = 80,
            x90 = 90,
            x100 = 100,
            //---------------
            q4Low = 55,
            q3Medium = 65,
            q2Normal = 75,
            q1Good = 85
        }
    }
}