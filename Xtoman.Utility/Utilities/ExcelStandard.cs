﻿using OfficeOpenXml;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace Xtoman.Utility
{
    public static class ExcelStandard
    {
        public static DataTable ExcelStandardToTable(this string path, bool hasHeader)
        {
            using (var pck = new ExcelPackage())
            {
                using (var stream = File.OpenRead(path))
                {
                    pck.Load(stream);
                }
                var ws = pck.Workbook.Worksheets.First();
                var tbl = new DataTable();
                foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                {
                    tbl.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
                }
                var startRow = hasHeader ? 2 : 1;
                for (var rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                    var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                    var row = tbl.NewRow();
                    foreach (var cell in wsRow)
                    {
                        if (cell.Start.Column - 1 < tbl.Columns.Count)
                            row[cell.Start.Column - 1] = cell.Text;
                    }
                    tbl.Rows.Add(row);
                }
                return tbl;
            }
        }

        public static void ToExcelStandard(this DataTable dt, string fileName)
        {
            var package = new ExcelPackage();
            var worksheet = package.Workbook.Worksheets.Add("excel");
            worksheet.Cells["A1"].LoadFromDataTable(dt, true);
            package.SaveAs(new FileInfo(fileName));
        }

        public static void ToExcelStandard<T>(this IEnumerable<T> enumerable, string fileName)
        {
            DataTable dt = enumerable.ToDataTable();
            var package = new ExcelPackage();
            var worksheet = package.Workbook.Worksheets.Add("excel");
            worksheet.Cells["A1"].LoadFromDataTable(dt, true);
            package.SaveAs(new FileInfo(fileName));
        }
    }
}
