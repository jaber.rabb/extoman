﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http.Routing;

namespace Xtoman.Utility
{
    /// <summary>
    /// Extends the HttpRequestMessage collection
    /// </summary>
    public static class WebApiHelper
    {
        ///// <summary>
        ///// Returns a dictionary of QueryStrings that's easier to work with 
        ///// than GetQueryNameValuePairs KevValuePairs collection.
        ///// 
        ///// If you need to pull a few single values use GetQueryString instead.
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //public static Dictionary<string, string> GetQueryStrings(this HttpRequestMessage request)
        //{
        //    return request.GetQueryNameValuePairs()
        //                  .ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);
        //}

        ///// <summary>
        ///// Returns an individual querystring value
        ///// </summary>
        ///// <param name="request"></param>
        ///// <param name="key"></param>
        ///// <returns></returns>
        //public static string GetQueryString(this HttpRequestMessage request, string key)
        //{
        //    // IEnumerable<KeyValuePair<string,string>> - right!
        //    var queryStrings = request.GetQueryNameValuePairs();
        //    if (queryStrings == null)
        //        return null;

        //    var match = queryStrings.FirstOrDefault(kv => string.Compare(kv.Key, key, true) == 0);
        //    if (string.IsNullOrEmpty(match.Value))
        //        return null;

        //    return match.Value;
        //}

        /// <summary>
        /// Returns an individual HTTP Header value
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetHeader(this HttpRequestMessage request, string key)
        {
            if (!request.Headers.TryGetValues(key, out IEnumerable<string> keys))
                return null;

            return keys.First();
        }

        /// <summary>
        /// Retrieves an individual cookie from the cookies collection
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cookieName"></param>
        /// <returns></returns>
        public static string GetCookie(this HttpRequestMessage request, string cookieName)
        {
            CookieHeaderValue cookie = request.Headers.GetCookies(cookieName).FirstOrDefault();
            if (cookie != null)
                return cookie[cookieName].Value;

            return null;
        }

        public static T GetRouteVariable<T>(this IHttpRouteData routeData, string name)
        {
            if (routeData.Values.TryGetValue(name, out object result))
            {
                return (T)result;
            }
            return default(T);
        }
    }

    public class CookieCollection : Dictionary<string, CookieHeaderValue>
    {
        public CookieCollection(Collection<CookieHeaderValue> cookies)
        {
            foreach (var cookie in cookies)
                Add(cookie.Cookies[0].Name, cookie);
        }

        public new CookieHeaderValue this[string key]
        {
            get
            {
                return base[key];
            }
            set
            {
                base[key] = value;
            }
        }

        public CookieHeaderValue this[int index]
        {
            get
            {
                return this.ElementAt(index).Value;
            }
            set
            {
                this[this.ElementAt(index).Key] = value;
            }
        }
    }
}
