﻿using Xtoman.Utility.PropertyConfiguration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Xtoman.Utility
{
    public class MappingInfo
    {
        public List<string> IgnoredProperties { get; set; }
        public Dictionary<string, PrimitivePropertyConfiguration> PrimitivePropertyConfigurations { get; set; }
    }

    public static class ReflectionHelper
    {
        public static MappingInfo GetMappingInfo<T>(this T config)
        {
            var isEntityConfig = config.GetType().BaseType?.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>);
            if (!isEntityConfig)
                throw new ArgumentException("config is not EntityTypeConfiguration", "config");

            var mappingInfo = new MappingInfo() { PrimitivePropertyConfigurations = new Dictionary<string, PrimitivePropertyConfiguration>() };

            var q = config.GetType().GetRuntimeProperties().Single(p => p.Name == "Configuration").GetValue(config, null);
            var ignoredProperties = q.GetType().GetRuntimeProperties().Single(p => p.Name == "IgnoredProperties").GetValue(q, null) as HashSet<PropertyInfo>;
            var primitivePropertyConfigurations = q.GetType().GetRuntimeProperties().Single(p => p.Name == "PrimitivePropertyConfigurations").GetValue(q, null) as IEnumerable;
            foreach (var item in primitivePropertyConfigurations)
            {
                var key = item.GetType().GetProperties().Single(p => p.Name == "Key").GetValue(item, null).ToString();
                var value = item.GetType().GetProperties().Single(p => p.Name == "Value").GetValue(item, null);
                var valueType = value.GetType();

                var result = value.MapTo<StringPropertyConfiguration>(true) ??
                    value.MapTo<BinaryPropertyConfiguration>(true) ??
                    value.MapTo<DecimalPropertyConfiguration>(true) ??
                    value.MapTo<DateTimePropertyConfiguration>(true) ??
                    value.MapTo<LengthPropertyConfiguration>(true) ??
                    value.MapTo<PrimitivePropertyConfiguration>(true);

                mappingInfo.PrimitivePropertyConfigurations.Add(key, result);
            }
            mappingInfo.IgnoredProperties = ignoredProperties.Select(p => p.Name).ToList();

            return mappingInfo;
        }

        public static T MapTo<T>(this object value, bool chechName = false) where T : class, new()
        {
            var valueType = value.GetType();
            if (chechName && valueType.Name != typeof(T).Name)
                return null;

            var result = new T();
            foreach (var prop in typeof(T).GetProperties())
            {
                var propInfo = valueType.GetProperty(prop.Name);
                if (propInfo != null)
                {
                    var propValue = propInfo.GetValue(value, null);
                    prop.SetValue(result, propValue, null);
                }
            }
            return result;
        }

        public static string ToDisplay<T, TProp>(Expression<Func<T, TProp>> propertyExpression, DisplayProperty property = DisplayProperty.Name)
        {
            var propInfo = propertyExpression.GetPropInfo();
            var attr = propInfo.GetAttribute<DisplayNameAttribute>(false);
            return attr.GetType().GetProperty(property.ToString()).GetValue(attr, null) as string ?? propInfo.Name;
        }

        public static PropertyInfo GetPropInfo<T, TPropType>(this Expression<Func<T, TPropType>> keySelector)
        {
            //var propInfo = (keySelector.Body as MemberExpression)?.Member as PropertyInfo;
            var member = keySelector.Body as MemberExpression;
            if (member == null)
                throw new ArgumentException($"Expression '{keySelector}' refers to a method, not a property.");
            var propInfo = member.Member as PropertyInfo;
            if (propInfo == null)
                throw new ArgumentException($"Expression '{keySelector}' refers to a field, not a property.");
            return propInfo;
        }
        //public static MemberInfo GetPropertyInformation(this Expression propertyExpression)
        //{
        //    var memberExpr = propertyExpression as MemberExpression;
        //    if (memberExpr == null)
        //    {
        //        var unaryExpr = propertyExpression as UnaryExpression;
        //        if (unaryExpr != null && unaryExpr.NodeType == ExpressionType.Convert)
        //        {
        //            memberExpr = unaryExpr.Operand as MemberExpression;
        //        }
        //    }
        //    if (memberExpr != null && memberExpr.Member.MemberType == MemberTypes.Property)
        //    {
        //        return memberExpr.Member;
        //    }
        //    return null;
        //}
        public static T GetAttribute<T>(this MemberInfo member, bool isRequired = false) where T : Attribute
        {
            var attribute = member.GetCustomAttributes(typeof(T), false).SingleOrDefault();
            if (attribute == null && isRequired)
                throw new ArgumentException($"The {typeof(T).Name} attribute must be defined on member {member.Name}");
            return (T)attribute;
        }

        public static bool BaseTypeIsGeneric(this Type type, Type genericType)
        {
            return type.BaseType?.IsGenericType == true && type.BaseType.GetGenericTypeDefinition() == genericType;
        }

        public static IEnumerable<Type> GetTypesAssignableFrom<TType>() /*where TType : Type, Attribute*/
        {
            var type = typeof(TType);
            if (typeof(Attribute).IsAssignableFrom(type))
            {
                return type.Assembly.GetTypes().Where(p => p.IsDefined(typeof(TType), true)).ToList();
                //return type.Assembly.GetTypes().Where(p => p.GetCustomAttributes(typeof(TAttribute), true).Length > 0).ToList();
                //return type.Assembly.GetTypes().Where(p => Attribute.IsDefined(p, typeof(TAttribute))).ToList();
            }
            else
            {
                return type.Assembly.GetTypes().Where(p => type.IsAssignableFrom(p)).ToList();
            }
        }

        public static bool IsEnumerable(this Type type)
        {
            return type != typeof(string) && typeof(IEnumerable).IsAssignableFrom(type);
            //type.GetInterface(typeof(IEnumerable<>).FullName) != null;
        }

        public static bool IsEnumerable<T>(this Type type)
        {
            return type != typeof(string) && typeof(IEnumerable<T>).IsAssignableFrom(type) && type.IsGenericType;
        }

        public static bool IsUserDefinedClass(this Type type)
        {
            return type.Assembly.GetName().Name != "mscorlib";
        }

        public static Type GetBaseType<T>()
        {
            var type = typeof(T);
            var underlyingType = Nullable.GetUnderlyingType(type);
            return underlyingType ?? type;

            //if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            //    return typeof(T).GetGenericArguments()[0];
            //return type;
        }

        public static bool IsCustomType(Type type)
        {
            //var a = typeof(Type).Assembly == Assembly.GetExecutingAssembly();
            //var b = typeof(Type).Assembly.GetName().Name != "mscorlib";

            return IsCustomReferenceType(type) || IsCustomReferenceType(type);
        }

        public static bool IsCustomValueType(Type type)
        {
            return type.IsValueType && !type.IsPrimitive && type.Namespace != null && !type.Namespace.StartsWith("System.");
        }

        public static bool IsCustomReferenceType(Type type)
        {
            return !type.IsValueType && !type.IsPrimitive && type.Namespace != null && !type.Namespace.StartsWith("System.");
        }

        /// <summary>
        /// Returns a _private_ Property Value from a given Object. Uses Reflection.
        /// Throws a ArgumentOutOfRangeException if the Property is not found.
        /// </summary>
        /// <typeparam name="T">Type of the Property</typeparam>
        /// <param name="obj">Object from where the Property Value is returned</param>
        /// <param name="propName">Propertyname as string.</param>
        /// <returns>PropertyValue</returns>
        public static T GetPrivatePropertyValue<T>(this object obj, string propName)
        {
            CheckHelper.NotNull(obj, nameof(obj));
            PropertyInfo pi = obj.GetType().GetProperty(propName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            if (pi == null)
                throw new ArgumentOutOfRangeException("propName", $"Property {propName} was not found in Type {obj.GetType().FullName}");
            return (T)pi.GetValue(obj, null);
        }

        public static Object GetPropValue(this Object obj, String name)
        {
            foreach (String part in name.Split('.'))
            {
                if (obj == null) { return null; }

                Type type = obj.GetType();
                PropertyInfo info = type.GetProperty(part);
                if (info == null) { return null; }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }

        public static T GetPropValue<T>(this Object obj, String name)
        {
            Object retval = GetPropValue(obj, name);
            if (retval == null) { return default(T); }

            // throws InvalidCastException if types are incompatible
            return (T)retval;
        }

        /// <summary>
        /// Returns a private Field Value from a given Object. Uses Reflection.
        /// Throws a ArgumentOutOfRangeException if the Property is not found.
        /// </summary>
        /// <typeparam name="T">Type of the Field</typeparam>
        /// <param name="obj">Object from where the Field Value is returned</param>
        /// <param name="propName">Field Name as string.</param>
        /// <returns>FieldValue</returns>
        public static T GetPrivateFieldValue<T>(this object obj, string propName)
        {
            CheckHelper.NotNull(obj, nameof(obj));
            Type t = obj.GetType();
            FieldInfo fi = null;
            while (fi == null && t != null)
            {
                fi = t.GetField(propName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                t = t.BaseType;
            }
            if (fi == null)
                throw new ArgumentOutOfRangeException("propName", $"Field {propName} was not found in Type {obj.GetType().FullName}");
            return (T)fi.GetValue(obj);
        }

        /// <summary>
        /// Sets a _private_ Property Value from a given Object. Uses Reflection.
        /// Throws a ArgumentOutOfRangeException if the Property is not found.
        /// </summary>
        /// <typeparam name="T">Type of the Property</typeparam>
        /// <param name="obj">Object from where the Property Value is set</param>
        /// <param name="propName">Propertyname as string.</param>
        /// <param name="val">Value to set.</param>
        /// <returns>PropertyValue</returns>
        public static void SetPrivatePropertyValue<T>(this object obj, string propName, T val)
        {
            Type t = obj.GetType();
            if (t.GetProperty(propName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance) == null)
                throw new ArgumentOutOfRangeException("propName", $"Property {propName} was not found in Type {obj.GetType().FullName}");
            t.InvokeMember(propName,
                           BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.SetProperty
                           | BindingFlags.Instance, null, obj, new object[] { val });
        }

        /// <summary>
        /// Set a private Field Value on a given Object. Uses Reflection.
        /// </summary>
        /// <typeparam name="T">Type of the Field</typeparam>
        /// <param name="obj">Object from where the Property Value is returned</param>
        /// <param name="propName">Field name as string.</param>
        /// <param name="val">the value to set</param>
        /// <exception cref="ArgumentOutOfRangeException">if the Property is not found</exception>
        public static void SetPrivateFieldValue<T>(this object obj, string propName, T val)
        {
            CheckHelper.NotNull(obj, nameof(obj));
            Type t = obj.GetType();
            FieldInfo fi = null;
            while (fi == null && t != null)
            {
                fi = t.GetField(propName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                t = t.BaseType;
            }
            if (fi == null)
                throw new ArgumentOutOfRangeException("propName", $"Field {propName} was not found in Type {obj.GetType().FullName}");
            fi.SetValue(obj, val);
        }
    }
}
