﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Xtoman.Utility
{
    public static class TaskHelper
    {
        public static Task<Task> WithTimeout(this Task task, int millisecondsDelay)
        {
            return Task.WhenAny(task, Task.Delay(millisecondsDelay));
        }

        public static async Task<IEnumerable<Task>> GetComplatedTasks(this IEnumerable<Task<Task>> tasksOfTasks)
        {
            return (await Task.WhenAll(tasksOfTasks)).Where(p => p.Status == TaskStatus.RanToCompletion);
        }

        public static IEnumerable<T> ResultOfType<T>(this IEnumerable<Task> tasks)
        {
            return tasks.OfType<Task<T>>().Select(p => p.Result);
        }
    }
}
