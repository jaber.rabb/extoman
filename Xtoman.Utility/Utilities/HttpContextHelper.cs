﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using UAParser;

namespace Xtoman.Utility
{
    public class UserAgent
    {
#pragma warning disable IDE1006 // Naming Styles
        public string agent_type { get; set; }
        public string agent_name { get; set; }
        public string agent_version { get; set; }
        public string os_type { get; set; }
        public string os_name { get; set; }
        public string os_versionName { get; set; }
        public string os_versionNumber { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }

    public class UserAgentInfo
    {
        public string DeviceBrand { get; set; }
        public string DeviceModel { get; set; }
        public bool IsCrawler { get; set; }
        public string OsName { get; set; }
        public string OsVersion { get; set; }
        public string BrowserName { get; set; }
        public string BrowserVersion { get; set; }
    }

    public static class HttpContextHelper
    {
        public static bool IsPjaxRequest(this HttpRequest request) => request.Headers["X-PJAX"].HasValue();
        public static bool IsPjaxRequest(this HttpRequestBase request) => request.Headers["X-PJAX"].HasValue();

        public static void AddOnlineVisitor(this HttpApplicationState application)
        {
            if (application["Visitor"] == null)
                application["Visitor"] = 1;
            else
                application["Visitor"] = Convert.ToInt32(application["Visitor"]) + 1;
        }

        public static void ReduceOnlineVisitor(this HttpApplicationState application)
        {
            if (application["Visitor"] != null && Convert.ToInt32(application["Visitor"]) > 0)
            {
                application["Visitor"] = Convert.ToInt32(application["Visitor"]) - 1;
            }
        }

        public static long? GetCurrentVisitorId()
        {
            long? visitorId = null;
            var cookie = HttpContext.Current.Request.Cookies[AppSettingManager.VisitorCookieName];
            if (cookie != null)
            {
                try
                {
                    //.First(p => p.Value == "0")
                    visitorId = cookie.Values.ToList().Last().Key.UrlDecode().Decrypt(AppSettingManager.VisitorCookieEncryptPassword).ToInt();
                }
                catch { }
            }
            return visitorId;
        }

        public static UserAgentInfo ParseUserAgent(this HttpContext httpContext)
        {
            //return httpContext.HttpContextBase().ParseUserAgent();

            if (httpContext?.Request?.UserAgent == null)
                return null;
            var result = ParseUserAgent(httpContext?.Request?.UserAgent);
            return result;
        }

        public static UserAgentInfo ParseUserAgent(this HttpContextBase httpContext)
        {
            if (httpContext?.Request?.UserAgent == null)
                return null;
            var result = ParseUserAgent(httpContext?.Request?.UserAgent);
            return result;
        }

        public static UserAgentInfo ParseUserAgent(string userAgent)
        {
            //Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/7534.48.3
            //Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36
            //Mozilla/5.0 (Linux; Android 4.4.2; fa-ir; SAMSUNG SM-T331 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Version/1.5 Chrome/28.0.1500.94 Safari/537.36
            //Mozilla/5.0 (Linux; Android 6.0; LG-H540 Build/MRA58K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.124 Mobile Safari/537.36
            //Mozilla/5.0 (compatible; MJ12bot/v1.4.7; http://mj12bot.com/)
            //Mozilla/5.0 (Linux; Android 4.4.4; G620S-L02 Build/HuaweiG620S-L02) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/33.0.0.0 Mobile Safari/537.36

            var uaParser = Parser.GetDefault();
            var result = uaParser.Parse(userAgent);
            return new UserAgentInfo
            {
                DeviceBrand = result.Device.Brand,
                DeviceModel = result.Device.Model,
                IsCrawler = result.Device.IsSpider,
                OsName = result.OS.Family,
                OsVersion = string.Join(".", new[] { result.OS.Major, result.OS.Minor, result.OS.Patch }.Where(p => p != null)),
                BrowserName = result.UserAgent.Family,
                BrowserVersion = string.Join(".", new[] { result.UserAgent.Major, result.UserAgent.Minor, result.UserAgent.Patch }.Where(p => p != null))
            };
        }

        public static bool UrlIsLocal(this string url)
        {
            try
            {
                //Use absolute links. Not only will it make your on-site link navigation less prone to problems (like links to and from https pages), but if someone scrapes your content, you’ll get backlink juice out of it.
                return !new Uri(url).IsAbsoluteUri;
            }
            catch
            {
                return true;
            }
        }

        public static bool DownloadFile(string url, string path)
        {
            Stream stream = null;
            bool success;
            try
            {
                var req = (HttpWebRequest)WebRequest.Create(url);
                var response = req.GetResponse();
                stream = response.GetResponseStream();
                if (response.Headers.AllKeys.Contains("Content-Encoding")
                    && response.Headers["Content-Encoding"].Contains("gzip"))
                {
                    stream = new GZipStream(stream, CompressionMode.Decompress);
                }
                using (var fileStream = File.Create(path))
                {
                    stream.CopyTo(fileStream);
                }
                success = true;
            }
            catch
            {
                success = false;
            }
            finally
            {
                stream?.Dispose();
            }
            return success;
        }

        public static string OperatingSystem()
        {
            if (HttpContext.Current.Request.UserAgent.IndexOf("Windows 95") > 0)
                return "Windows 95";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Windows 98") > 0)
                return "Windows 98";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Windows NT 5.0") > 0)
                return "Windows 2000";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Windows NT 5.1") > 0)
                return "Windows XP";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Windows NT 5.2") > 0)
                return "Windows Server 2003";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Windows NT 6.0") > 0)
                return "Windows Vista";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Windows NT 6.1") > 0)
                return "Windows 7";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Windows NT 6.2") > 0)
                return "Windows 8";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Windows NT 6.3") > 0)
                return "Windows Server 2012";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Mac OS X") > 0)
                return "Mac OS X";
            else if (HttpContext.Current.Request.UserAgent.IndexOf("Linux") > 0)
                return "Linux";
            else
                return "none";
        }

        public static string Browser(HttpContextBase context = null)
        {
            if (context == null)
                return HttpContext.Current.Request.Browser.Browser + " V" + HttpContext.Current.Request.Browser.Version;
            return context.Request.Browser.Browser + " V" + context.Request.Browser.Version;
        }

        public static string IpAddress(this HttpContextBase context)
        {
            var ip = context?.Request.ServerVariables["REMOTE_ADDR"] ?? context?.Request.UserHostAddress ??
                HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] ?? HttpContext.Current.Request.UserHostAddress;

            if (ip == "localhost" || ip == "::1")
                ip = "127.0.0.1";
            return ip;
        }

        public static string HttpForwarded(this HttpContextBase context)
        {
            var ip = context?.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (ip == "localhost" || ip == "::1")
                ip = "127.0.0.1";
            return ip;
        }

        public static string UrlReferrer()
        {
            var reuest = HttpContext.Current.Request;
            return reuest.UrlReferrer?.AbsoluteUri;
        }

        public static UserAgent GetUserAgent()
        {
            var uas = HttpContext.Current.Request.UserAgent.UrlEncode();
            var api = $"http://www.useragentstring.com/?uas={uas}&getJSON=all";
            var str = new WebClient() { UseDefaultCredentials = true }.DownloadString(api);
            var userAgent = str.Deserialize<UserAgent>();
            return userAgent;
        }

        public static string GetUrl(string path)
        {
            var newPath = path;//.Substring(1, path.Length - 1);;
            if (string.IsNullOrEmpty(newPath) || newPath.Trim() == "/")
                newPath = Path.Combine("/", GetCurrentNeutralCulture());
            //newPath = string.Format("{0}{1}", @"\", CultureHelper.GetCurrentNeutralCulture());
            return newPath;
        }

        public static string GetCurrentNeutralCulture()
        {
            string name = Thread.CurrentThread.CurrentCulture.Name;

            if (!name.Contains("-")) return name;

            return name.Split('-')[0]; // Read first part only. E.g. "en", "es"
        }

        public static string GetCurrentPageUrl(this HttpRequest request)
        {
            return request.Url.AbsoluteUri.Replace(request.Url.PathAndQuery, "") + request.RawUrl;
        }

        public static HttpContextBase HttpContextBase(this HttpContext context)
        {
            return new HttpContextWrapper(context) as HttpContextBase;
        }

        public static string GetBaseUrl(this HttpContext context, bool excludeSubdomain = false)
        {
            return context.HttpContextBase().GetBaseUrl(excludeSubdomain);
        }

        public static string GetBaseUrl(this HttpContextBase context, bool excludeSubdomain = false)
        {
            var scheme = "https";
            var host = "www.extoman.co";
            var port = "";
            if (context?.Request?.Url != null)
            {
                var uri = context.Request.Url;
                port = uri.Port != 80 ? (":" + uri.Port) : "";
                scheme = uri.Scheme;
                host = excludeSubdomain ? GetHostWithoutSubdomain(uri.Host) : uri.Host;
            }
            if (AppSettingManager.Https)
                scheme = "https";
            return $"{scheme}://{host}{port}";
        }

        //public static string GetBaseUrl(this HttpContextBase context, bool advanced = false)
        //{
        //    if (advanced)
        //    {
        //        var scheme = context.Request.Url.Scheme;
        //        var host = context.Request.Url.Host;
        //        var port = context.Request.Url.Port == 80 ? "" : ":" + context.Request.Url.Port;
        //        var path = context.Request.ApplicationPath;
        //        var appPath = $"{scheme}://{host}{port}{path}";

        //        if (!appPath.EndsWith("/"))
        //            appPath += "/";
        //        return appPath;
        //    }
        //    else
        //    {
        //        return $"{context.Request.Url.Scheme}://{context.Request.Url.Host}/";
        //    }
        //}

        public static string GetHostWithoutSubdomain(string host)
        {
            return string.Join(".", host.Split('.').Reverse().Take(2).Reverse());
            //var host = new System.Uri(sURL).Host; new.xtoman.com
            //var domain = host.Substring(host.LastIndexOf('.', host.LastIndexOf('.') - 1) + 1);
        }

        public static string HtmlEncode(this string data)
        {
            return HttpUtility.HtmlEncode(data);
        }

        public static string HtmlDecode(this string data)
        {
            return HttpUtility.HtmlDecode(data);
        }

        public static NameValueCollection ParseQueryString(this string query)
        {
            return HttpUtility.ParseQueryString(query);
        }

        public static List<NameValueObject> ToList(this NameValueCollection collection)
        {
            var lst = new List<NameValueObject>();

            for (var i = 0; i < collection.Count; i++)
            {
                lst.Add(new NameValueObject(collection.Keys[i], collection[i]));
            }
            return lst;
        }

        public static IEnumerable<HttpCookie> Where(this HttpCookieCollection collection,
            Func<HttpCookie, bool> predicate)
        {
            var lst = new List<HttpCookie>();

            for (var i = 0; i < collection.Count; i++)
            {
                if (predicate(collection[i]))
                    lst.Add(collection[i]);
            }
            return lst.AsEnumerable();
        }

        public class NameValueObject
        {
            public string Key { get; set; }
            public string Value { get; set; }

            public NameValueObject(string key, string value)
            {
                Key = key;
                Value = value;
            }
        }

        public static string UrlEncode(this string url)
        {
            return HttpUtility.UrlEncode(url);
        }

        public static string UrlDecode(this string url)
        {
            return HttpUtility.UrlDecode(url);
        }

        public static string UrlPathEncode(this string url)
        {
            return HttpUtility.UrlPathEncode(url);
        }

        public static string MapPath(string path)
        {
            return HostingEnvironment.MapPath(path);
        }

        public static void DownloadToClient(this string fileName, HttpResponse response, bool inline = false)
        {
            response.Clear();
            response.ClearHeaders();
            response.ClearContent();
            response.BufferOutput = false;  // for large files
            response.ContentType = Path.GetExtension(fileName).TrimStart('.').GetMimeType();
            response.AddHeader("Content-Disposition", inline ? "inline;" : "attachment;filename=" + Path.GetFileName(fileName));
            response.AddHeader("Content-Length", new FileInfo(fileName).Length.ToString());
            //response.BinaryWrite(File.ReadAllBytes(filepath));
            //response.WriteFile(filepath);
            response.TransmitFile(fileName);
            response.Flush();
            response.End();
            response.Close();
        }

        public static void DownloadToClient(this Image img, HttpResponse response, ImageFormat format, string fileName = null)
        {
            response.Clear();
            response.ClearHeaders();
            response.ClearContent();
            response.BufferOutput = false;  // for large files
            response.ContentType = format.ToString().GetMimeType();
            response.AddHeader("Content-Disposition", "inline;" + (string.IsNullOrEmpty(fileName) ? "" : "filename=" + fileName));
            response.BinaryWrite(img.ToBytes(format));
            response.Flush();
            response.End();
            response.Close();
        }

        public static bool IsEmbeddedIntoAnotherDomain(this HttpRequestBase Request)
        {
            return Request.UrlReferrer != null && !Request.Url.Host.Equals(Request.UrlReferrer.Host, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
