﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Web.Helpers;
using Encoder = System.Drawing.Imaging.Encoder;
using Image = System.Drawing.Image;

namespace Xtoman.Utility
{
    public static class ImageHelper
    {
		public static Size GetImageSize(string fileName)
        {
            return Image.FromFile(fileName).Size;
        }

		public static Image Crop(this Image image, int width, int height, int x, int y)
        {
            var bmp = image as Bitmap;

            var cropBmp = bmp.Clone(new Rectangle(x, y, width, height), bmp.PixelFormat);
            image.Dispose();
            return cropBmp;
        }

        public static Font ToFont(this string fileName, int size = 20, FontStyle style = FontStyle.Regular)
        {
            var c = new PrivateFontCollection();
            c.AddFontFile(fileName);
            return new Font(c.Families[0], size, style);
        }

        public static Color SetTransparencyArgb(this Color color, int alpha)
        {
            return Color.FromArgb(alpha, color);
        }

        public static Color ToColor(this string str)
        {
            return ColorTranslator.FromHtml(str);
        }

        public static SizeF GetTextSize(string text, Font font)
        {
            Image img = new Bitmap(1, 1);

            Graphics drawing = Graphics.FromImage(img);
            return drawing.MeasureString(text, font);
        }

        public static Image DrawText(string text, Font font, Color textColor, Color backColor)
        {
            //first, create a dummy bitmap just to get a graphics object
            Image img = new Bitmap(1, 1);

            Graphics drawing = Graphics.FromImage(img);

            //measure the string to see how big the image needs to be
            SizeF textSize = drawing.MeasureString(text, font);

            //free up the dummy image and old graphics object
            img.Dispose();
            drawing.Dispose();

            //create a new image of the right size
            img = new Bitmap((int)textSize.Width, (int)textSize.Height);

            drawing = Graphics.FromImage(img);

            //paint the background
            drawing.Clear(backColor);

            //create a brush for the text
            Brush textBrush = new SolidBrush(textColor);

            drawing.SmoothingMode = SmoothingMode.AntiAlias;
            drawing.TextRenderingHint = TextRenderingHint.AntiAlias;

            var stringFormat = new StringFormat { FormatFlags = StringFormatFlags.DirectionRightToLeft, Alignment = StringAlignment.Far };
            drawing.DrawString(text, font, textBrush, 0, 0, stringFormat);

            drawing.Save();

            textBrush.Dispose();
            drawing.Dispose();

            return img;
        }

		public static byte[] AddWaterMark(this string filePath, string text)
        {
            using (var img = Image.FromFile(filePath))
            {
                using (var memStream = new MemoryStream())
                {
                    using (var bitmap = new Bitmap(img))//avoid gdi+ errors
                    {
                        bitmap.Save(memStream, ImageFormat.Png);
                        var webImage = new WebImage(memStream);
                        webImage.AddTextWatermark(text, verticalAlign: "Top", horizontalAlign: "Left", fontColor: "Brown");
                        return webImage.GetBytes();
                    }
                }
            }
        }

        public static bool CheckImageMaxSize(this Stream inputStream, int maxWidth, int maxHeight)
        {
            var img = Bitmap.FromStream(inputStream);

            if (img.Width > maxWidth || img.Height > maxHeight)
                return false;
            return true;
        }

        public static bool CheckImageMinSize(this Stream inputStream, int minWidth, int minHeight)
        {
            var img = Bitmap.FromStream(inputStream);

            if (img.Width < minWidth || img.Height < minHeight)
                return false;
            return true;
        }

        public static void CompressImage(this Image img, string path, ImageComperssion ic)
        {
            var qualityParam = new EncoderParameter(Encoder.Quality, Convert.ToInt32(ic));

            var format = img.RawFormat;

            var codec = ImageCodecInfo.GetImageDecoders().FirstOrDefault(c => c.FormatID == format.Guid);

            var mimeType = codec == null ? "image/jpeg" : codec.MimeType;

            ImageCodecInfo jpegCodec = null;

            var codecs = ImageCodecInfo.GetImageEncoders();

            for (var i = 0; i < codecs.Length; i++)
            {
                if (codecs[i].MimeType == mimeType)
                {
                    jpegCodec = codecs[i];
                    break;
                }
            }
            var encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;
            img.Save(path, jpegCodec, encoderParams);
        }

        public enum ImageComperssion
        {
            X1Maximum = 50,
            X2Good = 60,
            X3Normal = 70,
            X4Fast = 80,
            X5Minimum = 90
        }

        public static void SaveImage(this Stream inputStream, string savePath, ImageComperssion ic = ImageComperssion.X3Normal)
        {
            Image img = new Bitmap(inputStream);
            img.CompressImage(savePath, ic);
        }

        public static void ResizeImage(this Stream inputStream, int width, int height, string savePath, ImageComperssion ic = ImageComperssion.X3Normal)
        {
            Image img = Image.FromStream(inputStream);

            Image result = new Bitmap(width, height);

            using (var g = Graphics.FromImage(result))
            {
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.DrawImage(img, 0, 0, width, height);
            }
            result.CompressImage(savePath, ic);
        }

        public static void ResizeImage(this Image img, int width, int height, string savePath, ImageComperssion ic = ImageComperssion.X3Normal)
        {
            Image result = new Bitmap(width, height);

            using (var g = Graphics.FromImage(result))
            {
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.DrawImage(img, 0, 0, width, height);
            }
            result.CompressImage(savePath, ic);
        }

        public static void CompressImage(this Stream inputStream, string path, ImageComperssion ic = ImageComperssion.X3Normal)
        {
            var img = new Bitmap(inputStream);

            var qualityParam = new EncoderParameter(Encoder.Quality, Convert.ToInt32(ic));

            var format = img.RawFormat;

            var codec = ImageCodecInfo.GetImageDecoders().FirstOrDefault(c => c.FormatID == format.Guid);

            var mimeType = codec == null ? "image/jpeg" : codec.MimeType;

            var codecs = ImageCodecInfo.GetImageEncoders();

            var jpegCodec = codecs.FirstOrDefault(t => t.MimeType == mimeType);

            var encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;
            img.Save(path, jpegCodec, encoderParams);
        }

        public static void ResizeImageByWidth(this Stream inputStream, int width, string savePath, ImageComperssion ic = ImageComperssion.X3Normal)
        {
            Image img = new Bitmap(inputStream);

            var height = img.Height * width / img.Width;

            Image result = new Bitmap(width, height);

            using (var g = Graphics.FromImage(result))
            {
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.DrawImage(img, 0, 0, width, height);
            }
            result.CompressImage(savePath, ic);
        }

        public static void ResizeImageByWidth(this Image img, int width, string savePath, ImageComperssion ic = ImageComperssion.X3Normal)
        {
            var height = img.Height * width / img.Width;

            Image result = new Bitmap(width, height);

            using (var g = Graphics.FromImage(result))
            {
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.DrawImage(img, 0, 0, width, height);
            }
            result.CompressImage(savePath, ic);
        }

        public static void ResizeImageByHeight(this Stream inputStream, int height, string savePath, ImageComperssion ic = ImageComperssion.X3Normal)
        {
            Image img = new Bitmap(inputStream);

            var width = img.Width * height / img.Height;

            Image result = new Bitmap(width, height);

            using (var g = Graphics.FromImage(result))
            {
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.DrawImage(img, 0, 0, width, height);
            }
            result.CompressImage(savePath, ic);
        }

        public static void ResizeImageByHeight(this Image img, int height, string savePath, ImageComperssion ic = ImageComperssion.X3Normal)
        {
            var width = img.Width * height / img.Height;

            Image result = new Bitmap(width, height);

            using (var g = Graphics.FromImage(result))
            {
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.DrawImage(img, 0, 0, width, height);
            }
            result.CompressImage(savePath, ic);
        }
    }
}
