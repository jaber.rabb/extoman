﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Xtoman.Utility
{
    public class WwwRedirectHandler : MvcHandler
    {
        public WwwRedirectHandler(RequestContext requestContext) : base(requestContext)
        {
        }

        protected override void ProcessRequest(HttpContextBase httpContext)
        {
            if (httpContext.Request.IsLocal)
            {
                base.ProcessRequest(httpContext);
            }
            else
            {
                // check for domain without www
                if (!RequestContext.HttpContext.Request.Url.AbsoluteUri.Contains("://www."))
                {
                    var url = RequestContext.HttpContext.Request.Url.AbsoluteUri.Replace("://", "://www.");
                    httpContext.Response.Status = "301 Moved Permanently";
                    httpContext.Response.StatusCode = 301;
                    httpContext.Response.AppendHeader("Location", url);
                    return;
                }
                base.ProcessRequest(httpContext);
            }
        }
    }

    public class RedirectRouteHandler : IRouteHandler
    {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return new WwwRedirectHandler(requestContext);
        }

        /// <summary>
        /// <para>Assign route handler to all routes</para>
        /// </summary>
        /// <param name="routes">Routes</param>
        public static void Assign(RouteCollection routes)
        {
            using (routes.GetReadLock())
            {
                foreach (var route in routes.OfType<Route>().Where(r => !(r.RouteHandler is RedirectRouteHandler)))
                {
                    route.RouteHandler = new RedirectRouteHandler();
                }
            }
        }
    }
}
