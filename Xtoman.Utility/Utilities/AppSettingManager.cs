﻿using System;
using System.Security.Cryptography;
using System.Web.Configuration;

namespace Xtoman.Utility
{
    public static class AppSettingManager
    {
        #region Behpardakht
        private static int? _behpardakht_TerminalCode;
        private static string _behpardakht_Username;
        private static string _behpardakht_Password;
        public static int Behpardakht_TerminalCode => _behpardakht_TerminalCode ?? (_behpardakht_TerminalCode = GetInt("Behpardakht_TerminalCode")).Value;
        public static string Behpardakht_Username => _behpardakht_Username ?? (_behpardakht_Username = GetString("Behpardakht_Username"));
        public static string Behpardakht_Password => _behpardakht_Password ?? (_behpardakht_Password = GetString("Behpardakht_Password"));
        #endregion

        #region Saman
        private static string _sep_Password;
        private static string _sep_MerchandId;
        public static string Sep_Password => _sep_Password ?? (_sep_Password = GetString("Sep_Password"));
        public static string Sep_MerchandId => _sep_MerchandId ?? (_sep_MerchandId = GetString("Sep_MerchandId"));
        #endregion

        #region PerfectMoney
        private static string _perfectMoney_MemberId;
        private static string _perfectMoney_USD_Account;
        private static string _perfectMoney_Passphere;
        private static string _perfectMoney_Alt_Passphere;
        public static string PerfectMoney_MemberId => _perfectMoney_MemberId ?? (_perfectMoney_MemberId = GetHashedString("PerfectMoney_MemberId"));
        public static string PerfectMoney_USD_Account => _perfectMoney_USD_Account ?? (_perfectMoney_USD_Account = GetString("PerfectMoney_USD_Account"));
        public static string PerfectMoney_Passphere => _perfectMoney_Passphere ?? (_perfectMoney_Passphere = GetHashedString("PerfectMoney_Passphere"));
        public static string PerfectMoney_Alt_Passphere => _perfectMoney_Alt_Passphere ?? (_perfectMoney_Alt_Passphere = GetHashedString("PerfectMoney_Alt_Passphere"));
        #endregion

        #region PAYEER
        private static string _pAYEER_MerchantShopId;
        private static string _pAYEER_SecretKey;
        private static string _pAYEER_ApiId;
        private static string _pAYEER_AccountId;
        private static string _pAYEER_ApiSecretKey;
        public static string PAYEER_MerchantShopId => _pAYEER_MerchantShopId ?? (_pAYEER_MerchantShopId = GetString("PAYEER_MerchantShopId"));
        public static string PAYEER_SecretKey => _pAYEER_SecretKey ?? (_pAYEER_SecretKey = GetString("PAYEER_SecretKey"));
        public static string PAYEER_ApiId => _pAYEER_ApiId ?? (_pAYEER_ApiId = GetString("PAYEER_ApiId"));
        public static string PAYEER_AccountId => _pAYEER_AccountId ?? (_pAYEER_AccountId = GetString("PAYEER_AccountId"));
        public static string PAYEER_ApiSecretKey => _pAYEER_ApiSecretKey ?? (_pAYEER_ApiSecretKey = GetString("PAYEER_ApiSecretKey"));
        #endregion

        #region PayIR
        private static string _payIr_ApiId;
        private static string _payIR_PhoneNumber;
        private static string _payIR_Password;
        public static string PayIR_ApiId => _payIr_ApiId ?? (_payIr_ApiId = GetHashedString("PayIR_ApiId"));
        public static string PayIR_PhoneNumber => _payIR_PhoneNumber ?? (_payIR_PhoneNumber = GetHashedString("PayIR_PhoneNumber"));
        public static string PayIR_Password => _payIR_Password ?? (_payIR_Password = GetHashedString("PayIR_Password"));
        #endregion

        #region Vandar
        private static string _vandar_ApiKey;
        public static string Vandar_ApiKey => _vandar_ApiKey ?? (_vandar_ApiKey = GetHashedString("Vandar_ApiKey"));
        #endregion

        #region CoinPayments.net
        private static string _coinPayments_PrivateKey;
        private static string _coinPayments_PublicKey;
        private static string _coinPayments_MerchantId;
        private static string _coinPayments_IPNSecret;
        public static string CoinPayments_PrivateKey => _coinPayments_PrivateKey ?? (_coinPayments_PrivateKey = GetString("CoinPayments_PrivateKey"));
        public static string CoinPayments_PublicKey => _coinPayments_PublicKey ?? (_coinPayments_PublicKey = GetString("CoinPayments_PublicKey"));
        public static string CoinPayments_MerchantId => _coinPayments_MerchantId ?? (_coinPayments_MerchantId = GetString("CoinPayments_MerchantId"));
        public static string CoinPayments_IPNSecret => _coinPayments_IPNSecret ?? (_coinPayments_IPNSecret = GetString("CoinPayments_IPNSecret"));
        #endregion

        #region Changelly
        private static string _changelly_Key;
        private static string _changelly_Secret;
        public static string Changelly_Key => _changelly_Key ?? (_changelly_Key = GetString("Changelly_Key"));
        public static string Changelly_Secret => _changelly_Secret ?? (_changelly_Secret = GetString("Changelly_Secret"));
        #endregion

        #region Bitfinex
        private static string _bitfinex_Secret;
        private static string _bitfinex_Key;

        public static string Bitfinex_Secret => _bitfinex_Secret ?? (_bitfinex_Secret = GetString("Bitfinex_Secret"));
        public static string Bitfinex_Key => _bitfinex_Key ?? (_bitfinex_Key = GetString("Bitfinex_Key"));
        #endregion

        #region LocalBitcoins
        private static string _localBitcoins_Secret;
        private static string _localBitcoins_Key;

        public static string LocalBitcoins_Secret => _localBitcoins_Secret ?? (_localBitcoins_Secret = GetString("LocalBitcoins_Secret"));
        public static string LocalBitcoins_Key => _localBitcoins_Key ?? (_localBitcoins_Key = GetString("LocalBitcoins_Key"));
        #endregion

        #region PayamTak
        private static string _payamTak_UserName;
        private static string _payamTak_Password;
        private static string _payamTak_From;
        private static string _payamTak_FromPattern;

        public static string PayamTak_UserName => _payamTak_UserName ?? (_payamTak_UserName = GetString("PayamTak_UserName"));
        public static string PayamTak_Password => _payamTak_Password ?? (_payamTak_Password = GetString("PayamTak_Password"));
        public static string PayamTak_From => _payamTak_From ?? (_payamTak_From = GetString("PayamTak_From"));
        public static string PayamTak_FromPattern => _payamTak_FromPattern ?? (_payamTak_FromPattern = GetString("PayamTak_FromPattern"));
        #endregion

        #region Other
        private static bool? _lockBrowser;
        private static bool? _isLocal;
        private static bool? _logApi;
        private static bool? _https;
        private static bool? _forceToWww;
        private static bool? _forceToHttps;
        private static bool? _avoidTrailingSlash;
        private static string _xtomanVideo;
        private static string _cookieName = "bjhzgxcjhkjp";
        private static string _visitorCookieEncryptPassword = "xKs96n0O";

        public static string VisitorCookieName => _cookieName;
        public static string VisitorCookieEncryptPassword => _visitorCookieEncryptPassword;
        public static bool LockBrowser => _lockBrowser ?? (_lockBrowser = GetBoolean("LockBrowser")).Value;
        public static string XtomanVideo => _xtomanVideo ?? (_xtomanVideo = WebConfigurationManager.AppSettings["XtomanVideo"]);
        public static bool IsLocale => _isLocal ?? (_isLocal = GetBoolean("IsLocal")).Value;
        public static bool LogApi => _logApi ?? (_logApi = GetBoolean("LogApi")).Value;
        public static bool Https => _https ?? (_https = GetBoolean("Https")).Value;
        public static bool ForceToWww => _forceToWww ?? (_forceToWww = GetBoolean("ForceToWww")).Value;
        public static bool ForceToHttps => _forceToHttps ?? (_forceToHttps = GetBoolean("ForceToHttps")).Value;
        public static bool AvoidTrailingSlash => _avoidTrailingSlash ?? (_avoidTrailingSlash = GetBoolean("AvoidTrailingSlash")).Value;
        #endregion

        #region Binance
        private static string _binance_Secret;
        private static string _binance_Key;
        public static string Binance_Secret => _binance_Secret ?? (_binance_Secret = GetString("BinanceTest_Secret").HasValue() ? GetString("BinanceTest_Secret") : GetHashedString("Binance_Secret"));
        public static string Binance_Key => _binance_Key ?? (_binance_Key = GetString("BinanceTest_Key").HasValue() ? GetString("BinanceTest_Key") : GetHashedString("Binance_Key"));
        #endregion

        #region Kraken
        private static string _kraken_Secret;
        private static string _kraken_Key;
        private static string _kraken_Asset;
        public static string Kraken_Secret => _kraken_Secret ?? (_kraken_Secret = GetHashedString("Kraken_Secret"));
        public static string Kraken_Key => _kraken_Key ?? (_kraken_Key = GetHashedString("Kraken_Key"));
        public static string Kraken_Asset => _kraken_Asset ?? (_kraken_Asset = GetString("Kraken_Asset"));
        #endregion

        #region Finnotech
        private static string _finnotech_ClientId;
        private static string _finnotech_Secret;
        private static string _finnotech_ClientId2;
        private static string _finnotech_Secret2;
        private static string _finnotech_NationalCode;
        private static string _finnotech_AccountNumber;
        public static string Finnotech_ClientId => _finnotech_ClientId ?? (_finnotech_ClientId = GetHashedString("Fin_ClientId"));
        public static string Finnotech_Secret => _finnotech_Secret ?? (_finnotech_Secret = GetHashedString("Fin_Secret"));
        public static string Finnotech_ClientId2 => _finnotech_ClientId2 ?? (_finnotech_ClientId2 = GetHashedString("Fin_ClientId2"));
        public static string Finnotech_Secret2 => _finnotech_Secret2 ?? (_finnotech_Secret2 = GetHashedString("Fin_Secret2"));
        public static string Finnotech_NationalCode => _finnotech_NationalCode ?? (_finnotech_NationalCode = GetHashedString("Fin_NationalCode"));
        public static string Finnotech_AccountNumber => _finnotech_AccountNumber ?? (_finnotech_AccountNumber = GetHashedString("Fin_AccountNumber"));

        #endregion

        #region HitBTC
        private static string _hitbtc_Secret;
        private static string _hitbtc_Key;
        public static string HitBTC_Secret => _hitbtc_Secret ?? (_hitbtc_Secret = GetString("HitBTC_Secret"));
        public static string HitBTC_Key => _hitbtc_Key ?? (_hitbtc_Key = GetString("HitBTC_Key"));
        #endregion

        #region Binance Deposit Addresses
        private static string _binance_ADA_Address;
        private static string _binance_BTC_Address;
        private static string _binance_BCH_Address;
        private static string _binance_DASH_Address;
        private static string _binance_DCR_Address;
        private static string _binance_EOS_Address;
        private static string _binance_ETC_Address;
        private static string _binance_ETH_Address;
        private static string _binance_KMD_Address;
        private static string _binance_LTC_Address;
        private static string _binance_LSK_Address;
        private static string _binance_NAV_Address;
        private static string _binance_NEO_Address;
        private static string _binance_PIVX_Address;
        private static string _binance_STEEM_Address;
        private static string _binance_STRAT_Address;
        private static string _binance_USDT_Address;
        private static string _binance_WAVES_Address;
        private static string _binance_TRX_Address;
        private static string _binance_XEM_Address;
        private static string _binance_XLM_Address;
        private static string _binance_XMR_Address;
        private static string _binance_XVG_Address;
        private static string _binance_XZC_Address;
        private static string _binance_ZEC_Address;
        private static string _binance_XRP_Address;
        private static string _binance_STEEM_Tag;
        private static string _binance_EOS_Tag;
        private static string _binance_XEM_Tag;
        private static string _binance_XLM_Tag;
        private static string _binance_XMR_Tag;
        private static string _binance_XRP_Tag;
        private static string _binance_ZEN_Address;
        private static string _binance_QTUM_Address;
        private static string _binance_RVN_Address;
        private static string _binance_DOGE_Address;
        public static string Binance_ADA_Address => _binance_ADA_Address ?? (_binance_ADA_Address = GetString("Binance_ADA_Address"));
        public static string Binance_DOGE_Address => _binance_DOGE_Address ?? (_binance_DOGE_Address = GetString("Binance_DOGE_Address"));
        public static string Binance_BTC_Address => _binance_BTC_Address ?? (_binance_BTC_Address = GetString("Binance_BTC_Address"));
        public static string Binance_BCH_Address => _binance_BCH_Address ?? (_binance_BCH_Address = GetString("Binance_BCH_Address"));
        public static string Binance_DASH_Address => _binance_DASH_Address ?? (_binance_DASH_Address = GetString("Binance_DASH_Address"));
        public static string Binance_DCR_Address => _binance_DCR_Address ?? (_binance_DCR_Address = GetString("Binance_DCR_Address"));
        public static string Binance_EOS_Address => _binance_EOS_Address ?? (_binance_EOS_Address = GetString("Binance_EOS_Address"));
        public static string Binance_EOS_Tag => _binance_EOS_Tag ?? (_binance_EOS_Tag = GetString("Binance_EOS_Tag"));
        public static string Binance_ETC_Address => _binance_ETC_Address ?? (_binance_ETC_Address = GetString("Binance_ETC_Address"));
        public static string Binance_ETH_Address => _binance_ETH_Address ?? (_binance_ETH_Address = GetString("Binance_ETH_Address"));
        public static string Binance_KMD_Address => _binance_KMD_Address ?? (_binance_KMD_Address = GetString("Binance_KMD_Address"));
        public static string Binance_LTC_Address => _binance_LTC_Address ?? (_binance_LTC_Address = GetString("Binance_LTC_Address"));
        public static string Binance_LSK_Address => _binance_LSK_Address ?? (_binance_LSK_Address = GetString("Binance_LSK_Address"));
        public static string Binance_NAV_Address => _binance_NAV_Address ?? (_binance_NAV_Address = GetString("Binance_NAV_Address"));
        public static string Binance_NEO_Address => _binance_NEO_Address ?? (_binance_NEO_Address = GetString("Binance_NEO_Address"));
        public static string Binance_PIVX_Address => _binance_PIVX_Address ?? (_binance_PIVX_Address = GetString("Binance_PIVX_Address"));
        public static string Binance_STEEM_Address => _binance_STEEM_Address ?? (_binance_STEEM_Address = GetString("Binance_STEEM_Address"));
        public static string Binance_STEEM_Tag => _binance_STEEM_Tag ?? (_binance_STEEM_Tag = GetString("Binance_STEEM_Tag"));
        public static string Binance_STRAT_Address => _binance_STRAT_Address ?? (_binance_STRAT_Address = GetString("Binance_STRAT_Address"));
        public static string Binance_USDT_Address => _binance_USDT_Address ?? (_binance_USDT_Address = GetString("Binance_USDT_Address"));
        public static string Binance_WAVES_Address => _binance_WAVES_Address ?? (_binance_WAVES_Address = GetString("Binance_WAVES_Address"));
        public static string Binance_TRX_Address => _binance_TRX_Address ?? (_binance_TRX_Address = GetString("Binance_TRX_Address"));
        public static string Binance_XEM_Address => _binance_XEM_Address ?? (_binance_XEM_Address = GetString("Binance_XEM_Address"));
        public static string Binance_XLM_Address => _binance_XLM_Address ?? (_binance_XLM_Address = GetString("Binance_XLM_Address"));
        public static string Binance_XMR_Address => _binance_XMR_Address ?? (_binance_XMR_Address = GetString("Binance_XMR_Address"));
        public static string Binance_XVG_Address => _binance_XVG_Address ?? (_binance_XVG_Address = GetString("Binance_XVG_Address"));
        public static string Binance_XZC_Address => _binance_XZC_Address ?? (_binance_XZC_Address = GetString("Binance_XZC_Address"));
        public static string Binance_ZEC_Address => _binance_ZEC_Address ?? (_binance_ZEC_Address = GetString("Binance_ZEC_Address"));
        public static string Binance_XRP_Address => _binance_XRP_Address ?? (_binance_XRP_Address = GetString("Binance_XRP_Address"));
        public static string Binance_ZEN_Address => _binance_ZEN_Address ?? (_binance_ZEN_Address = GetString("Binance_ZEN_Address"));
        public static string Binance_QTUM_Address => _binance_QTUM_Address ?? (_binance_QTUM_Address = GetString("Binance_QTUM_Address"));
        public static string Binance_RVN_Address => _binance_RVN_Address ?? (_binance_RVN_Address = GetString("Binance_RVN_Address"));
        public static string Binance_XEM_Tag => _binance_XEM_Tag ?? (_binance_XEM_Tag = GetString("Binance_XEM_Tag"));
        public static string Binance_XLM_Tag => _binance_XLM_Tag ?? (_binance_XLM_Tag = GetString("Binance_XLM_Tag"));
        public static string Binance_XMR_Tag => _binance_XMR_Tag ?? (_binance_XMR_Tag = GetString("Binance_XMR_Tag"));
        public static string Binance_XRP_Tag => _binance_XRP_Tag ?? (_binance_XRP_Tag = GetString("Binance_XRP_Tag"));
        #endregion

        #region Helpers
        //private static string GetHashedString(string name, PaddingMode paddingMode, CipherMode cipherMode)
        //{
        //    var str = WebConfigurationManager.AppSettings[name] ?? "";
        //    return CryptographyHelper.DecryptData(str, "8FHZ`!/'S9e+T:e]", paddingMode, cipherMode);
        //}

        private static string GetHashedString(string name)
        {
            var str = WebConfigurationManager.AppSettings[name] ?? "";
            return CryptographyHelper.DecryptData(str, "8FHZ`!/'S9e+T:e]");
        }

        private static string GetString(string name)
        {
            return WebConfigurationManager.AppSettings[name] ?? "";
        }
        //private static decimal GetDecimal(string name)
        //{
        //    return WebConfigurationManager.AppSettings[name]?.ToDecimal() ?? 0;
        //}
        private static int GetInt(string name)
        {
            if (name.HasValue())
            {
                var stringValue = WebConfigurationManager.AppSettings[name];
                if (stringValue.HasValue())
                    return stringValue?.ToInt() ?? 0;
            }
            return 0;
        }
        private static bool GetBoolean(string name)
        {
            return WebConfigurationManager.AppSettings[name]?.Equals("true", StringComparison.OrdinalIgnoreCase) ?? false;
        }
        #endregion
    }
}
