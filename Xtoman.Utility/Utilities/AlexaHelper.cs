﻿using Fizzler.Systems.HtmlAgilityPack;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Xtoman.Utility
{
    public static class AlexaHelper
    {
        public static async Task<AlexaData> GetAlexaDataAsync(string websiteUrl)
        {
            websiteUrl = websiteUrl.RemoveStr("https://", "http://", "www.", "/");
            var url = $"http://www.alexa.com/siteinfo/{websiteUrl}";
            var doc = new HtmlWeb().Load(url);
            var globleRank = doc.DocumentNode.QuerySelector(".globleRank .metrics-data").InnerText.Trim().RemoveStr(",").ToInt();
            var countryRank = doc.DocumentNode.QuerySelector(".countryRank .metrics-data").InnerText.Trim().RemoveStr(",").ToInt();
            var imageUrl = doc.DocumentNode.QuerySelector("#rank-rank-rank").Attributes["src"].Value;
            var chartImage = await DownloadImage(imageUrl);
            var grow = doc.DocumentNode.QuerySelector(".globleRank .change-up").InnerText.Trim().RemoveStr(",").ToInt();
            var countries = doc.DocumentNode.QuerySelectorAll("#demographics_div_country_table tbody tr").Select(p =>
            {
                var country = p.QuerySelector("td:nth-child(1) a").InnerText.Trim();
                var percent = p.QuerySelector("td:nth-child(2) span ").InnerText.Trim().RemoveStr("%");
                var rank = p.QuerySelector("td:nth-child(3) span ").InnerText.Trim().RemoveStr(",");
                return new AlexaCountryData
                {
                    Country = country,
                    PercentOfVisitors = Convert.ToSingle(percent),
                    RankInCountry = rank.ToInt()
                };
            }).ToList();
            var a = doc.DocumentNode.QuerySelectorAll("#engagement-content .metrics-data").ToList();
            var bounceRate = Convert.ToSingle(a[0].InnerText.Trim().RemoveStr("%"));
            var pageviews = Convert.ToSingle(a[1].InnerText.Trim());
            var b = a[2].InnerText.Trim().Split(':');
            var dailyTime = new TimeSpan(b[0].ToInt(), b[1].ToInt(), 0);
            var keywords = doc.DocumentNode.QuerySelectorAll("#keywords_top_keywords_table tbody tr").Select(p =>
            {
                var keyword = p.QuerySelector("td:nth-child(1) span:nth-child(2)").InnerText.Trim();
                var percent = p.QuerySelector("td:nth-child(2) span ").InnerText.Trim().RemoveStr("%");
                return new AlexaKeywordData
                {
                    Keyword = keyword,
                    PercentOfSearchTraffic = Convert.ToSingle(percent)
                };
            }).ToList();

            return new AlexaData
            {
                BounceRate = bounceRate,
                ChartImage = chartImage,
                DailyPageviewsPerVisitor = pageviews,
                DailyTimeOnSite = dailyTime,
                GlobalRank = globleRank,
                Grow = grow,
                CountryRank = countryRank,
                TopCountries = countries,
                TopKeywords = keywords
            };
        }

        private static async Task<Image> DownloadImage(string imageUrl)
        {
            var client = new WebClient();
            var stream = await client.OpenReadTaskAsync(imageUrl);
            var bitmap = new Bitmap(stream);
            await stream.FlushAsync();
            stream.Close();
            client.Dispose();
            return bitmap;
        }
    }

    public class AlexaCountryData
    {
        public string Country { get; set; }
        public float PercentOfVisitors { get; set; }
        public int RankInCountry { get; set; }
    }

    public class AlexaKeywordData
    {
        public string Keyword { get; set; }
        public float PercentOfSearchTraffic { get; set; }
    }

    public class AlexaSubdomainData
    {
        public string Subdomain { get; set; }
        public float PercentOfVisitors { get; set; }
    }

    public class AlexaData
    {
        public int GlobalRank { get; set; }
        public int CountryRank { get; set; }
        public Image ChartImage { get; set; }
        public int Grow { get; set; }
        public float BounceRate { get; set; }
        public float DailyPageviewsPerVisitor { get; set; }
        public TimeSpan DailyTimeOnSite { get; set; }

        public List<AlexaCountryData> TopCountries { get; set; }
        public List<AlexaKeywordData> TopKeywords { get; set; }
    }
}
