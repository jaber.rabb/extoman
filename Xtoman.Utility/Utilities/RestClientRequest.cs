﻿using Xtoman.Utility;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RestSharp
{
    public class RestClientRequest : RestRequest
    {
        public RestClient Client;

        public RestClientRequest(string baseUrl, string action) : base(action)
        {
            Client = new RestClient(baseUrl);
        }

        public RestClientRequest(string baseUrl)
        {
            Client = new RestClient(baseUrl);
        }

        public IRestResponse SendPostRequest()
        {
            Method = Method.POST;
            var response = Client.Execute(this);
            if (response.StatusCode != HttpStatusCode.OK)
                new Exception($"StatusCode : {response.StatusCode} - Content: {response.Content} - Messag : {response.ErrorMessage}", response.ErrorException).LogError();
            return FixString(response);
        }

        public async Task<IRestResponse> SendPostRequestAsync()
        {
            Method = Method.POST;
            var response = await Client.ExecuteTaskAsync(this);
            if (response.StatusCode != HttpStatusCode.OK)
                new Exception($"StatusCode : {response.StatusCode} - Content: {response.Content} - Messag : {response.ErrorMessage}", response.ErrorException).LogError();
            return FixString(response);
        }

        public IRestResponse SendGetRequest()
        {
            Method = Method.GET;
            var response = Client.Execute(this);
            if (response.StatusCode != HttpStatusCode.OK)
                new Exception($"StatusCode : {response.StatusCode} - Content: {response.Content} - Messag : {response.ErrorMessage}", response.ErrorException).LogError();
            return FixString(response);
        }

        public async Task<IRestResponse> SendGetRequestAsync()
        {
            Method = Method.GET;
            var response = await Client.ExecuteTaskAsync(this);
            if (response.StatusCode != HttpStatusCode.OK)
                new Exception($"StatusCode : {response.StatusCode} - Content: {response.Content} - Messag : {response.ErrorMessage}", response.ErrorException).LogError();
            return FixString(response);
        }

        public IRestResponse SendDeleteRequest()
        {
            Method = Method.DELETE;
            var response = Client.Execute(this);
            if (response.StatusCode != HttpStatusCode.OK)
                new Exception($"StatusCode : {response.StatusCode} - Content: {response.Content} - Messag : {response.ErrorMessage}", response.ErrorException).LogError();
            return FixString(response);
        }

        public async Task<IRestResponse> SendDeleteRequestAsync()
        {
            Method = Method.DELETE;
            var response = await Client.ExecuteTaskAsync(this);
            if (response.StatusCode != HttpStatusCode.OK)
                new Exception($"StatusCode : {response.StatusCode} - Content: {response.Content} - Messag : {response.ErrorMessage}", response.ErrorException).LogError();
            return FixString(response);
        }

        public void AddJsonParameter(object value)
        {
            RequestFormat = DataFormat.Json;
            AddHeader("Content-Type", "application/json");
            AddHeader("Accept", "application/json");
            AddParameter("application/json", value.Serialize(), ParameterType.RequestBody);
            //AddJsonBody(value);
        }

        public void AddFormParameter(object value)
        {
            RequestFormat = DataFormat.Json;
            AddHeader("Content-Type", "application/x-www-form-urlencoded");
            //AddObject(value);
            //AddJsonBody(value);
            AddBody(value);
        }

        public IRestResponse FixString(IRestResponse response)
        {
            response.Content = response.Content.FixPersianChars().Fa2En();
            return response;
        }
    }
}
