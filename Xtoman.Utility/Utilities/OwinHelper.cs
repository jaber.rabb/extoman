﻿using Microsoft.Owin;

namespace Xtoman.Utility
{
    public static class OwinHelper
    {
        public static bool IsAjaxOwinRequest(this IOwinRequest request)
        {
            return request?.Query["X-Requested-With"] == "XMLHttpRequest" || request?.Headers["X-Requested-With"] == "XMLHttpRequest";
        }
    }
}
