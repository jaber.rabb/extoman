﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Xtoman.Utility
{
    public class MailSender
    {
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public string FromMail { get; set; }
        public string FromName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public List<Attachment> Attachments { get; set; }
        public bool Ssl { get; set; }


        private void SetConfig()
        {
            SmtpHost = "smtp.zoho.com";
            SmtpPort = 587;
            Username = "info@extoman.com";
            Password = "#261112aQ";
            FromMail = "info@extoman.com";
            FromName = "پشتیبانی ساوش";
            Ssl = true;
        }

        public MailSender()
        {
            SetConfig();
        }

        public MailSender(params string[] attachments)
        {
            SetConfig();
            Attachments = attachments.Select(p => new Attachment(p)).ToList();
        }

        public MailSender(params Attachment[] attachments)
        {
            SetConfig();
            Attachments = attachments.ToList();
        }

        public bool Send(string subject, string body, params string[] toMails)
        {
            try
            {
                var mailMsg = new MailMessage
                {
                    BodyEncoding = Encoding.UTF8,
                    HeadersEncoding = Encoding.UTF8,
                    SubjectEncoding = Encoding.UTF8,
                    Priority = MailPriority.High,
                    Subject = subject,
                    IsBodyHtml = true,
                    Body = body,
                    From = new MailAddress(FromMail, FromName, Encoding.UTF8),
                    Sender = new MailAddress(FromMail, FromName, Encoding.UTF8)
                };
                toMails.ToList().ForEach(p => mailMsg.To.Add(p));
                Attachments?.ForEach(p => mailMsg.Attachments.Add(p));
                var smtp = new SmtpClient(SmtpHost, SmtpPort)
                {
                    Credentials = new NetworkCredential(Username, Password),
                    EnableSsl = Ssl
                };
                smtp.Send(mailMsg);
                return true;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return false;
            }
        }
    }
}