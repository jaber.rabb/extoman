﻿using System;

namespace Xtoman.Utility
{
    public static class PersianDateTimeHelper
    {
        public static DateTime FirstOfPersianMonth(this DateTime dateTime)
        {
            var firstOfMonth = PersianDateTime.Now.FirstDayOfMonth;
            return firstOfMonth.ToDateTime();
        }

        public static DateTime LastOfPersianMonth(this DateTime dateTime)
        {
            var lastOfMonth = PersianDateTime.Now.LastDayOfMonth;
            return lastOfMonth.ToDateTime();
        }

        public static string PersianMonthName(this DateTime dateTime)
        {
            return new PersianDateTime(dateTime).MonthName;
        }

        public static int MonthDays(this DateTime dateTime)
        {
            return new PersianDateTime(dateTime).DaysInMonth;
        }
    }
}
