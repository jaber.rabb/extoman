﻿using System;
using System.Net;

namespace Xtoman.Utility
{
    public class WebClientWithTimeout : WebClient
    {
        private int _timeout;
        public WebClientWithTimeout(int timeoutMilliseconds) : base()
        {
            _timeout = timeoutMilliseconds;
        }
        protected override WebRequest GetWebRequest(Uri address)
        {
            var webRequest = base.GetWebRequest(address);
            webRequest.Timeout = _timeout > 0 ? _timeout : 5000; // timeout in milliseconds (ms)
            return webRequest;
        }
    }
}
