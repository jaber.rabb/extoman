﻿using Elmah;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;

namespace Xtoman.Utility
{
    public static class CommonUtility
    {
        public static string Domain => "extoman.co";
        public static T GetObject<T>(this IEnumerable<T> list) /*where T : new()*/
        {
            //return new T()
            return Activator.CreateInstance<T>();
        }

        public static void LogError(this Exception ex)
        {
            try
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
            catch
            {
                ErrorLog.GetDefault(null).Log(new Error(ex));
            }
        }

        public class FileNameResult
        {
            public string AbsolutePathForSave { get; set; }
            public string RelativePathForWeb { get; set; }
        }

        public static FileNameResult RenameFile(string fileName, string newName)
        {
            fileName = fileName.Trim().Replace('/', '\\').Trim('\\');
            var isPathRooted = Path.IsPathRooted(fileName);
            if (!isPathRooted)
                fileName = HttpContext.Current.Server.MapPath("\\" + fileName);

            var dir = Path.GetDirectoryName(fileName);
            var fileNameWithoutExt = Path.GetFileNameWithoutExtension(newName).CleanString();
            var trimedFileName = TrimFileForSave(fileNameWithoutExt);
            var ext = Path.GetExtension(fileName);
            var index = 2;
            if (File.Exists(dir + "\\" + trimedFileName + ext))
            {
                while (File.Exists(dir + "\\" + trimedFileName + " ({0})".Format(index) + ext))
                {
                    index++;
                }
                trimedFileName += " ({0})".Format(index);
            }
            var absolutePath = dir + "\\" + trimedFileName + ext;
            var root = HttpContext.Current.Server.MapPath("~").TrimEnd('\\');

            File.Move(fileName, absolutePath);

            return new FileNameResult
            {
                AbsolutePathForSave = absolutePath,
                RelativePathForWeb = absolutePath.Replace(root, "").Replace("\\", "/")
            };
        }

        public static string RootPath()
        {
            return HttpContext.Current.Server.MapPath("~").TrimEnd('\\').ToLower();
        }

        public static FileNameResult GetFileName(this HttpPostedFileBase file, bool isAdmin, string path, string format = "yyyy/MM/dd")
        {
            // Moshkele gereftane address e root
            var root = RootPath();
            if (isAdmin)
            {
                var parentDir = Directory.GetParent(root.EndsWith("\\") ? root : string.Concat(root, "\\")).Parent.FullName;
                root = parentDir;
            }
            var path2 = path.Replace("/", "\\").Trim('\\');

            string dir;
            if (format.HasValue())
            {
                var time = DateTime.Now.ToFaDateTimeZone(format).Replace("/", "\\").TrimEnd('\\');
                FolderPath(isAdmin, path2 + "\\" + time);
                dir = root + "\\" + path2 + "\\" + time;
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
            }
            else
            {
                FolderPath(isAdmin, path2);
                dir = root + "\\" + path2;
            }

            var fileNameWithoutExt = Path.GetFileNameWithoutExtension(file.FileName).CleanString();
            var trimedFileName = TrimFileForSave(fileNameWithoutExt);
            var ext = Path.GetExtension(file.FileName);

            var index = 2;
            if (File.Exists(dir + "\\" + trimedFileName + ext))
            {
                while (File.Exists(dir + "\\" + trimedFileName + " ({0})".Format(index) + ext))
                {
                    index++;
                }
                trimedFileName += " ({0})".Format(index);
            }

            var absolutePath = dir + "\\" + trimedFileName + ext;
            return new FileNameResult
            {
                AbsolutePathForSave = absolutePath,
                RelativePathForWeb = absolutePath.Replace(root, "").Replace("\\", "/")
            };
        }


        public static FileNameResult GetFileName(this HttpPostedFileBase file, string path, string format = "yyyy/MM/dd")
        {
            return GetFileName(file, false, path, format);
        }

        public static string FormatFileName(string filename, string format = "{0}\\{1}.{2}") //  GetDirectoryName\FileName.Extension
        {
            return string.Format(format, Path.GetDirectoryName(filename), Path.GetFileNameWithoutExtension(filename), Path.GetExtension(filename).TrimStart('.'));
        }

        public static string ToStaticFilePathAdmin(this string url)
        {
            return "C://inetpub/vhosts/extoman.co/httpdocs" + url;
        }

        public static string ToAbsoluteFilePath(this string url)
        {
            return $"{HttpContext.Current.Server.MapPath("~").TrimEnd('\\')}\\{url}";
        }

        public static string ToStaticUrl(this string url)
        {
            var strUrl = "https://static." + Domain + url.Replace("/static", "");
            return strUrl;
        }

        public static string ToStaticUrl(this string url, string subdomain = "", string prefix = "", string cdnOverwrite = "")
        {
            if (string.IsNullOrEmpty(url))
                return "";

            url = "/" + url.TrimStart('~').TrimStart('/');
            var urlEncoded = VirtualPathUtility.ToAbsolute(url).UrlPathEncode().TrimStart('/');
            if (AppSettingManager.IsLocale)
                return "http://localhost:19065/" + urlEncoded;

            cdnOverwrite = cdnOverwrite.TrimEnd('/');
            prefix = prefix.Trim('/');

            if (cdnOverwrite != "")
                return $"{cdnOverwrite}/{prefix}/{urlEncoded}";

            var baseUrl = HttpContext.Current.GetBaseUrl(true);
            if (subdomain != "")
                baseUrl = baseUrl.Insert(baseUrl.IndexOf("://") + 3, subdomain.TrimEnd('.') + ".");

            return $"{baseUrl}/{prefix}/{urlEncoded}";
        }

        public static string StripHtml(this string inputString)
        {
            foreach (Match m in Regex.Matches(inputString, "<img.+?src=[\"'](.+?)[\"'].+?>", RegexOptions.IgnoreCase | RegexOptions.Multiline))
            {
                inputString = inputString.Replace(m.Groups[1].Value, m.Groups[1].Value.ToStaticUrl());
            }
            return inputString;
        }

        public static string TrimFileForSave(string filename)
        {
            var arr = filename.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            var result = arr.Join("-")
                .ToLower()
                //.Replace("/", "")
                .Replace("[", "")
                .Replace("]", "")
                .Replace("{", "")
                .Replace("}", "")
                .Replace("(", "")
                .Replace(")", "")
                .Replace("!", "")
                .Replace("\\", "")
                .Replace(":", "")
                .Replace("*", "")
                .Replace("?", "")
                .Replace("\"", "")
                .Replace("<", "")
                .Replace(">", "")
                .Replace("|", "")
                .Replace("%", "")
                .Replace("~", "")
                .Replace("!", "")
                .Replace("@", "")
                .Replace("#", "")
                .Replace("$", "")
                .Replace("^", "")
                .Replace("&", "")
                .Replace("+", "")
                .Replace("=", "")
                .Replace(",", "")
                .Replace(";", "")
                .Replace("`", "")
                .Replace("'", "");
            return result;
        }

        public static string GetLink(string text, string url)
        {
            return "<a href=\"" + HttpContext.Current.GetBaseUrl() + url.TrimStart('/') + "\">" + text + "</a>";
        }

        public static string GetVirtualPath(this UrlHelper url, string absolutePath)
        {
            return url.Content(absolutePath.Replace(url.RequestContext.HttpContext.Request.PhysicalApplicationPath, "~/"));
        }

        public static string FolderPath(this string folderNames, bool isAdmin = false)
        {
            return FolderPath(isAdmin, folderNames.Trim('\\').Split('\\'));
        }

        public static string FolderPath(bool isAdmin = false, params string[] folderNames)
        {
            var root = RootPath();
            if (isAdmin)
            {
                var parentDir = Directory.GetParent(root.EndsWith("\\") ? root : string.Concat(root, "\\")).Parent.FullName; // It's OK
                root = parentDir;
            }

            foreach (var folderName in folderNames)
            {
                root += "\\" + folderName;
                if (!Directory.Exists(root))
                    Directory.CreateDirectory(root);
            }
            return root;
        }

        public static string ToUrl(this object picName, string noPicture, params string[] folderNames)
        {
            var path = "../";

            foreach (var folderName in folderNames)
            {
                path += folderName + "/";
            }
            path += (picName == null ? noPicture : picName.ToString());
            return path;
        }

        public static bool DeleteFile(this string filePath)
        {
            try
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*";

            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string RandomDigit(int length)
        {
            const string chars = "0123456789";

            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        /// <summary>
        /// Generate random digit code
        /// </summary>
        /// <param name="length">Length</param>
        /// <returns>Result string</returns>
        public static string GenerateRandomDigitCode(int length)
        {
            var random = new Random();
            string str = string.Empty;
            for (int i = 0; i < length; i++)
                str = string.Concat(str, random.Next(10).ToString());
            return str;
        }

        /// <summary>
        /// Returns an random interger number within a specified rage
        /// </summary>
        /// <param name="min">Minimum number</param>
        /// <param name="max">Maximum number</param>
        /// <returns>Result</returns>
        public static int GenerateRandomInteger(int min = 0, int max = int.MaxValue)
        {
            var randomNumberBuffer = new byte[10];
            new RNGCryptoServiceProvider().GetBytes(randomNumberBuffer);
            return new Random(BitConverter.ToInt32(randomNumberBuffer, 0)).Next(min, max);
        }

        public static string ToEnglishKeyboardKeys(this string str)
        {
            var charactersMap = CharactersMap();
            foreach (var item in CharactersMap())
                str = str.Replace(item.Value, item.Key);
            return str;
        }

        public static string ToPersianKeyboardKeys(this string str)
        {
            var charactersMap = CharactersMap();
            foreach (var item in CharactersMap())
                str.Replace(item.Key, item.Value);
            return str;
        }

        private static Dictionary<char, char> CharactersMap()
        {
            return new Dictionary<char, char>
            {
                { 'q', 'ض' },
                { 'w', 'ص' },
                { 'e', 'ث' },
                { 'r', 'ق' },
                { 't', 'ف' },
                { 'y', 'غ' },
                { 'u', 'ع' },
                { 'i', 'ه' },
                { 'o', 'خ' },
                { 'p', 'ح' },
                { '[', 'ج' },
                { ']', 'چ' },
                { 'a', 'ش' },
                { 's', 'س' },
                { 'd', 'ی' },
                { 'f', 'ب' },
                { 'g', 'ل' },
                { 'h', 'ا' },
                { 'j', 'ت' },
                { 'k', 'ن' },
                { 'l', 'م' },
                { ';', 'ک' },
                { '\'', 'گ' },
                { '\\', 'پ' },
                { 'z', 'ظ' },
                { 'x', 'ط' },
                { 'c', 'ز' },
                { 'v', 'ر' },
                { 'b', 'ذ' },
                { 'n', 'د' },
                { 'm', 'ئ' },
                { ',', 'و' },
                { 'T', '،' },
                { 'Y', '؛' },
                { 'U', ',' },
                { 'I', ']' },
                { 'O', '[' },
                { 'P', '\\' },
                { '{', '}' },
                { '}', '{' },
                { 'G', 'ۀ' },
                { 'H', 'آ' },
                { 'J', 'ـ' },
                { 'K', '«' },
                { 'L', '»' },
                { ':', ':' },
                { '"', '"' },
                { '|', '|' },
                { 'Z', 'ة' },
                { 'X', 'ي' },
                { 'C', 'ژ' },
                { 'V', 'ؤ' },
                { 'B', 'إ' },
                { 'N', 'أ' },
                { 'M', 'ء' },
                { '?', '؟' },
            };
        }

        public static string FixUrl(this string url, string seperator = "-")
        {
            var encoded = url.EmptyIfNull()
                .RemoveStr("%", ".", ":", "~", "/", "!", "@", "#", "$", "^", "&", "*", "(", ")", "'", "+", "=", "[", "]", "{", "}", ",", ";", "/", "|", "`", "<", ">", "\"", "\\");
            //    .Replace("%", "%".UrlEncode())
            //    .Replace(".", ".".UrlEncode())
            //    .Replace(":", ":".UrlEncode())
            //    .Replace("~", "~".UrlEncode())
            //    .Replace("/", "/".UrlEncode())
            //    .Replace("!", "!".UrlEncode())
            //    .Replace("@", "@".UrlEncode())
            //    .Replace("#", "#".UrlEncode())
            //    .Replace("$", "$".UrlEncode())
            //    .Replace("^", "^".UrlEncode())
            //    .Replace("&", "&".UrlEncode())
            //    .Replace("*", "*".UrlEncode())
            //    .Replace("(", "(".UrlEncode())
            //    .Replace(")", ")".UrlEncode())
            //    .Replace("'", "'".UrlEncode())
            //    .Replace("+", "+".UrlEncode())
            //    .Replace("=", "=".UrlEncode())
            //    .Replace("[", "[".UrlEncode())
            //    .Replace("]", "]".UrlEncode())
            //    .Replace("{", "{".UrlEncode())
            //    .Replace("}", "}".UrlEncode())
            //    .Replace(",", ",".UrlEncode())
            //    .Replace(";", ";".UrlEncode())
            //    .Replace("/", "/".UrlEncode())
            //    .Replace("|", "|".UrlEncode())
            //    .Replace("`", "`".UrlEncode())
            //    .Replace("<", "<".UrlEncode())
            //    .Replace(">", ">".UrlEncode())
            //    .Replace("\"", "\"".UrlEncode())
            //    .Replace("\\", "\\".UrlEncode());

            var arr = encoded.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            return arr.Join(seperator);
        }

        public static string SuggestUrl(this string url)
        {
            var str = url
                .Replace("%", "-")
                .Replace(".", "-")
                .Replace(":", "-")
                .Replace("~", "-")
                .Replace("/", "-")
                .Replace("!", "-")
                .Replace("@", "-")
                .Replace("#", "-")
                .Replace("$", "-")
                .Replace("^", "-")
                .Replace("&", "-")
                .Replace("*", "-")
                .Replace("(", "-")
                .Replace(")", "-")
                .Replace("'", "-")
                .Replace("+", "-")
                .Replace("=", "-")
                .Replace("[", "-")
                .Replace("]", "-")
                .Replace("{", "-")
                .Replace("}", "-")
                .Replace(",", "-")
                .Replace(";", "-")
                .Replace("`", "-")
                .Replace("<", "-")
                .Replace(">", "-")
                .Replace("|", "-")
                .Replace("/", "-")
                .Replace("\\", "-")
                .Replace("\"", "-")
                .Replace("?", "")
                .TrimEnd('-');

            var arr = str.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            return arr.Join("-").Left(60);
        }

        public static string SuggestUrlForFileName(this string url)
        {
            var str = url
                .Replace("%", "-")
                //.Replace(".", "-")
                .Replace(":", "-")
                .Replace("~", "-")
                .Replace("/", "-")
                .Replace("!", "-")
                .Replace("@", "-")
                .Replace("#", "-")
                .Replace("$", "-")
                .Replace("^", "-")
                .Replace("&", "-")
                .Replace("*", "-")
                //.Replace("(", "-")
                //.Replace(")", "-")
                .Replace("'", "-")
                .Replace("+", "-")
                .Replace("=", "-")
                .Replace("[", "-")
                .Replace("]", "-")
                .Replace("{", "-")
                .Replace("}", "-")
                .Replace(",", "-")
                .Replace(";", "-")
                .Replace("`", "-")
                .Replace("<", "-")
                .Replace(">", "-")
                .Replace("|", "-")
                .Replace("/", "-")
                .Replace("\\", "-")
                .Replace("\"", "-")
                .Replace("-", " ");

            var arr = str.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            return arr.Join("-").Left(60);
        }

        public static string BreadCrumbs(params object[] items)
        {
            var lst = new List<string>();

            foreach (var item in items)
            {
                var li = item.ToString();

                if (li != "")
                    lst.Add(li);
            }
            return string.Join(" > ", lst);
        }

        public static Uri ToTiny(this Uri longUri)
        {
            var url = longUri.ToString().UrlEncode();
            var request = WebRequest.Create($"http://tinyurl.com/api-create.php?url={url}");

            var response = request.GetResponse();

            Uri returnUri = null;

            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                returnUri = new Uri(reader.ReadToEnd());
            }
            return returnUri;
        }

        public static bool Not(this bool b)
        {
            return !b;
        }

        public static bool IsNotNull(this object obj)
        {
            return obj != null;
        }

        public static string DescriptionTag(this string text)
        {
            string result =
                text.Summary(1000, "")
                    .Filter()
                    .Replace(" از ", " ")
                    .Replace(" او ", " ")
                    .Replace(" برای ", " ")
                    .Replace(" با ", " ")
                    .Replace(" کی ", " ")
                    .Replace(" و ", " ")
                    .Replace(" به ", " ")
                    .Replace(" ما ", " ")
                    .Replace(" در ", " ")
                    .Replace(" تا ", " ")
                    .Replace(" را ", " ")
                    .Replace(" است ", " ")
                    .Replace(" که ", " ")
                    .Replace(" اگر ", " ")
                    .Replace(" برابر ", " ")
                    .Replace(" آن ", " ")
                    .Replace(" هر ", " ")
                    .Replace(" بر ", " ")
                    .Replace(" آنها ", " ");

            if (result.Length < 300) return result;
            return result.Substring(0, 300);
        }

        public static T SingleOrDefaultValue<T>(this IEnumerable<T> entities) where T : class, new()
        {
            var entity = entities.SingleOrDefault();
            return entity ?? new T();
        }

        public static T FirstOrDefaultValue<T>(this IEnumerable<T> entities) where T : class, new()
        {
            var entity = entities.FirstOrDefault();
            return entity ?? new T();
        }

        public static T If<T>(this bool predicate, T trueValue, T falseValue)
        {
            if (predicate)
                return trueValue;
            return falseValue;
        }

        public static T NewDefaultValue<T>(this T enitty) where T : class
        {
            enitty.GetType().GetProperties().ToList().ForEach(p =>
            {
                if (p.PropertyType == typeof(bool) || p.PropertyType == typeof(bool?))
                    p.SetValue(enitty, false, null);
                if (p.PropertyType == typeof(byte) || p.PropertyType == typeof(byte?))
                    p.SetValue(enitty, Convert.ToByte(0), null);
                if (p.PropertyType == typeof(Int16) || p.PropertyType == typeof(Int16?))
                    p.SetValue(enitty, Convert.ToInt16(0), null);
                if (p.PropertyType == typeof(Int32) || p.PropertyType == typeof(Int32?))
                    p.SetValue(enitty, Convert.ToInt32(0), null);
                if (p.PropertyType == typeof(Int64) || p.PropertyType == typeof(Int64?))
                    p.SetValue(enitty, Convert.ToInt64(0), null);
                if (p.PropertyType == typeof(Decimal) || p.PropertyType == typeof(Decimal?))
                    p.SetValue(enitty, Convert.ToDecimal(0), null);
                if (p.PropertyType == typeof(float) || p.PropertyType == typeof(float?))
                    p.SetValue(enitty, float.Parse("0"), null);
                if (p.PropertyType == typeof(Double) || p.PropertyType == typeof(Double?))
                    p.SetValue(enitty, Convert.ToDouble(0), null);
                if (p.PropertyType == typeof(DateTime) || p.PropertyType == typeof(DateTime?))
                    p.SetValue(enitty, new DateTime(), null);
                if (p.PropertyType == typeof(DateTimeOffset) || p.PropertyType == typeof(DateTimeOffset?))
                    p.SetValue(enitty, new DateTimeOffset(), null);
                if (p.PropertyType == typeof(string))
                    p.SetValue(enitty, "a", null);
                if (p.PropertyType == typeof(byte[]))
                    p.SetValue(enitty, new byte[] { }, null);
            });
            return enitty;
        }

        public static string ReplaceByFirstMatch(string pattern, string input, string output)
        {
            var regex = new Regex(pattern);
            if (regex.IsMatch(input))
                return string.Format(output, regex.Match(input).Value);
            return input;
        }

        /// <summary>
        /// Compare two arrasy
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="a1">Array 1</param>
        /// <param name="a2">Array 2</param>
        /// <returns>Result</returns>
        public static bool ArraysEqual<T>(T[] a1, T[] a2)
        {
            //also see Enumerable.SequenceEqual(a1, a2);
            if (ReferenceEquals(a1, a2))
                return true;

            if (a1 == null || a2 == null)
                return false;

            if (a1.Length != a2.Length)
                return false;

            var comparer = EqualityComparer<T>.Default;
            for (int i = 0; i < a1.Length; i++)
            {
                if (!comparer.Equals(a1[i], a2[i])) return false;
            }
            return true;
        }

        public static TResult Return<TInput, TResult>(this TInput o, Func<TInput, TResult> evaluator, TResult failureValue) where TInput : class
        {
            return o == null ? failureValue : evaluator(o);
        }

        public static Bundle AddStyleBundle(this Bundle bundle, params string[] paths)
        {
            foreach (var path in paths)
            {
                bundle = bundle.Include(path, new CssRewriteUrlTransform());
            }
            return bundle;
        }
    }
}