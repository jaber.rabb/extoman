﻿using System;

namespace Xtoman.Utility
{
    public static class NumberHelper
    {
        public static decimal RoundByStep(this decimal number, decimal step)
        {
            if (number > 0 && step > 0)
            {
                //Round Result
                var roundMath = 1 / step;
                number = Math.Round(number * roundMath, MidpointRounding.ToEven) / roundMath;
                if (number == 0)
                    number += step;
            }
            return number;
        }
    }
}
