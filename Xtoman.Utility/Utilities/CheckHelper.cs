﻿using System;

namespace Xtoman.Utility
{
    public static class CheckHelper
    {
        public static void NotNull<T>(T value, string parameterName) where T : class
        {
            if (value == null)
                throw new ArgumentNullException(parameterName);
        }

        public static void NotNull<T>(T? value, string parameterName) where T : struct
        {
            if (value == null)
                throw new ArgumentNullException(parameterName);
        }

        public static void NotEmpty(string value, string parameterName)
        {
            if (!value.HasValue())
                throw new ArgumentException(parameterName);
        }
    }
}
