﻿using Newtonsoft.Json;
using System;
using System.Linq;

namespace Xtoman.Utility
{
    public static class AppUrl
    {
        /// <summary>
        /// Custom url creator
        /// </summary>
        /// <param name="baseUrl">www.test.com/api</param>
        /// <param name="action">products</param>
        /// <param name="segments">{segmentname1}/{segmentname2}</param>
        /// <param name="inputs">new { id = 1 }</param>
        /// <returns></returns>
        public static Uri Create(string baseUrl, string action, string segments, object inputs = null)
        {
            baseUrl = baseUrl.Trim(new char[] { '/', '\\', ' ' });
            action = action.Trim(new char[] { '/', '\\', ' ' });
            segments = segments.Trim(new char[] { '/', '\\', ' ' });
            var url = $"{baseUrl}/{action}/{segments}";
            var parameters = "";
            if (inputs != null)
            {
                parameters = "?";
                var validProperties = inputs.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                foreach (var property in validProperties)
                {
                    var value = property.GetValue(inputs)?.ToString() ?? "";
                    var segName = "{" + property.Name + "}";
                    if (value.HasValue())
                    {
                        if (segments.Contains(segName))
                            url = url.Replace(segName, value);
                        else
                            parameters += $"{property.Name}={value}&";
                    }
                }
                if (parameters.Length > 1)
                    parameters = parameters.TrimEnd('&');
            }
            return new Uri(url + parameters);
        }
    }
}
