﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Adapters;

namespace Xtoman.Utility
{
    public static class WebFormHelper
    {
        public static ListItemCollection GetSelectedItems(this ListBox listBox)
        {
            var items = new ListItemCollection();
            foreach (ListItem item in listBox.Items)
            {
                if (item.Selected) items.Add(item);
            }
            return items;
        }

        public static List<int> GetSelectedValues(this ListItemCollection lic)
        {
            var arr = new List<int>();
            foreach (ListItem item in lic)
            {
                if (item.Selected) arr.Add(item.Value.ToInt());
            }
            return arr;
        }

        public static void TableToExcel<T>(this IEnumerable<T> enumerable)
        {
            var Response = HttpContext.Current.Response;
            const string FileName = "Report.xls";
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            //Response.ContentType = "application/vnd.xls"
            //Response.ContentEncoding = System.Text.Encoding.UTF8 : Response.Charset = "UTF8"
            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + FileName + "\"");
            using (var SW = new StringWriter())
            {
                using (var TW = new HtmlTextWriter(SW))
                {
                    var DG = new DataGrid()
                    {
                        DataSource = enumerable
                    };
                    DG.DataBind();
                    DG.RenderControl(TW);
                    Response.Write(SW.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
        }

        public static void TableToExcel(this DataTable table)
        {
            var Response = HttpContext.Current.Response;
            const string FileName = "Report.xls";
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            //Response.ContentType = "application/vnd.xls"
            //Response.ContentEncoding = System.Text.Encoding.UTF8 : Response.Charset = "UTF8"
            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + FileName + "\"");
            using (var SW = new StringWriter())
            {
                using (var TW = new HtmlTextWriter(SW))
                {
                    var DG = new DataGrid()
                    {
                        DataSource = table
                    };
                    DG.DataBind();
                    DG.RenderControl(TW);
                    Response.Write(SW.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
        }

        public static bool DataBind(this ListControl control, object datasource, string textField, string valueField)
        {
            return DataBind(control, datasource, textField, null, valueField);
        }

        public static bool DataBind(this ListControl control, object datasource, string textField,
            string textFieldFormat, string valueField)
        {
            control.DataTextField = textField;
            control.DataValueField = valueField;

            if (textFieldFormat.HasValue())
                control.DataTextFormatString = textFieldFormat;

            control.DataSource = datasource;
            control.DataBind();

            if (control.Items.Count > 0)
            {
                control.SelectedIndex = 0;
                return true;
            }
            return false;
        }

        public static void AddJs(this Page page, string src)
        {
            page.Header.Controls.Add(new Literal { Text = "<script type='text/javascript' src='" + src + "'></script>\n" });
        }

        public static void AddCss(this Page page, string src)
        {
            page.Header.Controls.Add(new Literal { Text = "<link href='" + src + "' rel='stylesheet' />\n" });
        }

        /// <summary>
        /// window.open("login.html", "n2", "fullScreen=yes,width=450,height=550,top=100,left=250,resizable=no,scrollbars=no,menubar=no,toolbar=no,status=no,titlebar=no,location=no,border=1")
        /// </summary>
        public static void OpenPopUp(this WebControl Control, string EventCont, string PagePath, string PageName,
            string fullScreen, string width, string height, string top, string left, string resizable, string scrollbars,
            string menubar, string toolbar, string status, string titlebar, string location, string border)
        {
            var StrJavaScript = "window.open('" + PagePath + "','" + PageName + "',";
            StrJavaScript += "'fullScreen=" + fullScreen + ",width=" + width + ",height=" + height;
            StrJavaScript += ",top=" + top + ",left=" + left + ",resizable=" + resizable;
            StrJavaScript += ",scrollbars=" + scrollbars + ",menubar=" + menubar + ",toolbar=" + toolbar;
            StrJavaScript += ",status=" + status + ",titlebar=" + titlebar + ",location=" + location + ",border=" +
                             border + "')";

            Control.Attributes.Add(EventCont, StrJavaScript);
        }

        public enum MetaType
        {
            KeyWords,
            Description,
            Author,
            Robots,
            Copyright,
            Distribution,
            Expires,
            Refresh,
            ContentType,
            Charset,
            RevisitAfter,
            Language
        }

        public static void AddMetaTag(this Page page, MetaType type, string str)
        {
            //page.Header.Controls.Add(new Literal { Text = "<meta name=\"" + type.ToString().ToLower() + "\" content=\"" + str + "\" />\n" });

            var meta = new HtmlMeta();
            switch (type)
            {
                case MetaType.RevisitAfter:
                    meta.Name = "revisit-after";
                    meta.Content = str + " Days";
                    break;
                case MetaType.Language:
                    meta.Name = type.ToString().ToLower();
                    meta.Content = "fa-IR";
                    break;
                case MetaType.ContentType:
                    meta.HttpEquiv = "content-type";
                    meta.Content = "text/html; charset=utf-8";
                    break;
                case MetaType.Refresh:
                    meta.HttpEquiv = "refresh";
                    meta.Content = str;
                    break;
                case MetaType.Charset:
                    meta.Attributes["charset"] = "utf-8";
                    break;
                default:
                    meta.Name = type.ToString().ToLower();
                    meta.Content = str;
                    break;
            }
            page.Header.Controls.Add(meta);
        }

        private static readonly Hashtable m_executingPages = new Hashtable();

        public static void Show(this string sMessage)
        {
            // If this is the first time a page has called this method then
            if (!m_executingPages.Contains(HttpContext.Current.Handler))
            {
                // Attempt to cast HttpHandler as a Page.
                if (HttpContext.Current.Handler is Page executingPage)
                {
                    // Create a Queue to hold one or more messages.
                    var messageQueue = new Queue();
                    // Add our message to the Queue
                    messageQueue.Enqueue(sMessage);
                    // Add our message queue to the hash table. Use our page reference
                    // (IHttpHandler) as the key.
                    m_executingPages.Add(HttpContext.Current.Handler, messageQueue);
                    // Wire up Unload event so that we can inject
                    // some JavaScript for the alerts.
                    executingPage.Unload += ExecutingPage_Unload;
                }
            }
            else
            {
                // If were here then the method has allready been
                // called from the executing Page.
                // We have allready created a message queue and stored a
                // reference to it in our hastable.
                var queue = (Queue)m_executingPages[HttpContext.Current.Handler];
                // Add our message to the Queue
                queue.Enqueue(sMessage);
            }
        }

        // Our page has finished rendering so lets output the
        // JavaScript to produce the alert's
        private static void ExecutingPage_Unload(object sender, EventArgs e)
        {
            // Get our message queue from the hashtable
            var queue = (Queue)m_executingPages[HttpContext.Current.Handler];
            if (queue != null)
            {
                var sb = new StringBuilder();
                // How many messages have been registered?
                var iMsgCount = queue.Count;
                // Use StringBuilder to build up our client slide JavaScript.
                sb.Append("<script language='javascript'>");
                // Loop round registered messages
                string sMsg;
                while (iMsgCount-- > 0)
                {
                    sMsg = (string)queue.Dequeue();
                    sMsg = sMsg.Replace("\n", "\\n");
                    sMsg = sMsg.Replace("\"", "'");
                    sb.Append(@"alert( """ + sMsg + @""" );");
                }
                // Close our JS
                sb.Append("</script>");
                // Were done, so remove our page reference from the hashtable
                m_executingPages.Remove(HttpContext.Current.Handler);
                // Write the JavaScript to the end of the response stream.
                HttpContext.Current.Response.Write(sb.ToString());
            }
        }

        public static void RedirectWithDelay(this Page page, string url, int delay = 3)
        {
            var script = "<script type='text/javascript'>setTimeout(function() { window.location.replace('" + url + "'); }, " + (delay * 1000).ToString() + ");</script>";
            page.ClientScript.RegisterStartupScript(page.GetType(), "RedirectWithDelay", script);
            ScriptManager.RegisterStartupScript(page, page.GetType(), "RedirectWithDelay", script, false);
        }
    }

    /* This codes makes the dropdownlist control recognize items with "--"
     * for the label or items with an OptionGroup attribute and render them
     * as <optgroup> instead of <option>.
     */
    public class DropDownListAdapter : WebControlAdapter
    {
        protected override void RenderContents(HtmlTextWriter writer)
        {
            //System.Web.HttpContext.Current.Response.Write("here");
            var list = (ListBox)Control;

            var renderedOptionGroups = new List<string>();

            foreach (ListItem item in list.Items)
            {
                Page.ClientScript.RegisterForEventValidation(list.UniqueID, item.Value);
                //Is the item part of an option group?
                if (item.Value == "OptionGroup")
                {
                    var currentOptionGroup = item.Text;
                    //Was the option header already written, then just render the list item
                    if (renderedOptionGroups.Contains(currentOptionGroup))
                    {
                        RenderListItem(item, writer);
                    }
                    //The header was not written,do that first
                    else
                    {
                        //Close previous group
                        if (renderedOptionGroups.Count > 0)
                            RenderOptionGroupEndTag(writer);

                        RenderOptionGroupBeginTag(currentOptionGroup, writer);
                        renderedOptionGroups.Add(currentOptionGroup);
                        //RenderListItem(item, writer);
                    }
                }
                //Simple separator
                else if (item.Text == "--")
                {
                    RenderOptionGroupBeginTag("--", writer);
                    RenderOptionGroupEndTag(writer);
                }
                //Default behavior, render the list item as normal
                else
                {
                    RenderListItem(item, writer);
                }
            }

            if (renderedOptionGroups.Count > 0)
                RenderOptionGroupEndTag(writer);
        }

        private void RenderOptionGroupBeginTag(string name, HtmlTextWriter writer)
        {
            writer.WriteBeginTag("optgroup");
            writer.WriteAttribute("label", name);
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
        }

        private void RenderOptionGroupEndTag(HtmlTextWriter writer)
        {
            writer.WriteEndTag("optgroup");
            writer.WriteLine();
        }

        private void RenderListItem(ListItem item, HtmlTextWriter writer)
        {
            writer.WriteBeginTag("option");
            writer.WriteAttribute("value", item.Value, true);
            if (item.Selected)
                writer.WriteAttribute("selected", "selected", false);

            foreach (string key in item.Attributes.Keys)
                writer.WriteAttribute(key, item.Attributes[key]);

            writer.Write(HtmlTextWriter.TagRightChar);
            HttpUtility.HtmlEncode(item.Text, writer);
            writer.WriteEndTag("option");
            writer.WriteLine();
        }
    }
}
