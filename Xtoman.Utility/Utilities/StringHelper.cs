﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text.RegularExpressions;
using Xtoman.Utility.SeoHelper;

namespace Xtoman.Utility
{
    public static class StringHelper
    {
        private static readonly HashSet<char> DefaultNonWordCharacters = new HashSet<char> { '،', ',', '.', ':', ';', '؛' };

        /// <summary>
        /// Returns a substring from the start of <paramref name="value"/> no 
        /// longer than <paramref name="length"/>.
        /// Returning only whole words is favored over returning a string that 
        /// is exactly <paramref name="length"/> long. 
        /// </summary>
        /// <param name="value">The original string from which the substring 
        /// will be returned.</param>
        /// <param name="length">The maximum length of the substring.</param>
        /// <param name="nonWordCharacters">Characters that, while not whitespace, 
        /// are not considered part of words and therefor can be removed from a 
        /// word in the end of the returned value. 
        /// Defaults to ",", ".", ":" and ";" if null.</param>
        /// <exception cref="System.ArgumentException">
        /// Thrown when <paramref name="length"/> is negative
        /// </exception>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown when <paramref name="value"/> is null
        /// </exception>
        public static string CropWholeWords(
          this string value,
          int length,
          HashSet<char> nonWordCharacters = null)
        {
            if (value == null)
                throw new ArgumentNullException("value");

            if (length < 0)
                throw new ArgumentException("Negative values not allowed.", "length");

            if (nonWordCharacters == null)
                nonWordCharacters = DefaultNonWordCharacters;

            if (length >= value.Length)
                return value;

            int end = length;

            for (int i = end; i > 0; i--)
            {
                if (value[i].IsWhitespace())
                    break;

                if (nonWordCharacters.Contains(value[i])
                    && (value.Length == i + 1 || value[i + 1] == ' '))
                    //Removing a character that isn't whitespace but not part 
                    //of the word either (ie ".") given that the character is 
                    //followed by whitespace or the end of the string makes it
                    //possible to include the word, so we do that.
                    break;

                end--;
            }

            if (end == 0)
                //If the first word is longer than the length we favor 
                //returning it as cropped over returning nothing at all.
                end = length;

            return value.Substring(0, end);
        }

        public static string Replace(this string str, string oldVal, string newVal, StringComparison comparison)
        {
            newVal = newVal ?? "";
            if (string.IsNullOrEmpty(str) || string.IsNullOrEmpty(oldVal) || oldVal.Equals(newVal, comparison))
                return str;
            int foundAt = 0;
            while ((foundAt = str.IndexOf(oldVal, foundAt, comparison)) != -1)
            {
                str = str.Remove(foundAt, oldVal.Length).Insert(foundAt, newVal);
                foundAt += newVal.Length;
            }
            return str;
        }

        public static string Replace(this string str, string oldVal, string newVal, bool ignoreCase)
        {
            if (string.IsNullOrEmpty(str) || string.IsNullOrEmpty(oldVal) || oldVal.Equals(newVal, StringComparison.InvariantCultureIgnoreCase))
                return str;
            if (ignoreCase)
            {
                return Regex.Replace(str, oldVal, newVal, RegexOptions.IgnoreCase);
            }
            return str.Replace(oldVal, newVal);
        }

        private static bool IsWhitespace(this char character)
        {
            return character == ' ' || character == 'n' || character == 't';
        }

        /// <summary>
        /// Validates if string is a valid JSON
        /// </summary>
        /// <param name="stringValue">String to validate</param>
        /// <returns></returns>
        public static bool IsValidJson(this string stringValue)
        {
            if (string.IsNullOrWhiteSpace(stringValue))
            {
                return false;
            }

            var value = stringValue.Trim();

            if ((value.StartsWith("{") && value.EndsWith("}")) || //For object
                (value.StartsWith("[") && value.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(value);
                    return true;
                }
                catch (JsonReaderException)
                {
                    return false;
                }
            }

            return false;
        }

        public static string ReplaceMany(this string str, string[] replaceParams, string with)
        {
            var result = str;
            foreach (var item in replaceParams)
            {
                result = str.Replace(item, with);
            }
            return result;
        }

        public static bool HasValue(this string value, bool ignoreWhiteSpace = true)
        {
            return !(ignoreWhiteSpace ? string.IsNullOrWhiteSpace(value) : string.IsNullOrEmpty(value));
        }

        public static IEnumerable<string> SplitTextWordsByLenght(string text, int lenght)
        {
            var words = text.GetWords();
            var temp = new List<string>();
            var list = new List<string>();
            foreach (var item in words)
            {
                var len = string.Join(" ", temp.Concat(new[] { item })).Length;
                if (len > lenght)
                {
                    list.Add(string.Join(" ", temp));
                    temp.Clear();
                }
                temp.Add(item);
            }
            list.Add(string.Join(" ", temp));
            return list;
        }

        public static string GetNumbers(string input)
        {
            return new string(input.Where(c => char.IsDigit(c)).ToArray());
        }

        public static IEnumerable<string> SplitTextWordByCount(string text, int count)
        {
            var words = text.GetWords();
            var len = Math.Ceiling(words.Length / (decimal)count);
            for (int i = 0; i < len; i++)
            {
                yield return string.Join(" ", words.Skip(i * count).Take(count));
            }
        }

        public static List<KeyValuePair<string, string>> GetParameters(string parameters)
        {
            return parameters.Split('/').Select(p =>
            {
                if (p.Contains("-"))
                {
                    var arr = p.Split('-');
                    return new KeyValuePair<string, string>(arr[0].ToLower(), arr[1]);
                }
                return new KeyValuePair<string, string>(null, p);
            }).ToList();
        }

        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }

        public static string GetFirstWord(this string str)
        {
            var arr = str.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            return arr.Length >= 1 ? arr[0] : "";
        }

        public static string NullIfEmpty(this string str)
        {
            return str?.Length == 0 ? null : str;
        }

        public static string EmptyIfNull(this string str)
        {
            return str ?? "";
        }

        public static string Summary(this string str, int lenght, bool hasDot = false)
        {
            if (str.Length < lenght) return str;
            else return str.Substring(0, lenght - (hasDot ? 4 : 0)) + (hasDot ? " ..." : "");
        }

        public static string Summary(this string str, int lenght, string searchText)
        {
            var j = 0;

            try
            {
                str = str.Replace("●", " ");
                for (var i = 1; i <= str.Length; i++)
                {
                    if (i > str.Length)
                    {
                        break;
                    }
                    j++;
                    if (str.Substring(j - 1, 1) == "<")
                    {
                        string Temp = str.Substring(j - 1, 1);
                        j++;
                        while (str.Substring(j - 1, 1) != ">")
                        {
                            Temp += str.Substring(j - 1, 1);
                            j++;
                        }
                        Temp += str.Substring(j - 1, 1);
                        str = str.Replace(Temp, "");
                        j = 0;
                        i = 0;
                    }
                }
            }
            catch { }

            if (searchText.HasValue())
            {
                str = str.Replace(searchText, "<b><span style='color:red'>" + searchText + "</span></b>");
            }
            if (str.Length < lenght)
                return str;
            return str.Substring(0, lenght);
        }

        /// <summary>
        /// Checks string object's value to array of string values
        /// </summary>
        /// <param name="str"></param>       
        /// <param name="stringValues">Array of string values to compare</param>
        /// <returns>Return true if any string value matches</returns>
        public static bool In(this string str, params string[] stringValues)
        {
            foreach (var item in stringValues)
            {
                if (string.Compare(str, item, StringComparison.OrdinalIgnoreCase) == 0)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Returns characters from right of specified length
        /// </summary>
        /// <param name="str">String value</param>
        /// <param name="length">Max number of charaters to return</param>
        /// <returns>Returns string from right</returns>
        public static string Right(this string str, int length)
        {
            return str?.Length > length ? str.Substring(str.Length - length) : str;
        }

        /// <summary>
        /// Returns characters from left of specified length
        /// </summary>
        /// <param name="str">String value</param>
        /// <param name="length">Max number of charaters to return</param>
        /// <returns>Returns string from left</returns>
        public static string Left(this string str, int length)
        {
            return str?.Length > length ? str.Substring(0, length) : str;
        }

        public static string RemoveLeft(this string str, int length)
        {
            return str.Remove(0, length);
        }

        public static string RemoveRight(this string str, int length)
        {
            return str.Remove(str.Length - length);
        }

        /// <summary>
        ///  Replaces the format item in a specified System.String with the text equivalent
        ///  of the value of a specified System.Object instance.
        /// </summary>
        /// <param name="str">A composite format string</param>
        /// <param name="args">An System.Object array containing zero or more objects to format.</param>
        /// <returns>A copy of format in which the format items have been replaced by the System.String
        /// equivalent of the corresponding instances of System.Object in args.</returns>
        public static string Format(this string str, params object[] args)
        {
            return string.Format(str, args);
        }

        public static string Join(this IEnumerable<string> arr, string separator)
        {
            return string.Join(separator, arr);
        }

        public static string DeleteChars(this string str, params char[] chars)
        {
            return new string(str.Where(ch => !chars.Contains(ch)).ToArray());
        }

        public static string DeleteStrs(this string str, params string[] strs)
        {
            foreach (var item in strs)
                str = str.Replace(item, "");
            return str;
        }

        public static string ToLowerFirst(this string str)
        {
            char[] a = str.ToCharArray();
            a[0] = char.ToLower(a[0]);
            return new string(a);
            //return str.Substring(0, 1).ToLower() + str.Substring(1);
        }

        public static string ToUpperFirst(this string str)
        {
            char[] a = str.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
            //return str.Substring(0, 1).ToUpper() + (str.Length > 1 ? str.Substring(1) : "");
        }

        public static string ToUpperFirstWords(this string str)
        {
            return CultureHelper.GetCultureInfo("en-US").TextInfo.ToTitleCase(str.ToLower());
        }

        /// <summary>
        /// Returns the first string with a non-empty non-null value.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="alternative"></param>
        public static string Or(this string str, string alternative)
        {
            return str.HasValue() ? str : alternative;
        }

        public static string En2Fa(this string str)
        {
            return
                str.Replace("0", "۰")
                    .Replace("1", "۱")
                    .Replace("2", "۲")
                    .Replace("3", "۳")
                    .Replace("4", "۴")
                    .Replace("5", "۵")
                    .Replace("6", "۶")
                    .Replace("7", "۷")
                    .Replace("8", "۸")
                    .Replace("9", "۹");
        }

        public static string Fa2En(this string str)
        {
            return str
                    .Replace("۰", "0")
                    .Replace("۱", "1")
                    .Replace("۲", "2")
                    .Replace("۳", "3")
                    .Replace("۴", "4")
                    .Replace("۵", "5")
                    .Replace("۶", "6")
                    .Replace("۷", "7")
                    .Replace("۸", "8")
                    .Replace("۹", "9")

                    .Replace("٠", "0")
                    .Replace("١", "1")
                    .Replace("٢", "2")
                    .Replace("٣", "3")
                    .Replace("٤", "4")
                    .Replace("٥", "5")
                    .Replace("٦", "6")
                    .Replace("٧", "7")
                    .Replace("٨", "8")
                    .Replace("٩", "9");
        }

        public static string FixPersianChars(this string str)
        {
            //.Replace((char)1610, (char)1740) ye
            //.Replace((char)1603, (char)1705); ke
            return str.Replace("ﮎ", "ک").Replace("ﮏ", "ک").Replace("ﮐ", "ک").Replace("ﮑ", "ک").Replace("ك", "ک").Replace("ي", "ی").Replace(" ", " ").Replace("‌", " ");//.Replace("ئ", "ی");
        }

        public static string CleanString(this string str)
        {
            return str.Trim().FixPersianChars().Fa2En().NullIfEmpty();
        }

        public const char ArabicYeChar = (char)1610;
        public const char PersianYeChar = (char)1740;

        public const char ArabicKeChar = (char)1603;
        public const char PersianKeChar = (char)1705;

        //public static string ApplyCorrectYeKe(this object data)
        //{
        //    return data == null ? null : ApplyCorrectYeKe(data.ToString());
        //}

        public static string ApplyCorrectYeKe(this string data)
        {
            return string.IsNullOrWhiteSpace(data) ?
                        string.Empty :
                        data.Replace(ArabicYeChar, PersianYeChar).Replace(ArabicKeChar, PersianKeChar).Trim();
        }

        public static void ApplyCorrectYeKe(this DbCommand command)
        {
            command.CommandText = command.CommandText.ApplyCorrectYeKe();

            foreach (DbParameter parameter in command.Parameters)
            {
                switch (parameter.DbType)
                {
                    case DbType.AnsiString:
                    case DbType.AnsiStringFixedLength:
                    case DbType.String:
                    case DbType.StringFixedLength:
                    case DbType.Xml:
                        parameter.Value = (parameter.Value as string).ApplyCorrectYeKe();
                        break;
                }
            }
        }

        public static string RemoveLetters(this string text)
        {
            return Regex.Replace(text, "[^0-9.]", "");
        }

        public static string RemoveSymbols(this string text)
        {
            return Regex.Replace(text, "[^A-Za-z0-9.]+", "");
        }

        public static string RemoveStr(this string text, params string[] strs)
        {
            strs.ForEach(p => text = text.Replace(p, ""));
            return text;
        }

        public static string ToTitleCase(string str)
        {
            //CultureInfo.CurrentCulture
            return CultureHelper.GetCultureInfo("en-US").TextInfo.ToTitleCase(str.ToLower());
            //red house : Red House
        }

        private static readonly List<KeyValuePair<char, char>> _characters = new List<KeyValuePair<char, char>>
        {
            new KeyValuePair<char, char>('q', 'ض'),
            new KeyValuePair<char, char>('w', 'ص'), //«»:ةيژؤBإأء
            new KeyValuePair<char, char>('e', 'ث'),
            new KeyValuePair<char, char>('r', 'ق'),
            new KeyValuePair<char, char>('t', 'ف'),
            new KeyValuePair<char, char>('T', '،'), //***
            new KeyValuePair<char, char>('y', 'غ'),
            new KeyValuePair<char, char>('Y', '؛'), //***
            new KeyValuePair<char, char>('u', 'ع'),
            new KeyValuePair<char, char>('i', 'ه'),
            new KeyValuePair<char, char>('o', 'خ'),
            new KeyValuePair<char, char>('p', 'ح'),
            new KeyValuePair<char, char>('[', 'ج'),
            new KeyValuePair<char, char>(']', 'چ'),
            new KeyValuePair<char, char>('\\', 'پ'),
            new KeyValuePair<char, char>('a', 'ش'),
            new KeyValuePair<char, char>('s', 'س'),
            new KeyValuePair<char, char>('d', 'ی'),
            new KeyValuePair<char, char>('f', 'ب'),
            new KeyValuePair<char, char>('g', 'ل'),
            new KeyValuePair<char, char>('G', 'ۀ'), //***
            new KeyValuePair<char, char>('h', 'ا'),
            new KeyValuePair<char, char>('H', 'آ'), //***
            new KeyValuePair<char, char>('j', 'ت'),
            new KeyValuePair<char, char>('J', 'ـ'), //***
            new KeyValuePair<char, char>('k', 'ن'),
            new KeyValuePair<char, char>('K', '«'), //***
            new KeyValuePair<char, char>('l', 'م'),
            new KeyValuePair<char, char>('L', '»'), //***
            new KeyValuePair<char, char>(';', 'ک'),
            new KeyValuePair<char, char>('\'', 'گ'),
            new KeyValuePair<char, char>('z', 'ظ'),
            new KeyValuePair<char, char>('x', 'ط'),
            new KeyValuePair<char, char>('c', 'ز'),
            new KeyValuePair<char, char>('C', 'ژ'), //***
            new KeyValuePair<char, char>('v', 'ر'),
            new KeyValuePair<char, char>('V', 'ؤ'), //***
            new KeyValuePair<char, char>('b', 'ذ'),
            new KeyValuePair<char, char>('B', 'إ'), //***
            new KeyValuePair<char, char>('n', 'د'),
            new KeyValuePair<char, char>('N', 'أ'), //***
            new KeyValuePair<char, char>('m', 'ئ'),
            new KeyValuePair<char, char>('M', 'ء'), //***
            new KeyValuePair<char, char>(',', 'و')
        };
        public static string IncorrectEnglishToFarsi(string text)
        {
            if (text == null)
                return null;

            var result = "";
            foreach (var item in text)
            {
                var aaa = _characters.Where(p => p.Key == item).Select(p => new { p.Value }).SingleOrDefault();
                if (aaa == null)
                {
                    var lower = item.ToString().ToLower()[0];
                    aaa = _characters.Where(p => p.Key == lower).Select(p => new { p.Value }).SingleOrDefault();
                }
                var ch = aaa?.Value ?? item;
                result += ch;
            }
            return result;
        }

        public static string FarsiToIncorrectEnglish(string text)
        {
            if (text == null)
                return null;

            var result = "";
            foreach (var item in text)
            {
                var aaa = _characters.Where(p => p.Value == item).Select(p => new { p.Key }).SingleOrDefault();
                var ch = aaa?.Key ?? item;
                result += ch;
            }
            return result;
        }

        public static TEnum ToEnum<TEnum>(this string value, TEnum defaultValue) where TEnum : struct
        {
            if (string.IsNullOrEmpty(value))
                return defaultValue;

            return Enum.TryParse(value, true, out TEnum result) ? result : defaultValue;
        }

        public static TEnum ToEnumFromDisplay<TEnum>(this string value, string displayAttributeName, TEnum defaultValue) where TEnum : struct
        {
            var result = defaultValue;
            if (string.IsNullOrEmpty(value))
                return defaultValue;

            if (value != null)
            {
                foreach (TEnum en in (TEnum[])Enum.GetValues(typeof(TEnum)))
                {
                    var attributes =
                        (DisplayAttribute[])
                            en.GetType().GetField(en.ToString()).GetCustomAttributes(typeof(DisplayAttribute), false);
                    if (attributes.Length > 0)
                    {
                        var attr = attributes[0];
                        var attributeValue = attr?.GetType()?.GetProperty(displayAttributeName)?.GetValue(attr, null) as string;
                        if (attributeValue == value)
                            result = en;
                    }
                }
            }
            return result;
        }

        public static TEnum ToEnumFromDisplayShortName<TEnum>(this string value, TEnum defaultValue) where TEnum : struct
        {
            return value.ToEnumFromDisplay("ShortName", defaultValue);
        }

        public static TEnum ToEnumFromDisplayName<TEnum>(this string value, TEnum defaultValue) where TEnum : struct
        {
            return value.ToEnumFromDisplay("Name", defaultValue);
        }

        /// <summary>
        /// تعیین معتبر بودن کد ملی
        /// </summary>
        /// <param name="nationalCode">کد ملی وارد شده</param>
        /// <returns>
        /// در صورتی که کد ملی صحیح باشد خروجی <c>true</c> و در صورتی که کد ملی اشتباه باشد خروجی <c>false</c> خواهد بود
        /// </returns>
        /// <exception cref="System.Exception"></exception>
        public static Boolean IsValidNationalCode(this String nationalCode)
        {
            //در صورتی که کد ملی وارد شده تهی باشد

            if (String.IsNullOrEmpty(nationalCode))
                throw new Exception("لطفا کد ملی را صحیح وارد نمایید");


            //در صورتی که کد ملی وارد شده طولش کمتر از 10 رقم باشد
            if (nationalCode.Length != 10)
                throw new Exception("طول کد ملی باید ده کاراکتر باشد");

            //در صورتی که کد ملی ده رقم عددی نباشد
            var regex = new Regex(@"\d{10}");
            if (!regex.IsMatch(nationalCode))
                throw new Exception("کد ملی تشکیل شده از ده رقم عددی می‌باشد؛ لطفا کد ملی را صحیح وارد نمایید");

            //در صورتی که رقم‌های کد ملی وارد شده یکسان باشد
            var allDigitEqual = new[] { "0000000000", "1111111111", "2222222222", "3333333333", "4444444444", "5555555555", "6666666666", "7777777777", "8888888888", "9999999999" };
            if (allDigitEqual.Contains(nationalCode)) return false;


            //عملیات شرح داده شده در بالا
            var chArray = nationalCode.ToCharArray();
            var num0 = Convert.ToInt32(chArray[0].ToString()) * 10;
            var num2 = Convert.ToInt32(chArray[1].ToString()) * 9;
            var num3 = Convert.ToInt32(chArray[2].ToString()) * 8;
            var num4 = Convert.ToInt32(chArray[3].ToString()) * 7;
            var num5 = Convert.ToInt32(chArray[4].ToString()) * 6;
            var num6 = Convert.ToInt32(chArray[5].ToString()) * 5;
            var num7 = Convert.ToInt32(chArray[6].ToString()) * 4;
            var num8 = Convert.ToInt32(chArray[7].ToString()) * 3;
            var num9 = Convert.ToInt32(chArray[8].ToString()) * 2;
            var a = Convert.ToInt32(chArray[9].ToString());

            var b = (((((((num0 + num2) + num3) + num4) + num5) + num6) + num7) + num8) + num9;
            var c = b % 11;

            return (((c < 2) && (a == c)) || ((c >= 2) && ((11 - c) == a)));
        }

        public static string HideSomeChars(this string str)
        {
            if (str.HasValue())
            {
                var halfNumberOfCharsToHide = (str.Length / 4m).RoundByStep(1).ToInt(); // Example 11 = 5, 12 = 6, 10 = 5
                var left = str.RemoveRight(str.Length - halfNumberOfCharsToHide);
                var right = str.RemoveLeft(str.Length - halfNumberOfCharsToHide);
                var missingNumber = str.Length - left.Length + right.Length;
                return left + RepeatChar('*', missingNumber) + right;
            }
            return "";
        }

        public static string HideSomeStringChars(this string str)
        {
            if (str.HasValue())
            {
                if (str.IsEmail())
                {
                    //var strBeforeAt = Regex.Replace(str, @"@([a-zA-Z0-9_.]+)\.([a-zA-Z]{2,7})$", "");
                    var strAtEnd = Regex.Replace(str, @"^([a-zA-Z0-9_.]+)", "");
                    str = str.RemoveRight(str.Length - 3) + "****" + strAtEnd;
                }
                else if (str.IsMobile())
                {
                    str = str.RemoveRight(7) + "****" + str.RemoveLeft(8);
                }
                else
                {
                    str = str.RemoveRight(str.Length - 3) + "****" + str.RemoveLeft(str.Length - 3);
                }
            }
            return str;
        }

        public static string HideSomeChars(this string str, int maxLength)
        {
            if (str.HasValue())
            {
                var halfNumberOfCharsToHide = (str.Length / 4m).RoundByStep(1).ToInt(); // Example 11 = 5, 12 = 6, 10 = 5
                var left = str.RemoveRight(str.Length - halfNumberOfCharsToHide);
                var right = str.RemoveLeft(str.Length - halfNumberOfCharsToHide);
                var missingNumber = str.Length - left.Length + right.Length;
                if ((left.Length + right.Length + missingNumber) > maxLength)
                {
                    var dif = maxLength - (left.Length + right.Length + missingNumber);
                    missingNumber = dif;
                }
                if (missingNumber == 0)
                    missingNumber = 3;

                return left + RepeatChar('*', missingNumber) + right;
            }
            return "";
        }

        private static string RepeatChar(char ch, int times)
        {
            var str = "";
            for (int i = 0; i < times; i++)
            {
                str += ch;
            }
            return str;
        }

        //public static string ReplacePart(int fromLength, int toLength, char character)
        //{

        //}
    }
}