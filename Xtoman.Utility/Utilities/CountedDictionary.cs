﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Xtoman.Utility
{
    /// <summary>
    /// Represents a collection of keys and values. Additionally contains a count <see
    /// cref="Count"/> which can be used as <c>count</c> argument when polling for new data.
    /// </summary>
    /// <typeparam name="TKey">The type of the keys in the dictionary.</typeparam>
    /// <typeparam name="TValue">The type of the values in the dictionary.</typeparam>
    [JsonConverter(typeof(CountedDictionaryConverter))]
    public class CountedDictionary<TKey, TValue> : Dictionary<TKey, TValue>
    {
        /// <summary>
        /// Id to be used as <c>since</c> when polling for new data.
        /// </summary>
        [JsonProperty("count")]
        public new long Count { get; set; }
    }

    public class CountedDictionaryConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType) => true;

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            object result = Activator.CreateInstance(objectType);

            Type typeKey = objectType.GenericTypeArguments[0];
            Type typeValue = objectType.GenericTypeArguments[1];

            PropertyInfo propLast = objectType.GetProperties().Single(p => p.Name == nameof(CountedDictionary<int, int>.Count) && p.PropertyType == typeof(long));
            //PropertyInfo propLast = objectType
            //    .GetRuntimeProperty(nameof(CountedDictionary<int, int>.Count));
            MethodInfo methodAdd = objectType
                .GetRuntimeMethod(nameof(CountedDictionary<int, int>.Add), new[] { typeKey, typeValue });

            reader.Read();
            while (reader.TokenType != JsonToken.EndObject)
            {
                string key = reader.Value.ToString();
                reader.Read();
                if (key == "last")
                {
                    propLast.SetValue(result, serializer.Deserialize<long>(reader));
                }
                else
                {
                    methodAdd.Invoke(result, new[] { key, serializer.Deserialize(reader, typeValue) });
                }
                reader.Read();
            }

            return result;
        }

        public override bool CanWrite => false;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            => throw new NotImplementedException();
    }
}
