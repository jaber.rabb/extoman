﻿using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xtoman.Utility
{
    public static class HttpUrlHelper
    {
        public static string ToQueryParameters(this Dictionary<string, string> dict)
        {
            var query = string.Join("&", dict.Keys.Select(key => string.Format("{0}={1}",
                key, HttpUtility.UrlEncode(dict[key]))));

            return query;
        }
    }
}
