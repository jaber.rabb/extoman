﻿namespace Xtoman.Utility.PostalEmail
{
    public class PostEmail : SendingEmailModel
    {
        public string PostTitle { get; set; }
        public string PostImageUrl { get; set; }
        public string PostUrl { get; set; }
    }
}
