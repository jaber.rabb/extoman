﻿namespace Xtoman.Utility.PostalEmail
{
    public class SupportEmail : SendingEmailModel
    {
        public string TicketSubject { get; set; }
        public string TicketText { get; set; }
        public string TicketUrl { get; set; }
    }
}
