﻿namespace Xtoman.Utility.PostalEmail
{
    public class CoinPaymentsPaidEmail : SendingEmailModel
    {
        public decimal Amount { get; set; }
        public string TransactionId { get; set; }
        public string Currency { get; set; }
        public string OrderGuid { get; set; }
    }
}
