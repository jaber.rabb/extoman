﻿namespace Xtoman.Utility.PostalEmail
{
    public class ConfirmEmail : SendingEmailModel
    {
        public string ConfirmCode { get; set; }
        public string ConfirmUrl { get; set; }
    }
}