﻿using Postal;
using System.IO;
using System.Net.Mail;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Xtoman.Utility.PostalEmail
{
    public static class PostalEmailHelper
    {
        public static MailMessage GetMailMessage(this Email email)
        {
            var viewsPath = Path.GetFullPath(HostingEnvironment.MapPath(@"~/Views/Emails"));
            var engines = new ViewEngineCollection
            {
                new FileSystemRazorViewEngine(viewsPath),
            };

            var emailService = new EmailService(engines);
            return emailService.CreateMailMessage(email);
        }
    }
}
