﻿namespace Xtoman.Utility.PostalEmail
{
    public class SupportTicketAnswerEmail : SendingEmailModel
    {
        public string TicketSubject { get; set; }
        public string TicketUrl { get; set; }
        public string TicketText { get; set; }
    }
}
