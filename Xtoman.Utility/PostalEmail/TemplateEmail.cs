﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Utility.PostalEmail
{
    public class TemplateEmail : SendingEmailModel
    {
        public string TextHtml { get; set; }
    }
}
