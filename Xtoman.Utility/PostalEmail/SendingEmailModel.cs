﻿using Postal;

namespace Xtoman.Utility.PostalEmail
{
    public class SendingEmailModel : Email
    {
        public string To { get; set; }
        public string From { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Subject { get; set; }
    }
}
