﻿namespace Xtoman.Utility.PostalEmail
{
    public class VerifyRespondEmail : SendingEmailModel
    {
        public bool VerifyAccepted { get; set; }
        public string VerifyDescription { get; set; }
    }

    public class VerifyXEmail : VerifyRespondEmail
    {
        public string Value { get; set; }
    }
}
