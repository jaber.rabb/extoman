﻿namespace Xtoman.Utility.PostalEmail
{
    public class OrderPaidEmail : PostEmail
    {
        public string FromCurrency { get; set; }
        public string ToCurrency { get; set; }
        public string PayAmount { get; set; }
        public string TxFee { get; set; }
        public string ReceiveAmount { get; set; }
        public string ExchangeTime { get; set; }
        public string OrderUrl { get; set; }
    }
}
