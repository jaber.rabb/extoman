﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;

public static class HtmlHelpers
{
    //In >= MVC4, just use @Html.DisplayNameFor
    //Before that : use your own Helper
    public static MvcHtmlString GetDisplayName<TModel, TProperty>(
        this HtmlHelper<TModel> htmlHelper,
        Expression<Func<TModel, TProperty>> expression
    )
    {
        var metaData = ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData);
        string value = metaData.DisplayName ?? (metaData.PropertyName ?? ExpressionHelper.GetExpressionText(expression));
        return MvcHtmlString.Create(value);
    }
}