using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Xtoman.FinTest
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsync().Wait();
            Console.ReadLine();
        }

        static async Task RunAsync()
        {

            Console.WriteLine("Calling the back-end API");

            //Need to change the port number
            //provide the port number where your api is running
            string apiBaseAddress = "http://localhost:42600/api/";

            HMACDelegatingHandler customDelegatingHandler = new HMACDelegatingHandler();

            HttpClient client = HttpClientFactory.Create(customDelegatingHandler);

            var cardModel = new
            {
                CardNumber = "6221061105306379"
            };

            HttpResponseMessage response = await client.PostAsJsonAsync(apiBaseAddress + "card", cardModel);

            if (response.IsSuccessStatusCode)
            {
                string responseString = await response.Content.ReadAsStringAsync();
                Console.WriteLine(responseString);
                Console.WriteLine("HTTP Status: {0}, Reason {1}. Press ENTER to exit", response.StatusCode, response.ReasonPhrase);
            }
            else
            {
                Console.WriteLine("Failed to call the API. HTTP Status: {0}, Reason {1}", response.StatusCode, response.ReasonPhrase);
            }
        }
    }
}