﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.ReplyMarkups;

namespace Xtoman.TelegramBot
{
    public static class Program
    {
        private static readonly TelegramBotClient Bot = new TelegramBotClient("629953180:AAFOWpIAIGIeYL4HeXdrAY4WoYgNbmRNQYg");
        public static void Main(string[] args)
        {
            var me = Bot.GetMeAsync().Result;
            Console.Title = me.Username;

            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.OnCallbackQuery += BotOnCallbackQueryReceived;
            Bot.OnInlineQuery += BotOnInlineQueryReceived;
            Bot.OnInlineResultChosen += BotOnChosenInlineResultReceived;
            Bot.OnReceiveError += BotOnReceiveError;

            Bot.StartReceiving(Array.Empty<UpdateType>());
            Console.WriteLine($"Start listening for @{me.Username}");
            Console.ReadLine();
            Bot.StopReceiving();
        }

        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;

            if (message == null || message.Type != MessageType.Text) return;

            switch (message.Text.Split(' ').First())
            {
                case "%d8%ac%d9%87%d8%aa+%d8%b4%d8%b1%d9%88%d8%b9+%d8%ab%d8%a8%d8%aa+%d9%86%d8%a7%d9%85+%d8%a8%d8%b1+%d8%b1%d9%88%db%8c+%da%af%d8%b2%db%8c%d9%86%d9%87+%d8%a7%d8%b1%d8%b3%d8%a7%d9%84+%d8%b4%d9%85%d8%a7%d8%b1%d9%87+%d8%aa%d9%84%da%af%d8%b1%d8%a7%d9%85+%da%a9%d9%84%db%8c%da%a9+%da%a9%d9%86%db%8c%d8%af+":
                    ReplyKeyboardMarkup RegisterReplyKeyboard = new[]
                    {
                        new[] { HttpUtility.UrlEncode(@"📞 ارسال شماره تلگرام") },
                    };

                    await Bot.SendTextMessageAsync(
                        message.Chat.Id,
                        HttpUtility.UrlEncode(@"جهت شروع ثبت نام بر روی گزینه ارسال شماره تلگرام کلیک کنید "));
                    break;
                case "🔓 ورود":
                    break;
                case "/start":
                    ReplyKeyboardMarkup ReplyKeyboard = new[]
                    {
                        new[] { "🔏 ثبت نام", "🔓 ورود" },
                    };

                    var startText = new StringBuilder();
                    startText.AppendLine("به 🤖 ربات اکس تومن خوش آمدید.");
                    startText.AppendLine("برای شروع اگر عضو اکس تومن هستید از منوی زیر، ورود را بزنید در غیر این صورت ثبت نام نمایید.");
                    startText.AppendLine("--------------");
                    startText.AppendLine("آدرس وب سایت: www.extoman.co");
                    startText.AppendLine("کانال تلگرام: @extoman");
                    startText.AppendLine("پشتیبانی تلگرام: @extomansupport");
                    await Bot.SendTextMessageAsync(
                        message.Chat.Id,
                        startText.ToString(),
                        replyMarkup: ReplyKeyboard);
                    break;
                default:
                    await Bot.SendTextMessageAsync(message.Chat.Id, message.Text);
                    System.IO.File.WriteAllText(@"e:\path.txt", message.Text);
                    break;
                //// send inline keyboard
                //case "/inline":
                //    await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                //    await Task.Delay(500); // simulate longer running task

                //    var inlineKeyboard = new InlineKeyboardMarkup(new[]
                //    {
                //        new [] // first row
                //        {
                //            InlineKeyboardButton.WithCallbackData("1.1"),
                //            InlineKeyboardButton.WithCallbackData("1.2"),
                //        },
                //        new [] // second row
                //        {
                //            InlineKeyboardButton.WithCallbackData("2.1"),
                //            InlineKeyboardButton.WithCallbackData("2.2"),
                //        }
                //    });

                //    await Bot.SendTextMessageAsync(
                //        message.Chat.Id,
                //        "Choose",
                //        replyMarkup: inlineKeyboard);
                //    break;

                //case "/photo":
                //    await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.UploadPhoto);

                //    const string file = @"Files/tux.png";

                //    var fileName = file.Split(Path.DirectorySeparatorChar).Last();

                //    using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                //    {
                //        await Bot.SendPhotoAsync(
                //            message.Chat.Id,
                //            fileStream,
                //            "Nice Picture");
                //    }
                //    break;

                // request location or contact
                //case "/request":
                //    var RequestReplyKeyboard = new ReplyKeyboardMarkup(new[]
                //    {
                //        KeyboardButton.WithRequestLocation("Location"),
                //        KeyboardButton.WithRequestContact("Contact"),
                //    });

                //    await Bot.SendTextMessageAsync(
                //        message.Chat.Id,
                //        "Who or Where are you?",
                //        replyMarkup: RequestReplyKeyboard);
                //    break;

//                default:
//                    const string usage = @"
//Usage:
///inline   - send inline keyboard
///keyboard - send custom keyboard
///photo    - send a photo
///request  - request location or contact";

//                    await Bot.SendTextMessageAsync(
//                        message.Chat.Id,
//                        usage,
//                        replyMarkup: new ReplyKeyboardRemove());
//                    break;
            }
        }

        private static async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            var callbackQuery = callbackQueryEventArgs.CallbackQuery;

            await Bot.AnswerCallbackQueryAsync(
                callbackQuery.Id,
                $"Received {callbackQuery.Data}");

            await Bot.SendTextMessageAsync(
                callbackQuery.Message.Chat.Id,
                $"Received {callbackQuery.Data}");
        }

        private static async void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs inlineQueryEventArgs)
        {
            Console.WriteLine($"Received inline query from: {inlineQueryEventArgs.InlineQuery.From.Id}");

            InlineQueryResultBase[] results = {
                new InlineQueryResultLocation(
                    id: "1",
                    latitude: 40.7058316f,
                    longitude: -74.2581888f,
                    title: "New York")   // displayed result
                    {
                        InputMessageContent = new InputLocationMessageContent(
                            latitude: 40.7058316f,
                            longitude: -74.2581888f)    // message if result is selected
                    },

                new InlineQueryResultLocation(
                    id: "2",
                    latitude: 13.1449577f,
                    longitude: 52.507629f,
                    title: "Berlin") // displayed result
                    {
                        InputMessageContent = new InputLocationMessageContent(
                            latitude: 13.1449577f,
                            longitude: 52.507629f)   // message if result is selected
                    }
            };

            await Bot.AnswerInlineQueryAsync(
                inlineQueryEventArgs.InlineQuery.Id,
                results,
                isPersonal: true,
                cacheTime: 0);
        }

        private static void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs chosenInlineResultEventArgs)
        {
            Console.WriteLine($"Received inline result: {chosenInlineResultEventArgs.ChosenInlineResult.ResultId}");
        }

        private static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            Console.WriteLine("Received error: {0} — {1}",
                receiveErrorEventArgs.ApiRequestException.ErrorCode,
                receiveErrorEventArgs.ApiRequestException.Message);
        }
    }
}