﻿using System;
using System.Linq;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Service;

namespace Xtoman.Jobs
{
    public class MessageJobs : IMessageJobs
    {
        private readonly IQueuedMessageService _queuedMessageService;
        private readonly ISMSSenderService _smsSenderService;
        private readonly IEmailSenderService _emailSenderService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly MessageSettings _messageSettings;
        private readonly int defaultMaxTry = 3;
        public MessageJobs(
            IQueuedMessageService queuedMessageService,
            ISMSSenderService sMSSenderService,
            IEmailAccountService emailAccountService,
            IEmailSenderService emailSenderService,
            MessageSettings messageSettings)
        {
            _queuedMessageService = queuedMessageService;
            _smsSenderService = sMSSenderService;
            _emailSenderService = emailSenderService;
            _emailAccountService = emailAccountService;
            _messageSettings = messageSettings;
        }

        public void SendQueuedMessage()
        {
            var smsMaxTry = _messageSettings.SMSSendingMaxTry > 0 ? _messageSettings.SMSSendingMaxTry : defaultMaxTry;
            var emailMaxTry = _messageSettings.EmailSendingMaxTry > 0 ? _messageSettings.EmailSendingMaxTry : defaultMaxTry;
            var maxTry = new int[] { smsMaxTry, emailMaxTry }.Max();
            if (maxTry == 0) maxTry = defaultMaxTry;
            var messages = _queuedMessageService.GetAllNotSent(maxTry);
            try
            {
                if (messages != null && messages.Count > 0)
                {
                    var emailMessages = messages
                        .Where(p => p.Type == QueuedMessageType.Email && p.TryCount < emailMaxTry)
                        .Take(50)
                        .ToList();

                    if (emailMessages != null && emailMessages.Count > 0)
                    {
                        foreach (var item in emailMessages)
                        {
                            var emailAccount = item.EmailAccountId.HasValue ? item.EmailAccount : 
                                _emailAccountService.TableNoTracking.Where(p => p.Id == _messageSettings.Default_EmailAccountId).FirstOrDefault();

                            item.TryCount++;
                            item.IsSent = _emailSenderService.SendEmail(emailAccount, item.Subject, item.Text, emailAccount?.Email ?? "", "ایکس تومن", item.To, item.ToName);
                        }
                    }

                    var smsMessages = messages
                        .Where(p => p.Type == QueuedMessageType.SMS && p.TryCount < smsMaxTry)
                        .Take(50)
                        .ToList();

                    if (smsMessages != null && emailMessages.Count > 0)
                        foreach (var item in smsMessages)
                        {
                            item.TryCount++;
                            item.IsSent = _smsSenderService.SendSMS(item.To, item.Text);
                        }
                }

            }
            catch (Exception)
            {
            }
            finally
            {
                _queuedMessageService.CleanUpdate(messages);
            }
        }
    }
}
