﻿using System.Linq;
using Xtoman.Domain.Models;
using Xtoman.Service;
using Xtoman.Service.WebServices;

namespace Xtoman.Jobs
{
    public class PayIRJobs : IPayIRJobs
    {
        private readonly IPayIRService _payIRService;
        private readonly IWithdrawService _withdrawService;
        public PayIRJobs(
            IPayIRService payIRService,
            IWithdrawService withdrawService)
        {
            _payIRService = payIRService;
            _withdrawService = withdrawService;
        }

        public void GetUpdateCashoutsStatus()
        {
            var withdraws = _withdrawService.Table
                .Where(p => p.WalletApiType == WalletApiType.PayIR && p.Status != WithdrawStatus.Completed).ToList();

            foreach (var item in withdraws)
            {
                var payResult = _payIRService.CashoutStatusAsync(item.ApiWithdrawId).Result;
                if (payResult?.Status == 1 && payResult.Data.StatusCode == 4)
                {
                    item.Status = WithdrawStatus.Completed;
                    item.TransactionId = "9902260562002191";
                    _withdrawService.Update(item);
                }
            }
        }
    }
}
