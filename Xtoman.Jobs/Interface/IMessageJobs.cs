﻿using System;

namespace Xtoman.Jobs
{
    public interface IMessageJobs
    {
        void SendQueuedMessage();
    }
}
