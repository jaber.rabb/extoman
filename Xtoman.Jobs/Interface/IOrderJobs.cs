﻿namespace Xtoman.Jobs
{
    public interface IOrderJobs
    {
        void CancelOutDatedOrders();
    }
}
