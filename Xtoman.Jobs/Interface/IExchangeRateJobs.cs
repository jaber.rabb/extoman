﻿namespace Xtoman.Jobs
{
    public interface IExchangeRateJobs
    {
        void UpdateFiatCurrencyRates();
        void UpdateCryptoExchangeRates();
    }
}
