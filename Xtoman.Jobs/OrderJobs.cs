﻿using System;
using System.Linq;
using Xtoman.Domain.Models;
using Xtoman.Service;

namespace Xtoman.Jobs
{
    public class OrderJobs : IOrderJobs
    {
        #region Properties
        private readonly IExchangeOrderService _exchangeOrderService;
        #endregion

        #region Constructor
        public OrderJobs(
            IExchangeOrderService exchangeOrderService)
        {
            _exchangeOrderService = exchangeOrderService;
        }
        #endregion

        #region Methods
        public void CancelOutDatedOrders()
        {
            //var lastFifteenMinutes = DateTime.UtcNow.AddMinutes(-15);
            var now = DateTime.UtcNow;
            var outDatedOrders = _exchangeOrderService.Table
                .Where(p => (p.ExpireDate <= now || p.ExpireDate == null) && p.OrderStatus == OrderStatus.Pending && p.PaymentStatus != PaymentStatus.Paid)
                .ToList();

            outDatedOrders.ForEach((x) =>
            {
                if (!(x.Exchange.ExchangeFiatCoinType.ToString().Contains("CoinTo") && x.PaymentStatus == PaymentStatus.InPayment))
                {
                    x.UpdateUserId = new int?();
                    x.UpdateDate = DateTime.UtcNow;
                    x.OrderStatus = OrderStatus.Canceled;
                    x.PaymentStatus = PaymentStatus.Canceled;
                }
            });

            _exchangeOrderService.Update(outDatedOrders);
        }
        #endregion
    }
}
