﻿using System;
using System.Linq;
using Xtoman.Domain.Models;
using Xtoman.Service;
using Xtoman.Service.WebServices;

namespace Xtoman.Jobs
{
    public class WithdrawJobs : IWithdrawJob
    {
        #region Properties
        private readonly IBinanceService _binanceService;
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly IWithdrawService _withdrawService;
        private readonly ICurrencyTypeService _currencyTypeService;
        #endregion

        #region Constructor
        public WithdrawJobs(
            IBinanceService binanceService,
            IExchangeOrderService exchangeOrderService,
            IWithdrawService withdrawService,
            ICurrencyTypeService currencyTypeService)
        {
            _binanceService = binanceService;
            _exchangeOrderService = exchangeOrderService;
            _withdrawService = withdrawService;
            _currencyTypeService = currencyTypeService;
        }
        #endregion

        #region Job Methods
        public void WithdrawForCoinToCoinOrders()
        {
            var orders = _exchangeOrderService.Table
                .Where(p => p.Exchange.ExchangeFiatCoinType == ExchangeFiatCoinType.CoinToCoin &&
                            p.PaymentStatus == PaymentStatus.Paid &&
                            p.OrderTrades.Any() &&
                            !p.OrderTrades.Any(x => x.TradeStatus == TradeStatus.Queue || x.TradeStatus == TradeStatus.Voided || x.TradeStatus == TradeStatus.Failed) &&
                            p.OrderTrades.Any(x => x.ToAmount > 0) &&
                            !p.OrderWithdraws.Any(x => x.TradingPlatform == TradingPlatform.Binance))
                .ToList();

            if (orders != null)
            {
                foreach (var order in orders)
                {
                    if (order.OrderTrades.Count == 1)
                    {
                        var expectedFromAmount = order.PaidAmount - (order.PaidAmount * 0.5m / 100) - (order.Exchange.FromCurrency.TransactionFee ?? 0); // Minus CoinPayment Fees
                        expectedFromAmount -= (expectedFromAmount * order.ExchangeExtraFeePercentInTime / 100); // Minus Exchange Fee

                        var amount = 0m;
                        var orderTrade = order.OrderTrades.FirstOrDefault();
                        if (orderTrade != null)
                        {
                            if (orderTrade.FromAmount <= expectedFromAmount)
                            {
                                amount = orderTrade.ToAmount - (orderTrade.ToAmount / 1000) - (order.Exchange.ToCurrency.TransactionFee ?? 0);
                            }
                            else
                            {
                                amount = expectedFromAmount * orderTrade.ToAmount / orderTrade.FromAmount;
                                amount = amount - (amount / 1000) - (order.Exchange.ToCurrency.TransactionFee ?? 0);
                            }
                        }

                        if (amount > 0)
                        {
                            var asset = order.Exchange.ToCurrency.Type.ToBinanceAsset();
                            var success = false;
                            try
                            {
                                var withdrawResult = _binanceService.Withdraw(asset, amount, order.ReceiveAddress, order.ReceiveMemoTagPaymentId, order.Id.ToString(), 10000);
                                success = withdrawResult != null && withdrawResult.Success;
                                if (success)
                                {
                                    var withdraw = new Withdraw()
                                    {
                                        OrderId = order.Id,
                                        ECurrencyType = order.Exchange.ToCurrency.Type,
                                        TradingPlatform = TradingPlatform.Binance,
                                        Amount = amount,
                                        WithdrawType = WithdrawFromType.TradingPlatform,
                                        WalletApiType = WalletApiType.Binance,
                                        Fee = order.GatewayFee,
                                        ApiWithdrawId = withdrawResult?.Id,
                                    };
                                    _withdrawService.InsertAsync(withdraw);
                                    success = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                order.Errors = ex.Message;
                                order.OrderStatus = OrderStatus.WithdrawFailed;
                                _exchangeOrderService.Update(order);
                            }
                            finally
                            {
                                if (!success)
                                {
                                    var withdraw = new Withdraw()
                                    {
                                        Amount = amount,
                                        ECurrencyType = order.Exchange.ToCurrency.Type,
                                        Fee = order.GatewayFee,
                                        Status = WithdrawStatus.Failure,
                                        WalletApiType = WalletApiType.Binance,
                                        WithdrawType = WithdrawFromType.TradingPlatform,
                                        TradingPlatform = TradingPlatform.Binance,
                                        OrderId = order.Id
                                    };
                                    _withdrawService.Insert(withdraw);
                                }
                            }

                        }
                    }
                    else if (order.OrderTrades.Count == 2)
                    {
                        var expectedFromAmount = order.PaidAmount - (order.PaidAmount * 0.5m / 100) - (order.Exchange.FromCurrency.TransactionFee ?? 0); // Minus CoinPayment Fees
                        expectedFromAmount -= (expectedFromAmount * order.ExchangeExtraFeePercentInTime / 100); // Minus Exchange Fee

                        var toFirstAmount = 0m;
                        var toSecondAmount = 0m;
                        var firstTrade = order.OrderTrades.Where(p => p.FromCryptoType == order.Exchange.FromCurrency.Type).FirstOrDefault();
                        var secondTrade = order.OrderTrades.Where(p => p.ToCryptoType == order.Exchange.ToCurrency.Type).FirstOrDefault();
                        if (firstTrade != null)
                        {
                            if (firstTrade.FromAmount <= expectedFromAmount)
                            {
                                toFirstAmount = firstTrade.ToAmount - (firstTrade.ToAmount / 1000);
                            }
                            else
                            {
                                toFirstAmount = expectedFromAmount * firstTrade.ToAmount / firstTrade.FromAmount;
                                toFirstAmount = toFirstAmount - (toFirstAmount / 1000);
                            }
                        }

                        if (toFirstAmount > 0)
                        {
                            if (secondTrade.FromAmount <= toFirstAmount)
                            {
                                toSecondAmount = secondTrade.ToAmount - (secondTrade.ToAmount / 1000) - (order.Exchange.ToCurrency.TransactionFee ?? 0);
                            }
                            else
                            {
                                toSecondAmount = toFirstAmount * secondTrade.ToAmount / secondTrade.FromAmount;
                                toSecondAmount = toSecondAmount - (toSecondAmount / 1000) - (order.Exchange.ToCurrency.TransactionFee ?? 0);
                            }
                        }

                        if (toSecondAmount > 0)
                        {
                            var asset = order.Exchange.ToCurrency.Type.ToBinanceAsset();
                            var success = false;
                            try
                            {
                                var withdrawResult = _binanceService.Withdraw(asset, toSecondAmount, order.ReceiveAddress, order.ReceiveMemoTagPaymentId, order.Id.ToString(), 10000);
                                success = withdrawResult != null && withdrawResult.Success;
                                if (success)
                                {
                                    var withdraw = new Withdraw()
                                    {
                                        OrderId = order.Id,
                                        ECurrencyType = order.Exchange.ToCurrency.Type,
                                        TradingPlatform = TradingPlatform.Binance,
                                        Amount = toSecondAmount,
                                        WithdrawType = WithdrawFromType.TradingPlatform,
                                        WalletApiType = WalletApiType.Binance,
                                        Fee = order.GatewayFee,
                                        ApiWithdrawId = withdrawResult?.Id,
                                    };
                                    _withdrawService.InsertAsync(withdraw);
                                    success = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                order.Errors = ex.Message;
                                order.OrderStatus = OrderStatus.WithdrawFailed;
                                _exchangeOrderService.Update(order);
                            }
                            finally
                            {
                                if (!success)
                                {
                                    var withdraw = new Withdraw()
                                    {
                                        Amount = toSecondAmount,
                                        ECurrencyType = order.Exchange.ToCurrency.Type,
                                        Fee = order.GatewayFee,
                                        Status = WithdrawStatus.Failure,
                                        WalletApiType = WalletApiType.Binance,
                                        WithdrawType = WithdrawFromType.TradingPlatform,
                                        TradingPlatform = TradingPlatform.Binance,
                                        OrderId = order.Id
                                    };
                                    _withdrawService.Insert(withdraw);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void UpdateBinanceWithdrawFee()
        {
            var allCoinsInfo = _binanceService.GetAllCoinsInformation();
            var currencies = _currencyTypeService.Table.ToList();
            foreach (var currency in currencies)
            {
                var assetCoin = currency.Type.ToBinanceAsset();
                var coinInfo = allCoinsInfo.Where(p => p.Coin == assetCoin).FirstOrDefault();
                if (coinInfo != null)
                {
                    var network = assetCoin;
                    var coinNetwork = coinInfo.NetworkList.Where(p => p.Network == network).FirstOrDefault();
                    if (coinNetwork == null)
                        coinNetwork = coinInfo.NetworkList.Where(p => p.Network == "ETH").FirstOrDefault();

                    if (coinNetwork == null)
                        coinNetwork = coinInfo.NetworkList.FirstOrDefault();

                    currency.TransactionFee = coinNetwork.WithdrawFee;

                    _currencyTypeService.UpdateAsync(currency);
                }   
            }
        }
        #endregion
    }
}
