﻿using System.Linq;
using Xtoman.Domain.Models;
using Xtoman.Service;
using System.Data.Entity;
using Xtoman.Service.WebServices;
using Xtoman.Domain.Settings;
using System;

namespace Xtoman.Jobs
{
    public class ExchangeRateJobs : IExchangeRateJobs
    {
        #region Properties
        private readonly IExchangeTypeService _exchangeTypeService;
        private readonly IExchangeTypeRateService _exchangeTypeRateService;
        private readonly ICoinMarketCapService _coinMarketCapService;
        private readonly ISettingService _settingService;
        #endregion

        #region Constructor
        public ExchangeRateJobs(
            IExchangeTypeRateService exchangeTypeRateService,
            IExchangeTypeService exchangeTypeService,
            ICoinMarketCapService coinMarketCapService,
            ISettingService settingService)
        {
            _exchangeTypeRateService = exchangeTypeRateService;
            _exchangeTypeService = exchangeTypeService;
            _coinMarketCapService = coinMarketCapService;
            _settingService = settingService;
        }
        #endregion

        #region Methods
        public void UpdateFiatCurrencyRates()
        {
            var fiatExchangeTypes = _exchangeTypeService.TableNoTracking
                .Where(p => p.FromCurrency.Type == ECurrencyType.Euro || p.ToCurrency.Type == ECurrencyType.Euro ||
                       //p.FromCurrency.Type == ECurrencyType.Dirham || p.ToCurrency.Type == ECurrencyType.Dirham ||
                       p.FromCurrency.Type == ECurrencyType.USDollar || p.ToCurrency.Type == ECurrencyType.USDollar)
                .Include(p => p.ExchangeTypeRates)
                .Select(p => new
                {
                    p.Id,
                    LastRate = p.ExchangeTypeRates.OrderByDescending(x => x.InsertDate).Where(x => x.RateIncludeExtraFee != 0).FirstOrDefault(),
                    p.FromCurrency,
                    p.ToCurrency,
                    p.ExtraFeePercent
                })
                .ToList();

            var dollarPrice = _settingService.GetSettingWithoutCache<int>("PaymentSettings.USDPrice");
            var euroPrice = _settingService.GetSettingWithoutCache<int>("PaymentSettings.EURPrice");

            #region Dollar price
            var shetabToDollar = fiatExchangeTypes.Where(p => p.FromCurrency.Type == ECurrencyType.Shetab && p.ToCurrency.Type == ECurrencyType.USDollar).FirstOrDefault();
            var shetabToDollarRate = new ExchangeTypeRate()
            {
                ExchangeTypeId = shetabToDollar.Id,
                DollarPriceInToman = dollarPrice,
                EuroPriceInToman = euroPrice,
                ExtraFeePercent = shetabToDollar.ExtraFeePercent,
                Rate = dollarPrice,
                RateIncludeExtraFee = dollarPrice + (dollarPrice * shetabToDollar.ExtraFeePercent / 100)
            };

            if (shetabToDollarRate.Rate != shetabToDollar.LastRate?.Rate || shetabToDollarRate.RateIncludeExtraFee != shetabToDollar.LastRate?.RateIncludeExtraFee)
                if (shetabToDollarRate.Rate > 0 && shetabToDollarRate.RateIncludeExtraFee > 0)
                    _exchangeTypeRateService.Insert(shetabToDollarRate);

            var dollarToShetab = fiatExchangeTypes.Where(p => p.ToCurrency.Type == ECurrencyType.Shetab && p.FromCurrency.Type == ECurrencyType.USDollar).FirstOrDefault();
            var dollarToShetabRate = new ExchangeTypeRate()
            {
                ExchangeTypeId = dollarToShetab.Id,
                DollarPriceInToman = dollarPrice,
                EuroPriceInToman = euroPrice,
                ExtraFeePercent = dollarToShetab.ExtraFeePercent,
                Rate = dollarPrice,
                RateIncludeExtraFee = dollarPrice - (dollarPrice * dollarToShetab.ExtraFeePercent / 100)
            };

            if (dollarToShetabRate.Rate != dollarToShetab.LastRate?.Rate || dollarToShetabRate.RateIncludeExtraFee != dollarToShetab.LastRate?.RateIncludeExtraFee)
                if (dollarToShetabRate.Rate > 0 && dollarToShetabRate.RateIncludeExtraFee > 0)
                    _exchangeTypeRateService.Insert(dollarToShetabRate);
            #endregion

            #region Euro price
            var shetabToEuro = fiatExchangeTypes.Where(p => p.FromCurrency.Type == ECurrencyType.Shetab && p.ToCurrency.Type == ECurrencyType.Euro).FirstOrDefault();
            var shetabToEuroRate = new ExchangeTypeRate()
            {
                ExchangeTypeId = shetabToEuro.Id,
                DollarPriceInToman = euroPrice,
                EuroPriceInToman = euroPrice,
                ExtraFeePercent = shetabToEuro.ExtraFeePercent,
                Rate = euroPrice,
                RateIncludeExtraFee = euroPrice + (euroPrice * shetabToEuro.ExtraFeePercent / 100)
            };

            if (shetabToEuroRate.Rate != shetabToEuro.LastRate?.Rate || shetabToEuroRate.RateIncludeExtraFee != shetabToEuro.LastRate?.RateIncludeExtraFee)
                if (shetabToEuroRate.Rate > 0 && shetabToEuroRate.RateIncludeExtraFee > 0)
                    _exchangeTypeRateService.Insert(shetabToEuroRate);

            var euroToShetab = fiatExchangeTypes.Where(p => p.ToCurrency.Type == ECurrencyType.Shetab && p.FromCurrency.Type == ECurrencyType.Euro).FirstOrDefault();
            var euroToShetabRate = new ExchangeTypeRate()
            {
                ExchangeTypeId = euroToShetab.Id,
                DollarPriceInToman = euroPrice,
                EuroPriceInToman = euroPrice,
                ExtraFeePercent = euroToShetab.ExtraFeePercent,
                Rate = euroPrice,
                RateIncludeExtraFee = euroPrice - (euroPrice * euroToShetab.ExtraFeePercent / 100)
            };

            if (euroToShetabRate.Rate != euroToShetab.LastRate?.Rate || euroToShetabRate.RateIncludeExtraFee != euroToShetab.LastRate?.RateIncludeExtraFee)
                if (euroToShetabRate.Rate > 0 && euroToShetabRate.RateIncludeExtraFee > 0)
                    _exchangeTypeRateService.Insert(euroToShetabRate);
            #endregion
        }
        public void UpdateCryptoExchangeRates()
        {
            #region ExchangeTypes
            var fiatExchangeTypes = _exchangeTypeService.TableNoTracking
                .Where(p => p.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham ||
                            p.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman ||
                            p.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham)
                .Select(p => new
                {
                    LastRate = p.ExchangeTypeRates.OrderByDescending(x => x.InsertDate).Where(x => x.RateIncludeExtraFee != 0).FirstOrDefault(),
                    p.FromCurrency,
                    p.ToCurrency,
                    p.ExtraFeePercent
                })
                .ToList();

            var cryptoExchangeTypes = _exchangeTypeService.TableNoTracking
                .Where(p => p.ExchangeFiatCoinType != ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham &&
                            p.ExchangeFiatCoinType != ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman &&
                            p.ExchangeFiatCoinType != ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham)
                .Select(p => new
                {
                    LastRate = p.ExchangeTypeRates.OrderByDescending(x => x.InsertDate).Where(x => x.RateIncludeExtraFee != 0).FirstOrDefault(),
                    p.Id,
                    p.FromCurrency,
                    p.ToCurrency,
                    p.ExtraFeePercent,
                    p.ExchangeFiatCoinType
                })
                .ToList();
            #endregion

            #region Euro Prices
            var euroBuyPrice = Convert.ToInt32(fiatExchangeTypes
                .Where(p => p.FromCurrency.Type == ECurrencyType.Shetab && p.ToCurrency.Type == ECurrencyType.Euro)
                .Select(p => p.LastRate?.RateIncludeExtraFee)
                .DefaultIfEmpty(0)
                .FirstOrDefault() ?? 0);
            var euroSellPrice = Convert.ToInt32(fiatExchangeTypes
                .Where(p => p.FromCurrency.Type == ECurrencyType.Euro && p.ToCurrency.Type == ECurrencyType.Shetab)
                .Select(p => p.LastRate?.RateIncludeExtraFee)
                .DefaultIfEmpty(0)
                .FirstOrDefault() ?? 0);
            #endregion

            #region Dollar Prices
            var dollarBuyPrice = Convert.ToInt32(fiatExchangeTypes
                .Where(p => p.FromCurrency.Type == ECurrencyType.Shetab && p.ToCurrency.Type == ECurrencyType.USDollar)
                .Select(p => p.LastRate?.RateIncludeExtraFee)
                .DefaultIfEmpty(0)
                .FirstOrDefault() ?? 0);
            var dollarSellPrice = Convert.ToInt32(fiatExchangeTypes
                .Where(p => p.FromCurrency.Type == ECurrencyType.USDollar && p.ToCurrency.Type == ECurrencyType.Shetab)
                .Select(p => p.LastRate?.RateIncludeExtraFee)
                .DefaultIfEmpty(0)
                .FirstOrDefault() ?? 0);
            #endregion

            #region Coins Prices
            var coinMarketCapTickers = _coinMarketCapService.GetTicker(0, 1000);
            #endregion

            if (dollarBuyPrice > 0 && euroBuyPrice > 0 && dollarSellPrice > 0 && euroSellPrice > 0)
            {
                foreach (var item in cryptoExchangeTypes)
                {
                    var fromType = coinMarketCapTickers.Where(p => p.Id == item.FromCurrency.CoinMarketCapId).FirstOrDefault();
                    var toType = coinMarketCapTickers.Where(p => p.Id == item.ToCurrency.CoinMarketCapId).FirstOrDefault();

                    var isBuy = item.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatTomanToCoin ||
                                             item.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatUSDToCoin ||
                                             item.ExchangeFiatCoinType == ExchangeFiatCoinType.CoinToCoin;

                    var exchangeTypeRateDollarPrice = isBuy ? dollarBuyPrice : dollarSellPrice;
                    var exchangeTypeRateEuroPrice = isBuy ? euroBuyPrice : euroSellPrice;

                    var exchangeTypeRate = new ExchangeTypeRate()
                    {
                        ExtraFeePercent = item.ExtraFeePercent,
                        ExchangeTypeId = item.Id,
                        DollarPriceInToman = exchangeTypeRateDollarPrice,
                        EuroPriceInToman = exchangeTypeRateEuroPrice
                    };
                    switch (item.ExchangeFiatCoinType)
                    {
                        case ExchangeFiatCoinType.CoinToCoin:
                            var coinToCoinRate = _coinMarketCapService.ExchangeRate(fromType, toType);
                            exchangeTypeRate.RateIncludeExtraFee = coinToCoinRate + (coinToCoinRate * item.ExtraFeePercent / 100);
                            exchangeTypeRate.Rate = coinToCoinRate;
                            break;
                        case ExchangeFiatCoinType.CoinToFiatUSD:
                            var coinUSDSellRate = fromType.Price_usd.Value;
                            exchangeTypeRate.Rate = coinUSDSellRate;
                            exchangeTypeRate.RateIncludeExtraFee = coinUSDSellRate - (coinUSDSellRate * item.ExtraFeePercent / 100);
                            break;
                        case ExchangeFiatCoinType.CoinToFiatToman:
                            var coinSellRate = fromType.Price_eur * exchangeTypeRateEuroPrice ?? 0;
                            exchangeTypeRate.Rate = coinSellRate;
                            exchangeTypeRate.RateIncludeExtraFee = coinSellRate - (coinSellRate * item.ExtraFeePercent / 100);
                            break;
                        case ExchangeFiatCoinType.FiatUSDToCoin:
                            var coinUSDBuyRate = toType.Price_usd.Value;
                            exchangeTypeRate.Rate = coinUSDBuyRate;
                            exchangeTypeRate.RateIncludeExtraFee = coinUSDBuyRate + (coinUSDBuyRate * item.ExtraFeePercent / 100);
                            break;
                        case ExchangeFiatCoinType.FiatTomanToCoin:
                            var coinBuyRate = toType.Price_eur * exchangeTypeRateEuroPrice ?? 0;
                            exchangeTypeRate.Rate = coinBuyRate;
                            exchangeTypeRate.RateIncludeExtraFee = coinBuyRate + (coinBuyRate * item.ExtraFeePercent / 100);
                            break;
                    }
                    if (exchangeTypeRate.Rate > 0 && exchangeTypeRate.DollarPriceInToman > 0 && exchangeTypeRate.EuroPriceInToman > 0 && exchangeTypeRate.RateIncludeExtraFee > 0)
                        if (exchangeTypeRate.Rate != item.LastRate?.Rate || exchangeTypeRate.RateIncludeExtraFee != item.LastRate?.RateIncludeExtraFee)
                            _exchangeTypeRateService.Insert(exchangeTypeRate);
                }
            }
        }
        #endregion
    }
}
