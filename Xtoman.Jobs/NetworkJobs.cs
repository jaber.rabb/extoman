﻿using System.Data.Entity;
using System.Linq;
using Xtoman.Domain.Models;
using Xtoman.Service;
using Xtoman.Service.WebServices;

namespace Xtoman.Jobs
{
    public class NetworkJobs
    {
        private readonly IBlockchainNetworkService _blockchainNetworkService;
        private readonly ICurrencyTypeNetworkService _currencyTypeNetworkService;
        private readonly IBinanceService _binanceService;
        public NetworkJobs(
            IBlockchainNetworkService blockchainNetworkService,
            ICurrencyTypeNetworkService currencyTypeNetworkService,
            IBinanceService binanceService)
        {
            _blockchainNetworkService = blockchainNetworkService;
            _currencyTypeNetworkService = currencyTypeNetworkService;
            _binanceService = binanceService;
        }

        public void UpdateCurrencyTypesAndNetworks()
        {
            var networks = _blockchainNetworkService.Table
                .ToList();

            var allCoinsInfo = _binanceService.GetAllCoinsInformation();
            foreach (var network in networks)
            {
                var binanceNetwork = allCoinsInfo.SelectMany(p => p.NetworkList).Where(p => p.Name == network.Name && p.Network == network.Network).FirstOrDefault();
                network.AddressRegex = binanceNetwork.AddressRegex;
                network.MemoRegex = binanceNetwork.MemoRegex;
                network.MinConfirm = binanceNetwork.MinConfirm;
                network.SpecialTips = binanceNetwork.SpecialTips;

                //_blockchainNetworkService.Context.MarkAsChanged(network);
                _blockchainNetworkService.Update(network);
            }

            var currencyTypes = _currencyTypeNetworkService.Table
                .Include(p => p.CurrencyType)
                .ToList();

            foreach (var currencyType in currencyTypes)
            {
                var assetName = currencyType.CurrencyType.Type.ToBinanceAsset();
                var binanceAsset = allCoinsInfo.Where(p => p.Coin == assetName).FirstOrDefault();
                var network = binanceAsset.NetworkList.Where(p => p.Network == currencyType.BlockchainNetwork.Network && p.Name == currencyType.BlockchainNetwork.Name).FirstOrDefault();

                currencyType.DepositEnable = network.DepositEnable;
                currencyType.WithdrawEnable = network.WithdrawEnable;
                currencyType.WithdrawFee = network.WithdrawFee;
                currencyType.WithdrawMaximum = network.WithdrawMax;
                currencyType.WithdrawMinimum = network.WithdrawMin;
                currencyType.IsDefault = network.IsDefault;

                //_currencyTypeNetworkService.Context.MarkAsChanged(currencyType);
                _currencyTypeNetworkService.Update(currencyType);
            }
        }
    }
}
