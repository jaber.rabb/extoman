﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.TradingPlatform.Binance;
using Xtoman.Service;
using Xtoman.Service.WebServices;

namespace Xtoman.Jobs
{
    public class TradeJobs : ITradeJobs
    {
        #region Properties
        private readonly ITradeService _tradeService;
        private readonly IBinanceService _binanceService;
        #endregion

        #region Constructors
        public TradeJobs(
            ITradeService tradeService,
            IBinanceService binanceService)
        {
            _tradeService = tradeService;
            _binanceService = binanceService;
        }
        #endregion

        public void CompleteBinanceQueueTrades()
        {
            var pendingTrades = _tradeService.Table.Where(p => p.ExchangeOrderId != null
                                                            && p.TradeStatus == TradeStatus.Queue
                                                            && p.TradingPlatform == TradingPlatform.Binance)
                                                            .ToList();

            if (pendingTrades != null && pendingTrades.Count > 0)
            {
                #region Sell to BTC and USDT
                // Get All Pending Trades
                var pendingSellTrades = pendingTrades.Where(p => (p.ToCryptoType == ECurrencyType.Tether || p.ToCryptoType == ECurrencyType.Bitcoin)
                                                           && p.FromCryptoType != ECurrencyType.Tether)
                                                           .ToList();

                if (pendingSellTrades != null && pendingSellTrades.Count > 0)
                {
                    var orderIds = pendingSellTrades.Select(p => p.ExchangeOrderId).Distinct().OrderBy(p => p).ToList();
                    foreach (var orderId in orderIds)
                    {
                        var toTradeItems = pendingSellTrades.Where(p => p.ExchangeOrderId == orderId.Value).ToList();
                        if (toTradeItems.Count == 1)
                        {
                            // Single trade
                            var toTrade = toTradeItems.FirstOrDefault();
                            var fromAsset = toTrade.FromCryptoType.ToBinanceAsset();
                            var toAsset = toTrade.ToCryptoType.ToBinanceAsset();

                            var binanceAccountInfo = _binanceService.GetAccountInfo(); // Get balance again
                            var fromCurrencyBalance = binanceAccountInfo.Balances.Where(p => p.Asset == fromAsset).Select(p => p.Free).FirstOrDefault();

                            if (fromCurrencyBalance >= toTrade.FromAmount)
                            {
                                var symbol = fromAsset + toAsset;

                                toTrade.TradeStatus = TradeStatus.Requested;
                                _tradeService.Update(toTrade);

                                var tradeSuccess = false;
                                try
                                {
                                    var tradeResult = _binanceService.PostNewOrder(symbol, toTrade.FromAmount, 0, BinanceOrderSide.SELL, BinanceOrderType.MARKET);
                                    if (!tradeResult.StatusEnum.HasFlag(BinanceTradeStatus.CANCELED | BinanceTradeStatus.EXPIRED | BinanceTradeStatus.REJECTED | BinanceTradeStatus.VOIDED))
                                    {
                                        toTrade.ToAmount = tradeResult.Fills.Select(p => (p.Price * p.Qty)).DefaultIfEmpty(0).Sum();
                                        toTrade.Price = tradeResult.Fills.Select(p => p.Price).DefaultIfEmpty(0).Sum();
                                        toTrade.TradingFee = tradeResult.Fills.Select(p => p.Commission).DefaultIfEmpty(0).Sum();
                                        toTrade.TradeStatus = TradeStatus.Completed;
                                        toTrade.TradingFeeType = toTrade.ToCryptoType;
                                        toTrade.ApiTradeId = tradeResult.ClientOrderId;
                                        _tradeService.Update(toTrade);
                                        tradeSuccess = true;
                                    }
                                    else
                                    {
                                        switch (tradeResult.StatusEnum)
                                        {
                                            case BinanceTradeStatus.CANCELED:
                                            case BinanceTradeStatus.PENDING_CANCEL:
                                            case BinanceTradeStatus.REJECTED:
                                            case BinanceTradeStatus.EXPIRED:
                                                toTrade.TradeStatus = TradeStatus.Queue;
                                                break;
                                            case BinanceTradeStatus.VOIDED:
                                            default:
                                                toTrade.TradeStatus = TradeStatus.Voided;
                                                break;
                                        }
                                        _tradeService.Update(toTrade);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Utility.CommonUtility.LogError(ex);
                                }
                                finally
                                {
                                    if (!tradeSuccess)
                                    {
                                        toTrade.TradeStatus = TradeStatus.Queue;
                                        _tradeService.Update(toTrade);
                                    }
                                }
                            }
                        }
                        else
                        {
                            // Double trade
                            var firstTrade = toTradeItems.Where(p => p.ToCryptoType == ECurrencyType.Bitcoin).FirstOrDefault();
                            var secondTrade = toTradeItems.Where(p => p.FromCryptoType == ECurrencyType.Bitcoin).FirstOrDefault();

                            var fromFirstAsset = firstTrade.FromCryptoType.ToBinanceAsset();
                            var toFirstAsset = firstTrade.ToCryptoType.ToBinanceAsset();

                            var binanceAccountInfo = _binanceService.GetAccountInfo(); // Get balance again
                            var fromCurrencyBalance = binanceAccountInfo.Balances.Where(p => p.Asset == fromFirstAsset).Select(p => p.Free).FirstOrDefault();
                            if (fromCurrencyBalance >= firstTrade.FromAmount)
                            {
                                var firstSymbol = fromFirstAsset + toFirstAsset;
                                firstTrade.TradeStatus = TradeStatus.Requested;
                                _tradeService.Update(firstTrade);

                                var firstTradeSuccess = false;
                                var secondTradeSucess = false;
                                try
                                {
                                    var tradeResult = _binanceService.PostNewOrder(firstSymbol, firstTrade.FromAmount, 0, BinanceOrderSide.SELL, BinanceOrderType.MARKET);
                                    if (!tradeResult.StatusEnum.HasFlag(BinanceTradeStatus.CANCELED | BinanceTradeStatus.EXPIRED | BinanceTradeStatus.REJECTED | BinanceTradeStatus.VOIDED))
                                    {
                                        firstTrade.ToAmount = tradeResult.Fills.Select(p => (p.Price * p.Qty)).DefaultIfEmpty(0).Sum();
                                        firstTrade.Price = tradeResult.Fills.Select(p => p.Price).DefaultIfEmpty(0).Sum();
                                        firstTrade.TradingFee = tradeResult.Fills.Select(p => p.Commission).DefaultIfEmpty(0).Sum();
                                        firstTrade.TradeStatus = TradeStatus.Completed;
                                        firstTrade.TradingFeeType = firstTrade.ToCryptoType;
                                        firstTrade.ApiTradeId = tradeResult.ClientOrderId;
                                        _tradeService.Update(firstTrade);

                                        firstTradeSuccess = true;

                                        // Start second trade
                                        var fromSecondAsset = secondTrade.FromCryptoType.ToBinanceAsset();
                                        var toSecondAsset = secondTrade.ToCryptoType.ToBinanceAsset();
                                        var secondSymbol = fromSecondAsset + toSecondAsset;
                                        secondTrade.TradeStatus = TradeStatus.Requested;
                                        secondTrade.FromAmount = firstTrade.ToAmount - (firstTrade.ToAmount / 1000);
                                        _tradeService.Update(secondTrade);
                                        var secondTradeResult = _binanceService.PostNewOrder(secondSymbol, secondTrade.FromAmount, 0, BinanceOrderSide.SELL, BinanceOrderType.MARKET);
                                        if (!secondTradeResult.StatusEnum.HasFlag(BinanceTradeStatus.CANCELED | BinanceTradeStatus.EXPIRED | BinanceTradeStatus.REJECTED | BinanceTradeStatus.VOIDED))
                                        {
                                            secondTrade.ToAmount = secondTradeResult.Fills.Select(p => (p.Price * p.Qty)).DefaultIfEmpty(0).Sum();
                                            secondTrade.Price = secondTradeResult.Fills.Select(p => p.Price).DefaultIfEmpty(0).Sum();
                                            secondTrade.TradingFee = secondTradeResult.Fills.Select(p => p.Commission).DefaultIfEmpty(0).Sum();
                                            secondTrade.TradeStatus = TradeStatus.Completed;
                                            secondTrade.TradingFeeType = secondTrade.ToCryptoType;
                                            secondTrade.ApiTradeId = secondTradeResult.ClientOrderId;
                                            _tradeService.Update(secondTrade);

                                            secondTradeSucess = true;
                                        }
                                        else
                                        {
                                            switch (secondTradeResult.StatusEnum)
                                            {
                                                case BinanceTradeStatus.CANCELED:
                                                case BinanceTradeStatus.PENDING_CANCEL:
                                                case BinanceTradeStatus.REJECTED:
                                                case BinanceTradeStatus.EXPIRED:
                                                    secondTrade.TradeStatus = TradeStatus.Queue;
                                                    break;
                                                case BinanceTradeStatus.VOIDED:
                                                default:
                                                    secondTrade.TradeStatus = TradeStatus.Voided;
                                                    break;
                                            }
                                            _tradeService.Update(secondTrade);
                                        }
                                    }
                                    else
                                    {
                                        switch (tradeResult.StatusEnum)
                                        {
                                            case BinanceTradeStatus.CANCELED:
                                            case BinanceTradeStatus.PENDING_CANCEL:
                                            case BinanceTradeStatus.REJECTED:
                                            case BinanceTradeStatus.EXPIRED:
                                                firstTrade.TradeStatus = TradeStatus.Queue;
                                                break;
                                            case BinanceTradeStatus.VOIDED:
                                            default:
                                                firstTrade.TradeStatus = TradeStatus.Voided;
                                                break;
                                        }
                                        _tradeService.Update(firstTrade);
                                    }
                                }
                                catch { }
                                finally
                                {
                                    if (!firstTradeSuccess)
                                    {
                                        firstTrade.TradeStatus = TradeStatus.Queue;
                                        _tradeService.Update(firstTrade);
                                    }
                                    if (!secondTradeSucess)
                                    {
                                        secondTrade.TradeStatus = TradeStatus.Queue;
                                        _tradeService.Update(secondTrade);
                                    }
                                }
                            }
                        }
                    }

                }
                #endregion

                #region Buy with BTC and USDT
                var pendingBuyTrades = pendingTrades.Where(p => p.ToCryptoType != ECurrencyType.Tether ||
                                                                  p.FromCryptoType == ECurrencyType.Tether ||
                                                                 (p.FromCryptoType == ECurrencyType.Bitcoin && p.ToCryptoType != ECurrencyType.Tether))
                                                           .ToList();

                if (pendingBuyTrades != null && pendingBuyTrades.Count > 0)
                {
                    var orderIds = pendingBuyTrades.Select(p => p.ExchangeOrderId).Distinct().OrderBy(p => p).ToList();
                    foreach (var orderId in orderIds)
                    {
                        var toTradeItems = pendingBuyTrades.Where(p => p.ExchangeOrderId == orderId.Value).ToList();
                        if (toTradeItems.Count == 1)
                        {
                            // Single trade
                            var toTrade = toTradeItems.FirstOrDefault();
                            var fromAsset = toTrade.FromCryptoType.ToBinanceAsset();
                            var toAsset = toTrade.ToCryptoType.ToBinanceAsset();

                            if (toTrade.FromAmount == 0)
                            {
                                toTrade.FromAmount = _tradeService.TableNoTracking
                                    .Where(p => p.ExchangeOrderId == orderId && p.ToCryptoType == toTrade.FromCryptoType)
                                    .Select(p => p.ToAmount).FirstOrDefault();

                                toTrade.FromAmount -= (toTrade.FromAmount / 1000);
                                _tradeService.Update(toTrade);
                            }

                            var binanceAccountInfo = _binanceService.GetAccountInfo(); // Get balance again
                            var fromCurrencyBalance = binanceAccountInfo.Balances.Where(p => p.Asset == fromAsset).Select(p => p.Free).FirstOrDefault();

                            if (toTrade.FromAmount > 0 && fromCurrencyBalance >= toTrade.FromAmount)
                            {
                                var symbol = toAsset + fromAsset; // Example: From USDT to BTC -> BTCUSDT

                                toTrade.TradeStatus = TradeStatus.Requested;
                                _tradeService.Update(toTrade);

                                var tradeSuccess = false;
                                try
                                {
                                    var priceForSymbol = _binanceService.GetPrice(symbol);
                                    var toAmount = toTrade.FromAmount / priceForSymbol.Price; // This is estimated. Maybe wrong

                                    toAmount = _binanceService.FixBinanceTradeSize(symbol, toAmount, priceForSymbol.Price);
                                    var tradeResult = _binanceService.PostNewOrder(symbol, toAmount, 0, BinanceOrderSide.BUY, BinanceOrderType.MARKET);
                                    if (!tradeResult.StatusEnum.HasFlag(BinanceTradeStatus.CANCELED | BinanceTradeStatus.EXPIRED | BinanceTradeStatus.REJECTED | BinanceTradeStatus.VOIDED))
                                    {
                                        toTrade.ToAmount = toAmount;
                                        toTrade.FromAmount = tradeResult.Fills.Select(p => (p.Price * p.Qty)).DefaultIfEmpty(0).Sum();
                                        toTrade.Price = tradeResult.Fills.Select(p => p.Price).DefaultIfEmpty(0).Sum();
                                        toTrade.TradingFee = tradeResult.Fills.Select(p => p.Commission).DefaultIfEmpty(0).Sum();
                                        toTrade.TradeStatus = TradeStatus.Completed;
                                        toTrade.TradingFeeType = toTrade.FromCryptoType;
                                        toTrade.ApiTradeId = tradeResult.ClientOrderId;
                                        _tradeService.Update(toTrade);
                                        tradeSuccess = true;
                                    }
                                    else
                                    {
                                        switch (tradeResult.StatusEnum)
                                        {
                                            case BinanceTradeStatus.CANCELED:
                                            case BinanceTradeStatus.PENDING_CANCEL:
                                            case BinanceTradeStatus.REJECTED:
                                            case BinanceTradeStatus.EXPIRED:
                                                toTrade.TradeStatus = TradeStatus.Queue;
                                                break;
                                            case BinanceTradeStatus.VOIDED:
                                            default:
                                                toTrade.TradeStatus = TradeStatus.Voided;
                                                break;
                                        }
                                        _tradeService.Update(toTrade);
                                    }
                                }
                                catch { }
                                finally
                                {
                                    if (!tradeSuccess)
                                    {
                                        toTrade.TradeStatus = TradeStatus.Queue;
                                        _tradeService.Update(toTrade);
                                    }
                                }
                            }
                        }
                        else
                        {
                            // Double Trade
                            var firstTrade = toTradeItems.Where(p => p.ToCryptoType == ECurrencyType.Bitcoin).FirstOrDefault();
                            var secondTrade = toTradeItems.Where(p => p.FromCryptoType == ECurrencyType.Bitcoin && p.ToCryptoType != ECurrencyType.Tether).FirstOrDefault();

                            var fromFirstAsset = firstTrade.FromCryptoType.ToBinanceAsset();
                            var toFirstAsset = firstTrade.ToCryptoType.ToBinanceAsset();

                            var binanceAccountInfo = _binanceService.GetAccountInfo(); // Get balance again
                            var fromFirstCurrencyBalance = binanceAccountInfo.Balances.Where(p => p.Asset == fromFirstAsset).Select(p => p.Free).FirstOrDefault();
                            if (fromFirstCurrencyBalance >= firstTrade.FromAmount)
                            {
                                var firstSymbol = toFirstAsset + fromFirstAsset; // Example: From USDT to BTC -> BTCUSDT

                                firstTrade.TradeStatus = TradeStatus.Requested;
                                _tradeService.Update(firstTrade);

                                var firstTradeSuccess = false;
                                var secondTradeSuccess = false;
                                try
                                {
                                    var priceForFirstSymbol = _binanceService.GetPrice(firstSymbol);
                                    var firstToAmount = firstTrade.FromAmount / priceForFirstSymbol.Price; // This is estimated. Maybe wrong

                                    firstToAmount = _binanceService.FixBinanceTradeSize(firstSymbol, firstToAmount, priceForFirstSymbol.Price);
                                    var firstTradeResult = _binanceService.PostNewOrder(firstSymbol, firstToAmount, 0, BinanceOrderSide.BUY, BinanceOrderType.MARKET);
                                    if (!firstTradeResult.StatusEnum.HasFlag(BinanceTradeStatus.CANCELED | BinanceTradeStatus.EXPIRED | BinanceTradeStatus.REJECTED | BinanceTradeStatus.VOIDED))
                                    {
                                        firstTrade.ToAmount = firstToAmount;
                                        firstTrade.FromAmount = firstTradeResult.Fills.Select(p => (p.Price * p.Qty)).DefaultIfEmpty(0).Sum();
                                        firstTrade.Price = firstTradeResult.Fills.Select(p => p.Price).DefaultIfEmpty(0).Sum();
                                        firstTrade.TradingFee = firstTradeResult.Fills.Select(p => p.Commission).DefaultIfEmpty(0).Sum();
                                        firstTrade.TradeStatus = TradeStatus.Completed;
                                        firstTrade.TradingFeeType = firstTrade.FromCryptoType;
                                        firstTrade.ApiTradeId = firstTradeResult.ClientOrderId;
                                        _tradeService.Update(firstTrade);
                                        firstTradeSuccess = true;

                                        // Start second trade
                                        var fromSecondAsset = secondTrade.FromCryptoType.ToBinanceAsset();
                                        var toSecondAsset = secondTrade.ToCryptoType.ToBinanceAsset();
                                        var secondSymbol = toSecondAsset + fromSecondAsset;
                                        secondTrade.TradeStatus = TradeStatus.Requested;
                                        secondTrade.FromAmount = firstTrade.ToAmount - (firstTrade.ToAmount / 1000);
                                        _tradeService.Update(secondTrade);

                                        var priceForSecondSymbol = _binanceService.GetPrice(secondSymbol);
                                        var secondToAmount = secondTrade.FromAmount / priceForSecondSymbol.Price; // This is estimated. Maybe wrong

                                        secondToAmount = _binanceService.FixBinanceTradeSize(secondSymbol, secondToAmount, priceForSecondSymbol.Price);
                                        var secondTradeResult = _binanceService.PostNewOrder(secondSymbol, secondToAmount, 0, BinanceOrderSide.BUY, BinanceOrderType.MARKET);

                                        if (!secondTradeResult.StatusEnum.HasFlag(BinanceTradeStatus.CANCELED | BinanceTradeStatus.EXPIRED | BinanceTradeStatus.REJECTED | BinanceTradeStatus.VOIDED))
                                        {
                                            secondTrade.ToAmount = secondToAmount;
                                            secondTrade.FromAmount = secondTradeResult.Fills.Select(p => (p.Price * p.Qty)).DefaultIfEmpty(0).Sum();
                                            secondTrade.Price = secondTradeResult.Fills.Select(p => p.Price).DefaultIfEmpty(0).Sum();
                                            secondTrade.TradingFee = secondTradeResult.Fills.Select(p => p.Commission).DefaultIfEmpty(0).Sum();
                                            secondTrade.TradeStatus = TradeStatus.Completed;
                                            secondTrade.TradingFeeType = secondTrade.FromCryptoType;
                                            secondTrade.ApiTradeId = secondTradeResult.ClientOrderId;
                                            _tradeService.Update(secondTrade);
                                            secondTradeSuccess = true;
                                        }
                                        else
                                        {
                                            switch (secondTradeResult.StatusEnum)
                                            {
                                                case BinanceTradeStatus.CANCELED:
                                                case BinanceTradeStatus.PENDING_CANCEL:
                                                case BinanceTradeStatus.REJECTED:
                                                case BinanceTradeStatus.EXPIRED:
                                                    secondTrade.TradeStatus = TradeStatus.Queue;
                                                    break;
                                                case BinanceTradeStatus.VOIDED:
                                                default:
                                                    secondTrade.TradeStatus = TradeStatus.Voided;
                                                    break;
                                            }
                                            _tradeService.Update(firstTrade);
                                        }
                                    }
                                    else
                                    {
                                        switch (firstTradeResult.StatusEnum)
                                        {
                                            case BinanceTradeStatus.CANCELED:
                                            case BinanceTradeStatus.PENDING_CANCEL:
                                            case BinanceTradeStatus.REJECTED:
                                            case BinanceTradeStatus.EXPIRED:
                                                firstTrade.TradeStatus = TradeStatus.Queue;
                                                break;
                                            case BinanceTradeStatus.VOIDED:
                                            default:
                                                firstTrade.TradeStatus = TradeStatus.Voided;
                                                break;
                                        }
                                        _tradeService.Update(firstTrade);
                                    }
                                }
                                catch { }
                                finally
                                {
                                    if (!firstTradeSuccess)
                                    {
                                        firstTrade.TradeStatus = TradeStatus.Queue;
                                        _tradeService.Update(firstTrade);
                                    }
                                    if (!secondTradeSuccess)
                                    {
                                        secondTrade.TradeStatus = TradeStatus.Queue;
                                        _tradeService.Update(secondTrade);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Coin to coin Sell/Buy
                var pendingCoinToCoinTrades = pendingTrades.Where(p => p.FromCryptoType != ECurrencyType.Bitcoin
                                                            && p.FromCryptoType != ECurrencyType.Tether
                                                            && p.ToCryptoType != ECurrencyType.Bitcoin
                                                            && p.ToCryptoType != ECurrencyType.Tether)
                                                   .ToList();

                if (pendingCoinToCoinTrades != null && pendingCoinToCoinTrades.Count > 0)
                {
                    var binancePrices = _binanceService.GetAllPricesChange24H();
                    var symbols = binancePrices.Select(p => p.Symbol).ToList();

                    var orderIds = pendingCoinToCoinTrades.Select(p => p.ExchangeOrderId).Distinct().OrderBy(p => p).ToList();
                    foreach (var orderId in orderIds)
                    {
                        var toTradeItems = pendingCoinToCoinTrades.Where(p => p.ExchangeOrderId == orderId.Value).ToList();
                        if (toTradeItems.Count == 1)
                        {
                            var toTrade = toTradeItems.FirstOrDefault();
                            var fromAsset = toTrade.FromCryptoType.ToBinanceAsset();
                            var toAsset = toTrade.ToCryptoType.ToBinanceAsset();
                            var isFromTo = symbols.Contains($"{fromAsset}{toAsset}");
                            var symbol = isFromTo ? $"{fromAsset}{toAsset}" : $"{toAsset}{fromAsset}";
                            var tradeSide = isFromTo ? BinanceOrderSide.SELL : BinanceOrderSide.BUY;

                            var tradeAmount = isFromTo ? toTrade.FromAmount : 0;
                            if (tradeAmount == 0)
                            {
                                var priceForSymbol = _binanceService.GetPrice(symbol);
                                //to buy Amount
                                tradeAmount = toTrade.FromAmount / priceForSymbol.Price; // This is estimated. Maybe wrong
                                tradeAmount = _binanceService.FixBinanceTradeSize(symbol, tradeAmount, priceForSymbol.Price);
                            }

                            var tradeSuccess = false;
                            try
                            {
                                var tradeResult = _binanceService.PostNewOrder(symbol, tradeAmount, 0, tradeSide, BinanceOrderType.MARKET);

                                if (!tradeResult.StatusEnum.HasFlag(BinanceTradeStatus.CANCELED | BinanceTradeStatus.EXPIRED | BinanceTradeStatus.REJECTED | BinanceTradeStatus.VOIDED))
                                {
                                    var tradedAmount = tradeResult.Fills.Select(p => (p.Price * p.Qty)).DefaultIfEmpty(0).Sum();
                                    toTrade.ToAmount = tradeSide == BinanceOrderSide.BUY ? tradeAmount : tradedAmount;
                                    toTrade.FromAmount = tradeSide == BinanceOrderSide.BUY ? tradedAmount : toTrade.FromAmount;
                                    toTrade.Price = tradeResult.Fills.Select(p => p.Price).DefaultIfEmpty(0).Sum();
                                    toTrade.TradingFee = tradeResult.Fills.Select(p => p.Commission).DefaultIfEmpty(0).Sum();
                                    toTrade.TradeStatus = TradeStatus.Completed;
                                    toTrade.TradingFeeType = tradeSide == BinanceOrderSide.BUY ? toTrade.FromCryptoType : toTrade.ToCryptoType;
                                    toTrade.ApiTradeId = tradeResult.ClientOrderId;
                                    _tradeService.Update(toTrade);
                                    tradeSuccess = true;
                                }
                                else
                                {
                                    switch (tradeResult.StatusEnum)
                                    {
                                        case BinanceTradeStatus.CANCELED:
                                        case BinanceTradeStatus.PENDING_CANCEL:
                                        case BinanceTradeStatus.REJECTED:
                                        case BinanceTradeStatus.EXPIRED:
                                            toTrade.TradeStatus = TradeStatus.Queue;
                                            break;
                                        case BinanceTradeStatus.VOIDED:
                                        default:
                                            toTrade.TradeStatus = TradeStatus.Voided;
                                            break;
                                    }
                                    _tradeService.Update(toTrade);
                                }
                            }
                            catch { }
                            finally
                            {
                                if (!tradeSuccess)
                                {
                                    toTrade.TradeStatus = TradeStatus.Queue;
                                    _tradeService.Update(toTrade);
                                }
                            }
                        }
                    }
                }
                #endregion
            }
        }
    }
}
