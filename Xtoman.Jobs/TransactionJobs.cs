﻿using System;
using System.Linq;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.TradingPlatform.Binance;
using Xtoman.Service;
using Xtoman.Service.WebServices;

namespace Xtoman.Jobs
{
    public class TransactionJobs : ITransactionJobs
    {
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly IWithdrawService _withdrawService;
        private readonly IBinanceService _binanceService;
        public TransactionJobs(
            IExchangeOrderService exchangeOrderService,
            IWithdrawService withdrawService,
            IBinanceService binanceService)
        {
            _exchangeOrderService = exchangeOrderService;
            _withdrawService = withdrawService;
            _binanceService = binanceService;
        }

        public void GetUpdateWithdrawalsTransactionIds()
        {
            var pastDays = DateTime.UtcNow.AddDays(-5);
            var exchangeOrders = _exchangeOrderService.Table
                .Where(p => p.OrderStatus == OrderStatus.Complete && p.ReceiveTransactionCode == null)
                .Where(p => p.Exchange.ExchangeFiatCoinType == ExchangeFiatCoinType.CoinToCoin ||
                            p.Exchange.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatTomanToCoin ||
                            p.Exchange.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatUSDToCoin)
                .Where(p => p.OrderWithdraws.Any(x => x.TradingPlatform == TradingPlatform.Binance && x.Status != WithdrawStatus.Completed))
                .Where(p => p.InsertDate >= pastDays)
                .ToList();

            if (exchangeOrders != null && exchangeOrders.Count > 0)
            {
                var binanceWithdraws = _binanceService.GetWithdrawHistory(null, null, null, null, 50000);
                if (binanceWithdraws != null && binanceWithdraws.Success && binanceWithdraws.WithdrawList != null && binanceWithdraws.WithdrawList.Any())
                {
                    exchangeOrders.ForEach((x) =>
                    {
                        var orderWithdrawBinanceId = x.OrderWithdraws
                            .Where(p => p.TradingPlatform == TradingPlatform.Binance)
                            .Select(p => p.ApiWithdrawId)
                            .FirstOrDefault();

                        var binanceWithdrawItem = binanceWithdraws.WithdrawList.Where(p => p.Id == orderWithdrawBinanceId).FirstOrDefault();
                        if (binanceWithdrawItem != null)
                        {
                            x.ReceiveTransactionCode = binanceWithdrawItem.TxId;
                            _exchangeOrderService.Update(exchangeOrders);

                            var withdraw = _withdrawService.Table.Single(p => p.ApiWithdrawId == orderWithdrawBinanceId);
                            withdraw.Status = Enum.TryParse(binanceWithdrawItem.StatusEnum.ToString(), true, out WithdrawStatus result) ? result : WithdrawStatus.Completed;
                            withdraw.TransactionId = binanceWithdrawItem.TxId;
                            _withdrawService.Update(withdraw);
                        }
                    });
                }
            }
        }
    }
}
