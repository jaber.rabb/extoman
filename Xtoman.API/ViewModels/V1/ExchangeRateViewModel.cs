﻿namespace Xtoman.API.ViewModels.V1
{
    public class ExchangeRateViewModel
    {
        public decimal Rate { get; set; }
        public bool IsAvailable { get; set; }
        public byte FromPrecision { get; set; }
        public byte ToPrecision { get; set; }
        public bool ReversedRate { get; set; }
    }
}