﻿namespace Xtoman.API.ViewModels.V1
{
    public class BalanceViewModel
    {
        public int CurrencyId { get; set; }
        public string Name { get; set; }
        public decimal Available { get; set; }
        public string UnitSign { get; set; }
    }
}