﻿using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Service;

namespace Xtoman.API.ViewModels.V1
{
    public class ExchangeTypeSimpleViewModel : BaseViewModel<ExchangeTypeViewModel, ExchangeType>
    {
        public int FromCurrencyId { get; set; }
        public int ToCurrencyId { get; set; }
        public bool Disabled { get; set; }
        public bool Invisible { get; set; }
    }

    public class ExchangeTypeViewModel : BaseViewModel<ExchangeTypeViewModel, ExchangeType>
    {
        public int FromCurrencyId { get; set; }
        public int ToCurrencyId { get; set; }
        public bool Disabled { get; set; }
        public bool Invisible { get; set; }
        public ExchangeFiatCoinType ExchangeFiatCoinType { get; set; }
        public ExchangeRate ExchangeRate { get; set; }
        public virtual CurrencyViewModel FromCurrency { get; set; }
        public virtual CurrencyViewModel ToCurrency { get; set; }
    }
}