﻿using Microsoft.Web.Http;
using System.Web.Http;
using Xtoman.Framework.WebApi;
using Xtoman.Service;

namespace Xtoman.API.ViewModels.V1
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/user")]
    public class UserController : BaseApiController
    {
        private readonly IAppUserManager _userManager;
        public UserController(IAppUserManager userManager)
        {
            _userManager = userManager;
        }


    }
}