﻿using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.API.ViewModels.V1
{
    public class CurrencyViewModel : BaseViewModel<CurrencyViewModel, CurrencyType>
    {
        public string Name { get; set; }
        public string AlternativeName { get; set; }
        public string Description { get; set; }
        public string UnitSign { get; set; }
        public bool UnitSignIsAfter { get; set; }
        public ECurrencyType Type { get; set; }
        public decimal MinimumReceiveAmount { get; set; }
        public decimal Step { get; set; }
        public bool Disabled { get; set; }
        public bool Invisible { get; set; }
        public string Url { get; set; }
        public bool IsFiat { get; set; }
        public byte MaxPrecision { get; set; }
        public string AddressRegEx { get; set; }
        public decimal? TransactionFee { get; set; }

        public int ConfirmsNeed { get; set; }

        public bool HasMemo { get; set; }
        public string MemoName { get; set; }
        public string MemoRegEx { get; set; }
        public string MemoDescription { get; set; }
    }
}