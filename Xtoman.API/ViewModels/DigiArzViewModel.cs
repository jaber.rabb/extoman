﻿using System.Collections.Generic;

namespace Xtoman.API.ViewModels
{
    public class DigiArzViewModel
    {
        public DigiArzItemViewModel digiarz { get; set; }
    }

    public class DigiArzItemViewModel
    {
        public List<DigiArzCurrencyViewModel> currency { get; set; }
    }

    public class DigiArzCurrencyViewModel
    {
        public string from { get; set; }
        public string to { get; set; }
        public decimal price { get; set; }
        public decimal amount { get; set; }
    }
}