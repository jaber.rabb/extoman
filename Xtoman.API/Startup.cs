﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Xtoman.API.Startup))]

namespace Xtoman.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
