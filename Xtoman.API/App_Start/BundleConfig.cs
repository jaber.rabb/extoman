﻿using System.Web;
using System.Web.Optimization;

namespace Xtoman.API
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;

            #region Scripts
            bundles.Add(new ScriptBundle("~/js/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/js/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/js/jquery-pjax").Include(
                        "~/Scripts/jquery.pjax.js"));

            bundles.Add(new ScriptBundle("~/js/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/js/main").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.js",
                      "~/Plugins/select2/js/select2.full.min.js",
                      "~/Plugins/jquery-bar-rating/jquery.barrating.min.js",
                      "~/Scripts/script.js"));

            #endregion

            #region Styles
            bundles.Add(new StyleBundle("~/css/main").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/fontawesome.css",
                      "~/Content/icomoon.css",
                      "~/Plugins/select2/css/select2.min.css",
                      "~/Plugins/jquery-bar-rating/themes/bars-reversed.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/pagedlist").Include(
                    "~/Content/css/pagedlist.css"));

            #endregion

            #region Plugins

            #region MD Persian Date Time
            bundles.Add(new ScriptBundle("~/js/persiandate").Include(
                "~/Scripts/persianDatepicker.js"));
            bundles.Add(new StyleBundle("~/css/persiandate").Include(
                "~/Content/persianDatepicker-default.css"));
            #endregion

            #region Socket.io
            bundles.Add(new ScriptBundle("~/js/socketio").Include(
                "~/Scripts/socket.io.js"));
            #endregion

            #region Jasny
            bundles.Add(new ScriptBundle("~/js/jasny-bootstrap").Include(
                     "~/Plugins/jasny-bootstrap/js/jasny-bootstrap*"));
            bundles.Add(new StyleBundle("~/css/jasny-bootstrap").Include(
                     "~/Plugins/jasny-bootstrap/css/jasny-bootstrap*"));
            #endregion

            #region OWL Carousel
            bundles.Add(new ScriptBundle("~/js/owl-carousel").Include(
                    "~/Plugins/owl-carousel/owl.carousel.min.js"));

            bundles.Add(new StyleBundle("~/css/owl-carousel").Include(
                    "~/Plugins/owl-carousel/assets/owl.carousel.min.css"));
            #endregion

            #region JQuery Bar Rating
            bundles.Add(new ScriptBundle("~/js/jquery-bar-rating").Include(
                    "~/Plugins/jquery-bar-rating/jquery.barrating.min.js"));

            bundles.Add(new StyleBundle("~/css/jquery-bar-rating").Include(
                    "~/Plugins/jquery-bar-rating/themes/bars-reversed.css"));
            // "~/Plugins/jquery-bar-rating/themes/bars-movie.css"));
            //"~/Plugins/jquery-bar-rating/themes/fontawesome-stars.css"));
            #endregion

            #region ChartJS
            bundles.Add(new ScriptBundle("~/js/chartjs").Include(
                // "~/Plugins/chartjs/Chart.min.js")); // Without moment js
                "~/Plugins/chartjs/Chart.bundle.min.js")); // include moment js
            #endregion

            #endregion

            BundleTable.EnableOptimizations = true;
        }
    }
}
