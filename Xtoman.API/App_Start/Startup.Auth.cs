﻿using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Owin;
using StructureMap.Web;
using System.Web.Http;
using Xtoman.Framework.DependencyResolution;
using Xtoman.Framework.WebApi.Token;
using Xtoman.Service;

namespace Xtoman.API
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit https://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            IoC.Container.Configure(config =>
            {
                config.For<IDataProtectionProvider>()
                      .HybridHttpOrThreadLocalScoped()
                      .Use(() => app.GetDataProtectionProvider());
            });
            IoC.Container.Configure(p => p.For<IJwtConfiguration>().Singleton().Use(JwtConfiguration.Configuration));
            var configuration = IoC.Container.GetInstance<IJwtConfiguration>();

            app.CreatePerOwinContext(() => IoC.Container.GetInstance<IAppUserManager>());

            var OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = configuration.TokenEndpointPath,
                AccessTokenExpireTimeSpan = configuration.AccessTokenExpireTimeSpan,
                AllowInsecureHttp = configuration.AllowInsecureHttp,
                AccessTokenFormat = new JwtWriterFormat(configuration),
                Provider = IoC.Container.GetInstance<IOAuthAuthorizationServerProvider>(),
                //RefreshTokenProvider = IoC.Container.GetInstance<IAuthenticationTokenProvider>()
            };

            app.UseOAuthAuthorizationServer(OAuthOptions);

            //app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
            //{
            //    AuthenticationMode = configuration.AuthenticationMode,
            //    AllowedAudiences = configuration.AllowedAudiences,
            //    IssuerSecurityTokenProviders = configuration.IssuerSecurityTokenProviders,
            //    //Provider = new OAuthBearerAuthenticationProvider
            //    //{
            //    //    OnValidateIdentity = context =>
            //    //    {
            //    //        context.Ticket.Identity.AddClaim(new System.Security.Claims.Claim("newCustomClaim", "newValue"));
            //    //        return Task.FromResult(0);
            //    //    }
            //    //}
            //});

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(GlobalConfiguration.Configuration);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //    consumerKey: "",
            //    consumerSecret: "");

            //app.UseFacebookAuthentication(
            //    appId: "",
            //    appSecret: "");

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});
        }
    }
}
