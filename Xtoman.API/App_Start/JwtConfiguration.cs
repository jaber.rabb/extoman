﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Jwt;
using System;
using System.Collections.Generic;
using System.ServiceModel.Security.Tokens;
using Xtoman.Framework.WebApi.Token;
using Xtoman.Utility;

namespace Xtoman.API
{
    public class JwtConfiguration : IJwtConfiguration
    {
        public bool AllowInsecureHttp { get; set; }
        public string Issuer { get; set; }
        public string Secret { get; set; }
        public string TokenPath { get; set; }
        public string AuthorizePath { get; set; }
        public int ExpirationMinutes { get; set; }
        public int RefreshTokenExpirationMinutes { get; set; }
        public PathString TokenEndpointPath { get; set; }
        public PathString AuthorizeEndpointPath { get; set; }
        public TimeSpan AccessTokenExpireTimeSpan { get; set; }
        //public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; set; }
        //public IOAuthAuthorizationServerProvider Provider { get; set; }
        //public IAuthenticationTokenProvider RefreshTokenProvider { get; set; }

        public AuthenticationMode AuthenticationMode { get; set; }
        public string[] AllowedAudiences { get; set; }
        public IEnumerable<IssuedSecurityTokenProvider> IssuerSecurityTokenProviders { get; set; }


        public static JwtConfiguration Configuration
        {
            get
            {
                var config = new JwtConfiguration()
                {
                    AllowInsecureHttp = true,
                    TokenPath = "/Token",
                    Issuer = "https://www.extoman.co/"
                };
#if DEBUG
                config.Issuer = "http://localhost/";
#endif
                config.Secret = "4d53bce03ec34c0a911182d4c228ee6c";
                config.ExpirationMinutes = 15;
                config.RefreshTokenExpirationMinutes = 60;

                //config.AccessTokenFormat = new JwtWriterFormat(/*Configuration*/);
                //config.Provider = IoC.Container.GetInstance<IOAuthAuthorizationServerProvider>();
                //config.RefreshTokenProvider = IoC.Container.GetInstance<IAuthenticationTokenProvider>();
                config.TokenEndpointPath = new PathString(config.TokenPath);
                config.AuthorizeEndpointPath = PathString.Empty;
                config.AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(config.ExpirationMinutes);

                config.AuthenticationMode = AuthenticationMode.Active;
                config.AllowedAudiences = new[] { "Any" };
                //config.IssuerSecurityTokenProviders = new[]
                //{
                //    //new X509CertificateSecurityTokenProvider(
                //    //    issuer: config.Issuer,
                //    //    certificate: new System.Security.Cryptography.X509Certificates.X509Certificate2(new SecurityService().GetBytes(config.Secret)))
                //    new SymmetricKeyIssuerSecurityTokenProvider(
                //        issuer: config.Issuer,
                //        key: config.Secret.GetBytes()
                //        /*base64Key: Convert.ToBase64String(config.Secret.GetBytes()*/)
                //};
                return config;
            }
        }
    }

}