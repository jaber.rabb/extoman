﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Xtoman.Framework.DependencyResolution;
using Xtoman.Framework.WebApi.DelegateHandlers;
using Xtoman.Framework.WebApi.Filters;
using Xtoman.Framework.WebApi.Services;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Dispatcher;
using System.Web.Http.Filters;
using Microsoft.Web.Http.Routing;
using System.Web.Http.Routing;

namespace Xtoman.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Filters.Add(new ErrorExceptionFilterAttribute());
            config.Filters.Add(new ValidationFilterAttribute());
            //config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

            //config.ParameterBindingRules.Add(typeof(DateTime?), parameter => new DateTimeParameterBinding(parameter));
            //config.ParameterBindingRules.Add(typeof(DateTime?), param => param.BindWithFormatter(new CustomMediaFormatter()));
            //config.ParameterBindingRules.Add(typeof(DateTime?), param => param.BindWithModelBinding(new DateTimeModelBinder()));
            //config.Services.Add(typeof(ModelBinderProvider), new SimpleModelBinderProvider(typeof(DateTime?), new DateTimeModelBinder()));

            // Web API configuration and services
            config.Services.Replace(typeof(IHttpControllerActivator), new StructureMapHttpControllerActivator(IoC.Container));
            config.Services.Replace(typeof(IFilterProvider), new IoCFilterProvider());
            config.Services.Replace(typeof(IHttpControllerTypeResolver), new BaseApiControllerTypeResolver());
            config.Services.Replace(typeof(IHttpControllerSelector), new NamespaceControllerSelector(config));

            // Web API routes
            //config.MapHttpAttributeRoutes();

            var constraintResolver = new DefaultInlineConstraintResolver()
            {
                ConstraintMap =
            {
                ["apiVersion"] = typeof( ApiVersionRouteConstraint )
            }
            };
            config.MapHttpAttributeRoutes(constraintResolver);
            config.AddApiVersioning();

            //config.Routes.MapHttpRoute(
            //    "DefaultVersionedApi",
            //    routeTemplate: "v{version}/{controller}/{action}/{id}/{id2}",
            //    constraints: new { version = @"\d+" },
            //    defaults: new
            //    {
            //        id = RouteParameter.Optional,
            //        id2 = RouteParameter.Optional
            //    });

            config.Routes.MapHttpRoute(
                name: "ExchangeRate",
                routeTemplate: "Rate/{id}/{id2}");

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{action}/{id}",
                defaults: new
                {
                    id = RouteParameter.Optional
                });

            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
            config.MessageHandlers.Add(new CorsRequestsHandler());

            var settings = config.Formatters.JsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.Indented;
            //settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            //var serializerSettings = new JsonSerializerSettings();
            //serializerSettings.Converters.Add(new IsoDateTimeConverter());
            //config.Formatters.JsonFormatter.SerializerSettings = serializerSettings;
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new IsoDateTimeConverter());

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            //config.EnableSystemDiagnosticsTracing();
        }
    }

    //public static class WebApiConfig
    //{
    //    public static void Register(HttpConfiguration config)
    //    {
    //        // Web API configuration and services
    //        // Configure Web API to use only bearer token authentication.
    //        config.SuppressDefaultHostAuthentication();
    //        config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

    //        // Web API routes
    //        config.MapHttpAttributeRoutes();

    //        config.Routes.MapHttpRoute(name: "DigiArzApi", routeTemplate: "digiarz", defaults: new { controller = "Digiarz", action = "Get" });

    //        config.Routes.MapHttpRoute(
    //            name: "DefaultApi",
    //            routeTemplate: "api/{controller}/{id}",
    //            defaults: new { id = RouteParameter.Optional }
    //        );
    //    }
    //}
}
