﻿using Microsoft.Web.Http;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Xtoman.API.ViewModels.V1;
using Xtoman.Framework.WebApi;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/balance")]
    public class BalanceController : BaseApiController
    {
        private readonly IBalanceManager _balanceManager;
        private readonly ICurrencyTypeService _currencyTypeService;
        public BalanceController(
            IBalanceManager balanceManager,
            ICurrencyTypeService currencyTypeService)
        {
            _balanceManager = balanceManager;
            _currencyTypeService = currencyTypeService;
        }

        [Route("v{version:apiVersion}/balance/{id?}")]
        public async Task<IHttpActionResult> Get(int? id = null)
        {
            try
            {
                var currencies = await _currencyTypeService.GetAllCachedAsync();
                if (id == null)
                {
                    var availableBalances = await _balanceManager.GetAllBalancesAsync();
                    return Ok(new List<BalanceViewModel>(availableBalances.Select(p => new BalanceViewModel()
                    {
                        CurrencyId = currencies.Where(x => x.Type.Equals(p.Type)).Select(x => x.Id).Single(),
                        Available = p.Amount,
                        Name = p.Name,
                        UnitSign = p.Type.ToDisplay(DisplayProperty.ShortName)
                    })
                    .ToList()));
                }
                else
                {
                    var currency = currencies.Where(p => p.Id == id.Value).FirstOrDefault();
                    if (currency != null)
                    {
                        var balance = await _balanceManager.GetBalanceAsync(currency.Type, true);
                        return Ok(new BalanceViewModel() { CurrencyId = id.Value, Available = balance, Name = currency.Name, UnitSign = currency.UnitSign });
                    }
                    return NotFound();
                }
            }
            catch (System.Exception ex)
            {
                ex.LogError();
                throw ex;
            }
        }
    }
}