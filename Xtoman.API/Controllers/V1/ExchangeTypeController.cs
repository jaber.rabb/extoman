﻿using AutoMapper;
using Microsoft.Web.Http;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Http;
using Xtoman.API.ViewModels.V1;
using Xtoman.Framework.WebApi;
using Xtoman.Service;

namespace Xtoman.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/exchangetype")]
    public class ExchangeTypeController : BaseApiController
    {
        private readonly IExchangeTypeService _exchangeTypeService;
        private readonly IPriceManager _priceManager;
        public ExchangeTypeController(
            IExchangeTypeService exchangeTypeService,
            IPriceManager priceManager)
        {
            _priceManager = priceManager;
            _exchangeTypeService = exchangeTypeService;
        }

        public async Task<IEnumerable<ExchangeTypeSimpleViewModel>> Get()
        {
            var model = await _exchangeTypeService.TableNoTracking
                .ProjectToListAsync<ExchangeTypeSimpleViewModel>();
            return model;
        }

        public async Task<ExchangeTypeViewModel> Get(int Id)
        {
            var entity = await _exchangeTypeService.TableNoTracking.SingleAsync(p => p.Id == Id);
            var model = new ExchangeTypeViewModel().FromEntity(entity);
            model.ExchangeRate = await _priceManager.GetExchangeRateAsync(entity);
            return model;
        }
    }
}