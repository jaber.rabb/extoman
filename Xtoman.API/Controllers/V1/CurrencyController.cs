﻿using Microsoft.Web.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Xtoman.API.ViewModels.V1;
using Xtoman.Framework.WebApi;
using Xtoman.Service;

namespace Xtoman.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/currency")]
    public class CurrencyController : BaseApiController
    {
        private readonly ICurrencyTypeService _currencyTypeService;
        public CurrencyController(ICurrencyTypeService currencyTypeService)
        {
            _currencyTypeService = currencyTypeService;
        }
        public async Task<IEnumerable<CurrencyViewModel>> Get()
        {
            var currencies = await _currencyTypeService.GetAllActiveCachedAsync();
            var model = new List<CurrencyViewModel>();
            foreach (var item in currencies)
                model.Add(new CurrencyViewModel().FromEntity(item));

            return model;
        }
    }
}
