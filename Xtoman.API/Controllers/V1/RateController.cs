﻿using Microsoft.Web.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Xtoman.API.ViewModels.V1;
using Xtoman.Framework.WebApi;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/rate")]
    public class RateController : BaseApiController
    {
        private readonly IPriceManager _priceManager;
        public RateController(IPriceManager priceManager)
        {
            _priceManager = priceManager;
        }

        //public async Task<IEnumerable<ExchangeRate>> Get()
        //{
        //    return await _priceManager.GetExchangeRateAsync();
        //}

        [Route("v{version:apiVersion}/rate/{id:int}/{id2:int}")]
        public async Task<ExchangeRateViewModel> Get(int id, int id2)
        {
            var result = await _priceManager.GetExchangeRateAsync(id, id2);
            result.Rate.ToBlockChainPrice(0);
            var rate = result.ReversedRate ? result.Rate.ToBlockChainPrice(result.FromPrecision).ToDecimal() : result.Rate.ToBlockChainPrice(result.ToPrecision).ToDecimal();
            return new ExchangeRateViewModel { 
                Rate = rate,
                IsAvailable = result.IsAvailable,
                FromPrecision = result.FromPrecision,
                ToPrecision = result.ToPrecision,
                ReversedRate = result.ReversedRate
            };
        }
    }
}