﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Xtoman.API.Models;
using Xtoman.Domain.Models;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.API.Controllers.V1
{
    [Authorize]
    [ApiVersion("1.0")]
    [RoutePrefix("v{version:apiVersion}/account")]
    public class AccountController : ApiController
    {
        private readonly IAppUserManager _userManager;
        private readonly IUserService _userService;
        public AccountController(
            IAppUserManager userManager,
            IUserService userService)
        {
            _userManager = userManager;
            _userService = userService;
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (await _userService.EmailOrPhoneIsUniqueAsync(model.EmailOrPhone))
            {
                var user = new AppUser
                {
                    UserName = model.EmailOrPhone,
                    Email = model.EmailOrPhone.IsEmail() ? model.EmailOrPhone : null,
                    ReferrerId = model.ReferrerId,
                    PhoneNumber = model.EmailOrPhone.IsEmail() ? null : model.EmailOrPhone
                };

                var result = await _userManager.CreateAsync(user, model.Password);

                if (!result.Succeeded)
                    return GetErrorResult(result);

                return Ok();
            }

            ModelState.AddModelError("EmailOrPhone", "شماره موبایل یا ایمیل وارد شده تکراری است.");
            return BadRequest(ModelState);
        }

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        #region Helpers
        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
        #endregion
    }
}
