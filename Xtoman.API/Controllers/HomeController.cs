﻿using System.Web.Mvc;
using Xtoman.Framework;

namespace Xtoman.API.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}