﻿using AutoMapper;
using Xtoman.Framework.Core;

namespace Xtoman.Api.AutoMapper
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.ConfigureAutoMapper();
            });
        }

    }
}