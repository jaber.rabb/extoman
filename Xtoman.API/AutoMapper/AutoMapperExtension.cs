﻿using AutoMapper;

namespace Xtoman.Api.AutoMapper
{
    public static class AutoMapperExtension
    {
        public static TDestination MapTo<TSource, TDestination>(this TSource source) => Mapper.Map<TSource, TDestination>(source);

        public static TDestination MapTo<TSource, TDestination>(this TSource source, TDestination destination) => Mapper.Map(source, destination);
    }
}