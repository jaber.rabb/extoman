﻿using AutoMapper;
using Xtoman.Framework.Core;

namespace Xtoman.Telegram.AutoMapper
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.ConfigureAutoMapper();
            });
        }
    }
}