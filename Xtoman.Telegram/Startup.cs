﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Xtoman.Telegram.Startup))]
namespace Xtoman.Telegram
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
