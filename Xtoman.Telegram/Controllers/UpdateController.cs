﻿using System.Threading.Tasks;
using System.Web.Http;
using Telegram.Bot.Types;
using Xtoman.Framework.WebApi;
using Xtoman.Service.WebServices;

namespace Xtoman.Telegram.Controllers
{
    public class UpdateController : BaseApiController
    {
        #region Properties
        private readonly ITelegramWebHook _telegramWebHook;
        #endregion

        #region Constructor
        public UpdateController(ITelegramWebHook telegramWebHook)
        {
            _telegramWebHook = telegramWebHook;
        }
        #endregion

        #region Actions
        public async Task<IHttpActionResult> Post([FromBody]Update update)
        {
            await _telegramWebHook.HandleUpdateAsync(update);
            return Ok();
        }
        #endregion
    }
}