﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Telegram.Controllers
{
    public class WebHookController : Controller
    {
        #region Properties
        private readonly ITelegramWebHook _telegramWebHook;
        #endregion

        #region Constructor
        public WebHookController(ITelegramWebHook telegramWebHook)
        {
            _telegramWebHook = telegramWebHook;
        }
        #endregion

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Index(string updateUrl)
        {
            if (!updateUrl.HasValue())
                updateUrl = "https://telegram.extoman.co/api/update";
            var result = await _telegramWebHook.SetWebHookAsync(updateUrl).ConfigureAwait(false);
            return Content(result);
        }

        public async Task<ActionResult> Delete()
        {
            var result = await _telegramWebHook.DeleteWebHookAsync().ConfigureAwait(false);
            return Content(result);
        }
    }
}