﻿using System.Web.Mvc;
using Xtoman.Framework.Mvc;

namespace Xtoman.Telegram
{
    public class ModelBinderConfig
    {
        public static void Register(ModelBinderDictionary binders)
        {
            binders.Add(typeof(string), new StringModelBinder());
            binders.Add(typeof(int), new IntModelBinder());
            binders.Add(typeof(decimal), new DecimalModelBinder());
            binders.Add(typeof(BaseViewModel), new BaseViewModelBinder());
        }
    }
}