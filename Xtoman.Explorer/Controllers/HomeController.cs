﻿using System.Web.Mvc;
using Xtoman.Framework;

namespace Xtoman.Explorer.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}