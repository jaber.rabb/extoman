﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Framework;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Explorer.Controllers
{
    public class BlockController : BaseController
    {
        private readonly IBlockCypherService _blockCypherService;
        public BlockController(IBlockCypherService blockCypherService)
        {
            _blockCypherService = blockCypherService;
        }

        public async Task<ActionResult> Index(string coin, string input)
        {
            var network = GetNetwork(coin);
            if (input.HasValue())
                return RedirectToAction("Index", "Home", new { coin });

            var model = await _blockCypherService.GetBlockAsync(network, input, null, null);
            return View(model);
        }

        public async Task<ActionResult> LoadMore(string coin, string input, int page = 1, int pSize = 20)
        {
            var txStart = (page - 1) * pSize;
            var network = GetNetwork(coin);
            if (input.HasValue())
                return HttpNotFound();

            var model = await _blockCypherService.GetBlockAsync(network, input, txStart, pSize);
            return PartialView(model);
        }
    }
}