﻿namespace Xtoman.Explorer
{
    public static class BlockchainRegEx
    {
        public static readonly string BitcoinAddressRegEx = "^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$";
        public static readonly string BitcoinBlockHashRegEx = "^[0]{8}[a-fA-F0-9]{56}$";
        public static readonly string BitcoinTransactionHashRegEx = "^[a-fA-F0-9]{64}$";

        public static readonly string LitecoinAddressRegEx = "^[LM3][a-km-zA-HJ-NP-Z1-9]{26,33}$";
        public static readonly string LitecoinBlockHashRegEx = "^[a-fA-F0-9]{64}$";
        public static readonly string LitecoinTransactionHashRegEx = "^[a-fA-F0-9]{64}$";

        public static readonly string DogecoinAddressRegEx = "^D{1}[5-9A-HJ-NP-U]{1}[1-9A-HJ-NP-Za-km-z]{32}";
        public static readonly string DogecoinBlockHashRegEx = "^[a-fA-F0-9]{64}$";
        public static readonly string DogecoinTransactionHashRegEx = "^[a-fA-F0-9]{64}$";

        public static readonly string DashAddressRegEx = "^X[1-9A-HJ-NP-Za-km-z]{33}";
        public static readonly string DashBlockHashRegEx = "^[0]{8}[a-fA-F0-9]{56}$";
        public static readonly string DashTransactionHashRegEx = "^[a-fA-F0-9]{64}$";
    }
}