﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Xtoman.Explorer
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "BitcoinIndex",
                url: "BTC",
                defaults: new { controller = "BTC", action = "Index" }
            );

            routes.MapRoute(
                name: "LitecoinIndex",
                url: "LTC",
                defaults: new { controller = "LTC", action = "Index" }
            );

            routes.MapRoute(
                name: "DogecoinIndex",
                url: "Doge",
                defaults: new { controller = "Doge", action = "Index" }
            );

            routes.MapRoute(
                name: "DashIndex",
                url: "Dash",
                defaults: new { controller = "Dash", action = "Index" }
            );

            routes.MapRoute(
                name: "Address",
                url: "{coin}/Address/{input}",
                defaults: new { controller = "Address", action = "Index", coin = UrlParameter.Optional, input = UrlParameter.Optional }
            );


            routes.MapRoute(
                name: "Transaction",
                url: "{coin}/Tx/{input}",
                defaults: new { controller = "Tx", action = "Index", coin = UrlParameter.Optional, input = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
