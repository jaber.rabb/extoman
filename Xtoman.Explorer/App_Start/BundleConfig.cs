﻿using System.Web.Optimization;

namespace Xtoman.Explorer
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Scripts
            bundles.Add(new ScriptBundle("~/js/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/js/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/js/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/js/main").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.js",
                      "~/Plugins/select2/js/select2.full.min.js",
                      "~/Scripts/script.js"));

            #endregion

            #region Styles
            bundles.Add(new StyleBundle("~/css/main").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/fontawesome.css",
                      "~/Content/icomoon.css",
                      "~/Plugins/select2/css/select2.min.css",
                      "~/Content/site.css"));
            #endregion
        }
    }
}
