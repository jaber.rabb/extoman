﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.Filters;
using Xtoman.Framework.DependencyResolution;
using Xtoman.Framework.WebApi.Filters;
using Xtoman.Framework.WebApi.Services;

namespace Xtoman.Fin
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Filters.Add(new ErrorExceptionFilterAttribute());
            config.Filters.Add(new ValidationFilterAttribute());

            // Web API configuration and services
            config.Services.Replace(typeof(IHttpControllerActivator), new StructureMapHttpControllerActivator(IoC.Container));
            config.Services.Replace(typeof(IFilterProvider), new IoCFilterProvider());
            config.Services.Replace(typeof(IHttpControllerTypeResolver), new BaseApiControllerTypeResolver());
            //config.Services.Replace(typeof(IHttpControllerSelector), new NamespaceControllerSelector(config));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
