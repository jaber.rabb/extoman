﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Xtoman.Domain.WebServices.Payment.Finnotech;
using Xtoman.Fin.ViewModels;
using Xtoman.Framework.WebApi;
using Xtoman.Service;
using Xtoman.Service.WebServices;

namespace Xtoman.Fin.Controllers
{
    public class CardController : BaseApiController
    {
        private readonly IFinnotechService _finnotechService;
        private readonly IFinnotechResponseService _finnotechResponseService;
        private readonly IUserApiKeyService _userApiKeyService;
        public CardController(
            IFinnotechService finnotechService,
            IUserApiKeyService userApiKeyService,
            IFinnotechResponseService finnotechResponseService)
        {
            _finnotechService = finnotechService;
            _userApiKeyService = userApiKeyService;
            _finnotechResponseService = finnotechResponseService;
        }

        // POST api/card
        [HMACAuthentication]
        public async Task<IHttpActionResult> Post(CardRequest request)
        {
            try
            {
                //var userId = User.Identity.GetUserId<int>();
                var apiKey = Request.Headers.Authorization.Parameter.Split(':')[0];
                var userId = await _userApiKeyService.TableNoTracking.Where(p => p.Key == apiKey).Select(p => p.UserId).FirstOrDefaultAsync();
                var finnotech = await _finnotechService.GetCardInformationAsync(request.CardNumber);

                var trackId = finnotech.TrackId.ToString();
                var finnotechResponse = await _finnotechResponseService.Table.Where(p => p.TrackId == trackId).FirstOrDefaultAsync();
                finnotechResponse.InsertUserId = userId;
                await _finnotechResponseService.UpdateAsync(finnotechResponse);

                if (finnotech.Code == 0)
                {
                    return Ok(new ApiResult<FinnotechCardInformation>
                    {
                        Data = finnotech,
                        Status = ApiStatusCode.Success,
                    });
                }
                else
                {
                    return Ok(new ApiResult<FinnotechCardInformation>()
                    {
                        Data = null,
                        Status = ApiStatusCode.BadRequest,
                        Error = finnotech.FarsiMessage
                    });
                }
            }
            catch (Exception ex)
            {
                return Ok(new ApiResult<FinnotechCardInformation>()
                {
                    Error = ex.Message,
                    Data = null,
                    Status = ApiStatusCode.InternalServerError
                });
            }
        }
    }
}