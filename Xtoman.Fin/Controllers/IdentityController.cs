﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Xtoman.Domain.WebServices.Payment.Finnotech;
using Xtoman.Fin.ViewModels;
using Xtoman.Framework.WebApi;
using Xtoman.Service;
using Xtoman.Service.WebServices;

namespace Xtoman.Fin.Controllers
{
    public class IdentityController : BaseApiController
    {
        private readonly IFinnotechService _finnotechService;
        private readonly IUserApiKeyService _userApiKeyService;
        private readonly IFinnotechResponseService _finnotechResponseService;

        public IdentityController(
            IFinnotechService finnotechService,
            IUserApiKeyService userApiKeyService,
            IFinnotechResponseService finnotechResponseService)
        {
            _finnotechService = finnotechService;
            _userApiKeyService = userApiKeyService;
            _finnotechResponseService = finnotechResponseService;
        }

        // POST api/identity

        [HMACAuthentication]
        public async Task<IHttpActionResult> Post(IdentityRequest request)
        {
            try
            {
                //var userId = User.Identity.GetUserId<int>();
                var apiKey = Request.Headers.Authorization.Parameter.Split(':')[0];
                var userId = await _userApiKeyService.TableNoTracking.Where(p => p.Key == apiKey).Select(p => p.UserId).FirstOrDefaultAsync();
                var finnotech = await _finnotechService.GetIdVerificationAsync(request.NationalId, request.BirthDate, request.FirstName + " " + request.LastName, request.FirstName, request.LastName, request.FatherName, userId);

                var trackId = finnotech.TrackId.ToString();
                var finnotechResponse = await _finnotechResponseService.Table.Where(p => p.TrackId == trackId).FirstOrDefaultAsync();
                finnotechResponse.InsertUserId = userId;
                await _finnotechResponseService.UpdateAsync(finnotechResponse);
                if (finnotech.Code == 0)
                {
                    return Ok(new ApiResult<FinnotechIdVerification>
                    {
                        Data = finnotech,
                        Status = ApiStatusCode.Success,
                    });
                }
                else
                {
                    return Ok(new ApiResult<FinnotechIdVerification>()
                    {
                        Data = null,
                        Status = ApiStatusCode.BadRequest,
                        Error = finnotech.FarsiMessage
                    });
                }
            }
            catch (Exception ex)
            {
                return Ok(new ApiResult<FinnotechIdVerification>()
                {
                    Error = ex.Message,
                    Data = null,
                    Status = ApiStatusCode.InternalServerError
                });
            }
        }
    }
}
