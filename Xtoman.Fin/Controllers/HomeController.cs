﻿using System.Web.Mvc;

namespace Xtoman.Fin.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "وب سرویس احراز هویت";
            return View();
        }
    }
}
