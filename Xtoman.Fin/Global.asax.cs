﻿using StructureMap.Web.Pipeline;
using System;
using System.Data.Entity.Infrastructure.Interception;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Xtoman.Data;
using Xtoman.Framework.DependencyResolution;
using Xtoman.Utility;

namespace Xtoman.Fin
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            MvcHandler.DisableMvcResponseHeader = true;

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ModelBinderConfig.Register(ModelBinders.Binders);
            //AttributeAdapterConfig.Register();
            //AutoMapperConfiguration.Configure();
            //WebMarkupMinConfig.Configure(WebMarkupMinConfiguration.Instance);

            DbInterception.Add(new ElmahEfInterceptor());
            SetDbInitializer();
            //Set current Controller factory as StructureMapControllerFactory
            ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory());
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            HttpContextLifecycle.DisposeAndClearAll();
        }

        public class StructureMapControllerFactory : DefaultControllerFactory
        {
            protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
            {
                if (controllerType == null)
                    throw new InvalidOperationException($"Page not found: {requestContext.HttpContext.Request.RawUrl}");

                return IoC.Container.GetInstance(controllerType) as Controller;
            }
        }

        private static void SetDbInitializer()
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
            IoC.Container.GetInstance<IUnitOfWork>().ForceDatabaseInitialize();
        }
    }
}
