﻿namespace Xtoman.Fin.ViewModels
{
    public class IdentityRequest
    {
        public string NationalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FatherName { get; set; }
        public string BirthDate { get; set; }
    }
}