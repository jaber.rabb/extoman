﻿namespace Xtoman.Wallet.Domain.Models
{
    public class UserWallet
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string EncryptedPrivateKey { get; set; }
    }
}
