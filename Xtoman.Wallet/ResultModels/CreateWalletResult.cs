﻿namespace Xtoman.Wallet.ResultModels
{
    public class CreateWalletResult
    {
        public string MasterKey { get; set; }
        public string Mnemonic { get; set; }
        public string PrivateKey { get; internal set; }
    }
}
