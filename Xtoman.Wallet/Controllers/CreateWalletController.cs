﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NBitcoin;
using Xtoman.Wallet.ResultModels;

namespace Xtoman.Wallet.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CreateWalletController : ControllerBase
    {
        private readonly ILogger<CreateWalletController> _logger;
        public CreateWalletController(ILogger<CreateWalletController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public CreateWalletResult Post(string passPhrase, string mnemonic = "")
        {
            if (!string.IsNullOrWhiteSpace(passPhrase))
            {
                var mnemo = string.IsNullOrWhiteSpace(mnemonic) ? new Mnemonic(Wordlist.English, WordCount.Twelve) : new Mnemonic(mnemonic); // Or Mnemonic("dasd dsadlas fdlksjfds fjds . ...")
                var hdRoot = mnemo.DeriveExtKey(passPhrase);
                return new CreateWalletResult()
                {
                    Mnemonic = mnemo.ToString(),
                    MasterKey = hdRoot.ToString(Network.Main),
                    PrivateKey = hdRoot.PrivateKey.ToString(Network.Main)
                };
            }
            return new CreateWalletResult();
        }
    }
}