﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Xtoman.Admin.ViewModels
{
    public class BinanceBalanceViewModel
    {
        [Display(Name = "موجودی آزاد معادل بیت کوین")]
        public decimal TotalFreeBTCValue => Assets.Select(p => p.FreeBTCValue).DefaultIfEmpty(0).Sum();
        [Display(Name = "موجودی آزاد معادل دلار")]
        public decimal TotalFreeUSDValue => Assets.Select(p => p.FreeUSDValue).DefaultIfEmpty(0).Sum();
        [Display(Name = "موجودی قفل شده معادل بیت کوین")]
        public decimal TotalLockedBTCValue => Assets.Select(p => p.LockedBTCValue).DefaultIfEmpty(0).Sum();
        [Display(Name = "موجودی قفل شده معادل دلار")]
        public decimal TotalLockedUSDValue => Assets.Select(p => p.LockedUSDValue).DefaultIfEmpty(0).Sum();
        [Display(Name = "موجودی کل معادل بیت کوین")]
        public decimal TotalBTCValue => TotalFreeBTCValue + TotalLockedBTCValue;
        [Display(Name = "موجودی کل معادل دلار")]
        public decimal TotalUSDValue => TotalFreeUSDValue + TotalLockedUSDValue;
        [Display(Name = "موجودی کل معادل تومان")]
        public decimal TotalTomanValue { get; set; }
        public List<BinanceBalanceItemViewModel> Assets { get; set; }
    }

    public class BinanceBalanceItemViewModel
    {
        [Display(Name = "نام ارز")]
        public string Name { get; set; }
        [Display(Name = "مقدار آزاد")]
        public decimal Free { get; set; }
        [Display(Name = "مقدار قفل شده")]
        public decimal Locked { get; set; }
        [Display(Name = "مقدار آزاد معادل بیت کوین")]
        public decimal FreeBTCValue { get; set; }
        [Display(Name = "مقدار آزاد معادل دلار")]
        public decimal FreeUSDValue { get; set; }
        [Display(Name = "مقدار قفل معادل بیت کوین")]
        public decimal LockedBTCValue { get; set; }
        [Display(Name = "مقدار قفل معادل دلار")]
        public decimal LockedUSDValue { get; set; }
        public decimal UnitPriceInUSD { get; set; }
        public decimal UnitPriceInToman { get; set; }
        public decimal UnitPriceInBTC { get; set; }
    }
}