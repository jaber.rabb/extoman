﻿using Xtoman.Domain.WebServices.TradingPlatform.Binance;

namespace Xtoman.Admin.ViewModels
{
    public class BinanceDepositHistoryViewModel
    {
        public BinanceDepositHistory DepositHistory { get; set; }
    }
}