﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.WebServices.TradingPlatform.Binance;

namespace Xtoman.Admin.ViewModels
{
    public class AddBinanceOrderViewModel
    {
        [Display(Name = "جفت ارز")]
        public string Symbol { get; set; }
        [Display(Name = "قیمت")]
        public decimal Price { get; set; }
        [Display(Name = "مقدار")]
        public decimal Quanity { get; set; }
        [Display(Name = "جهت")]
        public BinanceOrderSide Side { get; set; }
        [Display(Name = "نوع سفارش")]
        public BinanceOrderType OrderType { get; set; }
    }

    public class BinanceOrdersViewModel
    {
        public List<BinanceSymbolPrice> Symbols { get; set; }
        public List<BinanceSingleOrderViewModel> Orders { get; set; }
    }

    public class BinanceSingleOrderViewModel
    {
        public string Symbol { get; set; }
        public int OrderId { get; set; }
        public string ClientOrderId { get; set; }
        public long? ExchangeOrderId { get; set; }
        public decimal Price { get; set; }
        public decimal OrigQty { get; set; }
        public decimal ExecutedQty { get; set; }
        public string Status { get; set; }
        public string TimeInForce { get; set; }
        public string Type { get; set; }
        public string Side { get; set; }
        public decimal StopPrice { get; set; }
        public decimal IcebergQty { get; set; }
        public long Time { get; set; }
    }
}