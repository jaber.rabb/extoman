﻿using Xtoman.Domain.WebServices.TradingPlatform.Binance;

namespace Xtoman.Admin.ViewModels
{
    public class BinanceWithdrawHistoryViewModel
    {
        public BinanceWithdrawHistory WithdrawHistory { get; set; }
    }
}