﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class AddEditExchangeTypeViewModel : BaseViewModel<AddEditExchangeTypeViewModel, ExchangeType>
    {
        [Display(Name = "پرداخت با")]
        public int FromCurrencyId { get; set; }
        [Display(Name = "دریافت به")]
        public int ToCurrencyId { get; set; }
        [Display(Name = "غیر فعال؟")]
        public bool Disabled { get; set; }
        [AllowHtml]
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        public SelectList CurrencyTypes { get; set; }
    }
}