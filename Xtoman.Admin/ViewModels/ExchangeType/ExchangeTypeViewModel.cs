﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;

namespace Xtoman.Admin.ViewModels
{
    public class ExchangeTypeViewModel : BaseViewModel<ExchangeTypeViewModel, ExchangeType>
    {
        public int FromCurrencyId { get; set; }
        public int ToCurrencyId { get; set; }
        [Display(Name = "فعال")]
        public bool Disabled { get; set; }
        [Display(Name = "تبدیل از")]
        public string FromCurrencyName { get; set; }
        [Display(Name = "تبدیل به")]
        public string ToCurrencyName { get; set; }
        [Display(Name = "حداقل قابل دریافت")]
        public string ToCurrencyMinimumReceiveAmount { get; set; }

        public ECurrencyType FromCurrencyType { get; set; }
        public ECurrencyType ToCurrencyType { get; set; }

        [Display(Name = "سود ایکس تومن به درصد")]
        [DisplayFormat(DataFormatString = "{0:0.##}")]
        [MinValue(-20, ErrorMessage = "حداقل درصد -20 است.")]
        [MaxValue(20, ErrorMessage = "حداکثر درصد 20 است.")]
        public decimal ExtraFeePercent { get; set; }
        [Display(Name = "نوع تبدیل")]
        public ExchangeFiatCoinType ExchangeFiatCoinType { get; set; }
        public int UserExchangesCount { get; set; }
    }
}