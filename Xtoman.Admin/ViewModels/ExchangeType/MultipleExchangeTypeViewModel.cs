﻿using System.Collections.Generic;

namespace Xtoman.Admin.ViewModels
{
    public class MultipleRateViewModel
    {
        public decimal Rate { get; set; }
        public List<int> ExchangeTypeIds { get; set; }
        public List<ExchangeTypeViewModel> ExchangeTypes { get; set; }
    }
}