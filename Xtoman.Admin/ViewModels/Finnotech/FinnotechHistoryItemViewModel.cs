﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class FinnotechHistoryItemViewModel : BaseViewModel<FinnotechHistoryItemViewModel, Finnotech>
    {
        [Display(Name = "ردیف")]
        public new long Id { get; set; }
        [Display(Name = "کد پاسخ فینوتک")]
        public string Code { get; set; }
        [Display(Name = "کد پاسخ شبکه")]
        public int StatusCode { get; set; }
        public bool RequestSucceed => StatusCode == 200;
        [Display(Name = "پیام")]
        public string Message { get; set; }
        [Display(Name = "پیام فارسی")]
        public string FarsiMessage { get; set; }
        [Display(Name = "کد پیگیری")]
        public string TrackId { get; set; }
        [Display(Name = "نوع درخواست")]
        public FinnotechResponseType FinType { get; set; }
        public UserSimpleViewModel InsertUser { get; set; }
        [Display(Name = "تاریخ درخواست")]
        public DateTime InsertDate { get; set; }
    }
}