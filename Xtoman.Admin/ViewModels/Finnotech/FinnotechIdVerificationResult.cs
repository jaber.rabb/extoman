﻿namespace Xtoman.Admin.ViewModels
{
    public class FinnotechIdVerificationResult
    {
        public string Code { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public long HistoryId { get; set; }
        public string NationalId { get; set; }
        public string BirthDate { get; set; }
        public string Status { get; set; }
        public string FullName { get; set; }
        public int? FullNameSimilarity { get; set; }
        public string FirstName { get; set; }
        public int? FirstNameSimilarity { get; set; }
        public string LastName { get; set; }
        public int? LastNameSimilarity { get; set; }
        public string FatherName { get; set; }
        public int? FatherNameSimilarity { get; set; }
        public string DeathStatus { get; set; }
        public string TrackId { get; set; }
    }
}