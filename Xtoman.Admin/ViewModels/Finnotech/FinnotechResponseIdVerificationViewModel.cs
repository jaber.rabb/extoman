﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class FinnotechResponseIdVerificationViewModel : BaseViewModel<FinnotechResponseIdVerificationViewModel, FinnotechResponseIdVerification>
    {
        #region Base
        [Display(Name = "ردیف")]
        public new long Id { get; set; }
        [Display(Name = "تاریخ درخواست")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "آی پی درخواست کننده")]
        public string InsertUserIpAddress { get; set; }
        [Display(Name = "کاربر ارسال کننده")]
        public UserSimpleViewModel InsertUser { get; set; }
        [Display(Name = "کد درخواست")]
        public string Code { get; set; }
        [Display(Name = "کد پاسخ")]
        public int StatusCode { get; set; }
        [Display(Name = "پیام")]
        public string Message { get; set; }
        [Display(Name = "پیام فارسی")]
        public string FarsiMessage { get; set; }
        [Display(Name = "کد پیگیری")]
        public string TrackId { get; set; }
        [Display(Name = "نوع درخواست")]
        public FinnotechResponseType FinType { get; set; }
        #endregion

        [Display(Name = "کد ملی")]
        public string NationalId { get; set; }
        [Display(Name = "تاریخ تولد")]
        public string BirthDate { get; set; }
        [Display(Name = "وضعیت")]
        public string Status { get; set; }
        [Display(Name = "نام کامل")]
        public string FullName { get; set; }
        [Display(Name = "درصد تشابه نام کامل")]
        public int? FullNameSimilarity { get; set; }
        [Display(Name = "نام")]
        public string FirstName { get; set; }
        [Display(Name = "درصد تشابه نام")]
        public int? FirstNameSimilarity { get; set; }
        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }
        [Display(Name = "درصد تشابه نام خانوادگی")]
        public int? LastNameSimilarity { get; set; }
        [Display(Name = "نام پدر")]
        public string FatherName { get; set; }
        [Display(Name = "درصد تشابه نام پدر")]
        public int? FatherNameSimilarity { get; set; }
        [Display(Name = "وضعیت حیات")]
        public string DeathStatus { get; set; }
    }
}