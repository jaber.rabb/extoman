﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class FinnotechIdVerificationViewModel
    {
        [Display(Name = "آی دی کاربر")]
        public int? UserId { get; set; }
        [Required]
        [MinLength(10, ErrorMessage = "کد ملی باید 10 رقم باشد")]
        [Display(Name = "کد ملی")]
        [NationalCode]
        public string NationalCode { get; set; }
        [Required]
        [Display(Name = "نام")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }
        [Display(Name = "نام کامل")]
        public string FullName => FirstName + " " + LastName;
        [Display(Name = "نام پدر")]
        public string FatherName { get; set; }
        [Required]
        [Display(Name = "تاریخ تولد yyyy/mm/dd")]
        public string BirthDate { get; set; }
        [Display(Name = "درخواست جدید")]
        public byte New { get; set; }
        public byte Json { get; set; }
        public MediaViewModel VerificationMedia { get; set; }
    }
}