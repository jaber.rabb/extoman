﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.WebServices.Payment.Finnotech;

namespace Xtoman.Admin.ViewModels
{
    public class FinnotechCardInfoViewModel
    {
        [Display(Name = "کارت مقصد")]
        public string DestCard { get; set; }
        [Display(Name = "نام صاحب کارت")]
        public string Name { get; set; }
        [Display(Name = "نتیجه")]
        public string Result { get; set; }
        [Display(Name = "متن نتیجه")]
        public string ResultText => Result;
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        [Display(Name = "زمان انجام عملیات")]
        public string DoTime { get; set; }
        [Display(Name = "کد پیگیری")]
        public Guid TrackId { get; set; }
    }
}