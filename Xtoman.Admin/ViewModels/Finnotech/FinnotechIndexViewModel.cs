﻿namespace Xtoman.Admin.ViewModels
{
    public class FinnotechIndexViewModel
    {
        public int TotalRequests { get; set; }
        public int TotalSuccessRequests { get; set; }
        public int TotalFailedRequests => TotalRequests - TotalSuccessRequests;
    }
}