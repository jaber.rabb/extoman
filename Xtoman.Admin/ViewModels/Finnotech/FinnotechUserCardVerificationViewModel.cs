﻿using System;
using Xtoman.Domain.WebServices.Payment.Finnotech;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class FinnotechUserCardVerificationViewModel : ResultModel
    {
        public int HistoryId { get; set; }
        public string DestCard { get; set; }
        public string Name { get; set; }
        public string Result { get; set; }
        public string ResultText => Result;
        public string Description { get; set; }
        public string DoTime { get; set; }
        public Guid TrackId { get; set; }
    }
}