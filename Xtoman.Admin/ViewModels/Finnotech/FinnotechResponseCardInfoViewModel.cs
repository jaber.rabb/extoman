﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class FinnotechResponseCardInfoViewModel : BaseViewModel<FinnotechResponseCardInfoViewModel, FinnotechResponseCardInfo>
    {
        #region Base
        [Display(Name = "ردیف")]
        public new long Id { get; set; }
        [Display(Name = "تاریخ درخواست")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "آی پی درخواست کننده")]
        public string InsertUserIpAddress { get; set; }
        [Display(Name = "کاربر ارسال کننده")]
        public UserSimpleViewModel InsertUser { get; set; }
        [Display(Name = "کد درخواست")]
        public string Code { get; set; }
        [Display(Name = "کد پاسخ")]
        public int StatusCode { get; set; }
        [Display(Name = "پیام")]
        public string Message { get; set; }
        [Display(Name = "کد پیگیری")]
        public string TrackId { get; set; }
        [Display(Name = "نوع درخواست")]
        public FinnotechResponseType FinType { get; set; }
        #endregion

        [Display(Name = "شماره کارت")]
        public string DestCard { get; set; }
        [Display(Name = "نام صاحب کارت")]
        public string Name { get; set; }
        [Display(Name = "نتیجه")]
        public string Result { get; set; }
        [Display(Name = "توضیحات تکمیلی")]
        public string Description { get; set; }
        [Display(Name = "زمان انجام")]
        public string DoTime { get; set; }
        [Display(Name = "کارت بانکی کاربر")]
        public UserBankCardViewModel UserBankCard { get; set; }
    }
}