﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Payment.Finnotech;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class FinnotechResponseTransferToViewModel : BaseViewModel<FinnotechResponseTransferToViewModel, FinnotechResponseTransferTo>
    {
        #region Base
        [Display(Name = "ردیف")]
        public new long Id { get; set; }
        [Display(Name = "تاریخ درخواست")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "آی پی درخواست کننده")]
        public string InsertUserIpAddress { get; set; }
        [Display(Name = "کاربر ارسال کننده")]
        public UserSimpleViewModel InsertUser { get; set; }
        [Display(Name = "کد درخواست")]
        public int Code { get; set; }
        [Display(Name = "کد پاسخ")]
        public int StatusCode { get; set; }
        [Display(Name = "پیام")]
        public string Message { get; set; }
        [Display(Name = "پیام فارسی")]
        public string FarsiMessage { get; set; }
        [Display(Name = "کد پیگیری")]
        public string TrackId { get; set; }
        [Display(Name = "نوع درخواست")]
        public FinnotechResponseType FinType { get; set; }
        #endregion

        [Display(Name = "مبلغ")]
        public long Amount { get; set; }
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        [Display(Name = "نام")]
        public string DestinationFirstname { get; set; }
        [Display(Name = "نام خانوادگی")]
        public string DestinationLastname { get; set; }
        [Display(Name = "شماره مقصد")]
        public string DestinationNumber { get; set; }
        [Display(Name = "")]
        public string InquiryDate { get; set; }
        [Display(Name = "")]
        public long InquirySequence { get; set; }
        [Display(Name = "")]
        public string InquiryTime { get; set; }
        [Display(Name = "شماره پرداخت")]
        public long PaymentNumber { get; set; }
        [Display(Name = "")]
        public string RefCode { get; set; }
        [Display(Name = "نام مبدا")]
        public string SourceFirstname { get; set; }
        [Display(Name = "نام خانوادگی مبدا")]
        public string SourceLastname { get; set; }
        [Display(Name = "شماره مبدا")]
        public string SourceNumber { get; set; }
        [Display(Name = "وضعیت")]
        public string State { get; set; }
        [Display(Name = "نوع")]
        public FinnotechTransferType? TypeEnum { get; set; }
    }
}