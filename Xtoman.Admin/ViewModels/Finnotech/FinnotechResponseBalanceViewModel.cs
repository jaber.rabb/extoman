﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Payment.Finnotech;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class FinnotechResponseBalanceViewModel : BaseViewModel<FinnotechResponseBalanceViewModel, FinnotechResponseBalance>
    {
        #region Base
        [Display(Name = "ردیف")]
        public new long Id { get; set; }
        [Display(Name = "تاریخ درخواست")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "آی پی درخواست کننده")]
        public string InsertUserIpAddress { get; set; }
        [Display(Name = "کاربر ارسال کننده")]
        public UserSimpleViewModel InsertUser { get; set; }
        [Display(Name = "کد درخواست")]
        public int Code { get; set; }
        [Display(Name = "کد پاسخ")]
        public int StatusCode { get; set; }
        [Display(Name = "پیام")]
        public string Message { get; set; }
        [Display(Name = "پیام فارسی")]
        public string FarsiMessage { get; set; }
        [Display(Name = "کد پیگیری")]
        public string TrackId { get; set; }
        [Display(Name = "نوع درخواست")]
        public FinnotechResponseType FinType { get; set; }
        #endregion

        [Display(Name = "مانده واقعی حساب")]
        public double CurrentBalance { get; set; }
        [Display(Name = "مانده قابل برداشت")]
        public double AvailableBalance { get; set; }
        [Display(Name = "مانده")]
        public double EffectiveBalance { get; set; }
        [Display(Name = "وضعیت فراخوانی سرویس")]
        public FinnotechState State { get; set; }
        [Display(Name = "شماره حساب")]
        public string Number { get; set; }
    }
}