﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class CategorySelectListViewModel : BaseViewModel<CategorySelectListViewModel, Category>
    {
        [Display(Name = "ردیف")]
        public new int? Id { get; set; }
        [Display(Name = "نام")]
        public string Name { get; set; }
        [Display(Name = "دسته بندی مادر")]
        public string ParentCategory { get; set; }
    }
}