﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class AddEditCategoryViewModel : BaseViewModel<AddEditCategoryViewModel, Category>, ILocalizedModel<CategoryLocalizedViewModel>, IUrlHistoryModel<CategoryLocalizedViewModel>
    {
        [Display(Name = "موقعیت")]
        public int Order { get; set; }
        [Display(Name = "نام دسته بندی")]
        public string Name { get; set; }
        [AllowHtml]
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        [Display(Name = "توضیحات متا")]
        public string MetaDescription { get; set; }
        [Display(Name = "کلمات کلیدی متا")]
        public string MetaKeywords { get; set; }
        [Display(Name = "آدرس اصلی")]
        public string OriginalUrl { get; set; }
        [Display(Name = "آدرس صفحه")]
        public string Url { get; set; }
        [Display(Name = "دسته بندی مادر")]
        public int? ParentCategoryId { get; set; }
        [Display(Name = "رنگ")]
        public string ColorHex { get; set; }

        [Display(Name = "نام رای دهی")]
        public List<string> RateName {
            get {
                if (_rateName == null || _rateName.Count == 0) { 
                    _rateName = Rates != null ? Rates.Select(p => p.Name).ToList() : new List<string>();
                }
                return _rateName;
            }
            set {
                _rateName = value;
            }
        }

        private List<string> _rateName;
        public Language Language { get; set; }

        public ICollection<ProductCategoryRateViewModel> Rates { get; set; }

        private IList<CategoryLocalizedViewModel> _locales;
        public IList<CategoryLocalizedViewModel> Locales
        {
            get { return _locales ?? (_locales = new List<CategoryLocalizedViewModel>()); }
            set { _locales = value; }
        }
    }

    public class ProductCategoryRateViewModel : BaseViewModel<ProductCategoryRateViewModel, CategoryRate>
    {
        [Display(Name = "نام رای دهی")]
        public string Name { get; set; }
    }

    public class CategoryLocalizedViewModel : ILocalizedModelLocal, ILocalizedUrlHistoryModel
    {
        [Display(Name = "نام")]
        public string Name { get; set; }
        [AllowHtml]
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        [Display(Name = "توضیحات متا")]
        public string MetaDescription { get; set; }
        [Display(Name = "کلمات کلیدی متا")]
        public string MetaKeywords { get; set; }
        [Display(Name = "Url نام آشنا")]
        public string Url { get; set; }
        [Display(Name = "آدرس اصلی")]
        public string OriginalUrl { get; set; }
        [Display(Name = "زبان")]
        public int LanguageId { get; set; }
    }
}