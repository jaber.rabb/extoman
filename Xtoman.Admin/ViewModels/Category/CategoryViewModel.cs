﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Admin.ViewModels
{
    public class CategoryViewModel : CategorySelectListViewModel
    {
        [Display(Name = "Url نام آشنا")]
        public string Url { get; set; }
        [Display(Name = "ترتیب")]
        public int Order { get; set; }
    }
}