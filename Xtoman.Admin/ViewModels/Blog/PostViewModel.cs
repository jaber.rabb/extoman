﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class PostViewModel : BaseViewModel<PostViewModel, Post>, ILocalizedModel<PostLocalizedViewModel>, IUrlHistoryModel<PostLocalizedViewModel>
    {
        public PostViewModel()
        {
            TagsList = new Dictionary<int, string>();
            Locales = new List<PostLocalizedViewModel>();
            Categories = new List<PostCategory>();
            ImageList = new List<int>();
        }
        [Display(Name = "عنوان")]
        [Required]
        public string Title { get; set; } //

        [Display(Name = "Url نام آشنا")]
        [Required]
        public string Url { get; set; }
        public string OriginalUrl { get; set; }

        [Display(Name = "خلاصه")]
        public string Summary { get; set; } //

        [Display(Name = "متن")]
        [AllowHtml]
        public string Text { get; set; } //

        [Display(Name = "نوع نوشته")]
        public PostType PostType { get; set; } //

        [Display(Name = "متا توضیحات")]
        public string MetaDescription { get; set; }

        [Display(Name = "متا کلمات کلیدی")]
        public string MetaKeywords { get; set; }

        [Display(Name = "کلمه کلیدی اصلی")]
        public string FocusKeyword { get; set; }

        [Display(Name = "تصویر")]
        public int? MediaId { get; set; }

        [Display(Name = "تعداد بازدید")]
        public int? VisitCount { get; set; }

        [Display(Name = "تاریخ انتشار")]
        public DateTime? PublishDate { get; set; }

        [Display(Name = "نمایش در صفحه اصلی")]
        public bool IsPin { get; set; } //

        [Display(Name = "امکان نظر دهی")]
        public bool AllowComments { get; set; }

        [Display(Name = "انتشار داده شود؟")]
        public bool Enabled { get; set; }

        [Display(Name = "دسته بندی ها")]
        [Required(ErrorMessage = "انتخاب حداقل یک دسته بندی ضروری است.")]
        public int[] CategoriesId { get; set; }

        [Display(Name = "دسته بندی ها")]
        public string ListCategory { get; set; }

        [Display(Name = "برجسب ها")]
        public string TagNames { get; set; }
        [Display(Name = "برچسب ها")]
        public Dictionary<int, string> TagsList { get; set; }
        public Language Language { get; set; }
        public IList<PostLocalizedViewModel> Locales { get; set; }
        public ICollection<PostCategory> Categories { get; set; }
        public List<int> ImageList { get; set; }
        [Display(Name = "تصویر")]
        public MediaViewModel Media { get; set; }
        [Display(Name = "تصویر")]
        public string ImagePath { get; set; }
    }

    public class PostLocalizedViewModel : ILocalizedModelLocal, ILocalizedUrlHistoryModel
    {
        [Display(Name = "عنوان")]
        public string Title { get; set; }

        [Display(Name = "آدرس صفحه")]
        public string Url { get; set; }

        public string OriginalUrl { get; set; }

        [Display(Name = "خلاصه متن")]
        public string Summary { get; set; }

        [Display(Name = "متن")]
        [AllowHtml]
        public string Text { get; set; }

        [Display(Name = "تگ توضیحات")]
        public string MetaDescription { get; set; }

        [Display(Name = "تگ کلمات کلیدی")]
        public string MetaKeywords { get; set; }

        [Display(Name = "کلمه کلیدی پست")]
        public string FocusKeyword { get; set; }

        public int LanguageId { get; set; }
    }
}