﻿using Xtoman.Domain.WebServices.TradingPlatform.Kraken;

namespace Xtoman.Admin.ViewModels
{
    public class KrakenClosedOrdersViewModel
    {
        public KrakenClosedOrdersResult ClosedOrders { get; set; }
    }
}