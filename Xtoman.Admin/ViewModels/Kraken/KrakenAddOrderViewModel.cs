﻿using System.Collections.Generic;
using Xtoman.Domain.WebServices.TradingPlatform.Kraken;

namespace Xtoman.Admin.ViewModels
{
    public class KrakenAddOrderViewModel
    {
        public string Pair { get; set; }
        public KrakenOrderSide OrderSide { get; set; }
        public KrakenOrderType OrderType { get; set; }
        public decimal Volume { get; set; }
        public decimal? Price { get; set; }
        public decimal? Price2 { get; set; }
        public List<KrakenOrderFlagViewModel> OrderFlags { get; set; }

        //
        public KrakenAssetPairsResult AssetPairs { get; set; }
    }

    public class KrakenOrderFlagViewModel
    {
        public KrakenOrderFlag OrderFlag { get; set; }
        public bool IsSelected { get; set; }
    }
}