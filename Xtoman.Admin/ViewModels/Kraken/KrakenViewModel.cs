﻿using Xtoman.Domain.WebServices.TradingPlatform.Kraken;

namespace Xtoman.Admin.ViewModels
{
    public class KrakenViewModel
    {
        public KrakenActiveAssetsResult Assets { get; set; }
        public KrakenBalanceResult Balance { get; set; }
        public KrakenTradeBalanceResult TradeBalance { get; set; }
    }

    public class KrakenOpenOrdersViewModel
    {
        public KrakenOpenOrdersResult OpenOrders { get; set; }
    }
}