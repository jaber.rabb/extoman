﻿using System;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class TradeItemViewModel : BaseViewModel<TradeItemViewModel, Trade>
    {
        public new long Id { get; set; }
        public string ApiTradeId { get; set; }
        public bool IsManual { get; set; }
        public TradingPlatform TradingPlatform { get; set; }
        public TradeStatus TradeStatus { get; set; }
        public TradeOrderType? TradeOrderType { get; set; }
        public DateTime Date { get; set; }
        public decimal Price { get; set; }

        #region From
        public decimal FromAmount { get; set; }
        public ECurrencyType FromCryptoType { get; set; }
        #endregion

        #region To
        public decimal ToAmount { get; set; }
        public ECurrencyType ToCryptoType { get; set; }
        #endregion

        #region Fee
        public decimal TradingFee { get; set; }
        public ECurrencyType TradingFeeType { get; set; }
        #endregion

        #region Additional Fee || Contractor Fee
        public decimal AdditionalFee { get; set; }
        public ECurrencyType AdditionalFeeType { get; set; }
        #endregion
        public ExchangeOrdersListItemViewModel Order { get; set; }
    }
}