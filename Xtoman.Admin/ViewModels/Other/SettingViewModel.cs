﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xtoman.Domain.Settings;

namespace Xtoman.Admin.ViewModels
{
    public class SettingViewModel
    {
        public BlogSettings BlogSettings { get; set; }
        public CacheSettings CacheSettings { get; set; }
        public LocalizationSettings LocalizationSettings { get; set; }
        public MessageSettings MessageSettings { get; set; }
        public PaymentSettings PaymentSettings { get; set; }
        public CommentSettings CommentSettings { get; set; }

        public Dictionary<int, string> EmailAccounts { get; set; }
        public Dictionary<int, string> Languages { get; set; }
    }
}