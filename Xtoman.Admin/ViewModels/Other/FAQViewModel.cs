﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class FAQViewModel : BaseViewModel<FAQViewModel, FAQ>, ILocalizedModel<FAQLocalizedViewModel>
    {
        [Display(Name = "پرسش")]
        public string Question { get; set; }
        [Display(Name = "پاسخ")]
        public string Answer { get; set; }
        [Display(Name = "نام گروه")]
        public string GroupName { get; set; }
        public Language Language { get; set; }

        private IList<FAQLocalizedViewModel> _locales;
        public IList<FAQLocalizedViewModel> Locales
        {
            get { return _locales ?? (_locales = new List<FAQLocalizedViewModel>()); }
            set { _locales = value; }
        }
    }

    public class FAQLocalizedViewModel : ILocalizedModelLocal
    {
        [Display(Name = "پرسش")]
        public string Question { get; set; }
        [Display(Name = "پاسخ")]
        public string Answer { get; set; }
        [Display(Name = "نام گروه")]
        public string GroupName { get; set; }
        public int LanguageId { get; set; }
    }
}