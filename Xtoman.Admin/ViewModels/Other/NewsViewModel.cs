﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class NewsViewModel : BaseViewModel<NewsViewModel, News>
    {
        [Display(Name = "عنوان")]
        public string Title { get; set; }
        [Display(Name = "خلاصه")]
        public string Summary { get; set; }
        [Display(Name = "فوریتی؟")]
        public bool Emergency { get; set; }
        [Display(Name = "نام دسته بندی")]
        public string CategoryName { get; set; }
        [Display(Name = "تاریخ انتشار")]
        public DateTime? PublishDate { get; set; }
        [Display(Name = "URL نام آشنا")]
        public string Url { get; set; }
        [Display(Name = "فعال؟")]
        public bool Enabled { get; set; }
    }
}