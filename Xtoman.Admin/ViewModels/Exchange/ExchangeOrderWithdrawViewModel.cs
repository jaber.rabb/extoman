﻿using System;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class WithdrawViewModel : BaseViewModel<WithdrawViewModel, Withdraw>
    {
        public new long Id { get; set; }
        public string ApiWithdrawId { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public WithdrawFromType WithdrawType { get; set; }
        public ECurrencyType ECurrencyType { get; set; }
        public TradingPlatform? TradingPlatform { get; set; }
        public WalletApiType? WalletApiType { get; set; }
        public WithdrawStatus Status { get; set; }
        public DateTime InsertDate { get; set; }
        public string TransactionId { get; set; }
    }
}