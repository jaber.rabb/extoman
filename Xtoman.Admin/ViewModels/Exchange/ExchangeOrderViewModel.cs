﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class ExchangeOrderViewModel : BaseViewModel<ExchangeOrdersListItemViewModel, ExchangeOrder>
    {
        [Display(Name = "خطا")]
        public string Errors { get; set; }

        [Display(Name = "ردیف سفارش")]
        public new long Id { get; set; }
        [Display(Name = "شماره سفارش")]
        public Guid Guid { get; set; }
        [Display(Name = "روش پرداخت")]
        public PaymentType PaymentType { get; set; }
        [Display(Name = "وضعیت سفارش")]
        public OrderStatus OrderStatus { get; set; }
        [Display(Name = "وضعیت پرداخت")]
        public PaymentStatus PaymentStatus { get; set; }
        [Display(Name = "تاریخ لغو")]
        public DateTime? CancelDate { get; set; }
        [Display(Name = "تاریخ پرداخت")]
        public DateTime? PaymentDate { get; set; }
        [Display(Name = "توضیحات")]
        public string Description { get; set; }

        [Display(Name = "نرخ دلار در لحظه")]
        public int DollarPriceInToman { get; set; }
        [Display(Name = "درصد سود ایکس تومن")]
        public decimal ExchangeExtraFeePercentInTime { get; set; }
        [Display(Name = "نرخ لحظه ای ارز به دلار")]
        public decimal? CurrencyPriceInDollar { get; set; }
        [Display(Name = "مقدار قابل پرداخت مشتری")]
        public decimal PayAmount { get; set; }
        [Display(Name = "مقدار پرداخت شده توسط مشتری")]
        public decimal PaidAmount { get; set; }
        [Display(Name = "قیمت")]
        public decimal PriceAmount { get; set; }
        [Display(Name = "هزینه تراکنش")]
        public decimal GatewayFee { get; set; }
        [Display(Name = "پرداخت با")]
        public ECurrencyType PaymentECurrencyType { get; set; }

        [Display(Name = "مقدار دریافت")]
        public decimal ReceiveAmount { get; set; }
        [Display(Name = "مقدار دریافت مشتری + هزینه تراکنش")]
        public decimal ReceiveAmountWithFee => ReceiveAmount + GatewayFee;
        [Display(Name = "آدرس دریافت")]
        public string ReceiveAddress { get; set; }
        [Display(Name = "Memo/Tag/PaymentId")]
        public string ReceiveMemoTagPaymentId { get; set; }
        [Display(Name = "کد تراکنش دریافت")]
        public string ReceiveTransactionCode { get; set; }
        [Display(Name = "تاریخ دریافت")]
        public DateTime? ReceiveDate { get; set; }
        [Display(Name = "واریز از حساب ایکس تومن")]
        public string ReceivePayerAccountNo { get; set; }

        [Display(Name = "نام گیرنده سفارش")]
        public string UserFirstName { get; set; }
        [Display(Name = "نام خانوادگی گیرنده سفارش")]
        public string UserLastName { get; set; }
        [Display(Name = "ایمیل گیرنده سفارش")]
        public string UserEmail { get; set; }
        [Display(Name = "تلفن همراه گیرنده سفارش")]
        public string UserPhoneNumber { get; set; }
        [Display(Name = "نام سفارش دهنده")]
        public string InsertUserFirstName { get; set; }
        [Display(Name = "نام خانوادگی سفارش دهنده")]
        public string InsertUserLastName { get; set; }
        [Display(Name = "IP سفارش دهنده")]
        public string InsertUserIpAddress { get; set; }
        [Display(Name = "تاریخ سفارش")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "نام بروز رساننده سفارش")]
        public string UpdateUserFirstName { get; set; }
        [Display(Name = "نام خانوادگی بروز رساننده سفارش")]
        public string UpdateUserLastName { get; set; }
        [Display(Name = "گیرنده سفارش")]
        public string UserFullName => (UserFirstName + " " + UserLastName).Trim();
        [Display(Name = "سفارش دهنده")]
        public string InsertUserFullName => (InsertUserFirstName + " " + InsertUserLastName).Trim();
        [Display(Name = "پیگیری کننده سفارش")]
        public string UpdateUserFullName => (UpdateUserFirstName + " " + UpdateUserLastName).Trim();

        [Display(Name = "تبدیل از")]
        public ECurrencyType ExchangeFromCurrencyType { get; set; }
        [Display(Name = "تبدیل به")]
        public ECurrencyType ExchangeToCurrencyType { get; set; }

        public bool ExchangeToCurrencyIsFiat { get; set; }

        public bool ExchangeFromCurrencyIsFiat { get; set; }

        [Display(Name = "آدرس برای پرداخت مشتری")]
        public string PayAddress { get; set; }
        [Display(Name = "Memo/DestTag/PaymentId برای پرداخت مشتری")]
        public string PayMemoTagPaymentId { get; set; }
        [Display(Name = "کد تراکنش پرداخت مشتری")]
        public string PayTransactionId { get; set; }
        [Display(Name = "کد تراکنش پرداخت مشتری")]
        public string PayPaymentGatewayId { get; set; }
        [Display(Name = "پرداخت به کیف پول")]
        public WalletApiType PayWalletApiType { get; set; }
        [Display(Name = "زمان پایان مهلت پرداخت")]
        public DateTime? PayTimeEnd { get; set; }

        public BankResponseStatusViewModel BankResponse { get; set; }

        public List<WithdrawViewModel> OrderWithdraws { get; set; }
        public List<TradeViewModel> OrderTrades { get; set; }
        public List<OrderNoteViewModel> OrderNotes { get; set; }
        public OrderReviewViewModel Review { get; set; }
    }
}