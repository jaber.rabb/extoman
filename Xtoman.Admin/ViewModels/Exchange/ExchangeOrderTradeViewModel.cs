﻿using System;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class TradeViewModel : BaseViewModel<TradeViewModel, Trade>
    {
        public new long Id { get; set; }
        public string ApiTradeId { get; set; }
        public bool IsManual { get; set; }
        public TradingPlatform TradingPlatform { get; set; }
        public TradeStatus TradeStatus { get; set; }
        public DateTime Date { get; set; }
        #region From
        public decimal FromAmount { get; set; }
        public decimal FromUSDAmount { get; set; }
        public decimal FromTomanAmount { get; set; }
        public ECurrencyType FromCryptoType { get; set; }
        #endregion

        #region To
        public decimal ToAmount { get; set; }
        public decimal ToAmountInUSD { get; set; }
        public decimal ToAmountInToman { get; set; }
        public ECurrencyType ToCryptoType { get; set; }
        #endregion

        #region Fee
        public decimal TradingFee { get; set; }
        public decimal TradingFeeInToman { get; set; }
        public decimal TradingFeeInUSD { get; set; }
        public ECurrencyType TradingFeeType { get; set; }
        #endregion

        #region Additional Fee || Contractor Fee
        public decimal AdditionalFee { get; set; }
        public decimal AdditionalFeeInToman { get; set; }
        public decimal AdditionalFeeInUSD { get; set; }
        public ECurrencyType AdditionalFeeType { get; set; }
        #endregion

        public UserViewModel ContractorUser { get; set; }
    }
}