﻿using System;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class AddExchangeOrderViewModel : BaseViewModel<AddExchangeOrderViewModel, ExchangeOrder>
    {
        public decimal PayAmount { get; set; }
        public decimal PriceAmount { get; set; }
        public decimal GatewayFee { get; set; }
        public ECurrencyType PaymentECurrencyType { get; set; }
        public string Errors { get; set; }

        public int BankResponseId { get; set; }
        public int UserId { get; set; }

        public PaymentType PaymentType { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public DateTime? CancelDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public string Description { get; set; }

        #region Pay
        public decimal PaidAmount { get; set; }
        public string PayAddress { get; set; }
        public string PayMemoTagPaymentId { get; set; }
        public string PayTransactionId { get; set; }
        public string PayPaymentGatewayId { get; set; }
        public WalletApiType PayWalletApiType { get; set; }
        public DateTime? PayTimeEnd { get; set; }
        #endregion

        #region Receive
        public decimal ReceiveAmount { get; set; }
        public string ReceiveAddress { get; set; }
        public string ReceiveMemoTagPaymentId { get; set; }
        public string ReceiveTransactionCode { get; set; }
        public DateTime? ReceiveDate { get; set; }
        public string ReceivePayerAccountNo { get; set; }
        public decimal EstimatedReceiveAmount { get; set; }
        #endregion

        #region Other
        public decimal? CurrencyPriceInDollar { get; set; }
        public int DollarPriceInToman { get; set; }
        public int EuroPriceInToman { get; set; }
        public decimal BTCPriceInUSD { get; set; }
        public decimal BTCPriceInEUR { get; set; }
        public decimal BTCValueOfOrder { get; set; }
        public decimal ExchangeExtraFeePercentInTime { get; set; }
        #endregion

        public int ExchangeId { get; set; }
    }
}