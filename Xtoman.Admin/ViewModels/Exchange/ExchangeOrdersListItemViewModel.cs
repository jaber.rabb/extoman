﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class ExchangeOrdersListItemViewModel : BaseViewModel<ExchangeOrdersListItemViewModel, ExchangeOrder>
    {
        public Guid Guid { get; set; }
        [Display(Name = "شماره سفارش")]
        public new long Id { get; set; }
        [Display(Name = "وضعیت سفارش")]
        public OrderStatus OrderStatus { get; set; }
        [Display(Name = "وضعیت پرداخت")]
        public PaymentStatus PaymentStatus { get; set; }
        [Display(Name = "تاریخ لغو")]
        public DateTime? CancelDate { get; set; }
        [Display(Name = "تاریخ پرداخت")]
        public DateTime? PaymentDate { get; set; }

        [Display(Name = "مقدار پرداخت")]
        public decimal PayAmount { get; set; }
        [Display(Name = "قیمت")]
        public decimal PriceAmount { get; set; }
        [Display(Name = "هزینه تراکنش")]
        public decimal GatewayFee { get; set; }

        [Display(Name = "مقدار دریافت")]
        public decimal ReceiveAmount { get; set; }
        [Display(Name = "حساب دریافت")]
        public string ReceiveECAccountNo { get; set; }
        [Display(Name = "تاریخ دریافت")]
        public DateTime? ReceiveDate { get; set; }

        [Display(Name = "نام گیرنده سفارش")]
        public string UserFirstName { get; set; }
        [Display(Name = "نام خانوادگی گیرنده سفارش")]
        public string UserLastName { get; set; }
        [Display(Name = "ایمیل گیرنده سفارش")]
        public string UserEmail { get; set; }
        [Display(Name = "تلفن همراه گیرنده سفارش")]
        public string UserPhoneNumber { get; set; }
        [Display(Name = "نام سفارش دهنده")]
        public string InsertUserFirstName { get; set; }
        [Display(Name = "نام خانوادگی سفارش دهنده")]
        public string InsertUserLastName { get; set; }
        [Display(Name = "تاریخ سفارش")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "نام بروز رساننده سفارش")]
        public string UpdateUserFirstName { get; set; }
        [Display(Name = "نام خانوادگی بروز رساننده سفارش")]
        public string UpdateUserLastName { get; set; }
        [Display(Name = "گیرنده سفارش")]
        public string UserFullName => (UserFirstName + " " + UserLastName).Trim();
        [Display(Name = "سفارش دهنده")]
        public string InsertUserFullName => (InsertUserFirstName + " " + InsertUserLastName).Trim();
        [Display(Name = "پیگیری کننده سفارش")]
        public string UpdateUserFullName => (UpdateUserFirstName + " " + UpdateUserLastName).Trim();
        public int UserExchangeOrdersCount { get; set; }
        public int UserId { get; set; }

        [Display(Name = "تبدیل از")]
        public ECurrencyType ExchangeFromCurrencyType { get; set; }
        [Display(Name = "تبدیل به")]
        public ECurrencyType ExchangeToCurrencyType { get; set; }
    }
}