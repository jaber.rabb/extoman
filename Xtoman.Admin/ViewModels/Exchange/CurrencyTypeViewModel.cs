﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class CurrencyTypeViewModel : BaseViewModel<CurrencyTypeViewModel, CurrencyType>
    {
        [Display(Name = "نام")]
        public string Name { get; set; }
        [Display(Name = "نام دوم")]
        public string AlternativeName { get; set; }
        [Display(Name = "نشان واحد پولی")]
        public string UnitSign { get; set; }
        [Display(Name = "نشان بعد از مبلغ؟")]
        public bool UnitSignIsAfter { get; set; }
        [Display(Name = "مقدار موجود (اختیاری)")]
        [DisplayFormat(DataFormatString = "{0:0.##########}", ApplyFormatInEditMode = true)]
        public decimal? Available { get; set; }
        [Display(Name = "نوع ارز")]
        public ECurrencyType Type { get; set; }
        [Display(Name = "حداقل قابل خرید")]
        [DisplayFormat(DataFormatString = "{0:0.##########}", ApplyFormatInEditMode = true)]
        public decimal MinimumReceiveAmount { get; set; }
        [Display(Name = "تصویر")]
        public int? MediaId { get; set; }
        [Display(Name = "تصویر")]
        public string ImageSrc { get; set; }
        public MediaViewModel Media { get; set; }
        [Display(Name = "غیر فعال؟")]
        public bool Disabled { get; set; }
    }
}