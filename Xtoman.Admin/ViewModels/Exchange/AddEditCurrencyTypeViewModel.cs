﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class AddEditCurrencyTypeViewModel : BaseViewModel<AddEditCurrencyTypeViewModel, CurrencyType>
    {
        [Display(Name = "نام")]
        public string Name { get; set; }
        [Display(Name = "نام دیگر")]
        public string AlternativeName { get; set; }
        [AllowHtml]
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        [Display(Name = "نشان ارز")]
        public string UnitSign { get; set; }
        [Display(Name = "نمایش نشان ارز بعد از مقدار؟")]
        public bool UnitSignIsAfter { get; set; }
        [Display(Name = "موجود (اختیاری)")]
        public decimal? Available { get; set; }
        [Display(Name = "ارز حقیقی است")]
        public bool IsFiat { get; set; }
        public decimal Step { get; set; }
        [Display(Name = "نوع")]
        public ECurrencyType Type { get; set; }
        [Display(Name = "حداقل قابل دریافت")]
        public decimal MinimumReceiveAmount { get; set; }
        [Display(Name = "غیر فعال؟")]
        public bool Disabled { get; set; }
        [Display(Name = "تصویر")]
        public int? MediaId { get; set; }
        public string MediaUrl { get; set; }
        [Display(Name = "Url نام آشنا")]
        public string Url { get; set; }

        [Display(Name = "داری Memo/Tag/Description")]
        public bool HasMemo { get; set; }
        [Display(Name = "نام Memo/Tag/Description")]
        public string MemoName { get; set; }
        [AllowHtml]
        [Display(Name = "توضیحات Memo/Tag/Description")]
        public string MemoDescription { get; set; }
    }
}