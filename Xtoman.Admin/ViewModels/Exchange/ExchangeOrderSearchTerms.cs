﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;

namespace Xtoman.Admin.ViewModels
{
    public class SearchTerms
    {
        [Display(Name = "عبارت جستجو")]
        public string Text { get; set; }
        [Display(Name = "از ردیف")]
        public int? FromId { get; set; }
        [Display(Name = "تا ردیف")]
        public int? ToId { get; set; }
    }

    public class UserSearchTerms : SearchTerms
    {
        [Display(Name = "وضعیت احراز هویت")]
        public VerificationModelStatus? VerifyStatus { get; set; }

        [Display(Name = "جنسیت")]
        public int? Gender { get; set; }

        [Display(Name = "فیشر")]
        public int? IsPhisher { get; set; }
    }

    public class ExchangeOrderSearchTerms : SearchTerms
    {
        [Display(Name = "تاریخ سفارش از")]
        public string FromOrderDate { get; set; }
        [Display(Name = "تاریخ سفارش تا")]
        public string ToOrderDate { get; set; }

        [Display(Name = "تبدیل از")]
        public int? ExchangeTypeFromCurrencyTypeId { get; set; }
        [Display(Name = "تبدیل به")]
        public int? ExchangeTypeToCurrencyTypeId { get; set; }
        [Display(Name = "وضعیت سفارش")]
        public OrderStatus? OrderStatus { get; set; }
        [Display(Name = "وضعیت پرداخت")]
        public PaymentStatus? PaymentStatus { get; set; }
        [Display(Name = "سفارش های فیشینگ")]
        public int? UserIsPhisher { get; set; }

        //public Dictionary<int, string> CurrencyTypes { get; set; }
    }
}