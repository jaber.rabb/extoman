﻿using System;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class AddProductViewModel : BaseViewModel<AddProductViewModel, Product>
    {
        public string Name { get; set; }
        public string AlternateName { get; set; }
        public string Url { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string FocusKeyword { get; set; }
        public int VisitCount { get; set; }
        public float Rate { get; set; }
        public DateTime? PublishDate { get; set; }
        public DateTime? ReleaseDate { get; set; } // Coming soon products
        public int Width { get; set; } // cm
        public int Height { get; set; } // cm
        public int Depth { get; set; } // cm
        public int Weight { get; set; } // gram
        public int InStock { get; set; }
        public int? MediaId { get; set; } // media id
        public int DiscountOffPercent { get; set; }
        public bool ProductionStopped { get; set; }
        public string Waranty { get; set; }
        public int BrandId { get; set; }
    }
}