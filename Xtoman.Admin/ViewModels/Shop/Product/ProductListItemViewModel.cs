﻿using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class ProductListItemViewModel : BaseViewModel<ProductListItemViewModel, Product>
    {
    }
}