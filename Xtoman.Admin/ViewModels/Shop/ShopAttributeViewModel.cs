﻿using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class ShopAttributeViewModel : BaseViewModel<ShopAttributeViewModel, ShopAttribute>
    {
        public string ColorHex { get; set; }
        public string Name { get; set; }
    }
}