﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Utility;

namespace Xtoman.Admin.ViewModels
{
    public class SpecificationViewModel : BaseViewModel<SpecificationViewModel, Specification>
    {
        [Required]
        [Display(Name = "نام خصوصیت")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "نوع")]
        public SpecificationType Type { get; set; }
        [Required]
        [Display(Name = "وضعیت قرارگیری فیلتر جستجو")]
        public SpecificationSearchFilter Filter { get; set; }
        [Display(Name = "دسته بندی ها")]
        public ICollection<CategoryViewModel> Categories { get; set; }
        [Display(Name = "گروه خصوصیت")]
        [Required]
        public string GroupNameField
        {
            get
            {
                if (!_groupName.HasValue() && GroupName.HasValue())
                    _groupName = GroupName;
                return _groupName;
            }
            set
            {
                _groupName = value;
            }
        }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public List<int> SelectedCategories
        {
            get
            {
                if (_selectedCategories == null || _selectedCategories.Count == 0)
                {
                    _selectedCategories = Categories != null ? Categories.Select(p => p.Id.TryToInt()).ToList() : new List<int>();
                }
                return _selectedCategories;
            }
            set
            {
                _selectedCategories = value;
            }
        }
        private List<int> _selectedCategories;
        private string _groupName;

        [Display(Name = "پیش نوشته ها")]
        public ICollection<SpecificationDefaultViewModel> Defaults { get; set; }
    }

    public class SpecificationGroupViewModel : BaseViewModel<SpecificationGroupViewModel, SpecificationGroup>
    {
        [Display(Name = "نام گروه خصوصیت")]
        public string Name { get; set; }
    }

    public class SpecificationDefaultViewModel : BaseViewModel<SpecificationDefaultViewModel, SpecificationDefault>
    {
        [Display(Name = "متن پیش نوشته")]
        public string Text { get; set; }
    }
}