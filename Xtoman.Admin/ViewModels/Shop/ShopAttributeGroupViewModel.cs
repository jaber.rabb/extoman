﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class ShopAttributeGroupViewModel : BaseViewModel<ShopAttributeGroupViewModel, ShopAttributeGroup>
    {
        [Display(Name = "نام گروه")]
        public string Name { get; set; }
        [Display(Name = "رنگ است؟")]
        public bool IsColor { get; set; }
        [Display(Name = "نوع ویژگی ترکیب")]
        public AttributeGroupType Type { get; set; }
        [Display(Name = "جایگاه")]
        public int Position { get; set; }
        [Display(Name = "مشخصه ها")]
        public List<ShopAttributeViewModel> ShopAttributes { get; set; }
    }
}