﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Admin.ViewModels
{
    public class SendMassSmsViewModel
    {
        [Display(Name = "ارسال به")]
        public SendSMSTo To { get; set; }
        [Display(Name = "متن پیام")]
        public string Text { get; set; }
    }

    public enum SendSMSTo
    {
        [Display(Name = "همه", Description = "همه کاربران سایت ایکس تومن")]
        AllUsers,
        [Display(Name = "موبایل تایید شده", Description = "کاربرانی که شماره موبایلشان تایید شده")]
        AllValidatedPhoneNumbers,
        [Display(Name = "کاملا وریفایی شده", Description = "کاربرانی که حسابشان کاملا وریفایی شده")]
        VerifiedUsersOnly,
        [Display(Name = "سفارش تکمیل شده", Description = "کاربرانی که سفارش تکمیل شده دارند")]
        UsersWithCompletedOrders,
        [Display(Name = "سفارش تکمیل یا وریفایی کامل", Description = "کاربرانی که وریفایی شده اند یا سفارش تکمیل شده دارند")]
        UsersWithCompletedOrdersAndVerifiedUsers,
        [Display(Name = "سفارش تکمیل و وریفایی کامل", Description = "کاربرانی که وریفایی شده اند و سفارش تکمیل شده دارند")]
        VerifiedUsersWithCompletedOrders
    }
}