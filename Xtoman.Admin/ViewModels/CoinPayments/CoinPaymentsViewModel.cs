﻿using System.Collections.Generic;

namespace Xtoman.Admin.ViewModels
{
    public class CoinPaymentsViewModel
    {
        public List<CoinPaymentsBalanceViewModel> Balances { get; set; }
    }
}