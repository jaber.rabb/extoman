﻿using Xtoman.Domain.Models;

namespace Xtoman.Admin.ViewModels
{
    public class CoinPaymentsBalanceViewModel
    {
        public ECurrencyType Type { get; set; }
        public decimal Balance { get; set; }
        public bool IsActiveNow { get; set; }
    }
}