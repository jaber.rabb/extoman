﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class CoinPaymentsIPNListItemViewModel : BaseViewModel<CoinPaymentsIPNListItemViewModel, CoinPaymentsIPNResult>
    {
        [Display(Name = "تاریخ")]
        public DateTime InsertDate { get; set; }
        public string TxnId { get; set; }
        [Display(Name = "کد وضعیت")]
        public int Status { get; set; }
        [Display(Name = "وضعیت")]
        public string StatusText { get; set; }
        [Display(Name = "ارز")]
        public string Currency { get; set; }
        public int Confirms { get; set; }
        public decimal Amount { get; set; }
        [Display(Name = "مقدار پرداخت")]
        public string Amount1 { get; set; }
        [Display(Name = "شماره سفارش")]
        public string Custom { get; set; }
        [Display(Name = "مقدار پرداخت شده")]
        public string ReceivedAmount { get; set; }
        [Display(Name = "کانفیرم دریافت شده")]
        public string ReceivedConfirms { get; set; }
    }
}