﻿using System;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class CoinPaymentsIPNViewModel : BaseViewModel<CoinPaymentsIPNViewModel, CoinPaymentsIPNResult>
    {
        public DateTime InsertDate { get; set; }
        public string IpnVersion { get; set; }
        public string IpnType { get; set; }
        public string IpnMode { get; set; }
        public string IpnId { get; set; }
        public string Merchant { get; set; }
        public string Address { get; set; }
        public string TxnId { get; set; }
        public int Status { get; set; }
        public string StatusText { get; set; }
        public string Currency { get; set; }
        public int Confirms { get; set; }
        public decimal Amount { get; set; }
        public decimal Amounti { get; set; }
        public decimal? Fee { get; set; }
        public decimal? Feei { get; set; }
        public string CId { get; set; }
        public string Currency1 { get; set; }
        public string Currency2 { get; set; }
        public string Amount1 { get; set; }
        public string Amount2 { get; set; }
        public string BuyerName { get; set; }
        public string Email { get; set; }
        public string ItemName { get; set; }
        public string ItemNumber { get; set; }
        public string Invoice { get; set; }
        public string Custom { get; set; }
        public string SendTx { get; set; }
        public string ReceivedAmount { get; set; }
        public string ReceivedConfirms { get; set; }
    }
}