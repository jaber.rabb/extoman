﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class BrandViewModel : BaseViewModel<BrandViewModel, Brand>
    {
        [Display(Name = "نام")]
        public string Name { get; set; }
        [Display(Name = "نام دوم")]
        public string AlternativeName { get; set; }
        [Display(Name = "کشور سازنده")]
        public string Country { get; set; }
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        [Display(Name = "آدرس نام آشنای صفحه")]
        public string Url { get; set; }
        [Display(Name = "متا کلمات کلیدی")]
        public string MetaKeywords { get; set; }
        [Display(Name = "متا توضیحات")]
        public string MetaDescription { get; set; }
        [Display(Name = "تاریخ افزودن")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "تاریخ ویرایش")]
        public DateTime? UpdateDate { get; set; }
        [Display(Name = "افزوده توسط")]
        public UserSimpleViewModel InsertUser { get; set; }
        [Display(Name = "ویرایش توسط")]
        public UserSimpleViewModel UpdateUser { get; set; }

        [Display(Name = "تصویر")]
        public MediaViewModel Media { get; set; }
    }
}