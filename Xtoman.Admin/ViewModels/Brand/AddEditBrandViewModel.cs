﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class AddEditBrandViewModel : BaseViewModel<AddEditBrandViewModel, Brand>, ILocalizedModel<BrandLocalizedViewModel>, IUrlHistoryModel<BrandLocalizedViewModel>
    {

        [Required]
        [Display(Name = "نام برند")]
        public string Name { get; set; }
        [Display(Name = "نام دوم")]
        public string AlternativeName { get; set; }
        [Display(Name = "آدرس اصلی")]
        public string OriginalUrl { get; set; }
        [Required]
        [Display(Name = "URL آشنا")]
        public string Url { get; set; }
        [AllowHtml]
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        [Display(Name = "کشور سازنده")]
        public string Country { get; set; }
        [Display(Name = "کلمات کلیدی برند")]
        public string MetaKeywords { get; set; }
        [Display(Name = "متا توضیحات برند")]
        public string MetaDescription { get; set; }
        public int? MediaId { get; set; }
        public MediaViewModel Media { get; set; }

        public Language Language { get; set; }

        private IList<BrandLocalizedViewModel> _locales;
        public IList<BrandLocalizedViewModel> Locales
        {
            get { return _locales ?? (_locales = new List<BrandLocalizedViewModel>()); }
            set { _locales = value; }
        }
    }

    public class BrandLocalizedViewModel : ILocalizedModelLocal, ILocalizedUrlHistoryModel
    {
        [Display(Name = "نام")]
        public string Name { get; set; }
        [AllowHtml]
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        [Display(Name = "توضیحات متا")]
        public string MetaDescription { get; set; }
        [Display(Name = "کلمات کلیدی متا")]
        public string MetaKeywords { get; set; }
        [Display(Name = "Url نام آشنا")]
        public string Url { get; set; }
        [Display(Name = "آدرس اصلی")]
        public string OriginalUrl { get; set; }
        [Display(Name = "زبان")]
        public int LanguageId { get; set; }
    }
}