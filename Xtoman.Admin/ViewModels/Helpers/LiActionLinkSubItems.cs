﻿namespace Xtoman.Admin.ViewModels
{
    public class LiActionLinkSubItems
    {
        public string Text { get; set; }
        public string Action { get; set; }
        public object RouteValues { get; set; }
        public int BadgeValue { get; set; }
        public string BadgeText { get; set; }
    }

    public class LiActionBadgeItem
    {
        public string Text { get; set; }
        public int Value { get; set; }
        public string Badge { get; set; }
    }
}