﻿using System.Collections.Generic;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class RoleViewModel : BaseViewModel<RoleViewModel, AppRole>
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public List<PermissionViewModel> Permissions { get; set; }
    }

    public class PermissionViewModel : BaseViewModel<PermissionViewModel, Permission>
    {
        public string Name { get; set; }
        public string ControllerName { get; set; }
        public string ControllerAction { get; set; }
    }
}