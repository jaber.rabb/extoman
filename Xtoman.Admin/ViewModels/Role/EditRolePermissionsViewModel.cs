﻿using System.Collections.Generic;

namespace Xtoman.Admin.ViewModels
{
    public class EditRolePermissionsViewModel
    {
        public int Id { get; set; }
        public RoleViewModel Role { get; set; }
        public List<ControllerViewModel> Controllers { get; set; }
    }

    public class ControllerViewModel
    {
        public string Name { get; set; }
        public string Display { get; set; }
        public string IconClass { get; set; }
        public List<ActionViewModel> Actions { get; set; }
        public bool IsAllActionsInPermission { get; set; }
    }

    public class ActionViewModel
    {
        public string Name { get; set; }
        public string Attributes { get; set; }
        public string ReturnType { get; set; }
        public bool IsInPermission { get; set; }
    }
}