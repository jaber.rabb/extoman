﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class CarrierViewModel : BaseViewModel<CarrierViewModel, Carrier>
    {
        [Display(Name = "نام")]
        public string Name { get; set; }
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        [Display(Name = "رایگان است؟")]
        public bool IsFree { get; set; }
        [Display(Name = "پرداخت در محل است؟")]
        public bool IsPayInPlace { get; set; }
        [Display(Name = "مدت زمان تحویل به روز")]
        public string ArriveDays { get; set; }
    }
}