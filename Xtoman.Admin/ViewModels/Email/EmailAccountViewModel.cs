﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class EmailAccountViewModel : BaseViewModel<EmailAccountViewModel, EmailAccount>
    {
        [Display(Name = "آدرس ایمیل")]
        public string Email { get; set; }
        [Display(Name = "نام حساب")]
        public string DisplayName { get; set; }
        [Display(Name = "آدرس سرور")]
        public string Host { get; set; }
        [Display(Name = "پورت سرور")]
        public int Port { get; set; }
        [Display(Name = "نام کاربری حساب")]
        public string Username { get; set; }
        [Display(Name = "کلمه عبور")]
        public string Password { get; set; }
        [Display(Name = "استفاده از پروتکل ایمن SSL?")]
        public bool EnableSsl { get; set; }
        [Display(Name = "استفاده از اطلاعات ورود پیش فرض؟")]
        public bool UseDefaultCredentials { get; set; }
        public string FriendlyName
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(DisplayName))
                    return Email + " (" + DisplayName + ")";
                return Email;
            }
        }
    }
}