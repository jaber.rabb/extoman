﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class SendEmailViewModel
    {
        [AllowHtml]
        [Display(Name = "متن ایمیل")]
        public string Text { get; set; }
        
        [Display(Name = "آدرس ایمیل")]
        public int? EmailAccountId { get; set; }

        [Display(Name = "موضوع")]
        public string Subject { get; set; }

        [Display(Name = "استفاده از قالب پیش فرض")]
        public bool UseTemplate { get; set; }
    }
}