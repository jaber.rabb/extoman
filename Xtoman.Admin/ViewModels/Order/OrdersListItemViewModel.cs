﻿using System;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class OrdersListItemViewModel : BaseViewModel<OrdersListItemViewModel, Order>
    {
        public new long Id { get; set; }
        public Guid Guid { get; set; }
        public PaymentType PaymentType { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public DateTime? CancelDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public string Description { get; set; }

        #region Pricing
        public decimal PayAmount { get; set; }
        public decimal PriceAmount { get; set; }
        public decimal GatewayFee { get; set; }
        public ECurrencyType PaymentECurrencyType { get; set; }
        public string Errors { get; set; }
        #endregion
        public int BankResponseId { get; set; }
        public int UserId { get; set; }
        public virtual AppUser User { get; set; }
    }
}