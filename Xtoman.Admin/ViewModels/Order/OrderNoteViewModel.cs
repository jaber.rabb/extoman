﻿using System;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class OrderNoteViewModel : BaseViewModel<OrderNoteViewModel, OrderNote>
    {
        public string Note { get; set; }
        public bool DisplayToCustomer { get; set; }
        public UserSimpleViewModel InsertUser { get; set; }
        public DateTime InsertDate { get; set; }
    }
}