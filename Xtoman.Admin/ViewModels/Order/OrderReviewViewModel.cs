﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Utility;

namespace Xtoman.Admin.ViewModels
{
    public class OrderReviewViewModel : BaseViewModel<OrderReviewViewModel, OrderReview>
    {
        public new long Id { get; set; }
        [Display(Name = "نظر")]
        public string Text { get; set; }
        [Display(Name = "امتیاز")]
        public byte VoteRate { get; set; }
        [Display(Name = "وضعیت نمایش")]
        public Approvement Visibility { get; set; }
        [Display(Name = "شماره سفارش")]
        public long OrderId { get; set; }

        [Display(Name = "نام")]
        public string OrderUserFirstName { get; set; }
        [Display(Name = "نام خانوادگی")]
        public string OrderUserLastName { get; set; }
        [Display(Name = "نام کاربری")]
        public string OrderUserUserName { get; set; }
        [Display(Name = "آی دی کاربر")]
        public int OrderUserId { get; set; }
        [Display(Name = "نام کامل")]
        public string FullName
        {
            get
            {
                if (OrderUserFirstName.HasValue() && OrderUserLastName.HasValue())
                {
                    return OrderUserFirstName + " " + OrderUserLastName;
                }
                return OrderUserUserName;
            }
        }
    }
}