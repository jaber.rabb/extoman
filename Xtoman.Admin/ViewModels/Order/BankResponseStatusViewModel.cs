﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Payment.Mellat;
using Xtoman.Domain.WebServices.Payment.Saman;
using Xtoman.Framework.Mvc;
using Xtoman.Utility;

namespace Xtoman.Admin.ViewModels
{
    public class BankResponseStatusViewModel : BaseViewModel<BankResponseStatusViewModel, BankResponseStatus>
    {
        [Display(Name = "نام درگاه بانک")]
        public BankGatewayType? BankGatewayType { get; set; }
        [Display(Name = "شماره سفارش بانک")]
        public long BankOrderId { get; set; }
        [Display(Name = "کد پاسخ بانک")]
        public int? BankResponseCode { get; set; }
        [Display(Name = "شناسه پرداخت بانک")]
        public string BankRefId { get; set; }
        [Display(Name = "کد پیگیری بانک")]
        public string BankSaleReferenceId { get; set; }
        [Display(Name = "اطلاعات پرداخت کننده")]
        public string BankCardHolderInfo { get; set; }
        [Display(Name = "شماره کارت پرداخت کننده")]
        public string BankCardHolderPan { get; set; }

        [Display(Name = "پاسخ بانک")]
        public string BankResponseText
        {
            get
            {
                if (BankGatewayType.HasValue && BankResponseCode.HasValue)
                {
                    switch (BankGatewayType.Value)
                    {
                        case Domain.Models.BankGatewayType.BehPardakht:
                            try
                            {
                                return ((MellatResponseMessage)BankResponseCode.Value).ToDisplay();
                            }
                            catch (Exception)
                            {
                                return "نامشخص";
                            }
                        case Domain.Models.BankGatewayType.Sep:
                            try
                            {
                                return ((SamanMessageStatus)BankResponseCode.Value).ToDisplay();
                            }
                            catch (Exception)
                            {
                                return "نامشخص";
                            }
                    }
                }
                return "-";
            }
        }
    }
}