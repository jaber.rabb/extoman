﻿using System.Collections.Generic;

namespace Xtoman.Admin.ViewModels
{
    public class SideMenuGroupItemModel
    {
        public string Name { get; set; }
        public List<string> Controllers { get; set; }
    }

    public static class SideMenu
    {
        public static List<SideMenuGroupItemModel> Groups
        {
            get
            {
                var result = new List<SideMenuGroupItemModel>
                {
                    new SideMenuGroupItemModel()
                    {
                        Name = "Shop",
                        Controllers = new List<string>()
                        {
                            "",
                            "Brand",
                            "Carrier",
                            "ShopAttribute",
                            "Specification",
                            "Supplier"
                        }
                    },
                    new SideMenuGroupItemModel()
                    {
                        Name = "Blog",
                        Controllers = new List<string>()
                        {
                            "",
                            ""
                        }
                    },
                    new SideMenuGroupItemModel()
                    {
                        Name = "Category",
                        Controllers = new List<string>()
                        {
                            "",
                            ""
                        }
                    },
                    new SideMenuGroupItemModel()
                    {
                        Name = "User",
                        Controllers = new List<string>()
                        {
                            "",
                            ""
                        }
                    },
                    new SideMenuGroupItemModel()
                    {
                        Name = "WebService",
                        Controllers = new List<string>()
                        {
                            "",
                            ""
                        }
                    },
                    new SideMenuGroupItemModel()
                    {
                        Name = "Exchange",
                        Controllers = new List<string>()
                        {
                            "",
                            ""
                        }
                    },
                };
                return result;
            }
        }
    }
}