﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class IdentityVerificationViewModel : BaseViewModel<IdentityVerificationViewModel, AppUser>
    {
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }
        [Display(Name = "نام")]
        public string FirstName { get; set; }
        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }
        [Display(Name = "تاریخ تولد")]
        public DateTime? BirthDate { get; set; }
        [Display(Name = "جنسیت")]
        public bool? Gender { get; set; } // false 0 = male | true 1 = female
        [Display(Name = "تصویر ارسالی")]
        public MediaViewModel VerificationMedia { get; set; }
        [Display(Name = "توضیحات")]
        public string IdentityVerificationDescription { get; set; }
        [Display(Name = "وضعیت")]
        public VerificationStatus IdentityVerificationStatus { get; set; }
        public string FullName => FirstName + " " + LastName;
    }
}