﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class TelephoneVerificationViewModel : BaseViewModel<TelephoneVerificationViewModel, AppUser>
    {
        [Display(Name = "نام")]
        public string FirstName { get; set; }
        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }

        [Display(Name = "نام و نام خانوادگی")]
        public string FullName => FirstName + " " + LastName;

        [Display(Name = "ایمیل")]
        public string Email { get; set; }
        [Display(Name = "تلفن همراه")]
        public string PhoneNumber { get; set; }

        [Display(Name = "تلفن")]
        public string Telephone { get; set; }
        [Display(Name = "وضعیت")]
        public VerificationStatus TelephoneConfirmationStatus { get; set; }
        [Display(Name = "وضعیت احراز مدارک")]
        public VerificationStatus IdentityVerificationStatus { get; set; }
        [Display(Name = "توضیحات")]
        public string TelephoneConfirmDescription { get; set; }
    }
}