﻿namespace Xtoman.Admin.ViewModels
{
    public class VerificationIndexViewModel
    {
        public int TelephoneVerifications { get; set; }
        public int IdentityVerifications { get; set; }
        public int BankCardVerifications { get; set; }
    }
}