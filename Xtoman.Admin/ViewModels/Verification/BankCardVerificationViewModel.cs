﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class BankCardVerificationViewModel : BaseViewModel<BankCardVerificationViewModel, UserBankCard>
    {
        [Display(Name = "شماره کارت بانکی")]
        public string CardNumber { get; set; }
        [Display(Name = "وضعیت")]
        public VerificationStatus VerificationStatus { get; set; }
        [Display(Name = "وضعیت احراز مدارک")]
        public VerificationStatus UserIdentityVerificationStatus { get; set; }
        [Display(Name = "تاریخ احراز")]
        public DateTime? VerifyDate { get; set; }
        [Display(Name = "توضیحات")]
        public string VerifyDescription { get; set; }

        public int UserId { get; set; }
        [Display(Name = "نام")]
        public string UserFirstName { get; set; }
        [Display(Name = "نام خانوادگی")]
        public string UserLastName { get; set; }

        [Display(Name = "موبایل")]
        public string UserPhoneNumber { get; set; }
        [Display(Name = "ایمیل")]
        public string UserEmail { get; set; }

        [Display(Name = "نام و نام خانوادگی")]
        public string FullName => UserFirstName + " " + UserLastName;


        public long? FinnotechHistoryId { get; set; }
        public FinnotechResponseCardInfoViewModel FinnotechHistory { get; set; }
    }
}