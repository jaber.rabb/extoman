﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class AddEditBankAccountViewModel : BaseViewModel<AddEditBankAccountViewModel, BankAccount>
    {
        [Required]
        [Display(Name = "نام حساب")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "شماره حساب")]
        public string AccountNumber { get; set; }
        [Required]
        [Display(Name = "نام بانک")]
        public string BankName { get; set; }
        [Display(Name = "شماره شبا")]
        public string Sheba { get; set; }
        [Required]
        [Display(Name = "شماره کارت")]
        public string CardNumber { get; set; }
    }
}