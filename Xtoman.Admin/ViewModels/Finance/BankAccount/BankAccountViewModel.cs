﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class BankAccountViewModel : BaseViewModel<BankAccountViewModel, BankAccount>
    {
        [Display(Name = "نام حساب")]
        public string Name { get; set; }
        [Display(Name = "شماره حساب")]
        public string AccountNumber { get; set; }
        [Display(Name = "نام بانک")]
        public string BankName { get; set; }
        [Display(Name = "شماره شبا")]
        public string Sheba { get; set; }
        [Display(Name = "شماره کارت")]
        public string CardNumber { get; set; }
        [Display(Name = "پرداخت های دستی")]
        public int PaidPaymentsCount { get; set; }
    }
}