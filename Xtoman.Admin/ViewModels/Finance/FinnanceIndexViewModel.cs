﻿namespace Xtoman.Admin.ViewModels
{
    public class FinanceIndexViewModel
    {
        public decimal ThisMonthSellAmount { get; set; }
        public decimal ThisMonthBuyAmount { get; set; }
    }
}