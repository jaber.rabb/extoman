﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class ManualTradingDepositViewModel : BaseViewModel<ManualTradingDepositViewModel, ManualTradingDeposit>
    {
        [Display(Name = "مقدار")]
        public decimal Amount { get; set; }
        [Display(Name = "نوع ارز")]
        public ECurrencyType Type { get; set; }
        [Display(Name = "پلتفرم ترید")]
        public TradingPlatform TradingPlatform { get; set; }
        [Display(Name = "درصد کمیسیون پیمانکار")]
        public int CommissionPercent { get; set; }
        [Display(Name = "روش کار با پیمانکار")]
        public ContractType ContractType { get; set; }
        public int? ContractorUserId { get; set; }
        [Display(Name = "نام پیمانکار")]
        public string ContractorUserFirstName { get; set; }
        [Display(Name = "نام خانوادگی پیمانکار")]
        public string ContractorUserLastName { get; set; }
        [Display(Name = "ایمیل پیمانکار")]
        public string ContractorUserEmail { get; set; }
        [Display(Name = "شماره همراه پیمانکار")]
        public string ContractorUserPhoneNumber { get; set; }
    }
}