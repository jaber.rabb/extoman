﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;

namespace Xtoman.Admin.ViewModels
{
    public class AddEditManualTradingDepositViewModel : BaseViewModel<AddEditManualTradingDepositViewModel, ManualTradingDeposit>
    {
        [Required]
        [MinValue(0)]
        [Display(Name = "مقدار")]
        public decimal Amount { get; set; }
        [Display(Name = "نوع ارز")]
        public ECurrencyType Type { get; set; }
        [Display(Name = "پلتفرم ترید")]
        public TradingPlatform TradingPlatform { get; set; }
        [Display(Name = "درصد کمیسیون پیمانکار")]
        public int CommissionPercent { get; set; }
        [Display(Name = "روش کار با پیمانکار")]
        public ContractType ContractType { get; set; }
        [Display(Name = "پیمانکار")]
        public int? ContractorUserId { get; set; }
    }
}