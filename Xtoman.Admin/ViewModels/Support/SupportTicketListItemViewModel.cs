﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class SupportTicketListItemViewModel : BaseViewModel<SupportTicketListItemViewModel, SupportTicket>
    {
        [Display(Name = "ایمیل یا موبایل")]
        public string EmailOrPhoneNumber { get; set; }
        [Display(Name = "موضوع")]
        public string Subject { get; set; }
        [Display(Name = "اهمیت")]
        public Priority Priority { get; set; }
        [Display(Name = "وضعیت")]
        public TicketStatus Status { get; set; }
        [Display(Name = "واحد پشتیبانی مربوطه")]
        public SupportDepartment Department { get; set; }
        [Display(Name = "آخرین ارسال")]
        public DateTime LastDate { get; set; }
        [Display(Name = "تعداد پاسخ")]
        public int RepliesCount { get; set; }
    }
}