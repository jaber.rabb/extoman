﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class SupportTicketViewModel : BaseViewModel<SupportTicketViewModel, SupportTicket>
    {
        public int? InsertUserId { get; set; }
        [Display(Name = "موضوع")]
        public string Subject { get; set; }
        [Display(Name = "دپارتمان")]
        public SupportDepartment Department { get; set; }
        [Display(Name = "اهمیت")]
        public Priority Priority { get; set; }
        [Display(Name = "وضعیت تیکت")]
        public TicketStatus Status { get; set; }
        [Display(Name = "تاریخ ثبت")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "تاریخ آخرین ارسال")]
        public DateTime LastDate { get; set; }
        [Display(Name = "متن")]
        public string Text { get; set; }
        [Display(Name = "از طرف")]
        public string EmailOrPhoneNumber { get; set; }
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }
        public string FullName { get; set; }
        [Display(Name = "پاسخ ها")]
        public List<SupportTicketReplyViewModel> Replies { get; set; }
        public EditUserViewModel UpdateUser { get; set; }
    }

    public class SupportTicketReplyViewModel
    {
        [Display(Name = "تاریخ ارسال")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "متن پاسخ")]
        public string Text { get; set; }
        [Display(Name = "پاسخ از طرف پشتیبانی")]
        public bool IsSupportReply { get; set; }
        public string InsertUserUserName { get; set; }
        public string InsertUserFirstName { get; set; }
        public string InsertUserLastName { get; set; }
        public string InsertUserFullName => InsertUserFirstName + " " + InsertUserLastName;
    }

    public class SupportTicketReplySubmitViewModel : BaseViewModel<SupportTicketReplySubmitViewModel, SupportTicketReply>
    {
        public SupportTicketReplySubmitViewModel()
        {
            IsSupportReply = true;
        }
        [Required]
        [Display(Name = "متن پاسخ")]
        public string Text { get; set; }
        public bool IsSupportReply { get; set; }
    }
}