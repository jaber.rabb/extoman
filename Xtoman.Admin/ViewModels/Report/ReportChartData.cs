﻿namespace Xtoman.Admin.ViewModels
{
    public class ReportChartData
    {
        public int BuyCount { get; set; }
        public decimal BuyAmount { get; set; }
        public int SellCount { get; set; }
        public decimal SellAmount { get; set; }
    }
}