﻿using System;

namespace Xtoman.Admin.ViewModels
{
    public class ReportIndexViewModel
    {
        public int TotalNumberOfOrdersInMonth { get; set; }
        public int TotalNumberOfBuyOrdersInMonth { get; set; }
        public int TotalNumberOfSellOrdersInMonth { get; set; }
        public decimal TotalBuyOrdersAmountInToman { get; set; }
        public decimal TotalSellOrdersAmountInToman { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string MonthName { get; set; }
    }
}