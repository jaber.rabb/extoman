﻿namespace Xtoman.Admin.ViewModels
{
    public class PotentialUsersViewModel
    {
        public int VerifiedWithNoOrderCount { get; set; }
        public int PartialVerificationCount { get; set; }
        public int NotSeenFor3MonthsCount { get; set; }
        public int UsersWithCompletedOrdersCount { get; set; }
        public int IdentityVerificationFailedsCount { get; set; }
    }
}