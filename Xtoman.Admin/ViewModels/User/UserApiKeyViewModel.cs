﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models.Api;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class UserApiKeyViewModel : BaseViewModel<UserApiKeyViewModel, UserApiKey>
    {
        [Display(Name = "Api Key")]
        public string Key { get; set; }
        [Display(Name = "Api Secret")]
        public string Secret { get; set; }
        public int UserId { get; set; }
        [Display(Name = "تاریخ ایجاد")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "ایجاد شده توسط")]
        public UserSimpleViewModel InsertUser { get; set; }
        [Display(Name = "کاربر")]
        public UserSimpleViewModel User { get; set; }
    }
}