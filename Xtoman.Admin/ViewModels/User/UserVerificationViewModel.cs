﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class UserVerificationViewModel : BaseViewModel<UserVerificationViewModel, UserVerification>
    {
        public new long Id { get; set; }
        public UserViewModel InsertUser { get; set; }
        [Display(Name = "تاریخ ایجاد")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "نوع احراز")]
        public UserVerificationType Type { get; set; }
        [Display(Name = "وضعیت احراز")]
        public VerificationStatus Status { get; set; }
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        [Display(Name = "تصویر")]
        public MediaViewModel Media { get; set; }
    }
}