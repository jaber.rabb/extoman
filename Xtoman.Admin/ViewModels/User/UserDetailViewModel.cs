﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Utility;

namespace Xtoman.Admin.ViewModels
{
    public class UserDetailViewModel : BaseViewModel<UserDetailViewModel, AppUser>
    {
        [Display(Name = "کاربر محدود شده؟")]
        public bool IsBuyLimited { get; set; }
        [Display(Name = "نام")]
        public string FirstName { get; set; }
        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }
        [Display(Name = "موبایل")]
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        [Display(Name = "ایمیل")]
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        [Display(Name = "تاریخ تولد")]
        public DateTime? BirthDate { get; set; }
        [Display(Name = "جنسیت")]
        public bool? Gender { get; set; } // false 0 = male | true 1 = female
        [Display(Name = "معرف")]
        public UserViewModel Referrer { get; set; }
        [Display(Name = "تاریخ عضویت")]
        public DateTime RegDate { get; set; }
        [Display(Name = "حساب مسدود شده")]
        public bool IsSuspended { get; set; }
        [Display(Name = "آخرین تاریخ ورود")]
        public DateTime? LastLoginDate { get; set; }
        [Display(Name = "توضیحات احراز مدارک")]
        public string IdentityVerificationDescription { get; set; }
        [Display(Name = "وضعیت احراز مدارک")]
        public VerificationStatus IdentityVerificationStatus { get; set; }
        [Display(Name = "تاریخ احراز مدارک")]
        public DateTime? IdentityVerificationVerifyDate { get; set; }
        [Display(Name = "شماره تلفن ثابت")]
        public string Telephone { get; set; }
        [Display(Name = "وضعیت احراز تلفن ثابت")]
        public VerificationStatus TelephoneConfirmationStatus { get; set; }
        [Display(Name = "توضیحات احراز تلفن ثابت")]
        public string TelephoneConfirmDescription { get; set; }
        public MediaViewModel VerificationMedia { get; set; }
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }
        [Display(Name = "کاربر Phisher است؟")]
        public bool IsPhisher { get; set; }
        public string FullName
        {
            get {
                var fullName = (FirstName + " " + LastName).Trim();
                return fullName.HasValue() ? fullName : UserName;
            }
        }
        public List<UserBankCardViewModel> UserBankCards { get; set; }

        //Manual
        public List<ExchangeOrdersListItemViewModel> ExchangeOrders { get; set; }
        public List<SupportTicketListItemViewModel> Supports { get; set; }
        public List<UserVerificationViewModel> UserVerifications { get; set; }
        public List<UserSimpleViewModel> Referrals { get; set; }
    }
}