﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Utility;

namespace Xtoman.Admin.ViewModels
{
    public class UserSimpleViewModel : BaseViewModel<UserSimpleViewModel, AppUser>
    {
        [Display(Name = "کاربر محدود شده؟")]
        public bool IsBuyLimited { get; set; }
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }
        [Display(Name = "ایمیل")]
        public string Email { get; set; }
        [Display(Name = "نام")]
        public string FirstName { get; set; }
        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }
        [Display(Name = "موبایل")]
        public string PhoneNumber { get; set; }
        [Display(Name = "نام کامل")]
        public string FullName
        {
            get
            {
                if (!_fullName.HasValue())
                {
                    var fullName = FirstName.Trim() + " " + LastName.Trim();
                    if (!fullName.HasValue())
                        fullName = UserName;
                    _fullName = fullName;
                }
                return _fullName;
            }
        }

        private string _fullName = "";
    }
}