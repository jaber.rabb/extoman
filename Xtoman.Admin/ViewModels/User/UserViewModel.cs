﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Utility;

namespace Xtoman.Admin.ViewModels
{
    public class UserViewModel : BaseViewModel<UserViewModel, AppUser>
    {
        [Display(Name = "ایمیل")]
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        [Display(Name = "تلفن همراه")]
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }
        [Display(Name = "نام")]
        public string FirstName { get; set; }
        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }
        [Display(Name = "نام کامل")]
        public string FullName => (FirstName + " " + LastName).Trim().HasValue() ? (FirstName + " " + LastName).Trim() : UserName;
        [Display(Name = "تاریخ تولد")]
        public DateTime? BirthDate { get; set; }
        [Display(Name = "جنسیت")]
        public bool? Gender { get; set; } // false 0 = male | true 1 = female
        [Display(Name = "تاریخ عضویت")]
        public DateTime RegDate { get; set; }
        [Display(Name = "بلاک شده؟")]
        public bool IsSuspended { get; set; }
        [Display(Name = "آخرین ورود")]
        public DateTime? LastLoginDate { get; set; }
        [Display(Name = "احراز هویت")]
        public DateTime? VerifyDate { get; set; }
        [Display(Name = "احراز هویت")]
        public VerificationModelStatus VerifyStatus
        {
            get
            {
                if (IdentityVerificationStatus == VerificationStatus.Confirmed)
                {
                    if (EmailConfirmed)
                    {
                        if (PhoneNumberConfirmed)
                        {
                            if (TelephoneConfirmationStatus == VerificationStatus.Confirmed)
                            {
                                return VerificationModelStatus.Confirmed;
                            }
                            else
                            {
                                switch (TelephoneConfirmationStatus)
                                {
                                    case VerificationStatus.NotSent:
                                        return VerificationModelStatus.TelephoneNotEntered;
                                    case VerificationStatus.Pending:
                                        return VerificationModelStatus.TelephonePending;
                                    case VerificationStatus.NotConfirmed:
                                        return VerificationModelStatus.TelephoneNotConfirmed;
                                    default:
                                        return VerificationModelStatus.TelephoneConfirmed;
                                }
                            }
                        }
                        else
                        {
                            return PhoneNumber.HasValue() ? VerificationModelStatus.PhoneNumberNotConfirmed : VerificationModelStatus.PhoneNumberNotEntered;
                        }
                    }
                    else
                    {
                        return Email.HasValue() ? VerificationModelStatus.EmailNotConfirmed : VerificationModelStatus.EmailNotEntered;
                    }
                }
                else
                {
                    switch (IdentityVerificationStatus)
                    {
                        case VerificationStatus.NotSent:
                            return VerificationModelStatus.IdentityNotSent;
                        case VerificationStatus.Pending:
                            return VerificationModelStatus.IdentityPending;
                        default:
                        case VerificationStatus.Confirmed:
                            return VerificationModelStatus.IdentityConfirmed;
                        case VerificationStatus.NotConfirmed:
                            return VerificationModelStatus.IdentityNotConfirmed;
                    }
                }
            }
        }

        public VerificationStatus IdentityVerificationStatus { get; set; }
        public VerificationStatus TelephoneConfirmationStatus { get; set; }
        public List<UserBankCardViewModel> UserBankCards { get; set; }

        [Display(Name = "معرف")]
        public string ReferrerUserName { get; set; }
    }
}