﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class UserBankCardViewModel : BaseViewModel<UserBankCardViewModel, UserBankCard>
    {
        [Display(Name = "شماره کارت")]
        public string CardNumber { get; set; }
        [Display(Name = "وضعیت احراز کارت")]
        public VerificationStatus VerificationStatus { get; set; }
        [Display(Name = "تاریخ احراز کارت")]
        public DateTime? VerifyDate { get; set; }
        [Display(Name = "توضیحات احراز کارت")]
        public string VerifyDescription { get; set; }
    }
}