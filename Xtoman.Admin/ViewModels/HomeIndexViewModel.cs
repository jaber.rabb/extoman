﻿using System.Collections.Generic;
using Xtoman.Domain.WebServices;

namespace Xtoman.Admin.ViewModels
{
    public class HomeIndexViewModel
    {
        #region Order
        public int DailyOrdersCount { get; set; }
        public int WeeklyOrdersCount { get; set; }
        public int MonthlyOrdersCount { get; set; }
        public int TotalCompletedOrders { get; set; }
        public int ProcessingOrdersCount { get; set; }
        #endregion

        #region Users
        public int TotalUsers { get; set; }
        public int TotalVerifiedUsers { get; set; }
        public int TodayNewUsers { get; set; }
        public int PastWeekNewUsers { get; set; }
        public int PastMonthNewUsers { get; set; }
        public int VerifyPendingsCount { get; set; }
        public int LoyalUsersCount { get; set; }
        #endregion

        public string TotalShetabPaidComplete { get; set; }
        public string TotalBTCSold { get; set; }
        public List<BalanceResultItem> Balances { get; set; }
        public string TotalPMSold { get; set; }
    }
}