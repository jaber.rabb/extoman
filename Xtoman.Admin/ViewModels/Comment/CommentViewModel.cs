﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin.ViewModels
{
    public class CommentViewModel : BaseViewModel<CommentViewModel, Comment>
    {
        public int? ParentCommentId { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        public string Text { get; set; }
        public GeneralType Type { get; set; }
        public bool? IsConfirm { get; set; }
        public int? ConfirmUserId { get; set; }
        public int RelatedEntityId { get; set; }
        public DateTime? ConfirmDate { get; set; }
        public DateTime? InsertDate { get; set; }
        public string AnonymousID { get; set; }
        public virtual Post Post { get; set; }
        public virtual CommentViewModel Parent { get; set; }
        public virtual AppUser User { get; set; }
    }
}