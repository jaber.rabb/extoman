﻿using Xtoman.Domain.WebServices.Payment.LocalBitcoins;

namespace Xtoman.Admin.ViewModels
{
    public class LocalBitcoinsIndexViewModel
    {
        public LocalBitcoinsBuySellOnline Buy { get; set; }
        public LocalBitcoinsBuySellOnline Sell { get; set; }
    }
}