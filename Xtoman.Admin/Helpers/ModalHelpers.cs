﻿using System.Web.Mvc;
using System.Web.Mvc.Html;

public static class ModalHelpers
{
    public static MvcHtmlString CreateDeleteModal(this HtmlHelper htmlHelper, bool ajax = false)
    {
        htmlHelper.ViewBag.ajax = ajax;
        var controllerName = htmlHelper.ViewContext.RouteData.Values["controller"].ToString();
        var actionName = htmlHelper.ViewContext.RouteData.Values["action"].ToString();
        var modalId = string.Format("DeleteModal_{0}_{1}", controllerName, actionName);
        return htmlHelper.Partial("_DeleteView", modalId);
    }
    public static MvcHtmlString CreateDeleteModal(this HtmlHelper htmlHelper, string modalId, bool ajax = false)
    {
        htmlHelper.ViewBag.ajax = ajax;
        modalId = modalId.Replace('.', '_');
        return htmlHelper.Partial("_DeleteView", modalId);
    }
}