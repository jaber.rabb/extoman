﻿using System.Collections.Generic;
using System.Linq;
using Xtoman.Admin.ViewModels;
using Xtoman.Utility;

public static class ThumbnailHelpers
{
    public static string ImgSrc(this List<MediaThumbnailViewModel> thumbnails, string type, string defaultUrl = "")
    {
        var src = thumbnails?.Where(p => p.MediaThumbnailSizeName.Equals(type, System.StringComparison.InvariantCultureIgnoreCase)).Select(p => p.Url.ToStaticUrl()).FirstOrDefault() ?? defaultUrl;
        return src;
    }

    public static string ImgSrc(this List<MediaThumbnailViewModel> thumbnails, string type, string subdomain, string defaultUrl = "")
    {
        var src = thumbnails?.Where(p => p.MediaThumbnailSizeName.Equals(type, System.StringComparison.InvariantCultureIgnoreCase)).Select(p => p.Url.ToStaticUrl()).FirstOrDefault() ?? defaultUrl;
        return src;
    }
}