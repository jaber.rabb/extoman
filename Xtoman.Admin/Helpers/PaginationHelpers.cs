﻿using PagedList.Mvc;

namespace System.Web.Mvc
{
    public static class PaginationHelpers
    {
        public static PagedListRenderOptions GetPagedListRenderOptions(this HtmlHelper htmlHelper)
        {
            return new PagedListRenderOptions()
            {
                UlElementClasses = new string[] { "pagination" },
                LiElementClasses = new string[] { "paginate_button", "page-item" },
                ContainerDivClasses = new string[] { "dataTables_paginate", "paging_simple_numbers" },
                Display = PagedListDisplayMode.IfNeeded,
                DisplayLinkToFirstPage = PagedListDisplayMode.Never,
                DisplayLinkToIndividualPages = true,
                DisplayLinkToLastPage = PagedListDisplayMode.Always,
                DisplayLinkToNextPage = PagedListDisplayMode.Always,
                DisplayLinkToPreviousPage = PagedListDisplayMode.Always,
                DisplayEllipsesWhenNotShowingAllPageNumbers = false,
                LinkToPreviousPageFormat = "<i class='fa fa-angle-right'></i>",
                LinkToNextPageFormat = "<i class='fa fa-angle-left'></i>",
                LinkToLastPageFormat = "<i class='fa fa-angle-double-left'></i>",
                MaximumPageNumbersToDisplay = 5,
                FunctionToTransformEachPageLink = (liTag, aTag) => {
                    aTag.Attributes.Add("class", "page-link");
                    liTag.InnerHtml = aTag.ToString();
                    return liTag;
                }
            };
        }
    }
}