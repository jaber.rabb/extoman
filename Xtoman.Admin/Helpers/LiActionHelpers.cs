﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;

namespace Xtoman.Admin.Helpers
{
    public static class LiActionHelpers
    {
        public static MvcHtmlString LiActionLink(this HtmlHelper htmlHelper, string text, string action, string controller, string iconClass, List<LiActionBadgeItem> badges)
        {
            var context = htmlHelper.ViewContext;
            if (context.Controller.ControllerContext.IsChildAction)
                context = htmlHelper.ViewContext.ParentActionViewContext;
            var currentRouteValues = context.RouteData.Values;
            var currentAction = currentRouteValues["action"].ToString();
            var currentController = currentRouteValues["controller"].ToString();
            var clss = "";
            if (currentAction.Equals(action, StringComparison.InvariantCulture) &&
                currentController.Equals(controller, StringComparison.InvariantCulture))
                clss = "active";
            clss = clss.Trim();
            var badgesSpan = "";
            for (int i = 0; i < badges.Count; i++)
            {
                var item = badges[i];
                var marginClass = i + 1 != badges.Count ? " m-l-5" : "";
                if (item.Value > 0)
                    badgesSpan += @"<span class=""badge badge-" + item.Badge + marginClass + @" pull-right"" data-toggle=""tooltip"" title=""" + item.Text + @""">" + item.Value + "</span>";
            }
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            var li = String.Format("<li><a href=\"{0}\" class=\"waves-effect waves-primary{1}\"><i class=\"{2}\"></i><span> {3} </span>{4}</a></li>",
                urlHelper.Action(action, controller),
                !string.IsNullOrWhiteSpace(clss) ? " " + clss : String.Empty,
                iconClass,
                text,
                badgesSpan
            );
            return new MvcHtmlString(li);
        }

        public static MvcHtmlString LiActionLink(this HtmlHelper htmlHelper, List<AppRole> roles, string text, string action, string controller, string iconClass, object routeValues = null)
        {
            var context = htmlHelper.ViewContext;
            if (context.Controller.ControllerContext.IsChildAction)
                context = htmlHelper.ViewContext.ParentActionViewContext;
            var currentRouteValues = context.RouteData.Values;
            var currentAction = currentRouteValues["action"].ToString();
            var currentController = currentRouteValues["controller"].ToString();
            var clss = "";
            if (currentAction.Equals(action, StringComparison.InvariantCulture) &&
                currentController.Equals(controller, StringComparison.InvariantCulture))
                clss = "active";
            clss = clss.Trim();
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            var li = String.Format("<li><a href=\"{0}\" class=\"waves-effect waves-primary{1}\"><i class=\"{2}\"></i><span> {3} </span></a></li>",
                urlHelper.Action(action, controller, routeValues),
                !string.IsNullOrWhiteSpace(clss) ? " " + clss : String.Empty,
                iconClass,
                text
            );
            return new MvcHtmlString(li);
        }


        public static MvcHtmlString LiActionLink(this HtmlHelper htmlHelper, string text, string action, string controller, string iconClass, object routeValues = null)
        {
            var context = htmlHelper.ViewContext;
            if (context.Controller.ControllerContext.IsChildAction)
                context = htmlHelper.ViewContext.ParentActionViewContext;
            var currentRouteValues = context.RouteData.Values;
            var currentAction = currentRouteValues["action"].ToString();
            var currentController = currentRouteValues["controller"].ToString();
            var clss = "";
            if (currentAction.Equals(action, StringComparison.InvariantCulture) &&
                currentController.Equals(controller, StringComparison.InvariantCulture))
                clss = "active";
            clss = clss.Trim();
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            var li = String.Format("<li><a href=\"{0}\" class=\"waves-effect waves-primary{1}\"><i class=\"{2}\"></i><span> {3} </span></a></li>",
                urlHelper.Action(action, controller, routeValues),
                !string.IsNullOrWhiteSpace(clss) ? " " + clss : String.Empty,
                iconClass,
                text
            );
            return new MvcHtmlString(li);
        }

        public static MvcHtmlString LiActionLink(this HtmlHelper htmlHelper, List<AppRole> roles, string text, string controller, string iconClass, List<LiActionLinkSubItems> subitems)
        {
            var context = htmlHelper.ViewContext;
            if (context.Controller.ControllerContext.IsChildAction)
                context = htmlHelper.ViewContext.ParentActionViewContext;
            var currentRouteValues = context.RouteData.Values;
            var currentAction = currentRouteValues["action"].ToString();
            var currentController = currentRouteValues["controller"].ToString();
            var user = context.HttpContext.User;
            var userRoles = new List<AppRole>();
            foreach (var role in roles)
                if (user.IsInRole(role.Name))
                    userRoles.Add(role);

            var li = "";
            if (userRoles.Any(x => x.Name == "Admin") || userRoles.Any(x => x.Permissions.Any(f => f.ControllerName.Equals(currentController, StringComparison.InvariantCultureIgnoreCase))))
            {
                var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
                var sublisitems = "";
                foreach (var item in subitems)
                {
                    if (userRoles.Any(x => x.Name == "Admin") || userRoles.Any(x => x.Permissions.Any(f => f.ControllerName.Equals(currentController, StringComparison.InvariantCultureIgnoreCase) && f.ControllerAction.Equals(item.Action, StringComparison.InvariantCultureIgnoreCase))))
                    {
                        var activeClass = currentAction.Equals(item.Action, StringComparison.InvariantCulture) && currentController.Equals(controller, StringComparison.InvariantCulture);
                        sublisitems += item.BadgeValue > 0 ?
                                                           LiActionLink(htmlHelper, item.Text, item.Action, controller, activeClass, item.BadgeValue, item.BadgeText, item.RouteValues) :
                                                           LiActionLink(htmlHelper, item.Text, item.Action, controller, activeClass, item.RouteValues);
                    }
                }

                var liclass = "";
                if (currentController.Equals(controller, StringComparison.InvariantCulture))
                    liclass = "subdrop active";

                li = String.Format("<li class=\"has_sub\"><a href=\"#\" onclick=\"return false;\" class=\"waves-effect waves-primary{0}\"><i class=\"{1}\"></i><span> {2} </span> <span class=\"menu-arrow\"></span></a><ul class=\"list-unstyled\">{3}</ul></li>",
                    !string.IsNullOrWhiteSpace(liclass) ? " " + liclass : String.Empty,
                    iconClass,
                    text,
                    sublisitems
                );
            }

            return new MvcHtmlString(li);
        }


        public static MvcHtmlString LiActionLink(this HtmlHelper htmlHelper, string text, string controller, string iconClass, List<LiActionLinkSubItems> subitems)
        {
            var context = htmlHelper.ViewContext;
            if (context.Controller.ControllerContext.IsChildAction)
                context = htmlHelper.ViewContext.ParentActionViewContext;
            var currentRouteValues = context.RouteData.Values;
            var currentAction = currentRouteValues["action"].ToString();
            var currentController = currentRouteValues["controller"].ToString();

            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            var sublisitems = "";
            foreach (var item in subitems)
            {
                var activeClass = currentAction.Equals(item.Action, StringComparison.InvariantCulture) && currentController.Equals(controller, StringComparison.InvariantCulture);
                sublisitems += item.BadgeValue > 0 ?
                                                   LiActionLink(htmlHelper, item.Text, item.Action, controller, activeClass, item.BadgeValue, item.BadgeText, item.RouteValues) :
                                                   LiActionLink(htmlHelper, item.Text, item.Action, controller, activeClass, item.RouteValues);
            }

            var liclass = "";
            if (currentController.Equals(controller, StringComparison.InvariantCulture))
                liclass = "subdrop active";

            var li = String.Format("<li class=\"has_sub\"><a href=\"#\" onclick=\"return false;\" class=\"waves-effect waves-primary{0}\"><i class=\"{1}\"></i><span> {2} </span> <span class=\"menu-arrow\"></span></a><ul class=\"list-unstyled\">{3}</ul></li>",
                !string.IsNullOrWhiteSpace(liclass) ? " " + liclass : String.Empty,
                iconClass,
                text,
                sublisitems
            );
            return new MvcHtmlString(li);
        }

        private static MvcHtmlString LiActionLink(HtmlHelper htmlHelper, string text, string action, string controller, bool activeClass, int badgeValue, string badgeText, object routeValues = null)
        {
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            var href = urlHelper.Action(action, controller, routeValues);
            var tooltip = !string.IsNullOrWhiteSpace(badgeText) ? @"data-toggle=""tooltip"" title=""" + badgeText + "\" " : "";
            var badgeSpan = string.Format(@"<span {0}class=""badge badge-primary pull-right"">{1}</span>", tooltip, badgeValue);
            var a = string.Format(@"<a href=""{0}"">{1}{2}</a>", href, text, badgeSpan);
            var li = String.Format("<li{0}>{1}</li>",
                activeClass ? " class=\"active\"" : String.Empty,
                a
            //htmlHelper.ActionLink(text, action, controller, routeValues, null)
            );
            return new MvcHtmlString(li);
        }

        private static MvcHtmlString LiActionLink(HtmlHelper htmlHelper, string text, string action, string controller, bool activeClass, object routeValues = null)
        {
            var li = String.Format("<li{0}>{1}</li>",
                activeClass ? " class=\"active\"" : String.Empty,
                htmlHelper.ActionLink(text, action, controller, routeValues, null)
            );
            return new MvcHtmlString(li);
        }
    }
}