﻿using AutoMapper;
using System.Web.Mvc;
using Xtoman.Framework.Core;

namespace Xtoman.Admin
{
    public static class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.ConfigureAutoMapper();
                cfg.CreateMap<ViewContext, ViewContext>().ForMember(p => p.ViewData, p => p.Ignore());
            });
        }
    }
}