﻿$(document).ajaxStart(function () {
    $('#loading').fadeIn();
});
$(document).ajaxComplete(function () {
    $('#loading').fadeOut();
});
$(document).ajaxError(function (event, jqxhr, settings, thrownError) {
    alertFailed("خطا! " + jqxhr.status, "خطای " + jqxhr.status+ " در هنگام ارسال اطلاعات!");
});

function alertSuccess(text) {
    $.Notification.notify('success', 'bottom left', 'عملیات موفق', text);
}

function alertSuccess(title, text) {
    $.Notification.notify('success', 'bottom left', title, text);
}

function alertInfo(text) {
    $.Notification.notify('info', 'bottom left', 'توجه', text);
}

function alertInfo(title, text) {
    $.Notification.notify('info', 'bottom left', title, text);
}

function alertWarning(text) {
    $.Notification.notify('warn', 'bottom left', 'اخطار', text);
}

function alertWarning(title, text) {
    $.Notification.notify('warning', 'bottom left', title, text);
}

function alertFailed(text) {
    $.Notification.notify('error', 'bottom left', 'خطا!', text);
}

function alertFailed(title, text) {
    $.Notification.notify('error', 'bottom left', title, text);
}

function loadAjax(url, containerSelector) {
    $.ajax({
        url: url,
        type: "POST",
        success: function (data, textStatus, jqXHR) {
            $(containerSelector).html(data);
        }
    });
}

function doAjaxModal(postData, url) {
    $.ajax({
        url: url,
        type: "POST",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(postData),
        success: function (data, textStatus, jqXHR) {
            if (data.Status) {
                showModal(data.Text);
            }
        }
    });
}

function doAjax(postData, url, handleData) {
    if (typeof handleData === 'undefined') { handleData = function (i) { } }
    $.ajax({
        url: url,
        type: "POST",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(postData),
        success: function (data, textStatus, jqXHR) {
            if (data.Status) {
                alertSuccess(data.Text);
            }
            else {
                alertFailed(data.Text);
            }
            handleData(data);
        }
    });
}

function doAjaxWithToken(postData, url, token, handleData) {
    if (typeof handleData === 'undefined') { handleData = function (i) { } }
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(postData),
        headers: {
            __RequestVerificationToken: token
        },
        cache: false,
        processData: false,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data, textStatus, jqXHR) {
            if (data.Status) {
                alertSuccess(data.Text);
            }
            else {
                alertFailed(data.Text);
            }
            handleData(data);
        }
    });
}


function doAjaxMessage(postData, url, successMessage, handleData) {
    if (typeof handleData === 'undefined') { handleData = function (i) { } }
    $.ajax({
        url: url,
        type: "POST",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(postData),
        success: function (data, textStatus, jqXHR) {
            if (data.Status) {
                alertSuccess(successMessage);
            }
            else {
                alertFailed(data.Text);
            }
            handleData(data);
        }
    });
}

function showModal(modalBody) {
    var showModal =
        $('<div class="modal fade" id="modal-show">' +
            '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                    '<div class="modal-body">' +
                        modalBody +
                    '</div>' +
                    '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-danger" data-dismiss="modal">بستن</button>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '</div>');

    showModal.modal('show');
    showModal.on('hidden.bs.modal', function (e) {
        showModal.modal('dispose');
        showModal = "";
    });
}

function confirmDialog(question, callback) {
    var confirmModal =
    $('<div class="modal fade" id="modal-confirm-dialog" role="dialog" aria-hidden="true" aria-labelledby="Approve">' +
        '<div class="modal-dialog">' +
            '<div class="modal-content">' +
                '<div class="modal-body">' +
                    '<div class="row">' +
                        '<div class="col-12">' +
                            '<h2>' + question + '</h2>' +
                            '<a id="okButton" class="btn btn-lg btn-primary btn-embossed btn-block">بله مطمئنم</a>' +
                            '<a class="btn btn-lg btn-danger btn-block" data-dismiss="modal">نه! مطمئن نیستم</button>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>' +
    '</div>');

    confirmModal.find('#okButton').click(function (event) {
        callback();
        confirmModal.modal('hide');
    });

    confirmModal.modal('show');

    confirmModal.on('hidden.bs.modal', function () {
        $(this).data('bs.modal', null).remove();
    });
};

function confirmWithInputDialog(question, inputlabelText, inputNullError, callback) {
    var confirmModal =
        $('<div class="modal fade" id="modal-confirm-with-input" role="dialog" aria-hidden="true" aria-labelledby="Confirm">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="col-12">' +
            '<h2>' + question + '</h2>' +
            '<div class="form-group">' +
            '<label for="customInput">' + inputlabelText+'</label>' +
            '<input type="text" id="customInput" name="customInput" class="form-control"/>' +
            '</div>' +
            '<div class="col-12">' +
            '<a id="okButton" class="btn btn-lg btn-primary btn-embossed btn-block">بله مطمئنم</a>' +
            '<a class="btn btn-lg btn-danger btn-block" data-dismiss="modal">نه! مطمئن نیستم</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>');

    confirmModal.find('#okButton').click(function (event) {
        var theInput = confirmModal.find($('#customInput')).val();
        console.log(theInput);
        if (theInput === null || theInput === "") {
            alertInfo(inputNullError);
            return;
        }
        callback(theInput);
        confirmModal.modal('hide');
    });
    confirmModal.modal('show');

    confirmModal.on('hidden.bs.modal', function () {
        $(this).data('bs.modal', null).remove();
    });
};

var refreshMinutes = 10;
var isPaused = false;
var isGoing = false;
var countDownInterval;
var refreshInterval;
function initListRefresh() {
    var playOrPauseInterval = window.setInterval(function () {
        if (isPaused) {
            clearInterval(countDownInterval);
            clearInterval(refreshInterval);
            isGoing = false;
        } else {
            if (!isGoing) {
                refresh();
                refreshInterval = setInterval(refresh, refreshMinutes * 60000);
            }
        }
    }, 1000);
}

function refresh() {
    countDown();
    $('#search-form').submit();
    isGoing = true;
}
function addMinutes(date, minutes) {
    return new Date(date.getTime() + minutes * 60000);
}

function pauseRefresh() {
    isPaused = true;
    $('#pauseRefresh').hide();
    $('#playRefresh').show();
}

function playRefresh() {
    isPaused = false;
    $('#pauseRefresh').show();
    $('#playRefresh').hide();
}

function countDown() {
    // Set the date we're counting down to
    var countDownDate = addMinutes(new Date(), refreshMinutes).getTime();
    // Update the count down every 1 second
    clearInterval(countDownInterval);
    countDownInterval = setInterval(function () {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        //var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        //var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        document.getElementById("countdowner").innerHTML =
            //days + "d " + hours + "h " +
            minutes + "m " + seconds + "s ";

        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "EXPIRED";
        }
    }, 1000);
}

function insertAtCursor(myField, myValue) {
    //IE support
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = myValue;
    }
    //MOZILLA and others
    else if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos)
            + myValue
            + myField.value.substring(endPos, myField.value.length);
        myField.selectionStart = startPos + myValue.length;
        myField.selectionEnd = startPos + myValue.length;
    } else {
        myField.value += myValue;
    }
}