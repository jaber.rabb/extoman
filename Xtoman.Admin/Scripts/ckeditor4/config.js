﻿/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
//CKEDITOR.stylesSet.add('my_styles', [
//    // Block-level styles
//    { name: 'Blue Title', element: 'h2', styles: { 'color': 'Blue' } },
//    { name: 'Red Title', element: 'h3', styles: { 'color': 'Red' } },
//    // Inline styles
//    { name: 'CSS Style', element: 'span', attributes: { 'class': 'my_style' } },
//    { name: 'Marker: Yellow', element: 'span', styles: { 'background-color': 'Yellow' } }
//]);
CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.skin = 'office2013';
    config.toolbar = [
        { items: ["Source", "Maximize", "NewPage", "Print", "Preview"] },
        { items: ["Cut", "Copy", "Paste", "PasteText", "PasteFromWord", "-", "Undo", "Redo"] },
        { items: ["Find", "Replace"] },
        { items: ["Link", "Unlink", "Anchor", "-", "Image", "Table", "HorizontalRule"] },
        { items: ["NumberedList", "BulletedList", "-", "Outdent", "Indent", "Blockquote"] },
        { items: ["JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock", "-", "BidiLtr", "BidiRtl"] },
        { items: ["Bold", "Italic", "Underline", "Strike", "Subscript", "Superscript", "-", "RemoveFormat"] },
        { items: ["Styles", "Format", /*"Font", "FontSize", */"TextColor", "BGColor"] }
    ];
    config.extraPlugins = 'dragresize';
    config.contentsLangDirection = 'rtl';
    config.enterMode = CKEDITOR.ENTER_P;
    //config.font_names = 'میترا (B Mitra)/B Mitra;' + 'یکان (B Yekan)/B Yekan;' + 'تاهما (Tahoma)/Tahoma;';
    config.contentsCss = '/scripts/ckeditor4/ckeditor.css';
    //config.removePlugins = 'htmldataprocessor';
    //config.allowedContent =
    //'h1 h2 h3 h4 p blockquote strong em;' +
    //'a[!href];' +
    //'img(left,right)[!src,alt,width,height];';
    //config.format_tags = 'p;h1;h2;h3;h4;h5;h6;pre;address;div';
    //config.format_tags = 'p;h1;h2;pre;div';
    //config.stylesSet = 'my_styles';
    config.protectedSource.push(/<\?[\s\S]*?\?>/g);                                           // PHP code
    config.protectedSource.push(/<%[\s\S]*?%>/g);                                             // ASP code
    config.protectedSource.push(/(<asp:[^\>]+>[\s|\S]*?<\/asp:[^\>]+>)|(<asp:[^\>]+\/>)/gi);  // ASP.Net code
};
