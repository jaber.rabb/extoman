﻿$(document).ready(function () {
    CKEDITOR.on('instanceReady', function (evt) {
        checkSeo();
    });
    CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        if (dialogName === 'table') {
            var addCssClass = dialogDefinition.getContents('advanced').get('advCSSClasses');
            addCssClass['default'] = 'table table-hover';
        }
        if (dialogName == 'image') {
            var advanced = dialogDefinition.getContents('advanced');
            var styles = advanced.get('txtGenClass');
            styles['default'] = 'img-responsive rounded ';
        }
    });

    for (var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].on('blur', function (e) {
            if (e.editor.getData() != $("#div_" + e.editor.name).html()) {
                $("#div_" + e.editor.name).html(e.editor.getData());
                $("#" + e.editor.name).change();
            }
        });
    }

    $(".seoValueNotifier, textarea.CkEditor").change(function () {
        checkSeo();
    });

    function checkSeo() {
        $('.seoResult').html('<i id="loading-list" class="fa fa fa-fw fa-spinner fa-pulse fa-lg"></i> <span>در حال بررسی وضعیت سئو</span>');
        var form = $('.seoResult').closest("form");
        var data = form.find(":input").serializeArray();
        //replace name of array keys for solve greater than index of 0
        for (i = 0; i < data.length; i++) {
            data[i].name = data[i].name.replace(/\d/i, "0")
        };
        $.ajax({
            url: checkSeoUrl,
            type: "post",
            data: data,
            global: false,
            success: function (result) {
                form.find(".seoResult").html(result);
            }
        });
    }

    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $("#MediaUrl_jasnyUpload").fileinput()
        .on("change.bs.fileinput", function () {
            //$('<div class="jasny-loading default1" ></div>').insertAfter($(this).find(".fileinput-preview img"));
            var imgPath = $("#MediaUrl");
            imgPath.val('');
            var mediaId = $('#MediaId');
            mediaId.val('');
            var data = new FormData();
            data.append('file', $("#MediaUrl_jasnyFile")[0].files[0]);
            data.append('alt', $('#Name').val().trim() + " (" + $('#AlternativeName').val().trim() + ")");
            data.append('type', 'currency');
            $.ajax({
                url: uploadUrl,
                type: 'POST',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    //$("#MediaUrl_jasnyUpload ").find(".jasny-loading.default1").remove();
                    imgPath.val(response.Url);
                    mediaId.val(response.MediaId).valid();
                }
            });
        }).on("clear.bs.fileinput", function () {
            $("#MediaUrl").val('').valid();
        });
});