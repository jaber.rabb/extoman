﻿$(document).ready(function () {
    $('#ParentCategoryId').select2();

    CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        if (dialogName === 'table') {
            var addCssClass = dialogDefinition.getContents('advanced').get('advCSSClasses');
            addCssClass['default'] = 'table table-hover';
        }
        if (dialogName == 'image') {
            var advanced = dialogDefinition.getContents('advanced');
            var styles = advanced.get('txtGenClass');
            styles['default'] = 'img-responsive rounded ';
        }
    });

    for (var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].on('blur', function (e) {
            if (e.editor.getData() != $("#div_" + e.editor.name).html()) {
                $("#div_" + e.editor.name).html(e.editor.getData());
                $("#" + e.editor.name).change();
            }
        });
    }

    metaKeywordsInputElement.tagsInput({ width: 'auto', defaultText: "Meta keywords" });

    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
});