﻿$(document).ready(function () {
    $('#CategoriesId').select2();
    CKEDITOR.on('instanceReady', function (evt) {
        checkSeo();
    });
    CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        if (dialogName === 'table') {
            var addCssClass = dialogDefinition.getContents('advanced').get('advCSSClasses');
            addCssClass['default'] = 'table table-hover';
        }
        if (dialogName == 'image') {
            var advanced = dialogDefinition.getContents('advanced');
            var styles = advanced.get('txtGenClass');
            styles['default'] = 'img-responsive rounded ';
        }
    });

    for (var i in CKEDITOR.instances) {
        CKEDITOR.instances[i].on('blur', function (e) {
            if (e.editor.getData() != $("#div_" + e.editor.name).html()) {
                $("#div_" + e.editor.name).html(e.editor.getData());
                $("#" + e.editor.name).change();
            }
        });
    }

    //metaKeywordsInputElement.tagsInput({ width: 'auto', defaultText: "Meta keywords" });
    $("input.tag").tagedit({ autocompleteURL: tagsInputAutoCompleteUrl });
    //$('input.tag').tagEditor({ autocomplete: { 'source': tagsInputAutoCompleteUrl, minLength: 3 } });

    $(".seoValueNotifier, textarea.CkEditor").change(function () {
        checkSeo();
    });

    function checkSeo() {
        $('.seoResult').html('<i id="loading-list" class="fa fa fa-fw fa-spinner fa-pulse fa-lg"></i> <span>در حال بررسی وضعیت سئو</span>');
        var form = $(".seoCheckForm .div_tab:first");
        //var form = $(this).closest(".div_tab:first");
        var data = form.find(":input").serializeArray();
        //replace name of array keys for solve greater than index of 0
        for (i = 0; i < data.length; i++) {
            data[i].name = data[i].name.replace(/\d/i, "0")
        };
        $.ajax({
            url: checkSeoUrl,
            type: "post",
            data: data,
            global: false,
            success: function (result) {
                form.find(".seoResult").html(result);
            }
        });
    }

    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $("#ImagePath_jasnyUpload").fileinput()
        .on("change.bs.fileinput", function () {
            //$('<div class="jasny-loading default1" ></div>').insertAfter($(this).find(".fileinput-preview img"));
            var imgPath = $("#ImagePath");
            imgPath.val('');
            var mediaId = $('#MediaId');
            mediaId.val('');
            var data = new FormData();
            data.append('file', $("#ImagePath_jasnyFile")[0].files[0]);
            data.append('alt', $('#Title').val().trim());
            data.append('type', 'post');
            $.ajax({
                url: uploadUrl,
                type: 'POST',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function (jqXHR, settings) {
                    $('#loading').show();
                    document.getElementById("submitBtn").disabled = true;
                },
                success: function (data, textStatus, jqXHR) {
                    //$("#ImagePath_jasnyUpload ").find(".jasny-loading.default1").remove();
                    imgPath.val(data.Url);
                    mediaId.val(data.MediaId).valid();
                },
                complete: function (jqXHR, textStatus) {
                    $('#loading').hide();
                    document.getElementById("submitBtn").disabled = false;
                }
            });
        }).on("clear.bs.fileinput", function () {
            $("#ImagePath").val('').valid();
        });

    if (typeof selectedCatIds != "undefined") {
        $.each(selectedCatIds, function (i, val) {
            $('#CategoriesId option[value="' + val + '"]').attr("selected", "selected");
        })
    }
    $('#CategoriesId').select2();
});