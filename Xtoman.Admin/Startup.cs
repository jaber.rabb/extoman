﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Xtoman.Admin.Startup))]
namespace Xtoman.Admin
{


    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureHangFire(app);
        }
    }
}
