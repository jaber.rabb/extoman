﻿using Hangfire;
using Hangfire.Dashboard;
using Hangfire.StructureMap;
using Microsoft.Owin;
using Owin;
using Xtoman.Framework.DependencyResolution;
using Xtoman.Jobs;
using Xtoman.Utility;

namespace Xtoman.Admin
{
    public partial class Startup
    {
        public void ConfigureHangFire(IAppBuilder app)
        {
            GlobalConfiguration.Configuration
                .UseStructureMapActivator(IoC.Container)
                .UseElmahLogProvider()
                .UseSqlServerStorage("XtomanHangFireConnection");

            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute { Attempts = 3 });

            app.UseHangfireServer();
            app.UseHangfireDashboard("/ControlJobs", new DashboardOptions()
            {
                Authorization = new[] { new HangFireAuthorizationFilter() }
            });

            if (!AppSettingManager.IsLocale)
            {
                var _messageJobs = IoC.Container.GetInstance<MessageJobs>();
                RecurringJob.AddOrUpdate("SendQueuedMessage", () => _messageJobs.SendQueuedMessage(), Cron.MinuteInterval(1));

                var _orderJobs = IoC.Container.GetInstance<OrderJobs>();
                RecurringJob.AddOrUpdate("CancelOutDatedOrders", () => _orderJobs.CancelOutDatedOrders(), Cron.MinuteInterval(2));

                var _transactionJobs = IoC.Container.GetInstance<TransactionJobs>();
                RecurringJob.AddOrUpdate("GetUpdateWithdrawalsTransactionIds", () => _transactionJobs.GetUpdateWithdrawalsTransactionIds(), Cron.MinuteInterval(15));

                var _tradeQueueJobs = IoC.Container.GetInstance<TradeJobs>();
                RecurringJob.AddOrUpdate("CompleteBinanceQueueTrades", () => _tradeQueueJobs.CompleteBinanceQueueTrades(), Cron.MinuteInterval(5));

                var _payIRJobs = IoC.Container.GetInstance<PayIRJobs>();
                RecurringJob.AddOrUpdate("GetUpdateCashoutsStatus", () => _payIRJobs.GetUpdateCashoutsStatus(), Cron.HourInterval(6));

                //var _withdrawJobs = IoC.Container.GetInstance<WithdrawJobs>();
                //RecurringJob.AddOrUpdate("WithdrawForCoinToCoinOrders", () => _withdrawJobs.WithdrawForCoinToCoinOrders(), Cron.MinuteInterval(5));

                // --------- Deprecated --------------
                //RecurringJob.AddOrUpdate("UpdateBinanceWithdrawFee", () => _withdrawJobs.UpdateBinanceWithdrawFee(), Cron.MinuteInterval(5));
                // -----------------------------------

                var _networkJobs = IoC.Container.GetInstance<NetworkJobs>();
                RecurringJob.AddOrUpdate("UpdateCurrencyTypesAndNetworks", () => _networkJobs.UpdateCurrencyTypesAndNetworks(), Cron.MinuteInterval(5));
            }
        }
    }

    public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            // In case you need an OWIN context, use the next line, `OwinContext` class
            // is the part of the `Microsoft.Owin` package.
            var owinContext = new OwinContext(context.GetOwinEnvironment());

            var isAuth = owinContext.Authentication.User.Identity.IsAuthenticated;
            if (isAuth)
                return owinContext.Authentication.User.IsInRole("Admin");

            return isAuth;
        }
    }
}