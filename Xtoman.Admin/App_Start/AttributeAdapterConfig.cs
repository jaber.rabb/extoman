﻿using Xtoman.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Xtoman.Admin
{
    public static class AttributeAdapterConfig
    {
        public static void Register()
        {
            //ModelMetadataProviders.Current = new CustomModelMetadataProvider();
            //ModelValidatorProviders.Providers.Add(new CustomMetadataValidationProvider());

            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RequiredAttribute), typeof(CustomRequiredAttributeAdapter));
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(StringLengthAttribute), typeof(CustomStringLengthAttributeAdapter));
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(MinLengthAttribute), typeof(CustomMinLengthAttributeAdapter));
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(MaxLengthAttribute), typeof(CustomMaxLengthAttributeAdapter));
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RangeAttribute), typeof(CustomRangeAttributeAdapter));
        }
    }
}