﻿using System;
using System.Web.Mvc;
using Xtoman.Framework.Mvc;

namespace Xtoman.Admin
{
    public static class ModelBinderConfig
    {
        public static void Register(ModelBinderDictionary binders)
        {
            binders.Add(typeof(string), new StringModelBinder());
            binders.Add(typeof(int), new IntModelBinder());
            binders.Add(typeof(int?), new IntModelBinder());
            binders.Add(typeof(decimal), new DecimalModelBinder());
            binders.Add(typeof(TimeSpan), new SeconsToTimeSpanModelBinder());
            binders.Add(typeof(TimeSpan?), new SeconsToTimeSpanModelBinder());
            binders.Add(typeof(BaseViewModel), new BaseViewModelBinder());
        }
    }
}