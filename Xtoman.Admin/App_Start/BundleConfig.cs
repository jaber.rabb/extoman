﻿using System.Web;
using System.Web.Optimization;

namespace Xtoman.Admin
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            #region Scripts
            bundles.Add(new ScriptBundle("~/js/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/js/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/js/jqueryajax").Include(
                        "~/Scripts/jquery.unobtrusive-ajax*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/js/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/js/main").Include(
                        "~/Scripts/popper.min.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/detect.js",
                        "~/Scripts/fastclick.js",
                        "~/Scripts/jquery.slimscroll.js",
                        "~/Scripts/jquery.blockUI.js",
                        "~/Scripts/waves.js",
                        "~/Scripts/wow.min.js",
                        "~/Scripts/jquery.nicescroll.js",
                        "~/Scripts/jquery.scrollTo.min.js",
                        "~/Scripts/jquery.app.js",
                        "~/Scripts/jquery.core.js",
                        "~/Scripts/general.js"));

            bundles.Add(new ScriptBundle("~/js/plugins").Include(
                      "~/Plugins/switchery/switchery.min.js",
                      "~/Plugins/notifyjs/notify.min.js",
                      "~/Plugins/notifyjs/styles/metro/notify-metro.js",
                      "~/Plugins/select2/dist/js/select2.min.js"));

            bundles.Add(new ScriptBundle("~/js/magnifierjs").Include(
                      "~/Plugins/Magnifierjs/Magnifier.js"));

            #endregion

            #region Styles
            bundles.Add(new StyleBundle("~/css/main").Include(
                      "~/Content/fonts/fa-fonts.css",
                      "~/Content/css/bootstrap.min.css",
                      "~/Content/css/style.css"));

            bundles.Add(new StyleBundle("~/css/icon").Include(
                      "~/Content/css/icons.css"));

            bundles.Add(new StyleBundle("~/css/plugins").Include(
                      "~/Plugins/switchery/switchery.min.css",
                      "~/Plugins/select2/dist/css/select2.min.css"));

            bundles.Add(new StyleBundle("~/Content/pagedlist").Include(
                      "~/Content/css/pagedlist.css"));

            bundles.Add(new StyleBundle("~/css/magnifierjs").Include(
                      "~/Plugins/Magnifierjs/magnifier.css"));
            #endregion
        }
    }
}
