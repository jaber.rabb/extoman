﻿using System.Web.Mvc;
using Xtoman.Framework;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers
{
    [RolePermissionAuthorize]
    public class BaseAdminController : BaseController
    {
    }
}