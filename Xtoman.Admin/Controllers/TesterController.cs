﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Framework;
using Xtoman.Service;
using Xtoman.Utility;
using static Xtoman.Utility.CommonUtility;

namespace Xtoman.Admin.Controllers
{
    [AllowAnonymous]
    public class TesterController : BaseController
    {
        private readonly IUserBankCardService _userBankCardService;
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly IMessageManager _messageManager;
        public TesterController(
            IUserBankCardService userBankCardService,
            IExchangeOrderService exchangeOrderService,
            IMessageManager messageManager)
        {
            _userBankCardService = userBankCardService;
            _exchangeOrderService = exchangeOrderService;
            _messageManager = messageManager;
        }

        public async Task<ActionResult> FixBank()
        {
            var sellOrders = await _exchangeOrderService.TableNoTracking
                .Where(p => p.Exchange.ToCurrency.Type == ECurrencyType.Shetab && p.PaymentStatus == PaymentStatus.Paid)
                .Select(p => new { p.ReceiveAddress, p.ReceiveMemoTagPaymentId, p.UserId })
                .ToListAsync();

            var result = $"Sell orders: {sellOrders.Count}";
            var existedCount = 0;
            var notExistedCount = 0;
            foreach (var bankAccountInOrder in sellOrders)
            {
                var cardNumberInOrder = Regex.Match(bankAccountInOrder.ReceiveAddress.Replace(" ", "").Replace("-", ""), @"\d+").Value;
                var bankNameInOrder = new String(bankAccountInOrder.ReceiveAddress.Where(Char.IsLetter).ToArray());
                var shebaNumberInOrder = bankAccountInOrder.ReceiveMemoTagPaymentId;

                var userBankCards = await _userBankCardService.Table
                    .Where(p => p.UserId == bankAccountInOrder.UserId)
                    .ToListAsync();

                var isShebaNumber = shebaNumberInOrder.HasValue() && (shebaNumberInOrder.Length == 24 || shebaNumberInOrder.Length == 26);

                if (userBankCards.Any(x => x.CardNumber == cardNumberInOrder))
                {
                    existedCount++;
                    var bankCard = userBankCards.Where(p => p.CardNumber == cardNumberInOrder).FirstOrDefault();
                    if (!bankCard.BankName.HasValue() || !bankCard.ShebaNumber.HasValue() || !bankCard.BankAccountNumber.HasValue())
                    {
                        bankCard.BankName = bankNameInOrder;
                        if (isShebaNumber)
                        {
                            bankCard.ShebaNumber = shebaNumberInOrder;
                        }
                        else
                        {
                            bankCard.BankAccountNumber = shebaNumberInOrder;
                        }
                        await _userBankCardService.UpdateAsync(bankCard);
                    }
                }
                else
                {
                    notExistedCount++;
                    var userBankCard = new UserBankCard()
                    {
                        CardNumber = cardNumberInOrder,
                        BankName = bankNameInOrder,
                        BankAccountNumber = isShebaNumber ? "" : shebaNumberInOrder,
                        ShebaNumber = isShebaNumber ? shebaNumberInOrder : "",
                        UserId = bankAccountInOrder.UserId
                    };
                    await _userBankCardService.InsertAsync(userBankCard);
                }
            }
            result += $" | Not existed count: {notExistedCount} | Existed count: {existedCount}";
            return Content(result);
        }


        public async Task<ActionResult> FixOld()
        {
            var userBankCards = await _userBankCardService.Table.Where(p => p.BankName != null || p.BankAccountNumber != null || p.ShebaNumber != null)
                .ToListAsync();

            foreach (var item in userBankCards)
            {
                if (!item.ShebaNumber.HasValue() && item.BankAccountNumber.HasValue() && item.BankAccountNumber.Contains("IR"))
                {
                    item.BankAccountNumber = item.BankAccountNumber.Trim().Replace(" ", "").Replace("-", "");
                    item.ShebaNumber = item.BankAccountNumber;
                    if (item.ShebaNumber.Length > 26)
                    {
                        if (item.ShebaNumber.StartsWith("IR"))
                        {
                            item.ShebaNumber = item.ShebaNumber.RemoveRight(item.ShebaNumber.Length - 26);
                            item.BankAccountNumber = item.BankAccountNumber.RemoveLeft(26);
                        }
                        else
                        {
                            item.ShebaNumber = item.ShebaNumber.RemoveLeft(item.ShebaNumber.Length - 26);
                            item.BankAccountNumber = item.BankAccountNumber.RemoveRight(26);
                        }

                    }
                }

                if (!item.BankName.HasValue() && item.BankAccountNumber.HasValue() && !item.BankAccountNumber.Any(char.IsDigit))
                {
                    item.BankName = item.BankAccountNumber.Trim();
                }

                if (item.BankName == item.BankAccountNumber)
                {
                    item.BankAccountNumber = null;
                }

                if (item.CardNumber.HasValue() && item.CardNumber.Length != 16 && (!item.BankAccountNumber.HasValue() || item.BankAccountNumber.Contains("IR")))
                {
                    item.BankAccountNumber = item.CardNumber;
                    item.CardNumber = null;
                }

                if (item.ShebaNumber.HasValue() && item.ShebaNumber.Length == 24)
                {
                    item.ShebaNumber = "IR" + item.ShebaNumber;
                }

                if (item.ShebaNumber.HasValue() && item.ShebaNumber.Contains("IR") && item.ShebaNumber == item.BankAccountNumber)
                {
                    item.BankAccountNumber = null;
                }

                await _userBankCardService.UpdateAsync(item);
            }
            return Content("Done");
        }


        //public ActionResult Index()
        //{
        //    var format = "yyyy/MM/dd";
        //    var root = RootPath();
        //    var parentDir = Directory.GetParent(root.EndsWith("\\") ? root : string.Concat(root, "\\")).Parent.FullName; // It's OK
        //    root = parentDir;
        //    var time = DateTime.Now.ToFaDateTimeZone(format).Replace("/", "\\").TrimEnd('\\');
        //    var path = $"static/upload/{MediaType.Image.ToDisplay()}".Replace("/", "\\").Trim('\\').ToLower() + "\\" + time;

        //    FolderPath(true, path);
        //    var dir = root + "\\" + path;
        //    if (!Directory.Exists(dir))
        //        Directory.CreateDirectory(dir);

        //    var fileNameWithoutExt = "test";
        //    var trimedFileName = TrimFileForSave(fileNameWithoutExt);
        //    var ext = ".jpg";

        //    var index = 2;
        //    if (System.IO.File.Exists(dir + "\\" + trimedFileName + ext))
        //    {
        //        while (System.IO.File.Exists(dir + "\\" + trimedFileName + " ({0})".Format(index) + ext))
        //        {
        //            index++;
        //        }
        //        trimedFileName += " ({0})".Format(index);
        //    }

        //    var absolutePath = dir + "\\" + trimedFileName + ext;
        //    return Json(new FileNameResult
        //    {
        //        AbsolutePathForSave = absolutePath,
        //        RelativePathForWeb = absolutePath.Replace(root, "").Replace("\\", "/")
        //    }, JsonRequestBehavior.AllowGet);
        //}
    }
}