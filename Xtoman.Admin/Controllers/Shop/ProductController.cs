﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-shopping-cart")]
    [DisplayName("محصول ها")]
    public class ProductController : BaseAdminController
    {
        private readonly IProductService _productService;
        private readonly IBrandService _brandService;
        private readonly IProductCategoryService _productCategoryService;
        public ProductController(
            IProductService productService,
            IBrandService brandService,
            IProductCategoryService productCategoryService)
        {
            _productService = productService;
            _brandService = brandService;
            _productCategoryService = productCategoryService;
        }

        public async Task<ActionResult> Index()
        {
            var list = _productService.TableNoTracking;

            var model = await list.ProjectToPagedListAsync<ProductListItemViewModel>(1, 10);

            await PrepareViewBagsAsync();
            return View(model);
        }

        public async Task<ActionResult> Add()
        {
            await PrepareViewBagsAsync();
            return View();
        }

        public async Task<ActionResult> Add(AddProductViewModel model)
        {
            if (ModelState.IsValid)
            {

            }
            return View(model);
        }

        #region Helpers
        private async Task PrepareViewBagsAsync()
        {
            await PrepareBrandsViewBagAsync();
            await PrepareCategoriesViewBagAsync();
        }
        private async Task PrepareCategoriesViewBagAsync()
        {
            var items = new List<CategorySelectListViewModel>();
            var categories = await _productCategoryService.TableNoTracking.Select(p => new { p.Id, p.ParentCategoryId, p.Name }).ToListAsync();
            var parentCategories = categories.Where(p => p.ParentCategoryId == null || p.ParentCategoryId == 0).ToList();
            foreach (var item in parentCategories)
            {
                items.Add(new CategorySelectListViewModel()
                {
                    ParentCategory = item.Name,
                    Id = item.Id,
                    Name = "- " + item.Name
                });
                var childs = categories.Where(p => p.ParentCategoryId == item.Id).ToList();
                foreach (var c in childs)
                {
                    items.Add(new CategorySelectListViewModel()
                    {
                        ParentCategory = item.Name,
                        Id = c.Id,
                        Name = c.Name
                    });
                }
            }
            ViewBag.Categories = new MultiSelectList(items, "Id", "Name", "ParentCategory");
        }
        private async Task PrepareBrandsViewBagAsync()
        {
            var brands = await _brandService.TableNoTracking.Select(p => new { p.Id, p.Name }).ToListAsync();
            ViewBag.Brands = new MultiSelectList(brands, "Id", "Name");
        }
        #endregion
    }
}