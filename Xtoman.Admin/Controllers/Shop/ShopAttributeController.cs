﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Framework.Core;
using Xtoman.Service;
using Xtoman.Utility;
using System.Linq;
using System.Data.Entity;
using System;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using System.ComponentModel;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-paint-roller")]
    [DisplayName("ویژگی های محصول")]
    public class ShopAttributeController : BaseAdminController
    {
        #region Properties
        //private readonly IShopAttributeService _shopAttributeService;
        private readonly IShopAttributeGroupService _shopAttributeGroupService;
        #endregion

        #region Constructor
        public ShopAttributeController(
            IShopAttributeGroupService shopAttributeGroupService)
            //IShopAttributeService shopAttributeService)
        {
            //_shopAttributeService = shopAttributeService;
            _shopAttributeGroupService = shopAttributeGroupService;
        }
        #endregion

        #region Actions
        public async Task<ActionResult> Index(int? page = 1, int? pSize = 10, string term = "")
        {
            int pageNumber = (page ?? 1);
            int pageSize = (pSize ?? 10);
            var list = _shopAttributeGroupService.TableNoTracking;

            if (term.HasValue())
                list = list.Where(p => p.Name.Contains(term));

            var model = await list.ProjectToPagedListAsync<ShopAttributeGroupViewModel>(pageNumber, pageSize);

            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(ShopAttributeGroupViewModel model)
        {
            try
            {
                var entity = model.ToEntity();
                await _shopAttributeGroupService.InsertAsync(entity);
                SuccessNotification($"گروه ویژگی ترکیب با نام {model.Name} از نوع {model.Type.ToDisplay()} اضافه شد.");
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
            }
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int Id)
        {
            var model = await _shopAttributeGroupService.TableNoTracking
                .Include(p => p.ShopAttributes)
                .SingleAsync(p => p.Id == Id);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(ShopAttributeGroupViewModel model)
        {
            try
            {
                var entity = model.ToEntity(await _shopAttributeGroupService.Table.Include(p => p.ShopAttributes).SingleAsync(p => p.Id == model.Id));
                await _shopAttributeGroupService.UpdateAsync(entity);
                SuccessNotification($"گروه ویژگی ترکیب با نام {model.Name} از نوع {model.Type.ToDisplay()} ویرایش شد.");
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
            }
            return View(model);
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> Delete(int Id)
        {
            var result = new ResultModel();
            try
            {
                await _shopAttributeGroupService.DeleteAsync(Id);
                result.Status = true;
                result.Text = $"ویژگی ترکیب {Id} با موفقیت پاک شد.";
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }
            return Json(result);
        }
        #endregion

        #region Helpers

        #endregion
    }
}