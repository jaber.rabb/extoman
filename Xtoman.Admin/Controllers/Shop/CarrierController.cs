﻿using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers.Shop
{
    [MenuIcon("ti-truck")]
    [DisplayName("حامل بار")]
    public class CarrierController : BaseAdminController
    {
        #region Properties
        private readonly ICarrierService _carrierService;
        #endregion

        #region Constructor
        public CarrierController(
            ICarrierService carrierService)
        {
            _carrierService = carrierService;
        }
        #endregion

        #region Actions
        public async Task<ActionResult> Index(int? page = 1, int? pSize = 10, string term = "")
        {
            int pageNumber = (page ?? 1);
            int pageSize = (pSize ?? 10);
            var list = _carrierService.TableNoTracking;
            if (term.HasValue())
                list = list.Where(p => p.Name.Contains(term));

            var model = await list.OrderByDescending(x => x.Id)
                .ProjectToPagedListAsync<CarrierViewModel>(pageNumber, pageSize);

            return View(model);
        }
        #endregion

        #region Helpers

        #endregion
    }
}