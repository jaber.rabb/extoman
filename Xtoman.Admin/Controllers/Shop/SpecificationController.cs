﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-tag")]
    [DisplayName("مشخصه های محصول")]
    public class SpecificationController : BaseAdminController
    {
        #region Properties
        private readonly ISpecificationService _specificationService;
        private readonly ISpecificationGroupService _specificationGroupService;
        private readonly IProductCategoryService _productCategoryService;
        #endregion

        #region Constructor
        public SpecificationController(
            ISpecificationService specificationService,
            ISpecificationGroupService specificationGroupService,
            IProductCategoryService productCategoryService)
        {
            _specificationService = specificationService;
            _specificationGroupService = specificationGroupService;
            _productCategoryService = productCategoryService;
        }
        #endregion

        #region Actions
        public async Task<ActionResult> Index(int? page = 1, int? pSize = 10, string term = "")
        {
            int pageNumber = (page ?? 1);
            int pageSize = (pSize ?? 10);
            var list = _specificationService.TableNoTracking;
            if (term.HasValue())
                list = list.Where(p => p.Name.Contains(term));

            var model = await list.OrderByDescending(x => x.Id)
                .ProjectToPagedListAsync<SpecificationViewModel>(pageNumber, pageSize);

            await PrepareCategoriesViewBagAsync();
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Add()
        {
            await PrepareCategoriesViewBagAsync();
            await PrepareSpecificationGroupsViewBagAsync();
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(SpecificationViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var entity = model.ToEntity();
                    entity.Group = null;
                    entity.GroupId = await GetGroupIdAsync(model.GroupNameField);

                    await _specificationService.InsertAsync(entity);
                    SuccessNotification($"خصوصیت جدید با نام {model.Name} از نوع {model.Type.ToDisplay()} افزوده شد.");
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ErrorNotification(ex.Message);
                }
            }

            await PrepareCategoriesViewBagAsync();
            await PrepareSpecificationGroupsViewBagAsync();
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int Id)
        {
            var model = await _specificationService.TableNoTracking
                .Include(p => p.Group)
                .Include(p => p.Defaults)
                .Include(p => p.Categories)
                .ProjectToSingleAsync<SpecificationViewModel>(p => p.Id == Id);

            await PrepareCategoriesViewBagAsync();
            await PrepareSpecificationGroupsViewBagAsync();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(SpecificationViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var specification = await _specificationService.Table
                        .Include(p => p.Group)
                        .Include(p => p.Defaults)
                        .Include(p => p.Categories)
                        .SingleAsync();

                    var entity = model.ToEntity(specification);
                    entity.Group = null;
                    entity.GroupId = await GetGroupIdAsync(model.GroupNameField);
                    await _specificationService.UpdateAsync(entity);
                    SuccessNotification($"خصوصیت جدید با نام {model.Name} از نوع {model.Type.ToDisplay()} افزوده شد.");
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ErrorNotification(ex.Message);
                }
            }

            await PrepareCategoriesViewBagAsync();
            await PrepareSpecificationGroupsViewBagAsync();
            return View(model);
        }
        #endregion

        #region Helpers
        private async Task<int> GetGroupIdAsync(string groupNameField)
        {
            var groupId = await _specificationGroupService.TableNoTracking
                .Where(p => p.Name.Equals(groupNameField))
                .Select(p => p.Id)
                .DefaultIfEmpty(0)
                .FirstOrDefaultAsync();

            if (groupId == 0)
            {
                var group = new SpecificationGroup() { Name = groupNameField };
                await _specificationGroupService.InsertAsync(group);
                groupId = group.Id;
            }

            return groupId;
        }
        private async Task PrepareCategoriesViewBagAsync()
        {
            var items = new List<CategorySelectListViewModel>();
            var categories = await _productCategoryService.TableNoTracking.Select(p => new { p.Id, p.ParentCategoryId, p.Name }).ToListAsync();
            var parentCategories = categories.Where(p => p.ParentCategoryId == null || p.ParentCategoryId == 0).ToList();
            foreach (var item in parentCategories)
            {
                items.Add(new CategorySelectListViewModel()
                {
                    ParentCategory = item.Name,
                    Id = item.Id,
                    Name = "- " + item.Name
                });
                var childs = categories.Where(p => p.ParentCategoryId == item.Id).ToList();
                foreach (var c in childs)
                {
                    items.Add(new CategorySelectListViewModel()
                    {
                        ParentCategory = item.Name,
                        Id = c.Id,
                        Name = c.Name
                    });
                }
            }
            ViewBag.Categories = new MultiSelectList(items, "Id", "Name", "ParentCategory");
        }
        private async Task PrepareSpecificationGroupsViewBagAsync()
        {
            var groups = await _specificationGroupService.TableNoTracking
                .Select(p => p.Name)
                .ToListAsync();

            ViewBag.Groups = new SelectList(groups, "Name", "Name");
        }
        #endregion
    }
}