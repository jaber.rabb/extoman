﻿using AutoMapper;
using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-flag-alt")]
    [DisplayName("برند ها")]
    public class BrandController : BaseAdminController
    {
        #region Properties
        private readonly IBrandService _brandService;
        #endregion

        #region Constructor
        public BrandController(
            IBrandService brandService)
        {
            _brandService = brandService;
        }
        #endregion

        #region Actions
        public async Task<ActionResult> Index(int? page = 1, int? pSize = 10, string term = "")
        {
            int pageNumber = (page ?? 1);
            int pageSize = (pSize ?? 10);
            var list = _brandService.TableNoTracking;
            if (term.HasValue())
                list = list.Where(p => p.Name.Contains(term) || p.AlternativeName.Contains(term));

            var model = await list.OrderByDescending(x => x.Id)
                .ProjectToPagedListAsync<BrandViewModel>(pageNumber, pageSize);

            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new AddEditBrandViewModel();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Add(AddEditBrandViewModel model)
        {
            await CheckUrlAsync(model);
            if (ModelState.IsValid)
            {
                if (await _brandService.NameIsUniqueAsync(model.Name, model.AlternativeName))
                {
                    await _brandService.InsertAsync(model.ToEntity());
                    SuccessNotification($"برند جدید با نام {model.Name} اضافه شد.");
                    return RedirectToAction("Index");
                }
                else
                    ModelState.AddModelError("", "نام یا نام دوم تکراری است.");
            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            var model = await _brandService.TableNoTracking
                .ProjectToSingleAsync<AddEditBrandViewModel>();

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(AddEditBrandViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (await _brandService.NameIsUniqueAsync(model.Name, model.AlternativeName, model.Id))
                {
                    var entity = model.ToEntity(await _brandService.Table.SingleAsync(p => p.Id == model.Id));
                    await _brandService.UpdateAsync(entity);
                    SuccessNotification($"برند با نام {model.Name} ویرایش شد.");
                    return RedirectToAction("Index");
                }
                else
                    ModelState.AddModelError("", "نام یا نام دوم تکراری است.");
            }

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            var result = new ResultModel();
            try
            {
                await _brandService.DeleteAsync(id);
                result.Status = true;
                result.Text = $"برند ردیف {id} حذف شد.";
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }

            if (Request.IsAjaxRequest())
                return Json(result);

            if (result.Status)
                SuccessNotification(result.Text);
            else
                ErrorNotification(result.Text);

            return RedirectToAction("Index");
        }
        #endregion

        #region Helpers
        private async Task CheckUrlAsync(AddEditBrandViewModel model)
        {
            var result = await _brandService.IsUniqueUrlAsync(model, true);
            if (result.IsUnique == false)
                ModelState.AddModelError("Url", "Url نام آشنا تکراری است.");
            for (int i = 0; i < result.Locales.Count; i++)
            {
                if (result.Locales[i].IsUnique == false)
                    ModelState.AddModelError($"Locales[{i}].Url", $"Url نام آشنا در زبان {result.Locales[i].LanguageName} تکراری است!");
            }
        }
        #endregion

    }
}