﻿using System.ComponentModel;
using System.Web.Mvc;
using Xtoman.Framework.Mvc.Attributes;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-package")]
    [DisplayName("تامین کننده کالا")]
    public class SupplierController : Controller
    {
        #region Properties

        #endregion

        #region Constructor
        public SupplierController()
        {

        }
        #endregion

        #region Actions

        #endregion

        #region Helpers

        #endregion
    }
}