﻿using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-money")]
    [DisplayName("امور مالی")]
    public class FinanceController : BaseAdminController
    {
        #region Properties
        private readonly IBankAccountService _bankAccountService;
        private readonly IManualTradingDepositService _manualTradingDepositService;
        private readonly IUserService _userService;
        private readonly IExchangeOrderService _exchangeOrderService;
        #endregion

        #region Constructor
        public FinanceController(
            IBankAccountService bankAccountService,
            IManualTradingDepositService manualTradingDepositService,
            IUserService userService,
            IExchangeOrderService exchangeOrderService)
        {
            _bankAccountService = bankAccountService;
            _manualTradingDepositService = manualTradingDepositService;
            _userService = userService;
            _exchangeOrderService = exchangeOrderService;
        }
        #endregion

        #region Actions
        public async Task<ActionResult> Index()
        {
            var persianDateFirstDayOfMonth = DateTime.Now.FirstOfPersianMonth();

            var thisMonthExchangesQuery = _exchangeOrderService.TableNoTracking
                .Where(p => p.PaymentStatus == PaymentStatus.Paid)
                .Where(p => p.InsertDate >= persianDateFirstDayOfMonth);

            var model = new FinanceIndexViewModel()
            {
                ThisMonthBuyAmount = await thisMonthExchangesQuery.Where(p => p.Exchange.FromCurrency.Type == ECurrencyType.Shetab).Select(p => p.PayAmount).DefaultIfEmpty(0).SumAsync(),
                ThisMonthSellAmount = await thisMonthExchangesQuery.Where(p => p.Exchange.ToCurrency.Type == ECurrencyType.Shetab).Select(p => p.ReceiveAmount).DefaultIfEmpty(0).SumAsync()
            };
            return View(model);
        }

        //#region Bank Account
        //public async Task<ActionResult> BankAccounts()
        //{
        //    var model = await _bankAccountService.TableNoTracking
        //        .ProjectToListAsync<BankAccountViewModel>();

        //    return View(model);
        //}

        //[HttpGet]
        //public ActionResult AddBankAccount()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public async Task<ActionResult> AddBankAccount(AddEditBankAccountViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            var entity = model.ToEntity();
        //            await _bankAccountService.InsertAsync(entity);
        //            SuccessNotification("حساب بانکی با موفقیت افزوده شد.");
        //        }
        //        catch (Exception ex)
        //        {
        //            ErrorNotification(ex.Message);
        //        }
        //        return RedirectToAction("BankAccounts");
        //    }
        //    return View(model);
        //}

        //[HttpGet]
        //public async Task<ActionResult> EditBankAccount(int id)
        //{
        //    var model = await _bankAccountService.TableNoTracking
        //        .Where(p => p.Id == id)
        //        .ProjectToSingleAsync<AddEditBankAccountViewModel>();

        //    return View(model);
        //}

        //[HttpPost]
        //public async Task<ActionResult> EditBankAccount(AddEditBankAccountViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            var entity = await _bankAccountService.Table.Where(p => p.Id == model.Id).SingleAsync();
        //            entity = model.ToEntity(entity);
        //            await _bankAccountService.UpdateAsync(entity);
        //            SuccessNotification("حساب بانکی با موفقیت ویرایش شد.");
        //        }
        //        catch (Exception ex)
        //        {
        //            ErrorNotification(ex.Message);
        //        }
        //        return RedirectToAction("BankAccounts");
        //    }
        //    return View(model);
        //}

        //[HttpPost]
        //public async Task<ActionResult> DeleteBankAccount(int id)
        //{
        //    try
        //    {
        //        var entity = await _bankAccountService.Table.SingleAsync(p => p.Id == id);
        //        await _bankAccountService.DeleteAsync(entity);
        //        SuccessNotification("حساب بانکی با موفقیت حذف شد.");
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorNotification(ex.Message);
        //    }
        //    return RedirectToAction("BankAccounts");
        //}
        //#endregion

        //#region Manual Trading Deposit
        //public async Task<ActionResult> ManualTradingDeposits(int page = 1, int psize = 30)
        //{
        //    var model = await _manualTradingDepositService.TableNoTracking
        //        .ProjectToPagedListAsync<ManualTradingDepositViewModel>(page, psize);

        //    return View(model);
        //}

        //[HttpGet]
        //public async Task<ActionResult> AddManualTradingDeposit()
        //{
        //    await PrepareContractorUsersSelectListAsync();
        //    return View();
        //}

        //[HttpPost]
        //public async Task<ActionResult> AddManualTradingDeposit(AddEditManualTradingDepositViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            var entity = model.ToEntity();
        //            await _manualTradingDepositService.InsertAsync(entity);
        //            SuccessNotification("سپرده دستی جدید در پلتفرم ترید به سیستم افزوده شد.");
        //        }
        //        catch (Exception ex)
        //        {
        //            ErrorNotification(ex.Message);
        //        }
        //    }
        //    await PrepareContractorUsersSelectListAsync();
        //    return View(model);
        //}

        //[HttpGet]
        //public async Task<ActionResult> EditManualTradingDeposit(int id)
        //{
        //    return View();
        //}
        //#endregion

        #endregion

        #region Helpers
        private async Task PrepareContractorUsersSelectListAsync()
        {
            var contractorUsers = await _userService.TableNoTracking
                .Where(p => p.Roles.Any(x => x.Role.Name == "Contractor"))
                .Select(p => new { p.Id, Name = ((p.FirstName + " " + p.LastName) + " (" + p.Email + ")").Trim() }).ToListAsync();

            ViewBag.Contractors = new SelectList(contractorUsers, "Id", "Name");
        }
        #endregion
    }
}