﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers.Other
{
    [MenuIcon("ti-mobile")]
    [DisplayName("پیامک")]
    public class SMSController : Controller
    {
        public readonly IFarazsmsService _farazSmsService;
        public readonly IUserService _userService;
        public readonly IQueuedMessageService _queuedMessageService;
        public readonly List<string> textFields = new List<string>() { "{نام}", "{آدرس-سایت}", "{تلگرام}" };
        public SMSController(
            IFarazsmsService farazsmsService,
            IUserService userService,
            IQueuedMessageService queuedMessageService)
        {
            _farazSmsService = farazsmsService;
            _userService = userService;
            _queuedMessageService = queuedMessageService;
        }
        public virtual async Task<ActionResult> Index()
        {
            var credit = await _farazSmsService.GetCreditByTomanAsync();
            ViewBag.Credit = credit;
            return View();
        }

        [HttpPost]
        public virtual async Task<ActionResult> Index(SendMassSmsViewModel model)
        {
            try
            {
                var smsCount = 0;
                var users = await _userService.TableNoTracking
                    .Where(p => p.PhoneNumber != null)
                    .Select(p => new
                    {
                        p.FirstName,
                        p.LastName,
                        p.UserName,
                        p.IdentityVerificationStatus,
                        p.PhoneNumber,
                        p.PhoneNumberConfirmed,
                        p.EmailConfirmed,
                        p.TelephoneConfirmationStatus,
                    //p.UserBankCards,
                    p.Orders
                    })
                    //.Include(p => p.Orders)
                    .ToListAsync();

                switch (model.To)
                {
                    case SendSMSTo.AllUsers:
                        break;
                    case SendSMSTo.AllValidatedPhoneNumbers:
                        users = users.Where(p => p.PhoneNumberConfirmed).ToList();
                        break;
                    case SendSMSTo.VerifiedUsersOnly:
                        users = users.Where(p => p.PhoneNumberConfirmed && p.EmailConfirmed
                                                                        && p.IdentityVerificationStatus == VerificationStatus.Confirmed
                                                                        && p.TelephoneConfirmationStatus == VerificationStatus.Confirmed).ToList();
                        break;
                    case SendSMSTo.UsersWithCompletedOrders:
                        users = users.Where(p => p.Orders.Any(x => x.OrderStatus == OrderStatus.Complete)).ToList();
                        break;
                    case SendSMSTo.UsersWithCompletedOrdersAndVerifiedUsers:
                        users = users.Where(p => (p.PhoneNumberConfirmed && p.EmailConfirmed
                                && p.IdentityVerificationStatus == VerificationStatus.Confirmed
                                && p.TelephoneConfirmationStatus == VerificationStatus.Confirmed)
                                || p.Orders.Any(x => x.OrderStatus == OrderStatus.Complete)).ToList();
                        break;
                    case SendSMSTo.VerifiedUsersWithCompletedOrders:
                        users = users.Where(p => p.PhoneNumberConfirmed && p.EmailConfirmed
                                && p.IdentityVerificationStatus == VerificationStatus.Confirmed
                                && p.TelephoneConfirmationStatus == VerificationStatus.Confirmed
                                && p.Orders.Any(x => x.OrderStatus == OrderStatus.Complete)).ToList();
                        break;
                }

                if (textFields.Any(x => model.Text.Contains(x)))
                {
                    ViewBag.IsMass = false;
                    // Send for each user in queue message
                    foreach (var item in users)
                    {
                        if (item.PhoneNumber.HasValue())
                        {
                            await _queuedMessageService.InsertAsync(new QueuedMessage()
                            {
                                Type = QueuedMessageType.SMS,
                                To = item.PhoneNumber,
                                Text = ReplaceTextFields(model.Text, item.FirstName, item.LastName, item.UserName)
                            });
                        }
                        smsCount++;
                    }
                    ViewBag.Result = true;
                }
                else
                {
                    ViewBag.IsMass = true;
                    // Send Mass SMS To
                    var phones = users.Select(p => p.PhoneNumber).ToList();
                    var result = _farazSmsService.SendMassSms(phones, model.Text);
                    ViewBag.Result = result;
                    smsCount = phones.Count;
                }
                ViewBag.SMSCount = smsCount;
            }
            catch (Exception ex)
            {
                ex.LogError();
                ViewBag.Result = false;
            }
            finally
            {
                ViewBag.To = model.To;
                ViewBag.Text = model.Text;
            }
            return View("SmsResult");
        }

        private string ReplaceTextFields(string text, string firstName, string lastName, string userName)
        {
            var name = (firstName + " " + lastName).Trim();
            if (!name.HasValue())
                name = userName;

            foreach (var field in textFields)
            {
                switch (field)
                {
                    case "{نام}":
                        text = text.Replace(field, name);
                        break;
                    case "آدرس سایت":
                        text = text.Replace(field, "www.extoman.co");
                        break;
                    case "تلگرام":
                        text = text.Replace(field, "t.me/extoman");
                        break;
                }
            }

            return text;
        }
    }
}