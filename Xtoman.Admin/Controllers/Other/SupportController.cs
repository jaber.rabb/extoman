﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNet.Identity;
using PagedList;
using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Utility;
using Xtoman.Utility.PostalEmail;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-support")]
    [DisplayName("پشتیبانی")]
    public class SupportController : BaseAdminController
    {
        private readonly ISupportTicketService _supportTicketService;
        private readonly ISupportTicketReplyService _supportTicketReplyService;
        private readonly IUserService _userService;
        private readonly IMessageManager _messageManager;
        public SupportController(
            ISupportTicketService supportTicketService,
            ISupportTicketReplyService supportTicketReplyService,
            IUserService userService,
            IMessageManager messageManager)
        {
            _supportTicketService = supportTicketService;
            _supportTicketReplyService = supportTicketReplyService;
            _userService = userService;
            _messageManager = messageManager;
        }

        public async Task<ActionResult> Index(int? page = 1, SupportDepartment? department = null, TicketStatus? status = null, string term = "")
        {
            var query = _supportTicketService.TableNoTracking;
            if (term.HasValue())
                query = query.Where(p => p.Text.Contains(term) || p.Subject.Contains(term) || p.EmailOrPhoneNumber.Contains(term));

            if (status.HasValue)
            {
                ViewBag.Status = status.ToInt();
                query = query.Where(p => p.Status == status.Value);
            }

            if (department.HasValue)
            {
                ViewBag.Department = department.ToInt();
                query = query.Where(p => p.Department == department.Value);
            }

            //var model = await query
            //    .ProjectToPagedListAsync<SupportTicketListItemViewModel>(p => p.Status, (int)page, 30);

            var pageSize = 30;
            var skip = (page.Value - 1) * pageSize;
            var queryViewModel = query.ProjectTo<SupportTicketListItemViewModel>().OrderBy(x => x.Status).ThenByDescending(x => x.LastDate);
            var totalCount = await query.CountAsync();
            var list = await queryViewModel.Skip(skip).Take(pageSize).ToListAsync();
            var model = new StaticPagedList<SupportTicketListItemViewModel>(list, page.Value, pageSize, totalCount);

            ViewBag.CurrentPage = page;
            return View(model);
        }

        public async Task<ActionResult> Details(int id)
        {
            var model = await _supportTicketService.TableNoTracking
                .Where(p => p.Id == id)
                .ProjectToSingleAsync<SupportTicketViewModel>();

            if (model.UserName.HasValue())
            {
                model.FullName = await _userService.TableNoTracking
                    .Where(p => p.UserName == model.UserName || p.Email == model.EmailOrPhoneNumber || p.PhoneNumber == model.EmailOrPhoneNumber)
                    .Select(p => p.FirstName + " " + p.LastName)
                    .FirstOrDefaultAsync();
            }
            else if (model.InsertUserId.HasValue && model.InsertUserId > 0)
            {
                model.FullName = await _userService.TableNoTracking
                    .Where(p => p.Id == model.InsertUserId.Value)
                    .Select(p => p.FirstName + " " + p.LastName)
                    .FirstOrDefaultAsync();
            }
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> SendReply(int TicketId, SupportTicketReplySubmitViewModel model)
        {
            try
            {
                var entity = model.ToEntity();
                entity.SupportTicketId = TicketId;
                await _supportTicketReplyService.InsertAsync(entity);
                SuccessNotification($"پاسخ تیکت شماره {TicketId} ارسال شد.");

                var ticket = await _supportTicketService.Table.Where(p => p.Id == TicketId).SingleAsync();
                ticket.Status = Domain.Models.TicketStatus.Answered;
                ticket.LastDate = DateTime.UtcNow;
                await _supportTicketService.UpdateAsync(ticket);

                var ticketUrl = $"https://www.extoman.co/Support/Ticket/{ticket.Id}";
                var supportTicketAnswerEmail = new SupportTicketAnswerEmail()
                {
                    From = "no-reply@extoman.com",
                    Subject = $"پاسخ درخواست پشتیبانی با موضوع {ticket.Subject}",
                    To = ticket.EmailOrPhoneNumber,
                    Username = ticket.UserName,
                    ViewName = "SupportTicketAnswer",
                    Fullname = ticket.EmailOrPhoneNumber,
                    TicketSubject = ticket.Subject,
                    TicketText = ticket.Text,
                    TicketUrl = ticketUrl
                };

                var answerUserName = User.Identity.GetUserName();
                var emailBody = supportTicketAnswerEmail.GetMailMessage().Body;
                await _messageManager.SupportTicketAnsweredAsync(emailBody, ticket.Subject, ticketUrl, ticket.EmailOrPhoneNumber, answerUserName);
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
            }
            return RedirectToAction("Index");
        }

        [ChildActionOnly]
        public virtual ActionResult WaitingCount()
        {
            var count = _supportTicketService.TableNoTracking
                .Where(p => p.Status == Domain.Models.TicketStatus.WaitingReply)
                .Count();
            return Content(count.ToString());
        }

        [ChildActionOnly]
        public virtual ActionResult OpenCount()
        {
            var count = _supportTicketService.TableNoTracking
                .Where(p => p.Status == Domain.Models.TicketStatus.Answered)
                .Count();
            return Content(count.ToString());
        }

        [HttpPost]
        [AjaxOnly]
        public virtual async Task<ActionResult> CloseOne(int id)
        {
            var result = new ResultModel();
            try
            {
                var ticket = await _supportTicketService.Table.SingleAsync(p => p.Id == id);
                ticket.Status = TicketStatus.Closed;

                await _supportTicketService.UpdateAsync(ticket);

                result.Text = $"تیکت بسته شد.";
                result.Status = true;
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }
            return Json(result);
        }

        [HttpPost]
        [AjaxOnly]
        public virtual async Task<ActionResult> Close(TicketStatus? type = null)
        {
            var result = new ResultModel();
            try
            {
                var query = _supportTicketService.Table.Where(p => p.Status != TicketStatus.Closed);
                if (type != null)
                    query = query.Where(p => p.Status == type.Value);

                var tickets = await query.ToListAsync();

                foreach (var ticket in tickets)
                    ticket.Status = TicketStatus.Closed;

                await _supportTicketService.UpdateAsync(tickets);

                result.Text = $"تعداد {tickets.Count} بسته شد.";
                result.Status = true;
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }
            return Json(result);
        }
    }
}