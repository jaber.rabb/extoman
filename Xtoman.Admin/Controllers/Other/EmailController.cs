﻿using AutoMapper;
using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Utility;
using Xtoman.Utility.PostalEmail;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-email")]
    [DisplayName("ایمیل")]
    public class EmailController : BaseAdminController
    {
        private readonly IEmailAccountService _emailAccountService;
        private readonly IQueuedMessageService _queuedMessageService;
        private readonly IUserService _userService;
        public EmailController(
            IEmailAccountService emailAccountService,
            IQueuedMessageService queuedMessageService,
            IUserService userService)
        {
            _emailAccountService = emailAccountService;
            _queuedMessageService = queuedMessageService;
            _userService = userService;
        }

        public virtual async Task<ActionResult> Index()
        {
            var model = await _emailAccountService.TableNoTracking
                .ProjectToListAsync<EmailAccountViewModel>();

            return View(model);
        }

        [HttpGet]
        public virtual ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Add(EmailAccountViewModel model)
        {
            var entity = model.ToEntity();
            await _emailAccountService.InsertAsync(entity);
            SuccessNotification($"{model.FriendlyName} افزوده شد!");
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual async Task<ActionResult> Edit(int id)
        {
            var model = await _emailAccountService.TableNoTracking.SingleAsync(p => p.Id == id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Edit(EmailAccountViewModel model)
        {
            var entity = await _emailAccountService.Table.SingleAsync(p => p.Id == model.Id);
            entity = model.ToEntity(entity);
            await _emailAccountService.UpdateAsync(entity);
            SuccessNotification($"{model.FriendlyName} به روز رسانی شد!");
            return RedirectToAction("Index");
        }

        [HttpPost]
        public virtual async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _emailAccountService.DeleteAsync(_emailAccountService.GetById(id));
                SuccessNotification("حساب ایمیل حذف شد.");
            }
            catch (Exception ex)
            {
                ErrorNotification(ex);
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual ActionResult Send()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Send(SendEmailViewModel model)
        {
            try
            {
                var users = await _userService.TableNoTracking
                    .Select(p => new
                    {
                        FullName = p.FirstName + " " + p.LastName,
                        p.UserName,
                        p.Email
                    })
                .ToListAsync();

                foreach (var item in users)
                {
                    if (item.Email.HasValue())
                    {

                        var text = model.Text;
                        if (model.UseTemplate)
                        {
                            //Use postal mail body
                            var templateEmail = new TemplateEmail()
                            {
                                From = "no-reply@extoman.com",
                                Fullname = item.FullName,
                                Subject = model.Subject,
                                To = item.Email,
                                Username = item.UserName,
                                ViewName = "EmailTemplate",
                                TextHtml = model.Text
                            };
                            text = templateEmail.GetMailMessage().Body;
                        }
                        await _queuedMessageService.InsertAsync(new QueuedMessage()
                        {
                            EmailAccountId = model.EmailAccountId,
                            Subject = model.Subject,
                            Text = text,
                            To = item.Email,
                            ToName = item.FullName,
                            Type = QueuedMessageType.Email
                        });
                    }
                }
                SuccessNotification("ایمیل ها با موفقیت برای ارسال آماده شدند.");
                return View();
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
                return View(model);
            }
        }
    }
}