﻿using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;

namespace Xtoman.Admin.Controllers.Other
{
    [MenuIcon("ti-split-v")]
    [DisplayName("ترید")]
    public class TradeController : BaseAdminController
    {
        private readonly ITradeService _tradeService;
        public TradeController(
            ITradeService tradeService)
        {
            _tradeService = tradeService;
        }


        public async Task<ActionResult> Index(SearchTerms model, int page = 1, int pageSize = 30)
        {
            var query = _tradeService.TableNoTracking;
            var trades = await query
                .ProjectToPagedListAsync<TradeItemViewModel>(page, pageSize);

            return View(trades);
        }
    }
}