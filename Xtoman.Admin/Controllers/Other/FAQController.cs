﻿using PagedList.EntityFramework;
using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-help")]
    [DisplayName("پرسش و پاسخ")]
    public class FAQController : BaseAdminController
    {
        private readonly IFAQService _faqService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizedPropertyService _localizedPropertyService;
        public FAQController(IFAQService faqService, ILanguageService languageService, ILocalizedPropertyService localizedPropertyService)
        {
            _faqService = faqService;
            _languageService = languageService;
            _localizedPropertyService = localizedPropertyService;
        }

        public virtual async Task<ActionResult> Index(int page = 1)
        {
            var model = await _faqService.TableNoTracking
                .Select(p => new FAQViewModel()
                {
                    Answer = p.Answer,
                    GroupName = p.GroupName,
                    Id = p.Id,
                    Question = p.Question
                })
                .ToPagedListAsync(page, 100);
            return View(model);
        }

        public virtual async Task<ActionResult> AddAsync()
        {
            var model = new FAQViewModel();
            await AddLocalesAsync(_languageService, model.Locales);
            return View(model);
        }

        [HttpPost]
        public virtual async Task<ActionResult> Add(FAQViewModel model)
        {
            if (ModelState.IsValid)
            {
                var faq = new FAQ()
                {
                    Answer = model.Answer,
                    Question = model.Question,
                    GroupName = model.GroupName
                };
                await _faqService.InsertAsync(faq);
                if (faq.Id != 0)
                {
                    await _localizedPropertyService.UpdateLocalesAsync(faq, model.Locales);
                    SuccessNotification($"New FAQ with question: \"{faq.Question}\" added successfully.");
                }
                else
                {
                    ErrorNotification("An error occurred while adding new post!");
                }
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public virtual async Task<ActionResult> Edit(int id)
        {
            var faq = await _faqService.GetByIdAsync(id);
            var model = new FAQViewModel()
            {
                Id = faq.Id,
                Answer = faq.Answer,
                GroupName = faq.GroupName,
                Question = faq.Question
            };
            return View(model);
        }

        [HttpPost]
        public virtual async Task<ActionResult> Edit(FAQViewModel model)
        {
            if (ModelState.IsValid)
            {
                var faq = new FAQ()
                {
                    Id = model.Id,
                    Answer = model.Answer,
                    Question = model.Question,
                    GroupName = model.GroupName
                };
                _faqService.Context.MarkAsChanged(faq);
                await _faqService.UpdateAsync(faq);
                await _localizedPropertyService.UpdateLocalesAsync(faq, model.Locales);
                SuccessNotification("FAQ item changes submited.");
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public virtual async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _faqService.DeleteAsync(_faqService.GetById(id));
                SuccessNotification("Deleted");
            }
            catch (Exception ex)
            {
                ErrorNotification(ex);
            }
            return RedirectToAction("Index");
        }
    }
}