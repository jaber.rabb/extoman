﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Service;
using Xtoman.Service.WebServices;
using Xtoman.Service.WebServices.Price.Nerkh;
using System.Linq;
using Xtoman.Domain.Models;
using System.Data.Entity;
using System.Collections.Generic;
using Xtoman.Utility;
using System;
using System.ComponentModel;
using Xtoman.Framework.Mvc.Attributes;

namespace Xtoman.Admin.Controllers.Other
{
    [MenuIcon("ti-ruler-alt-2")]
    [DisplayName("برآورد")]
    public class EstimatorController : BaseAdminController
    {
        private readonly IBinanceService _binanceService;
        private readonly INerkhService _nerkhService;
        private readonly IBalanceManager _balanceManager;
        private readonly IExchangeOrderService _exchangeOrderService;
        public EstimatorController(
            IBinanceService binanceService,
            INerkhService nerkhService,
            IBalanceManager perfectMoneyService,
            IExchangeOrderService exchangeOrderService)
        {
            _binanceService = binanceService;
            _nerkhService = nerkhService;
            _balanceManager = perfectMoneyService;
            _exchangeOrderService = exchangeOrderService;
        }

        public async Task<ActionResult> CoinSoldDollar()
        {
            var binanceAccountInfo = await _binanceService.GetAccountInfoAsync();
            var usdtBalance = binanceAccountInfo.Balances.Where(p => p.Asset == "USDT").Select(p => p.Free).DefaultIfEmpty(0).FirstOrDefault();
            var usdPriceInToman = await _nerkhService.DollarPriceAsync();
            var exchangeOrderCount = 0; // Number of exchange order trade items summed
            var takeCount = 0;
            var takeNumber = 10;
            var summedPercent = 0m; // Percent of summed items 
            var estimatedUsdtPriceInToman = 0m;
            var tomanPaidFor = 0m;
            while (summedPercent < 1m)
            {
                var exchangeOrders = await _exchangeOrderService.TableNoTracking
                    .Where(p => p.PaymentStatus == PaymentStatus.Paid && p.Exchange.FromCurrency.IsFiat == false)
                    .OrderByDescending(p => p.Id)
                    .Skip(takeNumber * takeCount)
                    .Take(takeNumber)
                    .Include(p => p.OrderTrades)
                    .ToListAsync();

                takeCount++;

                foreach (var exchangeOrder in exchangeOrders)
                {
                    if (exchangeOrder != null && exchangeOrder.OrderTrades.Any(x => x.ToCryptoType == ECurrencyType.Tether && x.TradeStatus == TradeStatus.Completed))
                    {
                        var trade = new {
                            TomanAmount = exchangeOrder.Exchange.ToCurrency.Type == ECurrencyType.Shetab ? exchangeOrder.ReceiveAmount : exchangeOrder.ReceiveAmount * usdPriceInToman,
                            USDTAmount = exchangeOrder.OrderTrades.Where(x => x.ToCryptoType == ECurrencyType.Tether).Select(x => x.ToAmount).DefaultIfEmpty(0).FirstOrDefault() };

                        if (trade?.USDTAmount > 0m && trade?.TomanAmount > 0m)
                        {
                            var usdtPrice = trade.TomanAmount / trade.USDTAmount;
                            var percentOfAll = trade.USDTAmount / usdtBalance;

                            var remainingPercent = 1m - summedPercent;

                            if (percentOfAll > remainingPercent)
                            {
                                var percentOfTomanPaidFor = remainingPercent / percentOfAll;
                                tomanPaidFor += (trade.TomanAmount * percentOfTomanPaidFor);
                                percentOfAll = remainingPercent;
                            }
                            else
                            {
                                tomanPaidFor += trade.TomanAmount;
                            }

                            if (percentOfAll > 0m)
                            {
                                exchangeOrderCount++;
                                summedPercent += percentOfAll;
                                estimatedUsdtPriceInToman += (usdtPrice * percentOfAll);
                            }
                        }
                    }
                }
            }

            var result = 
                new {
                    Status = true,
                    USDPrice = usdPriceInToman,
                    USDPriceText = usdPriceInToman.ToPriceWithoutFloat(),
                    USDTPaidPrice = estimatedUsdtPriceInToman,
                    USDPaidPriceText = estimatedUsdtPriceInToman.ToPriceWithoutFloat(),
                    AvailableUSD = usdtBalance,
                    TomanPaidFor = tomanPaidFor,
                    TomanPaidForText = tomanPaidFor.ToPriceWithoutFloat(),
                    OrderCount = exchangeOrderCount
                };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> FiatSoldDollar()
        {
            var perfectBalance = await _balanceManager.GetPerfectUSDBalanceAsync();
            var usdPriceInToman = await _nerkhService.DollarPriceAsync();

            var exchangeOrderCount = 0; // Number of exchange order or trade items summed
            var takeCount = 0;
            var takeNumber = 10;
            var summedPercent = 0m; // Percent of summed items 
            var estimatedUsdtPriceInToman = 0m;
            var tomanPaidFor = 0m;

            while (summedPercent < 1m)
            {
                var exchangeOrders = await _exchangeOrderService.TableNoTracking
                    .Where(p => p.PaymentStatus == PaymentStatus.Paid && p.Exchange.FromCurrency.IsFiat == true && p.Exchange.FromCurrency.Type != ECurrencyType.Shetab)
                    .OrderByDescending(p => p.Id)
                    .Skip(takeNumber * takeCount)
                    .Take(takeNumber)
                    .Include(p => p.OrderTrades)
                    .Include(p => p.Exchange)
                    .Include(p => p.Exchange.ToCurrency)
                    .Include(p => p.Exchange.FromCurrency)
                    .ToListAsync();

                takeCount++;

                foreach (var exchangeOrder in exchangeOrders)
                {
                    var trade = new {
                        TomanAmount = exchangeOrder.Exchange.ToCurrency.Type == ECurrencyType.Shetab ?
                            exchangeOrder.ReceiveAmount :
                            exchangeOrder.OrderTrades.Where(x => x.FromCryptoType == ECurrencyType.Tether).Select(x => x.FromAmount).DefaultIfEmpty(0).FirstOrDefault() * exchangeOrder.DollarPriceInToman,

                        USDTAmount = exchangeOrder.PaidAmount > 0 ? exchangeOrder.PaidAmount : exchangeOrder.PayAmount
                    };

                    if (trade?.USDTAmount > 0m && trade?.TomanAmount > 0m)
                    {
                        var usdtPrice = trade.TomanAmount / trade.USDTAmount;
                        var percentOfAll = trade.USDTAmount / perfectBalance;

                        var remainingPercent = 1m - summedPercent;

                        if (percentOfAll > remainingPercent)
                        {
                            var percentOfTomanPaidFor = remainingPercent / percentOfAll;
                            tomanPaidFor += (trade.TomanAmount * percentOfTomanPaidFor);
                            percentOfAll = remainingPercent;
                        }
                        else
                        {
                            tomanPaidFor += trade.TomanAmount;
                        }

                        if (percentOfAll > 0m)
                        {
                            exchangeOrderCount++;
                            summedPercent += percentOfAll;
                            estimatedUsdtPriceInToman += (usdtPrice * percentOfAll);
                        }
                    }
                }
            }

            var result =
                new
                {
                    Status = true,
                    USDPrice = usdPriceInToman,
                    USDPriceText = usdPriceInToman.ToPriceWithoutFloat(),
                    USDTPaidPrice = estimatedUsdtPriceInToman,
                    USDPaidPriceText = estimatedUsdtPriceInToman.ToPriceWithoutFloat(),
                    AvailableUSD = perfectBalance,
                    TomanPaidFor = tomanPaidFor,
                    TomanPaidForText = tomanPaidFor.ToPriceWithoutFloat(),
                    OrderCount = exchangeOrderCount
                };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}