﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Service;
using Xtoman.Utility;
using System.Linq;
using System.Data.Entity;
using Xtoman.Domain.Models;
using Xtoman.Admin.ViewModels;
using System.Collections.Generic;
using Xtoman.Framework.Mvc.Attributes;
using System.ComponentModel;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-stats-up")]
    [DisplayName("گزارش")]
    public class ReportController : BaseAdminController
    {
        private readonly IExchangeOrderService _exchangeOrderService;
        public ReportController(IExchangeOrderService exchangeOrderService)
        {
            _exchangeOrderService = exchangeOrderService;
        }

        public virtual async Task<ActionResult> Index()
        {
            var fromDate = DateTime.Now.FirstOfPersianMonth();
            var toDate = DateTime.Now.LastOfPersianMonth();
            var monthDays = DateTime.Now.MonthDays();

            var exchangeOrders = await _exchangeOrderService.TableNoTracking.Where(p => p.OrderStatus == OrderStatus.Complete && p.InsertDate >= fromDate && p.InsertDate < toDate).ToListAsync();
            var exchangeBuyOrders = exchangeOrders.Where(p => p.Exchange.FromCurrency.Type == ECurrencyType.Shetab).ToList();
            var exchangeSellOrders = exchangeOrders.Where(p => p.Exchange.ToCurrency.Type == ECurrencyType.Shetab).ToList();

            var totalNumberOfOrdersInMonth = exchangeOrders.Count;
            var totalNumberOfBuyOrdersInMonth = exchangeBuyOrders.Count;
            var totalNumberOfSellOrdersInMonth = exchangeSellOrders.Count;

            var totalBuyOrdersAmountInToman = exchangeBuyOrders.Select(p => p.PayAmount).Sum();
            var totalSellOrdersAmountInToman = exchangeSellOrders.Select(p => p.ReceiveAmount).Sum();

            var exchangeBuyOrdersCountAndAmountPerDay = new List<ReportChartData>();

            for (int i = 0; i < monthDays; i++)
            {
                var dayStartDate = fromDate.AddDays(i);
                var dayEndDate = fromDate.AddDays(i + 1);
                var exchangeOrdersOfDay = exchangeOrders.Where(p => p.InsertDate >= dayStartDate && p.InsertDate < dayEndDate).ToList();
                var exchangeBuyOrdersOfDay = exchangeOrders.Where(p => p.Exchange.FromCurrency.Type == ECurrencyType.Shetab).ToList();
                var exchangeSellOrdersOfDay = exchangeOrders.Where(p => p.Exchange.ToCurrency.Type == ECurrencyType.Shetab).ToList();

                exchangeBuyOrdersCountAndAmountPerDay.Add(new ReportChartData() {
                    BuyCount = exchangeBuyOrdersOfDay.Count,
                    BuyAmount = exchangeBuyOrdersOfDay.Select(p => p.PayAmount).DefaultIfEmpty(0).Sum(),
                    SellCount = exchangeSellOrdersOfDay.Count,
                    SellAmount = exchangeSellOrders.Select(p => p.ReceiveAmount).DefaultIfEmpty(0).Sum()
                });
            }

            var model = new ReportIndexViewModel()
            {
                TotalNumberOfOrdersInMonth = totalNumberOfOrdersInMonth,
                TotalNumberOfBuyOrdersInMonth = totalNumberOfBuyOrdersInMonth,
                TotalNumberOfSellOrdersInMonth = totalNumberOfSellOrdersInMonth,
                TotalBuyOrdersAmountInToman = totalBuyOrdersAmountInToman,
                TotalSellOrdersAmountInToman = totalSellOrdersAmountInToman,
                FromDate = fromDate,
                ToDate = toDate,
                MonthName = DateTime.Now.PersianMonthName()
            };

            return View(model);
        }
    }
}