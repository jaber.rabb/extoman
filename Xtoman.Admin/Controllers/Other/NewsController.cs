﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-rss-alt")]
    [DisplayName("خبرها")]
    public class NewsController : BaseAdminController
    {
        private readonly INewsService _newsService;
        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }
        public virtual async Task<ActionResult> Index(int? page = 1, string term = "", int? enabled = null)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var list = _newsService.TableNoTracking;
            if (enabled == 1)
                list = list.Where(p => p.PublishDate != null);
            else if (enabled == 2)
                list = list.Where(p => p.PublishDate == null || p.PublishDate >= DateTime.UtcNow);

            if (string.IsNullOrWhiteSpace(term) == false)
                list = list.Where(p => p.Title.Contains(term) || p.Summary.Contains(term) || p.Url.Contains(term));

            var model = await list.OrderByDescending(x => x.PublishDate).ThenByDescending(x => x.Id)
                .ProjectToPagedListAsync<NewsViewModel>(pageNumber, pageSize);

            return View(model);
        }
    }
}