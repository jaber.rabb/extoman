﻿using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Settings;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-settings")]
    [DisplayName("تنظیم ها")]
    public class SettingController : BaseAdminController
    {
        private readonly BlogSettings _blogSettings;
        private readonly CacheSettings _cacheSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MessageSettings _messageSettings;
        private readonly PaymentSettings _paymentSettings;
        private readonly CommentSettings _commentSettings;
        private readonly ISettingService _settingService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly ILanguageService _languageService;
        public SettingController(
            CacheSettings cacheSettings,
            LocalizationSettings localizationSettings,
            MessageSettings messageSettings,
            PaymentSettings paymentSettings,
            BlogSettings blogSettings,
            CommentSettings commentSettings,
            IEmailAccountService emailAccountService,
            ILanguageService languageService,
            ISettingService settingService)
        {
            _cacheSettings = cacheSettings;
            _localizationSettings = localizationSettings;
            _messageSettings = messageSettings;
            _paymentSettings = paymentSettings;
            _blogSettings = blogSettings;
            _commentSettings = commentSettings;
            _settingService = settingService;
            _emailAccountService = emailAccountService;
            _languageService = languageService;
        }
        public async Task<ActionResult> Index()
        {
            var model = new SettingViewModel()
            {
                BlogSettings = _blogSettings,
                CacheSettings = _cacheSettings,
                LocalizationSettings = _localizationSettings,
                MessageSettings = _messageSettings,
                PaymentSettings = _paymentSettings,
                CommentSettings = _commentSettings
            };
            await PrepareViewModel(model);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Index(SettingViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _settingService.SaveSettingAsync(model.BlogSettings);
                await _settingService.SaveSettingAsync(model.CacheSettings);
                await _settingService.SaveSettingAsync(model.LocalizationSettings);
                await _settingService.SaveSettingAsync(model.MessageSettings);
                await _settingService.SaveSettingAsync(model.PaymentSettings);
                await _settingService.SaveSettingAsync(model.CommentSettings);
                SuccessNotification("تغییرات ذخیره شد!");
            }
            await PrepareViewModel(model);
            return View(model);
        }

        #region Helpers
        private async Task PrepareViewModel(SettingViewModel viewmodel)
        {
            viewmodel.EmailAccounts = await _emailAccountService.TableNoTracking
                .Select(p => new { p.Id, Name = p.DisplayName + " - " + p.Email })
                .ToDictionaryAsync(p => p.Id, p => p.Name);

            viewmodel.Languages = await _languageService.TableNoTracking
                .Select(p => new { p.Id, p.Name })
                .ToDictionaryAsync(p => p.Id, p => p.Name);
        }
        #endregion
    }
}