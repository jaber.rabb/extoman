﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-layout-media-center-alt")]
    [DisplayName("مدیا")]
    public class MediaController : BaseAdminController
    {
        public readonly IMediaService _mediaService;
        public readonly IMediaThumbnailService _mediaThumbnailService;
        public readonly IMediaThumbnailSizeService _mediaThumbnailSizeService;
        public readonly IPostService _postService;
        public MediaController(
            IMediaThumbnailService mediaThumbnailService,
            IMediaThumbnailSizeService mediaThumbnailSizeService,
            IMediaService mediaService,
            IPostService postService)
        {
            _mediaService = mediaService;
            _mediaThumbnailService = mediaThumbnailService;
            _mediaThumbnailSizeService = mediaThumbnailSizeService;
            _postService = postService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public virtual async Task<ActionResult> Upload(HttpPostedFileBase file, string alt = "")
        {
            var result = await UploadFileAsync(file, alt: alt);
            return Json(result);
        }

        [HttpPost]
        public async Task<string> HtmlEditor(HttpPostedFileBase upload, string ckEditorFuncNum, int id = 0)
        {
            var result = await UploadFileAsync(upload, id);
            if (result.Status)
                return "<script>window.parent.CKEDITOR.tools.callFunction(" + ckEditorFuncNum + ", '" + result.Url + "');</script>";
            return null;
        }

        #region Helper
        private async Task<UploadResultModel> UploadFileAsync(HttpPostedFileBase file, int id = 0, string alt = "")
        {
            var result = new UploadResultModel() { Text = "File upload failed!" };
            var mediaType = file.GetFileMediaType();
            var fileName = file.GetFileName(true, $"static/upload/{mediaType.ToDisplay()}", "yyyy/MM/dd");
            if (file.IsValid(new string[] { "image", "video" }) && mediaType != MediaType.Undefined)
            {
                file.SaveAs(fileName.AbsolutePathForSave); // must be unique
                var name = file.GetPureFileName();
                var media = new Media()
                {
                    Alt = alt.HasValue() ? alt : name,
                    Name = name,
                    Url = fileName.RelativePathForWeb,
                    Type = mediaType,
                };

                var mediaThumbnailSizes = await _mediaThumbnailSizeService.GetAllCachedAsync();
                mediaThumbnailSizes.ForEach((thumbSize) =>
                    PrepareMediaThumbnail(media, mediaType, fileName.AbsolutePathForSave, file, thumbSize));

                //PrepareMediaCategory(media);

                await _mediaService.InsertAsync(media);
                if (media.Id != 0)
                {
                    result = new UploadResultModel()
                    {
                        Status = true,
                        Text = "File uploaded successfully",
                        Url = fileName.RelativePathForWeb,
                        MediaId = media.Id
                    };
                }
                // If media for other content create a switch for add content media
                await AddPostMediaAsync(id, media);
            }
            return result;
        }

        private async Task AddPostMediaAsync(int postId, Media Media)
        {
            if (postId != 0 && Media.Id != 0)
            {
                var post = await _postService.GetByIdAsync(postId);
                post.Medias.Add(Media);
                await _postService.UpdateAsync(post);
            }
        }

        private void PrepareMediaThumbnail(Media media, MediaType mediaType, string originalFilePath, HttpPostedFileBase file, MediaThumbnailSize thumbSize)
        {
            var thumbName = $"{thumbSize.Width}x{thumbSize.Height}";
            var fileName = file.GetFileName(true, $"static/upload/{mediaType.ToDisplay()}/thumbs/" + thumbName);
            var saveToFilePath = fileName.AbsolutePathForSave;
            switch (mediaType)
            {
                case MediaType.Image:
                    file.SaveAs(saveToFilePath);
                    ImageMagickHelper.ResizeCrop(originalFilePath, thumbSize.Width, thumbSize.Height, saveToFilePath, thumbSize.Crop);
                    media.Thumbnails.Add(new MediaThumbnail()
                    {
                        Width = thumbSize.Width,
                        Height = thumbSize.Height,
                        MediaThumbnailSizeId = thumbSize.Id,
                        Url = fileName.RelativePathForWeb
                    });
                    break;
                case MediaType.Video:
                    break;
            }
        }
        #endregion
    }
}