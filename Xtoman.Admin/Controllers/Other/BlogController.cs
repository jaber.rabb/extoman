﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Utility;
using Xtoman.Utility.SeoHelper;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-pencil")]
    [DisplayName("مدیریت وبلاگ")]
    public class BlogController : BaseAdminController
    {
        private readonly IPostService _postService;
        private readonly IPostCategoryService _postCategoryService;
        private readonly IPostTagService _postTagService;
        private readonly IUrlHistoryService _urlHistoryService;
        private readonly ILocalizedPropertyService _localizedPropertyService;
        private readonly ILanguageService _languageService;
        private readonly IMediaService _mediaService;
        private readonly IMessageManager _messageManager;
        public BlogController(
            IPostService postService,
            ILanguageService languageService,
            IPostCategoryService postCategoryService,
            IUrlHistoryService urlHistoryService,
            ILocalizedPropertyService localizedPropertyService,
            IPostTagService postTagService,
            IMediaService mediaService,
            IMessageManager messageManager)
        {
            _postService = postService;
            _postCategoryService = postCategoryService;
            _postTagService = postTagService;
            _languageService = languageService;
            _localizedPropertyService = localizedPropertyService;
            _urlHistoryService = urlHistoryService;
            _mediaService = mediaService;
            _messageManager = messageManager;
            //DisplayName = "مدیریت وبلاگ";
        }
        public virtual async Task<ActionResult> Index(int? page = 1, string term = "", string category = "", int? enabled = null)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var list = _postService.TableNoTracking.Include(x => x.Categories);
            if (enabled == 1)
                list = list.Where(p => p.PublishDate != null);
            else if (enabled == 2)
                list = list.Where(p => p.PublishDate == null || p.PublishDate >= DateTime.UtcNow);
            if (string.IsNullOrWhiteSpace(term) == false)
                list = list.Where(p => p.Title.Contains(term) || p.Summary.Contains(term) || p.Url.Contains(term) || p.Tags.Any(x => x.Name.Contains(term)));
            if (string.IsNullOrWhiteSpace(category) == false)
                list = list.Where(p => p.Categories.Any(x => x.Name.Contains(category)));

            var model = await list.OrderByDescending(x => x.PublishDate).ThenByDescending(x => x.Id)
                .IncludeProperties(p => p.Media, p => p.Media.Thumbnails, p => p.Media.Thumbnails.Select(f => f.MediaThumbnailSize))
                .ProjectToPagedListAsync<PostViewModel>(pageNumber, pageSize);

            model.ForEach(p => p.ListCategory = p.Categories.Select(x => x.Name).Join(", "));
            return View(model);
        }

        public virtual async Task<ActionResult> Add()
        {
            var postViewModel = new PostViewModel()
            {
                PublishDate = DateTime.UtcNow
            };

            await PrepareCategoriesForAddOrEdit();
            await AddLocalesAsync(_languageService, postViewModel.Locales);
            return View(postViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Add(PostViewModel model)
        {
            await CheckUrlAsync(model);

            if (ModelState.IsValid)
            {
                var post = model.ToEntity();
                post.TagNames = AddTag(post);
                AddCategories(post, model.CategoriesId);
                post.InsertDate = DateTime.UtcNow;

                if (model.MediaId != 0)
                    await AddPostMediaAsync(post, model.MediaId);

                await _postService.InsertAsync(post);

                if (post.Id != 0)
                {
                    await _localizedPropertyService.UpdateLocalesAsync(post, model.Locales);
                    await _urlHistoryService.SaveUrlsAsync(post, model);
                    SuccessNotification($"پست جدید با عنوان \"{post.Title}\" اضافه شد.");

                    var postUrl = $"https://www.extoman.co/post/{post.Url}";
                    //var newPostEmail = new PostEmail()
                    //{
                    //    PostTitle = model.Title,
                    //    From = "no-reply@extoman.com",
                    //    Fullname = "test",
                    //    PostImageUrl = post.Media.Url.ToStaticUrl(),
                    //    Subject = model.Title,
                    //    To = "reports@extoman.com",
                    //    Username = "test",
                    //    ViewName = "NewPost",
                    //    PostUrl = postUrl
                    //};
                    //var newPostEmailBody = newPostEmail.GetMailMessage().Body;
                    await _messageManager.NewPostAddedAsync("", post.Title, post.Summary, post.Url, post.Media.Url.ToAbsoluteFilePath());
                    return RedirectToAction("Index");
                }
                else
                {
                    ErrorNotification("هنگام افزودن پست خطایی رخ داد!");
                }
            }
            await PrepareCategoriesForAddOrEdit();
            PrepareModelTagsList(model);
            return View(model);

        }

        public virtual async Task<ActionResult> Edit(int id)
        {
            var entity = await _postService.TableNoTracking.Where(p => p.Id == id)
                .IncludeProperties(p => p.Media,
                                   p => p.Categories,
                                   p => p.Media.Thumbnails,
                                   p => p.Media.Thumbnails.Select(f => f.MediaThumbnailSize))
                .SingleAsync();
            var model = new PostViewModel().FromEntity(entity);
            model.OriginalUrl = model.Url;
            model.CategoriesId = entity.Categories.Select(p => p.Id).ToArray();
            model.ImagePath = "https://static." + DomainName + entity.Media?.Thumbnails?.Where(p => p.MediaThumbnailSize.Name == "post.medium")
                    .Select(p => p.Url).FirstOrDefault().Replace("/static", "");
            foreach (var item in entity.Tags.Select(p => new { p.Id, p.Url }))
            {
                model.TagsList.Add(item.Id, item.Url.ToString());
            }
            await PrepareCategoriesForAddOrEdit();
            await AddLocalesWithUrlsAsync(_languageService, entity, model.Locales);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Edit(PostViewModel model)
        {
            await CheckUrlAsync(model);

            if (ModelState.IsValid)
            {
                await _postService.DeleteTagsAsync(model.Id);
                await _postService.DeleteCategoriesAsync(model.Id);
                await _postService.DeleteMediaAsync(model.Id);
                var entity = _postService.GetById(model.Id);
                var post = model.ToEntity(entity);
                post.TagNames = AddTag(post);
                AddCategories(post, model.CategoriesId);
                if (model.MediaId != 0)
                {
                    await AddPostMediaAsync(post, model.MediaId);
                }
                try
                {
                    await _postService.UpdateAsync(post);
                    await _localizedPropertyService.UpdateLocalesAsync(post, model.Locales);
                    await _urlHistoryService.SaveUrlsAsync(post, model);
                    SuccessNotification($"پست شماره {post.Id} با موفقیت ویرایش شد.");
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ex.LogError();
                    var innerException = ex.InnerException != null && ex.InnerException.Message != ex.Message ?
                        $" - {ex.InnerException.Message}" : ex.InnerException != null && ex.InnerException.InnerException != null ?
                        $" - {ex.InnerException.InnerException.Message}" : "";
                    ErrorNotification($"{ex.Message}.{innerException}");
                    ErrorNotification($"Post.MediaId:{post.MediaId}, Model.MediaId:{model.MediaId}");
                    ErrorNotification("در هنگام به روز رسانی پست خطایی رخ داد!");
                }
            }
            await PrepareCategoriesForAddOrEdit();
            PrepareModelTagsList(model);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _postService.DeleteTagsAsync(id);
                await _postService.DeleteCategoriesAsync(id);
                await _postService.DeleteMediaAsync(id);
                await _postService.DeleteAsync(_postService.GetById(id));
                SuccessNotification("Deleted");
            }
            catch (Exception ex)
            {
                ErrorNotification(ex);
            }
            return RedirectToAction("Index");
        }

        [NoCache]
        [AjaxOnly]
        [AllowAnonymous]
        public virtual async Task<ActionResult> SuggestUrl(string url, int id, int langId, bool IsChangeUrl)
        {
            if (!IsChangeUrl)
                url = url.SuggestUrl();
            var result = await GetUrlAsync(url, id, langId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult TagsAutoComplete(string term)
        {
            var list = _postTagService.TableNoTracking
                .Where(p => p.Url.Contains(term))
                .OrderBy(p => p.Url).Take(10)
                .Select(p => new
                {
                    id = p.Id,
                    label = p.Url,
                    value = p.Url
                })
                .ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public virtual PartialViewResult CheckSEO(PostViewModel model)
        {
            SeoCheker seoChecker;
            if (model.Locales.Count == 1)
                seoChecker = new SeoCheker(model.Locales[0].Title, model.Locales[0].FocusKeyword, model.Locales[0].Url, model.Locales[0].MetaDescription, model.Locales[0].Text);
            else
                seoChecker = new SeoCheker(model.Title, model.FocusKeyword, model.Url, model.MetaDescription, model.Text);
            seoChecker.Check();
            return PartialView("_SEO", seoChecker);
        }

        public async Task<ActionResult> PostEnable(int id, bool enable)
        {
            try
            {
                var post = await _postService.Table.SingleAsync(p => p.Id == id);
                post.Enabled = enable;
                await _postService.UpdateAsync(post);
                return Json(new ResultModel() { Status = true, Text = $"پست شماره {id} نمایش داده {(enable ? "می شود" : "نمی شود")}." });
            }
            catch (Exception ex)
            {
                return Json(new ResultModel() { Text = ex.Message });
            }
        }

        #region Helpers
        private async Task<string> GetUrlAsync(string url, int id, int langId)
        {
            var postViewModel = new PostViewModel
            {
                Url = url,
                Id = id,
            };
            if (langId != 0)
                postViewModel.Locales.Add(new PostLocalizedViewModel { LanguageId = langId });
            var result = await _postService.IsUniqueUrlAsync(postViewModel, true);
            if (langId != 0 && !result.Locales[0].IsUnique)
                return "";
            if (!result.IsUnique)
                return "";
            return url;
        }

        private async Task AddPostMediaAsync(Post post, int? featuredMediaId)
        {
            if (featuredMediaId.HasValue && featuredMediaId.Value > 0)
            {
                var media = await _mediaService.GetByIdAsync(featuredMediaId.Value);
                post.Medias.Add(media);
            }
        }

        private void PrepareModelTagsList(PostViewModel model)
        {
            var tags = GetTagValues();
            foreach (var item in tags)
            {
                model.TagsList.Add((int)item.Key, item.Value);
            }
        }

        private string AddTag(Post post)
        {
            var tags = GetTagValues();
            var list = new List<int>();
            foreach (var item in tags)
            {
                if (item.Key.HasValue)
                {
                    list.Add(item.Key.Value);
                }
                else
                {
                    var name = item.Value.CleanString();
                    var entity = _postTagService.Table.Where(p => p.Url == name).SingleOrDefault();
                    if (entity == null)
                    {
                        var tag = new PostTag { Name = name, Url = name.FixUrl() };
                        _postTagService.Insert(tag);
                        list.Add(tag.Id);
                    }
                    else
                    {
                        list.Add(entity.Id);
                    }
                }
            }
            var entities = _postTagService.Table.Where(p => list.Contains(p.Id)).ToList();
            entities.ForEach(p => post.Tags.Add(p));
            return string.Join(",", tags.Select(p => p.Value));
        }

        private IEnumerable<KeyValuePair<int?, string>> GetTagValues()
        {
            foreach (var item in Request.Form.AllKeys.Where(p => p.Contains("tag[")).ToList())
            {
                var values = Request.Form[item];
                var key = item.RemoveStr(new[] { "tag[", "-a", "]" }).ToNullableInt();
                foreach (var value in values.Split(','))
                {
                    yield return new KeyValuePair<int?, string>(key, value);
                }
            }
        }

        private void AddCategories(Post post, int[] categoryIds)
        {
            //http://www.entityframeworktutorial.net/update-many-to-many-entities-in-entity-framework.aspx
            //http://www.entityframeworktutorial.net/EntityFramework4.3/update-many-to-many-entity-using-dbcontext.aspx
            var entities = _postCategoryService.Table.Where(p => categoryIds.Contains(p.Id)).ToList();
            entities.ForEach(p => post.Categories.Add(p));
        }

        private async Task CheckUrlAsync(PostViewModel postViewModel)
        {
            var result = await _postService.IsUniqueUrlAsync(postViewModel, true);
            if (result.IsUnique == false)
                ModelState.AddModelError("Url", "Url is not unique!");
            for (int i = 0; i < result.Locales.Count; i++)
            {
                if (result.Locales[i].IsUnique == false)
                    ModelState.AddModelError($"Locales[{i}].Url", $"Url in language {result.Locales[i].LanguageName} is not unique!");
            }
        }

        private async Task PrepareCategoriesForAddOrEdit()
        {
            var items = new List<CategorySelectListViewModel>();
            var categories = await _postCategoryService.TableNoTracking.ToListAsync();
            var parentCategories = categories.Where(p => p.ParentCategoryId == null || p.ParentCategoryId == 0).ToList();
            foreach (var item in parentCategories)
            {
                items.Add(new CategorySelectListViewModel()
                {
                    ParentCategory = item.Name,
                    Id = item.Id,
                    Name = "- " + item.Name
                });
                var childs = categories.Where(p => p.ParentCategoryId == item.Id).ToList();
                foreach (var c in childs)
                {
                    items.Add(new CategorySelectListViewModel()
                    {
                        ParentCategory = item.Name,
                        Id = c.Id,
                        Name = c.Name
                    });
                }
            }
            ViewBag.Categories = new MultiSelectList(items, "Id", "Name", "ParentCategory");
        }
        #endregion
    }
}