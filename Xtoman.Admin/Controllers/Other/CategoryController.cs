﻿using AutoMapper;
using PagedList.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-list")]
    [DisplayName("دسته بندی ها")]
    public class CategoryController : BaseAdminController
    {
        private readonly ICategoryService _categoryService;
        private readonly INewsCategoryService _newsCategoryService;
        private readonly IPostCategoryService _postCategoryService;
        private readonly IProductCategoryService _productCategoryService;
        private readonly ICategoryRateService _categoryRateService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizedPropertyService _localizedPropertyService;
        private readonly IUrlHistoryService _urlHistoryService;
        public CategoryController(
            ICategoryService categoryService,
            INewsCategoryService newsCategoryService,
            IPostCategoryService postCategoryService,
            IProductCategoryService productCategoryService,
            ICategoryRateService categoryRateService,
            ILanguageService languageService,
            ILocalizedPropertyService localizedPropetyService,
            IUrlHistoryService urlHistoryService)
        {
            _categoryService = categoryService;
            _newsCategoryService = newsCategoryService;
            _postCategoryService = postCategoryService;
            _productCategoryService = productCategoryService;
            _categoryRateService = categoryRateService;
            _languageService = languageService;
            _localizedPropertyService = localizedPropetyService;
            _urlHistoryService = urlHistoryService;
        }

        #region News category
        public async Task<ActionResult> NewsCategories(int? page = 1, string term = "")
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var list = _newsCategoryService.TableNoTracking;
            if (string.IsNullOrWhiteSpace(term) == false)
                list = list.Where(p => p.Name.Contains(term));
            var model = await list.OrderByDescending(x => x.Order).ThenByDescending(x => x.Id)
                .Select(p => new CategoryViewModel
                {
                    Id = p.Id,
                    Name = p.Name,
                    Url = p.Url,
                    Order = p.Order,
                    ParentCategory = p.ParentCategory != null ? p.ParentCategory.Name : "بدون مادر"
                }).ToPagedListAsync(pageNumber, pageSize);
            ViewBag.Type = GeneralType.News;
            return View("Index", model);
        }
        [HttpGet]
        public async Task<ActionResult> AddNewsCategory()
        {
            var model = new AddEditCategoryViewModel();
            await PrepareNewsCategoriesForParent();
            await AddLocalesAsync(_languageService, model.Locales);
            ViewBag.TitleType = "دسته بندی خبر";
            return View("Add", model);
        }
        [HttpPost]
        public async Task<ActionResult> AddNewsCategory(AddEditCategoryViewModel model)
        {
            await CheckUrlAsync(model);
            if (ModelState.IsValid)
            {
                var entity = new NewsCategory()
                {
                    ColorHex = model.ColorHex,
                    Description = model.Description,
                    MetaDescription = model.MetaDescription,
                    MetaKeywords = model.MetaKeywords,
                    Name = model.Name,
                    Order = model.Order,
                    ParentCategoryId = model.ParentCategoryId,
                    Url = model.Url.FixUrl()
                };
                await _newsCategoryService.InsertAsync(entity);
                if (entity.Id != 0)
                {
                    await _localizedPropertyService.UpdateLocalesAsync(entity, model.Locales);
                    await _urlHistoryService.SaveUrlsAsync(entity, model);
                    SuccessNotification($"دسته بندی با نام \"{entity.Name}\" با موفقیت اضافه شد.");
                    return RedirectToAction("NewsCategories");
                }
                else
                {
                    ErrorNotification("An error occurred while adding new post category!");
                }
            }
            await PrepareNewsCategoriesForParent();
            ViewBag.TitleType = "دسته بندی خبر";
            return View("Add", model);
        }
        public virtual async Task<ActionResult> EditNewsCategory(int id)
        {
            var entity = await _newsCategoryService.TableNoTracking.Where(p => p.Id == id)
                .Include(p => p.ParentCategory)
                .SingleAsync();
            var model = new AddEditCategoryViewModel().FromEntity(entity);
            model.OriginalUrl = model.Url;
            await AddLocalesWithUrlsAsync(_languageService, entity, model.Locales);
            ViewBag.TitleType = "دسته بندی خبر";
            await PrepareNewsCategoriesForParent();
            return View("Edit", model);
        }
        [HttpPost]
        public virtual async Task<ActionResult> EditNewsCategory(AddEditCategoryViewModel model)
        {
            await CheckUrlAsync(model);
            if (ModelState.IsValid)
            {
                var entity = new NewsCategory()
                {
                    Id = (int)model.Id,
                    ColorHex = model.ColorHex,
                    Description = model.Description,
                    MetaDescription = model.MetaDescription,
                    MetaKeywords = model.MetaKeywords,
                    Name = model.Name,
                    Order = model.Order,
                    ParentCategoryId = model.ParentCategoryId,
                    Url = model.Url.FixUrl()
                };
                await _newsCategoryService.UpdateAsync(entity);
                try
                {
                    await _newsCategoryService.UpdateAsync(entity);
                    await _localizedPropertyService.UpdateLocalesAsync(entity, model.Locales);
                    await _urlHistoryService.SaveUrlsAsync(entity, model);
                    return RedirectToAction("NewsCategories");
                }
                catch (Exception)
                {
                    ErrorNotification("An error occurred while updating post category!");
                }
            }
            ViewBag.TitleType = "دسته بندی خبر";
            await PrepareNewsCategoriesForParent();
            return View("Edit", model);
        }
        public async Task<ActionResult> DeleteNewsCategory(int id)
        {
            try
            {
                await _newsCategoryService.DeleteAsync(_newsCategoryService.GetById(id));
                SuccessNotification("Deleted");
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
            }
            return RedirectToAction("Index");
        }
        #endregion

        #region Products category
        public async Task<ActionResult> ProductCategories(int? page = 1, int? pSize = 10, string term = "")
        {
            int pageNumber = (page ?? 1);
            int pageSize = (pSize ?? 10);
            var list = _productCategoryService.TableNoTracking;
            if (string.IsNullOrWhiteSpace(term) == false)
                list = list.Where(p => p.Name.Contains(term));
            var model = await list.OrderByDescending(x => x.Order).ThenByDescending(x => x.Id)
                .Select(p => new CategoryViewModel
                {
                    Id = p.Id,
                    Name = p.Name,
                    Url = p.Url,
                    Order = p.Order,
                    ParentCategory = p.ParentCategory != null ? p.ParentCategory.Name : "بدون مادر"
                }).ToPagedListAsync(pageNumber, pageSize);
            ViewBag.Type = GeneralType.Product;
            return View("Index", model);
        }
        [HttpGet]
        public async Task<ActionResult> AddProductCategory()
        {
            var model = new AddEditCategoryViewModel();
            await PrepareProductCategoriesForParent();
            await AddLocalesAsync(_languageService, model.Locales);
            ViewBag.TitleType = "دسته بندی محصول";
            return View("Add", model);
        }
        [HttpPost]
        public async Task<ActionResult> AddProductCategory(AddEditCategoryViewModel model)
        {
            await CheckUrlAsync(model);
            if (ModelState.IsValid)
            {
                var entity = new ProductCategory()
                {
                    ColorHex = model.ColorHex,
                    Description = model.Description,
                    MetaDescription = model.MetaDescription,
                    MetaKeywords = model.MetaKeywords,
                    Name = model.Name,
                    Order = model.Order,
                    ParentCategoryId = model.ParentCategoryId,
                    Url = model.Url.FixUrl(),
                };

                if (model.RateName != null && model.RateName.Any())
                    foreach (var item in model.RateName)
                        entity.Rates.Add(new CategoryRate() { Name = item });

                await _productCategoryService.InsertAsync(entity);
                if (entity.Id != 0)
                {
                    await _localizedPropertyService.UpdateLocalesAsync(entity, model.Locales);
                    await _urlHistoryService.SaveUrlsAsync(entity, model);
                    SuccessNotification($"دسته بندی با نام \"{entity.Name}\" با موفقیت اضافه شد.");
                    return RedirectToAction("ProductCategories");
                }
                else
                {
                    ErrorNotification("An error occurred while adding new product category!");
                }
            }
            await PrepareProductCategoriesForParent();
            ViewBag.TitleType = "دسته بندی محصول";
            return View("Add", model);
        }
        public virtual async Task<ActionResult> EditProductCategory(int id)
        {
            var entity = await _productCategoryService
                .TableNoTracking.Where(p => p.Id == id)
                .Include(p => p.ParentCategory)
                .Include(p => p.Rates)
                .SingleAsync();

            var model = new AddEditCategoryViewModel().FromEntity(entity);

            model.OriginalUrl = model.Url;
            await AddLocalesWithUrlsAsync(_languageService, entity, model.Locales);
            ViewBag.TitleType = "دسته بندی محصول";
            await PrepareProductCategoriesForParent();
            return View("Edit", model);
        }
        [HttpPost]
        public virtual async Task<ActionResult> EditProductCategory(AddEditCategoryViewModel model)
        {
            await CheckUrlAsync(model);
            if (ModelState.IsValid)
            {
                var entity = await _categoryService
                    .TableNoTracking.Where(p => p.Id == model.Id)
                    .Include(p => p.ParentCategory)
                    .Include(p => p.Rates)
                    .SingleAsync();

                entity = model.ToEntity(entity);

                if (model.RateName != null && model.RateName.Any())
                {
                    var categoryRates = await _categoryRateService.Table
                        .Where(p => p.CategoryId == model.Id)
                        .ToListAsync();

                    //Add if not exist Entity
                    foreach (var rateName in model.RateName)
                        if (!categoryRates.Any(x => x.Name == rateName))
                        {
                            var categoryRate = new CategoryRate() { Name = rateName, CategoryId = model.Id };
                            await _categoryRateService.InsertAsync(categoryRate);
                            categoryRates.Add(categoryRate);
                        }
                    //Delete if not exist Model
                    var toDelete = new List<CategoryRate>();
                    foreach (var item in categoryRates)
                        if (!model.RateName.Any(x => x == item.Name))
                            toDelete.Add(item);

                    await _categoryRateService.DeleteAsync(toDelete);
                }

                try
                {
                    await _categoryService.UpdateAsync(entity);
                    await _localizedPropertyService.UpdateLocalesAsync(entity, model.Locales);
                    await _urlHistoryService.SaveUrlsAsync(entity, model);
                    return RedirectToAction("ProductCategories");
                }
                catch (Exception)
                {
                    ErrorNotification("An error occurred while updating product category!");
                }
            }
            ViewBag.TitleType = "دسته بندی محصول";
            await PrepareProductCategoriesForParent();
            return View("Edit", model);
        }
        public async Task<ActionResult> DeleteProductCategory(int id)
        {
            try
            {
                await _productCategoryService.DeleteAsync(_productCategoryService.GetById(id));
                SuccessNotification("Deleted");
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
            }
            return RedirectToAction("Index");
        }
        #endregion

        #region Post category
        public async Task<ActionResult> PostCategories(int? page = 1, string term = "")
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var list = _postCategoryService.TableNoTracking;
            if (string.IsNullOrWhiteSpace(term) == false)
                list = list.Where(p => p.Name.Contains(term));
            var model = await list.OrderByDescending(x => x.Order).ThenByDescending(x => x.Id)
                .Select(p => new CategoryViewModel
                {
                    Id = p.Id,
                    Name = p.Name,
                    Url = p.Url,
                    Order = p.Order,
                    ParentCategory = p.ParentCategory != null ? p.ParentCategory.Name : "بدون مادر"
                }).ToPagedListAsync(pageNumber, pageSize);
            ViewBag.Type = GeneralType.Post;
            return View("Index", model);
        }
        [HttpGet]
        public async Task<ActionResult> AddPostCategory()
        {
            var model = new AddEditCategoryViewModel();
            await PreparePostCategoriesForParent();
            await AddLocalesAsync(_languageService, model.Locales);
            ViewBag.TitleType = "دسته بندی پست";
            return View("Add", model);
        }
        [HttpPost]
        public async Task<ActionResult> AddPostCategory(AddEditCategoryViewModel model)
        {
            await CheckUrlAsync(model);
            if (ModelState.IsValid)
            {
                var postCategory = new PostCategory()
                {
                    ColorHex = model.ColorHex,
                    Description = model.Description,
                    MetaDescription = model.MetaDescription,
                    MetaKeywords = model.MetaKeywords,
                    Name = model.Name,
                    Order = model.Order,
                    ParentCategoryId = model.ParentCategoryId,
                    Url = model.Url.FixUrl()
                };
                await _postCategoryService.InsertAsync(postCategory);
                if (postCategory.Id != 0)
                {
                    await _localizedPropertyService.UpdateLocalesAsync(postCategory, model.Locales);
                    await _urlHistoryService.SaveUrlsAsync(postCategory, model);
                    SuccessNotification($"دسته بندی جدید پست با نام \"{postCategory.Name}\" با موفقیت اضافه شد.");
                    return RedirectToAction("PostCategories");
                }
                else
                {
                    ErrorNotification("An error occurred while adding new post category!");
                }
            }
            ViewBag.TitleType = "دسته بندی پست";
            await PreparePostCategoriesForParent();
            return View("Add", model);
        }
        public virtual async Task<ActionResult> EditPostCategory(int id)
        {
            var entity = await _postCategoryService.TableNoTracking.Where(p => p.Id == id)
                .Include(p => p.ParentCategory)
                .SingleAsync();
            var model = new AddEditCategoryViewModel().FromEntity(entity);
            model.OriginalUrl = model.Url;
            await AddLocalesWithUrlsAsync(_languageService, entity, model.Locales);
            ViewBag.TitleType = "دسته بندی پست";
            await PreparePostCategoriesForParent();
            return View("Edit", model);
        }
        [HttpPost]
        public virtual async Task<ActionResult> EditPostCategory(AddEditCategoryViewModel model)
        {
            await CheckUrlAsync(model);
            if (ModelState.IsValid)
            {
                var postCategory = new PostCategory()
                {
                    Id = (int)model.Id,
                    ColorHex = model.ColorHex,
                    Description = model.Description,
                    MetaDescription = model.MetaDescription,
                    MetaKeywords = model.MetaKeywords,
                    Name = model.Name,
                    Order = model.Order,
                    ParentCategoryId = model.ParentCategoryId,
                    Url = model.Url.FixUrl()
                };
                await _postCategoryService.UpdateAsync(postCategory);
                try
                {
                    await _postCategoryService.UpdateAsync(postCategory);
                    await _localizedPropertyService.UpdateLocalesAsync(postCategory, model.Locales);
                    await _urlHistoryService.SaveUrlsAsync(postCategory, model);
                    return RedirectToAction("PostCategories");
                }
                catch (Exception)
                {
                    ErrorNotification("An error occurred while updating post category!");
                }
            }
            await PreparePostCategoriesForParent();
            ViewBag.TitleType = "دسته بندی پست";
            return View("Edit", model);
        }
        public async Task<ActionResult> DeletePostCategory(int id)
        {
            try
            {
                await _postCategoryService.DeleteAsync(_postCategoryService.GetById(id));
                SuccessNotification("Deleted");
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
            }
            return RedirectToAction("Index");
        }
        #endregion

        #region Helper
        private async Task PrepareNewsCategoriesForParent()
        {
            var items = new List<CategorySelectListViewModel>
            {
                new CategorySelectListViewModel()
                {
                    Id = null,
                    Name = "انتخاب مادر (اختیاری)",
                    ParentCategory = ""
                }
            };
            var categories = await _newsCategoryService.TableNoTracking.ToListAsync();
            var parentCategories = categories.Where(p => p.ParentCategoryId == null || p.ParentCategoryId == 0).ToList();
            foreach (var item in parentCategories)
            {
                items.Add(new CategorySelectListViewModel()
                {
                    ParentCategory = item.Name,
                    Id = item.Id,
                    Name = "- " + item.Name
                });
                var childs = categories.Where(p => p.ParentCategoryId == item.Id).ToList();
                foreach (var c in childs)
                {
                    items.Add(new CategorySelectListViewModel()
                    {
                        ParentCategory = item.Name,
                        Id = c.Id,
                        Name = c.Name
                    });
                }
            }
            ViewBag.Categories = new SelectList(items, "Id", "Name", "ParentCategory");
        }

        private async Task PrepareProductCategoriesForParent()
        {
            var items = new List<CategorySelectListViewModel>
            {
                new CategorySelectListViewModel()
                {
                    Id = null,
                    Name = "انتخاب مادر (اختیاری)",
                    ParentCategory = ""
                }
            };
            var categories = await _productCategoryService.TableNoTracking.ToListAsync();
            var parentCategories = categories.Where(p => p.ParentCategoryId == null || p.ParentCategoryId == 0).ToList();
            foreach (var item in parentCategories)
            {
                items.Add(new CategorySelectListViewModel()
                {
                    ParentCategory = item.Name,
                    Id = item.Id,
                    Name = "- " + item.Name
                });
                var childs = categories.Where(p => p.ParentCategoryId == item.Id).ToList();
                foreach (var c in childs)
                {
                    items.Add(new CategorySelectListViewModel()
                    {
                        ParentCategory = item.Name,
                        Id = c.Id,
                        Name = c.Name
                    });
                }
            }
            ViewBag.Categories = new SelectList(items, "Id", "Name", "ParentCategory");
        }

        private async Task PreparePostCategoriesForParent()
        {
            var items = new List<CategorySelectListViewModel>
            {
                new CategorySelectListViewModel()
                {
                    Id = null,
                    Name = "انتخاب مادر (اختیاری)",
                    ParentCategory = ""
                }
            };
            var categories = await _postCategoryService.TableNoTracking.ToListAsync();
            var parentCategories = categories.Where(p => p.ParentCategoryId == null || p.ParentCategoryId == 0).ToList();
            foreach (var item in parentCategories)
            {
                items.Add(new CategorySelectListViewModel()
                {
                    ParentCategory = item.Name,
                    Id = item.Id,
                    Name = "- " + item.Name
                });
                var childs = categories.Where(p => p.ParentCategoryId == item.Id).ToList();
                foreach (var c in childs)
                {
                    items.Add(new CategorySelectListViewModel()
                    {
                        ParentCategory = item.Name,
                        Id = c.Id,
                        Name = c.Name
                    });
                }
            }
            ViewBag.Categories = new SelectList(items, "Id", "Name", "ParentCategory");
        }

        //private void CheckUrl(AddEditCategoryViewModel model)
        //{
        //    var result = _postCategoryService.IsUniqueUrl(model, true);
        //    if (result.IsUnique == false)
        //        ModelState.AddModelError("Url", "Url is not unique!");
        //    for (int i = 0; i < result.Locales.Count; i++)
        //    {
        //        if (result.Locales[i].IsUnique == false)
        //            ModelState.AddModelError($"Locales[{i}].Url", $"Url in language {result.Locales[i].LanguageName} is not unique!");
        //    }
        //}

        private async Task CheckUrlAsync(AddEditCategoryViewModel model)
        {
            model.Url = model.Url.FixUrl();
            var result = await _categoryService.IsUniqueUrlAsync(model, true);
            if (result.IsUnique == false)
                ModelState.AddModelError("Url", "Url نام آشنا تکراری است.");
            for (int i = 0; i < result.Locales.Count; i++)
            {
                if (result.Locales[i].IsUnique == false)
                    ModelState.AddModelError($"Locales[{i}].Url", $"Url نام آشنا در زبان {result.Locales[i].LanguageName} تکراری است!");
            }
        }
        #endregion
    }
}