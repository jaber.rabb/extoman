﻿using Microsoft.AspNet.Identity;
using PagedList.EntityFramework;
using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-comment-alt")]
    [DisplayName("نظرها")]
    public class CommentController : BaseAdminController
    {
        private readonly ICommentService _commentService;
        private readonly IPostCommentService _postCommentService;
        private readonly IPostService _postService;
        private readonly IAppUserManager _userManager;
        public CommentController(
            ICommentService commentService,
            IPostCommentService postCommentService,
            IPostService postService,
            IAppUserManager userManager)
        {
            _commentService = commentService;
            _postCommentService = postCommentService;
            _postService = postService;
            _userManager = userManager;
        }

        /// <summary>
        /// List of all comments
        /// </summary>
        /// <param name="type">filter by type</param>
        /// <param name="page">page number</param>
        /// <returns></returns>
        public virtual async Task<ActionResult> Index(GeneralType? type = null, int page = 1, int? isconfirm = null)
        {
            var pageSize = 10;
            var commentQuery = _commentService.TableNoTracking;
            if (isconfirm != null)
            {
                if (isconfirm == 0)
                    commentQuery = commentQuery.Where(p => !(bool)p.IsConfirm);
                else
                    commentQuery = commentQuery.Where(p => (bool)p.IsConfirm);
            }
            switch (type)
            {
                case GeneralType.Post:
                    commentQuery = commentQuery.Where(p => p is PostComment);
                    break;
                default:
                    break;
            }
            var model = await commentQuery
                .OrderByDescending(p => p.InsertDate)
                .Select(p => new CommentViewModel()
                {
                    AnonymousID = p.AnonymousId,
                    ConfirmDate = p.ConfirmDate,
                    ConfirmUserId = p.ConfirmUserId,
                    Id = p.Id,
                    InsertDate = p.InsertDate,
                    IsConfirm = p.IsConfirm,
                    Parent = new CommentViewModel().FromEntity(p.Parent),
                    ParentCommentId = p.ParentCommentId,
                    Post = p is PostComment ? (p as PostComment).Post : null,
                    RelatedEntityId = p is PostComment ? (p as PostComment).PostId : 0,
                    Text = p.Text,
                    Type = p is PostComment ? GeneralType.Post : GeneralType.News,
                    User = p.InsertUser
                })
                .ToPagedListAsync(page, pageSize);
            return View(model);
        }

        /// <summary>
        /// Post comments
        /// </summary>
        /// <param name="id">Post Id</param>
        /// <param name="page">Page number</param>
        /// <returns>Paged list of related post comments</returns>
        public async Task<ActionResult> Post(int id, int page = 1, int pageSize = 10)
        {
            int pageNumber = page;
            var query =  _postCommentService.TableNoTracking
                .Where(p => p.PostId == id);

            var model = await query
                .OrderByDescending(x => x.Id)
                .ProjectToPagedListAsync<CommentViewModel>(pageNumber, pageSize);
            return View(model);
        }

        [HttpPost]
        public virtual async Task<ActionResult> CreateOrReplay(CommentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetCurrentUserAsync();
                var comment = new Comment()
                {
                    AnonymousId = model.AnonymousID,
                    ConfirmDate = DateTime.UtcNow,
                    ConfirmUserId = user.Id,
                    IsAutenticated = true,
                    ParentCommentId = model.ParentCommentId,
                    Text = model.Text,
                    IsConfirm = true,
                    InsertDate = DateTime.UtcNow,
                    InsertUserId = user.Id,
                    FullName = user.FullName,
                    Email = user.Email,
                };
                switch (model.Type)
                {
                    case GeneralType.Post:
                        var postComment = (comment as PostComment);
                        postComment.PostId = model.RelatedEntityId;
                        postComment.Post = await _postService.GetByIdAsync(model.RelatedEntityId);
                        postComment.Post.ApprovedCommentsCount++;
                        await _postCommentService.InsertAsync(postComment);
                        break;
                }
                return RedirectToAction(model.Type, model.RelatedEntityId);
            }
            return View(model);
        }

        public virtual async Task<ActionResult> Confirm(int id, bool isConfirm)
        {
            var comment = await _commentService.GetByIdAsync(id);
            comment.IsConfirm = isConfirm;
            comment.ConfirmUserId = User.Identity.GetUserId<int>();
            switch (comment)
            {
                case PostComment PostComment:
                    PostComment.Post.ApprovedCommentsCount++;
                    break;
            }
            await _commentService.UpdateAsync(comment);
            return Json(new ResultModel
            {
                Status = true,
                Text = isConfirm ? "Comment confirmed!" : "Comment unconfirmed!"
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual async Task<ActionResult> Edit(CommentViewModel model)
        {
            var entity = await _commentService.GetByIdAsync(model.Id);
            entity.Text = model.Text;
            await _commentService.UpdateAsync(entity);
            return Json(new ResultModel() { Status = true, Text = "Comment edited!" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Delete(int id, GeneralType type, int relatedEntityId)
        {
            var entity = await _commentService.GetByIdAsync(id);
            await DeleteCommentAsync(entity);
            SuccessNotification("Comment deleted!");
            return RedirectToAction(type, relatedEntityId);
        }

        #region Helper
        private RedirectToRouteResult RedirectToAction(GeneralType type, int id)
        {
            switch (type)
            {
                case GeneralType.Post:
                    return RedirectToAction("Post", new { id = id });
            }
            return RedirectToAction("Index");
        }

        private async Task DeleteCommentAsync(Comment comment)
        {
            var parentComments = await _commentService.Table.Where(x => x.ParentCommentId == comment.Id).ToListAsync();
            foreach (var subComment in parentComments)
            {
                if (await _commentService.TableNoTracking.Where(x => x.ParentCommentId == subComment.Id).AnyAsync())
                {
                    await DeleteCommentAsync(subComment);
                }
                else
                {
                    await _commentService.DeleteAsync(subComment);
                }
            }
            await _commentService.DeleteAsync(comment);
        }
        #endregion
    }
}