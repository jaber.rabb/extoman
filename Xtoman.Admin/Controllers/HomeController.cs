﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers
{
    public class HomeController : BaseAdminController
    {
        private readonly IBalanceManager _balanceManager;
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly IUserService _userService;
        //private readonly ITransactionFeeManager transactionFeeManager;
        public HomeController(
            IBalanceManager balanceManager,
            IExchangeOrderService exchangeOrderService,
            IUserService userService)
        {
            _balanceManager = balanceManager;
            _exchangeOrderService = exchangeOrderService;
            _userService = userService;
        }
        public async Task<ActionResult> Index()
        {
            //var bitcoinBalance = await _balanceManager.AllBalancesAsync();
            var ordersQuery = _exchangeOrderService.TableNoTracking;
            var completedOrdersQuery = ordersQuery.Where(p => p.OrderStatus == OrderStatus.Complete);
            var usersQuery = _userService.TableNoTracking;
            var pastDayDate = DateTime.UtcNow.AddDays(-1);
            var pastWeekDate = DateTime.UtcNow.AddDays(-7);
            var pastMonthDate = DateTime.UtcNow.AddMonths(-1);

            var totalShetabPaidComplete = await completedOrdersQuery
                .Where(p => p.Exchange.FromCurrency.Type == ECurrencyType.Shetab)
                .Select(p => p.PayAmount).DefaultIfEmpty(0).SumAsync();

            var totalBTCSold = await completedOrdersQuery
                .Where(p => p.Exchange.ToCurrency.Type == ECurrencyType.Bitcoin)
                .Select(p => p.ReceiveAmount).DefaultIfEmpty(0).SumAsync();

            var totalPerfectSold = await completedOrdersQuery
                .Where(p => p.Exchange.ToCurrency.Type == ECurrencyType.PerfectMoney || p.Exchange.ToCurrency.Type == ECurrencyType.PerfectMoneyVoucher)
                .Select(p => p.ReceiveAmount).DefaultIfEmpty(0).SumAsync();

            var model = new HomeIndexViewModel()
            {
                TotalShetabPaidComplete = totalShetabPaidComplete.ToPriceWithoutFloat() + " تومان",
                TotalBTCSold = totalBTCSold.ToBlockChainPrice() + " BTC",
                TotalPMSold = totalPerfectSold.ToBlockChainPrice(2) + " دلار",
                //Balances = bitcoinBalance.Balances,
                DailyOrdersCount = await completedOrdersQuery.Where(p => p.InsertDate >= pastDayDate).CountAsync(),
                WeeklyOrdersCount = await completedOrdersQuery.Where(p => p.InsertDate >= pastWeekDate).CountAsync(),
                MonthlyOrdersCount = await completedOrdersQuery.Where(p => p.InsertDate >= pastMonthDate).CountAsync(),
                TotalCompletedOrders = await completedOrdersQuery.CountAsync(),
                ProcessingOrdersCount = await ordersQuery.Where(p => p.OrderStatus.HasFlag(OrderStatus.Processing)).CountAsync(),
                VerifyPendingsCount = await usersQuery.Where(p => p.IdentityVerificationStatus == VerificationStatus.Pending).CountAsync(),
                LoyalUsersCount = await usersQuery.Where(p => p.Orders.Where(x => x.OrderStatus == OrderStatus.Complete).Count() >= 2).CountAsync(),
                TotalUsers = await usersQuery.CountAsync(),
                TotalVerifiedUsers = await usersQuery.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed &&
                                                                 p.TelephoneConfirmationStatus == VerificationStatus.Confirmed &&
                                                                 p.UserBankCards.Any(x => x.VerificationStatus == VerificationStatus.Confirmed) &&
                                                                 p.PhoneNumberConfirmed && p.EmailConfirmed)
                    .CountAsync(),
                PastMonthNewUsers = await usersQuery.Where(p => p.RegDate >= pastMonthDate).CountAsync(),
                PastWeekNewUsers = await usersQuery.Where(p => p.RegDate >= pastWeekDate).CountAsync(),
                TodayNewUsers = await usersQuery.Where(p => p.RegDate >= pastDayDate).CountAsync()
            };
            return View(model);
        }
    }
}