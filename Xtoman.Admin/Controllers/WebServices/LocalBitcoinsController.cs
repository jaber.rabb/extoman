﻿using System.ComponentModel;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service.WebServices;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-wand")]
    [DisplayName("لوکال بیت کوین")]
    public class LocalBitcoinsController : BaseAdminController
    {
        private readonly ILocalBitcoinsService _localBitcoinsService;
        public LocalBitcoinsController(
            ILocalBitcoinsService localBitcoinsService)
        {
            _localBitcoinsService = localBitcoinsService;
        }

        public async Task<ActionResult> Index()
        {
            var buy = await _localBitcoinsService.BuyOnlineAdsIranAsync().ConfigureAwait(false);
            var sell = await _localBitcoinsService.SellOnlineAdsIranAsync().ConfigureAwait(false);

            var model = new LocalBitcoinsIndexViewModel()
            {
                Buy = buy,
                Sell = sell
            };
            return View(model);
        }
    }
}