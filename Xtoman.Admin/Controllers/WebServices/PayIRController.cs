﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers.WebServices
{
    public class PayIRController : BaseAdminController
    {
        #region Properties
        private readonly IPayIRService _payIRService;
        #endregion

        #region Constructor
        public PayIRController(
            IPayIRService payIRService)
        {
            _payIRService = payIRService;
        }
        #endregion

        #region Actions
        public async Task<ActionResult> Index()
        {
            var result = await _payIRService.CashoutBalanceAsync();
            if (result.Status == 1)
                return View(result.Data);

            return Content(result.Serialize());
        }

        public async Task<ActionResult> Transactions(int page = 1)
        {
            var result = await _payIRService.TransactionsAsync(page);

            if (result.Status == 1)
                return View(result.Data.Transactions);

            return Content(result.Serialize());
        }

        public async Task<ActionResult> Settlement(int page = 1)
        {
            var result = await _payIRService.TransactionsAsync(page);

            if (result.Status == 1)
                return View(result.Data.Transactions);

            return Content(result.Serialize());
        }
        

        public ActionResult Payout()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Payout(int amount, string name, string sheba, long uid)
        {
            var result = await _payIRService.CashoutRequestAsync(amount, name, sheba, uid);

            return Content(result.Serialize());
        }
        #endregion

        #region Helpers

        #endregion
    }
}