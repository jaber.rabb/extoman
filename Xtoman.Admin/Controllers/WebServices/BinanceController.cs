﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.TradingPlatform.Binance;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-wand")]
    [DisplayName("بایننس")]
    public class BinanceController : BaseAdminController
    {
        #region Properties
        private readonly IBinanceService _binanceService;
        private readonly IPriceManager _priceManager;
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly ICurrencyTypeService _currencyTypeService;
        private readonly IBlockchainNetworkService _blockchainNetworkService;
        private readonly ICurrencyTypeNetworkService _currencyTypeNetworkService;
        //private readonly IWithdrawService _withdrawService;
        #endregion

        #region Constructor
        public BinanceController(
            IBinanceService binanceService,
            IPriceManager priceManager,
            ICurrencyTypeService currencyTypeService,
            IBlockchainNetworkService blockchainNetworkService,
            ICurrencyTypeNetworkService currencyTypeNetworkService,
            IExchangeOrderService exchangeOrderService)
        {
            _binanceService = binanceService;
            _priceManager = priceManager;
            _exchangeOrderService = exchangeOrderService;
            _blockchainNetworkService = blockchainNetworkService;
            _currencyTypeNetworkService = currencyTypeNetworkService;
            _currencyTypeService = currencyTypeService;
        }
        #endregion

        #region Actions
        public async Task<ActionResult> Index()
        {
            var model = new BinanceViewModel()
            {
                Balance = await GetBinanceBalanceAsync()
            };
            return View(model);
        }

        public async Task<ActionResult> Orders(string symbol)
        {
            var model = new BinanceOrdersViewModel();
            if (symbol.HasValue())
            {
                var binanceOrders = await _binanceService.GetAllOrdersAsync(symbol, null, 500);
                binanceOrders = binanceOrders.OrderByDescending(p => p.OrderId).ToList();
                var orders = await _exchangeOrderService.TableNoTracking.Where(p => p.OrderTrades.Any(x => x.ApiTradeId != null))
                    .Select(p => new { p.Id, p.OrderTrades })
                    //.Take(500)
                    .ToListAsync();
                var ordersViewModel = new List<BinanceSingleOrderViewModel>();
                foreach (var item in binanceOrders)
                {
                    var exchangeOrderId = orders.Where(p => p.OrderTrades.Any(x => x.ApiTradeId == item.ClientOrderId))?.Select(p => p?.Id).DefaultIfEmpty(null).FirstOrDefault();
                    ordersViewModel.Add(new BinanceSingleOrderViewModel()
                    {
                        ClientOrderId = item.ClientOrderId,
                        IcebergQty = item.IcebergQty,
                        ExecutedQty = item.ExecutedQty,
                        OrderId = item.OrderId,
                        OrigQty = item.OrigQty,
                        Price = item.Price,
                        Side = item.Side,
                        Status = item.Status,
                        StopPrice = item.StopPrice,
                        Symbol = item.Symbol,
                        Time = item.Time,
                        TimeInForce = item.TimeInForce,
                        Type = item.Type,
                        ExchangeOrderId = exchangeOrderId
                    });
                }
                model.Orders = ordersViewModel;
                return PartialView("_OrdersList", model);
            }
            else
            {
                model.Symbols = await _binanceService.GetAllPricesAsync();
                return View(model);
            }
        }

        public async Task<ActionResult> AddOrder()
        {
            ViewBag.Symbols = await _binanceService.GetAllPricesAsync();
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddOrder(AddBinanceOrderViewModel model)
        {
            try
            {
                var orderResult = await _binanceService.PostNewOrderAsync(model.Symbol, model.Quanity, model.Price, model.Side, model.OrderType, BinanceTimeInForce.GTC);
                if (orderResult.StatusEnum.HasFlag(BinanceTradeStatus.FILLED | BinanceTradeStatus.NEW | BinanceTradeStatus.PARTIALLY_FILLED))
                {
                    SuccessNotification("سفارش ثبت گردید");
                }
                else
                {
                    ErrorNotification(orderResult.Status);
                }
                return View();
            }
            catch (Exception ex)
            {
                ex.LogError();
                ErrorNotification(ex.Message);
            }
            return View();
        }

        public async Task<ActionResult> WithdrawHistory(string asset)
        {
            try
            {
                var model = new BinanceWithdrawHistoryViewModel()
                {
                    WithdrawHistory = await _binanceService.GetWithdrawHistoryAsync(asset)
                };
                ViewBag.Asset = asset;

                return View(model);
            }
            catch (Exception ex)
            {
                ex.LogError();
                ViewBag.Exception = ex.Message;
            }
            return View();
        }

        public async Task<ActionResult> DepositHistory(string asset)
        {
            try
            {
                var model = new BinanceDepositHistoryViewModel()
                {
                    DepositHistory = await _binanceService.GetDepositHistoryAsync(asset)
                };
                ViewBag.Asset = asset;
                return View(model);
            }
            catch (Exception ex)
            {
                ex.LogError();
                ViewBag.Exception = ex.Message;
            }
            return View();
        }

        public async Task<ActionResult> Balance()
        {
            var model = await GetBinanceBalanceAsync();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private async Task<BinanceBalanceViewModel> GetBinanceBalanceAsync()
        {
            var result = await _binanceService.GetAllBalancesAsync();
            var (usdPriceInToman, eurPriceInToman) = await _priceManager.USDEURPriceAsync();

            var model = new BinanceBalanceViewModel() { Assets = new List<BinanceBalanceItemViewModel>() };
            foreach (var item in result.Assets)
                model.Assets.Add(new BinanceBalanceItemViewModel()
                {
                    Free = item.Free,
                    FreeBTCValue = item.FreeBTCValue,
                    FreeUSDValue = item.FreeUSDValue,
                    Locked = item.Locked,
                    LockedBTCValue = item.LockedBTCValue,
                    LockedUSDValue = item.LockedUSDValue,
                    Name = item.Name,
                    UnitPriceInBTC = item.UnitPriceInBTC,
                    UnitPriceInUSD = item.UnitPriceInUSD,
                    UnitPriceInToman = item.UnitPriceInUSD * usdPriceInToman
                });

            model.TotalTomanValue = model.TotalUSDValue * usdPriceInToman;
            return model;
        }
        #endregion

        #region Table records fill methods
        public async Task<ActionResult> AddCurrencyTypesNetworksList()
        {
            var result = new ResultModel();
            try
            {
                var allCoinsInfo = await _binanceService.GetAllCoinsInformationAsync();
                var currencyTypes = await _currencyTypeService.TableNoTracking.ToListAsync();

                // Add blockchain networks first
                foreach (var currencyType in currencyTypes)
                {
                    var currencyTypeAsset = currencyType.Type.ToBinanceAsset();
                    var coinInfo = allCoinsInfo.Where(p => p.Coin == currencyTypeAsset).FirstOrDefault();
                    if (coinInfo != null)
                    {
                        foreach (var network in coinInfo.NetworkList)
                        {
                            var networkEntity = new BlockchainNetwork()
                            {
                                Name = network.Name,
                                AddressRegex = network.AddressRegex,
                                Network = network.Network,
                                MemoRegex = network.MemoRegex,
                                MinConfirm = network.MinConfirm,
                                SpecialTips = network.SpecialTips,
                                IsAvailable = true
                            };
                            await _blockchainNetworkService.AddIfNotExistsAsync(p => p.Name == network.Name && p.Network == network.Network, networkEntity);
                        }
                    }
                }

                // Get all added network entities
                var networks = await _blockchainNetworkService.TableNoTracking
                    .Select(p => new { p.Name, p.Network, p.Id })
                    .ToListAsync();

                // Add currency types networks
                var currencyTypeNetworksCount = 0;
                foreach (var currencyType in currencyTypes)
                {
                    var currencyTypeAsset = currencyType.Type.ToBinanceAsset();
                    var coinInfo = allCoinsInfo.Where(p => p.Coin == currencyTypeAsset).FirstOrDefault();
                    if (coinInfo != null)
                    {
                        foreach (var network in coinInfo.NetworkList)
                        {
                            var networkId = networks.Where(p => p.Name == network.Name && p.Network == network.Network).Select(p => p.Id).FirstOrDefault();
                            var currencyTypeNetwork = new CurrencyTypeNetwork()
                            {
                                BlockchainNetworkId = networkId,
                                CurrencyTypeId = currencyType.Id,
                                IsDefault = network.IsDefault,
                                WithdrawFee = network.WithdrawFee,
                                WithdrawEnable = network.WithdrawEnable,
                                DepositEnable = network.DepositEnable,
                                WithdrawMaximum = network.WithdrawMax,
                                WithdrawMinimum = network.WithdrawMin
                            };

                            await _currencyTypeNetworkService.AddIfNotExistsAsync(p => p.CurrencyTypeId == currencyType.Id && p.BlockchainNetworkId == networkId, currencyTypeNetwork);
                            currencyTypeNetworksCount++;
                        }
                    }
                }

                result.Text = $"{networks.Count} networks added and {currencyTypeNetworksCount} currency types networks added.";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Test Methods
        public async Task<ActionResult> CoinsInfo()
        {
            var result = await _binanceService.GetAllCoinsInformationAsync();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}