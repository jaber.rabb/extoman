﻿using System.ComponentModel;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.WebServices.Blockchain;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers.WebServices
{
    [MenuIcon("ti-world")]
    [DisplayName("جستجوگر بلاکچین")]
    public class ExplorerController : BaseAdminController
    {
        #region Properties
        private readonly IBlockCypherService _blockCypherService;
        #endregion

        #region Constructor
        public ExplorerController(IBlockCypherService blockCypherService)
        {
            _blockCypherService = blockCypherService;
        }
        #endregion

        #region Actions
        public async Task<ActionResult> Index()
        {
            var model = await _blockCypherService.GetBlockchainAsync(BlockcypherNetwork.Bitcoin);
            return View(model);
        }

        public async Task<ActionResult> Address(string id)
        {
            if (!id.HasValue())
                return HttpNotFound();

            var model = await _blockCypherService.GetAddressFullAsync(BlockcypherNetwork.Bitcoin, id, null, null, null, null, null, null, true, true, null);
            return View(model);
        }

        public async Task<ActionResult> Tx(string id)
        {
            if (!id.HasValue())
                return HttpNotFound();

            var model = await _blockCypherService.GetTransactionAsync(BlockcypherNetwork.Bitcoin, id, null, null, null, true, true);
            return View(model);
        }
        #endregion
    }
}