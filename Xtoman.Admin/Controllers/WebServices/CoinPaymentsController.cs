﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Payment.CoinPayments;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-wand")]
    [DisplayName("کوین پیمنتس")]
    public class CoinPaymentsController : BaseAdminController
    {
        private readonly ICoinPaymentsIPNResultService _coinPaymentsIPNResultService;
        private readonly ICoinPaymentsService _coinPaymentsService;
        private readonly IWithdrawManager _withdrawManager;
        public CoinPaymentsController(
            ICoinPaymentsIPNResultService coinPaymentsIPNResultService,
            ICoinPaymentsService coinPaymentsService,
            IWithdrawManager withdrawManager)
        {
            _coinPaymentsIPNResultService = coinPaymentsIPNResultService;
            _coinPaymentsService = coinPaymentsService;
            _withdrawManager = withdrawManager;
        }

        public async Task<ActionResult> Index()
        {
            var accountBalance = await _coinPaymentsService.CoinBalancesAsync();
            if (accountBalance.error.Equals("ok", StringComparison.InvariantCultureIgnoreCase))
            {
                var balancesViewModel = GetBalancesViewModel(accountBalance.result);
                var model = new CoinPaymentsViewModel()
                {
                    Balances = balancesViewModel,
                };
                return View(model);
            }
            return Json(accountBalance, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Withdraws(int page = 1, int psize = 30)
        {
            var withdraws = await _coinPaymentsService.WithdrawalHistoryAsync(new WithdrawalHistoryInput() {
                limit = psize,
                start = (page - 1) * psize
            });
            return View(withdraws.result);
        }

        [AjaxOnly]
        [HttpPost]
        public ActionResult WalletAddress(ECurrencyType currencyType, WalletApiType walletType)
        {
            var result = new WalletAddressResult();
            try
            {
                var address = _withdrawManager.GetOurWalletAddress(currencyType, walletType);
                if (address != null && address.Address.HasValue())
                {
                    result.Address = address.Address;
                    result.PayTagId = address.PayTagId;
                    result.Status = true;
                    result.Text = "آدرس دریافت شد.";
                    SetWithdrawWithAmountSession(address.Address, address.PayTagId);
                }
                else
                {
                    result.Text = "خطا در هنگام دریافت آدرس"; ;
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
                result.Text = ex.Message;
            }
            return Json(result);
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> Withdraw(ECurrencyType currencyType, WalletApiType walletType)
        {
            var result = new ResultModel();
            try
            {
                (string address, string tag) = GetWithdrawSession();
                if (address.HasValue())
                {
                    (bool status, string text) = await _withdrawManager.WithdrawCoinPaymentsToWalletAddressAsync(currencyType, address, tag);
                    result.Status = status;
                    result.Text = text;
                }
                else
                {
                    result.Text = "خطا در دریافت آدرس از Session";
                }
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }
            return Json(result);
        }

        public async Task<ActionResult> IPN(int page = 1, int psize = 30)
        {
            var model = await _coinPaymentsIPNResultService.TableNoTracking
                .OrderByDescending(p => p.InsertDate)
                .ProjectToPagedListAsync<CoinPaymentsIPNListItemViewModel>(page, psize);

            ViewBag.Total = await _coinPaymentsIPNResultService.TableNoTracking.CountAsync();
            return View(model);
        }

        public async Task<ActionResult> IPNDetail(int id)
        {
            var model = await _coinPaymentsIPNResultService.TableNoTracking.Where(p => p.Id == id)
                .ProjectToSingleAsync<CoinPaymentsIPNViewModel>();

            return View(model);
        }

        [HttpPost]
        [AjaxOnly]
        public async Task<ActionResult> ViewTransaction(string id)
        {
            var result = new ResultModel();
            try
            {
                var transactionInfo = await _coinPaymentsService.GetTransactionInformationAsync(id, true);
                result.Status = true;
                result.Text = RenderPartialViewToString("_ViewTransactionPartial", transactionInfo);
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }
            return Json(result);
        }

        private List<CoinPaymentsBalanceViewModel> GetBalancesViewModel(Dictionary<string, CoinBalanceItem> coinBalances)
        {
            var result = new List<CoinPaymentsBalanceViewModel>();
            foreach (var item in coinBalances)
            {
                var type = ECurrencyTypeHelper.ToECurrencyAccountType(item.Key);
                if (type.HasValue)
                {
                    result.Add(new CoinPaymentsBalanceViewModel()
                    {
                        Balance = item.Value.balancef,
                        IsActiveNow = item.Value.coin_status == "online" && item.Value.status == "available",
                        Type = type.Value
                    });
                }
            }
            return result;
        }

        private (string address, string tag) GetWithdrawSession()
        {
            var address = HttpContext.Session["withdrawaddress"]?.ToString();
            var tag = HttpContext.Session["withdrawtag"]?.ToString();
            return (address, tag);
        }

        private void SetWithdrawWithAmountSession(string address, string tag = "")
        {
            ClearWithdrawSession();
            HttpContext.Session["withdrawaddress"] = address;
            if (tag.HasValue())
                HttpContext.Session["withdrawtag"] = tag;
        }

        private void ClearWithdrawSession()
        {
            HttpContext.Session.Remove("withdrawaddress");
            HttpContext.Session.Remove("withdrawtag");
        }
    }
}