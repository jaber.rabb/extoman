﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.WebServices.TradingPlatform.Kraken;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-wand")]
    [DisplayName("کرکن")]
    public class KrakenController : Controller
    {
        private readonly IKrakenService _krakenService;
        public KrakenController(IKrakenService krakenService)
        {
            _krakenService = krakenService;
        }

        public ActionResult Index()
        {
            var krakenAssets = _krakenService.GetActiveAssets();
            var krakenBalance = _krakenService.GetBalance();
            var krakenTradeBalance = _krakenService.GetTradeBalance("currency", "ZUSD");
            //var krakenAssetPairs = _krakenService.GetAssetPairs();
            //var krakenOpenOrders = _krakenService.GetOpenOrders(true);
            //var krakenLastMonthClosedOrders = _krakenService.GetClosedOrders(true, null, DateTime.UtcNow.AddMonths(-1).ConvertToUnixTimestamp());

            var model = new KrakenViewModel()
            {
                Balance = krakenBalance,
                TradeBalance = krakenTradeBalance,
                Assets = krakenAssets
            };
            return View(model);
        }

        public ActionResult OpenOrders()
        {
            if (Request.IsAjaxRequest())
            {
                var model = new KrakenOpenOrdersViewModel()
                {
                    OpenOrders = _krakenService.GetOpenOrders(true)
                };
                return PartialView(model);
            }
            return HttpNotFound();
        }

        public ActionResult ClosedOrders(DateTime? startDate = null, DateTime? endDate = null, int? offSet = null)
        {
            if (Request.IsAjaxRequest())
            {
                var startDateTimeStamp = startDate.HasValue ? startDate.Value.ConvertToUnixTimestamp() : new long?();
                var endDateTimeStamp = endDate.HasValue ? endDate.Value.ConvertToUnixTimestamp() : new long?();
                var model = new KrakenClosedOrdersViewModel()
                {
                    ClosedOrders = _krakenService.GetClosedOrders(true, null, startDateTimeStamp, null, null, null)
                };
                return PartialView(model);
            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult CancelOrder(string txid)
        {
            var cancelOrderResult = _krakenService.CancelOpenOrder(txid);
            if (cancelOrderResult.ResultType == KrakenGeneralResultType.success)
                return Json(new ResultModel() { Status = true, Text = $"سفارش {txid} لغو شد!" });

            var text = cancelOrderResult.ResultType == KrakenGeneralResultType.error ? cancelOrderResult.Errors.Select(p => p.Value).FirstOrDefault() : cancelOrderResult.Exception.Message;
            return Json(new ResultModel() { Text = text });
        }

        [HttpGet]
        public ActionResult Trade()
        {
            var assets = _krakenService.GetAssetPairs();
            var model = new KrakenAddOrderViewModel()
            {
                AssetPairs = assets
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Trade(KrakenAddOrderViewModel model)
        {
            var orderFlags = model.OrderFlags.Where(p => p.IsSelected).Select(p => p.OrderFlag).ToList();
            var krakenAddOrderResult = _krakenService.AddStandardOrder(model.Pair, model.OrderSide, model.OrderType, model.Volume, model.Price, model.Price2, null, orderFlags);
            return Content(krakenAddOrderResult.Serialize());
        }
    }
}