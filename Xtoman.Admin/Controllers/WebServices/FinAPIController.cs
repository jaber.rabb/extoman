﻿using AutoMapper;
using System;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models.Api;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-arrow-up")]
    [DisplayName("وب سرویس فین")]
    public class FinAPIController : BaseAdminController
    {
        private readonly IUserApiKeyService _userApiKeyService;
        public FinAPIController(IUserApiKeyService userApiKeyService)
        {
            _userApiKeyService = userApiKeyService;
        }

        #region MyRegion
        public async Task<ActionResult> Index()
        {
            var apiKeys = await _userApiKeyService.TableNoTracking
                .ProjectToListAsync<UserApiKeyViewModel>();

            return View(apiKeys);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(int userId)
        {
            try
            {
                var ApiKey = "";
                var ApiSecret = "";
                using (var cryptoProvider = new RNGCryptoServiceProvider())
                {
                    ApiKey = Guid.NewGuid().ToString();
                    byte[] secretKeyByteArray = new byte[256]; //256 bit
                    cryptoProvider.GetBytes(secretKeyByteArray);
                    ApiSecret = Convert.ToBase64String(secretKeyByteArray);
                }
                var userApiKey = new UserApiKey()
                {
                    Key = ApiKey,
                    Secret = ApiSecret,
                    UserId = userId
                };
                await _userApiKeyService.InsertAsync(userApiKey);
                SuccessNotification("Api جدید اضافه شد.");
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _userApiKeyService.DeleteAsync(_userApiKeyService.GetById(id));
                SuccessNotification("Api Key حذف شد.");
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
            }
            return RedirectToAction("Index");
        }
        #endregion
    }
}