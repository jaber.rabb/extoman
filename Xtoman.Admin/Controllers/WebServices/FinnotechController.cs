﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-wand")]
    [DisplayName("فینوتک")]
    public class FinnotechController : BaseAdminController
    {
        private readonly IFinnotechService _finnotechService;
        private readonly IFinnotechResponseService _finnotechResponseService;
        private readonly IFinnotechResponseBalanceService _finnotechResponseBalanceService;
        private readonly IFinnotechResponseIdVerificationService _finnotechResponseIdVerificationService;
        private readonly IUserVerificationService _userVerificationService;
        private readonly IFinnotechResponseCardInfoService _finnotechResponseCardInfoService;
        //private readonly IFinnotechResponseTransferService _finnotechResponseTransferService;
        private readonly IUserService _userService;
        private readonly IUserBankCardService _userBankCardService;

        public FinnotechController(
            IFinnotechService finnotechService,
            IFinnotechResponseService finnotechResponseService,
            IFinnotechResponseBalanceService finnotechResponseBalanceService,
            IFinnotechResponseIdVerificationService finnotechResponseIdVerificationService,
            IFinnotechResponseCardInfoService finnotechResponseCardInfoService,
            IUserService userService,
            IUserBankCardService userBankCardService,
            IUserVerificationService userVerificationService)
        {
            _finnotechService = finnotechService;
            _finnotechResponseService = finnotechResponseService;
            _finnotechResponseBalanceService = finnotechResponseBalanceService;
            _finnotechResponseIdVerificationService = finnotechResponseIdVerificationService;
            _finnotechResponseCardInfoService = finnotechResponseCardInfoService;
            _userService = userService;
            _userBankCardService = userBankCardService;
            _userVerificationService = userVerificationService;
        }

        public async Task<ActionResult> Index()
        {
            var query = _finnotechResponseService.TableNoTracking;
            var model = new FinnotechIndexViewModel()
            {
                TotalRequests = await query.CountAsync(),
                TotalSuccessRequests = await query.Where(p => p.StatusCode == 200).CountAsync()
            };
            return View(model);
        }

        public async Task<ActionResult> History(int page = 1, int pageSize = 10)
        {
            var query = _finnotechResponseService.TableNoTracking;
            var model = await query.OrderByDescending(p => p.InsertDate)
                .ProjectToPagedListAsync<FinnotechHistoryItemViewModel>(page, pageSize);

            return View(model);
        }

        public async Task<ActionResult> HistoryDetail(int Id)
        {
            var entity = await _finnotechResponseService.TableNoTracking.Select(p => new { p.Id, p.FinType }).SingleAsync(p => p.Id == Id);
            switch (entity.FinType)
            {
                case FinnotechResponseType.FinnotechResponseBalance:
                    var balanceModel = await _finnotechResponseBalanceService.TableNoTracking
                        .ProjectToSingleAsync<FinnotechResponseBalanceViewModel>(p => p.Id == Id);
                    return View("HistoryBalance", balanceModel);
                case FinnotechResponseType.FinnotechResponseCardInfo:
                    var cardInfoModel = await _finnotechResponseCardInfoService.TableNoTracking
                        .ProjectToSingleAsync<FinnotechResponseCardInfoViewModel>(p => p.Id == Id);
                    return View("HistoryCardInfo", cardInfoModel);
                case FinnotechResponseType.FinnotechResponseIdVerification:
                    var idVerificationModel = await _finnotechResponseIdVerificationService.TableNoTracking
                        .ProjectToSingleAsync<FinnotechResponseIdVerificationViewModel>(p => p.Id == Id);
                    return View("HistoryIdVerification", idVerificationModel);
                case FinnotechResponseType.FinnotechResponsePayaReport:
                    break;
                //case FinnotechResponseType.FinnotechResponseTransferTo:
                //    var transferModel = await _finnotechResponseTransferService.TableNoTracking
                //        .ProjectToSingleAsync<FinnotechResponseTransferToViewModel>(p => p.Id == Id);
                //    return View("HistoryTransfer", transferModel);
            }
            return null;
        }

        //public async Task<ActionResult> Balance()
        //{
        //    var result = await _finnotechService.BalanceV2Async();
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        //public async Task<ActionResult> Transfer()
        //{
        //    var result = await _finnotechService.TransferToAsync(50000, "IR060120000000008379344309", "مهدی", "زمانیان", "253", "تست");
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> UserCardInfo(int Id, int CardId)
        {
            var resultModel = new FinnotechUserCardVerificationViewModel();
            var user = await _userService.Table
                .Where(p => p.Id == Id)
                .Include(p => p.UserBankCards)
                .Include(p => p.UserBankCards.Select(x => x.FinnotechHistory))
                .SingleAsync();

            var userCard = user.UserBankCards.Where(p => p.Id == CardId).FirstOrDefault();
            if (userCard != null)
            {
                if (!userCard.FinnotechHistoryId.HasValue)
                {
                    var cardInfo = await _finnotechService.GetCardInformationAsync(userCard.CardNumber);
                    if (cardInfo != null && cardInfo.StatusCode == 200)
                    {
                        var bankCard = await _userBankCardService.Table.SingleAsync(p => p.Id == CardId);
                        bankCard.FinnotechHistoryId = cardInfo.HistoryId;
                        await _userBankCardService.UpdateAsync(bankCard);
                        return Json(new { Status = true, Text = "موفق", cardInfo.Result.Name });
                    }
                }
                else
                {
                    var cardInfo = await _finnotechResponseCardInfoService.TableNoTracking.ProjectToSingleAsync<FinnotechResponseCardInfoViewModel>(p => p.Id == userCard.FinnotechHistoryId);
                    return Json(new { Status = true, Text = "موفق", cardInfo.Name });
                }
            }

            return Json(new { Status = false, Text = "نا مشخص", Name = "نا مشخص" });
        }

        public async Task<ActionResult> CardInfo(string id)
        {
            if (!id.HasValue())
                return View();

            var result = await _finnotechService.GetCardInformationAsync(id);
            var model = new FinnotechCardInfoViewModel()
            {
                Description = result.Result?.Description ?? result.Error?.Message,
                DestCard = result.Result?.DestCard,
                DoTime = result.Result?.DoTime,
                Name = result.Result?.Name,
                Result = result.Result?.Result,
                TrackId = new Guid(result.TrackId)
            };
            return View("CardInfoResult", model);
        }

        [HttpGet]
        public async Task<ActionResult> IdVerification(int id = 0)
        {
            if (!AppSettingManager.IsLocale && (DateTime.Now.Hour >= 20 || DateTime.Now.Hour < 7))
                return View("IdVerificationTime");
            if (id > 0)
            {
                var user = await _userService.TableNoTracking
                    .Where(p => p.Id == id).Select(p => new { p.FirstName, p.LastName, p.FatherName, p.NationalCode, p.BirthDate, p.VerificationMedia })
                    .FirstOrDefaultAsync();

                if (user != null)
                {
                    var persianCalendar = new PersianCalendar();
                    var birthDate = user.BirthDate.ToShamsi();
                    var model = new FinnotechIdVerificationViewModel()
                    {
                        UserId = id,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        FatherName = user.FatherName,
                        NationalCode = user.NationalCode,
                        BirthDate = birthDate,
                        VerificationMedia = new MediaViewModel().FromEntity(user.VerificationMedia)
                    };
                    return View(model);
                }
            }
            return View(new FinnotechIdVerificationViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> IdVerification(FinnotechIdVerificationViewModel model)
        {
            var result = new FinnotechIdVerificationResult();
            if (model.UserId.HasValue && model.UserId.Value > 0 && model.New == 0)
            {
                var user = await _userService.TableNoTracking.Include(p => p.Finnotechs).SingleAsync(p => p.Id == model.UserId.Value);
                if (user.Finnotechs != null && user.Finnotechs.Any(p => p.FinType == FinnotechResponseType.FinnotechResponseIdVerification))
                {
                    var finnotechResponseIdVerifications = user.Finnotechs
                        .Where(p => p.FinType == FinnotechResponseType.FinnotechResponseIdVerification)
                        .OfType<FinnotechResponseIdVerification>()
                        .ToList();
                    var history = finnotechResponseIdVerifications.OrderByDescending(p => p.Id).FirstOrDefault();
                    if (history != null)
                    {
                        result = new FinnotechIdVerificationResult()
                        {
                            BirthDate = history.BirthDate,
                            Code = history.Code,
                            DeathStatus = history.DeathStatus,
                            FatherName = history.FatherName,
                            FirstName = history.FirstName,
                            FirstNameSimilarity = history.FirstNameSimilarity,
                            FullName = history.FullName,
                            FullNameSimilarity = history.FullNameSimilarity,
                            HistoryId = history.Id,
                            LastName = history.LastName,
                            LastNameSimilarity = history.LastNameSimilarity,
                            Message = history.Message,
                            NationalId = history.NationalId,
                            Status = history.Status,
                            StatusCode = history.StatusCode,
                            TrackId = history.TrackId
                        };
                    }
                }
            }
            if (result.HistoryId == 0)
            {
                var dateSplit = model.BirthDate.Split('/');
                var birthDate = dateSplit[0] + "/" + (dateSplit[1].ToInt() > 9 ? dateSplit[1] : "0" + dateSplit[1].ToInt()) + "/" + (dateSplit[2].ToInt() > 9 ? dateSplit[2] : "0" + dateSplit[2].ToInt());

                if (model.Json != 0)
                {
                    var stringResult = await _finnotechService.GetNationalVerificationStringAsync(model.NationalCode, birthDate, model.FullName, model.FirstName, model.LastName, model.FatherName, model.UserId);
                    return Content(stringResult);
                }

                var finnotechResult = await _finnotechService.GetNationalVerificationAsync(model.NationalCode, birthDate, model.FullName, model.FirstName, model.LastName, model.FatherName, model.UserId);

                if (model.UserId.HasValue && model.UserId.Value > 0)
                {
                    var user = await _userService.TableNoTracking.SingleAsync(p => p.Id == model.UserId.Value);

                    var description = $"مقادیر ارسالی: {model.SerializeDisplay()} نتیجه صحت سنجی از فینوتیک: {finnotechResult.SerializeDisplay()}";

                    var adminUserId = User.Identity.GetUserId<int>();
                    await _userVerificationService.ChangedAsync(user.Id, UserVerificationType.FinnotechVerify, user.IdentityVerificationStatus, description, adminUserId, user.VerificationMediaId);
                }

                result = new FinnotechIdVerificationResult()
                {
                    BirthDate = finnotechResult.Result?.BirthDate ?? model.BirthDate,
                    Code = finnotechResult.Error?.Code,
                    DeathStatus = finnotechResult.Result?.DeathStatus,
                    FatherName = finnotechResult.Result?.FatherName ?? model.FatherName,
                    FatherNameSimilarity = finnotechResult.Result?.FatherNameSimilarity,
                    FirstName = finnotechResult.Result?.FirstName ?? model.FirstName,
                    FirstNameSimilarity = finnotechResult.Result?.FirstNameSimilarity,
                    FullName = finnotechResult.Result?.FullName ?? model.FullName,
                    FullNameSimilarity = finnotechResult.Result?.FullNameSimilarity,
                    HistoryId = finnotechResult.HistoryId,
                    LastName = finnotechResult.Result?.LastName ?? model.LastName,
                    LastNameSimilarity = finnotechResult.Result?.LastNameSimilarity,
                    Message = finnotechResult.Error?.Message,
                    NationalId = finnotechResult.Result?.NationalCode ?? model.NationalCode,
                    Status = finnotechResult.Status.ToDisplay(),
                    StatusCode = finnotechResult.StatusCode,
                    TrackId = finnotechResult.TrackId
                };
            }

            return View("IdVerificationResult", result);
        }
    }
}