﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers.WebServices
{
    [MenuIcon("ti-wand")]
    [DisplayName("وندار")]
    public class VandarController : BaseAdminController
    {
        #region Properties
        private readonly IVandarV2Service _vandarV2Service;
        #endregion

        public VandarController(
            IVandarV2Service vandarV2Service)
        {
            _vandarV2Service = vandarV2Service;
        }

        #region Actions
        public async Task<ActionResult> Index()
        {
            var result = await _vandarV2Service.GetBusinessAsync();
            if (result.Status == 1)
                return View(result.Data);

            throw new Exception($"خطا در هنگام دریافت اطلاعات وندار: {(result.Message.HasValue() ? result.Message : result.Error)}");
        }

        #region IBAN
        public async Task<ActionResult> IBAN()
        {
            var result = await _vandarV2Service.GetBusinessIBANsAsync();
            if (result.Status == 1)
                return View(result.Data);

            throw new Exception($"خطا در هنگام دریافت اطلاعات وندار: {(result.Message.HasValue() ? result.Message : result.Error)}");
        }

        [HttpPost]
        [AjaxOnly]
        public async Task<ActionResult> AddIBAN(string IBAN)
        {
            var resultModel = new ResultModel();
            try
            {
                var result = await _vandarV2Service.AddNewIBANForBusinessAsync(IBAN);
                if (result.Status == 1)
                {
                    resultModel.Status = true;
                    resultModel.Text = "شماره حساب جدید اضافه شد.";
                }
            }
            catch (Exception ex)
            {
                resultModel.Text = ex.Message;
            }
            return Json(resultModel);
        }

        [HttpPost]
        [AjaxOnly]
        public async Task<ActionResult> DeleteIBAN(int id)
        {
            var resultModel = new ResultModel();
            try
            {
                var result = await _vandarV2Service.DeleteBusinessIBANAsync(id.ToString());
                if (result.Status == 1)
                {
                    resultModel.Status = true;
                    resultModel.Text = "شماره حساب حذف شد.";
                }
            }
            catch (Exception ex)
            {
                resultModel.Text = ex.Message;
            }
            return Json(resultModel);
        }
        #endregion

        public async Task<ActionResult> IPG()
        {
            var result = await _vandarV2Service.GetBusinessIPGAsync();
            if (result.Status == 1)
                return View(result.Data);

            throw new Exception($"خطا در هنگام دریافت اطلاعات وندار: {(result.Message.HasValue() ? result.Message : result.Error)}");
        }
        public async Task<ActionResult> Transaction(int page = 1, int psize = 10)
        {
            var result = await _vandarV2Service.GetBusinessTransactionsAsync(page, per_page:psize);
            if (result.Status == 1)
                return View(result.Data);

            throw new Exception($"خطا در هنگام دریافت اطلاعات وندار: {(result.Message.HasValue() ? result.Message : result.Error)}");
        }
        public async Task<ActionResult> Settlement(int page = 1, int psize = 10)
        {
            var result = await _vandarV2Service.GetSettlementsListAsync(page);
            if (result.Status == 1)
                return View(result.Data);

            throw new Exception($"خطا در هنگام دریافت اطلاعات وندار: {(result.Message.HasValue() ? result.Message : result.Error)}");
        }
        
        [HttpPost]
        [AjaxOnly]
        public async Task<ActionResult> CancelSettlement(long id)
        {
            var resultModel = new ResultModel();
            try
            {
                var result = await _vandarV2Service.DeleteSettlementAsync(id.ToString());
                if (result.Status == 1)
                {
                    resultModel.Status = true;
                    resultModel.Text = result.Message;
                }
            }
            catch (Exception ex)
            {
                resultModel.Text = ex.Message;
            }
            return Json(resultModel);
        }
        #endregion
    }
}