﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-export")]
    [DisplayName("خروجی گرفتن اطلاعات")]
    public class ExportController : BaseAdminController
    {
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly IUserService _userService;
        public ExportController(
            IExchangeOrderService exchangeOrderService,
            IUserService userService)
        {
            _exchangeOrderService = exchangeOrderService;
            _userService = userService;
        }

        public async Task<ActionResult> Index(string id)
        {
            var lastMonth = DateTime.Now.AddMonths(-1);
            var query = _exchangeOrderService.TableNoTracking
                .Where(p => p.PaymentStatus == PaymentStatus.Paid && p.OrderStatus == OrderStatus.Complete)
                .Where(p => p.InsertDate >= lastMonth)
                .Select(p => new
                {
                    p.Id,
                    p.ReceiveAmount,
                    p.PaidAmount,
                    p.PayAmount,
                    p.Exchange,
                    p.InsertDate,
                    p.User,
                    p.InsertUserIpAddress,
                    p.ReceiveAddress
                });

            switch (id)
            {
                case "buy":
                    query = query.Where(p => p.Exchange.FromCurrency.Type == ECurrencyType.Shetab);
                    break;
                case "sell":
                    query = query.Where(p => p.Exchange.ToCurrency.Type == ECurrencyType.Shetab);
                    break;
                case "exchange":
                    query = query.Where(p => p.Exchange.ToCurrency.Type != ECurrencyType.Shetab && p.Exchange.FromCurrency.Type != ECurrencyType.Shetab);
                    break;
                default:
                    break;
            }

            var data = await query.OrderBy(p => p.InsertDate).ToListAsync();

            var dataForGrid = data
                .Select(p =>
                new {
                    p.Id,
                    Receive = p.ReceiveAmount.ToBlockChainPrice(p.Exchange.ToCurrency.MaxPrecision) + " " + p.Exchange.ToCurrency.UnitSign,
                    Pay = (p.PaidAmount > 0 ? p.PaidAmount.ToBlockChainPrice(p.Exchange.FromCurrency.MaxPrecision) : p.PayAmount.ToBlockChainPrice(p.Exchange.FromCurrency.MaxPrecision)) + " " + p.Exchange.FromCurrency.UnitSign,
                    ExchangeType = "خرید " + p.Exchange.ToCurrency.Name,
                    Date = p.InsertDate.ToFaDateTimeZone(),
                    FullName = p.User.FirstName + " " + p.User.LastName,
                    IP = p.InsertUserIpAddress,
                    p.User.PhoneNumber,
                    p.User.Email,
                    p.ReceiveAddress
                })
                .ToList();

            var gv = new GridView
            {
                DataSource = dataForGrid
            };
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=DemoExcel.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = Encoding.UTF8.EncodingName;
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
            return View();
        }

        public async Task<ActionResult> Customers()
        {
            //var userQuery = _userService.TableNoTracking
            //    .Where(p => p.Orders.Any(x => x.OrderStatus == OrderStatus.Complete));

            //var totalUsersWithCompletedOrders = userQuery.CountAsync();

            var orderQuery = _exchangeOrderService.TableNoTracking
                .Where(p => p.PaymentStatus == PaymentStatus.Paid || p.OrderStatus == OrderStatus.Complete);

            var bitcoinUsers = await orderQuery.Where(p => (p.Exchange.FromCurrency.Type == ECurrencyType.Bitcoin && p.Exchange.ToCurrency.Type == ECurrencyType.Shetab) || (p.Exchange.ToCurrency.Type == ECurrencyType.Bitcoin && p.Exchange.FromCurrency.Type == ECurrencyType.Shetab)).CountAsync();

            var altCoinUsers = await orderQuery.Where(p => (!p.Exchange.FromCurrency.IsFiat && p.Exchange.FromCurrency.Type != ECurrencyType.Bitcoin && p.Exchange.ToCurrency.Type == ECurrencyType.Shetab)
                                                  || (!p.Exchange.ToCurrency.IsFiat && p.Exchange.ToCurrency.Type != ECurrencyType.Bitcoin && p.Exchange.FromCurrency.Type == ECurrencyType.Shetab)).CountAsync();

            var perfectUsers = await orderQuery.Where(p => (p.Exchange.FromCurrency.Type == ECurrencyType.Shetab && (p.Exchange.ToCurrency.Type == ECurrencyType.PerfectMoney || p.Exchange.ToCurrency.Type == ECurrencyType.PerfectMoneyVoucher)) || 
                                                     (p.Exchange.ToCurrency.Type == ECurrencyType.Shetab && (p.Exchange.FromCurrency.Type == ECurrencyType.PerfectMoney || p.Exchange.FromCurrency.Type == ECurrencyType.PerfectMoneyVoucher))).CountAsync();

            var coinToCoinUsers = await orderQuery.Where(p => p.Exchange.FromCurrency.Type != ECurrencyType.Shetab && p.Exchange.ToCurrency.Type != ECurrencyType.Shetab).CountAsync();

            return Content($"Bitcoin Users:{bitcoinUsers} | Altcoin Users:{altCoinUsers} | Perfect Users:{perfectUsers} | Coint to Coin Users:{coinToCoinUsers}");
        }
    }
}