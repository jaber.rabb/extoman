﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-user")]
    [DisplayName("کاربران")]
    public class UserController : BaseAdminController
    {
        #region Properties
        private readonly IUserService _userService;
        private readonly IMessageManager _messageManager;
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly ISupportTicketService _supportTicketService;
        private readonly IUserVerificationService _userVerificationService;
        private readonly IAppUserManager _userManager;
        #endregion

        #region Constructor
        public UserController(
            IUserService userService,
            IMessageManager messageManager,
            IExchangeOrderService exchangeOrderService,
            ISupportTicketService supportTicketService,
            IUserVerificationService userVerificationService,
            IAppUserManager userManager)
        {
            _userService = userService;
            _messageManager = messageManager;
            _exchangeOrderService = exchangeOrderService;
            _supportTicketService = supportTicketService;
            _userVerificationService = userVerificationService;
            _userManager = userManager;
        }
        #endregion

        #region Actions
        public ActionResult Index()
        {
            //var model = await _userService.TableNoTracking
            //    .OrderByDescending(p => p.Id)
            //    .ProjectToPagedListAsync<UserViewModel>(page, psize);

            return View();
        }

        public async Task<ActionResult> Search(UserSearchTerms term, int page = 1, int pageSize = 30)
        {
            var query = _userService.TableNoTracking;
            if (term.Text.HasValue())
            {
                var termInt = term.Text.TryToInt();
                query = query.Where(p => p.FirstName == term.Text ||
                                         p.LastName == term.Text ||
                                         (p.FirstName + " " + p.LastName) == term.Text ||
                                         p.Email == term.Text || p.PhoneNumber == term.Text ||
                                         p.Id == termInt ||
                                         p.Telephone == term.Text ||
                                         p.UserBankCards.Any(x => x.CardNumber == term.Text));

            }

            if (term.FromId.HasValue)
                query = query.Where(p => p.Id >= term.FromId);

            if (term.ToId.HasValue)
                query = query.Where(p => p.Id <= term.ToId);

            if (term.Gender.HasValue)
            {
                var gender = term.Gender.Value == 0;
                query = query.Where(p => p.Gender == gender);
            }

            if (term.IsPhisher.HasValue)
            {
                var IsPhisher = term.IsPhisher.Value == 1;
                query = query.Where(p => p.IsPhisher == IsPhisher);
            }

            if (term.VerifyStatus.HasValue)
            {
                switch (term.VerifyStatus.Value)
                {
                    case VerificationModelStatus.IdentityNotSent:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.NotSent);
                        break;
                    case VerificationModelStatus.IdentityPending:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Pending);
                        break;
                    case VerificationModelStatus.IdentityConfirmed:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed);
                        break;
                    case VerificationModelStatus.IdentityNotConfirmed:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.NotConfirmed);
                        break;
                    case VerificationModelStatus.TelephoneNotEntered:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && p.Telephone == null);
                        break;
                    case VerificationModelStatus.TelephonePending:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && p.TelephoneConfirmationStatus == VerificationStatus.Pending);
                        break;
                    case VerificationModelStatus.TelephoneNotConfirmed:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && p.TelephoneConfirmationStatus == VerificationStatus.NotConfirmed);
                        break;
                    case VerificationModelStatus.TelephoneConfirmed:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && p.TelephoneConfirmationStatus == VerificationStatus.Confirmed);
                        break;
                    case VerificationModelStatus.PhoneNumberNotEntered:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && p.PhoneNumber == null);
                        break;
                    case VerificationModelStatus.PhoneNumberNotConfirmed:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && p.PhoneNumber != null && !p.PhoneNumberConfirmed);
                        break;
                    case VerificationModelStatus.PhoneNumberConfirmed:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && p.PhoneNumber != null && p.PhoneNumberConfirmed);
                        break;
                    case VerificationModelStatus.EmailNotEntered:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && p.Email == null);
                        break;
                    case VerificationModelStatus.EmailNotConfirmed:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && p.Email != null && !p.EmailConfirmed);
                        break;
                    case VerificationModelStatus.EmailConfirmed:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && p.Email != null && p.EmailConfirmed);
                        break;
                    case VerificationModelStatus.BankCardNotEntered:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && !p.UserBankCards.Any());
                        break;
                    case VerificationModelStatus.BankCardPending:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && p.UserBankCards.Any(x => x.VerificationStatus == VerificationStatus.Pending));
                        break;
                    case VerificationModelStatus.BankCardNotConfirmed:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && !p.UserBankCards.Any(x => x.VerificationStatus == VerificationStatus.Confirmed) && p.UserBankCards.Any(x => x.VerificationStatus == VerificationStatus.NotConfirmed));
                        break;
                    case VerificationModelStatus.BankCardConfirmed:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && p.UserBankCards.Any(x => x.VerificationStatus == VerificationStatus.Confirmed));
                        break;
                    case VerificationModelStatus.Confirmed:
                        query = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && p.EmailConfirmed && p.PhoneNumberConfirmed && p.TelephoneConfirmationStatus == VerificationStatus.Confirmed && p.UserBankCards.Any(x => x.VerificationStatus == VerificationStatus.Confirmed));
                        break;
                }
            }

            var count = await query.CountAsync();
            var list = await query.ProjectToPagedListAsync<UserViewModel>(page, pageSize);

            var totalVerified = query.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed && p.EmailConfirmed && p.PhoneNumberConfirmed && p.TelephoneConfirmationStatus == VerificationStatus.Confirmed && p.UserBankCards.Any(x => x.VerificationStatus == VerificationStatus.Confirmed));
            ViewBag.TotalVerified = await totalVerified
                .CountAsync();

            ViewBag.TotalLoyal = await totalVerified
                .Where(p => p.Orders.Where(x => x.OrderStatus == OrderStatus.Complete).Count() >= 3)
                .CountAsync();

            ViewBag.TotalCount = count;

            return PartialView("_List", list);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            var model = await _userService.TableNoTracking.Where(p => p.Id == id)
                .ProjectToSingleAsync<EditUserViewModel>();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditUserViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var entity = await _userService.Table
                .Where(p => p.Id == model.Id)
                .SingleAsync();

            try
            {
                model.ToEntity(entity);
                await _userService.UpdateAsync(entity);
                SuccessNotification($"اطلاعات حساب کاربری {model.UserName} به تغییر یافت.");
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }

            return View(model);
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> ChangePassword(string userId, string password)
        {
            var result = new ResultModel();
            try
            {
                var uId = userId.TryToInt();
                if (uId <= 0)
                {
                    uId = await _userService.TableNoTracking.Where(p => p.PhoneNumber.Equals(userId, StringComparison.InvariantCultureIgnoreCase) || p.Email.Equals(userId, StringComparison.InvariantCultureIgnoreCase)).Select(p => p.Id).FirstOrDefaultAsync();
                }
                var removePassResult = await _userManager.RemovePasswordAsync(uId);
                if (removePassResult.Succeeded)
                {
                    var addPassResult = await _userManager.AddPasswordAsync(uId, password);
                    if (addPassResult.Succeeded)
                    {
                        result.Status = true;
                        result.Text = "کلمه عبور تغییر کرد.";
                        return Json(result);
                    }
                }
                result.Text = "خطایی رخ داد.";
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
                ex.LogError();
            }
            return Json(result);
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> ConfirmEmail(string userId)
        {
            var result = new ResultModel();
            try
            {
                var uId = userId.TryToInt();
                if (uId <= 0)
                {
                    uId = await _userService.TableNoTracking.Where(p => p.PhoneNumber.Equals(userId, StringComparison.InvariantCultureIgnoreCase) || p.Email.Equals(userId, StringComparison.InvariantCultureIgnoreCase)).Select(p => p.Id).FirstOrDefaultAsync();
                }
                var user = await _userService.Table.SingleAsync(p => p.Id == uId);
                user.EmailConfirmed = true;
                await _userService.UpdateAsync(user);
                var adminId = User.Identity.GetUserId<int>();
                await _userVerificationService.ChangedAsync(uId, UserVerificationType.Email, VerificationStatus.Confirmed, "ایمیل دستی تایید شد.", adminId);
                result.Status = true;
                result.Text = $"ایمیل {user.Email} تایید شد.";
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
                ex.LogError();
            }
            return Json(result);
        }

        public async Task<ActionResult> Detail(string id)
        {
            var userId = id.TryToInt();
            if (userId <= 0)
            {
                userId = await _userService.TableNoTracking
                    .Where(p => p.PhoneNumber.Equals(id, StringComparison.InvariantCultureIgnoreCase) || p.Email.Equals(id, StringComparison.InvariantCultureIgnoreCase))
                    .Select(p => p.Id)
                    .FirstOrDefaultAsync();
            }
            var model = await _userService.TableNoTracking
                .Where(p => p.Id == userId)
                .ProjectToSingleAsync<UserDetailViewModel>();

            model.ExchangeOrders = await _exchangeOrderService.TableNoTracking
                .Where(p => p.UserId == userId)
                .ProjectToListAsync<ExchangeOrdersListItemViewModel>();

            model.Supports = await _supportTicketService.TableNoTracking
                .Where(p => p.UserName == model.UserName || p.InsertUserId == userId || p.UpdateUserId == userId)
                .ProjectToListAsync<SupportTicketListItemViewModel>();

            model.UserVerifications = await _userVerificationService.TableNoTracking.Where(p => p.UserId == userId)
                .ProjectToListAsync<UserVerificationViewModel>();

            model.Referrals = await _userService.TableNoTracking.Where(p => p.ReferrerId == userId)
                .ProjectToListAsync<UserSimpleViewModel>();

            return View(model);
        }

        public async Task<ActionResult> Potential(string id)
        {
            if (id.HasValue())
            {
                switch (id)
                {
                    case "VerifiedWithNoOrder":
                        return View("VerifiedWithNoOrder");
                    case "PartialVerification":
                        return View("PartialVerification");
                    case "NotSeenForLongDays":
                        return View("NotSeenForLongDays");
                    case "IdentityVerificationFailed":
                        return View("IdentityVerificationFailed");
                }
            }

            var past3Month = DateTime.UtcNow.AddMonths(-3);
            var query = _userService.TableNoTracking.Where(p => !p.IsPhisher);

            var notSeenFor3MonthsCount = await query
                .Where(p => p.Orders.Any(x => x.PaymentStatus == PaymentStatus.Paid))
                .Where(p => p.Orders.OrderByDescending(x => x.InsertDate).Select(x => x.InsertDate).FirstOrDefault() < past3Month)
                .CountAsync();

            var verifiedWithNoOrderCount = await query
                .Where(p => p.TelephoneConfirmationStatus == VerificationStatus.Confirmed
                         && p.EmailConfirmed
                         && p.PhoneNumberConfirmed
                         && p.IdentityVerificationStatus == VerificationStatus.Confirmed
                         && p.UserBankCards.Any(x => x.VerificationStatus == VerificationStatus.Confirmed)
                         && !p.Orders.Any(x => x.PaymentStatus == PaymentStatus.Paid))
                .CountAsync();

            var partialVerifiedCount = await query
                .Where(p => !(p.TelephoneConfirmationStatus == VerificationStatus.Confirmed
                         && p.EmailConfirmed
                         && p.PhoneNumberConfirmed
                         && p.IdentityVerificationStatus == VerificationStatus.Confirmed
                         && p.UserBankCards.Any(x => x.VerificationStatus == VerificationStatus.Confirmed))
                         && (p.IdentityVerificationStatus == VerificationStatus.Confirmed || p.EmailConfirmed || p.PhoneNumberConfirmed || p.TelephoneConfirmationStatus == VerificationStatus.Confirmed || p.UserBankCards.Any()))
                .CountAsync();

            var usersWithCompletedOrders = await query
                .Where(p => p.Orders.Any(x => x.PaymentStatus == PaymentStatus.Paid))
                .CountAsync();

            var identityVerificationFailedsCount = await query
                .Where(p => p.IdentityVerificationStatus == VerificationStatus.NotConfirmed && p.PhoneNumberConfirmed)
                .CountAsync();

            var model = new PotentialUsersViewModel()
            {
                NotSeenFor3MonthsCount = notSeenFor3MonthsCount,
                VerifiedWithNoOrderCount = verifiedWithNoOrderCount,
                PartialVerificationCount = partialVerifiedCount,
                UsersWithCompletedOrdersCount = usersWithCompletedOrders,
                IdentityVerificationFailedsCount = identityVerificationFailedsCount
            };

            return View(model);
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> PhisherStatus(int UserId, int IsPhisher)
        {
            var result = new ResultModel();
            try
            {
                var user = await _userService.Table.SingleAsync(p => p.Id == UserId);
                user.IsPhisher = IsPhisher.ToBoolean();
                await _userService.UpdateAsync(user);
                result.Text = user.IsPhisher ? "کاربر Phisher است." : "کاربر Phisher نیست.";
                result.Status = true;
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
                ex.LogError();
            }
            return Json(result);
        }

        public async Task<ActionResult> LimitedStatus(int UserId, int IsLimited)
        {
            var result = new ResultModel();
            try
            {
                var user = await _userService.Table.SingleAsync(p => p.Id == UserId);
                user.IsBuyLimited = IsLimited.ToBoolean();
                await _userService.UpdateAsync(user);
                result.Text = user.IsPhisher ? "کاربر محدود است." : "کاربر محدود نیست.";
                result.Status = true;
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
                ex.LogError();
            }
            return Json(result);
        }

        [AjaxOnly]
        public async Task<ActionResult> Select2Ajax(string search = "")
        {
            if (search.HasValue())
            {
                search = search.Trim();
                var users = await _userService.TableNoTracking
                    .Where(p => p.FirstName.Contains(search) || p.LastName.Contains(search) || (p.FirstName + " " + p.LastName).Contains(search) ||
                        p.Email.Contains(search) || p.PhoneNumber.Contains(search) || p.NationalCode.Contains(search))
                        .Select(p => new { text = (p.FirstName + " " + p.LastName + " (" + p.UserName + ")").Trim(), id = p.Id }).ToListAsync();

                return Json(users, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        public async Task<ActionResult> IdVerificationUser(int id)
        {
            var user = await _userService.TableNoTracking
                .Where(p => p.Id == id)
                .Select(p => new { p.FirstName, p.LastName, p.FatherName, p.NationalCode, p.BirthDate })
                .SingleAsync();
            if (user != null)
            {
                return Json(new { Status = true, Text = "دریافت اطلاعات با موفقیت انجام شد.", user.FirstName, user.LastName, user.FatherName, user.NationalCode, BirthDate = user.BirthDate.ToShamsi() }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Status = false, Text = "خطا در هنگام دریافت اطلاعات کاربر." }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}