﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Service.WebServices;
using Xtoman.Utility;
using Xtoman.Utility.PostalEmail;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-thumb-up")]
    [DisplayName("احراز هویت")]
    public class VerificationController : BaseAdminController
    {
        #region Properties
        private readonly IUserService _userService;
        private readonly IUserBankCardService _userBankCardService;
        private readonly IMessageManager _messageManager;
        private readonly IUserVerificationService _userVerificationService;
        private readonly IFinnotechService _finnotechService;
        private readonly IFinnotechResponseCardInfoService _finnotechResponseCardInfoService;
        private readonly IVerificationManager _verificationManager;
        #endregion

        #region Constructor
        public VerificationController(
            IUserService userService,
            IUserBankCardService userBankCardService,
            IMessageManager messageManager,
            IUserVerificationService userVerificationService,
            IFinnotechService finnotechService,
            IFinnotechResponseCardInfoService finnotechResponseCardInfoService,
            IVerificationManager verificationManager)
        {
            _userService = userService;
            _userBankCardService = userBankCardService;
            _messageManager = messageManager;
            _userVerificationService = userVerificationService;
            _finnotechService = finnotechService;
            _finnotechResponseCardInfoService = finnotechResponseCardInfoService;
            _verificationManager = verificationManager;
        }
        #endregion

        #region Actions
        public async Task<ActionResult> Index()
        {
            var identityVerifications = await _userService.TableNoTracking
                .Where(p => p.IdentityVerificationStatus == VerificationStatus.Pending)
                .CountAsync();

            var telephoneVerifications = await _userService.TableNoTracking
                .Where(p => p.TelephoneConfirmationStatus.HasFlag(VerificationStatus.Pending | VerificationStatus.NotSent))
                .CountAsync();

            var bankCardVerifications = await _userBankCardService.TableNoTracking
                .Where(p => p.VerificationStatus.HasFlag(VerificationStatus.Pending | VerificationStatus.NotSent))
                .CountAsync();

            var model = new VerificationIndexViewModel()
            {
                BankCardVerifications = bankCardVerifications,
                IdentityVerifications = identityVerifications,
                TelephoneVerifications = telephoneVerifications
            };
            return View(model);
        }

        public async Task<ActionResult> Identities()
        {
            var model = await _userService.TableNoTracking.Where(p => p.IdentityVerificationStatus == VerificationStatus.Pending || (p.IdentityVerificationStatus == VerificationStatus.NotSent && p.MediaId != null))
                .OrderBy(p => p.VerificationMedia.AddDate)
                .ProjectToListAsync<IdentityVerificationViewModel>();

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Identity(int id)
        {
            var model = await _userService.TableNoTracking
                .Where(p => p.Id == id)
                .ProjectToSingleAsync<IdentityVerificationViewModel>();

            ViewBag.IsPhotoshop = AppSettingManager.IsLocale ? false : ImageMagickHelper.CheckIsPhotoshop(model.VerificationMedia.Url.ToStaticFilePathAdmin());
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Identity(int id, int accepted, string desc)
        {
            var result = new ResultModel();
            try
            {
                var user = await _userService.Table.FirstOrDefaultAsync(p => p.Id == id);
                if (user == null)
                    return RedirectToAction("Identities");

                //user.FinnotechHistoryId = accepted != 1 && user.FinnotechHistoryId.HasValue ? null : user.FinnotechHistoryId;
                user.IdentityVerificationStatus = accepted == 1 ? VerificationStatus.Confirmed : accepted == 0 ? VerificationStatus.NotConfirmed : user.IdentityVerificationStatus;
                user.IdentityVerificationVerifyDate = accepted == 1 ? DateTime.UtcNow : accepted == 0 ? null : user.IdentityVerificationVerifyDate;
                user.IdentityVerificationDescription = desc;

                await _userService.UpdateAsync(user);

                var adminUserId = User.Identity.GetUserId<int>();
                await _userVerificationService.ChangedAsync(id, UserVerificationType.Identity, user.IdentityVerificationStatus, desc, adminUserId, user.VerificationMediaId);

                result.Status = true;
                result.Text = accepted == 1 ? $"مدارک ارسال شده توسط {user.UserName} تایید شد." : accepted == 0 ? $"مدارک ارسال شده توسط {user.UserName} باطل شد." : "درخواست اشتباه است.";

                var fullName = user.FullName.Trim();
                if (!fullName.HasValue())
                    fullName = user.UserName;

                var verifyRespondEmail = new VerifyRespondEmail()
                {
                    From = "no-reply@extoman.com",
                    Fullname = fullName,
                    Subject = "پاسخ احراز هویت",
                    To = user.Email,
                    Username = user.UserName,
                    VerifyAccepted = accepted == 1,
                    VerifyDescription = desc,
                    ViewName = "VerifyRespond"
                };

                var verifyRespondEmailBody = verifyRespondEmail.GetMailMessage().Body;
                await _messageManager.UserVerifyRespondAsync(verifyRespondEmailBody, user.UserName, user.Email, user.PhoneNumber, desc, accepted == 1);
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }
            return Json(result);
        }

        public async Task<ActionResult> BankCards()
        {
            var model = await _userBankCardService.TableNoTracking
                .Where(p => p.CardNumber != null)
                .Where(p => p.User.IdentityVerificationStatus == VerificationStatus.Confirmed)
                .Where(p => p.VerificationStatus.HasFlag(VerificationStatus.Pending | VerificationStatus.NotSent))
                .OrderBy(p => p.Id)
                .ProjectToListAsync<BankCardVerificationViewModel>();

            var finnotechHistories = await _finnotechResponseCardInfoService.TableNoTracking
                .Where(p => p.UserBankCardId != null)
                .ToListAsync();

            model.ForEach((x) =>
            {
                if (x.FinnotechHistoryId.HasValue && finnotechHistories.Any(p => p.Id == x.FinnotechHistoryId))
                    x.FinnotechHistory = new FinnotechResponseCardInfoViewModel().FromEntity(finnotechHistories.Single(p => p.Id == x.FinnotechHistoryId));
            });

            return View(model);
        }

        [HttpPost]
        [AjaxOnly]
        public async Task<ActionResult> AutoCheckCard(int id)
        {
            var result = new ResultModel();
            try
            {
                var autoResult = await _verificationManager.AutoCardValidationAsync(id);
                if (autoResult)
                {
                    var user = await _userBankCardService.TableNoTracking
                        .Where(p => p.Id == id)
                        .Select(p => new { FullName = p.User.FirstName + " " + p.User.LastName, p.User.PhoneNumber, p.User.Email, p.CardNumber, p.User.UserName })
                        .FirstOrDefaultAsync();

                    var verifyRespondEmail = new VerifyXEmail()
                    {
                        From = "no-reply@extoman.com",
                        Fullname = user.FullName,
                        Subject = $"تایید شماره کارت {user.CardNumber}",
                        To = user.Email,
                        Username = user.UserName,
                        VerifyAccepted = true,
                        VerifyDescription = "",
                        ViewName = "VerifyBankCard",
                        Value = user.CardNumber
                    };
                    var verifyRespondEmailBody = verifyRespondEmail.GetMailMessage().Body;
                    await _messageManager.BankCardVerifiedAsync(verifyRespondEmailBody, user.FullName, user.CardNumber, user.PhoneNumber, user.Email, "", true);
                    result.Text = $"تایید شماره کارت {user.CardNumber} به نام {user.FullName} به صورت اتوماتیک انجام شد";
                    result.Status = true;

                    var adminUserId = User.Identity.GetUserId<int>();
                    await _userVerificationService.ChangedAsync(id, UserVerificationType.BankCard, VerificationStatus.Confirmed, result.Text, adminUserId);
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
                result.Text = ex.Message;
            }

            return Json(result);
        }

        [HttpPost]
        [AjaxOnly]
        public async Task<ActionResult> VerifyBankCard(int id, VerificationStatus status, string description)
        {
            var result = new ResultModel();
            try
            {
                var bankCard = await _userBankCardService.Table.SingleAsync(p => p.Id == id);
                bankCard.VerificationStatus = status;
                bankCard.VerifyDescription = description;
                //if (status == VerificationStatus.Confirmed)
                bankCard.VerifyDate = DateTime.UtcNow;

                await _userBankCardService.UpdateAsync(bankCard);
                result.Text = $"کارت شماره {bankCard.CardNumber} به وضعیت '{status.ToDisplay()}' تغییر پیدا کرد.";
                result.Status = true;

                //Send message then return result
                var userContact = await _userService.TableNoTracking
                    .Where(p => p.Id == bankCard.UserId)
                    .Select(p => new
                    {
                        p.PhoneNumber,
                        p.Email,
                        p.UserName,
                        p.FirstName,
                        p.LastName
                    })
                    .SingleAsync();

                var fullName = userContact.FirstName + " " + userContact.LastName;
                if (!fullName.HasValue())
                    fullName = userContact.UserName;

                var verifyRespondEmail = new VerifyXEmail()
                {
                    From = "no-reply@extoman.com",
                    Fullname = fullName,
                    Subject = $"تایید شماره کارت {bankCard.CardNumber}",
                    To = userContact.Email,
                    Username = userContact.UserName,
                    VerifyAccepted = status == VerificationStatus.Confirmed,
                    VerifyDescription = description,
                    ViewName = "VerifyBankCard",
                    Value = bankCard.CardNumber
                };
                var verifyRespondEmailBody = verifyRespondEmail.GetMailMessage().Body;
                await _messageManager.BankCardVerifiedAsync(verifyRespondEmailBody, fullName, bankCard.CardNumber, userContact.PhoneNumber, userContact.Email, description, verifyRespondEmail.VerifyAccepted);

                var adminUserId = User.Identity.GetUserId<int>();
                await _userVerificationService.ChangedAsync(id, UserVerificationType.BankCard, status, result.Text + " " + description, adminUserId);
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }
            return Json(result);
        }

        public async Task<ActionResult> Telephones()
        {
            var model = await _userService.TableNoTracking
                .Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed)
                .Where(p => p.TelephoneConfirmationStatus.HasFlag(VerificationStatus.Pending | VerificationStatus.NotSent))
                .ProjectToListAsync<TelephoneVerificationViewModel>();

            return View(model);
        }

        public async Task<ActionResult> RejectedTelephones()
        {
            var model = await _userService.TableNoTracking
                .Where(p => !p.IsPhisher)
                .Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed)
                .Where(p => p.TelephoneConfirmationStatus.HasFlag(VerificationStatus.NotConfirmed))
                .ProjectToListAsync<TelephoneVerificationViewModel>();

            return View("Telephones", model);
        }

        [HttpPost]
        public async Task<ActionResult> VerifyTelephone(int id, VerificationStatus status, string description)
        {
            var result = new ResultModel();
            try
            {
                var user = await _userService.Table.SingleAsync(p => p.Id == id);
                user.TelephoneConfirmationStatus = status;
                user.TelephoneConfirmDescription = description;
                //user.IsBuyLimited = true;
                //if (status == VerificationStatus.Confirmed)
                //user.TelephoneConfirmationDate = DateTime.UtcNow;

                await _userService.UpdateAsync(user);
                result.Text = $"تلفن ثابت {user.Telephone} به وضعیت '{status.ToDisplay()}' تغییر پیدا کرد.";
                result.Status = true;
                //Send message then return result
                var fullName = user.FirstName + " " + user.LastName;
                if (!fullName.HasValue())
                    fullName = user.UserName;

                var adminUserId = User.Identity.GetUserId<int>();
                await _userVerificationService.ChangedAsync(id, UserVerificationType.Telephone, user.IdentityVerificationStatus, result.Text + " " + description, adminUserId);

                var verifyRespondEmail = new VerifyXEmail()
                {
                    From = "no-reply@extoman.com",
                    Fullname = fullName,
                    Subject = "تایید شماره تلفن ثابت",
                    To = user.Email,
                    Username = user.UserName,
                    VerifyAccepted = status == VerificationStatus.Confirmed,
                    VerifyDescription = description,
                    ViewName = "VerifyTelephone",
                    Value = user.Telephone
                };

                var verifyRespondEmailBody = verifyRespondEmail.GetMailMessage().Body;
                await _messageManager.TelephoneVerifiedAsync(verifyRespondEmailBody, fullName, user.Telephone, user.PhoneNumber, user.Email, description, verifyRespondEmail.VerifyAccepted);
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }

            return Json(result);
        }

        public async Task<ActionResult> FixCardsVerification()
        {
            var result = new ResultModel();
            var autoCount = 0;
            var manualCount = 0;
            try
            {
                var bankCards = await _userBankCardService.TableNoTracking
                    .Where(p => p.CardNumber != null)
                    .Where(p => p.User.IdentityVerificationStatus == VerificationStatus.Confirmed && 
                                p.VerificationStatus != VerificationStatus.Confirmed &&
                                p.VerificationStatus != VerificationStatus.NotConfirmed
                                && p.FinnotechHistoryId == null)

                    .Select(p => new { p.Id, p.CardNumber, p.User.FirstName, p.User.LastName, p.User.PhoneNumber, p.User.UserName, p.User.Email })
                    .OrderByDescending(p => p.Id)
                    .Take(20)
                    .ToListAsync();

                foreach (var item in bankCards)
                {
                    var autoResult = await _verificationManager.AutoCardValidationAsync(item.Id);
                    if (autoResult)
                    {
                        var fullName = item.FirstName + " " + item.LastName;
                        var verifyRespondEmail = new VerifyXEmail()
                        {
                            From = "no-reply@extoman.com",
                            Fullname = fullName,
                            Subject = $"تایید شماره کارت {item.CardNumber}",
                            To = item.Email,
                            Username = item.UserName,
                            VerifyAccepted = true,
                            VerifyDescription = "",
                            ViewName = "VerifyBankCard",
                            Value = item.CardNumber
                        };
                        var verifyRespondEmailBody = verifyRespondEmail.GetMailMessage().Body;
                        await _messageManager.BankCardVerifiedAsync(verifyRespondEmailBody, fullName, item.CardNumber, item.PhoneNumber, item.Email, "", true);
                        autoCount++;
                    }
                    else
                    {
                        manualCount++;
                    }
                }
                result.Text = $"تعداد {autoCount} کارت به صورت اتوماتیک تایید شد و تعداد {manualCount} باید دستی تایید شود.";
                result.Status = true;
            }
            catch (Exception ex)
            {
                ex.LogError();
                result.Text = ex.Message;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //public async Task<ActionResult> FixCardsVerification()
        //{
        //    //var users = await _userService.TableNoTracking.Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed
        //    //                                                            && p.PhoneNumberConfirmed
        //    //                                                            && p.EmailConfirmed
        //    //                                                            && p.TelephoneConfirmationStatus == VerificationStatus.Confirmed
        //    //                                                            && p.Orders.Any(x => x.OrderStatus == OrderStatus.Complete)
        //    //                                                            && p.Orders.Any(x => x.BankResponse != null))
        //    //                                                     .Select(p => new { FullName = p.FirstName + " " + p.LastName , BankCards = p.UserBankCards }).ToListAsync();

        //    var validBankCards = await _userBankCardService.Table.Where(p => p.User.EmailConfirmed == true
        //                                                                 && p.User.PhoneNumberConfirmed == true
        //                                                                 && p.User.TelephoneConfirmationStatus == VerificationStatus.Confirmed
        //                                                                 && p.User.IdentityVerificationStatus == VerificationStatus.Confirmed
        //                                                                 && p.User.IsPhisher == false
        //                                                                 //&& p.User.Orders.Any(x => x.OrderStatus == OrderStatus.Complete && x.BankResponse != null)
        //                                                                 && p.VerificationStatus != VerificationStatus.Confirmed)
        //                                                        .Include(p => p.User)
        //                                                        .Take(10)
        //                                                        .ToListAsync();

        //    var count = 0;

        //    foreach (var item in validBankCards)
        //    {
        //        var fullName = (item.User.FirstName + " " + item.User.LastName).Trim();
        //        if (fullName.HasValue())
        //        {
        //            var finnotech = new FinnotechCardInfoViewModel();
        //            var responseCard = item.CardNumber.RemoveRight(12) + "-" + item.CardNumber.RemoveLeft(4).RemoveRight(10) + "xx-xxxx-" + item.CardNumber.RemoveLeft(12);
        //            var finnotechCardOldResponse = await _finnotechResponseCardInfoService.Table.Where(p => p.Result == "0" && p.DestCard == responseCard).FirstOrDefaultAsync();
        //            if (finnotechCardOldResponse != null)
        //            {
        //                finnotechCardOldResponse.ForUserId = item.UserId;
        //                await _finnotechResponseCardInfoService.UpdateAsync(finnotechCardOldResponse);
        //                finnotech = new FinnotechCardInfoViewModel()
        //                {
        //                    Description = finnotechCardOldResponse.Description,
        //                    Result = finnotechCardOldResponse.Result,
        //                    TrackId = new Guid(finnotechCardOldResponse.TrackId),
        //                    Name = finnotechCardOldResponse.Name,
        //                    DestCard = finnotechCardOldResponse.DestCard,
        //                    DoTime = finnotechCardOldResponse.DoTime
        //                };
        //            }
        //            else
        //            {
        //                var finnotechCard = await _finnotechService.GetCardInformationAsync(item.CardNumber);
        //                if (finnotechCard != null)
        //                {
        //                    await _finnotechResponseCardInfoService.InsertAsync(new FinnotechResponseCardInfo()
        //                    {
        //                        Code = finnotechCard.Code,
        //                        Description = finnotechCard.Description,
        //                        DestCard = finnotechCard.DestCard,
        //                        ForUserId = item.UserId,
        //                        FarsiMessage = finnotechCard.FarsiMessage,
        //                        DoTime = finnotechCard.DoTime,
        //                        Message = finnotechCard.Message,
        //                        Name = finnotechCard.Name,
        //                        TrackId = finnotechCard.TrackId.ToString(),
        //                        StatusCode = finnotechCard.StatusCode,
        //                        Result = finnotechCard.Result,
        //                        UserBankCardId = item.Id
        //                    });

        //                    finnotech = new FinnotechCardInfoViewModel()
        //                    {
        //                        Description = finnotechCard.Description,
        //                        Result = finnotechCard.Result,
        //                        TrackId = finnotechCard.TrackId,
        //                        Name = finnotechCard.Name,
        //                        DestCard = finnotechCard.DestCard,
        //                        DoTime = finnotechCard.DoTime
        //                    };
        //                }
        //            }
        //            if (finnotech.Name == fullName || finnotech.Name.Contains(fullName))
        //            {
        //                item.VerificationStatus = VerificationStatus.Confirmed;
        //                await _userBankCardService.UpdateAsync(item);

        //                var verifyRespondEmail = new VerifyXEmail()
        //                {
        //                    From = "no-reply@extoman.com",
        //                    Fullname = fullName,
        //                    Subject = $"تایید شماره کارت {item.CardNumber}",
        //                    To = item.User.Email,
        //                    Username = item.User.UserName,
        //                    VerifyAccepted = true,
        //                    VerifyDescription = "",
        //                    ViewName = "VerifyBankCard",
        //                    Value = item.CardNumber
        //                };
        //                var verifyRespondEmailBody = verifyRespondEmail.GetMailMessage().Body;
        //                await _messageManager.BankCardVerifiedAsync(verifyRespondEmailBody, fullName, item.CardNumber, item.User.PhoneNumber, item.User.Email, "", verifyRespondEmail.VerifyAccepted);

        //                count++;
        //            }
        //        }
        //    }
        //    return Content($"{0} کارت تایید شد.");
        //}

        [ChildActionOnly]
        public virtual ActionResult IdentitiesCount()
        {
            var count = _userService.TableNoTracking
                .Where(p => p.IdentityVerificationStatus == VerificationStatus.Pending)
                .Count();
            return Content(count.ToString());
        }

        [ChildActionOnly]
        public virtual ActionResult TelephonesCount()
        {
            var count = _userService.TableNoTracking
                .Where(p => p.IdentityVerificationStatus == VerificationStatus.Confirmed)
                .Where(p => p.TelephoneConfirmationStatus.HasFlag(VerificationStatus.Pending | VerificationStatus.NotSent))
                .Count();
            return Content(count.ToString());
        }

        [ChildActionOnly]
        public virtual ActionResult BankCardsCount()
        {
            var count = _userBankCardService.TableNoTracking
                .Where(p => p.User.IdentityVerificationStatus == VerificationStatus.Confirmed)
                .Where(p => p.VerificationStatus.HasFlag(VerificationStatus.Pending | VerificationStatus.NotSent))
                .Count();
            return Content(count.ToString());
        }
        #endregion
    }
}