﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-hand-open")]
    [DisplayName("نقش ها و دسترسی ها")]
    public class RoleController : BaseAdminController
    {
        private readonly IAppRoleManager _roleManager;
        private readonly IPermissionService _permissionService;
        private readonly IAppUserManager _userManager;
        private readonly IAppRoleService _roleService;
        public RoleController(
            IAppRoleManager roleManager,
            IPermissionService permissionService,
            IAppUserManager userManager,
            IAppRoleService roleService)
        {
            _roleManager = roleManager;
            _permissionService = permissionService;
            _userManager = userManager;
            _roleService = roleService;
            //DisplayName = "مدیریت نقش ها و دسترسی ها";
        }

        public async Task<ActionResult> Index()
        {
            var roles = await _roleManager.GetAllAppRolesAsync();
            var model = new List<RoleViewModel>();
            foreach (var role in roles)
                model.Add(new RoleViewModel().FromEntity(role));

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> EditRolePermissions(int id)
        {
            var role = await _roleManager.FindByIdAsync(id);
            if (role.Name == "Admin")
                return RedirectToAction("Index");

            var asm = Assembly.GetAssembly(typeof(MvcApplication));

            var controlleractionlist = asm.GetTypes()
                    .Where(type => typeof(BaseAdminController).IsAssignableFrom(type))
                    .SelectMany(type => type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))
                    .Where(m => !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true).Any())
                    .Select(x => new
                    {
                        Controller = x.DeclaringType.Name,
                        ControllerDisplayName = x.DeclaringType?.GetCustomAttribute<DisplayNameAttribute>()?.GetPropValue<string>("DisplayName") ?? x.DeclaringType.Name.Replace("Controller", ""),
                        ControllerIconClass = x.DeclaringType?.GetCustomAttribute<MenuIconAttribute>()?.GetPropValue<string>("IconClass") ?? "",
                        Action = x.Name,
                        ReturnType = x.ReturnType.Name,
                        Attributes = x.GetCustomAttributes().Select(a => a.GetType().Name.Replace("Attribute", "")).ToList()
                    })
                    .OrderBy(x => x.Controller).ThenBy(x => x.Action).ToList();

            var controllers = controlleractionlist.Select(p => p.Controller).Distinct().ToList();

            var controllersViewModel = new List<ControllerViewModel>();
            foreach (var controller in controllers)
            {
                var actionsViewModel = new List<ActionViewModel>();
                var actions = controlleractionlist.Where(p => p.Controller == controller).ToList();
                var actionNames = actions.Select(p => p.Action).Distinct().ToList();
                foreach (var item in actionNames)
                {
                    var actionsByName = actions.Where(p => p.Action == item).ToList();
                    actionsViewModel.Add(new ActionViewModel()
                    {
                        Name = item,
                        ReturnType = actionsByName.Select(p => p.ReturnType).Distinct().Join(","),
                        Attributes = actionsByName.SelectMany(p => p.Attributes).Distinct().Join(","),
                        IsInPermission = role.Permissions.Any(x => x.ControllerAction == item && x.ControllerName == controller)
                    });
                }
                controllersViewModel.Add(new ControllerViewModel()
                {
                    Name = controller,
                    Display = controlleractionlist.Where(p => p.Controller == controller).Select(p => p.ControllerDisplayName).FirstOrDefault(),
                    IconClass = controlleractionlist.Where(p => p.Controller == controller).Select(p => p.ControllerIconClass).FirstOrDefault(),
                    Actions = actionsViewModel,
                    IsAllActionsInPermission = actionsViewModel.All(p => p.IsInPermission)
                });
            }

            var model = new EditRolePermissionsViewModel()
            {
                Id = id,
                Role = new RoleViewModel().FromEntity(role),
                Controllers = controllersViewModel
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EditRolePermissions()
        {
            var id = Request.Form["Id"].TryToInt();
            var role = await _roleManager.FindByIdAsync(id);
            var permission = await _permissionService.Table.ToListAsync();
            var formKeys = HttpContext.Request.Form.AllKeys;
            var allRolePermission = role.Permissions.ToList();
            foreach (var item in allRolePermission)
                role.Permissions.Remove(item);

            foreach (var key in formKeys)
            {
                var keySplit = key.Split('-');
                if (keySplit[0].Contains("Controller"))
                {
                    var controller = keySplit[0];
                    var action = keySplit[1];
                    if (!role.Permissions.Any(x => x.ControllerName.Equals(controller, StringComparison.InvariantCultureIgnoreCase) && x.ControllerAction.Equals(action, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        if (permission.Any(x => x.ControllerName.Equals(controller, StringComparison.InvariantCultureIgnoreCase) && x.ControllerAction.Equals(action, StringComparison.InvariantCultureIgnoreCase)))
                        {
                            role.Permissions.Add(permission.Single(p => p.ControllerName.Equals(controller, StringComparison.InvariantCultureIgnoreCase) && p.ControllerAction.Equals(action, StringComparison.InvariantCultureIgnoreCase)));
                        }
                        else
                        {
                            role.Permissions.Add(new Domain.Models.Permission()
                            {
                                ControllerName = controller,
                                ControllerAction = action
                            });
                        }
                    }
                }
            }

            await _roleManager.UpdateAsync(role);
            return RedirectToAction("EditRolePermissions", new { id });
        }

        [ChildActionOnly]
        public List<Permission> AllObject()
        {
            var permissions = _permissionService.TableNoTracking.ToList();
            return permissions;
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> Add(string name)
        {
            var result = new ResultModel();
            try
            {
                var roleResult = await _roleManager.CreateAsync(new Domain.Models.AppRole() { Name = name });
                if (roleResult.Succeeded)
                {
                    result.Status = true;
                    result.Text = $"نقش کاربر جدید با نام {name} اضافه شد.";
                }
                else
                {
                    result.Text = roleResult.Errors.Join(",");
                }
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }
            return Json(result);
        }

        public async Task<ActionResult> Users(int id)
        {
            var users = await _roleManager.GetUsersInRoleByIdAsync(id);
            var model = new List<UserSimpleViewModel>();
            foreach (var item in users)
                model.Add(new UserSimpleViewModel().FromEntity(item));

            ViewBag.RoleId = id;
            ViewBag.RoleName = (await _roleManager.FindByIdAsync(id)).Name;
            return View(model);
        }

        [HttpGet]
        public ActionResult AddUserToRole(int id)
        {
            ViewBag.RoleId = id;
            ViewBag.RoleName = _roleManager.FindByIdAsync(id).Result.Name;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddUserToRole(int roleId, int userId)
        {
            try
            {
                var role = await _roleManager.FindByIdAsync(roleId);
                if (_roleManager.IsUserInRole(userId, role.Name))
                {
                    ErrorNotification($"نقش {role.Name} قبلا برای این کاربر افزوده شده.");
                }
                else
                {
                    var result = await _userManager.AddToRoleAsync(userId, role.Name);
                    if (result.Succeeded)
                        SuccessNotification($"کاربر جدید برای نقش {role.Name} اضافه شد.");
                    else
                        ErrorNotification(result.Errors.Join(", "));
                }
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
            }
            return RedirectToAction("Users", new { id = roleId });
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> DeleteRole(int id)
        {
            var result = new ResultModel();
            try
            {
                var role = await _roleManager.FindByIdAsync(id);
                if (role.Name != "Admin")
                {
                    var deleteResult = await _roleManager.DeleteAsync(role);
                    if (deleteResult.Succeeded)
                    {
                        result.Text = $"نقش کاربری {role.Name} حذف شد.";
                        result.Status = true;
                    }
                    else
                    {
                        result.Text = deleteResult.Errors.Join(",");
                    }
                }
                else
                {
                    result.Text = "نقش Admin قابل حذف نیست.";
                }
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }
            return Json(result);
        }
    }
}