﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Payment.PayIR;
//using Xtoman.Binance.Client;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Service.WebServices;
//using Xtoman.Service.WebServices;
using Xtoman.Utility;
using Xtoman.Utility.PostalEmail;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-exchange-vertical")]
    [DisplayName("سفارش های تبادل ارز")]
    public class ExchangeOrderController : BaseAdminController
    {
        #region Properties
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly IExchangeTypeService _exchangeTypeService;
        private readonly ICurrencyTypeService _currencyTypeService;
        private readonly ITradeManager _tradeManager;
        private readonly IWithdrawManager _withdrawManager;
        private readonly IMessageManager _messageManager;
        private readonly IOrderManager _orderManager;
        private readonly IPriceManager _priceManager;
        private readonly IPayIRService _payIRService;
        private readonly IVandarV2Service _vandarV2Service;
        private readonly IOrderReviewService _orderReviewService;
        private readonly IOrderNoteService _orderNoteService;
        #endregion

        #region Constructor
        public ExchangeOrderController(
            IExchangeOrderService exchangeOrderService,
            IExchangeTypeService exchangeTypeService,
            ICurrencyTypeService currencyTypeService,
            ITradeManager tradeManager,
            IWithdrawManager withdrawManager,
            IMessageManager messageManager,
            IOrderManager orderManager,
            IPriceManager priceManager,
            IPayIRService payIRService,
            IVandarV2Service vandarV2Service,
            IOrderReviewService orderReviewService,
            IOrderNoteService orderNoteService)
        {
            _exchangeOrderService = exchangeOrderService;
            _exchangeTypeService = exchangeTypeService;
            _currencyTypeService = currencyTypeService;
            _tradeManager = tradeManager;
            _withdrawManager = withdrawManager;
            _messageManager = messageManager;
            _orderManager = orderManager;
            _priceManager = priceManager;
            _payIRService = payIRService;
            _vandarV2Service = vandarV2Service;
            _orderReviewService = orderReviewService;
            _orderNoteService = orderNoteService;
            //DisplayName = "سفارش های تبادل ارز";
        }
        #endregion

        public async Task<ActionResult> Index()
        {
            var currencyTypes = await _currencyTypeService.TableNoTracking.ToDictionaryAsync(x => x.Id, x => x.Name);

            var currencyTypesSelectList = new List<SelectListItem>()/* { new SelectListItem() { Value = string.Empty, Text = "همه" } }*/;
            foreach (var item in currencyTypes)
                currencyTypesSelectList.Add(new SelectListItem() { Text = item.Value, Value = item.Key.ToString() });

            ViewBag.CurrencyTypes = currencyTypesSelectList;

            return View();
        }

        public async Task<ActionResult> Search(ExchangeOrderSearchTerms term, int page = 1, int pageSize = 30)
        {
            var query = _exchangeOrderService.TableNoTracking;
            if (term.Text.HasValue())
            {
                var isTermGuid = Guid.TryParse(term.Text, out Guid termGuid);
                var termLong = term.Text.TryToLong();
                query = query.Where(p => p.User.FirstName == term.Text ||
                                         p.User.LastName == term.Text ||
                                         (p.User.FirstName + " " + p.User.LastName) == term.Text ||
                                         p.User.Email == term.Text || p.User.PhoneNumber == term.Text ||
                                         p.Id == termLong || (isTermGuid && p.Guid == termGuid) ||
                                         p.ReceiveAddress == term.Text ||
                                         p.InsertUserIpAddress.Contains(term.Text));
            }

            if (term.FromId.HasValue)
                query = query.Where(p => p.Id >= term.FromId);

            if (term.ToId.HasValue)
                query = query.Where(p => p.Id <= term.ToId);

            if (term.ExchangeTypeFromCurrencyTypeId.HasValue)
                query = query.Where(p => p.Exchange.FromCurrencyId == term.ExchangeTypeFromCurrencyTypeId);

            if (term.ExchangeTypeToCurrencyTypeId.HasValue)
                query = query.Where(p => p.Exchange.ToCurrencyId == term.ExchangeTypeToCurrencyTypeId);

            if (term.OrderStatus.HasValue)
                query = query.Where(p => p.OrderStatus == term.OrderStatus.Value);

            if (term.PaymentStatus.HasValue)
                query = query.Where(p => p.PaymentStatus == term.PaymentStatus.Value);

            if (term.FromOrderDate.HasValue())
            {
                var date = Convert.ToDateTime(term.FromOrderDate).FirstTimeOfDay();
                query = query.Where(x => x.InsertDate >= date);
            }
            if (term.ToOrderDate.HasValue())
            {
                var date = Convert.ToDateTime(term.ToOrderDate).LastTimeOfDay();
                query = query.Where(x => x.InsertDate <= date);
            }

            if (term.UserIsPhisher.HasValue)
            {
                var IsPhisher = term.UserIsPhisher.Value == 1;
                query = query.Where(p => p.User.IsPhisher == IsPhisher);
            }

            var list = await query.ProjectToPagedListAsync<ExchangeOrdersListItemViewModel>(page, pageSize);

            var totalPaymentReceived = query.Where(p => p.PaymentStatus == PaymentStatus.Paid && p.Exchange.FromCurrency.Type == ECurrencyType.Shetab);
            ViewBag.TotalPaymentReceived = await totalPaymentReceived
                .Select(p => p.PayAmount)
                .DefaultIfEmpty(0)
                .SumAsync();

            ViewBag.TotalTomanUnpaid = await query
                .Where(p => p.PaymentStatus == PaymentStatus.Paid && p.OrderStatus == OrderStatus.Processing && p.Exchange.ToCurrency.Type == ECurrencyType.Shetab)
                .Select(p => p.ReceiveAmount)
                .DefaultIfEmpty(0)
                .SumAsync();

            ViewBag.TotalTomanPaid = await query
                .Where(p => p.PaymentStatus == PaymentStatus.Paid && p.OrderStatus == OrderStatus.Complete && p.Exchange.ToCurrency.Type == ECurrencyType.Shetab)
                .Select(p => p.ReceiveAmount)
                .DefaultIfEmpty(0)
                .SumAsync();

            ViewBag.TotalCount = await query.CountAsync();

            //var binanceClient = new BinanceClient(new ClientConfiguration()
            //{
            //    ApiKey = AppSettingManager.Binance_Key,
            //    SecretKey = AppSettingManager.Binance_Secret,
            //    CacheTime = TimeSpan.FromSeconds(30),
            //    EnableRateLimiting = true
            //});
            //var binanceResponse = await binanceClient.GetAccountInformation();
            //ViewBag.BinanceBalance = binanceResponse.Balances.Where(p => p.Asset == "BTC").Select(p => p.Free).FirstOrDefault();

            return PartialView("_List", list);
        }

        public virtual async Task<ActionResult> Add()
        {
            ViewBag.ExchangeTypes = await _exchangeTypeService.TableNoTracking
                .Where(p => !p.Disabled && !p.Invisible)
                .ProjectToListAsync<ExchangeTypeViewModel>();

            return View();
        }

        [HttpPost]
        public virtual async Task<ActionResult> Add(AddExchangeOrderViewModel model)
        {
            await Task.Delay(0);
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Rate(int fromId, int toId, decimal? amount, int isFrom = 1)
        {
            if (Request.IsAjaxRequest())
                if (amount.HasValue && amount.Value > 0)
                {
                    return Json(await _priceManager.GetExchangeAmountAsync(fromId, toId, amount.Value, isFrom == 1));
                }
                else
                {
                    return Json(await _priceManager.GetExchangeRateAsync(fromId, toId));
                }

            return HttpNotFound();
        }

        public virtual async Task<ActionResult> Details(long id)
        {
            var model = await _exchangeOrderService.TableNoTracking.Where(x => x.Id == id)
                .ProjectToSingleOrDefaultAsync<ExchangeOrderViewModel>();

            if (model == null)
                return RedirectToAction("Index");

            return View(model);
        }

        public virtual async Task<ActionResult> DetailsByGuid(Guid guid)
        {
            var id = await _exchangeOrderService.TableNoTracking.Where(p => p.Guid == guid).Select(p => p.Id).FirstOrDefaultAsync();
            return RedirectToAction("Details", new { id });
        }

        [HttpPost]
        public virtual async Task<ActionResult> AddNote(long orderId, string note)
        {
            if (note.HasValue())
            {
                await _orderNoteService.InsertAsync(new OrderNote()
                {
                    OrderId = orderId,
                    Note = note
                });
            }
            return RedirectToAction("Details", new { id = orderId });
        }

        [HttpPost]
        public virtual async Task<ActionResult> RemoveNote(int id)
        {
            var result = new ResultModel();
            try
            {
                await _orderNoteService.DeleteAsync(id);
                result.Text = "یادداشت حذف شد.";
                result.Status = true;
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }
            return Json(result);
        }

        public virtual async Task<ActionResult> VerifyPayment(long id)
        {
            var result = new ResultModel();
            try
            {
                var token = await _exchangeOrderService.TableNoTracking.Where(p => p.Id == id).Select(p => p.BankResponse.BankSaleReferenceId).FirstOrDefaultAsync();
                if (token.HasValue())
                {
                    var verifyPaymentResult = await _payIRService.VerifyAsync(new PayIRVerifyInput() { token = token });
                    if (verifyPaymentResult.Status == 1)
                    {
                        var order = await _exchangeOrderService.Table.SingleAsync(p => p.Id == id);

                        var isNotPhisher = !order.User.IsPhisher;
                        var isAmountOK = (order.PayAmount * 10).ToInt() == verifyPaymentResult.Amount;
                        var isFactorNumberOk = verifyPaymentResult.FactorNumber.TryToLong() == order.Id;
                        var isNotDoubleSpended = !await _exchangeOrderService.TableNoTracking.Where(p => p.Id != order.Id && p.PayTransactionId == verifyPaymentResult.TransactionId).AnyAsync();

                        var isCardVerified = order.User.UserBankCards
                            .Where(p => p.VerificationStatus == VerificationStatus.Confirmed)
                            .Any(x => string.Join("******", x.CardNumber.Left(6), x.CardNumber.Right(4)).Equals(verifyPaymentResult.CardNumber) ||
                                      x.CardNumber.Equals(verifyPaymentResult.CardNumber));

                        // Check user is not phisher && amount is ok && factornumber is ok
                        if (isNotPhisher && isAmountOK && isFactorNumberOk && isNotDoubleSpended)
                        {
                            order.PaymentStatus = PaymentStatus.Paid;
                            order.PayTransactionId = verifyPaymentResult.TransactionId;
                            order.BankResponse.BankCardHolderPan = verifyPaymentResult.CardNumber;
                        }
                        else
                        {
                            order.Errors += isNotPhisher == false ? " کاربر فیشر است." : "";
                            order.Errors += isAmountOK == false ? $" مبلغ پرداخت شده {(verifyPaymentResult.Amount / 10).ToPrice()} می باشد ولی مبلغ فاکتور {order.PayAmount.ToPrice()} می باشد." : "";
                            order.Errors += isFactorNumberOk == false ? $" پرداخت برای فاکتور {verifyPaymentResult.FactorNumber} ثبت شده است ولی برای سفارش {order.Id} انجام شده." : "";
                            order.Errors += isNotDoubleSpended == false ? $" پرداخت دوبار استفاده شده" : "";
                        }

                        if (!isCardVerified)
                        {
                            order.PaymentStatus = PaymentStatus.CardNotValid;
                            order.Errors += $" کارتی که با آن پرداخت شده احراز نشده";
                        }

                        await _exchangeOrderService.UpdateAsync(order);
                    }
                }
                result.Status = true;
                result.Text = "بررسی مجدد انجام شد.";
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }

            return Json(result);
        }

        [HttpPost]
        public virtual async Task<ActionResult> CheckCardAgain(long id)
        {
            var result = new ResultModel();
            try
            {
                var order = await _exchangeOrderService.Table.Where(p => p.Id == id)
                    .Include(p => p.Exchange)
                    .Include(p => p.User)
                    .Include(p => p.User.UserBankCards)
                    .Include(p => p.OrderTrades)
                    .SingleAsync();

                var isCardVerified = order.User.UserBankCards
                                                    .Where(p => p.VerificationStatus == VerificationStatus.Confirmed)
                                                    .Any(x => string.Join("******", x.CardNumber.Left(6), x.CardNumber.Right(4)).Equals(order.BankResponse.BankCardHolderPan) ||
                                                              x.CardNumber.Equals(order.BankResponse.BankCardHolderPan));

                if (isCardVerified && order.BankResponse.BankGatewayType.Value == BankGatewayType.Payir)
                {
                    var isWithdrawedBefore = order.OrderWithdraws.Any();
                    order.PaymentStatus = PaymentStatus.Paid;
                    await _exchangeOrderService.UpdateAsync(order);

                    result.Status = true;

                    if (!isWithdrawedBefore)
                    {
                        // Then Complete the order
                        switch (order.Exchange.ExchangeFiatCoinType)
                        {
                            case ExchangeFiatCoinType.FiatTomanToCoin:
                                await _orderManager.CompleteFiatToCoinOrderAsync(order);
                                //Get balance available and Withdraw coin to user address or Trade and withdraw from Trading Platforms like: bitfinex, binance, hitbtc
                                //await _paymentManager.WithdrawCoinOrTradeAsync(order.Id, order.Exchange.ToCurrency.Type, order.ReceiveAmount, order.ReceiveAddress, order.ReceiveMemoTagPaymentId)
                                break;
                            case ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham:
                                await _orderManager.CompleteFiatTomanToFiatUSDOrderAsync(order);
                                //await _paymentManager.WithdrawFiatUSDAsync(order.Id, order.Exchange.ToCurrency.Type, order.ReceiveAmount, order.ReceiveAddress);
                                break;
                        }

                        var userName = order.User.FullName.HasValue() ? order.User.FullName : order.User.UserName;
                        //var telegramText = $"سفارش کاربر {userName} به شماره {order.Id}, به مبلغ {order.PayAmount.ToPriceWithoutFloat()} تومان پرداخت شد.";
                        var orderUrl = Url.Action("Index", "ExchangeOrder", new { orderGuid = order.Guid }, Request.Url.Scheme);
                        var payAmount = order.Exchange.FromCurrency.UnitSign == "تومان" ? order.PayAmount.ToPriceWithoutFloat() + " تومان" : order.PayAmount.ToBlockChainPrice() + " " + order.Exchange.FromCurrency.UnitSign;
                        var receiveAmount = order.Exchange.ToCurrency.UnitSign == "تومان" ? order.ReceiveAmount.ToPriceWithoutFloat() + " تومان" : order.ReceiveAmount.ToBlockChainPrice() + " " + order.Exchange.ToCurrency.UnitSign;
                        var paidOrderEmail = new OrderPaidEmail()
                        {
                            ExchangeTime = "5 تا 30 دقیقه",
                            From = "no-reply@extoman.com",
                            To = order.User.Email,
                            FromCurrency = order.Exchange.FromCurrency.Name,
                            ToCurrency = order.Exchange.ToCurrency.Name,
                            Fullname = order.User.FullName,
                            OrderUrl = orderUrl,
                            PayAmount = payAmount,
                            ReceiveAmount = receiveAmount,
                            Subject = "سفارش جدید ثبت گردید",
                            Username = order.User.UserName,
                            ViewName = "OrderPaid",
                            TxFee = order.Exchange.ToCurrency.TransactionFee + " " + order.Exchange.ToCurrency.UnitSign
                        };

                        var paidOrderEmailBody = paidOrderEmail.GetMailMessage().Body;
                        await _messageManager.OrderPaidByTomanAsync(paidOrderEmailBody, order.User.Email, order.User.PhoneNumber, userName, order.Id, order.Guid.ToString(), order.PayAmount);
                        result.Text = "کارت تایید شد و سفارش تکمیل گردید.";
                    }
                    else
                    {
                        result.Text = "کارت تایید شده اما خروجی قبلا زده شده.";
                    }
                }
                else
                {
                    result.Text = "کارت تایید نشده";
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
                result.Text = ex.Message;
            }
            return Json(result);
        }

        [HttpPost]
        public virtual async Task<ActionResult> ReCalculateAgain(long id)
        {
            var result = new ResultModel();
            try
            {
                var order = await _exchangeOrderService.Table.SingleAsync(p => p.Id == id);
                var newReceiveAmount = await _priceManager.ReCalculateReceiveAmountAsync(order);
                if (newReceiveAmount != 0 && newReceiveAmount < order.ReceiveAmount)
                {
                    order.ReceiveAmount = newReceiveAmount;
                    await _exchangeOrderService.UpdateAsync(order);
                    result.Text = "به روز رسانی مقدار دریافتی انجام شد.";
                    result.Status = true;
                }
                else
                {
                    result.Text = $"نرخ بررسی شد. نرخ قبلی بهتر است، نرخ به دست آمده {newReceiveAmount}";
                    result.Status = true;
                    new Exception($"New Receive Amount: {newReceiveAmount}").LogError();
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
                result.Text = ex.Message;
            }
            return Json(result);
        }

        [HttpPost]
        public virtual async Task<ActionResult> TryTradeAgain(long id)
        {
            var result = new ResultModel();
            var order = await _exchangeOrderService.Table
                .Include(p => p.OrderTrades)
                .Where(p => p.Id == id).SingleAsync();

            if (order.PaymentStatus == PaymentStatus.Paid)
            {
                order.OrderStatus = OrderStatus.Processing;
                await _exchangeOrderService.UpdateAsync(order);
                var tradeResult = await _tradeManager.TryTradeAgain(order);
                var binanceTrade = tradeResult.Trades.Where(p => p.TradingPlatform == TradingPlatform.Binance).FirstOrDefault();
                var success = tradeResult.BinanceResult;
                order.OrderStatus = success ? OrderStatus.InComplete : OrderStatus.TradeFailed;
                result.Status = success;
                result.Text = success ? "ترید انجام شد." : "خطا در هنگام انجام ترید.";
                await _exchangeOrderService.UpdateAsync(order);
                if (tradeResult.BinanceResult)
                {
                    if (binanceTrade != null)
                    {
                        var binanceTelegramMessage = $"تبدیل {binanceTrade.FromCryptoType.ToDisplay()} با مقدار {binanceTrade.FromAmount.ToBlockChainPrice()} به {binanceTrade.ToCryptoType.ToDisplay()} با مقدار {binanceTrade.ToAmount.ToBlockChainPrice()} انجام شد. قیمت هر واحد {binanceTrade.ToCryptoType.ToDisplay(DisplayProperty.ShortName)} معادل {binanceTrade.Price} {binanceTrade.FromCryptoType.ToDisplay(DisplayProperty.ShortName)}";
                        await _messageManager.TelegramAdminMessageAsync(binanceTelegramMessage);
                    }
                }
                if (order.Errors.HasValue())
                {
                    result.Status = false;
                    result.Text = order.Errors;
                    await _messageManager.TelegramAdminMessageAsync(order.Errors);
                }
            }
            else
            {
                result.Text = "سفارش پرداخت نشده.";
                result.Status = false;
            }
            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> WithdrawFromCP(long id)
        {
            var result = new ResultModel();
            var order = await _exchangeOrderService.Table
                .Include(p => p.OrderWithdraws)
                .Where(p => p.Id == id).SingleAsync();

            if (order.PaymentStatus == PaymentStatus.Paid && order.OrderStatus != OrderStatus.Complete)
            {
                var (status, text) = await _withdrawManager.WithdrawFromCoinPaymentsForOrderAsync(order);
                if (status)
                {
                    order.OrderStatus = OrderStatus.Complete;
                    await _exchangeOrderService.UpdateAsync(order);
                    var walletAddressText = order.ReceiveAddress;
                    if (order.ReceiveMemoTagPaymentId.HasValue())
                        walletAddressText += $" و Payment Tag Id: {order.ReceiveMemoTagPaymentId}";
                    //var withdrawMessage = $"برداشت از {(order.Exchange.ToCurrency.IsFiat ? order.Exchange.ToCurrency.Name : "بایننس")} و واریز {(order.ReceiveAmount + order.GatewayFee).ToBlockChainPrice()} {order.Exchange.ToCurrency.UnitSign} بابت سفارش شماره {order.Id} به آدرس کیف پول {walletAddressText} انجام شد.";
                    await _messageManager.TelegramAdminMessageAsync(text);
                    result.Status = true;
                    //result.Text = withdrawResult.message;
                }
                else
                {
                    //var errorMessage = $"خطا در هنگام برداشت از Binance برای سفارش شماره {order.Id}";
                    await _messageManager.TelegramAdminMessageAsync(text);
                    order.OrderStatus = OrderStatus.WithdrawFailed;
                    await _exchangeOrderService.UpdateAsync(order);
                }
                result.Text = text;
            }
            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> TryWithdrawAgain(long id)
        {
            var result = new ResultModel();
            var order = await _exchangeOrderService.Table
                .Include(p => p.OrderWithdraws)
                //.Include(p => p.OrderTrades)
                .Where(p => p.Id == id).SingleAsync();
            if (order.PaymentStatus == PaymentStatus.Paid && order.OrderStatus != OrderStatus.Complete)
            {
                var (success, message) = await _withdrawManager.WithdrawAsync(order);
                if (success)
                {
                    order.OrderStatus = OrderStatus.Complete;
                    await _exchangeOrderService.UpdateAsync(order);
                    var walletAddressText = order.ReceiveAddress;
                    if (order.ReceiveMemoTagPaymentId.HasValue())
                        walletAddressText += $" و Payment Tag Id: {order.ReceiveMemoTagPaymentId}";
                    //var withdrawMessage = $"برداشت از {(order.Exchange.ToCurrency.IsFiat ? order.Exchange.ToCurrency.Name : "بایننس")} و واریز {(order.ReceiveAmount + order.GatewayFee).ToBlockChainPrice()} {order.Exchange.ToCurrency.UnitSign} بابت سفارش شماره {order.Id} به آدرس کیف پول {walletAddressText} انجام شد.";
                    await _messageManager.TelegramAdminMessageAsync(message);
                    result.Status = true;
                    //result.Text = withdrawResult.message;
                }
                else
                {
                    //var errorMessage = $"خطا در هنگام برداشت از Binance برای سفارش شماره {order.Id}";
                    await _messageManager.TelegramAdminMessageAsync(message);
                    order.OrderStatus = OrderStatus.WithdrawFailed;
                    await _exchangeOrderService.UpdateAsync(order);
                }
                result.Text = message;
            }
            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> TryTradeWithdrawAgain(long id)
        {
            var result = new ResultModel();
            var order = await _exchangeOrderService.Table
                .Include(p => p.OrderWithdraws)
                .Include(p => p.OrderTrades)
                .Where(p => p.Id == id).SingleAsync();

            if (order.PaymentStatus == PaymentStatus.Paid && order.OrderStatus != OrderStatus.Complete)
            {
                order.OrderStatus = OrderStatus.Processing;
                order.Errors = null;
                await _exchangeOrderService.UpdateAsync(order);
                var tradeResult = await _tradeManager.TryTradeAgain(order);
                var binanceTrade = tradeResult.Trades.Where(p => p.TradingPlatform == TradingPlatform.Binance).FirstOrDefault();
                var success = tradeResult.BinanceResult;
                order.OrderStatus = success ? OrderStatus.InComplete : OrderStatus.TradeFailed;
                result.Status = success;
                result.Text = success ? "ترید انجام شد." : "خطا در هنگام انجام ترید.";
                await _exchangeOrderService.UpdateAsync(order);
                if (success)
                {
                    if (binanceTrade != null)
                    {
                        var binanceTelegramMessage = $"تبدیل {binanceTrade.FromCryptoType.ToDisplay()} با مقدار {binanceTrade.FromAmount.ToBlockChainPrice()} به {binanceTrade.ToCryptoType.ToDisplay()} با مقدار {binanceTrade.ToAmount.ToBlockChainPrice()} در Binance انجام شد.";
                        await _messageManager.TelegramAdminMessageAsync(binanceTelegramMessage);
                    }
                    var withdrawResult = await _withdrawManager.WithdrawAsync(order);
                    if (withdrawResult.success)
                    {
                        order.OrderStatus = OrderStatus.Complete;
                        await _exchangeOrderService.UpdateAsync(order);
                        await _messageManager.TelegramAdminMessageAsync(withdrawResult.message);
                        result.Status = true;
                        result.Text = withdrawResult.message;
                    }
                    else
                    {
                        var errorMessage = $"خطا در هنگام برداشت از Binance برای سفارش شماره {order.Id}. Error: {withdrawResult.message}";
                        await _messageManager.TelegramAdminMessageAsync(errorMessage);
                        await _exchangeOrderService.UpdateAsync(order);
                        result.Text = errorMessage;
                    }
                }
                if (order.Errors.HasValue())
                {
                    result.Status = false;
                    result.Text = order.Errors;
                    await _messageManager.TelegramAdminMessageAsync(order.Errors);
                }
            }
            else
            {
                result.Text = "سفارش پرداخت نشده.";
            }

            return Json(result);
        }

        public async Task<ActionResult> ChangeOrderStatus(long id, OrderStatus orderStatus)
        {
            try
            {
                var exchangeOrder = await _exchangeOrderService.Table.SingleAsync(p => p.Id == id);
                exchangeOrder.OrderStatus = orderStatus;
                await _exchangeOrderService.UpdateAsync(exchangeOrder);
                var successText = $"وضعیت سفارش شماره {id} به {orderStatus.ToDisplay()} تغییر یافت";
                SuccessNotification(successText);
                return Json(new ResultModel() { Text = successText, Status = true });
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
                return Json(new ResultModel() { Text = ex.Message });
            }
        }

        public async Task<ActionResult> Reviews(SearchTerms terms, int page = 1, int pageSize = 30)
        {
            var query = _orderReviewService.TableNoTracking;

            var list = await query.ProjectToPagedListAsync<OrderReviewViewModel>(page, pageSize);

            ViewBag.TotalCount = await query.CountAsync();
            return View(list);
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> ChangeReviewVisibility(int id, Approvement visibility)
        {
            var result = new ResultModel();
            try
            {
                var review = await _orderReviewService.Table.SingleAsync(p => p.Id == id);
                review.Visibility = visibility;
                await _orderReviewService.UpdateAsync(review);
                result.Text = $"تغییر وضعیت نمایش {id} انجام شد.";
                result.Status = true;
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }
            return Json(result);
        }

        public async Task<ActionResult> PayIRPay(long id)
        {
            var result = new ResultModel();
            try
            {
                var order = await _exchangeOrderService.Table.Include(p => p.OrderWithdraws).SingleAsync(p => p.Id == id);
                if (order.PaymentStatus == PaymentStatus.Paid && order.OrderStatus != OrderStatus.Complete)
                {
                    order.ReceiveTransactionCode = "";
                    //order.OrderStatus = OrderStatus.Complete;
                    //await _exchangeOrderService.UpdateAsync(order);
                    var (status, text) = await _withdrawManager.WithdrawFromPayIRForOrderAsync(order);
                    result.Text = text;
                    result.Status = status;
                    if (status)
                        await TomanPaidSMSMessageAsync(order, "sheba");
                }
                else
                {
                    result.Text = "وضعیت پرداخت سفارش یا وضعیت سفارش برای انجام این عملیات اشتباه است.";
                }
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }

            await _messageManager.TelegramAdminMessageAsync(result.Text);
            return Json(result);
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> VandarPay(long id)
        {
            var result = new ResultModel();
            try
            {
                var order = await _exchangeOrderService.Table.SingleAsync(p => p.Id == id);
                if (order.PaymentStatus == PaymentStatus.Paid && order.OrderStatus != OrderStatus.Complete)
                {
                    order.ReceiveTransactionCode = "";
                    order.OrderStatus = OrderStatus.Complete;
                    await _exchangeOrderService.UpdateAsync(order);
                    var (status, text) = await _withdrawManager.WithdrawFromVandarForOrderAsync(order);
                    result.Text = text;
                    result.Status = status;
                    await TomanPaidSMSMessageAsync(order, "sheba");
                }
                else
                {
                    result.Text = "وضعیت پرداخت سفارش یا وضعیت سفارش برای انجام این عملیات اشتباه است.";
                }
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }

            await _messageManager.TelegramAdminMessageAsync(result.Text);
            return Json(result);
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> CancelVandarPay(long id)
        {
            var result = new ResultModel();
            try
            {
                var (status, text) = await _withdrawManager.CancelWithdrawFromVandarAsync(id);
                result.Text = text;
                result.Status = status;
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }

            await _messageManager.TelegramAdminMessageAsync(result.Text);
            return Json(result);
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> SatnaPaya(long id)
        {
            var result = new ResultModel();
            try
            {
                var order = await _exchangeOrderService.Table.SingleAsync(p => p.Id == id);
                order.ReceiveTransactionCode = "";
                order.OrderStatus = OrderStatus.Complete;
                await _exchangeOrderService.UpdateAsync(order);

                //Send Email, SMS or Sth. etc.
                result.Text = $"واریز وجه ریالی برای سفارش با شماره {order.Id} با مبلغ {order.ReceiveAmount.ToPriceWithoutFloat()} تومان ساتنا یا پایا شد.";
                result.Status = true;
                await _messageManager.TelegramAdminMessageAsync(result.Text);
                await TomanPaidSMSMessageAsync(order, "sheba");
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }
            return Json(result);
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> CardToCarded(long id, string transactionId)
        {
            var result = new ResultModel();
            try
            {
                var order = await _exchangeOrderService.Table.SingleAsync(p => p.Id == id);
                order.ReceiveTransactionCode = transactionId;
                order.OrderStatus = OrderStatus.Complete;
                await _exchangeOrderService.UpdateAsync(order);

                //Send Email, SMS or Sth. etc.
                result.Text = $"واریز وجه ریالی برای سفارش با شماره {order.Id} با مبلغ {order.ReceiveAmount.ToPriceWithoutFloat()} تومان با کد پیگیری {order.ReceiveTransactionCode} کارت به کارت شد.";
                result.Status = true;
                await _messageManager.TelegramAdminMessageAsync(result.Text);
                await TomanPaidSMSMessageAsync(order, "card");
            }
            catch (Exception ex)
            {
                result.Text = ex.Message;
            }
            return Json(result);
        }

        private async Task TomanPaidSMSMessageAsync(ExchangeOrder order, string type)
        {
            if (order.User.PhoneNumber.HasValue())
            {
                switch (type)
                {
                    case "card":
                        await _messageManager.TomanPaidAsync(order.User.PhoneNumber, order.Guid.ToString(), order.ReceiveTransactionCode);
                        break;
                    case "sheba":
                        await _messageManager.TomanPaidAsync(order.User.PhoneNumber, order.Guid.ToString(), null, order.ReceiveAmount <= 15000000 ? "پایا" : "ساتنا", "ارسال شده");
                        break;
                }
            }
        }
    }
}