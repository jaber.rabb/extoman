﻿using AutoMapper.QueryableExtensions;
using PagedList;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Utility;
using Xtoman.Utility.SeoHelper;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-money")]
    [DisplayName("ارزها")]
    public class CurrencyTypeController : BaseAdminController
    {
        private readonly ICurrencyTypeService _currencyTypeService;
        //private readonly IPaymentManager _paymentManager;
        private readonly IBalanceManager _balanceManager;
        public CurrencyTypeController(
            ICurrencyTypeService currencyTypeService,
            //IPaymentManager paymentManager,
            IBalanceManager balanceManager)
        {
            _currencyTypeService = currencyTypeService;
            //_paymentManager = paymentManager;
            _balanceManager = balanceManager;
            //DisplayName = "مدیریت لیست ارزها";
        }

        // GET: CurrencyType
        public virtual async Task<ActionResult> Index(int page = 1, int psize = 30)
        {
            var model = await _currencyTypeService.TableNoTracking
                .OrderByDescending(p => p.Id)
                .ProjectToPagedListAsync<CurrencyTypeViewModel>(page, psize);
            await PrepareAvailableAmountsAsync(model);
            return View(model);
        }

        private async Task PrepareAvailableAmountsAsync(IPagedList<CurrencyTypeViewModel> model)
        {
            var balances = await _balanceManager.GetAllBalancesAsync();
            foreach (var item in model)
                if (!item.Available.HasValue)
                    item.Available = balances
                        .Where(p => p.Type == item.Type)
                        .Select(p => p.Amount)
                        .Sum();
        }

        public virtual ActionResult Add()
        {
            var model = new AddEditCurrencyTypeViewModel()
            {
                MediaUrl = ""
            };
            return View(model);
        }

        [HttpPost]
        public virtual async Task<ActionResult> Add(AddEditCurrencyTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!await _currencyTypeService.TableNoTracking.Where(p => p.Type == model.Type).AnyAsync())
                {
                    await _currencyTypeService.InsertAsync(model.ToEntity());
                    SuccessNotification($"ارز جدید با نام {model.Name} ثبت گردید");
                    return RedirectToAction("Index");
                }
                else
                    ModelState.AddModelError("Type", $"نوع ارز {model.Type.ToDisplay()} قبلا وارد شده است.");
            }
            return View(model);
        }

        public virtual async Task<ActionResult> Edit(int id)
        {
            var model = await _currencyTypeService.TableNoTracking
                .Include(p => p.MediaId)
                .Include(p => p.Media.Url)
                .ProjectTo<AddEditCurrencyTypeViewModel>()
                .SingleOrDefaultAsync(p => p.Id == id);

            return View(model);
        }

        [HttpPost]
        public virtual async Task<ActionResult> Edit(AddEditCurrencyTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!await _currencyTypeService.TableNoTracking.Where(p => p.Type == model.Type && p.Id != model.Id).AnyAsync())
                {
                    var currencyType = await _currencyTypeService.Table.SingleOrDefaultAsync(p => p.Id == model.Id);
                    currencyType = model.ToEntity(currencyType);
                    currencyType.Media = null;
                    currencyType.MediaId = model.MediaId;
                    await _currencyTypeService.UpdateAsync(currencyType);
                    SuccessNotification($"ارز با نام {model.Name} ویرایش گردید.");
                    return RedirectToAction("Index");
                }
                else
                    ErrorNotification($"نوع ارز {model.Type.ToDisplay()} قبلا وارد شده است.");
                    ModelState.AddModelError("Type", $"نوع ارز {model.Type.ToDisplay()} قبلا وارد شده است.");
            }
            return View(model);
        }

        [HttpPost]
        public virtual async Task<ActionResult> Delete(int id)
        {
            await _currencyTypeService.DeleteAsync(id);
            SuccessNotification($"ارز انتخابی حذف گردید.");
            return View();
        }

        public virtual PartialViewResult CheckSEO(AddEditCurrencyTypeViewModel model)
        {
            SeoCheker seoChecker;
                seoChecker = new SeoCheker(model.Name + "(" + model.AlternativeName + ") چیست؟", $"{model.Name}, {model.AlternativeName}, قیمت {model.Name}, خرید {model.Name}, {model.Name} چیست", model.Url, $"{model.Name} یا {model.AlternativeName}، رمز ارز یا پول الکترونیکی رمز نگاری شده است که قیمت آن طی سال ها بسیار رشد کرده.", model.Description);
            seoChecker.Check();
            return PartialView("_SEO", seoChecker);
        }
    }
}