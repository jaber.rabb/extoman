﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Admin.ViewModels;
using Xtoman.Domain.Models;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc.Attributes;
using Xtoman.Service;
using Xtoman.Utility;
using Xtoman.Utility.SeoHelper;

namespace Xtoman.Admin.Controllers
{
    [MenuIcon("ti-exchange-vertical")]
    [DisplayName("نوع تبادل ها و نرخ")]
    public class ExchangeTypeController : BaseAdminController
    {
        #region Properties
        private IExchangeTypeService _exchangeTypeService;
        private ICurrencyTypeService _currencyTypeService;
        #endregion

        #region Constructor
        public ExchangeTypeController(
            IExchangeTypeService exchangeTypeService,
            ICurrencyTypeService currencyTypeService)
        {
            _exchangeTypeService = exchangeTypeService;
            _currencyTypeService = currencyTypeService;
            //DisplayName = "مدیریت تبادل ها و نرخ";
        }
        #endregion

        #region Actions
        public virtual async Task<ActionResult> Index(int page = 1, int psize = 30)
        {
            var model = await _exchangeTypeService.TableNoTracking
                .OrderByDescending(p => p.Id)
                .ProjectToPagedListAsync<ExchangeTypeViewModel>(page, psize);
            return View(model);
        }

        [HttpGet]
        public virtual async Task<ActionResult> MultipleRate()
        {
            var exchangeTypes = await _exchangeTypeService.TableNoTracking.
                ProjectToListAsync<ExchangeTypeViewModel>();

            exchangeTypes = exchangeTypes.OrderByDescending(p => p.UserExchangesCount).ToList();
            var model = new MultipleRateViewModel()
            {
                ExchangeTypes = exchangeTypes
            };
            return View(model);
        }

        [HttpPost]
        public virtual async Task<ActionResult> MultipleRate(MultipleRateViewModel model)
        {
            try
            {
                var successText = "";
                var entities = await _exchangeTypeService.Table.Where(p => model.ExchangeTypeIds.Any(x => x == p.Id)).ToListAsync();
                foreach (var item in entities)
                {
                    item.ExtraFeePercent = model.Rate;
                    successText += $"تغییر درصد سود ایکس تومن برای {item.FromCurrency.Name} به {item.ToCurrency.Name} به مقدار {model.Rate} انجام شد. ";
                }
                await _exchangeTypeService.UpdateAsync(entities);
                SuccessNotification(successText);
            }
            catch (Exception ex)
            {
                ErrorNotification(ex.Message);
            }
            return RedirectToAction("Index");
        }

        public virtual async Task<ActionResult> AutoAdd()
        {
            var resultCount = 0;
            var resultText = "";
            var currencyTypes = await _currencyTypeService.TableNoTracking.Where(p => !p.Invisible).ToListAsync();
            var exchangeTypes = await _exchangeTypeService.TableNoTracking.Where(p => !p.Invisible).ToListAsync();
            foreach (var fromCurrency in currencyTypes)
            {
                foreach (var toCurrency in currencyTypes)
                {
                    if (fromCurrency.Type != toCurrency.Type && !exchangeTypes.Any(x => x.FromCurrencyId == fromCurrency.Id && x.ToCurrencyId == toCurrency.Id))
                    {
                        var exchangeFiatCoinType = GetExchangeFiatCoinType(fromCurrency, toCurrency);
                        var extraFeePercent = GetExtraFeePercent(exchangeFiatCoinType);
                        var newExchangeType = new ExchangeType()
                        {
                            FromCurrencyId = fromCurrency.Id,
                            ToCurrencyId = toCurrency.Id,
                            ExchangeFiatCoinType = exchangeFiatCoinType,
                            ExtraFeePercent = extraFeePercent
                        };
                        await _exchangeTypeService.InsertAsync(newExchangeType);
                        resultCount++;
                        resultText += $"تبدیل {fromCurrency.Name} به {toCurrency.Name} با درصد {extraFeePercent.ToPriceWithoutFloat()} اضافه شد.";
                    }
                }
            }
            SuccessNotification($"تعداد {resultCount} نوع تبدیل اضافه شد. {resultText}");
            return RedirectToAction("Index");
        }

        private decimal GetExtraFeePercent(ExchangeFiatCoinType exchangeFiatCoinType)
        {
            switch (exchangeFiatCoinType)
            {
                case ExchangeFiatCoinType.CoinToFiatUSD:
                    return 15;
                case ExchangeFiatCoinType.CoinToFiatToman:
                    return 0;
                case ExchangeFiatCoinType.FiatUSDToCoin:
                    return 15;
                case ExchangeFiatCoinType.FiatTomanToCoin:
                    return 13;
                case ExchangeFiatCoinType.CoinToCoin:
                case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman:
                case ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham:
                case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham:
                    return 7;
                default:
                    return 13;
            }
        }

        [HttpGet]
        public virtual async Task<ActionResult> Add()
        {
            var model = new AddEditExchangeTypeViewModel()
            {
                CurrencyTypes = await CurrencyTypeSelectList()
            };
            return View(model);
        }

        [HttpPost]
        public virtual async Task<ActionResult> Add(AddEditExchangeTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var entity = model.ToEntity();
                await _exchangeTypeService.InsertAsync(entity);
                SuccessNotification($"نوع تبدیل جدید اضافه شد.");
                return RedirectToAction("Index");
            }
            model.CurrencyTypes = await CurrencyTypeSelectList();
            return View(model);
        }

        [HttpGet]
        public virtual async Task<ActionResult> Edit(int id)
        {
            var model = await _exchangeTypeService.TableNoTracking
                .ProjectTo<AddEditExchangeTypeViewModel>()
                .SingleOrDefaultAsync(p => p.Id == id);

            model.CurrencyTypes = await CurrencyTypeSelectList();
            return View(model);
        }

        public virtual async Task<ActionResult> Edit(AddEditExchangeTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            model.CurrencyTypes = await CurrencyTypeSelectList();
            return View(model);
        }

        public virtual async Task<ActionResult> Rate()
        {
            var model = await _exchangeTypeService.TableNoTracking
                .OrderByDescending(p => p.UserExchanges.Count())
                .ProjectToListAsync<ExchangeTypeViewModel>();

            return View(model);
        }

        public virtual async Task<ActionResult> RateBox()
        {
            var model = await _exchangeTypeService.TableNoTracking
                .OrderByDescending(p => p.UserExchanges.Count())
                .ProjectToListAsync<ExchangeTypeViewModel>();

            return View(model);
        }

        [HttpPost]
        public virtual async Task<ActionResult> RateByType(decimal percent, ExchangeFiatCoinType? type = null)
        {
            try
            {
                var query = _exchangeTypeService.Table.Where(p => p.ExtraFeePercent != percent);
                if (type.HasValue)
                    query = query.Where(p => p.ExchangeFiatCoinType == type.Value);

                var exchangeTypes = await query.ToListAsync();
                foreach (var item in exchangeTypes)
                {
                    item.ExtraFeePercent = percent;
                    await _exchangeTypeService.UpdateAsync(item);
                }
                SuccessNotification($"سود ایکس تومن برای تعداد {exchangeTypes.Count} تبدیل با نوع {type.ToDisplay()} به درصد {percent} تغییر یافت.");
            }
            catch (Exception ex)
            {
                ex.LogError();
                ErrorNotification(ex.Message);
            }
            return RedirectToAction("Rate");
        }

        public virtual async Task<ActionResult> EditRatePercent(int id)
        {
            var model = await _exchangeTypeService.TableNoTracking
                .Where(p => p.Id == id)
                .ProjectToSingleAsync<ExchangeTypeViewModel>();

            return View(model);
        }

        [HttpPost]
        public virtual async Task<ActionResult> EditRatePercent(int Id, decimal ExtraFeePercent)
        {
            if (ModelState.IsValid)
            {
                var entity = await _exchangeTypeService.Table
                .Where(p => p.Id == Id)
                .SingleAsync();

                try
                {
                    entity.ExtraFeePercent = ExtraFeePercent;
                    await _exchangeTypeService.UpdateAsync(entity);
                    SuccessNotification($"تغییر درصد سود تبدیل {entity.FromCurrency.Name} به {entity.ToCurrency.Name} به {entity.ExtraFeePercent} تغییر یافت.");
                    return RedirectToAction("Rate");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("ExtraFeePercent", ex.Message);
                }
            }

            var model = new ExchangeTypeViewModel() { Id = Id, ExtraFeePercent = ExtraFeePercent };
            return View(model);
        }

        public virtual PartialViewResult CheckSEO(AddEditExchangeTypeViewModel model)
        {
            SeoCheker seoChecker;
            var exchangeType = _exchangeTypeService.TableNoTracking
                .Where(p => p.FromCurrencyId == model.FromCurrencyId && p.ToCurrencyId == model.ToCurrencyId)
                .Include(p => p.FromCurrency)
                .Include(p => p.ToCurrency)
                .Single();

            var fromName = exchangeType.FromCurrency.Name;
            var toName = exchangeType.ToCurrency.Name;

            var title = "";
            var url = "";
            var keywords = "";
            var metaDescription = "";
            if (exchangeType.FromCurrency.Type == ECurrencyType.Shetab)
            {
                // Buy
                title = $"خرید {toName} با بهترین قیمت";
                url = $"خرید-{toName}";
                keywords = $"خرید {toName}، خرید و فروش {toName}، خرید {toName} با بهترین قیمت، خرید اتوماتیک {toName}، قیمت {toName} در سال 2019، تبدیل ریال به {toName}، تبدیل تومان به {toName}، قیمت امروز {toName}";
                metaDescription = $"خرید {toName} با بهترین قیمت، خرید و فروش {toName} به صورت اتوماتیک همراه با واریز آنی";
            }
            else if (exchangeType.ToCurrency.Type == ECurrencyType.Shetab)
            {
                // Sell
                title = $"فروش {fromName} با بهترین قیمت";
                url = $"فروش-{fromName}";
                keywords = $"فروش {fromName}، خرید و فروش {fromName}، فروش {fromName} با بهترین قیمت، فروش اتوماتیک {fromName}، قیمت {fromName} در سال {DateTime.Now.Year}، تبدیل {fromName} به ریال، تبدیل {fromName} به تومان، قیمت امروز {fromName}";
                metaDescription = $"فروش {fromName} با بهترین قیمت، خرید و فروش {toName} به صورت اتوماتیک همراه با واریز آنی";
            }
            else
            {
                // Exchange
                title = $"تبدیل {fromName} به {toName} با بهترین نرخ";
                url = $"{fromName}-به-{toName}";
                keywords = title + $"، به صورت اتوماتیک با بهترین نرخ تبدیل، فروش {fromName} و خرید {toName}، تبدیل کوین به کوین";
                metaDescription = title + $"، به صورت اتوماتیک با بهترین نرخ تبدیل، فروش {fromName} و خرید {toName}، تبدیل کوین به کوین";
            }

            seoChecker = new SeoCheker(title, keywords, url, metaDescription, model.Description);
            seoChecker.Check();
            return PartialView("_SEO", seoChecker);
        }
        #endregion

        #region Helpers
        private ExchangeFiatCoinType GetExchangeFiatCoinType(CurrencyType fromCurrency, CurrencyType toCurrency)
        {
            if (fromCurrency.IsFiat)
            {
                if (fromCurrency.Type == ECurrencyType.Shetab)
                {
                    if (toCurrency.IsFiat)
                    {
                        return ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham;
                    }
                    else
                    {
                        return ExchangeFiatCoinType.FiatTomanToCoin;
                    }
                }
                else
                {
                    if (toCurrency.IsFiat)
                    {
                        if (toCurrency.Type == ECurrencyType.Shetab)
                        {
                            return ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman;
                        }
                        else
                        {
                            return ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham;
                        }
                    }
                    else
                    {
                        return ExchangeFiatCoinType.FiatUSDToCoin;
                    }
                }
            }
            else
            {
                if (toCurrency.IsFiat)
                {
                    if (toCurrency.Type == ECurrencyType.Shetab)
                    {
                        return ExchangeFiatCoinType.CoinToFiatToman;
                    }
                    else
                    {
                        return ExchangeFiatCoinType.CoinToFiatUSD;
                    }
                }
                else
                {
                    return ExchangeFiatCoinType.CoinToCoin;
                }
            }
        }

        private async Task<SelectList> CurrencyTypeSelectList()
        {
            var entities = await _currencyTypeService.TableNoTracking
                .Select(p => new { Text = p.Name, Value = p.Id })
                .ToListAsync();
            return new SelectList(entities);
        }
        #endregion
    }
}