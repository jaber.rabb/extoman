﻿namespace System.Web.Mvc
{
    public static partial class MessageBox
    {
        public static Noty Noty(string text, MessageType type = MessageType.Alert, bool modal = false, MessageAlignment layout = MessageAlignment.Center, bool dismissQueue = false)
        {
            return new Noty().Text(text).Type(type).Modal(modal).Layout(layout).DismissQueue(dismissQueue);
        }
        public static Noty Noty(this HtmlHelper helper, string text, MessageType type = MessageType.Alert, bool modal = false, MessageAlignment layout = MessageAlignment.Center, bool dismissQueue = false)
        {
            return new Noty(helper).Text(text).Type(type).Modal(modal).Layout(layout).DismissQueue(dismissQueue);
        }
    }
}