﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Web.Mvc.Html;
using Xtoman.Component;
using Xtoman.Utility;

namespace System.Web.Mvc
{
    public static class BootstrapControl
    {
        public static MvcHtmlString BsTextBoxFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string icon = null, ComponentDirection? dir = null, int lable_col = 2, int editor_col = 4, object htmlAttribute = null)
        {
            string displayName = null;
            SetVariables(html, expression, ref displayName, out string style, out string dirName, out string label, out string validator, out object value, lable_col, dir);
            var attributes = ComponentUtility.MergeAttributes(htmlAttribute, new { @class = "form-control", placeholder = displayName, dir = dirName, style = style });
            var editor = html.TextBoxFor(expression, attributes);
            var result = SetTemplate(label, icon, editor_col, validator, editor.ToHtmlString(), dirName);
            return MvcHtmlString.Create(result);
        }

        public static MvcHtmlString BsEditorFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string icon = null, ComponentDirection? dir = null, int lable_col = 2, int editor_col = 4, object htmlAttribute = null)
        {
            string displayName = null;
            SetVariables(html, expression, ref displayName, out string style, out string dirName, out string label, out string validator, out object value, lable_col, dir);
            var attributes = ComponentUtility.MergeAttributes(htmlAttribute, new { @class = "form-control", placeholder = displayName, dir = dirName, style = style });
            var editor = html.EditorFor(expression, new { htmlAttributes = attributes });
            var result = SetTemplate(label, icon, editor_col, validator, editor.ToHtmlString(), dirName);
            return MvcHtmlString.Create(result);
        }

        public static MvcHtmlString BsCheckboxEditorFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string icon = null, ComponentDirection? dir = null, int lable_col = 2, int editor_col = 4, object htmlAttribute = null)
        {
            string displayName = null;
            SetVariables(html, expression, ref displayName, out string style, out string dirName, out string label, out string validator, out object value, lable_col, dir);
            var attributes = ComponentUtility.MergeAttributes(htmlAttribute, new { placeholder = displayName, dir = dirName, style = style });
            var editor = html.EditorFor(expression, new { htmlAttributes = attributes });
            var result = SetTemplate(label, icon, editor_col, validator, editor.ToHtmlString(), dirName);
            return MvcHtmlString.Create(result);
        }

        public static MvcHtmlString BsTextAreaFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string icon = null, int row = 3, ComponentDirection? dir = null, int lable_col = 2, int editor_col = 4, object htmlAttribute = null)
        {
            string displayName = null;
            SetVariables(html, expression, ref displayName, out string style, out string dirName, out string label, out string validator, out object value, lable_col, dir);
            var attributes = ComponentUtility.MergeAttributes(htmlAttribute, new { @class = "form-control", placeholder = displayName, rows = row, dir = dirName, style = style });
            var editor = html.TextAreaFor(expression, attributes);
            var result = SetTemplate(label, icon, editor_col, validator, editor.ToHtmlString(), dirName);
            return MvcHtmlString.Create(result);
        }

        public static MvcHtmlString BsPasswordFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string icon = null, ComponentDirection? dir = null, int lable_col = 2, int editor_col = 4, object htmlAttribute = null)
        {
            string displayName = null;
            SetVariables(html, expression, ref displayName, out string style, out string dirName, out string label, out string validator, out object value, lable_col, dir);
            var attributes = ComponentUtility.MergeAttributes(htmlAttribute, new { @class = "form-control", placeholder = displayName, dir = dirName, style = style });
            var editor = html.PasswordFor(expression, attributes);
            var result = SetTemplate(label, icon, editor_col, validator, editor.ToHtmlString(), dirName);
            return MvcHtmlString.Create(result);
        }

        public static MvcHtmlString BsDropDownFor<TModel, TValue, T1, T2>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, Dictionary<T1, T2> source, string defaultValue = null, string icon = null, ComponentDirection? dir = null, int lable_col = 2, int editor_col = 4, object htmlAttribute = null)
        {
            string displayName = null;
            SetVariables(html, expression, ref displayName, out string style, out string dirName, out string label, out string validator, out object value, lable_col, dir);
            var selectList = new SelectList(source, "Key", "Value", value);
            var items = selectList.Cast<SelectListItem>().ToList();
            if (defaultValue.HasValue())
                items.Insert(0, new SelectListItem { Value = null, Text = defaultValue });
            var attributes = ComponentUtility.MergeAttributes(htmlAttribute, new { @class = "form-control", placeholder = displayName, dir = dirName, style = style });
            var editor = html.DropDownListFor(expression, items, attributes);
            var result = SetTemplate(label, icon, editor_col, validator, editor.ToHtmlString(), dirName);
            return MvcHtmlString.Create(result);
        }

        public static MvcHtmlString BsListBoxFor<TModel, TValue, T1, T2>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, Dictionary<T1, T2> source, string defaultValue = null, string icon = null, ComponentDirection? dir = null, int lable_col = 2, int editor_col = 4, object htmlAttribute = null)
        {
            string displayName = null;
            SetVariables(html, expression, ref displayName, out string style, out string dirName, out string label, out string validator, out object value, lable_col, dir);
            var selectList = new SelectList(source, "Key", "Value", value);
            var items = selectList.Cast<SelectListItem>().ToList();
            if (!string.IsNullOrEmpty(defaultValue))
                items.Insert(0, new SelectListItem { Value = null, Text = defaultValue });
            var attributes = ComponentUtility.MergeAttributes(htmlAttribute, new { @class = "form-control", placeholder = displayName, dir = dirName, style = style });
            var editor = html.ListBoxFor(expression, items, attributes);
            var result = SetTemplate(label, icon, editor_col, validator, editor.ToHtmlString(), dirName);
            return MvcHtmlString.Create(result);
        }

        public static IEnumerable<SelectListItem> GetDropDownList<T>(this IEnumerable<T> source, string textField = "Name", string valueField = "Id", string selectedValue = null) where T : class
        {
            var list = new List<SelectListItem>();// { new SelectListItem { Text = "-انتخاب کنید-", Value = string.Empty } };
            var lisData = source.Select(m => new SelectListItem
            {
                Text = m.GetType().GetProperty(textField).GetValue(m, null).ToString(),
                Value = m.GetType().GetProperty(valueField).GetValue(m, null).ToString(),
                Selected = (selectedValue != null) && ((string)m.GetType().GetProperty(valueField).GetValue(m, null) == selectedValue),
            }).ToList();
            list.AddRange(lisData);
            return list;
        }

        public static MvcHtmlString BsDateTimePickerFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, bool enableTimePicker = false, string icon = null, ComponentDirection? dir = null, int lable_col = 2, int editor_col = 4, object htmlAttribute = null)
        {
            string displayName = null, style, dirName, label, validator;
            object value;
            SetVariables(html, expression, ref displayName, out style, out dirName, out label, out validator, out value, lable_col, dir);
            var attributes = ComponentUtility.MergeAttributes(htmlAttribute, new { @class = "form-control", placeholder = displayName, dir = dirName, style = style });
            var editor = html.BootstrapDateTimePickerFor(expression, attributes, enableTimePicker);
            var result = SetTemplate(label, icon, editor_col, validator, editor.ToHtmlString(), dirName);
            return MvcHtmlString.Create(result);
        }

        private static void SetVariables<TModel, TValue>(this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression,
            ref string displayName,
            out string style,
            out string dirName,
            out string label,
            out string validator,
            out object value,
            int lable_col,
            ComponentDirection? dir = null)
        {
            var metadata = expression == null ? null : ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            var htmlFieldName = expression == null ? null : ExpressionHelper.GetExpressionText(expression);
            if (expression != null)
                displayName = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
            value = expression == null ? null : metadata.Model;
            var direction = dir ?? (Thread.CurrentThread.CurrentCulture.TextInfo.IsRightToLeft ? ComponentDirection.RightToLeft : ComponentDirection.LeftToRight);
            var isRtl = direction == ComponentDirection.RightToLeft;
            dirName = direction.ToDescription();
            style = string.Format("direction: {0}; text-align: {1};", dirName, isRtl ? "right" : "left");
            if (expression == null)
                label = html.Label(displayName, new { @class = "control-label col-" + lable_col }).ToHtmlString();
            else
                label = html.LabelFor(expression, new { @class = "control-label col-" + lable_col }).ToHtmlString();
            validator = expression == null ? null : html.ValidationMessageFor(expression, null, new { style = "display: table-footer-group;" }).ToHtmlString();
        }

        private static string SetTemplate(string label, string icon, int editor_col, string validator, string editor, string dirName)
        {
            var cssClass = (string.IsNullOrEmpty(icon) ? "no-feedback" : "with-feedback");
            var iconElement = (string.IsNullOrEmpty(icon) ? "" : $@"<span class=""input-group-addon""><i class=""{icon}""></i></span>");
            return
                $@"<div class=""form-group row"">
                {label}
                <div class=""input-group {cssClass} component-{dirName} col-{editor_col} addOn"">
                    {validator}
                    {iconElement}
                    {editor}
                </div>
            </div>";
        }
    }
}