﻿using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc.Html;
using Xtoman.Component;

namespace System.Web.Mvc
{
    public static class BootstrapDateTimePicker
    {
        private const string location = "/Plugins/bootstrap-datetimepicker/build";
        public static MvcHtmlString BootstrapDateTimePickerFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, object htmlAttributes = null, bool enableTimePicker = false, ComponentDirection? dir = null, int lable_col = 2, int editor_col = 4)
        {
            var data = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
            var value = "";
            var castedValue = data.Model as DateTime?;
            if (castedValue?.Equals(default(DateTime)) == false)
                value = helper.DisplayFor(expression).ToString();

            var attributes = new Dictionary<string, object>() {
                { "Value", value },
                { "style", "cursor: pointer;" },
            };

            var elementId = helper.ViewData.TemplateInfo.GetFullHtmlFieldId(ExpressionHelper.GetExpressionText(expression));
            var mergAttr = ComponentUtility.MergeAttributes(htmlAttributes, attributes);
            var result = helper.TextBoxFor(expression, mergAttr).ToString();

            helper.StyleFileSingle(@"<link href=""" + location + @"/css/bootstrap-datetimepicker.min.css"" rel=""stylesheet"" />");
            helper.ScriptFileSingle(@"<script src=""/Scripts/moment.min.js""></script>");
            helper.ScriptFileSingle(@"<script src=""" + location + @"/js/bootstrap-datetimepicker.min.js""></script>");
            helper.Script($@"<script>
$('#{elementId}').datetimepicker();
</script>");

            return new MvcHtmlString(result);
        }
    }
}
