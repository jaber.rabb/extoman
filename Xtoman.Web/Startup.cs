﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Xtoman.Web.Startup))]
namespace Xtoman.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
