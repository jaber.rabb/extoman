﻿using System.Web.Mvc;
using System.Web.Mvc.Routing.Constraints;
using System.Web.Routing;

namespace Xtoman.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            const string digitRegex = @"\d+";

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("GetRobotsText", "robots.txt", new { controller = "Misc", action = "RobotsText" });
            routes.MapRoute("GetSitemapXml", "sitemap.xml", new { controller = "Misc", action = "SitemapXml" });

            routes.MapRoute(
                "RefLink",
                "x{r}",
                defaults: new { controller = "Account", action = "RedirectRefLink", r = "{r}" },
                constraints: new { r = digitRegex }
             );

            routes.MapRoute(name: "SinglePost", url: "Post/{url}", defaults: new { controller = "Blog", action = "Single", url = UrlParameter.Optional });

            routes.MapRoute(name: "Blog", url: "Blog/{*parameters}", defaults: new { controller = "Blog", action = "Index" });

            //routes.MapRoute(name: "BlogCategory", url: "Category/{url}/{*childurl}", defaults: new { controller = "Blog", action = "Category" });

            //routes.MapRoute(name: "BlogTag", url: "Tag/{url}", defaults: new { controller = "Blog", action = "Tag" });

            routes.MapRoute(
                name: "ExchangeOrderDetail",
                url: "ExchangeOrder/{orderGuid}",
                defaults: new { controller = "ExchangeOrder", action = "Index" },
                constraints: new { orderGuid = new GuidRouteConstraint() }
            );

            routes.MapRoute(
                name: "ExchangeSell",
                url: "Exchange/فروش-{fromUrl}",
                defaults: new { controller = "Exchange", action = "Index", toUrl = "شتاب-تومان-ریال" }
            );

            routes.MapRoute(
                name: "ExchangeBuy",
                url: "Exchange/خرید-{toUrl}",
                defaults: new { controller = "Exchange", action = "Index", fromUrl = "شتاب-تومان-ریال" }
            );

            routes.MapRoute(
                name: "Exchange",
                url: "Exchange/{fromUrl}-به-{toUrl}",
                defaults: new { controller = "Exchange", action = "Index" }
            );

            routes.MapRoute(
                name: "CurrencyTypesList",
                url: "Currency",
                defaults: new { controller = "Currency", action = "Index" }
            );

            routes.MapRoute(
                name: "CurrencyType",
                url: "Currency/{url}",
                defaults : new { controller = "Currency", action = "Single", url = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ExchangeBuyWithSegments",
                url: "Exchange/خرید-{toUrl}/{*segments}",
                defaults: new { controller = "Exchange", action = "Index", fromUrl = "شتاب-تومان-ریال", segments = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ExchangeSellWithSegments",
                url: "Exchange/فروش-{fromUrl}/{*segments}",
                defaults: new { controller = "Exchange", action = "Index", toUrl = "شتاب-تومان-ریال", segments = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ExchangeWithSegments",
                url: "Exchange/{fromUrl}-به-{toUrl}/{*segments}",
                defaults : new { controller = "Exchange", action = "Index", segments = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Terms",
                url: "Terms",
                defaults: new { controller = "Home", action = "Terms" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}
