﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.Google;
using Owin;
using StructureMap.Web;
using System;
using System.Web;
using Xtoman.Framework.DependencyResolution;
using Xtoman.Service;

namespace Xtoman.Web
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit https://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            IoC.Container.Configure(config =>
            {
                config.For<IDataProtectionProvider>()
                      .HybridHttpOrThreadLocalScoped()
                      .Use(() => app.GetDataProtectionProvider());
            });

            // This is necessary for `GenerateUserIdentityAsync` and `SecurityStampValidator` to work internally by ASP.NET Identity 2.x
            app.CreatePerOwinContext(() => (AppUserManager)IoC.Container.GetInstance<IAppUserManager>());

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            var cookieAuthOptions = new CookieAuthenticationOptions()
            {
                CookieManager = new SystemWebChunkingCookieManager(),
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                CookieDomain = HttpContext.Current.Request.IsLocal ? null : ".extoman.co",
                Provider = new CookieAuthenticationProvider
                {
                    //OnApplyRedirect = ctx =>
                    //{
                    //    if (!ctx.Request.IsAjaxRequest())
                    //        ctx.Response.Redirect(ctx.RedirectUri);
                    //},

                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.
                    OnValidateIdentity = IoC.Container.GetInstance<IAppUserManager>().OnValidateIdentity()
                },
                ExpireTimeSpan = TimeSpan.FromMinutes(30)
            };

            app.UseCookieAuthentication(cookieAuthOptions);

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            //app.UseFacebookAuthentication(
            //   appId: "",
            //   appSecret: "");

            var google = new GoogleOAuth2AuthenticationOptions
            {
                ClientId = "902283180154-e78348ga3js2r7fdkf5b1ans6oujd9fj.apps.googleusercontent.com",
                ClientSecret = "R4j3f4HdCrqRJOL7PEpVhxnr"
                //CallbackPath = new PathString("/Account/ExternalLoginCallback")
            };
            google.Scope.Add("https://www.googleapis.com/auth/plus.login");
            google.Scope.Add("https://www.googleapis.com/auth/plus.me");
            google.Scope.Add("https://www.google.com/m8/feeds/");
            google.Scope.Add("https://www.googleapis.com/auth/userinfo.email");
            google.Scope.Add("https://www.googleapis.com/auth/userinfo.profile");

            app.UseGoogleAuthentication(google);
        }
    }
}