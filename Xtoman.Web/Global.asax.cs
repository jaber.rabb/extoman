﻿using StructureMap.Web.Pipeline;
using System;
using System.Data.Entity.Infrastructure.Interception;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Xtoman.Data;
using Xtoman.Framework.DependencyResolution;
using Xtoman.Utility;
using Xtoman.Web.AutoMapper;

namespace Xtoman.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            MvcHandler.DisableMvcResponseHeader = true;
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ModelBinderConfig.Register(ModelBinders.Binders);
            AttributeAdapterConfig.Register();
            AutoMapperConfig.Configure();

            DbInterception.Add(new ElmahEfInterceptor());
            SetDbInitializer();
            //Set current Controller factory as StructureMapControllerFactory
            ControllerBuilder.Current.SetControllerFactory(new StructureMapControllerFactory());
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var error = Server.GetLastError();
            Server.ClearError();
            var httpException = error as HttpException;
            Response.StatusCode = httpException != null ? httpException.GetHttpCode() : (int)HttpStatusCode.InternalServerError;
            //Response.Headers.Add("ErrorMessage", httpException.Message);
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Headers.Remove("X-AspNet-Version");
            HttpContextLifecycle.DisposeAndClearAll();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        public class StructureMapControllerFactory : DefaultControllerFactory
        {
            protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
            {
                if (controllerType == null)
                    throw new HttpException(404, $"Page not found: { requestContext.HttpContext.Request.RawUrl }");
                //throw new InvalidOperationException();
                return IoC.Container.GetInstance(controllerType) as Controller;
            }
        }

        private static void SetDbInitializer()
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
            IoC.Container.GetInstance<IUnitOfWork>().ForceDatabaseInitialize();
        }
    }
}