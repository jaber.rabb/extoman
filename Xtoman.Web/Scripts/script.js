﻿var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
var isFromAmount = true;
var toUnitSign;
var fromUnitSign;
var previousTo;
var previousFrom;
var loadingTop;
var toMinAmount;
var fromMinAmount;
var toMaxAmount;
var fromMaxAmount;
var rateXHR;
//IE9 placeholder fallback
//credits http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html
if (!Modernizr.input.placeholder) {
    $('[placeholder]').focus(function () {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
            input.val('');
        }
    }).blur(function () {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
            input.val(input.attr('placeholder'));
        }
    }).blur();
    $('[placeholder]').parents('form').submit(function () {
        $(this).find('[placeholder]').each(function () {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
            }
        })
    });
}

function postAntiForgeryAjax(postData, url, handleData) {
    if (typeof handleData === 'undefined') { handleData = function (i) { } }
    var token = $('form input[name="__RequestVerificationToken"]').val();
    postData.__RequestVerificationToken = token;
    $.ajax({
        url: url,
        type: "POST",
        data: postData,
        success: function (data, textStatus, jqXHR) {
            handleData(data);
        }
    });
}

function postAjaxReload(postData, url, handleData) {
    if (typeof handleData === 'undefined') { handleData = function (i) { } }
    $.ajax({
        url: url,
        type: "POST",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(postData),
        success: function (data, textStatus, jqXHR) {
            handleData(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 503) {
                location.reload();
            }
        }
    });
}

function postAjax(postData, url, handleData) {
    if (typeof handleData === 'undefined') { handleData = function (i) { } }
    $.ajax({
        url: url,
        type: "POST",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(postData),
        success: function (data, textStatus, jqXHR) {
            handleData(data);
        }
    });
};

if (!Modernizr.svg) {
    $("img[src$='.svg']")
        .attr("src", fallback);
}

$('.dropdown-menu').on("click", "a.dropdown-item", function (e) {
    var dropdown = $(this).parent();
    dropdown.find($('.dropdown-item')).removeClass('active');
    dropdown.parent().find('button').text($(this).text());
    $(this).addClass('active');
});

$('.selectpicker').select2({
    width: '100%',
    dir: "rtl",
    language: "fa",
    templateResult: function (data) {
        var content = $(data.element).data("content");
        if (content && content != null) {
            return $(content);
        }
        return data.text;
    },
    templateSelection: function (data) {
        var content = $(data.element).data("selection");
        if (content && content != null) {
            return $(content);
        }
        return data.text;
    },
    matcher: function (params, data) {
        if ($.trim(params.term) === '') return data;
        if (typeof data.text === 'undefined') return null;

        // `params.term` should be the term that is used for searching
        // `data.text` is the text that is displayed for the data object
        if (data.text.toUpperCase().indexOf(params.term.toUpperCase()) > -1
            || $(data.element).data("tokens").toUpperCase().indexOf(params.term.toUpperCase()) > -1) {
            var modifiedData = $.extend({}, data, true);

            return modifiedData;
        }

        // Return `null` if the term should not be displayed
        return null;
    }
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    if ($('.ShowRate').length) {
        $('.ShowRate').barrating({
            theme: 'bars-reversed',
            readonly: true,
            showSelectedRating: true,
        });
    }
    if ($('#Rate').length) {
        $('#Rate').barrating({
            theme: 'bars-reversed',
            initialRating: 5,
            showSelectedRating: true
        });
    }
});

function roundNumber(num, scale) {
    if (!("" + num).includes("e")) {
        return +(Math.round(num + "e+" + scale) + "e-" + scale);
    } else {
        var arr = ("" + num).split("e");
        var sig = ""
        if (+arr[1] + scale > 0) {
            sig = "+";
        }
        return +(Math.round(+arr[0] + "e" + sig + (+arr[1] + scale)) + "e-" + scale);
    }
}

//const numberWithCommas = (x) => {
//    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
//}

const numberWithCommas = (x) => {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function showCardLoading(cardSelectorId) {
    var $card = $("#" + cardSelectorId);
    var loadingOverlay = $card.parent().find($(".loading-overlay"));
    var loadingImg = $card.parent().find($(".loading-img"));
    loadingOverlay.css({
        opacity: 0.5,
        top: loadingTop == null ? 35 : loadingTop,
        width: $card.outerWidth(),
        height: $card.outerHeight()
    });

    loadingImg.css({
        top: ($card.height() / 2) - 16,
        left: ($card.width() / 2) - 16
    });

    loadingOverlay.fadeIn();
}

function hideBothCardLoading() {
    $("#fromCard").parent().find($(".loading-overlay")).fadeOut();
    $("#toCard").parent().find($(".loading-overlay")).fadeOut();
}

function hideCardLoading(cardSelectorId) {
    var loadingOverlay = $("#" + cardSelectorId).parent().find($(".loading-overlay"));
    loadingOverlay.fadeOut();
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}

function getSigns() {
    if ($('#to').length && $('#to')[0].nodeName.toLowerCase() !== 'input')
        toUnitSign = $('#to option:selected').data('unitsign');
    if ($('#from').length && $('#from')[0].nodeName.toLowerCase() !== 'input')
        fromUnitSign = $('#from option:selected').data('unitsign');
}

function setSigns() {
    $('#toaddon').html(toUnitSign);
    $('#fromaddon').html(fromUnitSign);
}

function swapExchange(top) {
    var from = $('#from').val();
    var to = $('#to').val();
    var fromamount = $('#fromamount').val();
    var toamount = $('#toamount').val();
    $('#from').val(to).trigger("change");
    $('#to').val(from).trigger("change");
    isFromAmount = isFromAmount ? false : true;
    if (!isFromAmount && fromamount) {
        $('#fromamount').val('').trigger("change");
        $('#toamount').val(fromamount).trigger("change");
    }
    if (isFromAmount && toamount) {
        $('#toamount').val('').trigger("change");
        $('#fromamount').val(toamount).trigger("change");
    }
}

function getExchangeRate() {
    if (rateXHR) {
        rateXHR.abort();
    }
    var from = $('#from').val();
    var to = $('#to').val();
    if (from === to) {
        $('#rateContainer').html("<small>ارز دریافت یا پرداخت  را انتخاب نمایید.</small>");
        return;
    };
    if (typeof shetabId !== 'undefined') {
        if (from == shetabId) {
            $('#exchangeBtn').text('خرید');
        }
        else if (to == shetabId) {
            $('#exchangeBtn').text('فروش');
        }
        else {
            $('#exchangeBtn').text('تبدیل');
        }
    }
    $('#rateContainer').html('<span class="fas fa-spinner fa-pulse"></span> <small>در حال بررسی ...</small>');
    if ($('#receiveContainer').length || $('#payContainer').length) {
        if (isFromAmount) {
            $('#receiveContainer').html('<span class="fas fa-spinner fa-pulse"></span> <small>در حال بررسی ...</small>');
        } else {
            $('#payContainer').html('<span class="fas fa-spinner fa-pulse"></span> <small>در حال بررسی ...</small>');
        }
    }
    showCardLoading(isFromAmount ? "toCard" : "fromCard");
    getSigns();
    var fromamount = $('#fromamount').val();
    var toamount = $('#toamount').val();
    var postData = {
        fromId: from,
        toId: to,
        amount: isFromAmount ? fromamount : toamount,
        isFrom: isFromAmount ? 1 : 0
    }
    rateXHR = $.ajax({
        method: 'POST',
        url: rateUrl,
        data: postData,
        success: function (data) {
            var exRatePrecision = data.ReversedRate ? data.ToPrecision : data.FromPrecision;
            var exRate = numberWithCommas(roundNumber(data.Rate, exRatePrecision))
            var rateContainerText = data.ReversedRate ? "<small>هر " + fromUnitSign + " برابر با " + exRate + " " + toUnitSign + "</small>" : "<small>هر " + toUnitSign + " برابر با " + exRate + " " + fromUnitSign + "</small>";
            $('#rateContainer').html(rateContainerText);
            if (postData != null) {
                $('#fromamount').val(roundNumber(data.FromAmount, data.FromPrecision));
                $('#toamount').val(roundNumber(data.ToAmount, data.ToPrecision));
            }
            if ($('#receiveContainer').length || $('#payContainer').length) {
                if (isFromAmount) {
                    $('#receiveContainer').html(numberWithCommas(roundNumber(data.Rate * amount, exRatePrecision)) + " " + toUnitSign);
                } else {
                    $('#payContainer').html(numberWithCommas(roundNumber(data.Rate * amount, exRatePrecision)) + " " + fromUnitSign);
                }
            }
            if (data.FromMinAmount) {
                fromMinAmount = parseFloat(data.FromMinAmount);
            }
            if (data.ToMinAmount) {
                toMinAmount = parseFloat(data.ToMinAmount);
            }
            if (data.ToMaxAmount) {
                toMaxAmount = parseFloat(data.ToMaxAmount);
            }
            if (data.FromMaxAmount) {
                fromMaxAmount = parseFloat(data.FromMaxAmount);
            }
            var finalReceive;
            if (data.TransactionFee) {
                var transactionFee = roundNumber(data.TransactionFee, data.ToPrecision);
                $('#feeContainer').val(transactionFee + " " + toUnitSign);
                finalReceive = numberWithCommas(roundNumber((data.ToAmount - data.TransactionFee), data.ToPrecision));
            } else {
                $('#feeContainer').val("0 " + toUnitSign);
                finalReceive = numberWithCommas(roundNumber(data.ToAmount, data.ToPrecision));
            }
            $('#finalReceiveContainer').val(finalReceive + " " + toUnitSign);
        },
        complete: function (data) {
            //hideCardLoading(isFromAmount ? "toCard" : "fromCard");
            hideBothCardLoading();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //hideCardLoading(isFromAmount ? "toCard" : "fromCard");
            hideBothCardLoading();
            $('#finalReceiveContainer').val("خطا در هنگام دریافت نرخ!");
        }
    });
}

function getAllBalances() {
    $("[class^='Available-']").each(function (i, element) {
        $(element).html('<span class="fas fa-spinner fa-pulse"></span> <small>در حال بررسی ...</small>');
    });
    $.ajax({
        method: 'POST',
        url: balanceUrl,
        success: function (data) {
            if (data && data.length > 0) {
                $.each(data, function (index, item) {
                    $('.Available-' + item.UnitSign).each(function (f, v) {
                        $(v).html(numberWithCommas((roundNumber(item.Available, 8)) + " " + item.UnitSign));
                    });
                });
            }
        }
    });
}

function updateFrom() {
    previousFrom = $('#from').val();
    setCookie('exchangeFrom', previousFrom, 7);
    //getExchangeRate();
}

function updateTo() {
    previousTo = $('#to').val();
    setCookie('exchangeTo', previousTo, 7);
    //getExchangeRate();
}

$('#exchange-picker').on('change keyup keypress paste', '#toamount', function (event) {
    if (event.originalEvent !== undefined) isFromAmount = false;
});

$('#exchange-picker').on('change keyup keypress paste', '#fromamount', function (event) {
    if (event.originalEvent !== undefined) isFromAmount = true;
});

$('#exchange-picker').on('change', '#from,#to', function () {
    if ($('#from').val() === $('#to').val()) {
        if ($(this).attr('id') == "to") {
            $('#from').val(previousTo).trigger("change");
            updateTo();
        } else {
            $('#to').val(previousFrom).trigger("change");
            previousFrom = $('#from').val();
            updateFrom();
        }
        return;
    }
    if ($(this).attr('id') == "to") {
        updateTo();
    } else {
        updateFrom();
    }
    // Signs symbol
    getSigns();
    setSigns();
    // Step
    var selectedTo = $('#to option:selected');
    var selectedFrom = $('#from option:selected');
    $('#fromamount').attr('step', selectedFrom.data('step'));
    $('#toamount').attr('step', selectedTo.data('step'));
    $('#fromamount').attr('min', selectedFrom.data('min'));
    $('#toamount').attr('min', selectedTo.data('min'));
    getExchangeRate();
});

$('#exchange-picker').on('change', '#from,#to,#fromamount,#toamount', function (event) {
    if (event.originalEvent !== undefined) getExchangeRate();
});

var inputTimer = null;
$('#exchange-picker').on('keyup keypress paste', '#fromamount, #toamount', function () {
    if (inputTimer) clearTimeout(inputTimer);
    inputTimer = setTimeout(function () {
        getExchangeRate();
    }, 2000);
});

function goToUrl(url) {
    window.location.href = url;
}

function copyTextToClipboard(text) {
    var textArea = document.createElement("textarea");

    //
    // *** This styling is an extra step which is likely not required. ***
    //
    // Why is it here? To ensure:
    // 1. the element is able to have focus and selection.
    // 2. if element was to flash render it has minimal visual impact.
    // 3. less flakyness with selection and copying which **might** occur if
    //    the textarea element is not visible.
    //
    // The likelihood is the element won't even render, not even a flash,
    // so some of these are just precautions. However in IE the element
    // is visible whilst the popup box asking the user for permission for
    // the web page to copy to the clipboard.
    //

    // Place in top-left corner of screen regardless of scroll position.
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;

    // Ensure it has a small width and height. Setting to 1px / 1em
    // doesn't work as this gives a negative w/h on some browsers.
    textArea.style.width = '2em';
    textArea.style.height = '2em';

    // We don't need padding, reducing the size if it does flash render.
    textArea.style.padding = 0;

    // Clean up any borders.
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';

    // Avoid flash of white box if rendered for any reason.
    textArea.style.background = 'transparent';

    textArea.value = text;

    document.body.appendChild(textArea);

    textArea.select();

    var msg = "";
    try {
        var successful = document.execCommand('copy');
        msg = successful ? 'آدرس کپی شد' : 'کپی انجام نشد!';
    } catch (err) {
        msg = 'کپی انجام نشد!';
    }
    alert(msg);

    document.body.removeChild(textArea);
}

function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

function isImage(filename) {
    var ext = getExtension(filename).toLowerCase();
    switch (ext) {
        case 'jpg':
        case 'jpeg':
            return true;
    }
    return false;
}

//$(document).ready(function () {
//    var alertCookie = getCookie("extoman_alert");
//    if (alertCookie == "") {
//        alertDialog("خرید ارزهای دیجیتالی به جز پرفکت مانی به دلیل انجام به روز رسانی تا ساعت 14 بعداظهر امکان پذیر نیست.", function () {
//            setCookie("extoman_alert", "1");
//        });
//    }
//});

function alertDialog(question, callback) {
    var alertModal =
        $('<div class="modal fade" id="modal-alert-dialog" role="dialog" aria-hidden="true" aria-labelledby="Approve">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="col-12">' +
            '<h4>' + question + '</h4>' +
            '<a href="#" id="okButton" class="btn btn-lg btn-primary btn-embossed btn-block">متوجه شدم</a>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>');

    alertModal.on('hidden.bs.modal', function () {
        $(this).remove();
    });

    alertModal.find('#okButton').click(function (event) {
        event.preventDefault();
        callback();
        alertModal.modal('hide');
    });

    alertModal.modal('show');
};


function confirmDialog(question, callback) {
    var confirmModal =
        $('<div class="modal fade" id="modal-confirm-dialog" role="dialog" aria-hidden="true" aria-labelledby="Approve">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="col-12">' +
            '<h4>' + question + '</h4>' +
            '<a href="#" id="okButton" class="btn btn-lg btn-primary btn-embossed btn-block">بله مطمئنم</a>' +
            '<a href="#" class="btn btn-lg btn-danger btn-block" data-dismiss="modal">نه! مطمئن نیستم</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>');

    confirmModal.on('hidden.bs.modal', function () {
        $(this).remove();
    });

    confirmModal.find('#okButton').click(function (event) {
        event.preventDefault();
        callback();
        confirmModal.modal('hide');
    });

    confirmModal.modal('show');
};

function confirmWithInputDialog(question, inputlabelText, inputIdName, inputNullError, callback) {
    if (!inputIdName) {
        inputIdName = "customInput";
    }
    var confirmModal =
        $('<div class="modal fade" id="modal-confirm-with-input" role="dialog" aria-hidden="true" aria-labelledby="Confirm">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-body">' +
            '<div class="row">' +
            '<div class="col-12">' +
            '<h4>' + question + '</h4>' +
            '<div class="col-12">' +
            '<div class="form-group">' +
            '<input type="text" id="' + inputIdName + '" name="' + inputIdName + '" placeholder="' + inputlabelText + '" class="form-control"/>' +
            '</div>' +
            '</div>' +
            '<div class="col-12">' +
            '<a href="#" id="okButton" class="btn btn-lg btn-primary btn-embossed btn-block">ثبت</a>' +
            '<a href="#" class="btn btn-lg btn-danger btn-block" data-dismiss="modal">لغو</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>');

    confirmModal.on('hidden.bs.modal', function () {
        $(this).remove();
    });

    confirmModal.find('#okButton').click(function (event) {
        event.preventDefault();
        var theInput = confirmModal.find($('#customInput')).val();
        if (theInput === null || theInput === "") {
            alert(inputNullError);
            return;
        }
        callback(theInput);
        confirmModal.modal('hide');
    });
    confirmModal.modal('show');
};

function IsNationalCodeValid(input) {
    if (!/^\d{10}$/.test(input)
        || input === '0000000000'
        || input === '1111111111'
        || input === '2222222222'
        || input === '3333333333'
        || input === '4444444444'
        || input === '5555555555'
        || input === '6666666666'
        || input === '7777777777'
        || input === '8888888888'
        || input === '9999999999')
        return false;
    var check = parseInt(input[9]);
    var sum = 0;
    var i;
    for (i = 0; i < 9; ++i) {
        sum += parseInt(input[i]) * (10 - i);
    }
    sum %= 11;
    return (sum < 2 && check === sum) || (sum >= 2 && check + sum === 11);
}