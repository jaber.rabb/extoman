﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;

namespace Xtoman.Web.ViewModels
{
    public class AddSupportViewModel
    {
        [Required]
        [Display(Name = "ایمیل یا موبایل")]
        [RegularExpression(@"^(?=\d{11}$)(09)\d+|^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$", ErrorMessage = "ایمیل یا موبایل وارد شده اشتباه است.")]
        public string EmailOrPhoneNumber { get; set; }
        [Required]
        [Display(Name = "عنوان")]
        public string Subject { get; set; }
        [Required]
        [Display(Name = "دپارتمان")]
        public SupportDepartment Department { get; set; }
        [Required]
        [Display(Name = "متن")]
        public string Text { get; set; }
        [Display(Name = "اهمیت")]
        public Priority Priority { get; set; }
    }
}