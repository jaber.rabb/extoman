﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class SupportTicketViewModel : BaseViewModel<SupportTicketViewModel, SupportTicket>
    {
        [Display(Name = "موضوع")]
        public string Subject { get; set; }
        [Display(Name = "دپارتمان")]
        public SupportDepartment Department { get; set; }
        [Display(Name = "اهمیت")]
        public Priority Priority { get; set; }
        [Display(Name = "وضعیت تیکت")]
        public TicketStatus Status { get; set; }
        [Display(Name = "تاریخ ثبت")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "تاریخ آخرین ارسال")]
        public DateTime LastDate { get; set; }
        [Display(Name = "متن")]
        public string Text { get; set; }
        [Display(Name = "پاسخ ها")]
        public List<SupportTicketReplyViewModel> Replies { get; set; }
    }

    public class SupportTicketReplyViewModel
    {
        [Display(Name = "تاریخ ارسال")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "متن پاسخ")]
        public string Text { get; set; }
        [Display(Name = "پاسخ از طرف پشتیبانی")]
        public bool IsSupportReply { get; set; }
    }

    public class SupportTicketReplySubmitViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "متن پاسخ")]
        public string Text { get; set; }
    }
}