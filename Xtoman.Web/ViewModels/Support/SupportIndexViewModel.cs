﻿using System.Collections.Generic;

namespace Xtoman.Web.ViewModels
{
    public class SupportIndexViewModel
    {
        private List<SupportTicketItemViewModel> _tickets;
        public List<SupportTicketItemViewModel> Tickets
        {
            get { return _tickets ?? new List<SupportTicketItemViewModel>(); }
            set { _tickets = value; }
        }
        public AddSupportViewModel AddViewModel { get; set; }
    }
}