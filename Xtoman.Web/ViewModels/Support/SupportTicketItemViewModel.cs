﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class SupportTicketItemViewModel : BaseViewModel<SupportTicketItemViewModel, SupportTicket>
    {
        [Display(Name = "موضوع")]
        public string Subject { get; set; }
        [Display(Name = "دپارتمان")]
        public SupportDepartment Department { get; set; }
        [Display(Name = "اهمیت")]
        public Priority Priority { get; set; }
        [Display(Name = "وضعیت تیکت")]
        public TicketStatus Status { get; set; }
        [Display(Name = "تاریخ ثبت")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "تاریخ آخرین ارسال")]
        public DateTime LastDate { get; set; }
    }
}