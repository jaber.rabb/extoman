﻿using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class ExchangeTypeRateViewModel : BaseViewModel<ExchangeTypeRateViewModel, ExchangeTypeRate>
    {
        public new decimal Id { get; set; }
        public decimal Rate { get; set; }
    }
}