﻿namespace Xtoman.Web.ViewModels
{
    public class BalanceViewModel
    {
        //public ECurrencyType Type { get; set; }
        public string Name { get; set; }
        public decimal Available { get; set; }
        //public string ImgSrc { get; set; }
        public string UnitSign { get; set; }
    }
}