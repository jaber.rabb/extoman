﻿using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class FAQViewModel : BaseViewModel<FAQViewModel, FAQ>
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public string GroupName { get; set; }
        public string Url { get; set; }
    }
}