﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Utility;

namespace Xtoman.Web.ViewModels
{
    public class PanelViewModel
    {
        public UserViewModel User { get; set; }
        public int TotalIncomeFromReferrals { get; set; }
        public int TotalReferrals { get; set; }
        public DateTime LastOrderDate { get; set; }
        public ExchangeTypeSimpleViewModel LastOrderExchangeType { get; set; }
        public int TotalOrdersCount { get; set; }

        [Display(Name = "احراز هویت")]
        public VerificationModelStatus VerifyStatus
        {
            get
            {
                if (User.IdentityVerificationStatus == VerificationStatus.Confirmed)
                {
                    if (User.EmailConfirmed)
                    {
                        if (User.PhoneNumberConfirmed)
                        {
                            if (User.TelephoneConfirmationStatus == VerificationStatus.Confirmed)
                            {
                                return VerificationModelStatus.Confirmed;
                            }
                            else
                            {
                                switch (User.TelephoneConfirmationStatus)
                                {
                                    case VerificationStatus.NotSent:
                                        return VerificationModelStatus.TelephoneNotEntered;
                                    case VerificationStatus.Pending:
                                        return VerificationModelStatus.TelephonePending;
                                    case VerificationStatus.NotConfirmed:
                                        return VerificationModelStatus.TelephoneNotConfirmed;
                                    default:
                                        return VerificationModelStatus.TelephoneConfirmed;
                                }
                            }
                        }
                        else
                        {
                            return User.PhoneNumber.HasValue() ? VerificationModelStatus.PhoneNumberNotConfirmed : VerificationModelStatus.PhoneNumberNotEntered;
                        }
                    }
                    else
                    {
                        return User.Email.HasValue() ? VerificationModelStatus.EmailNotConfirmed : VerificationModelStatus.EmailNotEntered;
                    }
                }
                else
                {
                    switch (User.IdentityVerificationStatus)
                    {
                        case VerificationStatus.NotSent:
                            return VerificationModelStatus.IdentityNotSent;
                        case VerificationStatus.Pending:
                            return VerificationModelStatus.IdentityPending;
                        default:
                        case VerificationStatus.Confirmed:
                            return VerificationModelStatus.IdentityConfirmed;
                        case VerificationStatus.NotConfirmed:
                            return VerificationModelStatus.IdentityNotConfirmed;
                    }
                }
            }
        }

    }
}