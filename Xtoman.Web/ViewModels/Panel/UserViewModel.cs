﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class UserViewModel : BaseViewModel<AccountIndexViewModel, AppUser>
    {
        [Required]
        [Display(Name = "نام")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "ایمیل")]
        [RegularExpression(@"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$", ErrorMessage = "ایمیل وارد شده اشتباه است")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "تلفن همراه")]
        [RegularExpression(@"^(?=\d{11}$)(09)\d+", ErrorMessage = "تلفن همراه وارد شده اشتباه است")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "تلفن ثابت")]
        public string Telephone { get; set; }
        [Display(Name = "تاریخ تولد")]
        public DateTime? BirthDate { get; set; }
        [Display(Name = "جنسیت")]
        public bool? Gender { get; set; } // false 0 = male | true 1 = female
        [Display(Name = "نام کامل")]
        public string FullName => FirstName + " " + LastName;

        [Display(Name = "کد ملی")]
        [NationalCode]
        public string NationalCode { get; set; }
        //public List<AccountExchangeOrderViewModel> UserExchanges { get; set; }

        [Display(Name = "نام پدر")]
        public string FatherName { get; set; }

        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }

        [Display(Name = "وضعیت احراز مدارک")]
        public VerificationStatus IdentityVerificationStatus { get; set; }
        [Display(Name = "احراز تلفن ثابت محل سکونت")]
        public VerificationStatus TelephoneConfirmationStatus { get; set; }
        [Display(Name = "تایید تلفن همراه")]
        public bool PhoneNumberConfirmed { get; set; }
        [Display(Name = "تایید ایمیل")]
        public bool EmailConfirmed { get; set; }

        [Display(Name = "حساب محدود است")]
        public bool IsBuyLimited { get; set; }
    }
}