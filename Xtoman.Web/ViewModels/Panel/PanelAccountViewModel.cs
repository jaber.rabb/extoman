﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class PanelAccountViewModel : BaseViewModel<PanelAccountViewModel, AppUser>
    {
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }
        [Display(Name = "شماره موبایل")]
        public string PhoneNumber { get; set; }
        [Display(Name = "ایمیل")]
        public string Email { get; set; }

        public bool UserNameIsSet
        {
            get
            {
                return UserName != Email && UserName != PhoneNumber;
            }
        }
    }
}