﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Utility;

namespace Xtoman.Web.ViewModels
{
    public class PanelVerifyDetailViewModel : BaseViewModel<PanelVerifyDetailViewModel, AppUser>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string NationalCode { get; set; }
        public string FatherName { get; set; }

        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }

        public VerificationStatus IdentityVerificationStatus { get; set; }
        public VerificationStatus TelephoneConfirmationStatus { get; set; }

        public List<UserBankCardViewModel> UserBankCards { get; set; }

        public bool InformationCompleted
        {
            get
            {
                return FirstName.HasValue() && LastName.HasValue() && NationalCode.HasValue() && FatherName.HasValue() && BirthDate.HasValue;
            }
        }
        public bool IdentityCompleted
        {
            get
            {
                return IdentityVerificationStatus == VerificationStatus.Confirmed;
            }
        }
        public bool BankCardCompleted
        {
            get
            {
                return UserBankCards.Any(x => x.VerificationStatus == VerificationStatus.Confirmed);
            }
        }
        public bool LocationCompleted
        {
            get
            {
                return TelephoneConfirmationStatus == VerificationStatus.Confirmed;
            }
        }
        public bool EmailPhoneConfirmed
        {
            get
            {
                return PhoneNumberConfirmed && EmailConfirmed;
            }
        }
    }
}