﻿using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class MediaThumbnailViewModel : BaseViewModel<MediaThumbnailViewModel, MediaThumbnail>
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public string Url { get; set; }
        public string MediaThumbnailSizeName { get; set; }
    }
}