﻿using System.Collections.Generic;

namespace Xtoman.Web.ViewModels
{
    public class HomeViewModel : ExchangeCurrencyTypeViewModel
    {
        public int OrdersCountPast24 { get; set; }
        public int NewUsersCountPast24 { get; set; }
        public List<ReviewViewModel> Reviews { get; set; }
        public List<ShortPostViewModel> LatestPosts { get; set; }
        public ExchangeTypeViewModel TopExchangePast24 { get; set; }
    }

    public class NewHomeViewModel
    {
        public List<CurrencyTypeViewModel> Currencies { get; set; }
        public List<ShortPostViewModel> LatestPosts { get; set; }
    }
}