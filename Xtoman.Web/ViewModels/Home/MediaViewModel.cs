﻿using System.Collections.Generic;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;
using Xtoman.Utility;

namespace Xtoman.Web.ViewModels
{
    public class MediaViewModel : BaseViewModel<MediaViewModel, Media>
    {
        public string Url { get; set; }
        public string Alt { get; set; }
        public MediaType Type { get; set; }
        public List<MediaThumbnailViewModel> Thumbnails { get; set; }
    }
}