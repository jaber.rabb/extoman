﻿using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class OrderUserViewModel : BaseViewModel<OrderUserViewModel, AppUser>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool? Gender { get; set; } // false 0 = male | true 1 = female
    }
}