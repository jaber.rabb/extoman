﻿using System;
using System.Collections.Generic;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class BlogPostItemViewModel : BaseViewModel<BlogPostItemViewModel, Post>, ILocalizedEntity
    {
        public PostType PostType { get; set; }
        public int ApprovedCommentsCount { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public DateTime? PublishDate { get; set; }
        public string Url { get; set; }
        public int VisitCount { get; set; }
        public string TagNames { get; set; }
        public string ImageSrc { get; set; }

        public Media Media { get; set; }
        public List<PostCategoryViewModel> Categories { get; set; }
    }
}