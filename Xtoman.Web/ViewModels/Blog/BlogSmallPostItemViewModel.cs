﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class BlogSmallPostItemViewModel : BaseViewModel<BlogPostItemViewModel, Post>, ILocalizedEntity
    {
        public int ApprovedCommentsCount { get; set; }
        public string Title { get; set; }
        public DateTime? PublishDate { get; set; }
        public string Url { get; set; }
        public int VisitCount { get; set; }
        public string ImageSrc { get; set; }

        public Media Media { get; set; }
        public List<PostCategory> Categories { get; set; }
    }
}