﻿using System;
using System.Collections.Generic;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class BlogSinglePostViewModel : BaseViewModel<BlogSinglePostViewModel, Post>
    {
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public PostType PostType { get; set; }
        public int ApprovedCommentsCount { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public DateTime? PublishDate { get; set; }
        public string Url { get; set; }
        public int VisitCount { get; set; }
        public string TagNames { get; set; }
        public string Text { get; set; }
        public string ImageSrc { get; set; }

        public MediaViewModel Media { get; set; }
        public List<PostCategoryViewModel> Categories { get; set; }
        public List<PostTagViewModel> Tags { get; set; }
        //public List<PostCommentViewModel> Comments { get; set; }
    }

    public class PostTagViewModel : BaseViewModel<PostTagViewModel, PostTag>
    {
        public bool IsPin { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
    }
}