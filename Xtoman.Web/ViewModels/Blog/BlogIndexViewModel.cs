﻿using PagedList;
using System.Collections.Generic;

namespace Xtoman.Web.ViewModels
{
    public class BlogIndexViewModel : BlogSidebarViewModel
    {
        public int Total { get; set; }
        public IPagedList<BlogPostItemViewModel> Posts { get; set; }
    }
}