﻿namespace Xtoman.Web.ViewModels
{
    public class BlogSingleViewModel : BlogSidebarViewModel
    {
        public BlogSinglePostViewModel Post { get; set; }
    }
}