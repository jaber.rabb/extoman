﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xtoman.Web.ViewModels
{
    public class BlogSidebarViewModel
    {
        public BlogSidebarViewModel()
        {
            Sidebar = new BlogSidebarItemsViewModel();
        }
        public BlogSidebarItemsViewModel Sidebar { get; set; }
    }

    public class BlogSidebarItemsViewModel
    {
        public BlogSidebarItemsViewModel()
        {
            PopularPosts = new List<BlogSmallPostItemViewModel>();
            LatestPosts = new List<BlogSmallPostItemViewModel>();
            Categories = new List<BlogCategoryItemViewModel>();
        }
        public List<BlogSmallPostItemViewModel> PopularPosts { get; set; }
        public List<BlogSmallPostItemViewModel> LatestPosts { get; set; }
        public List<BlogCategoryItemViewModel> Categories { get; set; }
        public List<BlogTagItemViewModel> Tags { get; set; }
    }
}