﻿using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class BlogTagItemViewModel : BaseViewModel<BlogTagItemViewModel, PostTag>
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsPin { get; set; }
    }
}