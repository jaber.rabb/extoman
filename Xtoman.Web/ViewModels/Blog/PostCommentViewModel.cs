﻿using System;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class PostCommentViewModel : BaseViewModel<PostCommentViewModel, PostComment>
    {
        public string Text { get; set; }
        public byte Rating { get; set; }
        public int? ConfirmUserId { get; set; }
        public bool? IsConfirm { get; set; }
        public DateTime? ConfirmDate { get; set; }
        public bool IsAutenticated { get; set; }
        public string AnonymousID { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }

        public int? ParentCommentId { get; set; }
        public PostCommentViewModel Parent { get; set; }
    }
}