﻿using Xtoman.Domain.Models;

namespace Xtoman.Web.ViewModels
{
    public class VerifyViewModel
    {
        public bool InformationCompleted { get; set; }
        public bool EmailConfirmCompleted { get; set; }
        public bool PhoneNumberConfirmCompleted { get; set; }
        public bool? TelephoneConfirmCompleted { get; set; }
        public bool IdentityCompleted { get; set; }
        public bool BankCardCompleted { get; set; }
        public string IdentityText { get; set; }
        public string BankCardText { get; set; }
        public VerificationStatus IdentityStatus { get; set; }
    }
}