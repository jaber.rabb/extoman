﻿using System.Collections.Generic;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class AccountNotifyAlertsViewModel : BaseViewModel<AccountNotifyAlertsViewModel, AppUser>
    {
        public List<UserNotifyAlertViewModel> UserNotifications { get; set; }
    }
}