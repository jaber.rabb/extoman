﻿using System.Collections.Generic;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class AccountExchangesViewModel : BaseViewModel<AccountExchangesViewModel, AppUser>
    {
        public List<ExchangeOrderSimpleViewModel> ExchangeOrders { get; set; }
    }
}