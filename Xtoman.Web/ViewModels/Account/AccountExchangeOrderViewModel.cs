﻿using System;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class AccountExchangeOrderViewModel : BaseViewModel<AccountExchangeOrderViewModel, ExchangeOrder>
    {
        public decimal ReceiveAmount { get; set; }
        public string ReceiveECAccountNo { get; set; }
        public string ReceiveTransactionCode { get; set; }
        public DateTime? ReceiveDate { get; set; }
        public string ReceivePayerAccountNo { get; set; }
        public ExchangeTypeViewModel Exchange { get; set; }
    }
}