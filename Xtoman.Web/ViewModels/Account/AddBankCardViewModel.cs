﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Web.ViewModels
{
    public class AddBankCardViewModel
    {
        [Required]
        [Display(Name = "شماره کارت")]
        [RegularExpression("([0-9]{16}?)+")]
        public string CardNumber { get; set; }
    }
}