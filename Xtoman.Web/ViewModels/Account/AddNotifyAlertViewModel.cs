﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class AddNotifyAlertViewModel : BaseViewModel<AddNotifyAlertViewModel, UserNotifyAlert>
    {
        [Display(Name = "نوع اعلان")]
        public NotifyAlertType Type { get; set; }
        [Display(Name = "مقدار")]
        public decimal Value { get; set; }
        public int? ExchangeTypeId { get; set; }
        public int? BaseCurrencyId { get; set; }
    }
}