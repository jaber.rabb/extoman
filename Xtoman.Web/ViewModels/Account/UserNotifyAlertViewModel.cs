﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class UserNotifyAlertViewModel : BaseViewModel<UserNotifyAlertViewModel, UserNotifyAlert>
    {
        [Display(Name = "نوع اعلان")]
        public NotifyAlertType Type { get; set; }
        [Display(Name = "مقدار")]
        public decimal Value { get; set; }
        [Display(Name = "تاریخ افزودن اعلان")]
        public DateTime InsertDate { get; set; }
        [Display(Name = "تاریخ اعلان شده")]
        public DateTime? AlertedDate { get; set; }
        public bool SendViaEmail { get; set; }
        public bool SendViaSMS { get; set; }
        public bool SendViaTelegram { get; set; }
        public CurrencyTypeViewModel BaseCurrency { get; set; }
        public ExchangeTypeViewModel ExchangeType { get; set; }
    }
}