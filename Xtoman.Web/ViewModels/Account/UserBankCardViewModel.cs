﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class UserBankCardViewModel : BaseViewModel<UserBankCardViewModel, UserBankCard>
    {
        [Display(Name = "شماره کارت")]
        public string CardNumber { get; set; }
        [Display(Name = "وضعیت")]
        public VerificationStatus VerificationStatus { get; set; }
        [Display(Name = "تاریخ بررسی")]
        public DateTime? VerifyDate { get; set; }
        [Display(Name = "توضیحات")]
        public string VerifyDescription { get; set; }
        [Display(Name = "تاریخ اضافه شدن")]
        public DateTime InsertDate { get; set; }
    }

    public class UserBankAccountViewModel : BaseViewModel<UserBankAccountViewModel, UserBankCard>
    {
        [Display(Name = "شماره کارت")]
        public string CardNumber { get; set; }
        [Display(Name = "نام بانک")]
        public string BankName { get; set; }
        [Display(Name = "شماره شبا")]
        public string ShebaNumber { get; set; }
        [Display(Name = "شماره حساب")]
        public string BankAccountNumber { get; set; }
    }
}