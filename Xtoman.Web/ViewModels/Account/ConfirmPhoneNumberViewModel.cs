﻿using System.ComponentModel.DataAnnotations;

namespace Xtoman.Web.ViewModels
{
    public class ConfirmPhoneNumberViewModel
    {
        public int? UserId { get; set; }
        [RegularExpression("^([0-9]*|null)$")]
        public string PhoneNumber { get; set; }
        [RegularExpression("^([0-9]*|null)$")]
        [Display(Name = "کد پیامک")]
        public string Code { get; set; }
    }
}