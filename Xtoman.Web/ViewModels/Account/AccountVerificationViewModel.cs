﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class AccountVerificationViewModel : BaseViewModel<AccountVerificationViewModel, AppUser>
    {
        [Display(Name = "نام")]
        public string FirstName { get; set; }
        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }
        [Display(Name = "تاریخ تایید هویت")]
        public DateTime? IdentityVerificationVerifyDate { get; set; }
        [Display(Name = "وضعیت احراز هویت")]
        public VerificationStatus IdentityVerificationStatus { get; set; }
        [Display(Name = "توضیحات")]
        public string IdentityVerificationDescription { get; set; }
        public MediaViewModel VerificationMedia { get; set; }
        public string FullName => FirstName + " " + LastName;
    }
}