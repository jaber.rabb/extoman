﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Xtoman.Framework.Mvc.Attributes;

namespace Xtoman.Web.ViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "ایمیل")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        [Display(Name = "ارسال کننده کد")]
        public string SelectedProvider { get; set; }
        public ICollection<SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "کد")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "به خاطر سپاری این مرورگر؟")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "ایمیل یا موبایل")]
        [RegularExpression(@"^(?=\d{11}$)(09)\d+|^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$")]
        public string EmailOrPhone { get; set; }
    }

    //public class LoginViewModel
    //{
    //    [Required]
    //    [Display(Name = "ایمیل یا موبایل")]
    //    [RegularExpression(@"^(?=\d{11}$)(09)\d+|^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$", ErrorMessage = "ایمیل یا تلفن همراه وارد شده اشتباه است")]
    //    public string EmailOrPhone { get; set; }

    //    [Required]
    //    [DataType(DataType.Password)]
    //    [Display(Name = "کلمه عبور")]
    //    public string Password { get; set; }

    //    [Display(Name = "مرا به خاطر بسپار")]
    //    public bool RememberMe { get; set; }
    //}

    //public class RegisterViewModel
    //{
    //    [Required]
    //    [Display(Name = "ایمیل یا موبایل")]
    //    [Remote("EmailOrPhoneIsUnique", "Account", ErrorMessage = "این ایمیل یا موبایل قبلا ثبت شده.", HttpMethod = "POST")]
    //    [RegularExpression(@"^(?=\d{11}$)(09)\d+|^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$", ErrorMessage = "ایمیل یا تلفن همراه وارد شده اشتباه است")]
    //    public string EmailOrPhone { get; set; }

    //    [Required]
    //    [StringLength(100, ErrorMessage = "کلمه عبور باید حداقل {2} کاراکتر باشد.", MinimumLength = 6)]
    //    [DataType(DataType.Password)]
    //    [Display(Name = "کلمه عبور")]
    //    public string Password { get; set; }

    //    [DataType(DataType.Password)]
    //    [Display(Name = "تکرار کلمه عبور")]
    //    [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "تکرار کلمه عبور با کلمه عبور وارد شده همخوانی ندارد. ")]
    //    public string ConfirmPassword { get; set; }

    //    [EnforceTrue(ErrorMessage = "قوانین و مقررات باید پذیرفته شود.")]
    //    public bool Terms { get; set; }
    //}

    public class ResetPasswordViewModel
    {
        [Required]

        [Display(Name = "ایمیل یا موبایل")]
        public string EmailOrPhone { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "کلمه عبور باید حداقل {2} کاراکتر باشد.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "کلمه عبور")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "تکرار کلمه عبور")]
        [System.ComponentModel.DataAnnotations.CompareAttribute("Password", ErrorMessage = "تکرار کلمه عبور با کلمه عبور وارد شده همخوانی ندارد. ")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "کد بازنویسی")]
        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]

        [Display(Name = "ایمیل یا موبایل")]
        public string EmailOrPhone { get; set; }
    }
}
