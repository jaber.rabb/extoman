﻿namespace Xtoman.Web.ViewModels
{
    public class ApiServerErrorViewModel
    {
        public string Title { get; set; }
        public string Text { get; set; }
    }
}