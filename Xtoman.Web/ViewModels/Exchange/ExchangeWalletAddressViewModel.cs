﻿using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class ExchangeWalletAddressViewModel : BaseViewModel<ExchangeWalletAddressViewModel, ExchangeType>
    {
        public bool Disabled { get; set; }
        public CurrencyTypeViewModel FromCurrency { get; set; }
        public ExchangeWalletAddressCurrencyTypeViewModel ToCurrency { get; set; }
        public ExchangeFiatCoinType ExchangeFiatCoinType { get; set; }
    }

    public class ExchangeWalletAddressCurrencyTypeViewModel : BaseViewModel<ExchangeWalletAddressViewModel, CurrencyType>
    {
        public bool Disabled { get; set; }
        public string Url { get; set; }
        public bool IsFiat { get; set; }
        public string AddressRegEx { get; set; }
        public ECurrencyType Type { get; set; }
        public string Name { get; set; }
        public string UnitSign { get; set; }
        public string AlternativeName { get; set; }
        public string Description { get; set; }
        public decimal? TransactionFee { get; set; }

        public bool HasMemo { get; set; }
        public string MemoName { get; set; }
        public string MemoRegEx { get; set; }
        public string MemoDescription { get; set; }
    }
}