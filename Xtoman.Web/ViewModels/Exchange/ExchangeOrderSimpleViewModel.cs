﻿using System;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class ExchangeOrderSimpleViewModel : BaseViewModel<ExchangeOrderSimpleViewModel, ExchangeOrder>
    {
        public new long Id { get; set; }
        public Guid Guid { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal PayAmount { get; set; }
        public decimal PriceAmount { get; set; }
        public decimal GatewayFee { get; set; }

        #region Pay
        public PaymentType PaymentType { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public string PayAddress { get; set; }
        public string PayMemoTagPaymentId { get; set; }
        public string PayTransactionId { get; set; }
        public string PayPaymentGatewayId { get; set; }
        public string PayQRUrl { get; set; }
        public int PayConfirmsNeed { get; set; }
        public string PayStatusUrl { get; set; }
        public WalletApiType PayWalletApiType { get; set; }
        public DateTime? PayTimeEnd { get; set; }
        #endregion

        #region Receive
        public decimal ReceiveAmount { get; set; }
        public string ReceiveAddress { get; set; }
        public string ReceiveMemoTagPaymentId { get; set; }
        public string ReceiveTransactionCode { get; set; }
        public DateTime? ReceiveDate { get; set; }
        public string ReceivePayerAccountNo { get; set; }
        #endregion

        public ExchangeTypeViewModel Exchange { get; set; }
    }
}