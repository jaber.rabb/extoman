﻿namespace Xtoman.Web.ViewModels
{
    public class AmountWrongViewModel
    {
        public decimal ToAmount { get; set; }
        public decimal FromAmount { get; set; }
        public string FromCurrencyUnit { get; set; }
        public string FromCurrencyName { get; set; }
        public decimal FromCurrencyMin { get; set; }
        public decimal FromCurrencyMax { get; set; }
        public string ToCurrencyName { get; set; }
        public string ToCurrencyUnit { get; set; }
        public decimal ToCurrencyMin { get; set; }
        public decimal ToCurrencyMax { get; set; }
    }
}