﻿using System.Collections.Generic;

namespace Xtoman.Web.ViewModels
{
    public class ExchangeCurrencyTypeViewModel
    {
        public List<ExchangeTypeViewModel> ExchangeTypes { get; set; }
        public List<CurrencyTypeViewModel> FromCurrencyTypes { get; set; }
        public List<CurrencyTypeViewModel> ToCurrencyTypes { get; set; }
    }
}