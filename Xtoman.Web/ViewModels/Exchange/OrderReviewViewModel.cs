﻿using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class OrderReviewViewModel : BaseViewModel<OrderReviewViewModel, OrderReview>
    {
        public new long Id { get; set; }
        [Display(Name = "متن نظر")]
        public string Text { get; set; }
        [Display(Name = "امتیاز")]
        public byte VoteRate { get; set; }
        [Display(Name = "وضعیت نمایش")]
        public Approvement Visibility { get; set; }
    }
}