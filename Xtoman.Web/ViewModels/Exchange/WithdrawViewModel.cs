﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class WithdrawViewModel : BaseViewModel<WithdrawViewModel, Withdraw>
    {
        public new long Id { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public ECurrencyType ECurrencyType { get; set; }
        [Display(Name = "وضعیت انتقال")]
        public WithdrawStatus Status { get; set; }
        public DateTime InsertDate { get; set; }
        [Display(Name = "شماره تراکنش انتقال")]
        public string TransactionId { get; set; }
    }
}