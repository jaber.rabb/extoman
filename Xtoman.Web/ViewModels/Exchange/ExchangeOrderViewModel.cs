﻿using System;
using System.Collections.Generic;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class ExchangeOrderViewModel : BaseViewModel<ExchangeOrderViewModel, ExchangeOrder>
    {
        public new long Id { get; set; }
        public Guid Guid { get; set; }
        public PaymentType PaymentType { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public DateTime? CancelDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string Description { get; set; }

        public decimal PayAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal PriceAmount { get; set; }
        public decimal GatewayFee { get; set; }
        public ECurrencyType PaymentECurrencyType { get; set; }

        public int UserId { get; set; }

        public OrderUserViewModel User { get; set; }
        public string PayAddress { get; set; }
        public string PayMemoTagPaymentId { get; set; }
        public string PayTransactionId { get; set; }
        public string PayPaymentGatewayId { get; set; }
        public int PayConfirmsNeed { get; set; }
        public string PayStatusUrl { get; set; }
        public WalletApiType PayWalletApiType { get; set; }
        public DateTime? PayTimeEnd { get; set; }
        public decimal ReceiveAmount { get; set; }
        public string ReceiveAddress { get; set; }
        public string ReceiveMemoTagPaymentId { get; set; }
        public string ReceiveTransactionCode { get; set; }
        public DateTime? ReceiveDate { get; set; }
        public string ReceivePayerAccountNo { get; set; }
        public ExchangeTypeViewModel Exchange { get; set; }
        public OrderReviewViewModel Review { get; set; }
        public List<WithdrawViewModel> OrderWithdraws { get; set; }

        // Manual
        public string QRString { get; set; }
        public string MemoQRString { get; set; }
    }
}