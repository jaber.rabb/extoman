﻿using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class ExchangeConfirmViewModel : BaseViewModel<ExchangeConfirmViewModel, ExchangeType>
    {
        public int FromCurrencyId { get; set; }
        public ECurrencyType FromCurrencyType { get; set; }
        public int ToCurrencyId { get; set; }
        public string FromCurrencyName { get; set; }
        public string ToCurrencyName { get; set; }
        public ECurrencyType ToCurrencyType { get; set; }
        public string FromCurrencyUnitSign { get; set; }
        public string ToCurrencyUnitSign { get; set; }
        public string FromCurrencyUrl { get; set; }
        public string ToCurrencyUrl { get; set; }
        public string ToCurrencyMemoName { get; set; }
        public decimal? ToCurrencyTransactionFee { get; set; }
        public bool ToCurrencyIsFiat { get; set; }
        public bool IsFrom { get; set; }
        public decimal FromAmount { get; set; }
        public string WalletAddress { get; set; }
        public string Memo { get; set; }
        public decimal ToAmount { get; set; }

        public string CardNumber { get; set; }
        public string ShebaNumber { get; set; }
        public string BankName { get; set; }
    }
}