﻿using System.Collections.Generic;

namespace Xtoman.Web.ViewModels
{
    public class ExchangeIndexViewModel : ExchangeCurrencyTypeViewModel
    {
        public int? FromId { get; set; }
        public int? ToId { get; set; }
        public bool IsFromAmount { get; set; }
        public decimal? Amount { get; set; }
        public decimal WithdrawFee { get; set; }

        //Set manually
        public bool IsBuyLimited { get; set; }
        public decimal FromMaxAmount { get; set; }

        public List<ReviewViewModel> Reviews { get; set; }

        public decimal SchemaRatingValue { get; set; }
        public int SchemaRatingCount { get; set; }
        public int SchemaReviewCount { get; set; }
    }
}