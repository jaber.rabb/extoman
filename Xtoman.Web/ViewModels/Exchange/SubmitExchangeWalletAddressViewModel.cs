﻿namespace Xtoman.Web.ViewModels
{
    public class SubmitExchangeWalletAddressViewModel
    {
        public string Walletaddress { get; set; }
        public string Memo { get; set; }
        public string CardNumber { get; set; }
        public string ShebaNumber { get; set; }
        public string BankName { get; set; }
    }
}