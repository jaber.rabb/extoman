﻿namespace Xtoman.Web.ViewModels
{
    public class SubmitExchangeViewModel
    {
        public int from { get; set; }
        public int to { get; set; }
        public decimal fromamount { get; set; }
        public decimal toamount { get; set; }
        public int isfromamount { get; set; }
    }
}