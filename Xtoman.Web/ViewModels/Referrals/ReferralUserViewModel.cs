﻿using System;
using System.ComponentModel.DataAnnotations;
using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class ReferralUserViewModel : BaseViewModel<ReferralUserViewModel, AppUser>
    {
        [Display(Name = "نام")]
        public string FirstName { get; set; }
        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }
        [Display(Name = "ایمیل")]
        public string Email { get; set; }
        [Display(Name = "موبایل")]
        public string PhoneNumber { get; set; }
        [Display(Name = "تاریخ ثبت نام")]
        public DateTime RegDate { get; set; }
        [Display(Name = "سود حاصل")]
        public int Profit { get; set; }
    }
}