﻿using System.Collections.Generic;

namespace Xtoman.Web.ViewModels
{
    public class DiscountsViewModel
    {
        public List<DiscountViewModel> Discounts { get; set; }
        public int UserDiscountId { get; set; }
    }
}