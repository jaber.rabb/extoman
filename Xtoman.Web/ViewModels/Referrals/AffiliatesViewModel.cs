﻿using System.Collections.Generic;

namespace Xtoman.Web.ViewModels
{
    public class AffiliatesViewModel
    {
        public string UserReferralId { get; set; }
        public List<CommissionViewModel> Commissions { get; set; }
        public List<ReferralUserViewModel> Referrals { get; set; }
    }
}