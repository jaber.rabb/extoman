﻿using Xtoman.Domain.Models;
using Xtoman.Framework.Mvc;

namespace Xtoman.Web.ViewModels
{
    public class DiscountViewModel : BaseViewModel<DiscountViewModel, Discount>
    {
        public string Name { get; set; }
        public decimal MinValue { get; set; }
        public decimal Percent { get; set; }
    }
}