﻿using System.Collections.Generic;
using Xtoman.Domain.WebServices.TradingPlatform.Bitfinex;

namespace Xtoman.Web.ViewModels
{
    public class BitfinexTestViewModel
    {
        public List<BitfinexBalance> Balances { get; set; }
        public BitfinexTicker Ticker { get; set; }
        public List<BitfinexStatsItem> Stats { get; set; }
        public BitfinexFundingbook Fundingbook { get; set; }
        public BitfinexOrderbook Orderbook { get; set; }
        public List<BitfinexTrade> Trades { get; set; }
        public List<BitfinexLend> Lends { get; set; }
        public List<string> Symbols { get; set; }
        public List<BitfinexSymbolDetail> SymbolsDetails { get; set; }
    }
}