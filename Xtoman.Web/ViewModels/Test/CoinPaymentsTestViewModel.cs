﻿using Xtoman.Domain.WebServices.Payment.CoinPayments;

namespace Xtoman.Web.ViewModels
{
    public class CoinPaymentsTestViewModel
    {
        public CoinPaymentsBasicInfoResult BasicInfo { get; set; }
        public CoinPaymentsAddressResult Callback { get; set; }
        public CoinPaymentsAddressResult Deposit { get; set; }
        public CoinPaymentsCreateTransactionResult CreateTransaction { get; set; }
        public string QR { get; set; }

    }
}