﻿namespace Xtoman.Web.ViewModels
{
    public class CreateTransactionTestViewModel
    {
        public decimal Amount { get; set; }
        public string Address { get; set; }
        public string QRCode { get; set; }
    }
}