﻿using System.Web.Mvc;

public static class TagStyleHelpers
{
    public static string GetNumberStyle(this HtmlHelper htmlHelper, decimal? number)
    {
        var style = "";
        if (number.HasValue && number.Value != 0)
            style = "style=color:#" + (number > 0 ? "00a000" : "ff2f2f");

        return style;
    }
}