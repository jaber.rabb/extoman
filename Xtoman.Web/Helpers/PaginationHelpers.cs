﻿using PagedList.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;

namespace System.Web.Mvc
{
    public static class PaginationHelpers
    {
        public static PagedListRenderOptions GetPagedListRenderOptions(this HtmlHelper htmlHelper)
        {
            return new PagedListRenderOptions()
            {
                UlElementClasses = new string[] { "pagination" },
                LiElementClasses = new string[] { "paginate_button", "page-item" },
                ContainerDivClasses = new string[] { "dataTables_paginate", "paging_simple_numbers" },
                Display = PagedListDisplayMode.IfNeeded,
                DisplayLinkToFirstPage = PagedListDisplayMode.Never,
                DisplayLinkToIndividualPages = true,
                DisplayLinkToLastPage = PagedListDisplayMode.Always,
                DisplayLinkToNextPage = PagedListDisplayMode.Always,
                DisplayLinkToPreviousPage = PagedListDisplayMode.Always,
                DisplayEllipsesWhenNotShowingAllPageNumbers = false,
                LinkToPreviousPageFormat = "<i class='fa fa-angle-right'></i>",
                LinkToNextPageFormat = "<i class='fa fa-angle-left'></i>",
                LinkToLastPageFormat = "<i class='fa fa-angle-double-left'></i>",
                MaximumPageNumbersToDisplay = 5,
                FunctionToTransformEachPageLink = (liTag, aTag) => {
                    aTag.Attributes.Add("class", "page-link");
                    liTag.InnerHtml = aTag.ToString();
                    return liTag;
                }
            };
        }

        #region MergeAttributes
        private static object GetValue(IGrouping<string, KeyValuePair<string, object>> source, bool appendCssClass)
        {
            if (appendCssClass)
                return source.Key == "class" ? string.Join(" ", source.Select(p => p.Value)) : source.First().Value;
            return source.First().Value;
        }

        public static Dictionary<string, object> MergeAttributes(this HtmlHelper htmlHelper, object primaryAttributes, Dictionary<string, object> secondaryAttributes, bool appendCssClass = true) //not replace css class
        {
            if (primaryAttributes is Dictionary<string, object> primary)
                return MergeAttributes(primary, secondaryAttributes, appendCssClass);

            return new RouteValueDictionary(primaryAttributes).Concat(secondaryAttributes).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => GetValue(d, appendCssClass));
        }

        public static Dictionary<string, object> MergeAttributes(this HtmlHelper htmlHelper, object primaryAttributes, object secondaryAttributes, bool appendCssClass = true) //not replace css class
        {
            var primary = primaryAttributes as Dictionary<string, object>;
            var secondary = secondaryAttributes as Dictionary<string, object>;
            if (primary != null && secondary != null)
                return MergeAttributes(primary, secondary, appendCssClass);
            else if (primary != null)
                return MergeAttributes(primary, secondaryAttributes, appendCssClass);
            else if (secondary != null)
                return MergeAttributes(htmlHelper, primaryAttributes, secondary, appendCssClass);

            return new RouteValueDictionary(primaryAttributes).Concat(new RouteValueDictionary(secondaryAttributes)).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => GetValue(d, appendCssClass));
        }

        public static Dictionary<string, object> MergeAttributes(this Dictionary<string, object> primaryAttributes, Dictionary<string, object> secondaryAttributes, bool appendCssClass = true) //not replace css class
        {
            return primaryAttributes.Concat(secondaryAttributes).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => GetValue(d, appendCssClass));
        }

        public static Dictionary<string, object> MergeAttributes(this Dictionary<string, object> primaryAttributes, object secondaryAttributes, bool appendCssClass = true) //not replace css class
        {
            if (secondaryAttributes is Dictionary<string, object> secondary)
                return MergeAttributes(primaryAttributes, secondary, appendCssClass);

            return primaryAttributes.Concat(new RouteValueDictionary(secondaryAttributes)).GroupBy(d => d.Key).ToDictionary(d => d.Key, d => GetValue(d, appendCssClass));

            //var input = new TagBuilder("input");
            //var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            //input.MergeAttributes(attributes);
        }
        #endregion
    }
}