﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Service;
using Xtoman.Web.ViewModels;

namespace Xtoman.Web.Controllers
{
    public class AffiliatesController : Controller
    {
        private readonly ICommissionService _commissionService;
        private readonly IUserIncomeService _userIncomeService;
        private readonly IUserService _userService;
        public AffiliatesController(
            ICommissionService commissionService,
            IUserIncomeService userIncomeService,
            IUserService userService)
        {
            _commissionService = commissionService;
            _userIncomeService = userIncomeService;
            _userService = userService;
        }
        public async Task<ActionResult> Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var userId = User.Identity.GetUserId<int>();
                var model = new AffiliatesViewModel()
                {
                    UserReferralId = User.Identity.IsAuthenticated ? $"X{userId}" : "",
                    Commissions = await _commissionService.TableNoTracking.ProjectToListAsync<CommissionViewModel>(),
                    Referrals = await _userService.TableNoTracking.Where(p => p.ReferrerId == userId).ProjectToListAsync<ReferralUserViewModel>()
                };
                return View(model);
            }
            else
            {
                var model = new AffiliatesViewModel()
                {
                    Commissions = await _commissionService.TableNoTracking.ProjectToListAsync<CommissionViewModel>(),
                };
                return View(model);
            }
        }
    }
}