﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Service;

namespace Xtoman.Web.Controllers
{
    public class PriceController : Controller
    {
        #region MyRegion
        private readonly IBalanceManager _balanceManager;
        private readonly IPriceManager _priceManager;
        #endregion

        #region Constructor
        public PriceController(
            IBalanceManager balanceManager,
            IPriceManager priceManager)
        {
            _balanceManager = balanceManager;
            _priceManager = priceManager;
        }
        #endregion

        public async Task<ActionResult> Index()
        {
            var allBalances = await _balanceManager.GetAllBalancesAsync();
            var prices = await _priceManager.GetAllBuyAndSellAsync();

            return View();
        }
    }
}