﻿using AutoMapper;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Framework;
using Xtoman.Service;
using Xtoman.Service.WebServices;
using Xtoman.Utility;
using Xtoman.Web.ViewModels;

namespace Xtoman.Web.Controllers
{
    public class CurrencyController : BaseController
    {
        #region Properties
        private readonly ICurrencyTypeService _currencyTypeService;
        private readonly IPriceManager _priceManager;
        private readonly ICoinMarketCapService _coinMarketCapService;
        #endregion

        #region Constructor
        public CurrencyController(
            ICurrencyTypeService currencyTypeService,
            ICoinMarketCapService coinMarketCapService,
            IPriceManager priceManager)
        {
            _currencyTypeService = currencyTypeService;
            _priceManager = priceManager;
            _coinMarketCapService = coinMarketCapService;
        }
        #endregion

        #region Actions
        public async Task<ActionResult> Index(string url)
        {
            if (url.HasValue())
                return RedirectToAction(url);

            var currencyTypes = await _currencyTypeService.GetAllCachedAsync();
            var model = new CurrencyTypeViewModel().FromEntity(currencyTypes).ToList();
            return View(model);
        }

        public async Task<ActionResult> Single(string url)
        {
            try
            {
                var model = await _currencyTypeService.TableNoTracking
                    .Where(p => p.Url == url)
                    .IncludeProperties(p => p.Media)
                    .ProjectToSingleAsync<CurrencySingleViewModel>();

                PrepareModelShetabName(model);
                if (model == null)
                    return HttpNotFound();

                if (!model.IsFiat && model.CoinMarketCapId.HasValue())
                    ViewBag.CoinMarketCap = await _coinMarketCapService.GetTickerAsync(model.CoinMarketCapId);

                if (model.Type != ECurrencyType.Shetab)
                {
                    var exchangeTypeId = model.ExchangeTypesTo.Where(p => p.FromCurrencyType == ECurrencyType.Shetab).Select(p => p.Id).FirstOrDefault();
                    ViewBag.Rate = await _priceManager.GetExchangeRateAsync(exchangeTypeId);
                }

                return View(model);
            }
            catch (Exception ex)
            {
                ex.LogError();
                return HttpNotFound();
            }
        }

        private void PrepareModelShetabName(CurrencySingleViewModel model)
        {
            var shetabName = "ریال شتاب";
            var shetabAltName = "Rial Shetab";
            if (model.Type == ECurrencyType.Shetab)
            {
                model.Name = shetabName;
                model.AlternativeName = shetabAltName;
            }
            model.ExchangeTypesFrom.ForEach((x) =>
            {
                if (x.FromCurrencyType == ECurrencyType.Shetab)
                {
                    x.FromCurrencyName = shetabName;
                    x.FromCurrencyAlternativeName = shetabAltName;
                }
                if (x.ToCurrencyType == ECurrencyType.Shetab)
                {
                    x.ToCurrencyName = shetabName;
                    x.ToCurrencyAlternativeName = shetabAltName;
                }
            });
            model.ExchangeTypesTo.ForEach((x) =>
            {
                if (x.FromCurrencyType == ECurrencyType.Shetab)
                {
                    x.FromCurrencyName = shetabName;
                    x.FromCurrencyAlternativeName = shetabAltName;
                }
                if (x.ToCurrencyType == ECurrencyType.Shetab)
                {
                    x.ToCurrencyName = shetabName;
                    x.ToCurrencyAlternativeName = shetabAltName;
                }
            });
        }
        #endregion
    }
}