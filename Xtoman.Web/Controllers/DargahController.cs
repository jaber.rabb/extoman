﻿using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Payment.Mellat;
using Xtoman.Framework;
using Xtoman.Framework.Mvc;
using Xtoman.Service;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DargahController : BaseController
    {
        private readonly IMellatService _mellatService;
        private readonly IMessageManager _messageManager;
        private readonly IUserBankCardService _userBankCardService;
        private readonly IFinnotechService _finnotechService;
        public DargahController(IMellatService mellatService,
            IMessageManager messageManager,
            IUserBankCardService userBankCardService,
            IFinnotechService finnotechService)
        {
            _mellatService = mellatService;
            _messageManager = messageManager;
            _userBankCardService = userBankCardService;
            _finnotechService = finnotechService;
        }
        public async Task<ActionResult> Index()
        {
            await TestMellatPaymentAsync();
            return null;
        }

        //public async Task<ActionResult> Fin()
        //{
        //    var result = await _finnotechService.BasicAuthorizeAsync();
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        public async Task<ActionResult> MellatCallBack(MellatCallBack mellatCallBack, long timestamp)
        {
            if (!MellatHasReferenceValue(mellatCallBack))
                return Content("No Reference");

            if (mellatCallBack.resCode != MellatResponseMessage.Success.ToInt())
            {
                return Content($"Voided: {((MellatResponseMessage)mellatCallBack.resCode).ToDisplay()}");
            }

            if (await VerifyPaymentAsync(timestamp, mellatCallBack.cardHolderPan, mellatCallBack.saleReferenceId ?? 0))
            {
                return Content($"پرداخت با موفقیت انجام شد.");
            }
            else
            {
                return Content($"Verify failed");
            }
        }

        private async Task<bool> VerifyPaymentAsync(long timestamp, string pan, long saleReferenceId)
        {
            var success = false;
            var retryCount = 0;
            var bankResponded = false;

            //Check card number is verified
            var userId = User.Identity.GetUserId<int>();
            var userVerifiedCards = await _userBankCardService.TableNoTracking
                .Where(p => p.UserId == userId && p.VerificationStatus == VerificationStatus.Confirmed)
                .ToListAsync();

            var isCardVerified = userVerifiedCards.Any(x => string.Join("******", x.CardNumber.Left(6), x.CardNumber.Right(4)).Equals(pan));

            if (isCardVerified)
            {
                while (!bankResponded && retryCount < 3)
                {
                    retryCount++;
                    try
                    {
                        var verifyRequest = await _mellatService.VerifyRequestAsync(timestamp, timestamp, saleReferenceId);
                        success = verifyRequest == MellatResponseMessage.Success;
                        bankResponded = true;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                        await _messageManager.TelegramAdminMessageAsync($"خطا در هنگام تایید پرداخت تست بانک ملت");
                        switch (retryCount)
                        {
                            case 1:
                                await _messageManager.TelegramAdminMessageAsync($"خطا در هنگام تایید پرداخت تست بانک ملت");
                                break;
                            case 2:
                                await _messageManager.TelegramAdminMessageAsync($"خطا در هنگام تایید پرداخت تست بانک ملت");
                                break;
                            case 3:
                                await _messageManager.TelegramAdminMessageAsync($"خطا در هنگام تایید پرداخت تست بانک ملت");
                                break;
                        }
                    }
                }
            }
            return success;
        }

        private bool MellatHasReferenceValue(MellatCallBack mellatCallBack)
        {
            return mellatCallBack.saleReferenceId.HasValue;
        }

        private async Task TestMellatPaymentAsync()
        {
            var timestamp = DateTime.UtcNow.ConvertToUnixTimestamp();
            var amount = 200M;
            var bankCallBackUrl = Url.Action("MellatCallBack", "Dargah", new { timestamp });
            var retryCount = 0;
            var bankResponded = false;
            while (!bankResponded && retryCount < 3)
            {
                retryCount++;
                try
                {
                    await _mellatService.MakePaymentAsync(amount.ToLong(), $"Test {timestamp}", timestamp, bankCallBackUrl, httpContext: HttpContext);
                    bankResponded = true;
                }
                catch (Exception ex)
                {
                    ex.LogError();
                    if (retryCount == 3)
                    {
                        await _messageManager.TelegramAdminMessageAsync($"خطا در هنگام ورود به بانک {BankGatewayType.BehPardakht.ToDisplay()}");
                    }
                }
            }
        }
    }
}