﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Payment.CoinPayments;
using Xtoman.Framework;
using Xtoman.Service;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Web.Controllers
{
    public class TestCPController : BaseController
    {
        private readonly ICoinPaymentsService _coinPaymentsService;
        private readonly ICoinPaymentsIPNResultService _coinPaymentsIPNResultService;

        public TestCPController(
            ICoinPaymentsService coinPaymentsService,
            ICoinPaymentsIPNResultService coinPaymentsIPNResultService)
        {
            _coinPaymentsService = coinPaymentsService;
            _coinPaymentsIPNResultService = coinPaymentsIPNResultService;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Index(ECurrencyType type)
        {
            var currencySign = type.ToCoinPaymentsCurrency();
            var coinPaymentsCallBack = Url.Action("CallBack", "TestCP", null, Request.Url.Scheme);
            var model = await _coinPaymentsService.CallbackAddressAsync(currencySign, coinPaymentsCallBack);
            return View("Address", model);
        }

        [HttpPost]
        public virtual async Task<ActionResult> CallBack()
        {
            var ipnType = HttpContext.Request.Form["ipn_type"] != null ? HttpContext.Request.Form["ipn_type"].ToLower() : "";
            var hmac = HttpContext.Request.Headers["HMAC"];
            if (hmac == null)
                return LogAndReturnResult(HttpStatusCode.BadRequest, "HMAC Not Presented!");
            var source = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Request.InputStream, Encoding.UTF8))
                source = reader.ReadToEnd();

            var merchant = HttpContext.Request.Form["merchant"];
            if (!merchant.HasValue())
                return LogAndReturnResult(HttpStatusCode.BadRequest, "Merchant ID Not Presented!");

            if (!CoinPaymentsCryptoUtil.SignIsValid(source, hmac, merchant))
                return LogAndReturnResult(HttpStatusCode.BadRequest, "Invalid HMAC / MerchantId");

            new Exception($"IPN Type: {ipnType}").LogError();
            if (ipnType.HasValue())
                switch (ipnType)
                {
                    case "api":
                        var IPNApiResult = CoinPaymentsCryptoUtil.ParseIPNResult<IPNAPIResult>(HttpContext.Request.Form);
                        return await APIAsync(IPNApiResult, source, hmac);
                    case "deposit":
                        var IPNDepositResult = CoinPaymentsCryptoUtil.ParseIPNResult<IPNDepositResult>(HttpContext.Request.Form);
                        return await DepositAsync(IPNDepositResult, source, hmac);
                }
            return HttpNotFound();
        }

        private async Task<ActionResult> APIAsync(IPNAPIResult result, string source, string hmac)
        {
            var coinPaymentsIPNResult = PrepareCoinPaymentsAPIIPNResult(result);
            await _coinPaymentsIPNResultService.InsertAsync(coinPaymentsIPNResult);

            if (result.SuccessStatusLax(/*order.PayAmount, */result.received_amount.TryToDecimal()))
            {
                
            }
            else
            {

            }

            return LogAndReturnResult(HttpStatusCode.OK, "1");
        }

        public virtual async Task<ActionResult> DepositAsync(IPNDepositResult result, string source, string hmac)
        {
            var coinPaymentsIPNResult = PrepareCoinPaymentsDepositIPNResult(result);
            await _coinPaymentsIPNResultService.InsertAsync(coinPaymentsIPNResult);

            return LogAndReturnResult(HttpStatusCode.OK, "1");
        }

        private CoinPaymentsIPNResult PrepareCoinPaymentsDepositIPNResult(IPNDepositResult result)
        {
            return new CoinPaymentsIPNResult()
            {
                Address = result.address,
                Amount = result.amount,
                Amounti = result.amounti,
                Currency = result.currency,
                IpnId = result.ipn_id,
                IpnMode = result.ipn_mode,
                IpnType = result.ipn_type,
                TxnId = result.txn_id,
                IpnVersion = result.ipn_version,
                StatusText = result.status_text,
                Status = result.status,
                Confirms = result.confirms,
                Fee = result.fee,
                Feei = result.feei,
                Merchant = result.merchant
            };
        }

        private CoinPaymentsIPNResult PrepareCoinPaymentsAPIIPNResult(IPNAPIResult result)
        {
            return new CoinPaymentsIPNResult()
            {
                Amount = result.received_amount.HasValue() ? result.received_amount.TryToDecimal() : 0,
                Amount1 = result.amount1,
                Amount2 = result.amount2,
                BuyerName = result.buyer_name,
                Confirms = result.received_confirms.TryToInt(),
                Currency = result.currency1,
                Currency1 = result.currency1,
                Currency2 = result.currency2,
                IpnId = result.ipn_id,
                IpnMode = result.ipn_mode,
                IpnType = result.ipn_type,
                IpnVersion = result.ipn_version,
                ReceivedConfirms = result.received_confirms,
                Merchant = result.merchant,
                TxnId = result.txn_id,
                StatusText = result.status_text,
                Status = result.status,
                SendTx = result.send_tx,
                ReceivedAmount = result.received_amount,
                ItemNumber = result.item_number,
                ItemName = result.item_name,
                Invoice = result.invoice,
                Fee = result.fee.TryToDecimal(),
                Email = result.email,
                Custom = result.custom
            };
        }
    }
}