﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Framework;
using Xtoman.Service;
using System.Linq;
using Xtoman.Web.ViewModels;
using Microsoft.AspNet.Identity;
using Xtoman.Domain.Models;
using System.Data.Entity;
using Xtoman.Utility;
using AutoMapper;
using System;
using OtpSharp;
using Xtoman.Framework.Mvc;
using System.Web;
using Base32;
using Microsoft.Owin.Security;

namespace Xtoman.Web.Controllers
{
    [Authorize]
    public class PanelController : BaseController
    {
        #region Properties
        private readonly IUserService _userService;
        private readonly IReferralManager _referralManager;
        private readonly IUserFundManager _fundManager;
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly IUserBankCardService _userBankCardService;
        private readonly IAppUserManager _userManager;
        private readonly IAppSignInManager _signInManager;
        private readonly IAuthenticationManager _authenticationManager;
        #endregion

        #region Constructor
        public PanelController(
            IUserService userService,
            IReferralManager referralManager,
            IUserFundManager fundManager,
            IExchangeOrderService exchangeOrderService,
            IUserBankCardService userBankCardService,
            IAppUserManager userManager,
            IAppSignInManager signInManager,
            IAuthenticationManager authenticationManager)
        {
            _userService = userService;
            _referralManager = referralManager;
            _fundManager = fundManager;
            _exchangeOrderService = exchangeOrderService;
            _userBankCardService = userBankCardService;
            _userManager = userManager;
            _signInManager = signInManager;
            _authenticationManager = authenticationManager;
        }
        #endregion

        #region Actions
        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId<int>();

            var userTotalOrdersQuery = _exchangeOrderService.TableNoTracking.Where(p => p.UserId == userId && p.PaymentStatus == PaymentStatus.Paid);

            var userTotalOrdersCount = await userTotalOrdersQuery.CountAsync();

            var lastOrderQuery = userTotalOrdersQuery.OrderByDescending(p => p.InsertDate);

            var lastOrder = await lastOrderQuery.Select(p => p.Exchange)
                .ProjectToFirstOrDefaultAsync<ExchangeTypeSimpleViewModel>();

            var lastOrderDate = await lastOrderQuery.Select(p => p.InsertDate).FirstOrDefaultAsync();

            var totalIncome = await _userService.TableNoTracking.Where(p => p.Id == userId)
                .SelectMany(p => p.Incomes)
                .Select(p => p.Value)
                .DefaultIfEmpty(0)
                .SumAsync();

            var totalReferrals = await _userService.TableNoTracking.Where(p => p.ReferrerId == userId).CountAsync();

            var user = await _userService.TableNoTracking.ProjectToSingleAsync<UserViewModel>(p => p.Id == userId);

            //var discountLevel = await _fundManager.GetDiscountByUserIdAsync(userId);

            var model = new PanelViewModel()
            {
                User = user,
                TotalIncomeFromReferrals = totalIncome,
                LastOrderExchangeType = lastOrder,
                LastOrderDate = lastOrderDate,
                TotalReferrals = totalReferrals,
                TotalOrdersCount = userTotalOrdersCount
            };

            return View(model);
        }

        #region Settings
        public async Task<ActionResult> Account()
        {
            var userId = User.Identity.GetUserId<int>();
            var model = await _userService.TableNoTracking
                .Where(p => p.Id == userId)
                .ProjectToSingleAsync<PanelAccountViewModel>();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Account(PanelAccountViewModel model)
        {
            if (!ModelState.IsValid)
                return View();

            var userId = User.Identity.GetUserId<int>();
            var user = await _userManager.FindByIdAsync(userId);
            var phoneNumberChanged = model.PhoneNumber != user.PhoneNumber;
            var emailChanged = model.Email != user.Email;
            if (phoneNumberChanged)
            {
                user.PhoneNumber = model.PhoneNumber;
                user.PhoneNumberConfirmed = false;
                SuccessNotification("شماره موبایل تغییر یافت. برای تایید به بخش احراز هویت بروید.");
            }
            if (emailChanged)
            {
                user.Email = model.Email;
                user.EmailConfirmed = false;
                SuccessNotification("ایمیل تغییر یافت. برای تایید به بخش احراز هویت بروید.");

            }
            await _userManager.UpdateAsync(user);
            if (!emailChanged && !phoneNumberChanged)
                InfoNotification("هیچ تغییری اعمال نشد.");

            return RedirectToAction("Account");
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var result = await _userManager.ChangePasswordAsync(User.Identity.GetUserId<int>(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await _userManager.FindByIdAsync(User.Identity.GetUserId<int>());
                if (user != null)
                {
                    await _signInManager.SignInAsync(user, false, false);
                    SuccessNotification("کلمه عبور شما تغییر یافت.");
                }
                else
                {
                    ErrorNotification("خطایی رخ داد");
                }
            }
            else
            {
                AddErrors(result);
                return View(model);
            }

            return View();
        }

        public async Task<ActionResult> TwoFa()
        {
            var userId = User.Identity.GetUserId<int>();
            var twoFaIsEnabled = await _userManager.GetGoogleAuthenticationEnabledAsync(userId);
            // Check if two fa is not enabled 
            if (!twoFaIsEnabled)
            {
                byte[] secretKey = KeyGeneration.GenerateRandomKey(20);
                string userName = User.Identity.GetUserName();
                string barcodeUrl = KeyUrl.GetTotpUrl(secretKey, userName) + "&issuer=Extoman";

                var model = new GoogleAuthenticatorViewModel
                {
                    SecretKey = Base32Encoder.Encode(secretKey),
                    BarcodeUrl = HttpUtility.UrlEncode(barcodeUrl)
                };
                return View(model);
            }
            // else
            return View("Disable2Fa");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EnableTwoFa(GoogleAuthenticatorViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId<int>();
                var twoFaIsEnabled = await _userManager.GetGoogleAuthenticationEnabledAsync(userId);
                // Check if two fa is not enabled 
                if (twoFaIsEnabled)
                {
                    ErrorNotification("ورود دو مرحله ای برای حساب شما قبلا فعال شده است.");
                    return RedirectToAction("TwoFa");
                }
                byte[] secretKey = Base32Encoder.Decode(model.SecretKey);

                var otp = new Totp(secretKey);
                if (otp.VerifyTotp(model.Code, out long timeStepMatched, new VerificationWindow(2, 2)))
                {
                    var user = await _userManager.FindByIdAsync(userId);
                    user.IsGoogleAuthenticatorEnabled = true;
                    user.GoogleAuthenticatorSecretKey = model.SecretKey;
                    await _userManager.UpdateAsync(user);
                    await _userManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId<int>(), true);
                    if (user != null)
                        await SignInAsync(user, isPersistent: false);

                    SuccessNotification("ورود دو مرحله ای برای حساب شما فعال شد.");
                    return RedirectToAction("TwoFa");
                }
                else
                    ModelState.AddModelError("Code", "کد وارد شده صحیح نیست!");
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisableTwoFa(GoogleAuthenticatorViewModel model)
        {
            var userId = User.Identity.GetUserId<int>();
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                byte[] secretKey = Base32Encoder.Decode(user.GoogleAuthenticatorSecretKey);

                var otp = new Totp(secretKey);
                if (otp.VerifyTotp(model.Code, out long timeStepMatched, new VerificationWindow(2, 2)))
                {
                    user.IsGoogleAuthenticatorEnabled = false;
                    user.GoogleAuthenticatorSecretKey = null;

                    await _userManager.UpdateAsync(user);
                    await _userManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId<int>(), false);
                    await SignInAsync(user, isPersistent: false);
                    SuccessNotification("ورود دو مرحله ای برای حساب شما غیر فعال شد.");
                }
                else
                {
                    ModelState.AddModelError("Code", "کد 6 رقمی وارد شده اشتباه است");
                    return View("Disable2Fa");
                }
            }
            return RedirectToAction("TwoFa");
        }

        public async Task<ActionResult> BankAccounts()
        {
            var userId = User.Identity.GetUserId<int>();
            var model = await _userService.TableNoTracking.Where(p => p.Id == userId).Select(p => p.UserBankCards)
                .ProjectToListAsync<UserBankAccountViewModel>();
            return View(model);
        }

        public ActionResult AddBankAccount()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<ActionResult> AddBankAccount(UserBankAccountViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var userBankCard = new UserBankCard()
            {
                BankAccountNumber = model.BankAccountNumber,
                BankName = model.BankName,
                CardNumber = model.CardNumber,
                ShebaNumber = model.ShebaNumber
            };
            await _userBankCardService.InsertAsync(userBankCard);

            return RedirectToAction("BankAccounts");
        }
        #endregion

        #region Verification
        public async Task<ActionResult> VerifyDetail()
        {
            var userId = User.Identity.GetUserId<int>();

            var model = await _userService.TableNoTracking
                .Where(p => p.Id == userId)
                .ProjectToSingleAsync<PanelVerifyDetailViewModel>();

            return View(model);
        }
        #endregion

        #endregion

        #region Helpers
        private async Task SignInAsync(AppUser user, bool isPersistent)
        {
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie, DefaultAuthenticationTypes.TwoFactorCookie);
            _authenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, await _userManager.GenerateUserIdentityAsync(user));
        }

        private string GetGreetingMessage()
        {
            var hour = DateTime.Now.Hour;
            switch (hour)
            {
                case 0:
                    return "شب بخیر!";
                case 1:
                    return "شب بخیر! ساعت 1 همه خوابیدن به جز شما";
                case 2:
                    return "صبح بخیر! همه جا تعطیله به جز اکس تومن";
                case 3:
                    return "صبح بخیر! شما جغد هستید؟";
                case 4:
                    return "صبح بخیر! از دیشب نخوابیدید؟";
                case 5:
                    return "صبح بخیر! چیزی به صبح نمونده";
                case 6:
                    return "صبح بخیر! شما سحرخیز ترین هستید";
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                    return "صبح بخیر!";
                case 12:
                    return "روز بخیر!";
                case 13:
                case 14:
                    return "ظهر بخیر!";
                case 15:
                case 16:
                    return "بعداظهر بخیر!";
                case 17:
                case 18:
                    return "عصر بخیر!";
                case 19:
                case 20:
                case 21:
                case 22:
                    return "شب بخیر! خوش موقع آمدید";
                case 23:
                    return "شب رو به اتمام است";
                default:
                    return "خوش آمدید!";
            }
        }
        #endregion

        #region Overrides
        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var userId = User.Identity.GetUserId<int>();
            var userTotalOrdersQuery = _exchangeOrderService.TableNoTracking.Where(p => p.UserId == userId && p.PaymentStatus == PaymentStatus.Paid);

            ViewBag.TotalBuy = userTotalOrdersQuery
                .Where(p => p.Exchange.FromCurrency.Type == ECurrencyType.Shetab)
                .Select(p => p.PayAmount)
                .DefaultIfEmpty(0)
                .SumAsync().Result;

            ViewBag.TotalSell = userTotalOrdersQuery
                .Where(p => p.Exchange.ToCurrency.Type == ECurrencyType.Shetab)
                .Select(p => p.ReceiveAmount)
                .DefaultIfEmpty(0)
                .SumAsync().Result;

            ViewBag.GreetingMessage = GetGreetingMessage();
            var userFN = _userService.TableNoTracking.Where(p => p.Id == userId).Select(p => new { p.FirstName, p.LastName, p.UserName }).FirstOrDefaultAsync().Result;

            ViewBag.FullName = userFN.FirstName.HasValue() && userFN.LastName.HasValue() ? (userFN.FirstName + " " + userFN.LastName).Trim() : userFN.UserName;
            base.OnResultExecuting(filterContext);
        }
        #endregion
    }
}