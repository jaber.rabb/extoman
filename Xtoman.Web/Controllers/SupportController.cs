﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Framework;
using Xtoman.Framework.Mvc;
using Xtoman.Service;
using Xtoman.Utility;
using Xtoman.Utility.PostalEmail;
using Xtoman.Web.ViewModels;

namespace Xtoman.Web.Controllers
{
    public class SupportController : BaseController
    {
        private readonly IUserService _userService;
        private readonly ISupportTicketService _supportTicketService;
        private readonly ISupportTicketReplyService _supportTicketReplyService;
        private readonly IMessageManager _messageManager;
        public SupportController(
            IUserService userService,
            ISupportTicketService supportTicketService,
            ISupportTicketReplyService supportTicketReplyService,
            IMessageManager messageManager)
        {
            _userService = userService;
            _supportTicketService = supportTicketService;
            _supportTicketReplyService = supportTicketReplyService;
            _messageManager = messageManager;
        }

        public async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId<int>();
            var adViewModel = new AddSupportViewModel();
            var ticketsViewModel = new List<SupportTicketItemViewModel>();
            if (Request.IsAuthenticated)
            {
                adViewModel.EmailOrPhoneNumber = (await _userService.TableNoTracking
                    .Where(p => p.Id == userId)
                    .Select(p => new { EmailAndPhoneNumber = p.Email + "|" + p.PhoneNumber })
                    .SingleOrDefaultAsync())
                    .EmailAndPhoneNumber.Trim('|').Split('|')[0];

                ticketsViewModel = await _supportTicketService.TableNoTracking
                    .Where(p => p.EmailOrPhoneNumber == adViewModel.EmailOrPhoneNumber)
                    .OrderByDescending(p => p.InsertDate)
                    .ProjectToListAsync<SupportTicketItemViewModel>();
            }
            var model = new SupportIndexViewModel()
            {
                AddViewModel = adViewModel,
                Tickets = ticketsViewModel
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(SupportIndexViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!AppSettingManager.IsLocale)
                {
                    var captchaResult = await HttpContext.ValidateCaptchaAsync("6LflMzoUAAAAACuYcwEmXd1jTfby3YKO5CDhC_h9");
                    if (captchaResult != RecaptchaResponse.Success)
                    {
                        ModelState.AddModelError("", "شما ربات هستید.");
                        return View(model);
                    }
                }

                var userName = Request.IsAuthenticated ? User.Identity.GetUserName() : "";

                var entity = new SupportTicket()
                {
                    EmailOrPhoneNumber = model.AddViewModel.EmailOrPhoneNumber,
                    Priority = model.AddViewModel.Priority,
                    Subject = model.AddViewModel.Subject,
                    Text = model.AddViewModel.Text,
                    UserName = userName
                };
                await _supportTicketService.InsertAsync(entity);
                var userId = 0;
                var email = entity.EmailOrPhoneNumber.IsEmail() ? entity.EmailOrPhoneNumber : "";
                var phoneNumber = entity.EmailOrPhoneNumber.IsMobile() ? entity.EmailOrPhoneNumber : "";
                if (!userName.HasValue())
                    userName = email.HasValue() ? email : phoneNumber;

                var fullName = userName;
                if (Request.IsAuthenticated)
                {
                    userId = User.Identity.GetUserId<int>();
                    var emailAndPhone = await _userService.TableNoTracking.Where(p => p.Id == userId).Select(p => new { p.Email, p.PhoneNumber }).SingleAsync();
                    email = emailAndPhone.Email;
                    phoneNumber = emailAndPhone.PhoneNumber;
                    var user = await _userService.TableNoTracking.Where(p => p.Id == userId)
                        .Select(p => new
                        {
                            FullName = p.FirstName + " " + p.LastName,
                            p.UserName
                        })
                    .SingleAsync();
                    if (user.FullName.HasValue())
                        fullName = user.FullName;
                    userName = user.UserName;
                }

                var ticketUrl = Url.Action("Ticket", "Support", new { id = entity.Id }, protocol: Request.Url.Scheme);

                var supportEmail = new SupportEmail()
                {
                    From = "no-reply@extoman.com",
                    Fullname = fullName,
                    Subject = "درخواست پشتیبانی ثبت گردید",
                    TicketSubject = model.AddViewModel.Subject,
                    TicketText = model.AddViewModel.Text,
                    TicketUrl = ticketUrl,
                    To = email,
                    Username = userName,
                    ViewName = "SupportTicketSubmit"
                };

                var supportEmailBody = supportEmail.GetMailMessage().Body;
                await _messageManager.SupportTicketSubmitedAsync(supportEmailBody, email, phoneNumber, entity.Subject, entity.Text, ticketUrl);
                return RedirectToAction("Ticket", new { id = entity.Id });
            }
            if (Request.IsAuthenticated)
                model.Tickets = await _supportTicketService.TableNoTracking
                    .Where(p => p.EmailOrPhoneNumber == model.AddViewModel.EmailOrPhoneNumber)
                    .ProjectToListAsync<SupportTicketItemViewModel>();

            return View(model);
        }

        public async Task<ActionResult> Ticket(int id)
        {
            var ticketExist = false;
            if (Request.IsAuthenticated)
            {
                var userName = User.Identity.GetUserName();
                var user = _userService.TableNoTracking.Where(p => p.UserName == userName).Select(p => new { p.Email, p.PhoneNumber }).FirstOrDefault();

                var query = _supportTicketService.TableNoTracking
                    .Where(p => p.Id == id);

                if (user != null)
                    query = query.Where(p => p.UserName == userName || p.EmailOrPhoneNumber == user.Email || p.EmailOrPhoneNumber == user.PhoneNumber);

                ticketExist = await query.AnyAsync();
            }
            else
            {
                ticketExist = await _supportTicketService.TableNoTracking.Where(p => p.Id == id).AnyAsync();
            }
            if (ticketExist)
            {
                var model = await _supportTicketService.TableNoTracking
                    .Where(p => p.Id == id)
                    .ProjectToSingleAsync<SupportTicketViewModel>();

                return View(model);
            }
            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Reply(SupportTicketReplySubmitViewModel model)
        {
            var result = new ResultModel();
            if (ModelState.IsValid)
            {
                try
                {
                    var supportTicket = await _supportTicketService.Table.Where(p => p.Id == model.Id).FirstOrDefaultAsync();
                    if (supportTicket != null)
                    {
                        var isAuth = User.Identity.IsAuthenticated;
                        var isValid = false;
                        if (isAuth)
                        {
                            var userId = User.Identity.GetUserId<int>();
                            isValid = supportTicket.UserName == User.Identity.GetUserName();
                            if (!isValid)
                            {
                                var user = await _userService.TableNoTracking.Where(p => p.Id == userId).Select(p => new { p.PhoneNumber, p.Email }).FirstOrDefaultAsync();
                                isValid = user.Email == supportTicket.UserName || user.PhoneNumber == supportTicket.UserName;
                            }
                        }
                        else
                        {
                            isValid = !await _userService.TableNoTracking.AnyAsync(x => x.UserName == supportTicket.UserName || x.Email == supportTicket.UserName || x.PhoneNumber == supportTicket.UserName);
                        }
                        var entity = new SupportTicketReply() { SupportTicketId = model.Id, Text = model.Text, IsSupportReply = false };
                        if (supportTicket.Status != TicketStatus.WaitingReply)
                        {
                            supportTicket.Status = TicketStatus.WaitingReply;
                            await _supportTicketService.UpdateAsync(supportTicket);
                        }
                        await _supportTicketReplyService.InsertAsync(entity);
                        result.Text = "پاسخ شما با موفقیت ثبت شد.";
                        result.Status = true;
                    }
                }
                catch (Exception ex)
                {
                    ex.LogError();
                    result.Text = "خطایی هنگام ثبت پاسخ رخ داد.!";
                }

                if (result.Status)
                {
                    SuccessNotification(result.Text);
                }
                else
                    ErrorNotification(result.Text);

                return RedirectToAction("Ticket", new { model.Id });
            }
            else
                result.Text = "متن پاسخ خالی است.";

            if (Request.IsAjaxRequest())
                return Json(result);

            var retmodel = await _supportTicketService.TableNoTracking
                .Where(p => p.Id == model.Id)
                .ProjectToSingleAsync<SupportTicketViewModel>();

            return View(retmodel);
        }
    }
}