﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Framework;
using Xtoman.Service;
using Xtoman.Service.WebServices;
using Xtoman.Utility;
using Xtoman.Web.ViewModels;

namespace Xtoman.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IExchangeTypeService _exchangeTypeService;
        //private readonly IExchangeTypeRateService _exchangeTypeRateService;
        private readonly IPostService _postService;
        private readonly IPostCategoryService _postCategoryService;
        private readonly IPostTagService _postTagService;
        private readonly ICoinMarketCapService _coinMarketCapService;
        //private readonly IBalanceManager _balanceManager;
        private readonly ICurrencyTypeService _currencyTypeService;
        private readonly IOrderReviewService _orderReviewService;
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly IUserService _userService;
        public HomeController(
            //IExchangeTypeRateService exchangeTypeRateService,
            IExchangeTypeService exchangeTypeService,
            IPostService postService,
            IPostCategoryService postCategoryService,
            IPostTagService postTagService,
            ICoinMarketCapService coinMarketCapService,
            //IBalanceManager balanceManager,
            ICurrencyTypeService currencyTypeService,
            IOrderReviewService orderReviewService,
            IExchangeOrderService exchangeOrderService,
            IUserService userService)
        {
            _exchangeTypeService = exchangeTypeService;
            //_exchangeTypeRateService = exchangeTypeRateService;
            _postService = postService;
            _postCategoryService = postCategoryService;
            _postTagService = postTagService;
            _coinMarketCapService = coinMarketCapService;
            //_balanceManager = balanceManager;
            _currencyTypeService = currencyTypeService;
            _orderReviewService = orderReviewService;
            _exchangeOrderService = exchangeOrderService;
            _userService = userService;
        }

        //public async Task<ActionResult> Home()
        //{
        //    var now = DateTime.UtcNow;
        //    var model = new NewHomeViewModel()
        //    {
        //        Currencies = await _currencyTypeService.TableNoTracking
        //                        .Where(p => !p.IsFiat)
        //                        .ProjectToListAsync<CurrencyTypeViewModel>(),
        //        LatestPosts = await _postService.TableNoTracking
        //                        .Where(p => p.PublishDate != null && p.PublishDate <= now)
        //                        .OrderByDescending(p => p.PublishDate)
        //                        .Take(3)
        //                        .ProjectToListAsync<ShortPostViewModel>()
        //    };

        //    foreach (var x in model.Currencies)
        //    {
        //        //var ticker = coinMarketCapPrices.Where(p => p.Symbol.Equals(x.Type.ToDisplay(DisplayProperty.ShortName))).FirstOrDefault();
        //        //if (ticker != null)
        //        //{
        //        x.Price = /*ticker.Price_usd ??*/ 0;
        //        x.Change24 = /*ticker.Percent_change_24h ??*/ 0;
        //        x.ImageSrc = "/Content/img/لوگو-" + x.Url + ".png";
        //        //}
        //    }

        //    return View(model);
        //}

        public async Task<ActionResult> Index()
        {
            //if (!User.Identity.IsAuthenticated)
            //{
            //    return RedirectToAction("Home");
            //}
            var exchangeTypes = await _exchangeTypeService.TableNoTracking
                .Where(p => !p.FromCurrency.Invisible && !p.ToCurrency.Invisible)
                .OrderByDescending(p => p.UserExchanges.Count)
                .ProjectToListAsync<ExchangeTypeViewModel>();

            var now = DateTime.UtcNow;
            var past24 = now.AddDays(-1);

            var reviews = await _exchangeOrderService.TableNoTracking
                .Where(p => p.Review != null && p.Review.Text.Length > 10 && p.Review.VoteRate >= 3)
                .Select(p => new ReviewViewModel()
                {
                    Id = p.Review.Id,
                    OrderUserFirstName = p.InsertUser.FirstName,
                    OrderUserLastName = p.InsertUser.LastName,
                    OrderUserUserName = p.InsertUser.UserName,
                    Text = p.Review.Text,
                    Visibility = p.Review.Visibility,
                    VoteRate = p.Review.VoteRate,
                    OrderExchangeFromCurrency = new CurrencyTypeViewModel() {
                        Id = p.Exchange.FromCurrencyId,
                        UnitSign = p.Exchange.FromCurrency.UnitSign,
                        AlternativeName = p.Exchange.FromCurrency.AlternativeName,
                        Description = p.Exchange.FromCurrency.Description,
                        Disabled = p.Exchange.FromCurrency.Disabled,
                        Name = p.Exchange.FromCurrency.Name,
                        Type = p.Exchange.FromCurrency.Type,
                        Step = p.Exchange.FromCurrency.Step,
                        Url = p.Exchange.FromCurrency.Url,
                        IsFiat = p.Exchange.FromCurrency.IsFiat
                    },
                    OrderExchangeToCurrency = new CurrencyTypeViewModel()
                    {
                        Id = p.Exchange.ToCurrencyId,
                        UnitSign = p.Exchange.ToCurrency.UnitSign,
                        AlternativeName = p.Exchange.ToCurrency.AlternativeName,
                        Description = p.Exchange.ToCurrency.Description,
                        Disabled = p.Exchange.ToCurrency.Disabled,
                        Name = p.Exchange.ToCurrency.Name,
                        Type = p.Exchange.ToCurrency.Type,
                        Step = p.Exchange.ToCurrency.Step,
                        Url = p.Exchange.ToCurrency.Url,
                        IsFiat = p.Exchange.ToCurrency.IsFiat
                    }
                })
                .OrderByDescending(p => p.Id)
                .Take(16)
                .ToListAsync();
                //.ProjectToListAsync<ReviewViewModel>();

            //var reviews = await _orderReviewService.TableNoTracking
            //    .Where(p => p.Text.Length > 10 && p.VoteRate >= 3)
            //    .Where(p => p.Order is ExchangeOrder)
            //    .OrderByDescending(p => p.Id).Take(12)
            //    .ProjectToListAsync<ReviewViewModel>();

            foreach (var item in reviews)
            {
                if (item.OrderUserFirstName == "معین" && item.OrderUserLastName == "معینی")
                {
                    item.OrderUserFirstName = "فربد";
                    item.OrderUserLastName = "بختیاروند";
                }
            }

            var past24OrdersCount = await _exchangeOrderService.TableNoTracking.Where(p => p.InsertDate >= past24 && p.PaymentStatus == PaymentStatus.Paid).CountAsync();
            var newUsersPast24 = await _userService.TableNoTracking.Where(p => p.RegDate >= past24).CountAsync();
            var topExchangePast24 = await _exchangeTypeService.TableNoTracking
                .Select(p => p.UserExchanges.Where(x => x.InsertDate >= past24 && x.PaymentStatus == PaymentStatus.Paid))
                .OrderByDescending(p => p.Count()).SelectMany(p => p.Select(x => x.Exchange)).ProjectToFirstOrDefaultAsync<ExchangeTypeViewModel>();

            var model = new HomeViewModel()
            {
                NewUsersCountPast24 = newUsersPast24,
                OrdersCountPast24 = past24OrdersCount,
                TopExchangePast24 = topExchangePast24,
                Reviews = reviews,
                ExchangeTypes = exchangeTypes,
                FromCurrencyTypes = exchangeTypes.DistinctBy(p => p.FromCurrencyId)
                    .Select(p => new CurrencyTypeViewModel()
                    {
                        Id = p.FromCurrencyId,
                        Name = p.FromCurrencyName,
                        AlternativeName = p.FromCurrencyAlternativeName,
                        UnitSign = p.FromCurrencyUnitSign,
                        Disabled = p.FromCurrencyDisabled,
                        Min = p.FromCurrencyMinimumAmount,
                        Step = p.FromCurrencyStep,
                        Url = p.FromCurrencyUrl,
                        ImageSrc = "/Content/img/لوگو-" + p.FromCurrencyUrl + ".png",
                        Type = p.FromCurrencyType,
                    })
                    .ToList(),
                ToCurrencyTypes = exchangeTypes.DistinctBy(p => p.ToCurrencyId)
                    .Select(p => new CurrencyTypeViewModel()
                    {
                        Id = p.ToCurrencyId,
                        Name = p.ToCurrencyName,
                        AlternativeName = p.ToCurrencyAlternativeName,
                        UnitSign = p.ToCurrencyUnitSign,
                        Disabled = p.ToCurrencyDisabled,
                        Min = p.ToCurrencyMinimumAmount,
                        Step = p.ToCurrencyStep,
                        Url = p.ToCurrencyUrl,
                        ImageSrc = "/Content/img/لوگو-" + p.ToCurrencyUrl + ".png",
                        Type = p.ToCurrencyType,
                    })
                    .ToList(),
                LatestPosts = await _postService.TableNoTracking
                        .Where(p => p.Enabled)
                        .Where(p => p.PublishDate != null && p.PublishDate <= now)
                        .OrderByDescending(p => p.PublishDate)
                        .Take(3)
                        .ProjectToListAsync<ShortPostViewModel>()
            };

            PreparePosts(model.LatestPosts);
            return View(model);
        }

        public ActionResult Terms()
        {
            return View();
        }

        [ChildActionOnly]
        [OutputCache(Duration = 3600)]
        public ActionResult CurrencyTypesMenu()
        {
            var model = _currencyTypeService.TableNoTracking
                .ProjectToList<CurrencyTypeViewModel>();

            return PartialView(model);
        }

        private void PreparePosts(List<ShortPostViewModel> latestPosts)
        {
            latestPosts.ForEach((x) =>
            {
                x.Summary = x.Summary.CropWholeWords(75) + "...";
            });
        }

        #region Helper
        private string ImageSrcFromThumbnails(List<MediaThumbnailViewModel> thumbnails, string sizeName)
        {
            return thumbnails.Where(x => x.MediaThumbnailSizeName == sizeName).Select(x => x.Url).FirstOrDefault();
        }
        #endregion
    }
}