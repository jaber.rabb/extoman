﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml.Linq;
using Xtoman.Framework;
using Xtoman.Framework.Mvc;
using Xtoman.Service;

namespace Xtoman.Web.Controllers
{
    public class MiscController : BaseController
    {
        private readonly IExchangeTypeService _exchangeTypeService;
        private readonly IPostService _postService;
        private readonly IPostCategoryService _postCategoryService;
        private readonly IPostTagService _postTagService;
        public MiscController(
            IExchangeTypeService exchangeTypeService,
            IPostService postService,
            IPostCategoryService postCategoryService,
            IPostTagService postTagService)
        {
            _exchangeTypeService = exchangeTypeService;
            _postService = postService;
            _postCategoryService = postCategoryService;
            _postTagService = postTagService;
        }

        [Route("robots.txt", Name = "GetRobotsText")/*, OutputCache(Duration = 86400)*/]
        public ActionResult RobotsText()
        {
            var siteMapUrl = Url.RouteUrl("GetSitemapXml", null, Request.Url.Scheme).TrimEnd('/');
            var robotTxtString = GetRobotsTxtString(siteMapUrl);
            return Content(robotTxtString, "text/plain", Encoding.UTF8);
        }

        [Route("sitemap.xml", Name = "GetSitemapXml")/*, OutputCache(Duration = 86400)*/]
        public async Task<ContentResult> SitemapXml()
        {
            string xml = await GetSitemapDocumentAsync(Url);
            return Content(xml, "application/xml", Encoding.UTF8);
        }

        #region Helpers
        private async Task<string> GetSitemapDocumentAsync(UrlHelper url)
        {
            var sitemapNodes = await GetSitemapNodesAsync(url);
            XNamespace xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XElement root = new XElement(xmlns + "urlset");

            foreach (SitemapNode sitemapNode in sitemapNodes)
            {
                XElement urlElement = new XElement(
                    xmlns + "url",
                    new XElement(xmlns + "loc", sitemapNode.Url),
                    sitemapNode.LastModified == null ? null : new XElement(
                        xmlns + "lastmod",
                        sitemapNode.LastModified.Value.ToLocalTime().ToString("yyyy-MM-ddTHH:mm:sszzz")),
                    sitemapNode.Frequency == null ? null : new XElement(
                        xmlns + "changefreq",
                        sitemapNode.Frequency.Value.ToString().ToLowerInvariant()),
                    sitemapNode.Priority == null ? null : new XElement(
                        xmlns + "priority",
                        sitemapNode.Priority.Value.ToString("F1", CultureInfo.InvariantCulture)));
                root.Add(urlElement);
            }

            XDocument document = new XDocument(new XDeclaration("1.0", "UTF-8", null), root);
            return document.ToString();
        }

        private async Task<IReadOnlyCollection<SitemapNode>> GetSitemapNodesAsync(UrlHelper urlHelper)
        {
            List<SitemapNode> nodes = new List<SitemapNode>
            {
                new SitemapNode()
                {
                    Url = WebUtility.UrlDecode(urlHelper.Action("Index", "Home", null, Request.Url.Scheme)),
                    Priority = 1,
                    Frequency = SitemapFrequency.Always
                },
                new SitemapNode()
                {
                    Url = WebUtility.UrlDecode(urlHelper.Action("Index", "Exchange", null, Request.Url.Scheme)),
                    Priority = 0.9,
                },
                new SitemapNode()
                {
                    Url = WebUtility.UrlDecode(urlHelper.Action("Index", "Blog", null, Request.Url.Scheme)),
                    Priority = 0.9,
                    Frequency = SitemapFrequency.Always
                }
            };

            var exchangeTypes = await _exchangeTypeService.TableNoTracking.Select(p => new
            {
                FromCurrencyUrl = p.FromCurrency.Url,
                ToCurrencyUrl = p.ToCurrency.Url
            })
            .ToListAsync();
            exchangeTypes.ForEach((item) => {
                if (item.FromCurrencyUrl == "شتاب-تومان-ریال")
                {
                    // Buy
                    nodes.Add(new SitemapNode()
                    {
                        Url = WebUtility.UrlDecode(urlHelper.RouteUrl("ExchangeBuy", new { toUrl = item.ToCurrencyUrl }, Request.Url.Scheme)),
                        Priority = 1
                    });
                }
                else if (item.ToCurrencyUrl == "شتاب-تومان-ریال")
                {
                    // Sell
                    nodes.Add(new SitemapNode()
                    {
                        Url = WebUtility.UrlDecode(urlHelper.RouteUrl("ExchangeSell", new { fromUrl = item.FromCurrencyUrl }, Request.Url.Scheme)),
                        Priority = 1
                    });
                }
                else
                {
                    nodes.Add(new SitemapNode()
                    {
                        Url = WebUtility.UrlDecode(urlHelper.RouteUrl("Exchange", new { fromUrl = item.FromCurrencyUrl, toUrl = item.ToCurrencyUrl }, Request.Url.Scheme)),
                        Priority = 0.9
                    });
                }
            });

            var postUrls = await _postService.TableNoTracking.Select(p => p.Url).ToListAsync();
            postUrls.ForEach((url) => {
                nodes.Add(new SitemapNode()
                {
                    Url = WebUtility.UrlDecode(urlHelper.RouteUrl("SinglePost", new { url }, Request.Url.Scheme)),
                    Priority = 0.8,
                    Frequency = SitemapFrequency.Weekly
                });
            });

            var postCategoryUrls = await _postCategoryService.TableNoTracking.Select(p => p.Url).ToListAsync();
            postCategoryUrls.ForEach((url) =>
            {

            });

            var postTagUrls = await _postTagService.TableNoTracking.Select(p => p.Url).ToListAsync();
            postCategoryUrls.ForEach((url) =>
            {

            });
            return nodes;
        }

        private string GetRobotsTxtString(string siteMapUrl)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("user-agent: *");
            stringBuilder.AppendLine("disallow: /Error");
            stringBuilder.AppendLine("disallow: /Error/");

            stringBuilder.Append("sitemap: ");
            stringBuilder.AppendLine(siteMapUrl);

            return stringBuilder.ToString();
        }
        #endregion
    }
}