﻿using System.Web.Mvc;
using Xtoman.Framework;

namespace Xtoman.Web.Controllers
{
    public class MaintenanceController : BaseController
    {
        // GET: Maintenance
        public ActionResult Index()
        {
            return View();
        }
    }
}