﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Framework;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Web.Controllers
{
    public class PaymentController : BaseController
    {
        private readonly IExchangeOrderService _exchangeOrderService;
        public PaymentController(
            IExchangeOrderService exchangeOrderService)
        {
            _exchangeOrderService = exchangeOrderService;
        }

        #region Actions
        public async Task<ActionResult> Success(PerfectMoneyPaymentStatus perfectMoneyStatus)
        {
            return await RedirectToExchangeOrderAsync(perfectMoneyStatus, false);
        }

        public async Task<ActionResult> UnSuccess(PerfectMoneyPaymentStatus perfectMoneyStatus)
        {
            return await RedirectToExchangeOrderAsync(perfectMoneyStatus, true);
        }
        #endregion

        #region Helpers
        private async Task<ActionResult> RedirectToExchangeOrderAsync(PerfectMoneyPaymentStatus perfectMoneyStatus, bool isSuccess)
        {
            if (perfectMoneyStatus == null || !perfectMoneyStatus.PAYMENT_ID.HasValue())
                return HttpNotFound();

            var orderGuid = Guid.Parse(perfectMoneyStatus.PAYMENT_ID);
            var exchangeOrder = await _exchangeOrderService.Table.SingleAsync(p => p.Guid == orderGuid);
            if (exchangeOrder.PaymentStatus == PaymentStatus.InPayment)
            {
                exchangeOrder.PaymentStatus = PaymentStatus.NotPaid;
                await _exchangeOrderService.UpdateAsync(exchangeOrder);
            }
            return RedirectToAction("Index", "ExchangeOrder", new { orderGuid = perfectMoneyStatus.PAYMENT_ID });
        }
        #endregion
    }
}