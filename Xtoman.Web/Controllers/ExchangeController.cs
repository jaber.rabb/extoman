﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Framework;
using Xtoman.Service;
using Xtoman.Service.WebServices;
using Xtoman.Utility;
using Xtoman.Utility.PostalEmail;
using Xtoman.Web.ViewModels;

namespace Xtoman.Web.Controllers
{
    public class ExchangeController : BaseController
    {
        #region Properties
        private readonly IUserService _userService;
        private readonly ICurrencyTypeService _currencyTypeService;
        private readonly IExchangeTypeService _exchangeTypeService;
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly IPayIRService _payIRService;
        //private readonly IExchangeTypeRateService _exchangeTypeRateService;
        private readonly IMessageManager _messageManager;
        private readonly IPriceManager _priceManager;
        private readonly IBalanceManager _balanceManager;
        //private readonly IOrderReviewService _orderReviewService;
        #endregion

        #region Constructor
        public ExchangeController(
            IUserService userService,
            ICurrencyTypeService currencyTypeService,
            IExchangeTypeService exchangeTypeService,
            IExchangeOrderService exchangeOrderService,
            IPayIRService payIRService,
            //IExchangeTypeRateService exchangeTypeRateService,
            IMessageManager messageManager,
            IPriceManager priceManager,
            IBalanceManager balanceManager)
            //IOrderReviewService orderReviewService
        {
            _userService = userService;
            _currencyTypeService = currencyTypeService;
            _exchangeTypeService = exchangeTypeService;
            _exchangeOrderService = exchangeOrderService;
            _payIRService = payIRService;
            //_exchangeTypeRateService = exchangeTypeRateService;
            _messageManager = messageManager;
            _priceManager = priceManager;
            _balanceManager = balanceManager;
            //_orderReviewService = orderReviewService;
        }
        #endregion

        #region Actions
        public async Task<ActionResult> Index(string fromUrl, string toUrl, string segments)
        {
            //TODO: Clear exxchange session
            ClearExchangeSession();
            if (fromUrl.HasValue())
                FromId = await _currencyTypeService.TableNoTracking.Where(p => p.Url == fromUrl).Select(p => p.Id).FirstOrDefaultAsync();

            if (toUrl.HasValue())
                ToId = await _currencyTypeService.TableNoTracking.Where(p => p.Url == toUrl).Select(p => p.Id).FirstOrDefaultAsync();

            var (amount, isFrom) = GetSegments(segments);

            var exchangeType = await _exchangeTypeService.TableNoTracking
                .Where(p => p.FromCurrencyId == FromId.Value && p.ToCurrencyId == ToId.Value)
                .SingleAsync();

            List<ReviewViewModel> reviews;
            if (FromId.HasValue && FromId.Value > 0 && ToId.HasValue && ToId.Value > 0)
                reviews = await PrepareExchangeOrdersViewModel(exchangeType.Id);
            else
                reviews = new List<ReviewViewModel>();

            if (FromId.HasValue && FromId.Value > 0 && ToId.HasValue && ToId.Value > 0 && amount.HasValue)
            {
                SetExchangeSession(FromId.Value, ToId.Value, amount.Value, isFrom);

                var exchangeAmount = await _priceManager.GetExchangeAmountAsync(exchangeType, amount.Value, isFrom, true);
                var userId = User.Identity.IsAuthenticated ? User.Identity.GetUserId<int>() : 0;
                exchangeAmount.FromMaxAmount = await UpdateFromMaxAmountAsync(userId, exchangeAmount.FromMaxAmount);
                //if (!exchangeAmount.IsAvailable)
                //    return RedirectToAction("NotAvailable", new { name = $"{exchangeType.FromCurrency.Name} به {exchangeType.ToCurrency.Name}" });
                if (!exchangeType.Disabled && !exchangeType.FromCurrency.Disabled && !exchangeType.ToCurrency.Disabled)
                {
                    if (exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.CoinToCoin)
                        return RedirectToAction("NotAvailable", new { fromId = exchangeType.FromCurrencyId, toId = exchangeType.ToCurrencyId });
                    //if (exchangeAmount.ToMaxAmount == 0)
                    //    return RedirectToAction("NotAvailable");
                    else if (exchangeAmount.ToAmount >= exchangeAmount.ToMinAmount &&
                        exchangeAmount.ToAmount <= exchangeAmount.ToMaxAmount &&
                        exchangeAmount.FromAmount >= exchangeAmount.FromMinAmount &&
                        exchangeAmount.FromAmount <= exchangeAmount.FromMaxAmount)
                    {
                        return RedirectToAction("WalletAddress");
                    }
                }
            }

            var exchangeTypes = await _exchangeTypeService.TableNoTracking
                .Where(p => !p.FromCurrency.Invisible && !p.ToCurrency.Invisible)
                .OrderByDescending(p => p.UserExchanges.Count)
                .ProjectToListAsync<ExchangeTypeViewModel>();

            var model = new ExchangeIndexViewModel()
            {
                Reviews = reviews,
                WithdrawFee = exchangeTypes.Where(p => p.ToCurrencyId == ToId).Select(p => p.ToCurrencyTransactionFee).DefaultIfEmpty(0).FirstOrDefault().TryToDecimal(),
                FromId = FromId,
                ToId = ToId,
                Amount = amount,
                IsFromAmount = isFrom,
                ExchangeTypes = exchangeTypes,
                FromCurrencyTypes = exchangeTypes.DistinctBy(p => p.FromCurrencyId)
                    .Select(p => new CurrencyTypeViewModel()
                    {
                        Id = p.FromCurrencyId,
                        Name = p.FromCurrencyName,
                        AlternativeName = p.FromCurrencyAlternativeName,
                        UnitSign = p.FromCurrencyUnitSign,
                        Disabled = p.FromCurrencyDisabled,
                        Min = p.FromCurrencyMinimumAmount,
                        Step = p.FromCurrencyStep,
                        Url = p.FromCurrencyUrl,
                        Type = p.FromCurrencyType,
                        ImageSrc = "/Content/img/لوگو-" + p.FromCurrencyUrl + ".png",
                        //ImageSrc = p.FromCurrencyMedia != null ? p.FromCurrencyMedia.Thumbnails.ImgSrc(Domain.Models.MediaThumbnailType.AllTiny, p.FromCurrencyMedia.Url) : ""
                    })
                    .ToList(),
                ToCurrencyTypes = exchangeTypes.DistinctBy(p => p.ToCurrencyId)
                    .Select(p => new CurrencyTypeViewModel()
                    {
                        Id = p.ToCurrencyId,
                        Name = p.ToCurrencyName,
                        AlternativeName = p.ToCurrencyAlternativeName,
                        UnitSign = p.ToCurrencyUnitSign,
                        Disabled = p.ToCurrencyDisabled,
                        Min = p.ToCurrencyMinimumAmount,
                        Step = p.ToCurrencyStep,
                        Url = p.ToCurrencyUrl,
                        Type = p.ToCurrencyType,
                        ImageSrc = "/Content/img/لوگو-" + p.ToCurrencyUrl + ".png"
                        //ImageSrc = p.ToCurrencyMedia != null ? p.ToCurrencyMedia.Thumbnails.ImgSrc(Domain.Models.MediaThumbnailType.AllTiny, p.FromCurrencyMedia.Url) : ""
                    })
                    .ToList()
            };
            if (User.Identity.IsAuthenticated)
            {
                var userId = User.Identity.GetUserId<int>();
                var isLimited = await _userService.IsBuyLimitedAsync(userId);
                if (isLimited)
                {
                    model.IsBuyLimited = isLimited;
                    model.FromMaxAmount = await UpdateFromMaxAmountAsync(userId, 0);
                }
            }

            var ratingCount = model.Reviews.Count;
            var ratingReviewCount = model.Reviews.Where(p => p.Text.HasValue()).Count();

            var voteRateSum = reviews.Select(p => p.VoteRate.ToDecimal()).DefaultIfEmpty(0).Sum();

            model.SchemaRatingValue = voteRateSum > 0 ? (4.9m + (voteRateSum) / ratingCount) / 2 : 4.9m;
            model.SchemaRatingCount = ratingCount + 8376;
            model.SchemaReviewCount = ratingReviewCount + 8376;
            return View(model);
        }

        private async Task<List<ReviewViewModel>> PrepareExchangeOrdersViewModel(int exchangeTypeId)
        {
            var reviews = await _exchangeOrderService.TableNoTracking.Where(p => p.ExchangeId == exchangeTypeId)
                .Where(p => p.Review.Text.Length > 10)
                .Select(p => new ReviewViewModel()
                {
                    Id = p.Review.Id,
                    OrderUserFirstName = p.InsertUser.FirstName,
                    OrderUserLastName = p.InsertUser.LastName,
                    OrderUserUserName = p.InsertUser.UserName,
                    Text = p.Review.Text,
                    Visibility = p.Review.Visibility,
                    VoteRate = p.Review.VoteRate,
                    OrderExchangeFromCurrency = new CurrencyTypeViewModel()
                    {
                        Id = p.Exchange.FromCurrencyId,
                        UnitSign = p.Exchange.FromCurrency.UnitSign,
                        AlternativeName = p.Exchange.FromCurrency.AlternativeName,
                        Description = p.Exchange.FromCurrency.Description,
                        Disabled = p.Exchange.FromCurrency.Disabled,
                        Name = p.Exchange.FromCurrency.Name,
                        Type = p.Exchange.FromCurrency.Type,
                        Step = p.Exchange.FromCurrency.Step,
                        Url = p.Exchange.FromCurrency.Url,
                        IsFiat = p.Exchange.FromCurrency.IsFiat
                    },
                    OrderExchangeToCurrency = new CurrencyTypeViewModel()
                    {
                        Id = p.Exchange.ToCurrencyId,
                        UnitSign = p.Exchange.ToCurrency.UnitSign,
                        AlternativeName = p.Exchange.ToCurrency.AlternativeName,
                        Description = p.Exchange.ToCurrency.Description,
                        Disabled = p.Exchange.ToCurrency.Disabled,
                        Name = p.Exchange.ToCurrency.Name,
                        Type = p.Exchange.ToCurrency.Type,
                        Step = p.Exchange.ToCurrency.Step,
                        Url = p.Exchange.ToCurrency.Url,
                        IsFiat = p.Exchange.ToCurrency.IsFiat
                    }
                })
                .OrderByDescending(p => p.Id)
                .Take(20)
                .ToListAsync();

            return reviews;
        }

        private async Task<decimal> UpdateFromMaxAmountAsync(int userId, decimal fromMaxAmount)
        {
            if (userId != 0 && await _userService.IsBuyLimitedAsync(userId))
            {
                var shetabId = await _currencyTypeService.TableNoTracking.Where(p => p.Type == ECurrencyType.Shetab).Select(p => p.Id).FirstOrDefaultAsync();
                if (shetabId == FromId)
                {
                    // Update ToMaxAmount
                    var lastHour = DateTime.UtcNow.AddMinutes(-30);
                    var totalExchangeAmount = await _exchangeOrderService.TableNoTracking
                        .Where(p => p.UserId == userId && (p.InsertDate > lastHour || p.PaymentStatus == PaymentStatus.Paid))
                        .Select(p => p.PayAmount)
                        .DefaultIfEmpty(0)
                        .SumAsync();

                    fromMaxAmount = 2000000 - totalExchangeAmount;
                }
            }
            return fromMaxAmount;
        }

        public virtual async Task<ActionResult> NotAvailable(int fromId, int toId)
        {
            var model = await _exchangeTypeService.TableNoTracking
                .Where(p => p.FromCurrencyId == fromId && p.ToCurrencyId == toId)
                .ProjectToSingleAsync<ExchangeTypeViewModel>();

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(SubmitExchangeViewModel model)
        {
            var isFrom = model.isfromamount == 1 ? true : false;
            var amount = isFrom ? model.fromamount : model.toamount;
            SetExchangeSession(model.from, model.to, amount, isFrom);

            //var tetherId = _currencyTypeService.GetCachedByType(ECurrencyType.Tether);
            //if (model.from == tetherId || model.to == tetherId)
            //    return RedirectToAction("Tether");

            return RedirectToAction("WalletAddress");
        }

        [Authorize]
        public ActionResult Tether(int id = 0)
        {
            //if id == 1 ? is from tether else is to tether
            ViewBag.IsFromTether = id == 1;
            return View();
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult> WalletAddress()
        {
            var userId = User.Identity.GetUserId<int>();

            var model = await GetWalletAddressViewModel();

            if (model == null)
                return RedirectToAction("Index");

            if (model.ExchangeFiatCoinType == ExchangeFiatCoinType.CoinToCoin)
                return RedirectToAction("NotAvailable", new { fromId = model.FromCurrency.Id, toId = model.ToCurrency.Id });

            var isFromTether = model.FromCurrency.Type == ECurrencyType.Tether;

            if (model.FromCurrency.Type == ECurrencyType.Shetab)
            {
                var (IsVerified, IsFiatVerified) = await _userService.IsVerifiedAsync(userId);

                if (IsVerified && IsFiatVerified)
                {
                    if (model.ToCurrency.Type == ECurrencyType.PerfectMoneyVoucher)
                    {
                        if (isFromTether)
                            return RedirectToAction("Tether", new { id = 1 }); // IsFromTether

                        return RedirectToAction("Confirm");
                    }
                    return View(model);
                }

                else if (IsVerified && !IsFiatVerified)
                    return View("FiatNotVerified");

                return View("NotVerified");
            }
            else
            {
                if (model.ToCurrency.Type == ECurrencyType.Shetab)
                {
                    var fullName = await _userService.TableNoTracking
                        .Where(p => p.Id == userId).Select(p => p.FirstName + " " + p.LastName)
                        .SingleAsync();
                    ViewBag.FullName = fullName.Trim();
                }
                return View(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> WalletAddress(SubmitExchangeWalletAddressViewModel model)
        {
            var walletModel = await GetWalletAddressViewModel(model);
            if (walletModel == null)
                return RedirectToAction("Index");

            if (!ModelState.IsValid)
                return View(walletModel);

            var (fromId, toId, isFrom, amount, walletaddress, memo) = GetExchangeSession();
            if (toId == 0 || fromId == 0 || amount == 0)
                return RedirectToAction("Index");

            var toType = await _currencyTypeService.TableNoTracking
                .Where(p => p.Id == toId)
                .Select(p => p.Type)
                .SingleAsync();

            if (toType == ECurrencyType.Ripple && !model.Memo.HasValue())
            {
                var memoError = "Payment Id باید وارد شود.";
                ErrorNotification(memoError);
                ModelState.AddModelError("Memo", memoError);
                return View(await GetWalletAddressViewModel(model));
            }

            if (model.ShebaNumber.HasValue())
            {
                model.ShebaNumber = model.ShebaNumber.Replace(" ", "").Replace("-", "").Trim().ToUpper();
                if (model.ShebaNumber.Length == 24 && !model.ShebaNumber.Contains("IR"))
                    model.ShebaNumber = "IR" + model.ShebaNumber;

                if (model.ShebaNumber.Length != 26 || !model.ShebaNumber.Contains("IR"))
                {
                    var shebaError = "شماره شبا باید 24 عدد باشد همراه با IR جمعا 26 کاراکتر.";
                    ModelState.AddModelError("ShebaNumber", shebaError);
                    ErrorNotification(shebaError);
                    return View(await GetWalletAddressViewModel(model));
                }

                //var payIrSheba = await _payIRService.GetShebaDetailAsync(model.ShebaNumber);
                //var isShebaValid = payIrSheba?.Status == 1 && payIrSheba.Data != null && payIrSheba.Data.Status == "ACTIVE";

                //if (!isShebaValid)
                //{
                //    var shebaError = "شماره شبا اشتباه وارد شده است.";
                //    ModelState.AddModelError("ShebaNumber", shebaError);
                //    ErrorNotification(shebaError);
                //    return View(await GetWalletAddressViewModel(model));
                //}
            }

            HttpContext.Session["walletaddress"] = model.Walletaddress;
            if (model.Memo.HasValue())
                HttpContext.Session["memo"] = model.Memo;

            if (model.BankName.HasValue())
                HttpContext.Session["bankname"] = model.BankName;

            if (model.CardNumber.HasValue())
                HttpContext.Session["cardnumber"] = model.CardNumber;

            if (model.ShebaNumber.HasValue())
                HttpContext.Session["shebanumber"] = model.ShebaNumber;

            return RedirectToAction("Confirm");
        }

        [HttpGet]
        public async Task<ActionResult> Confirm()
        {
            var (fromId, toId, isFrom, amount, walletaddress, memo) = GetExchangeSession();
            if (toId == 0 || fromId == 0 || amount == 0)
                return RedirectToAction("Index");

            var (cardNumber, shebaNumber, bankName) = GetBankExchangeSession();

            var model = await _exchangeTypeService.TableNoTracking
                .Where(p => p.FromCurrencyId == fromId && p.ToCurrencyId == toId)
                .ProjectToSingleAsync<ExchangeConfirmViewModel>();

            var isFiatShetabValid = model.ToCurrencyType == ECurrencyType.Shetab && cardNumber.HasValue() && shebaNumber.HasValue() && bankName.HasValue();

            if (!isFiatShetabValid && !walletaddress.HasValue() && model.ToCurrencyType != ECurrencyType.PerfectMoneyVoucher)
                return RedirectToAction("WalletAddress");

            var userId = User.Identity.GetUserId<int>();
            var exchangeAmount = await _priceManager.GetExchangeAmountAsync(fromId, toId, amount, isFrom);
            if (model.FromCurrencyType == ECurrencyType.PerfectMoney)
                exchangeAmount.FromAmount = Convert.ToDecimal(exchangeAmount.FromAmount.ToBlockChainPrice(2));

            exchangeAmount.FromMaxAmount = await UpdateFromMaxAmountAsync(userId, exchangeAmount.FromMaxAmount);
            SetExchangeWithAmountSession(exchangeAmount.FromAmount, exchangeAmount.ToAmount);

            model.WalletAddress = walletaddress;
            model.FromAmount = exchangeAmount.FromAmount;
            model.IsFrom = isFrom;
            model.Memo = memo;
            model.ToAmount = exchangeAmount.ToAmount;
            model.CardNumber = cardNumber;
            model.ShebaNumber = shebaNumber;
            model.BankName = bankName;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Confirm(SubmitExchangeConfirmViewModel model)
        {
            var userId = User.Identity.GetUserId<int>();
            //UNDONE: Create new exchange order and redirect to exchangeorder controller and index(orderid) action;
            var (fromId, toId, amount, isFromAmount, fromAmount, toAmount, amountDateEnd, walletaddress, memo) = GetAllExchangeSession();
            var (cardNumber, shebaNumber, bankName) = GetBankExchangeSession();

            if (DateTime.UtcNow > amountDateEnd)
            {
                return RedirectToAction("Index");
            }

            var exchangeAmount = await _priceManager.GetExchangeAmountAsync(fromId, toId, amount, isFromAmount);
            exchangeAmount.FromMaxAmount = await UpdateFromMaxAmountAsync(userId, exchangeAmount.FromMaxAmount);

            var exchangeType = await _exchangeTypeService.TableNoTracking
                .Where(p => p.FromCurrencyId == fromId && p.ToCurrencyId == toId)
                .Select(p => new
                {
                    p.Id,
                    FromName = p.FromCurrency.Name,
                    ToName = p.ToCurrency.Name,
                    ToType = p.ToCurrency.Type,
                    FromSign = p.FromCurrency.UnitSign,
                    ToSign = p.ToCurrency.UnitSign,
                    p.ToCurrency.TransactionFee,
                    p.ExchangeFiatCoinType,
                    p.ExtraFeePercent
                })
                .SingleOrDefaultAsync();

            if (
                exchangeAmount.FromAmount >= exchangeAmount.FromMinAmount &&
                (exchangeAmount.ToMaxAmount == -1 || exchangeAmount.ToAmount <= exchangeAmount.ToMaxAmount) &&
                (exchangeAmount.FromMaxAmount == -1 || exchangeAmount.FromAmount <= exchangeAmount.FromMaxAmount) &&
                exchangeAmount.ToAmount >= exchangeAmount.ToMinAmount)
            {
                var isToPerfect = exchangeType.ToType.ToString().Contains("PerfectMoney");
                var estimatedReceiveAmount = GetToAmountMinusFees(exchangeAmount.ToAmount, exchangeType.ExchangeFiatCoinType, exchangeType.TransactionFee, isToPerfect);
                var receiveAmount = GetToAmountMinusFees(toAmount, exchangeType.ExchangeFiatCoinType, exchangeType.TransactionFee, isToPerfect);
                var toFee = toAmount - receiveAmount;
                var order = new ExchangeOrder()
                {
                    PayAmount = fromAmount, // + TxFee ?????,
                    PriceAmount = fromAmount,
                    ReceiveAddress = walletaddress.HasValue() ? walletaddress.Trim() : cardNumber + " " + bankName,
                    ReceiveMemoTagPaymentId = memo.HasValue() ? memo.Trim() : shebaNumber,
                    ExchangeId = exchangeType.Id,
                    EstimatedReceiveAmount = estimatedReceiveAmount,
                    ReceiveAmount = receiveAmount,
                    UserId = userId,
                    GatewayFee = toFee,
                    //New Items
                    ExpireDate = exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.CoinToCoin ? DateTime.UtcNow.AddDays(30) : exchangeType.FromName == "شتاب" || exchangeType.FromName == "پرفکت مانی" ? DateTime.UtcNow.AddMinutes(15) : DateTime.UtcNow.AddMinutes(30),
                    DollarPriceInToman = exchangeAmount.USDPriceInToman,
                    EuroPriceInToman = exchangeAmount.EURPriceInToman,
                    BTCPriceInUSD = exchangeAmount.BTCPriceInUSD,
                    BTCPriceInEUR = exchangeAmount.BTCPriceInEUR,
                    BTCValueOfOrder = exchangeAmount.BTCValueOfOrder,
                    //CurrencyPriceInDollar = ,
                    ExchangeExtraFeePercentInTime = exchangeType.ExtraFeePercent,
                    InsertUserIpAddress = IPAddress,
                    //TomanValueOfOrder = estimatedExchangeAmount.TomanValueOfOrder
                };
                await _exchangeOrderService.InsertAsync(order);

                //Prepare message email
                var user = await _userService.TableNoTracking
                    .Where(p => p.Id == userId)
                    .Select(p => new
                    {
                        p.Email,
                        p.PhoneNumber,
                        p.UserName,
                        FullName = p.FirstName + " " + p.LastName
                    })
                    .SingleAsync();

                var orderUrl = Url.Action("Index", "ExchangeOrder", new { orderGuid = order.Guid }, Request.Url.Scheme);
                var payAmount = exchangeType.FromSign == "تومان" ? fromAmount.ToPriceWithoutFloat() + " تومان" : fromAmount.ToBlockChainPrice() + " " + exchangeType.FromSign;
                var receiveAmountText = exchangeType.ToSign == "تومان" ? toAmount.ToPriceWithoutFloat() + " تومان" : toAmount.ToBlockChainPrice() + " " + exchangeType.ToSign;

                var newOrderEmail = new NewOrderEmail()
                {
                    ExchangeTime = "5 تا 30 دقیقه",
                    From = "no-reply@extoman.com",
                    To = user.Email,
                    FromCurrency = exchangeType.FromName,
                    ToCurrency = exchangeType.ToName,
                    Fullname = user.FullName,
                    OrderUrl = orderUrl,
                    PayAmount = payAmount,
                    ReceiveAmount = receiveAmountText,
                    Subject = "سفارش جدید ثبت گردید",
                    Username = user.UserName,
                    ViewName = "NewOrder",
                    TxFee = toFee + " " + exchangeType.ToSign
                };

                var newOrderEmailBody = newOrderEmail.GetMailMessage().Body;
                if (!AppSettingManager.IsLocale)
                    await _messageManager.NewOrderSubmitedAsync(newOrderEmailBody, user.Email, user.PhoneNumber, user.UserName, order.Guid.ToString(), exchangeType.FromName, exchangeType.ToName, payAmount, receiveAmountText);
                return RedirectToAction("Index", "ExchangeOrder", new { orderGuid = order.Guid });
            }
            new Exception($"Cannot go further. {exchangeAmount.Serialize()}").LogError();
            return RedirectToAction("Index");
        }

        private decimal GetToAmountMinusFees(decimal toAmount, ExchangeFiatCoinType exchangeFiatCoinType, decimal? transactionFee, bool isToPerfect)
        {
            if (transactionFee.HasValue && transactionFee > 0)
            {
                switch (exchangeFiatCoinType)
                {
                    case ExchangeFiatCoinType.CoinToCoin:
                    case ExchangeFiatCoinType.CoinToFiatToman:
                    case ExchangeFiatCoinType.FiatUSDToCoin:
                    case ExchangeFiatCoinType.FiatTomanToCoin:
                    case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman:
                    default:
                        return toAmount - transactionFee.Value;
                    case ExchangeFiatCoinType.CoinToFiatUSD:
                    case ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham:
                    case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham:
                        return toAmount - (toAmount * transactionFee.Value / 100M);
                }
            }
            return toAmount;
        }

        [HttpPost]
        public async Task<ActionResult> Rate(int fromId, int toId, decimal? amount, int isFrom = 1)
        {
            if (Request.IsAjaxRequest())
            {
                try
                {

                    if (amount.HasValue && amount.Value > 0)
                    {
                        var exchangeAmount = await _priceManager.GetExchangeAmountAsync(fromId, toId, amount.Value, isFrom == 1);
                        var userId = User.Identity.IsAuthenticated ? User.Identity.GetUserId<int>() : 0;
                        exchangeAmount.FromMaxAmount = await UpdateFromMaxAmountAsync(userId, exchangeAmount.FromMaxAmount);
                        return Json(exchangeAmount);
                    }
                    else
                    {
                        return Json(await _priceManager.GetExchangeRateAsync(fromId, toId));
                    }

                }
                catch (Exception ex)
                {
                    ex.LogError();
                    await _messageManager.TelegramAdminMessageAsync($"خطا در هنگام دریافت نرخ تبدیل {fromId} به {toId}. متن علت خطا: " + ex.Message);
                    throw ex;
                }
            }
            return HttpNotFound();
        }

        [HttpPost]
        public async Task<ActionResult> Balance(string unitSign)
        {
            if (Request.IsAjaxRequest())
            {
                if (unitSign.HasValue())
                    return Json(await _balanceManager.GetBalanceAsync(unitSign));
                else
                    return Json(await _balanceManager.GetAllBalancesAsync());
            }
            return HttpNotFound();
        }
        #endregion

        #region Helpers
        private async Task<ExchangeWalletAddressViewModel> GetWalletAddressViewModel(SubmitExchangeWalletAddressViewModel submitModel = null)
        {
            var userId = User.Identity.GetUserId<int>();
            var (fromId, toId, isFrom, amount, walletaddress, memo) = GetExchangeSession();
            if (toId == 0 || fromId == 0 || amount == 0)
                return null;

            var model = await _exchangeTypeService.TableNoTracking
                .Where(p => p.FromCurrencyId == fromId && p.ToCurrencyId == toId)
                .ProjectToSingleAsync<ExchangeWalletAddressViewModel>();

            if (model.Disabled)
                return null;

            if (submitModel != null)
            {
                ViewBag.BankName = submitModel.BankName;
                ViewBag.CardNumber = submitModel.CardNumber;
                ViewBag.ShebaNumber = submitModel.ShebaNumber;
            }
            return model;
        }
        private void SetExchangeSession(int from, int to, decimal amount, bool isfromamount)
        {
            HttpContext.Session["fromId"] = from;
            HttpContext.Session["toId"] = to;
            HttpContext.Session["isFromAmount"] = isfromamount ? 1 : 0;
            HttpContext.Session["amount"] = amount.Round();
        }
        private void SetExchangeWithAmountSession(decimal fromAmount, decimal toAmount)
        {
            HttpContext.Session["fromamount"] = fromAmount.Round();
            HttpContext.Session["toamount"] = toAmount.Round();
            HttpContext.Session["amountdateend"] = DateTime.UtcNow.AddMinutes(2).ToString();
        }
        private void ClearExchangeSession()
        {
            HttpContext.Session.Remove("fromId");
            HttpContext.Session.Remove("toId");
            HttpContext.Session.Remove("isFromAmount");
            HttpContext.Session.Remove("amount");
            HttpContext.Session.Remove("fromamount");
            HttpContext.Session.Remove("amountdateend");
            HttpContext.Session.Remove("walletaddress");
            HttpContext.Session.Remove("memo");
            HttpContext.Session.Remove("cardnumber");
            HttpContext.Session.Remove("shebanumber");
            HttpContext.Session.Remove("bankname");
        }
        private (int fromId, int toId, decimal fromAmount, decimal toAmount, DateTime amountDateEnd, string walletaddress, string memo) GetExchangeWithAmountSession()
        {
            var toId = HttpContext.Session["toId"].TryToInt();
            var fromId = HttpContext.Session["fromId"].TryToInt();
            var fromAmount = HttpContext.Session["fromamount"].TryToDecimal().Round();
            var toAmount = HttpContext.Session["toamount"].TryToDecimal().Round();
            var amountDateEnd = Convert.ToDateTime(HttpContext.Session["amountdateend"]);
            var walletaddress = HttpContext.Session["walletaddress"]?.ToString().Trim();
            var memo = HttpContext.Session["memo"]?.ToString().Trim();
            return (fromId, toId, fromAmount, toAmount, amountDateEnd, walletaddress, memo);
        }
        private (int fromId, int toId, decimal amount, bool isFromAmount, decimal fromAmount, decimal toAmount, DateTime amountDateEnd, string walletaddress, string memo) GetAllExchangeSession()
        {
            var toId = HttpContext.Session["toId"].TryToInt();
            var fromId = HttpContext.Session["fromId"].TryToInt();
            var amount = HttpContext.Session["amount"].TryToDecimal().Round();
            var isFromAmount = HttpContext.Session["isFromAmount"]?.ToString().ToBoolean() ?? false;
            var fromAmount = HttpContext.Session["fromamount"].TryToDecimal().Round();
            var toAmount = HttpContext.Session["toamount"].TryToDecimal().Round();
            var amountDateEnd = Convert.ToDateTime(HttpContext.Session["amountdateend"]);
            var walletaddress = HttpContext.Session["walletaddress"]?.ToString().Trim();
            var memo = HttpContext.Session["memo"]?.ToString().Trim();
            return (fromId, toId, amount, isFromAmount, fromAmount, toAmount, amountDateEnd, walletaddress, memo);
        }
        private (decimal? amount, bool isFrom) GetSegments(string segments)
        {
            decimal? amount = null;
            bool isFrom = false;
            if (segments.HasValue())
            {
                if (segments.Contains("/"))
                {
                    var amountSplit = segments.Split('/');
                    if (amountSplit.Count() > 1)
                    {
                        try
                        {
                            amount = amountSplit[0].ToDecimal();
                            isFrom = amountSplit[1].ToBoolean();
                        }
                        catch { }
                    }
                    else
                    {
                        try
                        {
                            amount = amountSplit[0].ToDecimal();
                        }
                        catch { }
                    }
                }
                else
                {
                    try
                    {
                        amount = segments.ToDecimal();
                    }
                    catch { }
                }
            }
            return (amount, isFrom);
        }
        private (string cardNumber, string shebaNumber, string bankName) GetBankExchangeSession()
        {
            var cardnumber = HttpContext.Session["cardnumber"]?.ToString().Trim();
            var shebanumber = HttpContext.Session["shebanumber"]?.ToString().Trim();
            var bankname = HttpContext.Session["bankname"]?.ToString().Trim();
            return (cardnumber, shebanumber, bankname);
        }
        private (int fromId, int toId, bool isFrom, decimal amount, string walletaddress, string memo) GetExchangeSession()
        {
            var toId = HttpContext.Session["toId"].TryToInt();
            var fromId = HttpContext.Session["fromId"].TryToInt();
            var isFromAmount = HttpContext.Session["isFromAmount"]?.ToString().ToBoolean() ?? false;
            var amount = HttpContext.Session["amount"].TryToDecimal().Round();
            var walletaddress = HttpContext.Session["walletaddress"]?.ToString().Trim();
            var memo = HttpContext.Session["memo"]?.ToString().Trim();
            return (fromId, toId, isFromAmount, amount, walletaddress, memo);
        }
        #endregion
    }
}