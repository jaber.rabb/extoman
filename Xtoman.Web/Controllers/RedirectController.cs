﻿using System.Web.Mvc;

namespace Xtoman.Web.Controllers
{
    public class RedirectController : Controller
    {
        public ActionResult OldUrl(string routeName, object routeValues)
        {
            return RedirectToRoute(routeName, routeValues);
        }
    }
}