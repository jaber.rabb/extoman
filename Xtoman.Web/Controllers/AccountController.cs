﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Xtoman.Domain;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Framework;
using Xtoman.Framework.Core;
using Xtoman.Framework.Mvc;
using Xtoman.Service;
using Xtoman.Utility;
using Xtoman.Utility.PostalEmail;
using Xtoman.Web.ViewModels;

namespace Xtoman.Web.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        private readonly IAppSignInManager _signInManager;
        private readonly IAppUserManager _userManager;
        private readonly IUserService _userService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly IUserNotificationService _userNotificationService;
        private readonly IOrderService _orderService;
        private readonly IMessageManager _messageManager;
        private readonly IMediaThumbnailService _mediaThumbnailService;
        private readonly IMediaService _mediaService;
        private readonly IMediaThumbnailSizeService _mediaThumbnailSizeService;
        private readonly IUserNotifyAlertService _userNotifyAlertService;
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly IUserVerificationService _userVerificationService;
        private readonly IUserBankCardService _userBankCardService;
        private readonly IVerificationManager _verificationManager;
        private readonly MessageSettings _messageSettings;

        public AccountController(
            IAppUserManager userManager,
            IAppSignInManager signInManager,
            IUserService userService,
            IEmailAccountService emailAccountService,
            IUserNotificationService userNotificationService,
            IOrderService orderService,
            IMessageManager messageManager,
            IMediaThumbnailService mediaThumbnailService,
            IMediaService mediaService,
            IMediaThumbnailSizeService mediaThumbnailSizeService,
            IUserNotifyAlertService userNotifyAlertService,
            IExchangeOrderService exchangeOrderService,
            IUserVerificationService userVerificationService,
            IUserBankCardService userBankCardService,
            IVerificationManager verificationManager,
            MessageSettings messageSettings)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _userService = userService;
            _emailAccountService = emailAccountService;
            _messageSettings = messageSettings;
            _userNotificationService = userNotificationService;
            _orderService = orderService;
            _mediaThumbnailService = mediaThumbnailService;
            _mediaThumbnailSizeService = mediaThumbnailSizeService;
            _mediaService = mediaService;
            _userNotifyAlertService = userNotifyAlertService;
            _exchangeOrderService = exchangeOrderService;
            _userVerificationService = userVerificationService;
            _userBankCardService = userBankCardService;
            _messageManager = messageManager;
            _verificationManager = verificationManager;
        }

        public virtual async Task<ActionResult> Index()
        {
            var userId = User.Identity.GetUserId<int>();

            var model = await _userService.TableNoTracking
                .Where(p => p.Id == userId)
                .ProjectToSingleAsync<AccountIndexViewModel>();

            if (Request.IsPjaxRequest())
                return PartialView(model);

            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> RedirectRefLink()
        {
            var referrerVId = HttpContext.Request.RawUrl.Replace("/", "");
            await AddReferrerAsync(referrerVId);
            return RedirectToAction("Register");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Edit(AccountIndexViewModel model)
        {
            var result = new ResultModel();
            if (ModelState.IsValid)
            {
                try
                {
                    var userId = User.Identity.GetUserId<int>();
                    var user = await _userService.Table
                        .Where(p => p.Id == userId)
                        .Include(p => p.UserBankCards)
                        .SingleAsync();

                    var modelFullName = model.FirstName + " " + model.LastName;
                    var emailChanged = !model.Email.Equals(user.Email, StringComparison.InvariantCultureIgnoreCase);
                    var mobileChanged = !model.PhoneNumber.Equals(user.PhoneNumber, StringComparison.InvariantCultureIgnoreCase);
                    var telephoneChanged = !model.Telephone.Equals(user.Telephone, StringComparison.InvariantCultureIgnoreCase);
                    var nameChanged = !modelFullName.Equals(user.FullName, StringComparison.InvariantCultureIgnoreCase);
                    var nationalCodeChanged = !model.NationalCode.Equals(user.NationalCode, StringComparison.InvariantCultureIgnoreCase);
                    var fatherNameChanged = !model.FatherName.Equals(user.FatherName, StringComparison.InvariantCultureIgnoreCase);
                    var emailFirstTimeInserted = !user.Email.HasValue() && model.Email.HasValue();
                    var mobileFirstTimeInserted = !user.PhoneNumber.HasValue() && model.PhoneNumber.HasValue();

                    var birthDateChanged = false;
                    if (model.BirthDate.HasValue && user.BirthDate.HasValue)
                    {
                        birthDateChanged = model.BirthDate.Value.Year != user.BirthDate.Value.Year || model.BirthDate.Value.Month != user.BirthDate.Value.Month || model.BirthDate.Value.Day != user.BirthDate.Value.Day;
                    }
                    else if (model.BirthDate.HasValue)
                    {
                        birthDateChanged = true;
                    }

                    if (emailChanged && model.Email.HasValue())
                        if (await _userService.EmailIsUniqueAsync(model.Email, user.Id))
                        {
                            if (user.UserName == user.Email)
                                user.UserName = model.Email;
                            user.Email = model.Email;
                            user.EmailConfirmed = false;
                        }
                        else
                            result.Text = "ایمیل وارد شده تکراری است. ";

                    if (mobileChanged && model.PhoneNumber.HasValue())
                        if (await _userService.PhoneIsUniqueAsync(model.PhoneNumber, user.Id))
                        {
                            if (user.UserName == user.PhoneNumber)
                                user.UserName = model.PhoneNumber;
                            user.PhoneNumber = model.PhoneNumber;
                            user.PhoneNumberConfirmed = false;
                        }
                        else
                            result.Text += "موبایل وارد شده تکراری است.";

                    if (telephoneChanged && model.Telephone.HasValue())
                    {
                        user.TelephoneConfirmationStatus = VerificationStatus.Pending;
                        user.Telephone = model.Telephone;
                        var telMsg = $"درخواست تایید تلفن ثابت برای شماره {user.Telephone} به نام {user.FullName} ثبت شد";
                        await _messageManager.TelegramAdminMessageAsync(telMsg);
                        await _userVerificationService.ChangedAsync(user.Id, UserVerificationType.Telephone, VerificationStatus.Pending, telMsg);
                    }

                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.FatherName = model.FatherName;
                    user.NationalCode = model.NationalCode;
                    user.Gender = model.Gender;
                    if (model.BirthDate.HasValue)
                        user.BirthDate = model.BirthDate;

                    if (nameChanged && user.FirstName.HasValue() && user.LastName.HasValue())
                    {
                        var userClaims = await _userManager.GetClaimsAsync(userId);
                        if (userClaims.Any(x => x.Type == "FullName"))
                        {
                            var fullNameClaim = userClaims.FirstOrDefault(x => x.Type == "FullName");
                            if (fullNameClaim != null)
                                await _userManager.RemoveClaimAsync(userId, fullNameClaim);
                        }
                        await _userManager.AddClaimAsync(userId, new Claim("FullName", user.FirstName + " " + user.LastName));
                    }

                    await _userService.UpdateAsync(user);

                    if (nameChanged || fatherNameChanged || nationalCodeChanged || birthDateChanged)
                    {
                        user.IdentityVerificationStatus = ChangeVerificationStatus(user);
                        //user.FinnotechHistoryId = null;
                        if (user.IdentityVerificationStatus == VerificationStatus.Pending)
                        {
                            var msg = $"نام یا اطلاعات شخصی کاربر {user.UserName} تغییر کرده و مدارکش مجددا در وضعیت بررسی قرار گرفت.";
                            await _userVerificationService.ChangedAsync(userId, UserVerificationType.Identity, user.IdentityVerificationStatus, msg);
                            await _messageManager.TelegramAdminMessageAsync(msg);
                            if (nameChanged && user.UserBankCards.Any(x => x.VerificationStatus == VerificationStatus.Confirmed))
                            {
                                var userVerifiedBankCards = await _userBankCardService.Table
                                    .Where(p => p.UserId == user.Id && p.VerificationStatus == VerificationStatus.Confirmed)
                                    .ToListAsync();
                                foreach (var item in userVerifiedBankCards)
                                    item.VerificationStatus = VerificationStatus.Pending;
                                await _userBankCardService.UpdateAsync(userVerifiedBankCards);
                                msg = $"کارت های تایید شده با نام {user.FullName} به دلیل تغییر نام کاربر به وضعیت بررسی تغییر پیدا کردند. ";
                                await _userVerificationService.ChangedAsync(userId, UserVerificationType.BankCard, user.IdentityVerificationStatus, msg);
                                await _messageManager.TelegramAdminMessageAsync(msg);
                            }
                        }
                        await _userService.UpdateAsync(user);
                    }

                    await PrepareAndSendEmailAndPhoneChangedMessageAsync(emailFirstTimeInserted, emailChanged, mobileFirstTimeInserted, mobileChanged, user);
                }
                catch (Exception ex)
                {
                    ex.LogError();
                    result.Text += "خطایی در سرور رخ داد.";
                }

                if (!result.Text.HasValue())
                    result = new ResultModel() { Status = true, Text = "اطلاعات شخصی شما با موفقیت ویرایش شد." };
                else
                    result.Text += "بخشی از اطلاعات ذخیره شد.";
            }
            else
            {
                result.Text = ModelState.GetErrors();
            }
            if (Request.IsAjaxRequest())
                return Json(result);
            else
            {
                if (result.Status)
                    SuccessNotification(result.Text);
                else
                    ErrorNotification(result.Text);
                return RedirectToAction("Index");
            }
        }

        private VerificationStatus ChangeVerificationStatus(AppUser user)
        {
            if (user.IdentityVerificationStatus != VerificationStatus.Pending && user.IdentityVerificationStatus == VerificationStatus.Confirmed)
                return VerificationStatus.Pending;
            return user.IdentityVerificationStatus;
        }

        public virtual async Task<ActionResult> Verify()
        {
            var userId = User.Identity.GetUserId<int>();
            var user = await _userService.TableNoTracking.Where(p => p.Id == userId)
                .Select(p => new
                {
                    p.FirstName,
                    p.LastName,
                    p.Email,
                    p.PhoneNumber,
                    p.Gender,
                    p.EmailConfirmed,
                    p.PhoneNumberConfirmed,
                    p.UserBankCards,
                    p.IdentityVerificationStatus,
                    p.IdentityVerificationDescription,
                    p.Telephone,
                    p.TelephoneConfirmationStatus
                })
                .SingleAsync();
            var informationCompleted = user.FirstName.HasValue() && user.LastName.HasValue() && user.Email.HasValue() && user.PhoneNumber.HasValue() && user.Gender.HasValue && user.Telephone.HasValue();
            var identityCompleted = user.IdentityVerificationStatus == VerificationStatus.Confirmed;
            var identityText = user.IdentityVerificationStatus == VerificationStatus.NotConfirmed ? user.IdentityVerificationDescription : user.IdentityVerificationStatus.ToDisplay();
            var bankCardCompleted = user.UserBankCards.Any(x => x.VerificationStatus == VerificationStatus.Confirmed);
            var bankCardText = bankCardCompleted ? "" : user.UserBankCards.Any() ? "هیچ کارتی هنوز تایید نشده" : "هیچ کارتی هنوز اضافه نشده";
            var telephoneConfirmed = user.Telephone.HasValue() ? user.TelephoneConfirmationStatus == VerificationStatus.Confirmed : new bool?();
            //Model
            var model = new VerifyViewModel
            {
                InformationCompleted = informationCompleted,
                EmailConfirmCompleted = user.EmailConfirmed,
                PhoneNumberConfirmCompleted = user.PhoneNumberConfirmed,
                TelephoneConfirmCompleted = telephoneConfirmed,
                IdentityCompleted = identityCompleted,
                IdentityStatus = user.IdentityVerificationStatus,
                IdentityText = identityText,
                BankCardCompleted = bankCardCompleted,
                BankCardText = bankCardText
            };

            if (Request.IsPjaxRequest())
                return PartialView(model);

            return View(model);
        }

        public virtual async Task<ActionResult> BankCards()
        {
            var userId = User.Identity.GetUserId<int>();

            var model = await _userBankCardService.TableNoTracking
                .Where(p => p.UserId == userId)
                .ProjectToListAsync<UserBankCardViewModel>();

            if (Request.IsPjaxRequest())
                return PartialView(model);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> BankCards(AddBankCardViewModel model)
        {
            if (Request.IsAjaxRequest())
            {
                var userId = User.Identity.GetUserId<int>();
                if (ModelState.IsValid)
                {
                    if (await _userBankCardService.IsUniqueAsync(userId, model.CardNumber))
                    {
                        var bankCard = new UserBankCard()
                        {
                            UserId = userId,
                            CardNumber = model.CardNumber
                        };
                        await _userBankCardService.InsertAsync(bankCard);
                        var result = new AddResultModel()
                        {
                            Id = bankCard.Id,
                            Status = true,
                            Text = "شماره کارت بانکی جدید افزوده شد."
                        };
                        var user = await _userService.TableNoTracking.Where(p => p.Id == userId).Select(p => new { FullName = p.FirstName + " " + p.LastName, p.Email, p.PhoneNumber, p.UserName }).SingleAsync();
                        var fullName = user.FullName.Trim();
                        if (fullName.HasValue())
                        {
                            try
                            {
                                var autoValidationResult = await _verificationManager.AutoCardValidationAsync(bankCard.Id);
                                if (autoValidationResult)
                                {
                                    var verifyRespondEmail = new VerifyXEmail()
                                    {
                                        From = "no-reply@extoman.com",
                                        Fullname = fullName,
                                        Subject = $"تایید شماره کارت {bankCard.CardNumber}",
                                        To = user.Email,
                                        Username = user.UserName,
                                        VerifyAccepted = true,
                                        VerifyDescription = "",
                                        ViewName = "VerifyBankCard",
                                        Value = bankCard.CardNumber
                                    };
                                    var verifyRespondEmailBody = verifyRespondEmail.GetMailMessage().Body;
                                    await _messageManager.BankCardVerifiedAsync(verifyRespondEmailBody, fullName, bankCard.CardNumber, user.PhoneNumber, user.Email, "", true);
                                    var msg = $"شماره کارت {model.CardNumber} برای کاربر با نام {fullName} اضافه شد و به صورت اتوماتیک تایید شد.";
                                    await _userVerificationService.ChangedAsync(userId, UserVerificationType.BankCard, VerificationStatus.Confirmed, msg);
                                    await _messageManager.TelegramAdminMessageAsync(msg);
                                }
                                else
                                {
                                    var msg = $"شماره کارت {model.CardNumber} به نام {fullName} جهت تایید اضافه گردید.";
                                    await _userVerificationService.ChangedAsync(userId, UserVerificationType.BankCard, VerificationStatus.Confirmed, msg);
                                    await _messageManager.TelegramAdminMessageAsync(msg);
                                }
                            }
                            catch (Exception ex)
                            {
                                ex.LogError();
                            }
                        }
                        return Json(result);
                    }
                    else
                    {
                        return Json(new AddResultModel()
                        {
                            Text = "شماره کارت وارد شده تکراری است."
                        });
                    }
                }
                else
                {
                    return Json(new AddResultModel()
                    {
                        Text = model.CardNumber.HasValue() ? "شماره کارت وارد شده اشتباه است." : "شماره کارتی وارد نشده است."
                    });
                }
            }
            return HttpNotFound();
        }

        [AjaxOnly]
        [HttpPost]
        public virtual async Task<ActionResult> RemoveBankCard(int id)
        {
            if (id > 0)
            {
                try
                {
                    await _userBankCardService.DeleteAsync(id);
                    return Json(new ResultModel() { Status = true, Text = "کارت حذف شد." });
                }
                catch (Exception ex)
                {
                    ex.LogError();
                    return Json(new ResultModel() { Text = "خطایی هنگام حذف کارت رخ داد." });
                }
            }
            return Json(new ResultModel() { Text = "کارت درست انتخاب نشده است." });
        }

        [AjaxOnly]
        [HttpPost]
        public virtual async Task<ActionResult> SendConfirmEmail()
        {
            var result = new ResultModel();
            try
            {
                if (Session["EmailCodeTimeEnd"] == null)
                {
                    var userId = User.Identity.GetUserId<int>();
                    var user = await _userService.TableNoTracking.Where(p => p.Id == userId).SingleAsync();
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(userId);
                    await PrepareAndSendConfirmEmailAsync(user, user.Email);
                    Session["EmailCodeTimeEnd"] = DateTime.UtcNow.AddMinutes(5);
                    result.Status = true;
                    result.Text = "کد ارسال شد.";
                }
                else
                {
                    result.Text = "کد قبلی هنوز معتبر است.";
                }
            }
            catch (Exception)
            {
                result.Text = "ایمیل وارد نشده است";
            }
            return Json(result);
        }

        [AjaxOnly]
        [HttpPost]
        public virtual async Task<ActionResult> SendConfirmSMS()
        {
            var result = new ResultModel();
            try
            {
                if (Session["SMSCodeTimeEnd"] == null)
                {
                    var userId = User.Identity.GetUserId<int>();
                    var phoneNumber = await _userService.TableNoTracking.Where(p => p.Id == userId).Select(p => p.PhoneNumber).SingleAsync();
                    var code = await _userManager.GenerateChangePhoneNumberTokenAsync(userId, phoneNumber);
                    await _messageManager.PhoneNumberConfirmSMSAsync(code, phoneNumber);
                    Session["SMSCodeTimeEnd"] = DateTime.UtcNow.AddMinutes(5);
                    result.Status = true;
                    result.Text = "کد ارسال شد.";
                }
                else
                {
                    result.Text = "کد قبلی هنوز معتبر است.";
                }
            }
            catch (Exception)
            {
                result.Text = "شماره موبایل وارد نشده است";
            }
            return Json(result);
        }

        [AjaxOnly]
        [HttpPost]
        public virtual async Task<ActionResult> ConfirmMobile(string Code)
        {
            var result = new ResultModel() { Text = "کد وارد شده اشتباه است." };
            if (Code.HasValue() && Code.IsDigit())
            {
                var userId = User.Identity.GetUserId<int>();
                var phoneNumber = await _userService.TableNoTracking.Where(p => p.Id == userId).Select(p => p.PhoneNumber).SingleAsync();
                var confirmed = await _userManager.VerifyChangePhoneNumberTokenAsync(userId, Code, phoneNumber);
                result.Status = confirmed;
                if (confirmed)
                {
                    //var user = await _userService.Table.SingleAsync(p => p.Id == userId);
                    //user.PhoneNumberConfirmed = true;
                    //await _userService.UpdateAsync(user);
                    result.Text = "شماره موبایل تایید شد.";
                }
            }
            return Json(result);
        }

        public virtual async Task<ActionResult> Verification()
        {
            var userId = User.Identity.GetUserId<int>();

            var model = await _userService.TableNoTracking
                .Where(p => p.Id == userId)
                .ProjectToSingleAsync<AccountVerificationViewModel>();

            if (Request.IsPjaxRequest())
                return PartialView(model);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Verification(HttpPostedFileBase verifyfile)
        {
            if (Request.IsAjaxRequest())
            {
                var result = new UploadResultModel();
                if (!AppSettingManager.IsLocale)
                {
                    var captchaResult = await HttpContext.ValidateCaptchaAsync("6LflMzoUAAAAACuYcwEmXd1jTfby3YKO5CDhC_h9");
                    if (captchaResult != RecaptchaResponse.Success)
                    {
                        result.Text = "تایید کنید ربات نیستید!";
                        return Json(result);
                    }
                }
                if (verifyfile != null)
                {
                    var mediaType = verifyfile.GetFileMediaType();
                    var fileName = verifyfile.GetFileName($"static/verify/{mediaType.ToDisplay()}");
                    if (verifyfile.ContentLength == 0)
                    {
                        result.Text = "فایل ارسال شده خالی است.";
                    }
                    else if (verifyfile.ContentLength / 1024 / 1024 > 4)
                    {
                        result.Text = "فایل ارسال شده بزرگتر از 4 مگابایت است.";
                    }
                    else if (verifyfile.IsValid(new string[] { "image" }) && mediaType == MediaType.Image)
                    {
                        verifyfile.SaveAs(fileName.AbsolutePathForSave); // must be unique
                        var name = verifyfile.GetPureFileName();
                        var userFullName = User.GetFullName();
                        if (!userFullName.HasValue()) userFullName = User.Identity.GetUserName();
                        var media = new Media()
                        {
                            Alt = "تصویر مدارک " + userFullName,
                            Name = name,
                            Url = fileName.RelativePathForWeb,
                            Type = mediaType
                        };

                        var mediaThumbnailSizes = await _mediaThumbnailSizeService.GetAllCachedAsync();
                        //mediaThumbnailSizes.ForEach((thumbSize) =>
                        var thumbSize = mediaThumbnailSizes.Where(p => p.Name == "verify.large").FirstOrDefault();
                        PrepareMediaThumbnail(media, mediaType, fileName.AbsolutePathForSave, verifyfile, thumbSize);

                        //PrepareMediaCategory(media);

                        await _mediaService.InsertAsync(media);
                        if (media.Id != 0)
                        {
                            result = new UploadResultModel()
                            {
                                Status = true,
                                Text = "فایل مدارک با موفقیت ارسال شد.",
                                Url = fileName.RelativePathForWeb,
                                MediaId = media.Id
                            };
                        }
                        // If media for other content create a switch for add content media
                        var userId = User.Identity.GetUserId<int>();
                        var user = await _userService.Table.SingleAsync(p => p.Id == userId);
                        await AddUserVerifyMediaAsync(user, media.Id);

                        var verificationEmail = new SendingEmailModel()
                        {
                            From = "no-reply@extoman.com",
                            Fullname = userFullName,
                            Subject = $"درخواست احراز شما ثبت گردید {userFullName}",
                            To = user.Email,
                            Username = user.UserName,
                            ViewName = "Verification"
                        };

                        var verificationEmailBody = verificationEmail.GetMailMessage().Body;
                        await _messageManager.UserVerifyRequestAsync(verificationEmailBody, user.Email, user.PhoneNumber, userFullName, media.Url);
                    }
                    else
                    {
                        result.Text = "فایل مدارک باید تصویر باشد.";
                    }
                }
                else
                {
                    result.Text = "فایل تصویر درست ارسال نشده.";
                }
                if (Request.IsAjaxRequest())
                    return Json(result);
                else
                {
                    if (result.Status)
                        SuccessNotification(result.Text);
                    else
                        ErrorNotification(result.Text);
                    return RedirectToAction("Verification");
                }
            }
            return HttpNotFound();
        }

        [HttpPost]
        public virtual async Task<ActionResult> VerificationReset()
        {
            try
            {
                var userId = User.Identity.GetUserId<int>();
                var user = await _userService.Table.SingleAsync(p => p.Id == userId);
                user.IdentityVerificationStatus = VerificationStatus.NotSent;
                user.IdentityVerificationVerifyDate = new DateTime?();
                user.VerificationMediaId = new int?();
                user.IdentityVerificationDescription = null;
                await _userService.UpdateAsync(user);
                return Json(new ResultModel() { Status = true, Text = "مدارک ارسالی قبلی شما حذف شد." });
            }
            catch (Exception ex)
            {
                ex.LogError();
                return Json(new ResultModel() { Status = false, Text = "خطایی رخ داد!" });
            }
        }

        [HttpGet]
        public virtual ActionResult Settings()
        {
            if (Request.IsPjaxRequest())
                return PartialView();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Settings(AccountSettingsViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var result = await _userManager.ChangePasswordAsync(User.Identity.GetUserId<int>(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await _userManager.FindByIdAsync(User.Identity.GetUserId<int>());
                if (user != null)
                {
                    await _signInManager.SignInAsync(user, false, false);
                }
                SuccessNotification("کلمه عبور شما تغییر یافت.");
            }
            else
            {
                AddErrors(result);
                if (Request.IsPjaxRequest())
                    return PartialView(model);

                return View(model);
            }

            if (Request.IsPjaxRequest())
                return PartialView();

            return View();
        }

        public virtual async Task<ActionResult> Exchanges(int? page = 1, int? psize = 10)
        {
            var userId = User.Identity.GetUserId<int>();

            var model = await _exchangeOrderService.TableNoTracking
                .Where(p => p.UserId == userId)
                .ProjectToPagedListAsync<ExchangeOrderSimpleViewModel>(page.Value, psize.Value);

            //var model = await _userService.TableNoTracking
            //    .Include(p => p.Orders)
            //    .Where(p => p.Id == userId)
            //    .ProjectToSingleAsync<AccountExchangesViewModel>();

            if (Request.IsPjaxRequest())
                return PartialView(model);

            return View(model);
        }

        public virtual async Task<ActionResult> Notifies()
        {
            var userId = User.Identity.GetUserId<int>();

            var model = await _userService.TableNoTracking
                .Where(p => p.Id == userId)
                .ProjectToSingleAsync<AccountNotifyAlertsViewModel>();

            if (Request.IsPjaxRequest())
                return PartialView(model);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> AddNotifyAlert(AddNotifyAlertViewModel model)
        {
            var result = new AddResultModel();
            var userId = User.Identity.GetUserId<int>();

            var entity = model.ToEntity();
            entity.UserId = userId;
            try
            {
                await _userNotifyAlertService.InsertAsync(entity);
                result = new AddResultModel()
                {
                    Id = entity.Id,
                    Status = true,
                    Text = "اعلان جدید با موفقیت اضافه شد."
                };
            }
            catch (Exception)
            {
                result.Text = "خطایی رخ داد!";
            }

            return Json(result);
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
                if (returnUrl.HasValue())
                    return RedirectToLocal(returnUrl);
                else
                    return RedirectToAction("Index", "Home");

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!AppSettingManager.IsLocale)
            {
                var captchaResult = await HttpContext.ValidateCaptchaAsync("6LflMzoUAAAAACuYcwEmXd1jTfby3YKO5CDhC_h9");
                if (captchaResult != RecaptchaResponse.Success)
                {
                    ModelState.AddModelError("", "شما ربات هستید.");
                    return View(model);
                }
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var signInModel = new SignInModel()
            {
                EmailOrPhone = model.EmailOrPhoneNumberOrUsername,
                Password = model.Password,
                IsPersistent = model.RememberMe,
                ShouldLockout = true,
                IPAddress = IPAddress,
                Browser = Browser,
                Subdomain = SubdomainType.Web
            };
            var result = await _signInManager.EmailOrPhoneSignInAsync(signInModel);

            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("VerifyCode", new { Provider = "GoogleAuthenticator", ReturnUrl = returnUrl, model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "کلمه عبور، ایمیل یا موبایل وارد شده اشتباه است.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await _signInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await _signInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register(string returnUrl)
        {
            if (Request.IsAuthenticated)
                return RedirectToAction("Index");
            //return RedirectToAction("Login");
            ViewBag.ReturnUrl = returnUrl;
            ViewBag.ReferredBy = Referrer;
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (!AppSettingManager.IsLocale)
                {
                    var captchaResult = await HttpContext.ValidateCaptchaAsync("6LflMzoUAAAAACuYcwEmXd1jTfby3YKO5CDhC_h9");
                    if (captchaResult != RecaptchaResponse.Success)
                    {
                        ModelState.AddModelError("", "شما ربات هستید.");
                        return View(model);
                    }
                }

                if (await _userService.EmailOrPhoneIsUniqueAsync(model.EmailOrPhone))
                {
                    var user = new AppUser
                    {
                        UserName = model.EmailOrPhone,
                        Email = model.EmailOrPhone.IsEmail() ? model.EmailOrPhone : null,
                        ReferrerId = ReferrerId,
                        PhoneNumber = model.EmailOrPhone.IsEmail() ? null : model.EmailOrPhone
                    };
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        await PrepareAndSendRegisterMessageAsync(user, model.EmailOrPhone);
                        return RedirectToAction("Index", "Home");
                    }
                    AddErrors(result);
                }
                else
                {
                    ModelState.AddModelError("", "این ایمیل یا موبایل قبلا به ثبت رسیده است.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(int userId, string code)
        {
            if (userId == 0 || code == null)
            {
                return View("Error");
            }
            var decodedCode = HttpUtility.UrlDecode(code);
            var result = await _userManager.ConfirmEmailAsync(userId, decodedCode);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ConfirmPhoneNumber(string PhoneNumber)
        {
            if (!PhoneNumber.IsDigit())
                PhoneNumber = null;
            var model = new ConfirmPhoneNumberViewModel()
            {
                PhoneNumber = PhoneNumber
            };
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ConfirmPhoneNumber(ConfirmPhoneNumberViewModel model)
        {
            if (ModelState.IsValid)
            {
                if ((model.UserId == null || model.UserId == 0) && model.PhoneNumber.HasValue())
                    model.UserId = await _userService.TableNoTracking.Where(p => p.PhoneNumber == model.PhoneNumber).Select(p => p.Id).FirstOrDefaultAsync();
                if (model.UserId == null || model.UserId == 0)
                {
                    ModelState.AddModelError("PhoneNumber", "شماره موبایل وارد نشده است.");
                    return View(model);
                }

                var verifyResult = await _userManager.VerifyChangePhoneNumberTokenAsync(model.UserId.Value, model.Code, model.PhoneNumber);
                if (verifyResult)
                {
                    return View("PhoneConfirm");
                }
                else
                {
                    ModelState.AddModelError("Code", "کد وارد شده اشتباه است");
                }
            }
            return View(model);
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!AppSettingManager.IsLocale)
                {
                    var captchaResult = await HttpContext.ValidateCaptchaAsync("6LflMzoUAAAAACuYcwEmXd1jTfby3YKO5CDhC_h9");
                    if (captchaResult != RecaptchaResponse.Success)
                    {
                        ModelState.AddModelError("", "شما ربات هستید.");
                        return View(model);
                    }
                }

                if (!model.EmailOrPhone.HasValue())
                    return HttpNotFound();

                ViewBag.Type = model.EmailOrPhone.IsEmail() ? "ایمیل" : "پیامک";
                var user = await _userManager.FindByEmailOrPhoneAsync(model.EmailOrPhone);
                //var emailConfirmed = await _userManager.IsEmailConfirmedAsync(user.Id);
                //var phoneConfirmed = await _userManager.IsPhoneNumberConfirmedAsync(user.Id);

                if (user == null)
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");

                var code = model.EmailOrPhone.IsEmail() ?
                    await _userManager.GeneratePasswordResetTokenAsync(user.Id) :
                    await _userManager.GenerateSMSPasswordResetToken(user.Id);
                var confirmUrl = Url.Action("ResetPassword", "Account", new { code }, protocol: Request.Url.Scheme);

                if (model.EmailOrPhone.IsMobile())
                    confirmUrl = HttpUtility.UrlEncode(confirmUrl);
                var fullName = (user.FirstName + " " + user.LastName);
                if (!fullName.HasValue())
                    fullName = user.UserName;

                var confirmEmail = new ConfirmEmail()
                {
                    ConfirmCode = code,
                    ConfirmUrl = confirmUrl,
                    Fullname = fullName,
                    Username = user.UserName,
                    ViewName = "ForgotPassword",
                    To = model.EmailOrPhone,
                    From = "no-reply@extoman.com",
                    Subject = "فراموشی کلمه عبور"
                };
                var confirmEmailMessage = confirmEmail.GetMailMessage().Body;
                await _messageManager.ForgotPasswordAsync(confirmEmailMessage, model.EmailOrPhone, user.UserName, fullName, confirmUrl, code);
                if (model.EmailOrPhone.IsMobile())
                    return RedirectToAction("ResetPasswordWithoutCode");
                return View("ForgotPasswordConfirmation");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPasswordWithoutCode()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            if (!code.HasValue())
                return View("ResetPasswordWithoutCode");
            //code = HttpUtility.UrlDecode(code);
            var model = new ResetPasswordViewModel()
            {
                Code = code
            };
            return View(model);
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailOrPhoneAsync(model.EmailOrPhone);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = model.EmailOrPhone.IsEmail() ?
                await _userManager.ResetPasswordAsync(user.Id, model.Code, model.Password) :
                await _userManager.SMSPasswordResetAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await _signInManager.GetVerifiedUserIdAsync();
            if (userId == 0)
            {
                return View("Error");
            }
            var userFactors = await _userManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await _signInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, model.ReturnUrl, model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await _signInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }

                var isEmail = model.Email;
                var user = new AppUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    EmailConfirmed = true
                };
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public virtual async Task<ActionResult> EmailOrPhoneIsUnique(string EmailOrPhone)
        {
            if (Request.IsAjaxRequest())
                return Json(await _userService.EmailOrPhoneIsUniqueAsync(EmailOrPhone));

            return HttpNotFound();
        }

        #region Helpers
        private async Task AddReferrerAsync(string r)
        {
            r = r.ToUpper();
            if (!r.HasValue() && Referrer.HasValue() && await _userService.ReferrerExistAsync(Referrer))
                r = Referrer;
            else
            {
                if (await _userService.ReferrerExistAsync(r))
                    Referrer = r;
                else
                    r = null;
            }
            ViewBag.ReferredBy = r;
        }

        private async Task AddUserVerifyMediaAsync(AppUser user, int mediaId)
        {
            if (user != null && user.Id != 0 && mediaId != 0)
            {
                user.VerificationMediaId = mediaId;
                user.IdentityVerificationStatus = VerificationStatus.Pending;
                await _userService.UpdateAsync(user);
            }
        }

        private void PrepareMediaThumbnail(Media media, MediaType mediaType, string originalFilePath, HttpPostedFileBase verifyfile, MediaThumbnailSize thumbSize)
        {
            var thumbName = $"{thumbSize.Width}x{thumbSize.Height}";
            var fileName = verifyfile.GetFileName($"static/verify/{mediaType.ToDisplay()}/thumbs/", thumbName);
            var saveToFilePath = fileName.AbsolutePathForSave;
            switch (mediaType)
            {
                case MediaType.Image:
                    verifyfile.SaveAs(saveToFilePath);
                    ImageMagickHelper.ResizeCrop(originalFilePath, thumbSize.Width, thumbSize.Height, saveToFilePath, thumbSize.Crop);
                    media.Thumbnails.Add(new MediaThumbnail()
                    {
                        Width = thumbSize.Width,
                        Height = thumbSize.Height,
                        MediaThumbnailSizeId = thumbSize.Id,
                        Url = fileName.RelativePathForWeb
                    });
                    break;
                case MediaType.Video:
                    break;
            }
        }

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "dkjsad178ehidnDSAhjg1dDAS1dhuks";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, int? userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId ?? 0;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public int UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != 0)
                {
                    properties.Dictionary[XsrfKey] = UserId.ToString();
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

        #region Helpers
        private async Task PrepareAndSendConfirmEmailAsync(AppUser user, string emailAddress)
        {
            var emailCode = "";
            if (emailAddress.HasValue())
            {
                emailCode = await _userManager.GenerateEmailConfirmationTokenAsync(user.Id);
                emailCode = HttpUtility.UrlEncode(emailCode);
            }
            var confirmEmailUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = emailCode }, protocol: Request.Url.Scheme);
            var fullName = (user.FirstName + " " + user.LastName);
            if (!fullName.HasValue())
                fullName = user.UserName;
            var confirmEmail = new ConfirmEmail()
            {
                ConfirmCode = emailCode,
                ConfirmUrl = confirmEmailUrl,
                Fullname = fullName,
                Username = user.UserName,
                ViewName = "EmailConfirm",
                To = emailAddress,
                From = "no-reply@extoman.com",
                Subject = $"تایید آدرس ایمیل {fullName}"
            };
            var confirmEmailBody = confirmEmail.GetMailMessage().Body.Replace("&amp;", "&");
            await _messageManager.EmailConfirmAsync(confirmEmailBody, emailAddress, fullName);
        }

        private async Task PrepareAndSendRegisterMessageAsync(AppUser user, string emailOrPhone)
        {
            var emailCode = "";
            var phoneCode = "";
            await _signInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
            if (user.Email.HasValue())
            {
                emailCode = await _userManager.GenerateEmailConfirmationTokenAsync(user.Id);
                emailCode = HttpUtility.UrlEncode(emailCode);
            }
            if (user.PhoneNumber.HasValue())
                phoneCode = await _userManager.GenerateChangePhoneNumberTokenAsync(user.Id, user.PhoneNumber);
            var confirmEmailUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = emailCode }, protocol: Request.Url.Scheme);
            var confirmPhoneNumberUrl = Url.Action("ConfirmPhoneNumber", "Account", new { phonenumber = user.PhoneNumber }, protocol: Request.Url.Scheme);
            var fullName = (user.FirstName + " " + user.LastName);
            if (!fullName.HasValue())
                fullName = user.UserName;
            var registerEmail = new ConfirmEmail()
            {
                ConfirmCode = emailCode,
                ConfirmUrl = confirmEmailUrl,
                Fullname = fullName,
                Username = user.UserName,
                ViewName = "Register",
                To = emailOrPhone,
                From = "no-reply@extoman.com",
                Subject = "تبریک! ثبت نام شما در ایکس تومن با موفقیت انجام شد"
            };
            var registerEmailBody = registerEmail.GetMailMessage().Body.Replace("&amp;", "&");
            await _messageManager.NewRegisteredUserAsync(registerEmailBody, emailOrPhone, phoneCode, confirmPhoneNumberUrl);
        }

        private async Task PrepareAndSendEmailAndPhoneChangedMessageAsync(bool emailFirstTime, bool emailChanged, bool mobileFirstTime, bool mobileChanged, AppUser user)
        {
            var fullName = user.FullName.Trim();
            if (!fullName.HasValue())
                fullName = user.UserName;

            if (emailChanged && user.Email.HasValue())
            {
                var emailCode = await _userManager.GenerateEmailConfirmationTokenAsync(user.Id);
                emailCode = HttpUtility.UrlEncode(emailCode);
                var emailConfirmUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = emailCode }, Request.Url.Scheme);
                if (emailFirstTime)
                {
                    var emailAddedEmail = new ConfirmEmail()
                    {
                        ConfirmCode = emailCode,
                        ConfirmUrl = emailConfirmUrl,
                        From = "no-reply@extoman.com",
                        Fullname = fullName,
                        To = user.Email,
                        Subject = "ایمیل شما ثبت شد.",
                        Username = user.UserName,
                        ViewName = "EmailAdded"
                    };
                    var emailAddedEmailBody = emailAddedEmail.GetMailMessage().Body;
                    await _messageManager.EmailAddedAsync(emailAddedEmailBody, fullName, user.Email);
                }
                else
                {
                    var emailChangedEmail = new ConfirmEmail()
                    {
                        ConfirmCode = emailCode,
                        ConfirmUrl = emailConfirmUrl,
                        From = "no-reply@extoman.com",
                        Fullname = fullName,
                        To = user.Email,
                        Subject = "ایمیل شما تغییر یافته است.",
                        Username = user.UserName,
                        ViewName = "EmailChanged"
                    };
                    var emailChangedEmailBody = emailChangedEmail.GetMailMessage().Body;
                    await _messageManager.EmailChangedAsync(emailChangedEmailBody, fullName, user.Email);
                }
            }

            if (mobileChanged && user.PhoneNumber.HasValue())
            {
                var phoneCode = await _userManager.GenerateChangePhoneNumberTokenAsync(user.Id, user.PhoneNumber);
                var phoneConfirmUrl = Url.Action("ConfirmPhoneNumber", "Account", new { user.PhoneNumber }, Request.Url.Scheme);
                if (mobileFirstTime)
                {
                    await _messageManager.PhoneNumberAddedAsync(user.PhoneNumber, phoneCode, phoneConfirmUrl);
                }
                else
                {
                    await _messageManager.PhoneNumberChangedAsync(user.PhoneNumber, phoneCode, phoneConfirmUrl);
                }
            }
        }
        #endregion
    }
}