﻿using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices;
using Xtoman.Domain.WebServices.Payment.Mellat;
using Xtoman.Domain.WebServices.Payment.PayIR;
using Xtoman.Domain.WebServices.Payment.Saman;
using Xtoman.Framework;
using Xtoman.Framework.Mvc;
using Xtoman.Service;
using Xtoman.Service.WebServices;
using Xtoman.Utility;
using Xtoman.Utility.PostalEmail;
using Xtoman.Web.ViewModels;

namespace Xtoman.Web.Controllers
{
    public class ExchangeOrderController : BaseController
    {
        #region Properties
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly IPaymentManager _paymentManager;
        private readonly ICoinPaymentsService _coinPaymentsService;
        private readonly IMellatService _mellatService;
        private readonly ISamanService _samanService;
        private readonly IPayIRService _payIRService;
        private readonly IVandarService _vandarService;
        private readonly IMessageManager _messageManager;
        private readonly ITradeManager _tradeManager;
        private readonly IWithdrawManager _withdrawManager;
        private readonly IBalanceManager _balanceManager;
        private readonly IPerfectMoneyStatusService _perfectMoneyStatusService;
        private readonly ICoinPaymentsIPNResultService _coinPaymentsIPNResultService;
        private readonly IOrderManager _orderManager;
        private readonly IUserBankCardService _userBankCardService;
        #endregion

        #region Constructor
        public ExchangeOrderController(
            IExchangeOrderService exchangeOrderService,
            IPaymentManager paymentManager,
            ICoinPaymentsService coinPaymentsService,
            IMellatService mellatService,
            ISamanService samanService,
            IPayIRService payIRService,
            IVandarService vandarService,
            IMessageManager messageManager,
            ITradeManager tradeManager,
            IWithdrawManager withdrawManager,
            IBalanceManager balanceManager,
            IPerfectMoneyStatusService perfectMoneyStatusService,
            ICoinPaymentsIPNResultService coinPaymentsIPNResultService,
            IOrderManager orderManager,
            IUserBankCardService userBankCardService)
        {
            _exchangeOrderService = exchangeOrderService;
            _paymentManager = paymentManager;
            _coinPaymentsService = coinPaymentsService;
            _mellatService = mellatService;
            _samanService = samanService;
            _payIRService = payIRService;
            _vandarService = vandarService;
            _messageManager = messageManager;
            _tradeManager = tradeManager;
            _withdrawManager = withdrawManager;
            _balanceManager = balanceManager;
            _perfectMoneyStatusService = perfectMoneyStatusService;
            _coinPaymentsIPNResultService = coinPaymentsIPNResultService;
            _orderManager = orderManager;
            _userBankCardService = userBankCardService;
        }
        #endregion

        #region Actions
        [Authorize]
        public virtual async Task<ActionResult> Index(Guid orderGuid)
        {
            var QRString = "";
            var MemoQRString = "";
            var userId = User.Identity.GetUserId<int>();
            var order = await _exchangeOrderService.Table
                .Where(p => p.Guid == orderGuid)
                .Include(p => p.Exchange)
                .Include(p => p.Exchange.FromCurrency)
                .Include(p => p.BankResponse)
                .Include(p => p.OrderWithdraws)
                .SingleAsync();

            if (!User.IsInRole("Admin"))
                if ((order == null || order.UserId != userId))
                    return HttpNotFound();

            if (order.PaymentStatus != PaymentStatus.Paid)
                switch (order.Exchange.ExchangeFiatCoinType)
                {
                    case ExchangeFiatCoinType.CoinToCoin:
                    case ExchangeFiatCoinType.CoinToFiatUSD:
                    case ExchangeFiatCoinType.CoinToFiatToman:
                        if (!order.PayAddress.HasValue()/* || order.ExpireDate <= DateTime.UtcNow*/)
                        {
                            //var isBinanceAddress = false;
                            //Domain.WebServices.CoinPaymentAddressResult;
                            var coinPaymentsCallBack = Url.Action("Index", "CPIPNCB", null, Request.Url.Scheme);
                            //var coinToFiatAddress = await _paymentManager.GetCoinToFiatPaymentAddressAsync(order.Exchange.FromCurrency.Type, orderGuid, order.PayAmount, coinPaymentsCallBack).ConfigureAwait(false);
                            var coinToFiatAddress = await _paymentManager.GetCoinPaymentAddressAsync(order.Exchange.FromCurrency.Type, order.Exchange.FromCurrency.IsERC20, coinPaymentsCallBack);
                            if (coinToFiatAddress == null)
                            {
                                var message = $"خطا در هنگام دریافت آدرس CoinPayments {order.Exchange.FromCurrency.Name}";
                                new Exception(message).LogError();
                                await _messageManager.TelegramAdminMessageAsync(message);

                                //if (order.Exchange.FromCurrency.Type == ECurrencyType.Bitcoin)
                                //{
                                //    coinToFiatAddress = new CoinPaymentAddressResult()
                                //    {
                                //        Address = DepositAddresses.BTC_Address
                                //    };
                                //}
                                //else
                                //{
                                var apiServerModel = new ApiServerErrorViewModel()
                                {
                                    Text = $"خطایی در هنگام ارتباط با کیف پول و دریافت آدرس {order.Exchange.FromCurrency.Name} رخ داد.",
                                    Title = "خطای دریافت آدرس"
                                };
                                return View("ApiServerError", apiServerModel);
                                //}
                            }
                            QRString = QRGenerator.GetImageBase64String(coinToFiatAddress.Address);
                            if (coinToFiatAddress.PubKey.HasValue())
                                MemoQRString = QRGenerator.GetImageBase64String(coinToFiatAddress.PubKey);
                            order.PayAddress = coinToFiatAddress.Address;
                            //order.PayConfirmsNeed = coinToFiatAddress.ConfirmsNeed;
                            order.PayMemoTagPaymentId = coinToFiatAddress.MemoTagPaymentId;
                            //order.PayQRUrl = coinToFiatAddress.QRUrl;
                            //order.PayPaymentGatewayId = coinToFiatAddress.Id;
                            //order.PayStatusUrl = coinToFiatAddress.StatusUrl;
                            order.PayWalletApiType = WalletApiType.CoinPayments;
                            order.PayTimeEnd = DateTime.UtcNow.AddMinutes(30);
                            await _exchangeOrderService.UpdateAsync(order);
                        }
                        else
                        {
                            QRString = QRGenerator.GetImageBase64String(order.PayAddress);
                            if (order.PayMemoTagPaymentId.HasValue())
                                MemoQRString = QRGenerator.GetImageBase64String(order.PayMemoTagPaymentId);
                        }
                        break;

                    case ExchangeFiatCoinType.FiatTomanToCoin:
                    case ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham:
                        var timestamp = DateTime.UtcNow.ConvertToUnixTimestamp();
                        if (order.BankResponse == null)
                            order.BankResponse = new BankResponseStatus();
                        if (order.BankResponse.BankGatewayType.HasValue && order.PaymentStatus == PaymentStatus.InPayment)
                        {
                            order.BankResponse.BankOrderId = timestamp; // Unique Id for bank payment
                            if (!order.InsertUserIpAddress.HasValue())
                                order.InsertUserIpAddress = IPAddress;
                            else if ((order.InsertUserIpAddress.HasValue() && order.InsertUserIpAddress != IPAddress))
                                order.InsertUserIpAddress += ", " + IPAddress;
                            await _exchangeOrderService.UpdateAsync(order);

                            if (order.BankResponse.BankGatewayType.Value == BankGatewayType.Vandar)
                                return await RedirectToVandarIrAsync(order);

                            if (!order.BankResponse.BankGatewayType.HasValue || order.BankResponse.BankGatewayType.Value == BankGatewayType.Payir)
                                return await RedirectToPayIrAsync(order);

                            //await DoBankPaymentAsync(order.BankResponse.BankGatewayType.Value, order);
                            return RedirectToAction("ServerError", "Error");
                            //return null;
                        }
                        var userCards = await _userBankCardService.TableNoTracking
                            .Where(p => p.UserId == userId && p.VerificationStatus == VerificationStatus.Confirmed)
                            .Select(p => p.CardNumber)
                            .ToListAsync();

                        ViewBag.Cards = userCards;
                        break; // If reach this, user have to choose payment gateway
                    case ExchangeFiatCoinType.FiatUSDToCoin:
                    case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman:
                    case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham:
                        //UNDONE: Open USD gateway
                        if (order.PaymentStatus == PaymentStatus.InPayment)
                        {
                            if (!order.InsertUserIpAddress.HasValue())
                                order.InsertUserIpAddress = IPAddress;
                            else if ((order.InsertUserIpAddress.HasValue() && order.InsertUserIpAddress != IPAddress))
                                order.InsertUserIpAddress += ", " + IPAddress;

                            await _exchangeOrderService.UpdateAsync(order);
                            if (order.Exchange.FromCurrency.Type != ECurrencyType.PerfectMoneyVoucher)
                            {
                                _paymentManager.MakePayment(order.PayAmount, order.Exchange.FromCurrency.Type, order.Guid.ToString(), order.Description);
                                return null;
                            }
                        }
                        break;
                }

            var model = new ExchangeOrderViewModel().FromEntity(order);
            if (order.Exchange.ExchangeFiatCoinType.ToString().Contains("CoinTo"))
            {
                model.QRString = QRString;
                model.MemoQRString = MemoQRString;
                var orderGuidString = orderGuid.ToString();
                ViewBag.CoinPaymentStatus = await _coinPaymentsIPNResultService.TableNoTracking
                    .Where(p => p.Custom == orderGuidString)
                    .OrderByDescending(p => p.Status)
                    .FirstOrDefaultAsync();
            }
            return View(model);
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> PMVoucher(string OrderId, string VoucherNumber, string VoucherActivation)
        {
            var result = new ResultModel();
            try
            {
                var (status, amount, batchNumber) = await _paymentManager.PaymentPerfectVoucherPaymentAsync(VoucherNumber, VoucherActivation);
                var guid = new Guid(OrderId);
                var order = await _exchangeOrderService.Table.SingleAsync(p => p.Guid == guid);
                if (status)
                {
                    order.PaymentStatus = PaymentStatus.Paid;
                    order.OrderStatus = OrderStatus.Processing;
                    order.PayTransactionId = batchNumber;
                    if ((double)order.PayAmount > amount)
                    {
                        order.ReceiveAmount = amount.ToDecimal() * order.ReceiveAmount / order.PayAmount;
                        order.PayAmount = amount.ToDecimal();
                    }
                    await _exchangeOrderService.UpdateAsync(order);
                    result.Text = $"مبلغ {amount} دلار پرفکت دریافت شد. کد تراکنش {batchNumber}";
                    result.Status = true;
                }
                else
                {
                    result.Text = $"خطا: ";
                    if (batchNumber == "Invalid ev_number")
                        result.Text += "شماره ووچر اشتباه است.";
                    else if (batchNumber == "Invalid ev_code")
                        result.Text += "کد ووچر اشتباه است.";
                    else
                        result.Text += batchNumber;


                    await _messageManager.TelegramAdminMessageAsync($"خطا در فروش ووچر پرفکت: {batchNumber}");
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
                await _messageManager.TelegramAdminMessageAsync($"خطا در فروش ووچر پرفکت: {ex.Message}");
                result.Text = "خطای نامشخص";
            }
            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> SetOrderBankPaymentGateWay(/*BankGatewayType GateWay, */Guid OrderId)
        {
            var order = await _exchangeOrderService.Table
                .Where(p => p.Guid == OrderId)
                .Include(p => p.BankResponse)
                .SingleAsync();

            if (order.BankResponse == null)
                order.BankResponse = new BankResponseStatus();

            order.BankResponse.BankGatewayType = BankGatewayType.Payir; // BankGatewayType.Payir;
            order.PaymentStatus = PaymentStatus.InPayment;
            await _exchangeOrderService.UpdateAsync(order);

            return Json(new ResultModel() { Status = true });
        }

        [AllowAnonymous]
        public async Task<ActionResult> BankCallBack(byte id, MellatCallBack mellatCallBack, SamanCallBack samanCallBack)
        {
            var bankGatewayType = (BankGatewayType)id;
            var order = await FindAndUpdateOrderAsync(bankGatewayType, mellatCallBack, samanCallBack);
            if (order == null)
                return HttpNotFound();

            if ((order.PaymentStatus != PaymentStatus.InPayment && order.PaymentStatus != PaymentStatus.NotVerified) || order.PaymentStatus == PaymentStatus.Paid)
                return RedirectToAction("Index", new { orderGuid = order.Guid });

            if (!HasReferenceValue(bankGatewayType, mellatCallBack, samanCallBack))
                await CancelOrderAsync(order);

            if (!PaymentSuccessed(order))
            {
                order.PaymentStatus = PaymentStatus.Voided;
                return RedirectToAction("Index", new { orderGuid = order.Guid });
            }

            order.PaymentStatus = PaymentStatus.Canceled;
            if (await VerifyPaymentAsync(order))
            {
                //Do order things and complete it.
                order.PaymentStatus = PaymentStatus.Paid;
                order.OrderStatus = OrderStatus.Processing;
                order.PaymentDate = DateTime.UtcNow;
                await _exchangeOrderService.UpdateAsync(order);

                var userName = order.User.FullName.HasValue() ? order.User.FullName : order.User.UserName;
                //var telegramText = $"سفارش کاربر {userName} به شماره {order.Id}, به مبلغ {order.PayAmount.ToPriceWithoutFloat()} تومان پرداخت شد.";
                var orderUrl = Url.Action("Index", "ExchangeOrder", new { orderGuid = order.Guid }, Request.Url.Scheme);
                var payAmount = order.Exchange.FromCurrency.UnitSign == "تومان" ? order.PayAmount.ToPriceWithoutFloat() + " تومان" : order.PayAmount.ToBlockChainPrice() + " " + order.Exchange.FromCurrency.UnitSign;
                var receiveAmount = order.Exchange.ToCurrency.UnitSign == "تومان" ? order.ReceiveAmount.ToPriceWithoutFloat() + " تومان" : order.ReceiveAmount.ToBlockChainPrice() + " " + order.Exchange.ToCurrency.UnitSign;
                var paidOrderEmail = new OrderPaidEmail()
                {
                    ExchangeTime = "5 تا 30 دقیقه",
                    From = "no-reply@extoman.com",
                    To = order.User.Email,
                    FromCurrency = order.Exchange.FromCurrency.Name,
                    ToCurrency = order.Exchange.ToCurrency.Name,
                    Fullname = order.User.FullName,
                    OrderUrl = orderUrl,
                    PayAmount = payAmount,
                    ReceiveAmount = receiveAmount,
                    Subject = "سفارش جدید ثبت گردید",
                    Username = order.User.UserName,
                    ViewName = "OrderPaid",
                    TxFee = order.Exchange.ToCurrency.TransactionFee + " " + order.Exchange.ToCurrency.UnitSign
                };

                var paidOrderEmailBody = paidOrderEmail.GetMailMessage().Body;
                await _messageManager.OrderPaidByTomanAsync(paidOrderEmailBody, order.User.Email, order.User.PhoneNumber, userName, order.Id, order.Guid.ToString(), order.PayAmount);

                if (!order.User.IsPhisher)
                {
                    switch (order.Exchange.ExchangeFiatCoinType)
                    {
                        case ExchangeFiatCoinType.FiatTomanToCoin:
                            await _orderManager.CompleteFiatToCoinOrderAsync(order);
                            //Get balance available and Withdraw coin to user address or Trade and withdraw from Trading Platforms like: bitfinex, binance, hitbtc
                            //await _paymentManager.WithdrawCoinOrTradeAsync(order.Id, order.Exchange.ToCurrency.Type, order.ReceiveAmount, order.ReceiveAddress, order.ReceiveMemoTagPaymentId)
                            break;
                        case ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham:
                            await _orderManager.CompleteFiatTomanToFiatUSDOrderAsync(order);
                            //await _paymentManager.WithdrawFiatUSDAsync(order.Id, order.Exchange.ToCurrency.Type, order.ReceiveAmount, order.ReceiveAddress);
                            break;
                    }
                }
                else
                {
                    order.OrderStatus = OrderStatus.Complete;
                    await _messageManager.TelegramAdminMessageAsync($"پرداخت کاربر {order.User.FirstName + " " + order.User.LastName } با مبلغ { order.PayAmount.ToPriceWithoutFloat() } تومان برای سفارش {order.Id} به نفع اکس تومن ظبط گردید.");
                }
                //await _messageManager.OrderPaidAsync();
            }
            //Send message
            await _exchangeOrderService.UpdateAsync(order);
            return RedirectToAction("Index", new { orderGuid = order.Guid });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Vandar(string token, VandarCallback model)
        {
            new Exception($"Vandar callback for token: {token} model: {model.Serialize()}").LogError();

            var orderQuery = _exchangeOrderService.Table
                .Where(p => p.BankResponse.BankSaleReferenceId == token);
            var orderCount = await orderQuery.CountAsync();
            if (orderCount == 1)
            {
                var order = await orderQuery.SingleAsync();
                if (model.Status == 1 && model.CardNumber.HasValue())
                {
                    if (order.PaymentStatus != PaymentStatus.Paid && order.OrderStatus != OrderStatus.Complete)
                    {
                        // Validate CardNumber
                        var isCardVerified = order.User.UserBankCards
                            .Where(p => p.VerificationStatus == VerificationStatus.Confirmed)
                            .Any(x => string.Join("******", x.CardNumber.Left(6), x.CardNumber.Right(4)).Equals(model.CardNumber) ||
                                      x.CardNumber.Equals(model.CardNumber));

                        // Validate Model Amount
                        var isModelAmountValid = (order.PayAmount * 10).ToInt() >= model.Amount.ToDecimal().ToInt();

                        if (isCardVerified && isModelAmountValid)
                        {
                            var confirmResult = await _vandarService.ConfirmAsync(new VandarConfirmInput() { confirm = true, token = token });
                            if (confirmResult.Status == 1)
                            {
                                order.PaymentStatus = PaymentStatus.NotVerified;
                                await _exchangeOrderService.UpdateAsync(order);
                                var verifyPaymentResult = await _vandarService.VandarVerifyAsync(new VandarVerifyInput() { token = token });
                                new Exception($"Vandar ExchangeOrder Payment Verify result ({verifyPaymentResult.Serialize()}").LogError();
                                if (verifyPaymentResult.Status == 1)
                                {
                                    // Check user is phisher
                                    if (!order.User.IsPhisher)
                                    {
                                        // Validate Amount
                                        var isVerifyAmountOK = (order.PayAmount * 10).ToInt() >= verifyPaymentResult.Amount.ToDecimal().ToInt();
                                        // Validate CardNumber
                                        var isVerifyCardVerified = order.User.UserBankCards
                                            .Where(p => p.VerificationStatus == VerificationStatus.Confirmed)
                                            .Any(x => string.Join("******", x.CardNumber.Left(6), x.CardNumber.Right(4)).Equals(verifyPaymentResult.CardNumber) ||
                                                      x.CardNumber.Equals(model.CardNumber));

                                        if (isVerifyAmountOK && isVerifyCardVerified)
                                        {
                                            // Validate OrderId by FactorNumber
                                            var isFactorNumberOk = verifyPaymentResult.FactorNumber.TryToLong() == order.Id;
                                            if (isFactorNumberOk)
                                            {
                                                // Validate Not Double Spending
                                                var isDoubleSpended = await _exchangeOrderService.TableNoTracking.Where(p => p.Id != order.Id && p.PayTransactionId == verifyPaymentResult.TransId).AnyAsync();
                                                if (!isDoubleSpended)
                                                {
                                                    order.PaymentStatus = PaymentStatus.Paid;
                                                    order.PayTransactionId = verifyPaymentResult.TransId;
                                                    order.BankResponse.BankCardHolderPan = verifyPaymentResult.CardNumber;

                                                    await _exchangeOrderService.UpdateAsync(order);

                                                    // Validate Not Same Time Request
                                                    // ????????

                                                    // Validate Trade & Withdraw
                                                    var isWithdrawedBefore = order.OrderWithdraws.Any();

                                                    if (!isWithdrawedBefore)
                                                    {
                                                        // Then Complete the order
                                                        switch (order.Exchange.ExchangeFiatCoinType)
                                                        {
                                                            case ExchangeFiatCoinType.FiatTomanToCoin:
                                                                await _orderManager.CompleteFiatToCoinOrderAsync(order);
                                                                //Get balance available and Withdraw coin to user address or Trade and withdraw from Trading Platforms like: bitfinex, binance, hitbtc
                                                                //await _paymentManager.WithdrawCoinOrTradeAsync(order.Id, order.Exchange.ToCurrency.Type, order.ReceiveAmount, order.ReceiveAddress, order.ReceiveMemoTagPaymentId)
                                                                break;
                                                            case ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham:
                                                                await _orderManager.CompleteFiatTomanToFiatUSDOrderAsync(order);
                                                                //await _paymentManager.WithdrawFiatUSDAsync(order.Id, order.Exchange.ToCurrency.Type, order.ReceiveAmount, order.ReceiveAddress);
                                                                break;
                                                        }

                                                        var userName = order.User.FullName.HasValue() ? order.User.FullName : order.User.UserName;
                                                        //var telegramText = $"سفارش کاربر {userName} به شماره {order.Id}, به مبلغ {order.PayAmount.ToPriceWithoutFloat()} تومان پرداخت شد.";
                                                        var orderUrl = Url.Action("Index", "ExchangeOrder", new { orderGuid = order.Guid }, Request.Url.Scheme);
                                                        var payAmount = order.Exchange.FromCurrency.UnitSign == "تومان" ? order.PayAmount.ToPriceWithoutFloat() + " تومان" : order.PayAmount.ToBlockChainPrice() + " " + order.Exchange.FromCurrency.UnitSign;
                                                        var receiveAmount = order.Exchange.ToCurrency.UnitSign == "تومان" ? order.ReceiveAmount.ToPriceWithoutFloat() + " تومان" : order.ReceiveAmount.ToBlockChainPrice() + " " + order.Exchange.ToCurrency.UnitSign;
                                                        var paidOrderEmail = new OrderPaidEmail()
                                                        {
                                                            ExchangeTime = "5 تا 30 دقیقه",
                                                            From = "no-reply@extoman.com",
                                                            To = order.User.Email,
                                                            FromCurrency = order.Exchange.FromCurrency.Name,
                                                            ToCurrency = order.Exchange.ToCurrency.Name,
                                                            Fullname = order.User.FullName,
                                                            OrderUrl = orderUrl,
                                                            PayAmount = payAmount,
                                                            ReceiveAmount = receiveAmount,
                                                            Subject = "سفارش جدید ثبت گردید",
                                                            Username = order.User.UserName,
                                                            ViewName = "OrderPaid",
                                                            TxFee = order.Exchange.ToCurrency.TransactionFee + " " + order.Exchange.ToCurrency.UnitSign
                                                        };

                                                        var paidOrderEmailBody = paidOrderEmail.GetMailMessage().Body;
                                                        await _messageManager.OrderPaidByTomanAsync(paidOrderEmailBody, order.User.Email, order.User.PhoneNumber, userName, order.Id, order.Guid.ToString(), order.PayAmount);
                                                    }

                                                }
                                                else
                                                {
                                                    order.OrderStatus = OrderStatus.Complete;
                                                    order.Errors += $" پرداخت دوبار استفاده شده";
                                                    await _messageManager.TelegramAdminMessageAsync($"Double Spend {order.Id}");
                                                }
                                            }
                                            else
                                            {
                                                order.PaymentStatus = PaymentStatus.Voided;
                                                order.Errors += $" پرداخت برای فاکتور {verifyPaymentResult.FactorNumber} ثبت شده است ولی برای سفارش {order.Id} انجام شده.";
                                            }
                                        }
                                        else
                                        {
                                            order.OrderStatus = OrderStatus.InComplete;
                                            if (!isVerifyCardVerified)
                                            {
                                                order.PaymentStatus = PaymentStatus.CardNotValid;
                                                order.Errors += $" کارتی که با آن پرداخت شده احراز نشده";
                                                await _messageManager.TelegramAdminMessageAsync($"کارت پرداخت شده ثبت نشده. {model.CardNumber}. سفارش {order.Id} مبلغ به کارت مبدا بازگشت داده شود یا در صورت افزودن کارت سفارش تکمیل گردد.");
                                            }
                                            else
                                            {
                                                order.Errors += $" مبلغ پرداخت شده {(verifyPaymentResult.Amount.ToDecimal() / 10).ToInt().ToPrice()} می باشد ولی مبلغ فاکتور {order.PayAmount.ToPrice()} می باشد.";
                                                await _messageManager.TelegramAdminMessageAsync($"پرداخت سفارش شماره {order.Id} با مقدار {order.PayAmount.ToInt()} مخالف مقدار پرداخت شده {verifyPaymentResult.Amount} است.");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        order.OrderStatus = OrderStatus.Complete;
                                        order.Errors += " کاربر فیشر است.";
                                        await _messageManager.TelegramAdminMessageAsync($"پرداخت کاربر {order.User.FirstName + " " + order.User.LastName } با مبلغ { order.PayAmount.ToPriceWithoutFloat() } تومان برای سفارش {order.Id} به نفع اکس تومن ضبط گردید.");
                                    }
                                }
                            }
                            else
                            {
                                await _messageManager.TelegramAdminMessageAsync($"خطا در تایید تراکنش وندار. {confirmResult.Errors.Join(", ")} {confirmResult.Message}".Trim());
                            }
                        }
                        else
                        {
                            order.PaymentStatus = PaymentStatus.CardNotValid;
                            order.OrderStatus = OrderStatus.RefundComplete;
                            order.Errors += $" کارتی که با آن پرداخت شده احراز نشده";
                            var confirmResult = await _vandarService.ConfirmAsync(new VandarConfirmInput() { confirm = false, token = token });
                            await _messageManager.TelegramAdminMessageAsync($"کارت پرداخت شده ثبت نشده. {model.CardNumber}. سفارش {order.Id} مبلغ به کارت مبدا بازگشت می خورد.");
                        }
                    }
                }
                else
                {
                    order.PaymentStatus = PaymentStatus.NotPaid;
                }
                if (order.PaymentStatus == PaymentStatus.InPayment)
                    order.PaymentStatus = PaymentStatus.NotPaid;
                await _exchangeOrderService.UpdateAsync(order);
                return RedirectToAction("Index", new { orderGuid = order.Guid });
            }
            else if (orderCount > 1)
            {
                new Exception($"Vandar: تعداد پرداخت ها و سفارش های یافت شده برای توکن: '{token}' بیشتر از یک مورد است.").LogError();
                return RedirectToAction("Exchanges", "Account");
            }
            else
            {
                new Exception($"Vandar: سفارشی با توکن {token} یافت نشد.").LogError();
                return RedirectToAction("Exchanges", "Account");
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> PayIr(byte status = 0, string token = "")
        {
            new Exception($"PayIr Status = {status}, Token = {token}").LogError();
            var orderQuery = _exchangeOrderService.Table
                .Where(p => p.BankResponse.BankSaleReferenceId == token);
            var orderCount = await orderQuery.CountAsync();
            if (orderCount == 1)
            {
                var order = await orderQuery.SingleAsync();
                if (status == 1)
                {
                    if (order.PaymentStatus != PaymentStatus.Paid && order.OrderStatus != OrderStatus.Complete)
                    {
                        order.PaymentStatus = PaymentStatus.NotVerified;
                        await _exchangeOrderService.UpdateAsync(order);
                        var verifyPaymentResult = await _payIRService.VerifyAsync(new PayIRVerifyInput() { token = token });
                        new Exception($"PayIR ExchangeOrder Payment Verify result ({verifyPaymentResult.Serialize()}").LogError();
                        if (verifyPaymentResult.Status == 1)
                        {
                            // Check user is phisher
                            if (!order.User.IsPhisher)
                            {
                                // Validate Amount
                                var isAmountOK = (order.PayAmount * 10).ToInt() == verifyPaymentResult.Amount;

                                if (isAmountOK)
                                {
                                    // Validate OrderId by FactorNumber
                                    var isFactorNumberOk = verifyPaymentResult.FactorNumber.TryToLong() == order.Id;
                                    if (isFactorNumberOk)
                                    {
                                        // Validate Not Double Spending
                                        var isDoubleSpended = await _exchangeOrderService.TableNoTracking.Where(p => p.Id != order.Id && p.PayTransactionId == verifyPaymentResult.TransactionId).AnyAsync();
                                        if (!isDoubleSpended)
                                        {
                                            order.PaymentStatus = PaymentStatus.Paid;
                                            order.PayTransactionId = verifyPaymentResult.TransactionId;
                                            order.BankResponse.BankCardHolderPan = verifyPaymentResult.CardNumber;

                                            await _exchangeOrderService.UpdateAsync(order);

                                            // Validate Not Same Time Request
                                            // ????????

                                            // Validate CardNumber
                                            var isCardVerified = order.User.UserBankCards
                                                .Where(p => p.VerificationStatus == VerificationStatus.Confirmed)
                                                .Any(x => string.Join("******", x.CardNumber.Left(6), x.CardNumber.Right(4)).Equals(verifyPaymentResult.CardNumber) ||
                                                          x.CardNumber.Equals(verifyPaymentResult.CardNumber));

                                            if (isCardVerified)
                                            {
                                                // Validate Trade & Withdraw
                                                var isWithdrawedBefore = order.OrderWithdraws.Any();

                                                if (!isWithdrawedBefore)
                                                {
                                                    // Then Complete the order
                                                    switch (order.Exchange.ExchangeFiatCoinType)
                                                    {
                                                        case ExchangeFiatCoinType.FiatTomanToCoin:
                                                            await _orderManager.CompleteFiatToCoinOrderAsync(order);
                                                            //Get balance available and Withdraw coin to user address or Trade and withdraw from Trading Platforms like: bitfinex, binance, hitbtc
                                                            //await _paymentManager.WithdrawCoinOrTradeAsync(order.Id, order.Exchange.ToCurrency.Type, order.ReceiveAmount, order.ReceiveAddress, order.ReceiveMemoTagPaymentId)
                                                            break;
                                                        case ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham:
                                                            await _orderManager.CompleteFiatTomanToFiatUSDOrderAsync(order);
                                                            //await _paymentManager.WithdrawFiatUSDAsync(order.Id, order.Exchange.ToCurrency.Type, order.ReceiveAmount, order.ReceiveAddress);
                                                            break;
                                                    }

                                                    var userName = order.User.FullName.HasValue() ? order.User.FullName : order.User.UserName;
                                                    //var telegramText = $"سفارش کاربر {userName} به شماره {order.Id}, به مبلغ {order.PayAmount.ToPriceWithoutFloat()} تومان پرداخت شد.";
                                                    var orderUrl = Url.Action("Index", "ExchangeOrder", new { orderGuid = order.Guid }, Request.Url.Scheme);
                                                    var payAmount = order.Exchange.FromCurrency.UnitSign == "تومان" ? order.PayAmount.ToPriceWithoutFloat() + " تومان" : order.PayAmount.ToBlockChainPrice() + " " + order.Exchange.FromCurrency.UnitSign;
                                                    var receiveAmount = order.Exchange.ToCurrency.UnitSign == "تومان" ? order.ReceiveAmount.ToPriceWithoutFloat() + " تومان" : order.ReceiveAmount.ToBlockChainPrice() + " " + order.Exchange.ToCurrency.UnitSign;
                                                    var paidOrderEmail = new OrderPaidEmail()
                                                    {
                                                        ExchangeTime = "5 تا 30 دقیقه",
                                                        From = "no-reply@extoman.com",
                                                        To = order.User.Email,
                                                        FromCurrency = order.Exchange.FromCurrency.Name,
                                                        ToCurrency = order.Exchange.ToCurrency.Name,
                                                        Fullname = order.User.FullName,
                                                        OrderUrl = orderUrl,
                                                        PayAmount = payAmount,
                                                        ReceiveAmount = receiveAmount,
                                                        Subject = "سفارش جدید ثبت گردید",
                                                        Username = order.User.UserName,
                                                        ViewName = "OrderPaid",
                                                        TxFee = order.Exchange.ToCurrency.TransactionFee + " " + order.Exchange.ToCurrency.UnitSign
                                                    };

                                                    var paidOrderEmailBody = paidOrderEmail.GetMailMessage().Body;
                                                    await _messageManager.OrderPaidByTomanAsync(paidOrderEmailBody, order.User.Email, order.User.PhoneNumber, userName, order.Id, order.Guid.ToString(), order.PayAmount);
                                                }
                                            }
                                            else
                                            {
                                                order.PaymentStatus = PaymentStatus.CardNotValid;
                                                order.OrderStatus = OrderStatus.Canceled;
                                                order.Errors += $" کارتی که با آن پرداخت شده احراز نشده";
                                                await _messageManager.TelegramAdminMessageAsync($"کارت پرداخت شده ثبت نشده. {verifyPaymentResult.CardNumber}. سفارش {order.Id} لغو شد. در صورت بازگشت نداده شدن پول تا 24 ساعت دستی انجام شود.");
                                            }
                                        }
                                        else
                                        {
                                            order.OrderStatus = OrderStatus.Complete;
                                            order.Errors += $" پرداخت دوبار استفاده شده";
                                            await _messageManager.TelegramAdminMessageAsync($"Double Spend {order.Id}");
                                        }
                                    }
                                    else
                                    {
                                        order.PaymentStatus = PaymentStatus.Voided;
                                        order.Errors += $" پرداخت برای فاکتور {verifyPaymentResult.FactorNumber} ثبت شده است ولی برای سفارش {order.Id} انجام شده.";
                                    }
                                }
                                else
                                {
                                    order.OrderStatus = OrderStatus.InComplete;
                                    order.Errors += $" مبلغ پرداخت شده {(verifyPaymentResult.Amount / 10).ToPrice()} می باشد ولی مبلغ فاکتور {order.PayAmount.ToPrice()} می باشد.";
                                    await _messageManager.TelegramAdminMessageAsync($"پرداخت سفارش شماره {order.Id} با مقدار {order.PayAmount.ToInt()} مخالف مقدار پرداخت شده {verifyPaymentResult.Amount} است.");
                                }
                            }
                            else
                            {
                                order.OrderStatus = OrderStatus.Complete;
                                order.Errors += " کاربر فیشر است.";
                                await _messageManager.TelegramAdminMessageAsync($"پرداخت کاربر {order.User.FirstName + " " + order.User.LastName } با مبلغ { order.PayAmount.ToPriceWithoutFloat() } تومان برای سفارش {order.Id} به نفع اکس تومن ضبط گردید.");
                            }
                        }
                    }
                }
                else
                {
                    order.PaymentStatus = PaymentStatus.NotPaid;
                }
                await _exchangeOrderService.UpdateAsync(order);
                return RedirectToAction("Index", new { orderGuid = order.Guid });
            }
            else if (orderCount > 1)
            {
                new Exception($"تعداد پرداخت ها و سفارش های یافت شده برای توکن: '{token}' بیشتر از یک مورد است.").LogError();
                return RedirectToAction("Exchanges", "Account");
            }
            else
            {
                new Exception($"سفارشی با توکن {token} یافت نشد.").LogError();
                return RedirectToAction("Exchanges", "Account");
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> PMStatus(PerfectMoneyPaymentStatus status)
        {
            await _perfectMoneyStatusService.InsertAsync(status);
            var paymentId = Guid.Parse(status.PAYMENT_ID);
            var order = _exchangeOrderService.Table
                .Include(p => p.User)
                .Include(p => p.Exchange)
                .Include(p => p.Exchange.FromCurrency)
                .Include(p => p.Exchange.ToCurrency)
                .Where(p => p.Guid == paymentId).FirstOrDefault();
            if (order == null)
            {
                var orderNotFoundMessage = $"پرداخت پرفکت مانی به شماره پیگیری {status.PAYMENT_BATCH_NUM} انجام شد ولی سفارش به شماره {status.PAYMENT_ID} پیدا نشد";
                await _messageManager.TelegramAdminMessageAsync(orderNotFoundMessage);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
            }
            if (order.PaymentStatus == PaymentStatus.Paid)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
            }
            var PaymentIsValid = IsPerfectMoneyPaymentValid(status, order.PayAmount);
            if (PaymentIsValid)
            {
                order.PaymentStatus = PaymentStatus.Paid;
                order.OrderStatus = OrderStatus.Processing;
                order.PaymentDate = DateTime.UtcNow;
                await _exchangeOrderService.UpdateAsync(order);

                var userName = order.User.FullName.HasValue() ? order.User.FullName : order.User.UserName;
                var orderUrl = Url.Action("Index", "ExchangeOrder", new { orderGuid = order.Guid }, Request.Url.Scheme);
                var payAmount = order.Exchange.FromCurrency.UnitSign == "تومان" ? order.PayAmount.ToPriceWithoutFloat() + " تومان" : order.PayAmount.ToBlockChainPrice() + " " + order.Exchange.FromCurrency.UnitSign;
                var receiveAmount = order.Exchange.ToCurrency.UnitSign == "تومان" ? order.ReceiveAmount.ToPriceWithoutFloat() + " تومان" : order.ReceiveAmount.ToBlockChainPrice() + " " + order.Exchange.ToCurrency.UnitSign;
                var paidOrderEmail = new OrderPaidEmail()
                {
                    ExchangeTime = "5 تا 30 دقیقه",
                    From = "no-reply@extoman.com",
                    To = order.User.Email,
                    FromCurrency = order.Exchange.FromCurrency.Name,
                    ToCurrency = order.Exchange.ToCurrency.Name,
                    Fullname = order.User.FullName,
                    OrderUrl = orderUrl,
                    PayAmount = payAmount,
                    ReceiveAmount = receiveAmount,
                    Subject = "سفارش پراخت شد",
                    Username = order.User.UserName,
                    ViewName = "OrderPaid",
                    TxFee = order.Exchange.ToCurrency.TransactionFee + " " + order.Exchange.ToCurrency.UnitSign
                };
                var paidOrderEmailBody = paidOrderEmail.GetMailMessage().Body;
                await _messageManager.OrderPaidByFiatUSDAsync(paidOrderEmailBody, order.User.Email, order.User.PhoneNumber, order.User.FullName, order.Id, order.Guid.ToString(), order.PayAmount);

                switch (order.Exchange.ExchangeFiatCoinType)
                {
                    case ExchangeFiatCoinType.FiatUSDToCoin:
                        await _orderManager.CompleteFiatToCoinOrderAsync(order);
                        break;
                    case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham:
                        await _orderManager.CompleteFiatTomanToFiatUSDOrderAsync(order);
                        break;
                    case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman:
                        //Do nothing. complete manual
                        break;
                }
            }
            else
            {
                await CancelOrderAsync(order);
            }

            return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
        }

        [Authorize]
        [HttpPost]
        [AjaxOnly]
        public virtual async Task<ActionResult> SetOrderInPayment(Guid OrderId)
        {
            var result = new ResultModel();
            try
            {
                var order = await _exchangeOrderService.Table.Where(p => p.Guid == OrderId).SingleAsync();
                order.PaymentStatus = PaymentStatus.InPayment;
                await _exchangeOrderService.UpdateAsync(order);
                result.Text = "درحال انتقال به صفحه پرداخت";
                result.Status = true;
            }
            catch (Exception ex)
            {
                ex.LogError();
                result.Text = "خطایی در هنگام ورود به صفحه پرداخت رخ داد.";
            }

            return Json(result);
        }
        #endregion

        #region Helpers

        //private async Task CompleteFiatTomanToFiatUSDOrderAsync(ExchangeOrder order)
        //{
        //    try
        //    {
        //        order.OrderStatus = OrderStatus.Processing;
        //        order.Errors = null;
        //        await _exchangeOrderService.UpdateAsync(order);
        //        var (withdrawSuccess, withdrawMessage) = await _withdrawManager.WithdrawAsync(order);
        //        await _messageManager.TelegramAdminMessageAsync(withdrawMessage);
        //        if (withdrawSuccess)
        //            order.OrderStatus = OrderStatus.Complete;
        //        else
        //            order.OrderStatus = OrderStatus.WithdrawFailed;
        //        await _exchangeOrderService.UpdateAsync(order);
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.LogError();
        //        await _messageManager.TelegramAdminMessageAsync($"خطا هنگام تکمیل مراحل سفارش خرید پرفکت با شتاب | {ex.Message}");
        //        order.OrderStatus = OrderStatus.InComplete;
        //    }
        //}

        //private async Task CompleteFiatToCoinOrderAsync(ExchangeOrder order)
        //{
        //    try
        //    {
        //        order.OrderStatus = OrderStatus.Processing;
        //        order.Errors = null;
        //        await _exchangeOrderService.UpdateAsync(order);
        //        var trades = new List<Trade>();
        //        var binanceTradeResult = false;
        //        //Trade on binance and kraken while success by 3 retry
        //        var tradeRetryCount = 0;
        //        while (!binanceTradeResult && tradeRetryCount < 3)
        //        {
        //            tradeRetryCount++;
        //            var tradeResult = await _tradeManager.BuyCryptoTradeOrderAsync(order);
        //            binanceTradeResult = tradeResult.BinanceResult;
        //            trades = tradeResult.Trades;
        //        }
        //        var binanceTrade = trades.Where(p => p.TradingPlatform == TradingPlatform.Binance).FirstOrDefault();
        //        if (binanceTradeResult)
        //        {
        //            if (binanceTrade != null)
        //            {
        //                var binanceTelegramMessage = $"تبدیل {binanceTrade.FromCryptoType.ToDisplay()} با مقدار {binanceTrade.FromAmount.ToBlockChainPrice()} به {binanceTrade.ToCryptoType.ToDisplay()} با مقدار {binanceTrade.ToAmount.ToBlockChainPrice()} انجام شد.";
        //                await _messageManager.TelegramAdminMessageAsync(binanceTelegramMessage);
        //            }
        //            var (withdrawSuccess, withdrawMessage) = await _withdrawManager.WithdrawAsync(order);
        //            if (withdrawSuccess)
        //            {
        //                order.OrderStatus = OrderStatus.Complete;
        //                await _exchangeOrderService.UpdateAsync(order);
        //                await _messageManager.TelegramAdminMessageAsync(withdrawMessage);
        //            }
        //            else
        //            {
        //                var errorMessage = $"خطا در هنگام برداشت از Binance برای سفارش شماره {order.Id}. Error: {withdrawMessage}";
        //                await _messageManager.TelegramAdminMessageAsync(errorMessage);
        //                await _exchangeOrderService.UpdateAsync(order);
        //            }
        //        }
        //        if (order.Errors.HasValue())
        //        {
        //            await _messageManager.TelegramAdminMessageAsync(order.Errors);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.LogError();
        //        await _messageManager.TelegramAdminMessageAsync($"خطا هنگام تکمیل مراحل سفارش خرید کوین با شتاب | {ex.Message}");
        //        order.OrderStatus = OrderStatus.InComplete;
        //    }
        //}

        private async Task CancelOrderAsync(ExchangeOrder order)
        {
            order.PaymentStatus = PaymentStatus.Canceled;
            await _exchangeOrderService.UpdateAsync(order);
        }

        private async Task<ActionResult> RedirectToVandarIrAsync(ExchangeOrder order)
        {
            var amount = AppSettingManager.IsLocale ? 10000 : order.PayAmount * 10;
            var vandarRedirect = Url.Action("Vandar", "ExchangeOrder", null, Request.Url.Scheme).TrimEnd('/');

            var retryCount = 0;
            var bankResponded = false;

            while (!bankResponded && retryCount < 3)
            {
                retryCount++;
                try
                {
                    var sendRequestInput = new VandarSendInput()
                    {
                        amount = amount.ToInt(),
                        factorNumber = order.Id.ToString(),
                        description = $"پرداخت برای سفارش شماره {order.Id}",
                        mobile_number = order.User.PhoneNumber,
                        callback_url = vandarRedirect
                    };

                    var vandarSendResult = await _vandarService.SendAsync(sendRequestInput);
                    if (vandarSendResult != null && vandarSendResult.Status == 1)
                    {
                        order.PaymentStatus = PaymentStatus.InPayment;
                        order.BankResponse.BankSaleReferenceId = vandarSendResult.Token;
                        await _exchangeOrderService.UpdateAsync(order);
                        bankResponded = true;
                        return Redirect($"https://vandar.io/ipg/2step/{vandarSendResult.Token}");
                    }
                    else
                    {
                        var errorMessage = vandarSendResult.Errors != null && vandarSendResult.Errors.Any() ? vandarSendResult.Errors.Join(", ") : "نامشخص";
                        await _messageManager.TelegramAdminMessageAsync($"خطا در هنگام ورود به بانک {BankGatewayType.Vandar.ToDisplay()}: {errorMessage}");
                        order.PaymentStatus = PaymentStatus.NotPaid;
                        await _exchangeOrderService.UpdateAsync(order);
                    }
                }
                catch (Exception ex)
                {
                    ex.LogError();
                    if (retryCount == 3)
                    {
                        await _messageManager.TelegramAdminMessageAsync($"خطا در هنگام ورود به بانک {BankGatewayType.Vandar.ToDisplay()}");
                        order.PaymentStatus = PaymentStatus.NotPaid;
                        await _exchangeOrderService.UpdateAsync(order);
                    }
                }
            }
            return RedirectToAction("ServerError", "Error");
        }

        private async Task<ActionResult> RedirectToPayIrAsync(ExchangeOrder order)
        {
            var amount = AppSettingManager.IsLocale ? 10000 : order.PayAmount * 10;
            var payIrRedirect = Url.Action("PayIr", "ExchangeOrder", null, Request.Url.Scheme).TrimEnd('/');

            //var payIrRedirect = HttpContext.GetBaseUrl() + "/" + Url.Action("PayIr", "ExchangeOrder", new { orderGuid = order.Guid }).TrimEnd('/');
            //payIrRedirect = payIrRedirect.Replace("extoman.com", "extoman.co", StringComparison.InvariantCultureIgnoreCase);
            var retryCount = 0;
            var bankResponded = false;
            while (!bankResponded && retryCount < 3)
            {
                retryCount++;
                try
                {
                    var sendRequestInput = new PayIRSendInput()
                    {
                        amount = amount.ToInt(),
                        factorNumber = order.Id.ToString(),
                        description = $"پرداخت برای سفارش شماره {order.Id}",
                        mobile = order.User.PhoneNumber,
                        redirect = payIrRedirect
                    };
                    var payIrSendResult = await _payIRService.SendAsync(sendRequestInput);

                    if (payIrSendResult != null && payIrSendResult.Status == 1)
                    {
                        order.PaymentStatus = PaymentStatus.InPayment;
                        order.BankResponse.BankSaleReferenceId = payIrSendResult.Token;
                        await _exchangeOrderService.UpdateAsync(order);
                        bankResponded = true;
                        return Redirect($"https://pay.ir/pg/{payIrSendResult.Token}");
                    }
                    else
                    {
                        await _messageManager.TelegramAdminMessageAsync($"خطا در هنگام ورود به بانک {BankGatewayType.Payir.ToDisplay()}: {payIrSendResult.ErrorMessage}");
                        order.PaymentStatus = PaymentStatus.NotPaid;
                        await _exchangeOrderService.UpdateAsync(order);
                    }
                }
                catch (Exception ex)
                {
                    ex.LogError();
                    if (retryCount == 3)
                    {
                        await _messageManager.TelegramAdminMessageAsync($"خطا در هنگام ورود به بانک {BankGatewayType.Payir.ToDisplay()}");
                        order.PaymentStatus = PaymentStatus.NotPaid;
                        await _exchangeOrderService.UpdateAsync(order);
                    }
                }
            }
            return RedirectToAction("ServerError", "Error");
        }

        private async Task DoBankPaymentAsync(BankGatewayType BankGatewayType, ExchangeOrder order)
        {
            var amount = AppSettingManager.IsLocale ? 100 : order.PayAmount;
            var bankCallBackUrl = Url.Action("BankCallBack", "ExchangeOrder", new { id = (byte)BankGatewayType });
            var retryCount = 0;
            var bankResponded = false;
            while (!bankResponded && retryCount < 3)
            {
                retryCount++;
                try
                {
                    switch (BankGatewayType)
                    {
                        case BankGatewayType.BehPardakht:
                            await _mellatService.MakePaymentAsync(amount.ToLong(), $"{order.Id}", order.BankResponse.BankOrderId, bankCallBackUrl, httpContext: HttpContext);
                            break;
                        case BankGatewayType.Sep:
                            await _samanService.MakePaymentAsync(amount.ToLong(), $"{order.Id}", order.BankResponse.BankOrderId, bankCallBackUrl, httpContext: HttpContext);
                            break;
                    }
                    bankResponded = true;
                }
                catch (Exception ex)
                {
                    ex.LogError();
                    if (retryCount == 3)
                    {
                        await _messageManager.TelegramAdminMessageAsync($"خطا در هنگام ورود به بانک {BankGatewayType.ToDisplay()}");
                        order.PaymentStatus = PaymentStatus.NotPaid;
                        await _exchangeOrderService.UpdateAsync(order);
                    }
                }
            }
        }

        private async Task<bool> VerifyPaymentAsync(ExchangeOrder order)
        {
            var success = false;
            var retryCount = 0;
            var bankResponded = false;

            //Check card number is verified
            var isCardVerified = order.User.UserBankCards
                .Where(p => p.VerificationStatus == VerificationStatus.Confirmed)
                .Any(x => string.Join("******", x.CardNumber.Left(6), x.CardNumber.Right(4)).Equals(order.BankResponse.BankCardHolderPan));

            if (isCardVerified)
            {
                while (!bankResponded && retryCount < 3)
                {
                    retryCount++;
                    try
                    {
                        switch (order.BankResponse.BankGatewayType)
                        {
                            case BankGatewayType.BehPardakht:
                                var verifyRequest = await _mellatService.VerifyRequestAsync(order.Id, order.BankResponse.BankOrderId, Convert.ToInt64(order.BankResponse.BankSaleReferenceId));
                                order.BankResponse.BankResponseCode = (int)verifyRequest;
                                success = verifyRequest == MellatResponseMessage.Success;
                                break;
                            case BankGatewayType.Sep:
                                var verifyTransaction = await _samanService.VerifyTransactionAsync(order.BankResponse.BankSaleReferenceId);
                                order.BankResponse.BankResponseCode = (int)verifyTransaction.Status;
                                success = verifyTransaction.Status == SamanMessageStatus.Success && verifyTransaction.AmountToman == order.PayAmount;
                                break;
                            default:
                                success = false;
                                break;
                        }
                        bankResponded = true;
                        order.PaymentStatus = success ? PaymentStatus.Paid : PaymentStatus.NotVerified;
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                        await _messageManager.TelegramAdminMessageAsync($"خطا در هنگام تایید پرداخت سفارش شماره {order.Id} از درگاه بانک {order.BankResponse.BankGatewayType.ToDisplay()} به شماره پیگیری {order.BankResponse.BankSaleReferenceId}");
                        switch (retryCount)
                        {
                            case 1:
                                await _messageManager.TelegramAdminMessageAsync($"تلاش مجدد برای تایید پرداخت سفارش شماره {order.Id} برای دفعه دوم در حال انجام می باشد.");
                                break;
                            case 2:
                                await _messageManager.TelegramAdminMessageAsync($"تلاش مجدد برای تایید پرداخت سفارش شماره {order.Id} برای دفعه سوم در حال انجام می باشد.");
                                break;
                            case 3:
                                await _messageManager.TelegramAdminMessageAsync($"تلاش مجدد برای تایید پرداخت سفارش شماره {order.Id} برای دفعه سوم هم با خطای بانک مواجه شد و مبلغ واریزی مشتری به حساب بازگشت داده شد.");
                                break;
                        }
                    }
                }
            }
            else
            {
                order.PaymentStatus = PaymentStatus.CardNotValid;
                order.OrderStatus = OrderStatus.RefundNotValidCard;
            }
            await _exchangeOrderService.UpdateAsync(order);
            return success;
        }

        private bool PaymentSuccessed(ExchangeOrder order)
        {
            switch (order.BankResponse.BankGatewayType)
            {
                case BankGatewayType.BehPardakht:
                    return order.BankResponse.BankResponseCode == MellatResponseMessage.Success.ToInt();
                case BankGatewayType.Sep:
                    return order.BankResponse.BankResponseCode == SamanMessageStatus.Success.ToInt();
                default:
                    return false;
            }
        }

        private bool HasReferenceValue(BankGatewayType bankType, MellatCallBack mellatCallBack, SamanCallBack samanCallBack)
        {
            switch (bankType)
            {
                case BankGatewayType.BehPardakht:
                    return mellatCallBack.saleReferenceId.HasValue;
                case BankGatewayType.Sep:
                    return samanCallBack.RefNum.HasValue();
                default:
                    return false;
            }
        }
        private async Task<ExchangeOrder> FindAndUpdateOrderAsync(BankGatewayType bankGatewayType, MellatCallBack mellatCallBack, SamanCallBack samanCallBack)
        {
            ExchangeOrder order;
            switch (bankGatewayType)
            {
                case BankGatewayType.BehPardakht:
                    order = await _exchangeOrderService.Table
                        .Where(p => p.BankResponse.BankOrderId == mellatCallBack.saleOrderId)
                        .Include(p => p.User)
                        .Include(p => p.User.UserBankCards)
                        .Include(p => p.BankResponse)
                        .Include(p => p.Exchange)
                        .Include(p => p.Exchange.FromCurrency)
                        .Include(p => p.Exchange.ToCurrency)
                        .Include(p => p.OrderWithdraws)
                        .Include(p => p.OrderTrades)
                        .SingleAsync();
                    order.BankResponse.BankRefId = mellatCallBack.refId;
                    order.BankResponse.BankSaleReferenceId = mellatCallBack.saleReferenceId.ToString();
                    order.BankResponse.BankResponseCode = mellatCallBack.resCode;
                    order.BankResponse.BankCardHolderInfo = mellatCallBack.cardHolderInfo;
                    order.BankResponse.BankCardHolderPan = mellatCallBack.cardHolderPan;
                    if (order.BankResponse.BankResponseCode == MellatResponseMessage.Success.ToInt() && order.PaymentStatus != PaymentStatus.Paid)
                        order.PaymentStatus = PaymentStatus.NotVerified;
                    else if (order.BankResponse.BankResponseCode != MellatResponseMessage.Success.ToInt())
                        order.PaymentStatus = PaymentStatus.NotPaid;
                    else if (order.BankResponse.BankResponseCode == MellatResponseMessage.UserCanceledTransaction.ToInt())
                        order.PaymentStatus = PaymentStatus.Canceled;

                    break;
                case BankGatewayType.Sep:
                    order = await _exchangeOrderService.Table
                        .Where(p => p.BankResponse.BankOrderId == samanCallBack.ResNum)
                        .Include(p => p.User)
                        .Include(p => p.User.UserBankCards)
                        .Include(p => p.BankResponse)
                        .Include(p => p.Exchange)
                        .Include(p => p.Exchange.FromCurrency)
                        .Include(p => p.Exchange.ToCurrency)
                        .Include(p => p.OrderWithdraws)
                        .Include(p => p.OrderTrades)
                        .SingleAsync();
                    order.BankResponse.BankSaleReferenceId = samanCallBack.RefNum;
                    order.BankResponse.BankResponseCode = samanCallBack.StateCode;
                    if (order.BankResponse.BankResponseCode == SamanMessageStatus.Success.ToInt() && order.PaymentStatus != PaymentStatus.Paid)
                        order.PaymentStatus = PaymentStatus.NotVerified;
                    else
                        order.PaymentStatus = PaymentStatus.NotPaid;

                    break;
                default:
                    throw new ArgumentException("درگاه بانکی نامشخص!");
            }
            order.BankResponse.BankGatewayType = bankGatewayType;
            await _exchangeOrderService.UpdateAsync(order);
            return order;
        }

        private bool IsPerfectMoneyPaymentValid(PerfectMoneyPaymentStatus status, decimal orderPayAmount)
        {
            var statusPaymentAmount = status.PAYMENT_AMOUNT.ToDecimal().ToString("{0:G29}");
            if (!status.PAYEE_ACCOUNT.Equals(AppSettingManager.PerfectMoney_USD_Account) || !statusPaymentAmount.Equals(orderPayAmount.ToString("{0:G29}")))
                return false;

            var v2hash = string.Join(":", new string[] {
                status.PAYMENT_ID,
                status.PAYEE_ACCOUNT,
                status.PAYMENT_AMOUNT,
                status.PAYMENT_UNITS,
                status.PAYMENT_BATCH_NUM,
                status.PAYER_ACCOUNT,
                AppSettingManager.PerfectMoney_Alt_Passphere.CalculateMD5Hash().ToUpper(), // Alt Passphere hash
                status.TIMESTAMPGMT
            })
            .CalculateMD5Hash()
            .ToUpper();

            return v2hash == status.V2_HASH;
        }
        #endregion
    }
}