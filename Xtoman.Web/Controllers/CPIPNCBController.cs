﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Payment.CoinPayments;
using Xtoman.Framework;
using Xtoman.Service;
using Xtoman.Service.WebServices;
using Xtoman.Utility;
using Xtoman.Utility.PostalEmail;

namespace Xtoman.Web.Controllers
{
    public class CPIPNCBController : BaseController
    {
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly ICoinPaymentsIPNResultService _coinPaymentsIPNResultService;
        private readonly ITradeManager _tradeManager;
        private readonly ITradeService _tradeService;
        private readonly IMessageManager _messageManager;
        private readonly IWithdrawManager _withdrawManager;
        private readonly IPriceManager _priceManager;
        private readonly IBalanceManager _balanceManager;
        public CPIPNCBController(
            IExchangeOrderService exchangeOrderService,
            ICoinPaymentsIPNResultService coinPaymentsIPNResultService,
            ITradeService tradeService,
            ITradeManager tradeManager,
            IMessageManager messageManager,
            IWithdrawManager withdrawManager,
            IPriceManager priceManager,
            IBalanceManager balanceManager)
        {
            _exchangeOrderService = exchangeOrderService;
            _coinPaymentsIPNResultService = coinPaymentsIPNResultService;
            _tradeService = tradeService;
            _tradeManager = tradeManager;
            _messageManager = messageManager;
            _withdrawManager = withdrawManager;
            _priceManager = priceManager;
            _balanceManager = balanceManager;
        }

        [HttpPost]
        public virtual async Task<ActionResult> Index()
        {
            LogMessage("CoinPayments IPN Request Recevied");
            var ipnType = HttpContext.Request.Form["ipn_type"] != null ? HttpContext.Request.Form["ipn_type"].ToLower() : "";
            var hmac = HttpContext.Request.Headers["HMAC"];
            if (hmac == null)
                return LogAndReturnResult(HttpStatusCode.BadRequest, "HMAC Not Presented!");
            var source = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Request.InputStream, Encoding.UTF8))
                source = reader.ReadToEnd();

            var merchant = HttpContext.Request.Form["merchant"];
            if (!merchant.HasValue())
                return LogAndReturnResult(HttpStatusCode.BadRequest, "Merchant ID Not Presented!");

            if (!CoinPaymentsCryptoUtil.SignIsValid(source, hmac, merchant))
                return LogAndReturnResult(HttpStatusCode.BadRequest, "Invalid HMAC / MerchantId");

            if (ipnType.HasValue())
                switch (ipnType)
                {
                    case "api":
                        var IPNApiResult = CoinPaymentsCryptoUtil.ParseIPNResult<IPNAPIResult>(HttpContext.Request.Form);
                        LogMessage($"API IPN: {IPNApiResult.Serialize()}");
                        return await APIAsync(IPNApiResult, source, hmac);
                    case "deposit": // This is the right api.
                        var IPNDepositResult = CoinPaymentsCryptoUtil.ParseIPNResult<IPNDepositResult>(HttpContext.Request.Form);
                        LogMessage($"Deposit IPN: {IPNDepositResult.Serialize()}");
                        return await DepositAsync(IPNDepositResult, source, hmac);
                    case "withdrawal":
                        var IPNWithdrawalResult = CoinPaymentsCryptoUtil.ParseIPNResult<IPNWithdrawalResult>(HttpContext.Request.Form);
                        LogMessage($"Withdrawal IPN: {IPNWithdrawalResult.Serialize()}");
                        return await WithdrawalAsync(IPNWithdrawalResult, source, hmac);
                }
            return HttpNotFound();
        }

        public virtual async Task<ActionResult> DepositAsync(IPNDepositResult result, string source, string hmac)
        {
            var now = DateTime.UtcNow;

            var coinPaymentsIPNResult = PrepareCoinPaymentsDepositIPNResult(result);
            await _coinPaymentsIPNResultService.InsertAsync(coinPaymentsIPNResult);

            var order = await _exchangeOrderService.Table
                .Where(p => p.PayAddress == result.address && p.PayMemoTagPaymentId == result.dest_tag)
                .Include(p => p.User)
                .Include(p => p.OrderTrades)
                .FirstOrDefaultAsync();

            if (order == null)
                return LogAndReturnResult(HttpStatusCode.OK, "The order with txn_id wasn't found.");

            if (CheckForDuplicate(order)) // This is so important to prevent double spending!
                return LogAndReturnResult(HttpStatusCode.OK, "Duplicate transactions");

            var paidAmount = result.amount.TryToDecimal();
            if (result.SuccessStatusLax(paidAmount))
            {
                order.PaidAmount = paidAmount;
                if (paidAmount != order.PayAmount)
                {
                    var newReceiveAmount = paidAmount * order.ReceiveAmount / order.PayAmount;
                    // Should update amount of order
                    order.ReceiveAmount = newReceiveAmount;
                }

                order.PaymentStatus = PaymentStatus.Paid;
                order.PaymentDate = now;
                order.PayTransactionId = result.txn_id;
                await _exchangeOrderService.UpdateAsync(order);

                var isExpired = order.ExpireDate.HasValue ? order.ExpireDate <= now : false;
                if (isExpired)
                {
                    var newReceiveAmount = await _priceManager.ReCalculateReceiveAmountAsync(order);
                    if (newReceiveAmount != 0 && newReceiveAmount < order.ReceiveAmount)
                    {
                        order.ReceiveAmount = newReceiveAmount;
                        await _exchangeOrderService.UpdateAsync(order);
                    }
                }

                switch (order.Exchange.ExchangeFiatCoinType)
                {
                    case ExchangeFiatCoinType.CoinToCoin:
                        await CoinToCoinOrderAsync(order);
                        //UNDONE: Exchange crypto(coin) to crypto(coin) in coinPayments or binance
                        break;
                    case ExchangeFiatCoinType.CoinToFiatUSD:
                        await TradeCoinForOrderAsync(order);
                        //UNDONE: Complete PerfectMoney, Payeer or WebMoney transfer
                        await CompleteCoinToFiatUSDOrderAsync(order);
                        break;
                    case ExchangeFiatCoinType.CoinToFiatToman:
                        // Add a queue trade for order
                        await TradeCoinForOrderAsync(order);
                        // Manually complete : Do nothing here! or User Finnotech to Withdraw toman to sheba number
                        break;
                }

                var fullName = order.User.FullName ?? order.User.UserName;
                var orderGuid = order.Guid.ToString();
                var coinPaymentsEmail = new CoinPaymentsPaidEmail()
                {
                    To = order.User.Email ?? order.User.UserName,
                    Currency = result.currency,
                    Fullname = fullName,
                    TransactionId = result.txn_id,
                    ViewName = "CoinPaymentsPaid",
                    From = "no-reply@extoman.com",
                    Subject = "پرداخت شما به اکس تومن انجام شد",
                    Username = order.User.UserName,
                    Amount = result.amount.TryToDecimal(),
                    OrderGuid = orderGuid
                };
                var emailBody = coinPaymentsEmail.GetMailMessage().Body;
                await _messageManager.CoinPaymentsPaidAsync(emailBody, order.User.Email, order.User.PhoneNumber, fullName, orderGuid, result.amount.TryToDecimal() + " " + coinPaymentsEmail.Currency);
            }
            else
            {
                switch (result.status)
                {
                    case -1:
                        order.PaymentStatus = PaymentStatus.Canceled;
                        break;
                    case 0:
                        order.PaymentStatus = PaymentStatus.InPayment;
                        break;
                }
            }
            await _exchangeOrderService.UpdateAsync(order);
            return LogAndReturnResult(HttpStatusCode.OK, "1");
        }

        private async Task CoinToCoinOrderAsync(ExchangeOrder order)
        {
            order.OrderStatus = OrderStatus.Processing;
            await _exchangeOrderService.UpdateAsync(order);
            // 1 - Withdraw from CoinPayments.net to Binance
            var (withrawFromCoinPaymentsStatus, withrawFromCoinPaymentsSText) = await _withdrawManager.WithdrawFromCoinPaymentsToBinanceAsync(order.Exchange.FromCurrency.Type);
            if (withrawFromCoinPaymentsStatus)
                await _messageManager.TelegramAdminMessageAsync(withrawFromCoinPaymentsSText);
            // 2 - Add trade for order
            var coinTradeResult = await _tradeManager.CoinToCoinTradeOrderAsync(order);
            // 3 - Calculate trade to amount (minus extoman profit)
            // 4 - Withdraw after trade completed
        }

        private async Task TradeCoinForOrderAsync(ExchangeOrder order)
        {
            try
            {
                //Calculate available balance for selling asset
                var fromCurrencyBinanceBalance = await _balanceManager.BinanceCurrenyBalanceAsync(order.Exchange.FromCurrency.Type);
                var assetSellQueueAmount = await _tradeService.TableNoTracking
                    .Where(p => p.TradeStatus == TradeStatus.Queue && p.FromCryptoType == order.Exchange.FromCurrency.Type)
                    .Select(p => p.FromAmount).DefaultIfEmpty(0).SumAsync();

                fromCurrencyBinanceBalance -= assetSellQueueAmount;

                //Estimate to sell amount
                var estimatedTradeQty = order.PaidAmount - (order.PaidAmount * 0.5M / 100);

                //Check if have to withdraw and minus withdraw fee
                var minusTransactionFee = estimatedTradeQty > fromCurrencyBinanceBalance;

                //Insert sell trade to queue and get trade quanity amount
                var tradeQty = await _tradeManager.SellCryptoTradeOrderAsync(order, minusTransactionFee);

                //If trade quanity is more than available balance in binance withdraw from coinpayments to binance
                if ((tradeQty > 0 && tradeQty > fromCurrencyBinanceBalance) || (order.Exchange.FromCurrency.Type != ECurrencyType.Bitcoin && order.Exchange.FromCurrency.Type != ECurrencyType.Tether))
                {
                    var (status, text) = await _withdrawManager.WithdrawFromCoinPaymentsToBinanceAsync(order.Exchange.FromCurrency.Type);
                    if (text.HasValue())
                        await _messageManager.TelegramAdminMessageAsync(text);
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
                await _messageManager.TelegramAdminMessageAsync(ex.Message);
            }
        }

        public virtual async Task<ActionResult> WithdrawalAsync(IPNWithdrawalResult result, string source, string hmac)
        {
            var coinPaymentsIPNResult = PrepareCoinPaymentsWithdrawalIPNResult(result);
            await _coinPaymentsIPNResultService.InsertAsync(coinPaymentsIPNResult);

            return LogAndReturnResult(HttpStatusCode.OK, "1");
        }

        public virtual async Task<ActionResult> APIAsync(IPNAPIResult result, string source, string hmac)
        {
            var coinPaymentsIPNResult = PrepareCoinPaymentsAPIIPNResult(result);
            await _coinPaymentsIPNResultService.InsertAsync(coinPaymentsIPNResult);

            var order = await _exchangeOrderService.Table
                .Where(p => p.PayPaymentGatewayId == result.txn_id)
                .Include(p => p.User)
                .Include(p => p.OrderTrades)
                .FirstOrDefaultAsync();

            if (order == null)
                return LogAndReturnResult(HttpStatusCode.OK, "The order with txn_id wasn't found.");

            if (CheckForDuplicate(order)) // This is so important to prevent double spending!
                return LogAndReturnResult(HttpStatusCode.OK, "Duplicate transactions");

            if (result.SuccessStatusLax(order.PayAmount, result.received_amount.TryToDecimal()))
            {
                order.PaymentStatus = PaymentStatus.Paid;
                order.PaymentDate = DateTime.UtcNow;
                order.PayTransactionId = result.send_tx;
                await _exchangeOrderService.UpdateAsync(order);
                switch (order.Exchange.ExchangeFiatCoinType)
                {
                    case ExchangeFiatCoinType.CoinToCoin:
                        //UNDONE: Exchange crypto(coin) to crypto(coin) in coinPayments or binance
                        break;
                    case ExchangeFiatCoinType.CoinToFiatUSD:
                        await TradeCoinForOrderAsync(order);
                        //UNDONE: Complete PerfectMoney, Payeer or WebMoney transfer
                        await CompleteCoinToFiatUSDOrderAsync(order);
                        break;
                    case ExchangeFiatCoinType.CoinToFiatToman:
                        // Add a queue trade for order
                        // Withdraw coin from CoinPayments.net Automaticaly
                        await TradeCoinForOrderAsync(order);
                        // Manually complete : Do nothing here!
                        break;
                }

                var fullName = order.User.FullName ?? order.User.UserName;
                var orderGuid = order.Guid.ToString();
                var coinPaymentsEmail = new CoinPaymentsPaidEmail()
                {
                    To = order.User.Email ?? order.User.UserName,
                    Currency = result.currency1 ?? result.currency2,
                    Fullname = fullName,
                    TransactionId = result.send_tx,
                    ViewName = "CoinPaymentsPaid",
                    From = "no-reply@extoman.com",
                    Subject = "پرداخت شما به اکس تومن انجام شد",
                    Username = order.User.UserName,
                    Amount = result.received_amount.TryToDecimal(),
                    OrderGuid = orderGuid
                };
                var emailBody = coinPaymentsEmail.GetMailMessage().Body;
                await _messageManager.CoinPaymentsPaidAsync(emailBody, order.User.Email, order.User.PhoneNumber, fullName, orderGuid, result.received_amount.TryToDecimal() + " " + coinPaymentsEmail.Currency);
            }
            else
            {
                switch (result.status)
                {
                    case -1:
                        order.PaymentStatus = PaymentStatus.Canceled;
                        break;
                    case 0:
                        if (result.received_confirms.TryToInt() > 0)
                            order.PaymentStatus = PaymentStatus.InPayment;
                        break;
                }
            }
            await _exchangeOrderService.UpdateAsync(order);

            return LogAndReturnResult(HttpStatusCode.OK, "1");
        }

        private async Task CompleteCoinToFiatUSDOrderAsync(ExchangeOrder order)
        {
            try
            {
                order.OrderStatus = OrderStatus.Processing;
                order.Errors = null;
                await _exchangeOrderService.UpdateAsync(order);
                var (withdrawSuccess, withdrawMessage) = await _withdrawManager.WithdrawAsync(order);
                await _messageManager.TelegramAdminMessageAsync(withdrawMessage);
                if (withdrawSuccess)
                    order.OrderStatus = OrderStatus.Complete;
                else
                    order.OrderStatus = OrderStatus.WithdrawFailed;
                await _exchangeOrderService.UpdateAsync(order);
            }
            catch (Exception ex)
            {
                ex.LogError();
                await _messageManager.TelegramAdminMessageAsync($"خطا هنگام تکمیل مراحل سفارش خرید پرفکت با شتاب | {ex.Message}");
                order.OrderStatus = OrderStatus.InComplete;
            }
        }
        #region Helpers
        private CoinPaymentsIPNResult PrepareCoinPaymentsDepositIPNResult(IPNDepositResult result)
        {
            return new CoinPaymentsIPNResult()
            {
                Address = result.address,
                Amount = result.amount,
                Amounti = result.amounti,
                Currency = result.currency,
                IpnId = result.ipn_id,
                IpnMode = result.ipn_mode,
                IpnType = result.ipn_type,
                TxnId = result.txn_id,
                IpnVersion = result.ipn_version,
                StatusText = result.status_text,
                Status = result.status,
                Confirms = result.confirms,
                Fee = result.fee,
                Feei = result.feei,
                Merchant = result.merchant
            };
        }

        private CoinPaymentsIPNResult PrepareCoinPaymentsWithdrawalIPNResult(IPNWithdrawalResult result)
        {
            return new CoinPaymentsIPNResult()
            {
                Address = result.address,
                Amount = result.amount,
                Amounti = result.amounti,
                Currency = result.currency,
                IpnId = result.ipn_id,
                IpnMode = result.ipn_mode,
                IpnType = result.ipn_type,
                TxnId = result.txn_id,
                IpnVersion = result.ipn_version,
                StatusText = result.status_text,
                Status = result.status,
                Merchant = result.merchant,
                CId = result.id,
            };
        }

        private CoinPaymentsIPNResult PrepareCoinPaymentsAPIIPNResult(IPNAPIResult result)
        {
            return new CoinPaymentsIPNResult()
            {
                Amount = result.received_amount.HasValue() ? result.received_amount.TryToDecimal() : 0,
                Amount1 = result.amount1,
                Amount2 = result.amount2,
                BuyerName = result.buyer_name,
                Confirms = result.received_confirms.TryToInt(),
                Currency = result.currency1,
                Currency1 = result.currency1,
                Currency2 = result.currency2,
                IpnId = result.ipn_id,
                IpnMode = result.ipn_mode,
                IpnType = result.ipn_type,
                IpnVersion = result.ipn_version,
                ReceivedConfirms = result.received_confirms,
                Merchant = result.merchant,
                TxnId = result.txn_id,
                StatusText = result.status_text,
                Status = result.status,
                SendTx = result.send_tx,
                ReceivedAmount = result.received_amount,
                ItemNumber = result.item_number,
                ItemName = result.item_name,
                Invoice = result.invoice,
                Fee = result.fee.TryToDecimal(),
                Email = result.email,
                Custom = result.custom
            };
        }

        private bool CheckForDuplicate(/*IPNAPIResult result, */ExchangeOrder order)
        {
            return order.PaymentStatus == PaymentStatus.Paid || order.OrderStatus == OrderStatus.Complete;
            //var ipnResult = await _coinPaymentsIPNResultService.TableNoTracking
            //    .Where(p => p.IpnId == result.ipn_id).ToListAsync();

            //if (ipnResult == null || !ipnResult.Any()) return true;
            //else
            //{
            //    if (ipnResult.Any(x => x.Status == 100))
            //        return true;
            //    if (ipnResult.Any(x => x.ReceivedAmount.HasValue()) && ipnResult.Any(x => x.ReceivedAmount.TryToDecimal() > 0))
            //        return true;
            //    if (ipnResult.Any(x => x.ReceivedConfirms.TryToInt() >= 2))
            //        return true;
            //}
            //return false;
        }
        #endregion
    }
}