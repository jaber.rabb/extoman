﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.WebServices;
using Xtoman.Framework;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Web.Controllers
{
    public class VandarController : BaseController
    {
        private readonly IVandarService _vandarService;
        public VandarController(IVandarService vandarService)
        {
            _vandarService = vandarService;
        }

        public async Task<ActionResult> Index()
        {
            var sendResult = await _vandarService.SendAsync(new VandarSendInput()
            {
                amount = 2000,
                mobile_number = "09136588879",
                factorNumber = "test factor number",
                description = "test description",
                callback_url = Url.Action("Callback", "Vandar", null, Request.Url.Scheme).TrimEnd('/')
            });

            if (sendResult != null && sendResult.Status == 1)
                return Redirect($"https://vandar.io/ipg/2step/{sendResult.Token}");

            return Json(sendResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> Callback(string token, VandarCallback model)
        {
            if (model.CardNumber.HasValue())
            {
                var confirmResult = await _vandarService.ConfirmAsync(new VandarConfirmInput() { confirm = true, token = token });
                if (confirmResult.Status == 1)
                {
                    await Task.Delay(1000);
                    var verifyResult = await _vandarService.VandarVerifyAsync(new VandarVerifyInput()
                    {
                        token = token
                    });
                    return Json(verifyResult);
                }
                return Json(confirmResult);
            }
            else
            {
                var confirmResult = await _vandarService.ConfirmAsync(new VandarConfirmInput() { confirm = false, token = token });
                return Json(confirmResult);
            }
        }
    }
}