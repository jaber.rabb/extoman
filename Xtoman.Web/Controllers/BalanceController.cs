﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Service;
using Xtoman.Service.WebServices;
using Xtoman.Utility;
using Xtoman.Web.ViewModels;

namespace Xtoman.Web.Controllers
{
    public class BalanceController : Controller
    {
        private readonly IBalanceManager _balanceManager;
        private readonly ICurrencyTypeService _currencyTypeService;
        private readonly IBinanceService _binanceService;
        private readonly IKrakenService _krakenService;
        private readonly IExchangeOrderService _exchangeOrderService;
        public BalanceController(
            IBalanceManager balanceManager,
            ICurrencyTypeService currencyTypeService,
            IBinanceService binanceService,
            IKrakenService krakenService,
            IExchangeOrderService exchangeOrderService)
        {
            _balanceManager = balanceManager;
            _currencyTypeService = currencyTypeService;
            _binanceService = binanceService;
            _krakenService = krakenService;
            _exchangeOrderService = exchangeOrderService;
        }

        [OutputCache(Duration = 100)]
        public async Task<ActionResult> Index()
        {
            var availableBalances = await _balanceManager.GetAllBalancesAsync();
            var model = availableBalances.Select(p => new BalanceViewModel()
            {
                Available = p.Amount,
                Name = p.Name,
                UnitSign = p.Type.ToDisplay(DisplayProperty.ShortName)
            })
            .ToList();
            return Json(model);
        }
    }
}