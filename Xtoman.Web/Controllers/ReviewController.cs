﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Framework;
using Xtoman.Framework.Mvc;
using Xtoman.Service;
using Xtoman.Utility;

namespace Xtoman.Web.Controllers
{
    public class ReviewController : BaseController
    {
        #region Properties
        //private readonly IOrderReviewService _reviewService;
        private readonly IOrderService _orderService;
        #endregion
        public ReviewController(
            //IOrderReviewService reviewService,
            IOrderService orderService)
        {
            //_reviewService = reviewService;
            _orderService = orderService;
        }

        [AjaxOnly]
        [HttpPost]
        public async Task<ActionResult> Send(Guid OrderGuid, string Text, byte Rate)
        {
            var result = new ResultModel();
            try
            {
                var order = await _orderService.Table.Where(p => p.Guid == OrderGuid).FirstOrDefaultAsync();
                if (order.Review == null || order.Review.VoteRate == 0 || !order.Review.Text.HasValue())
                {
                    order.Review = new OrderReview()
                    {
                        Text = Text,
                        VoteRate = Rate
                    };
                    await _orderService.UpdateAsync(order);
                }
                result.Text = $"نظر برای سفارش {OrderGuid} ثبت گردید.";
                result.Status = true;
            }
            catch (Exception ex)
            {
                ex.LogError();
                result.Text = "خطایی رخ داد!";
            }
            return Json(result);
        }
    }
}