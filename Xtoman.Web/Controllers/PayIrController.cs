﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.WebServices.Payment.PayIR;
using Xtoman.Framework;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Web.Controllers
{
    public class PayIrController : BaseController
    {
        private readonly IPayIRService _payIRService;
        public PayIrController(
            IPayIRService payIRService)
        {
            _payIRService = payIRService;
        }

        public async Task<ActionResult> Index(byte status = 0, string token = "")
        {
            var result = $"PayIR Test Status = {status}, Token = {token}";
            if (status == 1 && token.HasValue())
            {
                var verifyResult = await _payIRService.VerifyAsync(new PayIRVerifyInput() { token = token });
                result += $" Verify Result = ({verifyResult.Serialize()})";
            }
            new Exception(result).LogError();
            return Content(result);
        }

        public async Task<ActionResult> Test()
        {
            var amount = 10000;
            var payIrRedirect = Url.Action("Index", "PayIr", null, Request.Url.Scheme).TrimEnd('/');
            var sendRequestInput = new PayIRSendInput()
            {
                amount = amount,
                factorNumber = "382913213012",
                description = $"پرداخت برای سفارش شماره تست",
                mobile = "09136588879",
                redirect = payIrRedirect
            };

            try
            {
                var payIrSendResult = await _payIRService.SendAsync(sendRequestInput);

                if (payIrSendResult != null && payIrSendResult.Status == 1)
                    return Redirect($"https://pay.ir/pg/{payIrSendResult.Token}");

                return Content($"Problem");
            }
            catch (Exception ex)
            {
                return Content(ex.Message + " " + ex?.InnerException?.Message);
            }
        }
    }
}