﻿using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Framework;
using Xtoman.Service;
using Xtoman.Web.ViewModels;

namespace Xtoman.Web.Controllers
{
    public class FAQController : BaseController
    {
        private readonly IFAQService _faqService;
        public FAQController(IFAQService fAQService)
        {
            _faqService = fAQService;
        }
        public virtual async Task<ActionResult> Index()
        {
            var faqs = await _faqService.GetAllCachedAsync();
            var model = faqs.AsQueryable().ProjectToList<FAQViewModel>();
            return View(model);
        }
    }
}