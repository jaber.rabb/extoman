﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Framework;
using Xtoman.Service.WebServices;

namespace Xtoman.Web.Controllers
{
    public class FinnotechController : BaseController
    {
        private readonly IFinnotechService _finnotechService;
        public FinnotechController(IFinnotechService finnotechService)
        {
            _finnotechService = finnotechService;
        }
        public async Task<ActionResult> Index()
        {

            return View();
        }
    }
}