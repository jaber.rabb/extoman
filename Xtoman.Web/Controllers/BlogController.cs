﻿using AutoMapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Framework;
using Xtoman.Framework.Core;
using Xtoman.Service;
using Xtoman.Utility;
using Xtoman.Web.ViewModels;

namespace Xtoman.Web.Controllers
{
    public class BlogController : BaseController
    {
        #region Properties
        private readonly IPostService _postService;
        private readonly IPostCategoryService _postCategoryService;
        private readonly BlogSettings _blogSettings;
        private readonly CommentSettings _commentSettings;
        private int PageNumber = 1;
        private readonly string ViewModeCookieName = "BlogViewMode";
        private readonly int CommentSize = 7;
        private int PageSize = 10;
        private readonly string ViewMode = "grid";
        #endregion

        #region Constructor
        public BlogController(
            IPostService postService,
            IPostCategoryService postCategoryService,
            BlogSettings blogSettings,
            CommentSettings commentSettings)
        {
            _postService = postService;
            _postCategoryService = postCategoryService;
            _blogSettings = blogSettings;
            _commentSettings = commentSettings;
            if (_commentSettings.PostCommentVisiblesCount > 0)
                CommentSize = _commentSettings.PostCommentVisiblesCount;
            if (_blogSettings.BlogPageSize > 0)
                PageSize = _blogSettings.BlogPageSize;
        }
        #endregion

        #region Actions
        public async Task<ActionResult> Index(string parameters)
        {
            var mode = Request.Cookies[ViewModeCookieName]?.Value ?? ViewMode;
            ViewBag.ViewMode = mode;

            var query = PrepareQuery();
            var postQuery = PrepareIndexPostsQuery(query, parameters);

            var model = new BlogIndexViewModel()
            {
                Posts = await PrepareIndexPostsViewModelAsync(postQuery),
                Total = await postQuery.CountAsync()
            };

            if (Request.IsPjaxRequest())
                return PartialView(mode == "list" ? "_List" : "_Grid", model);

            //Sidebar
            model.Sidebar.PopularPosts = await PreparePopularPostsViewModelAsync(query);
            model.Sidebar.Categories = await PrepareCategoriesViewModelAsync();
            return View(model);
        }

        public async Task<ActionResult> Single(string url)
        {
            var postQuery = PrepareTableQuery();
            var post = await postQuery.Where(p => p.Url.Equals(url)).SingleAsync();
                //.ProjectToSingleOrDefaultAsync<BlogSinglePostViewModel>();
            post.VisitCount++;
            await _postService.UpdateAsync(post);

            var postModel = new BlogSinglePostViewModel().FromEntity(post);

            if (postModel == null)
                return RedirectToAction("Index");

            PreparePostTitle(postModel);
            PrepareInHtmlPostImage(postModel);
            PreparePostImage(postModel, "post.large");

            var model = new BlogSingleViewModel()
            {
                Post = postModel,
                Sidebar = new BlogSidebarItemsViewModel()
                {
                    Categories = await PrepareCategoriesViewModelAsync(),
                    PopularPosts = await PreparePopularPostsViewModelAsync(postQuery),
                    LatestPosts = await PrepareLatestPostsViewModelAsync(postQuery)
                }
            };

            return View(model);
        }

        private void PrepareInHtmlPostImage(BlogSinglePostViewModel postModel)
        {
            postModel.Text = postModel.Text.Replace("/static", "https://static." + DomainName);
        }
        #endregion

        #region Helpers
        private void PreparePostTitle(BlogSinglePostViewModel postModel)
        {
            var matchpattern = @"\s?<h[1](.*?)>[^<]+</h[1]>\s?";
            //var h1Title = $@"<h1 class=""page-title""><span>{postModel.Title}</span></h1>";
            //if (Regex.Match(postModel.Text, matchpattern).Success)
            postModel.Text = Regex.Replace(postModel.Text, matchpattern, "");
            //else
            //    postModel.Text = h1Title + postModel.Text;
        }

        private IQueryable<Post> PrepareTableQuery()
        {
            return _postService.Table
                .Include(p => p.Media)
                .Include(p => p.Media.Thumbnails)
                .Include(p => p.Media.Thumbnails.Select(x => x.MediaThumbnailSize))
                .Where(p => p.Enabled)
                .Where(p => p.PublishDate != null && p.PublishDate <= DateTime.UtcNow);
        }

        private IQueryable<Post> PrepareQuery()
        {
            return _postService.TableNoTracking
                .Include(p => p.Media)
                .Include(p => p.Media.Thumbnails)
                .Include(p => p.Media.Thumbnails.Select(x => x.MediaThumbnailSize))
                .Where(p => p.Enabled)
                .Where(p => p.PublishDate != null && p.PublishDate <= DateTime.UtcNow);
        }
        private IQueryable<Post> PrepareIndexPostsQuery(IQueryable<Post> postQuery, string parameters)
        {
            var defOrder = true;
            if (parameters.HasValue())
            {
                foreach (var segment in parameters.Split('/'))
                {
                    var key = segment.Split('-')[0].ToLower();
                    var value = segment.Split('-')[1].ToLower();
                    switch (key)
                    {
                        case "sort":
                            defOrder = false;
                            switch (value)
                            {
                                case "date_desc":
                                    postQuery = postQuery.OrderByDescending(p => p.PublishDate);
                                    break;
                                case "date_asc":
                                    postQuery = postQuery.OrderBy(p => p.PublishDate);
                                    break;
                                case "view_desc":
                                    postQuery = postQuery.OrderByDescending(p => p.VisitCount);
                                    break;
                                case "view_asc":
                                    postQuery = postQuery.OrderBy(p => p.VisitCount);
                                    break;
                            }
                            ViewBag.CurrentSort = value;
                            break;
                        case "page":
                            PageNumber = int.Parse(value);
                            break;
                        case "size":
                            PageSize = int.Parse(value);
                            break;
                        case "tag":
                            postQuery.Where(p => p.Tags.Any(x => x.Name == value));
                            break;
                        case "category":
                            postQuery.Where(p => p.Categories.Any(x => x.Name == value));
                            break;
                    }
                }

                if (defOrder)
                    postQuery = postQuery.OrderByDescending(x => x.PublishDate);
            }
            else
                postQuery = postQuery.OrderByDescending(x => x.PublishDate);

            return postQuery;
        }
        private async Task<List<BlogSmallPostItemViewModel>> PreparePopularPostsViewModelAsync(IQueryable<Post> postQuery, int? count = null)
        {
            var posts = await postQuery.Where(p => p.PostType == PostType.Standard)
                                       .OrderByDescending(p => p.VisitCount)
                                       .Take(5)
                                       .ProjectToListAsync<BlogSmallPostItemViewModel>();

            //Popular Posts Image Path
            PrepareSmallPostImage(posts, "post.small");
            return posts;
        }

        private async Task<List<BlogSmallPostItemViewModel>> PrepareLatestPostsViewModelAsync(IQueryable<Post> postQuery, int? count = null)
        {
            var posts = await postQuery.Where(p => p.PostType == PostType.Standard)
                                       .OrderByDescending(p => p.PublishDate)
                                       .Take(5)
                                       .ProjectToListAsync<BlogSmallPostItemViewModel>();

            //Popular Posts Image Path
            PrepareSmallPostImage(posts, "post.small");
            return posts;
        }
        private async Task<IPagedList<BlogPostItemViewModel>> PrepareIndexPostsViewModelAsync(IQueryable<Post> postQuery)
        {
            //var posts = await postQuery.ProjectToPagedListAsync<BlogPostItemViewModel>(PageNumber, PageSize,
            //    async (p) => p = await p.GetLocalizedAsync<BlogPostItemViewModel, Post>());
            var posts = await postQuery.ProjectToPagedListAsync<BlogPostItemViewModel>(PageNumber, PageSize);

            //Posts Image Path
            PreparePostImage(posts, "post.medium");

            return posts;
        }

        private void PreparePostImage(BlogSinglePostViewModel model, string sizeName)
        {
            if (model.Media != null && model.Media.Thumbnails != null)
            {
                model.ImageSrc = model.Media.Thumbnails
                    .Where(p => p.MediaThumbnailSizeName == sizeName)
                    .Select(p => p.Url.Replace("/static", "https://static." + DomainName))
                    .FirstOrDefault();
            }
        }
        private void PreparePostImage(IPagedList<BlogPostItemViewModel> modelItems, string sizeName)
        {
            modelItems.ForEach((modelItem) =>
            {
                if (modelItem.Media != null && modelItem.Media.Thumbnails != null)
                    modelItem.ImageSrc = modelItem.Media.Thumbnails
                    .Where(p => p.MediaThumbnailSize.Name == sizeName)
                    .Select(p => p.Url.Replace("/static", "https://static." + DomainName))
                    .FirstOrDefault();
            });
        }
        private void PrepareSmallPostImage(List<BlogSmallPostItemViewModel> modelItems, string sizeName)
        {
            modelItems.ForEach((modelItem) =>
            {
                if (modelItem.Media != null && modelItem.Media.Thumbnails != null)
                    modelItem.ImageSrc = modelItem.Media.Thumbnails
                    .Where(p => p.MediaThumbnailSize.Name == sizeName)
                    .Select(p => p.Url.Replace("/static", "https://static." + DomainName))
                    .FirstOrDefault();
            });
        }
        private async Task<List<BlogCategoryItemViewModel>> PrepareCategoriesViewModelAsync()
        {
            return await _postCategoryService.TableNoTracking.ProjectToListAsync<BlogCategoryItemViewModel>();
        }
        #endregion
    }
}