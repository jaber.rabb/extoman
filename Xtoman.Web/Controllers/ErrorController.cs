﻿using System.Web.Mvc;
using Xtoman.Framework;

namespace Xtoman.Web.Controllers
{
    public class ErrorController : BaseController
    {
        public ErrorController()
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NotAuthorized()
        {
            Response.StatusCode = 403;
            return View();
        }

        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View();
        }

        public ActionResult ServerError()
        {
            Response.StatusCode = 500;
            return View();
        }
    }
}