﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Web.Mvc;
using Xtoman.Service;
using Xtoman.Web.ViewModels;

namespace Xtoman.Web.Controllers
{
    public class DiscountsController : Controller
    {
        private readonly IDiscountService _discountService;
        private readonly IUserFundManager _userFundManager;
        public DiscountsController(
            IDiscountService discountService,
            IUserFundManager userFundManager)
        {
            _discountService = discountService;
            _userFundManager = userFundManager;
        }
        public async Task<ActionResult> Index()
        {
            var userDiscountId = 0;
            if (User.Identity.IsAuthenticated)
            {
                var userId = User.Identity.GetUserId<int>();
                userDiscountId = await _userFundManager.GetDiscountIdByUserIdAsync(userId);
            }
            var model = new DiscountsViewModel()
            {
                UserDiscountId = userDiscountId,
                Discounts = await _discountService.TableNoTracking.ProjectToListAsync<DiscountViewModel>()
            };

            return View(model);
        }
    }
}