﻿using AutoMapper;
using Xtoman.Framework.Core;

namespace Xtoman.Web.AutoMapper
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.ConfigureAutoMapper();
            });
        }
    }
}