﻿jQuery.validator.addMethod("enforcetrue", function (value, element, param) {
    return element.checked;
});
jQuery.validator.unobtrusive.adapters.addBool("enforcetrue");


jQuery.validator.addMethod("minvalue", function (value, element, param) {
    return value >= param;
});
jQuery.validator.unobtrusive.adapters.addBool("minvalue");

jQuery.validator.addMethod("maxvalue", function (value, element, param) {
    return value <= param;
});
jQuery.validator.unobtrusive.adapters.addBool("maxvalue");