﻿function initialPage() {
    var locParts = window.location.href.toLowerCase().split('/');
    var lastSegment = locParts.pop() || locParts.pop();  // handle potential trailing slash
    switch (lastSegment) {
        case "account":
            initialAccount();
            break;
    }
}

function initialAccount() {
    $("#BirthDate").persianDatepicker({
        cellWidth: 38, // by px
        cellHeight: 30, // by px
        selectedDate: $('#Display-BirthDate').text().trim()
    });
}

$(document).ready(function () {
    initialAccount();

    $.pjax.defaults.timeout = 5000;

    if ($('#smsTimer').length) {
        var endTimeMiliseconds = parseInt($('#smsTimer').data('end'));
        var endTime = new Date(endTimeMiliseconds);
        // Update the count down every 1 second
        var x = setInterval(function () {

            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = endTime - now;

            // Time calculations for days, hours, minutes and seconds
            //var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            //var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            document.getElementById("smsTimer").innerHTML = "ارسال مجدد (" + minutes + " دقیقه و " + seconds + " ثانیه)";

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                $("#timertd").html('<a class="btn btn-sm btn-secondary" href="#" onclick="ReSendConfirmSMS(); return false;">ارسال مجدد پیامک</a><br style="line-height:35px;" />');
            }
        }, 1000);
    }
    $('#BirthDate').val($('#Display-BirthDate').text().trim());
});

$('#account-row').on('click', '.nav.flex-column.nav-pills .nav-link', function () {
    $('.nav.flex-column.nav-pills .nav-link.active').removeClass('active');
    $(this).addClass('active');
    $('.page-title span').text($(this).text());
});

if ($.support.pjax) {
    $('#account-row').on('click', 'a', function (event) {
        if ($(this).hasClass('no-pjax')) return true;
        $.pjax.click(event, {
            container: '#account-container',
            custom_success: function () {
                $('.nav.flex-column.nav-pills .nav-link.active').removeClass('active');
            }
        });
        event.preventDefault();
    });
}
$(document).on('pjax:send', function () {
    $('#loading').show();
    $('#account-container').hide();
});
$(document).on('pjax:complete', function () {
    $('#account-container').show();
    $('#loading').hide();
    if (window.location.href.toLowerCase().includes("verification")) {
        window.location.reload();
    }
    initialPage();
});
$(document).on('pjax:error', function (xhr, textStatus, error, options) {
    $('#account-container').show();
    $('#loading').hide();
});

$('#account-container').on('pjax:success', function (xhr, textStatus, error, options) {
    if (typeof options.custom_success === 'function') {
        options.custom_success();
    }
});

function editAccount() {
    $('#Display-FirstName').hide();
    $('#Display-LastName').hide();
    $('#Display-Email').hide();
    $('#Display-PhoneNumber').hide();
    $('#Display-Telephone').hide();
    $('#Display-Gender').hide();
    $('#Display-BirthDate').hide();
    $('#Display-NationalCode').hide();
    $('#Display-FatherName').hide();
    $('#editBtn').hide();
    $('#Edit-FirstName').show();
    $('#Edit-LastName').show();
    $('#Edit-Email').show();
    $('#Edit-PhoneNumber').show();
    $('#Edit-Telephone').show();
    $('#Edit-Gender').show();
    $('#Edit-BirthDate').show();
    $('#Edit-NationalCode').show();
    $('#Edit-FatherName').show();
    $('#submitEditBtn').show();
    $('#cancelBtn').show();
}

function cancelEdit() {
    closeEdit();
}

function closeEdit() {
    $('#Display-FirstName').show();
    $('#Display-LastName').show();
    $('#Display-Email').show();
    $('#Display-PhoneNumber').show();
    $('#Display-Telephone').show();
    $('#Display-Gender').show();
    $('#Display-BirthDate').show();
    $('#Display-NationalCode').show();
    $('#Display-FatherName').show();
    $('#editBtn').show();

    $('#Edit-FirstName').hide();
    $('#Edit-LastName').hide();
    $('#Edit-Email').hide();
    $('#Edit-PhoneNumber').hide();
    $('#Edit-Telephone').hide();
    $('#Edit-Gender').hide();
    $('#Edit-BirthDate').hide();
    $('#Edit-NationalCode').hide();
    $('#Edit-FatherName').hide();
    $('#submitEditBtn').hide();
    $('#cancelBtn').hide();
}

$(document).on('submit', '#editForm', function (e) {
    e.preventDefault();
    if ($(this).valid()) {
        var nationalCode = $('#NationalCode').val();
        if (IsNationalCodeValid(nationalCode)) {
            var postData = {
                FirstName: $("#FirstName").val(),
                LastName: $("#LastName").val(),
                Email: $("#Email").val(),
                PhoneNumber: $("#PhoneNumber").val(),
                Gender: $("#Gender").val(),
                Telephone: $("#Telephone").val(),
                NationalCode: nationalCode,
                FatherName: $('#FatherName').val(),
                BirthDate: $('#BirthDate').data("gdate")
            };
            postAntiForgeryAjax(postData, editUrl, function (data) {
                if (data.Status) {
                    $('#Display-FirstName').html(postData.FirstName);
                    $('#Display-LastName').html(postData.LastName);
                    $('#Display-Email').html(postData.Email);
                    $('#Display-PhoneNumber').html(postData.PhoneNumber);
                    $('#Display-Telephone').html(postData.Telephone);
                    $('#Display-NationalCode').html(postData.NationalCode);
                    $('#Display-FatherName').html(postData.FatherName);
                    $('#Display-BirthDate').html($('#BirthDate').val());
                    $('#Display-Gender').html(postData.Gender === "true" ? "مرد" : "زن");
                    closeEdit();
                } else {
                    alert(data.Text);
                }
            });
        } else {
            alert("کد ملی وارد شده اشتباه است.");
        }
    }
});

var checkAgain = false;
var imgFileSizeKb = 0;

$(".fileinput").on("change.bs.fileinput", function () {
    return calculateImgSize();
});
$(".fileinput").on("clear.bs.fileinput", function () {
    return calculateImgSize();
});
$(".fileinput").on("reset.bs.fileinput", function () {
    return calculateImgSize();
});

function calculateImgSize() {
    if (checkAgain) {
        checkAgain = false;
        return false;
    }
    var imgpath = document.getElementById('verifyfile');
    if (imgpath.value !== "") {
        var imgFileSize = imgpath.files[0].size;
        imgFileSizeKb = imgFileSize / 1024;
    }
    else {
        imgFileSizeKb = 0;
    }
    if (imgFileSizeKb === 0) {
        $('#verify-validation-alert').show();
        $('#verify-validation-alert').text('تصویر مدارک انتخاب نشده است.');
        return false;
    }
    if (imgFileSizeKb > 4096) {
        checkAgain = true;
        $('#verify-validation-alert').show();
        $('#verify-validation-alert').text('سایز فایل تصویر بزرگتر از حد مجاز 4 مگابایت است.');
        $('.fileinput').fileinput('reset');
        $('.fileinput').fileinput('clear');
        return false;
    }
    else {
        $('#verify-validation-alert').hide();
        $('#verify-validation-alert').text('');
        return true;
    }
}

function checkFileType() {
    var fileElement = document.getElementById('verifyfile');
    //var fileName = fileElement.files[0].name;
    //var isImage = isImage(fileName);
    //if (!isImage) {
    //    $('#verify-validation-alert').show();
    //    $('#verify-validation-alert').text('فایل انتخاب شده تصویر نیست.');
    //    return false;
    //}
    $('#verify-validation-alert').hide();
    $('#verify-validation-alert').text('');
    return true;
}

$(document).on('submit', '#verificationForm', function (e) {
    e.preventDefault();
    //Loading

    var isSizeValid = calculateImgSize();
    if (!isSizeValid) return false;
    var isFileImage = checkFileType();
    if (!isFileImage) return false;
    $('#sendVerifyBtn').addClass('disabled');
    var formData = new FormData(this);
    var verifyUrl = $(this).attr('action');
    $.ajax({
        url: verifyUrl,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        data: formData,
        success: function (data) {
            $('#verify-validation-alert').text(data.Text);
            if (data.Status) {
                $('#verify-validation-alert').removeClass('alert-warning').removeClass('alert-danger').removeClass('alert-success').addClass('alert-success');
                $('#verifyStatus').text('در انتظار بررسی مدارک توسط اپراتور').css("color", "orange");
                $('#verifyDateText').text('هنوز مدارک تایید نشده است.');
                var imageElement = $('.fileinput-preview').html();
                $('#verifyImage').html(imageElement);
                $('#verifyImage img').addClass('img-fluid');
                $('#verifyDiv').remove();
            } else {
                $('#verify-validation-alert').removeClass('alert-warning').removeClass('alert-danger').removeClass('alert-success').addClass('alert-danger');
            }
            $('#sendVerifyBtn').removeClass('disabled');
            $('#verify-validation-alert').show();
        }
    });
});

function addNotify() {
    $('#notfiesTable').hide();
    $('#addNotifyDiv').show();
}

$('#addNotifyAlertForm #Type').on('change', function () {
    alert('Changed');
});

function verifyResend() {
    postAjax(null, resetVerifyUrl, function () {
        window.location.reload();
    });
}

function onloadCallback() {
    $('input[type="submit"]').removeAttr('disabled');
    grecaptcha.render('recaptchaDiv');
}

function addBankCard() {
    $('#addBankCard').hide();
    $('#addBankCardForm').show();
}

function cancelAddBankCard() {
    $('#addBankCardForm').hide();
    $('#addBankCard').show();
    $('#CardNumber').val('');
}

$(document).on('submit', '#addBankCardForm', function (e) {
    e.preventDefault();
    var cardNumber = $('#CardNumber').val();
    var postData = { CardNumber: cardNumber };
    var url = window.location.href;
    postAntiForgeryAjax(postData, url, function (data) {
        if (data.Status) {
            cancelAddBankCard();
            window.location.reload();
        }
        else {
            alert(data.Text);
        }
    });
});

function removeBankCard(id, cardNumber) {
    confirmDialog("آیا بابت حذف کارت بانکی " + cardNumber + " مطمئن هستید؟", function () {
        var postData = {
            id: id
        };
        postAjax(postData, removeBankCardUrl, function (data) {
            if (data.Status) {
                window.location.reload();
            }
            else {
                alert(data.Text);
            }
        });
    });
}

function ReSendConfirmSMS() {
    $('#resendSmsBtn').addClass('disabled');
    postAjax(null, sendConfirmSMSUrl, function (data) {
        $('#resendSmsBtn').removeClass('disabled');
        if (data.Status) {
            window.location.reload();
        } else {
            alert(data.Text);
        }
    });
}

function ReSendConfirmEmail() {
    $('#resendEmailBtn').addClass('disabled');
    postAjax(null, sendConfirmEmailUrl, function (data) {
        $('#resendEmailBtn').removeClass('disabled');
        if (data.Status) {
            window.location.reload();
        } else {
            alert(data.Text);
        }
    });
}

function ConfirmMobileCode() {
    $('#confirmMobileBtn').addClass('disabled');
    confirmWithInputDialog("کد پیامک شده را وارد نمایید", "کد", "confirmCode", "کد وارد نشده است", function () {
        var code = $('#modal-confirm-with-input').find($('#confirmCode')).val();
        postAjax({ Code: code }, confirmMobileUrl, function (data) {
            $('#confirmMobileBtn').removeClass('disabled');
            if (data.Status) {
                window.location.reload();
            } else {
                alert(data.Text);
            }
        });
    });
}