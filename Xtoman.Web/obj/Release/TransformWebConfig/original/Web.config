﻿<?xml version="1.0" encoding="utf-8"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  https://go.microsoft.com/fwlink/?LinkId=301880
  -->
<configuration>
  <configSections>
    <section name="entityFramework" type="System.Data.Entity.Internal.ConfigFile.EntityFrameworkSection, EntityFramework, Version=6.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false" />

    <!-- For more information on Entity Framework configuration, visit http://go.microsoft.com/fwlink/?LinkID=237468 -->

    <!-- For more information on Entity Framework configuration, visit http://go.microsoft.com/fwlink/?LinkID=237468 -->
    <sectionGroup name="elmah">
      <section name="security" requirePermission="false" type="Elmah.SecuritySectionHandler, Elmah" />
      <section name="errorLog" requirePermission="false" type="Elmah.ErrorLogSectionHandler, Elmah" />
      <section name="errorMail" requirePermission="false" type="Elmah.ErrorMailSectionHandler, Elmah" />
      <section name="errorFilter" requirePermission="false" type="Elmah.ErrorFilterSectionHandler, Elmah" />
    </sectionGroup>
    <!-- For more information on Entity Framework configuration, visit http://go.microsoft.com/fwlink/?LinkID=237468 -->
  </configSections>
  <connectionStrings configSource="ConnectionStrings.config"></connectionStrings>
  <appSettings file="private.config">
    <add key="webpages:Version" value="3.0.0.0" />
    <add key="webpages:Enabled" value="false" />
    <add key="ClientValidationEnabled" value="true" />
    <add key="UnobtrusiveJavaScriptEnabled" value="true" />
    <add key="elmah.mvc.disableHandler" value="false" />
    <add key="elmah.mvc.disableHandleErrorFilter" value="false" />
    <add key="elmah.mvc.requiresAuthentication" value="false" />
    <add key="elmah.mvc.IgnoreDefaultRoute" value="true" />
    <add key="elmah.mvc.allowedRoles" value="*" />
    <add key="elmah.mvc.allowedUsers" value="*" />
    <add key="elmah.mvc.route" value="ErrorLogs" />
    <add key="elmah.mvc.UserAuthCaseSensitive" value="true" />
    <add key="elmah.mvc.IgnoreDefaultRoute" value="false" />
    <add key="elmah.mvc.route" value="elmah" />
    <add key="IsLocal" value="true" />
  </appSettings>
  <!--
    For a description of web.config changes see http://go.microsoft.com/fwlink/?LinkId=235367.

    The following attributes can be set on the <httpRuntime> tag.
      <system.Web>
        <httpRuntime targetFramework="4.7" />
      </system.Web>
  -->
  <system.web>
    <machineKey validationKey="2A6DA1D17B710BB6284429DB8624DD97BB7AE51D14D2F9EC73E405762EEA2E685B856D899B2C1817D7EB261D9EEC8437D0EE944A9FBDF57FBB6566176375F841" decryptionKey="D725C6DD678DC83D63A3A5090973FB2C1F16EB2D9B58CB0A4EC63A6AF5826455" validation="SHA1" decryption="AES" />

    <customErrors mode="On" defaultRedirect="~/Error">
      <error statusCode="404" redirect="~/Error/NotFound" />
      <error statusCode="500" redirect="~/Error/ServerError" />
    </customErrors>
    <authentication mode="None" />
    <compilation debug="true" targetFramework="4.7" />
    <httpRuntime targetFramework="4.5.2" />

    <httpModules>
      <add name="ErrorLog" type="Elmah.ErrorLogModule, Elmah" />
      <add name="ErrorMail" type="Elmah.ErrorMailModule, Elmah" />
      <add name="ErrorFilter" type="Elmah.ErrorFilterModule, Elmah" />
      <add name="TelemetryCorrelationHttpModule" type="Microsoft.AspNet.TelemetryCorrelation.TelemetryCorrelationHttpModule, Microsoft.AspNet.TelemetryCorrelation" />
    </httpModules>
  </system.web>
  <system.webServer>
    <!--<httpProtocol>
      <customHeaders>
        <add name="Strict-Transport-Security" value="max-age=16070400; includeSubDomains" />
      </customHeaders>
    </httpProtocol>-->
    <modules>
      <remove name="FormsAuthentication" />
      <add name="RemoveServerHeaderModule" type="Xtoman.Framework.Mvc.RemoveServerHeaderModule, Xtoman.Framework" />

      <add name="ErrorLog" type="Elmah.ErrorLogModule, Elmah" preCondition="managedHandler" />
      <add name="ErrorMail" type="Elmah.ErrorMailModule, Elmah" preCondition="managedHandler" />
      <add name="ErrorFilter" type="Elmah.ErrorFilterModule, Elmah" preCondition="managedHandler" />
      <remove name="TelemetryCorrelationHttpModule" />
      <add name="TelemetryCorrelationHttpModule" type="Microsoft.AspNet.TelemetryCorrelation.TelemetryCorrelationHttpModule, Microsoft.AspNet.TelemetryCorrelation" preCondition="managedHandler" />
    </modules>
    <staticContent>
      <clientCache cacheControlCustom="public" cacheControlMode="UseMaxAge" cacheControlMaxAge="7.00:00:00" />
    </staticContent>
    <urlCompression doStaticCompression="true" doDynamicCompression="true" />
    <caching enabled="true" enableKernelCache="true">
      <profiles>
        <add location="Any" extension=".ico" policy="CacheForTimePeriod" kernelCachePolicy="CacheForTimePeriod" duration="7.00:00:00" />
        <add location="Any" extension=".eot" policy="CacheForTimePeriod" kernelCachePolicy="CacheForTimePeriod" duration="7.00:00:00" />
        <add location="Any" extension=".icon" policy="CacheForTimePeriod" kernelCachePolicy="CacheForTimePeriod" duration="7.00:00:00" />
        <add location="Any" extension=".otf" policy="CacheForTimePeriod" kernelCachePolicy="CacheForTimePeriod" duration="7.00:00:00" />
        <add location="Any" extension=".ttf" policy="CacheForTimePeriod" kernelCachePolicy="CacheForTimePeriod" duration="7.00:00:00" />
        <add location="Any" extension=".woff" policy="CacheForTimePeriod" kernelCachePolicy="CacheForTimePeriod" duration="7.00:00:00" />
        <add location="Any" extension=".png" policy="CacheForTimePeriod" kernelCachePolicy="CacheForTimePeriod" duration="7.00:00:00" />
        <add location="Any" extension=".jpeg" policy="CacheForTimePeriod" kernelCachePolicy="CacheForTimePeriod" duration="7.00:00:00" />
        <add location="Any" extension=".jpg" policy="CacheForTimePeriod" kernelCachePolicy="CacheForTimePeriod" duration="7.00:00:00" />
        <add location="Any" extension=".gif" policy="CacheForTimePeriod" kernelCachePolicy="CacheForTimePeriod" duration="7.00:00:00" />
        <add location="Any" extension=".js" policy="CacheForTimePeriod" kernelCachePolicy="CacheForTimePeriod" duration="7.00:00:00" />
        <add location="Any" extension=".css" policy="CacheForTimePeriod" kernelCachePolicy="CacheForTimePeriod" duration="7.00:00:00" />
      </profiles>
    </caching>

    <handlers>
      <add name="RobotsText" path="robots.txt" verb="GET" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0" />
      <add name="SitemapXml" path="sitemap.xml" verb="GET" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0" />
    </handlers>
    <validation validateIntegratedModeConfiguration="false" />
  </system.webServer>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin.Security" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin.Security.OAuth" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin.Security.Cookies" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" culture="neutral" publicKeyToken="30ad4fe6b2a6aeed" />
        <bindingRedirect oldVersion="0.0.0.0-11.0.0.0" newVersion="11.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Optimization" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-1.1.0.0" newVersion="1.1.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-1.6.5135.21930" newVersion="1.6.5135.21930" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Antlr3.Runtime" publicKeyToken="eb42632606e9261f" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.5.0.2" newVersion="3.5.0.2" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Diagnostics.DiagnosticSource" publicKeyToken="cc7b13ffcd2ddd51" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.3.1" newVersion="4.0.3.1" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="HtmlAgilityPack" publicKeyToken="bd319b19eaf3b43a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-1.8.5.0" newVersion="1.8.5.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.IdentityModel.Tokens.Jwt" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.4.0" newVersion="5.2.4.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Net.Http" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.1.1.2" newVersion="4.1.1.2" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Runtime" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.1.1.0" newVersion="4.1.1.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Runtime.InteropServices" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.1.1.0" newVersion="4.1.1.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Reflection" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.1.1.0" newVersion="4.1.1.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.IO" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.1.1.0" newVersion="4.1.1.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Text.RegularExpressions" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.1.0.0" newVersion="4.1.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Linq" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.1.1.0" newVersion="4.1.1.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Runtime.Extensions" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.1.1.0" newVersion="4.1.1.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="RazorEngine" publicKeyToken="9ee697374c7e744a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.10.0.0" newVersion="3.10.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="WebMarkupMin.Core" publicKeyToken="99472178d266584b" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-2.4.5.0" newVersion="2.4.5.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.IdentityModel.Tokens" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.4.0" newVersion="5.2.4.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-5.2.6.0" newVersion="5.2.6.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.ValueTuple" publicKeyToken="cc7b13ffcd2ddd51" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.3.0" newVersion="4.0.3.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <entityFramework>
    <defaultConnectionFactory type="System.Data.Entity.Infrastructure.LocalDbConnectionFactory, EntityFramework">
      <parameters>
        <parameter value="mssqllocaldb" />
      </parameters>
    </defaultConnectionFactory>
    <providers>
      <provider invariantName="System.Data.SqlClient" type="System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer" />
    </providers>

    <contexts>
      <context type="Elmah.SqlServer.EFInitializer.ElmahContext, Elmah.SqlServer.EFInitializer">
        <databaseInitializer type="Elmah.SqlServer.EFInitializer.ElmahDatabaseInitializer, Elmah.SqlServer.EFInitializer" />
      </context>
    </contexts>
  </entityFramework>
  <system.serviceModel>
    <bindings>
      <basicHttpBinding>
        <binding name="smsserverBinding" />
        <binding name="smsserverBinding1" />
        <binding name="PaymentGatewayImplServiceSoapBinding">
          <security mode="Transport" />
        </binding>
        <binding name="PaymentGatewayImplServiceSoapBinding1" />
        <binding name="PaymentIFBindingSoap2">
          <security mode="Transport" />
        </binding>
        <binding name="PaymentIFBindingSoap3" />
        <binding name="PaymentIFBindingSoap">
          <security mode="Transport" />
        </binding>
        <binding name="PaymentIFBindingSoap1" />
        <binding name="WebService3Soap">
          <security mode="Transport" />
        </binding>
        <binding name="WebService3Soap1" />
      </basicHttpBinding>
      <customBinding>
        <binding name="WebService3Soap12">
          <textMessageEncoding messageVersion="Soap12" />
          <httpsTransport />
        </binding>
      </customBinding>
    </bindings>
    <client>
      <endpoint address="https://bpm.shaparak.ir/pgwchannel/services/pgw" binding="basicHttpBinding" bindingConfiguration="PaymentGatewayImplServiceSoapBinding" contract="WebServices.Payment.Mellat.IPaymentGateway" name="PaymentGatewayImplPort" />
      <endpoint address="https://sep.shaparak.ir/Payments/InitPayment.asmx" binding="basicHttpBinding" bindingConfiguration="PaymentIFBindingSoap2" contract="WebServices.Payment.SamanToken.PaymentIFBindingSoap" name="PaymentIFBindingSoap1" />
      <endpoint address="https://sep.shaparak.ir/payments/referencepayment.asmx" binding="basicHttpBinding" bindingConfiguration="PaymentIFBindingSoap" contract="WebServices.Payment.Saman.PaymentIFBindingSoap" name="PaymentIFBindingSoap" />
      <endpoint address="http://37.130.202.188/public/class/sms/csharpservice/server.php" binding="basicHttpBinding" bindingConfiguration="smsserverBinding" contract="WebServices.Message.FarazSMS.smsserverPortType" name="smsserverPort" />
      <endpoint address="http://188.0.240.110/public/class/sms/csharpservice_new/server.php" binding="basicHttpBinding" bindingConfiguration="smsserverBinding1" contract="WebServices.Message.FarazSMSNew.smsserverPortType" name="smsserverPort1" />
      <endpoint address="https://portal.avanak.ir/webservice3.asmx" binding="basicHttpBinding" bindingConfiguration="WebService3Soap" contract="WebServices.Message.Avanak.WebService3Soap" name="WebService3Soap" />
      <endpoint address="https://portal.avanak.ir/webservice3.asmx" binding="customBinding" bindingConfiguration="WebService3Soap12" contract="WebServices.Message.Avanak.WebService3Soap" name="WebService3Soap12" />
    </client>
  </system.serviceModel>
  <elmah>
    <security allowRemoteAccess="true" />
    <errorLog type="Elmah.SqlErrorLog, Elmah" connectionStringName="XtomanElmahDbConnection" applicationName="*" />
  </elmah>  

  <location path="elmah.axd" inheritInChildApplications="false">
    <system.web>
      <httpHandlers>
        <add verb="POST,GET,HEAD" path="elmah.axd" type="Elmah.ErrorLogPageFactory, Elmah" />
      </httpHandlers>
      <authorization>
        <allow roles="Admin" />
        <deny users="*" />  
      </authorization>
    </system.web>
    <system.webServer>
      <staticContent>
        <mimeMap fileExtension=".json" mimeType="application/json; charset=UTF-8" />
      </staticContent>
      <handlers>
        <add name="ELMAH" verb="POST,GET,HEAD" path="elmah.axd" type="Elmah.ErrorLogPageFactory, Elmah" preCondition="integratedMode" />
      </handlers>
    </system.webServer>
  </location>
  <system.codedom>
    <compilers>
      <compiler language="c#;cs;csharp" extension=".cs" type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.CSharpCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" warningLevel="4" compilerOptions="/langversion:default /nowarn:1659;1699;1701" />
      <compiler language="vb;vbs;visualbasic;vbscript" extension=".vb" type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.VBCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" warningLevel="4" compilerOptions="/langversion:default /nowarn:41008 /define:_MYTYPE=\&quot;Web\&quot; /optionInfer+" />
    </compilers>
  </system.codedom>
</configuration>