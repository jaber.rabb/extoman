﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices;
using Xtoman.Domain.WebServices.Message.SMS.FarazSMS;
using Xtoman.Service.WebServices.Message.FarazSMSNew;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class FarazsmsService : WebServiceManager, IFarazsmsService
    {
        //private readonly string baseUrl = "http://37.130.202.188/api/select";
        private readonly string username = AppSettingManager.PayamTak_UserName;
        private readonly string password = AppSettingManager.PayamTak_Password;
        private readonly string fromPattern = AppSettingManager.PayamTak_FromPattern;
        //private readonly smsserverPortTypeClient _farazSMSClient;
        private readonly smsserverPortTypeClient _farazSMSClient;
        public FarazsmsService()
        {
            //_farazSMSClient = new smsserverPortTypeClient();
            _farazSMSClient = new smsserverPortTypeClient();
        }

        public async Task<string> TomanCardToCardPaidAsync(string to, string orderGuid, string transactionId)
        {
            var inputs = new Dictionary<string, string>() { { "orderGuid", orderGuid }, { "transactionId", transactionId } };
            return await SendByPatternAsync(to, "l2ebub4flg", inputs);
        }

        public async Task<string> TomanIBANPaidAsync(string to, string orderGuid, string payType, string status)
        {
            var inputs = new Dictionary<string, string>() { { "orderGuid", orderGuid }, { "paytype", payType }, { "status", status } };
            return await SendByPatternAsync(to, "vsxlgemy79", inputs);
        }

        public async Task<string> ForgotPasswordCodeAsync(string to, string userName, string code)
        {
            var inputs = new Dictionary<string, string>() { { "userName", userName }, { "code", code } };
            return await SendByPatternAsync(to, "784g9pcod1", inputs);
        }

        public async Task<decimal> GetCreditByRialAsync()
        {
            var request = new FarazSMSGetCreditRequest();
            var response = await FarazSMSHelper.GetResponseAsync(request);
            var result = response.ContentBody.RemoveLeft(1).RemoveRight(1);
            result = result.Split('"')[1];
            return result.TryToDecimal();
        }

        public async Task<decimal> GetCreditByTomanAsync()
        {
            var result = await GetCreditByRialAsync();
            return result / 10;
        }

        public async Task<string> NewRegisterAsync(string to, string emailOrPhone)
        {
            var inputs = new Dictionary<string, string>() { { "emailOrPhone", emailOrPhone } };
            return await SendByPatternAsync(to, "q8iicmhrnp", inputs);
        }

        public async Task<string> OrderAsync(string to, string userName, string orderGuid, bool isNew = true)
        {
            var inputs = new Dictionary<string, string>() { { "orderGuid", orderGuid }, { "userName", userName } };
            var code = isNew ? "l16ll4uqoq" : "nq5lapcuej";
            return await SendByPatternAsync(to, code, inputs);
        }

        public async Task<string> SendByPatternAsync(string to, string patternCode, Dictionary<string, string> inputs)
        {
            try
            {
                var toNum = new string[] { to };
                //var inputData = inputs.Select(x => x.ToString()).ToArray();
                var inputDataNew = inputs.Select(p => new input_data_type() { key = p.Key, value = p.Value }).ToArray();
                var result = await _farazSMSClient.sendPatternSmsAsync(fromPattern, toNum, username, password, patternCode.ToString(), inputDataNew);
                return result.@return;
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
            return "";
        }

        public bool SendMassSms(List<string> phones, string text)
        {
            var phoneNumber = "[" + string.Join(",", phones.Select(p => "\"" + p + "\"").ToList()) + "]";
            return SendSms(phoneNumber, text);
        }

        public bool SendSms(string phoneNumber, string text)
        {
            if (!phoneNumber.Contains("["))
                phoneNumber = "\"" + phoneNumber + "\"";
            var client = new RestClient("http://37.130.202.188/api/select");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("undefined", "{\"op\" : \"send\"" +
                ",\"uname\" : \"xtoman\"" +
                ",\"pass\":  \"xtoman.com\"" +
                ",\"message\" : \"" + text + "\"" +
                ",\"from\": \"+98simcard\"" +
                ",\"to\" : " + phoneNumber + "}"
                , ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            if (response.Content.Deserialize<List<string>>().First() == "0")
                return true;

            return false;
        }

        public async Task<string> TicketAnsweredAsync(string to, string emailOrPhoneNumber, string ticketSubject)
        {
            var inputs = new Dictionary<string, string>() { { "emailOrPhoneNumber", emailOrPhoneNumber }, { "ticketSubject", ticketSubject } };
            return await SendByPatternAsync(to, "rd78vr0m4r", inputs);
        }

        public async Task<string> TicketSubmitedAsync(string to, string userName)
        {
            var inputs = new Dictionary<string, string>() { { "userName", userName } };
            return await SendByPatternAsync(to, "jjdt9z77hb", inputs);
        }

        public async Task<string> VerifyCardAsync(string to, string cardNumber, bool isVerified)
        {
            var inputs = new Dictionary<string, string>() { { "cardNumber", cardNumber } };
            var code = isVerified ? "i8pwbfooin" : "iswevwfa6a";
            return await SendByPatternAsync(to, code, inputs);
        }

        public async Task<string> VerifyIdentityAsync(string to, string userName, bool? accepted = null, string description = "")
        {
            var inputs = new Dictionary<string, string>() { { "userName", userName } };
            var code = accepted.HasValue ? accepted.Value ? "r4bb4h4mib" : "xorpvr8378" : "kce1l54nz4";
            if (code == "xorpvr8378")
                inputs.Add("description", description);
            if (code.HasValue())
                return await SendByPatternAsync(to, code, inputs);
            return "";
        }

        public async Task<string> VerifyPhoneAsync(string to, string phoneCode, bool? isNew = null)
        {
            var inputs = new Dictionary<string, string>() { { "phoneCode", phoneCode } };
            var code = isNew.HasValue ? isNew.Value ? "2a131vl7by" : "5zlshd72w7" : "2a131vl7by";
            return await SendByPatternAsync(to, code, inputs);
        }

        public async Task<string> VerifyPhoneJustCodeAsync(string to, string code)
        {
            var inputs = new Dictionary<string, string>() { { "code", code } };
            return await SendByPatternAsync(to, "7bfdp8nbzs", inputs);
        }

        public async Task<string> VerifyTelephoneAsync(string to, string telephone, bool isVerified)
        {
            var inputs = new Dictionary<string, string>() { { "telephone", telephone } };
            var code = isVerified ? "j8ha7tnsz1" : "ngfu8x9217";
            return await SendByPatternAsync(to, code, inputs);
        }
    }

    public class FarazSMSHelper
    {
        //private const string ContentType = "application/json";
        private static readonly string url = "http://37.130.202.188/api/select";
        public static HttpUrlResponse GetResponse(FarazSMSRequestBase request)
        {
            var body = request.Serialize(true);

            using (var webClient = new WebClient())
            {
                //httpClient.Timeout = TimeSpan.FromSeconds(300);
                webClient.Headers.Add("Content-Type", "application/json");
                webClient.Encoding = Encoding.UTF8;

                string response = "";
                try
                {
                    response = webClient.UploadString(url, body);
                }
                catch (WebException e)
                {
                    response = new { error = "Exception while contacting FarazSMS API: " + e.Message }.Serialize();
                }
                catch (Exception e)
                {
                    response = new { error = "Unknown exception: " + e.Message }.Serialize();
                }

                var contentBody = response;
                List<KeyValuePair<string, IEnumerable<string>>> headers = new List<KeyValuePair<string, IEnumerable<string>>>();
                if (webClient.ResponseHeaders != null && webClient.ResponseHeaders.HasKeys())
                    foreach (var item in webClient.ResponseHeaders.AllKeys)
                        headers.Add(new KeyValuePair<string, IEnumerable<string>>(item, webClient.ResponseHeaders.GetValues(item)));

                var statusCode = response.HasValue() ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
                var isSuccess = statusCode == HttpStatusCode.OK;

                return new HttpUrlResponse(statusCode, isSuccess, headers, contentBody, url, body);
            }
        }
        public async static Task<HttpUrlResponse> GetResponseAsync(FarazSMSRequestBase request)
        {
            var body = request.Serialize(true);

            //var servicePointManager = ServicePointManager.FindServicePoint(new Uri(url));
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            using (var httpClient = new HttpClient())
            {
                //httpClient.Timeout = TimeSpan.FromSeconds(300);
                HttpResponseMessage response;

                var requestBody = new StringContent(body, Encoding.UTF8, "application/json");
                //var requestBody = new StringContent(body);
                response = await httpClient.PostAsync(url, requestBody).ConfigureAwait(false);

                var contentBody = await response.Content.ReadAsStringAsync();
                var headers = response.Headers.AsEnumerable();
                var statusCode = response.StatusCode;
                var isSuccess = response.IsSuccessStatusCode;

                return new HttpUrlResponse(statusCode, isSuccess, headers, contentBody, url, body);
            }
        }
    }
}
