﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Xtoman.Service.WebServices
{
    public interface IFarazsmsService
    {
        Task<decimal> GetCreditByRialAsync();
        Task<decimal> GetCreditByTomanAsync();
        Task<string> SendByPatternAsync(string to, string patternCode, Dictionary<string, string> inputs);

        ////////// Patterns
        Task<string> NewRegisterAsync(string to, string emailOrPhone);
        Task<string> VerifyPhoneAsync(string to, string phoneCode, bool? isNew = null);
        Task<string> VerifyPhoneJustCodeAsync(string to, string code);
        Task<string> VerifyIdentityAsync(string to, string userName, bool? accepted = null, string description = "");
        Task<string> TicketSubmitedAsync(string to, string userName);
        Task<string> ForgotPasswordCodeAsync(string to, string userName, string code);
        Task<string> OrderAsync(string to, string userName, string orderGuid, bool isNew = true); // false = paid
        Task<string> TicketAnsweredAsync(string to, string emailOrPhoneNumber, string ticketSubject);
        Task<string> VerifyTelephoneAsync(string to, string telephone, bool isVerified);
        Task<string> VerifyCardAsync(string to, string cardNumber, bool isVerified);
        Task<string> TomanCardToCardPaidAsync(string to, string orderGuid, string transactionId);
        Task<string> TomanIBANPaidAsync(string to, string orderGuid, string payType, string status);
        bool SendSms(string phoneNumber, string text);
        bool SendMassSms(List<string> phones, string text);
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
