﻿using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Message.SMS.PayamTak;

namespace Xtoman.Service.WebServices
{
    public interface IPayamTakService
    {
        PayamTakResult SendSMS(string to, string text, bool isFlash = false);
        PayamTakResult SendSMS(string[] to, string text, bool isFlash = false);
        Task<PayamTakResult> SendSMSAsync(string to, string text, bool isFlash = false);
        Task<PayamTakResult> SendSMSAsync(string[] to, string text, bool isFlash = false);
    }
}
