﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Message.SMS.PayamTak;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class PayamTakService : WebServiceManager, IPayamTakService
    {
        private readonly string baseUrl = "http://ippanel.com/services.jspd";
        private readonly string username = AppSettingManager.PayamTak_UserName;
        private readonly string password = AppSettingManager.PayamTak_Password;
        private readonly string from = AppSettingManager.PayamTak_From;

        public PayamTakResult SendSMS(string to, string text, bool isFlash = false)
        {
            return SendSMS(new string[] { to }, text, isFlash);
        }

        public PayamTakResult SendSMS(string[] to, string text, bool isFlash = false)
        {
            WebRequest request = WebRequest.Create(baseUrl);
            string toJson = JsonConvert.SerializeObject(to);
            request.Method = "POST";
            string postData = $"op=send&uname={username}&pass={password}&message={text}&to={toJson}&from={from}";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse response = request.GetResponse();
            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();
            return new PayamTakResult() { RetStatus = 1 }; // 1 = Success
        }

        //public PayamTakResult SendSMS(string[] to, string text, bool isFlash = false)
        //{
        //    var (client, request) = PrepareRestClientRequest(to, text, isFlash);
        //    try
        //    {
        //        var response = (client.Execute(request)).Content;
        //        if (response.HasValue())
        //        {
        //            var result = LogAndDeserialize<PayamTakResult>(response);
        //            if (!result.Success)
        //            {
        //                var error = new Exception(result.Status.ToDisplay());
        //                error.LogError();
        //            }

        //            return result;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //    return new PayamTakResult();
        //}

        public async Task<PayamTakResult> SendSMSAsync(string to, string text, bool isFlash = false)
        {
            return await SendSMSAsync(new string[] { to }, text, isFlash);
        }

        public async Task<PayamTakResult> SendSMSAsync(string[] to, string text, bool isFlash = false)
        {
            var (client, request) = PrepareRestClientRequest(to, text, isFlash);
            try
            {
                var response = (await client.ExecuteTaskAsync(request)).Content;
                if (response.HasValue())
                {
                    var result = LogAndDeserialize<PayamTakResult>(response);
                    if (!result.Success) { 
                        var error = new Exception(result.Status.ToDisplay());
                        error.LogError();
                    }

                    return result;
                }
            }
            catch (Exception)
            {
            }
            return new PayamTakResult();
        }

        private (RestClient restClient, RestRequest restRequest) PrepareRestClientRequest(string[] to, string text, bool isFlash = false)
        {
            var toString = string.Join(",", to);
            var client = new RestClient(new Uri(baseUrl + "SendSMS"));
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            //request.AddHeader("postman-token", "fcddb5f4-dc58-c7d5-4bf9-9748710f8789");
            request.AddHeader("cache-control", "no-cache");
            request.AddParameter("application/x-www-form-urlencoded", $"username={username}&password={password}&to={toString}&from={from}&text={text}&isflash={isFlash.ToString().ToLower()}", ParameterType.RequestBody);
            return (client, request);
        }
    }
}
