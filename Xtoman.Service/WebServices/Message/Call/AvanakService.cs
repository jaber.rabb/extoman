﻿using System;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Message.Call.Avanak;
using Xtoman.Service.WebServices.Message.Avanak;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class AvanakService : IAvanakService
    {
        private readonly string userName = "09136588879";
        private readonly string password = "3ploverap";
        private readonly WebService3SoapClient _client;
        public AvanakService()
        {
            _client = new WebService3SoapClient();
        }
        public Task<AvanakResult> GenerateTextToFemaleSpeechAsync(string title, string text)
        {
            throw new NotImplementedException();
        }

        public Task<AvanakResult> GenerateTextToMaleSpeechAsync(string title, string text)
        {
            throw new NotImplementedException();
        }

        public async Task<AvanakResult> QuickSendWithTextToSpeechAsync(string phoneNumber, string text)
        {
            var succeeded = false;
            var code = 0;
            string messsage;
            try
            {
                code = await _client.QuickSendWithTTSAsync(userName, password, text, phoneNumber, false, 20, "");
                if (code > 0)
                {
                    succeeded = true;
                    messsage = "ارسال موفق بود.";
                }
                else
                {
                    switch (code)
                    {
                        case 0:
                            messsage = "کاربر دمو است";
                            break;
                        case -1:
                            messsage = "اعتبارسنجی";
                            break;
                        case -2:
                            messsage = "اشکال در آپلود";
                            break;
                        case -3:
                            messsage = "کمبود اعتبار";
                            break;
                        case -4:
                            messsage = "عدم اتصال به تبدیل متن به صوت";
                            break;
                        case -5:
                            messsage = "تعداد کاراکتر بیشتر از 1000 است";
                            break;
                        case -6:
                            messsage = "خارج از محدوده زمانی ارسال است";
                            break;
                        case -7:
                            messsage = "آیدی سرور ارسال اشتباه است یا وجود ندارد";
                            break;
                        default:
                            messsage = "خطای نامشخص";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                messsage = ex.Message;
            }
            return new AvanakResult() { Code = code, Message = messsage, Succeeded = succeeded };
        }

        public async Task<AvanakResult> SendOTPAsync(string code, string phoneNumber)
        {
            if (!code.HasValue())
            {
                return new AvanakResult()
                {
                    Code = -20,
                    Message = "کد وارد نشده"
                };
            }
            var codeText = "";
            code = code.Trim();
            var codeArray = code.ToCharArray();
            foreach (var item in codeArray)
                codeText += item + ",";

            var text = $"کُدِ اِحرازِ تِلِفُنِ ثابِتِ شما دَر وِب سایْتِ اِکس تُمَن, {codeText} می باشد,,,, تِکْرار ,{codeText}";

            return await QuickSendWithTextToSpeechAsync(phoneNumber, text);
        }

        #region Helpers

        #endregion
    }
}
