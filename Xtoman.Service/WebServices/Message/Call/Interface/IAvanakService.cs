﻿using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Message.Call.Avanak;

namespace Xtoman.Service.WebServices
{
    public interface IAvanakService
    {
        Task<AvanakResult> GenerateTextToMaleSpeechAsync(string title, string text);
        Task<AvanakResult> GenerateTextToFemaleSpeechAsync(string title, string text);
        Task<AvanakResult> SendOTPAsync(string code, string phoneNumber);
        Task<AvanakResult> QuickSendWithTextToSpeechAsync(string phoneNumber, string text);
    }
}
