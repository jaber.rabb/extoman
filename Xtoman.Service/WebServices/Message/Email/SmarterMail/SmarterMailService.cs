﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Message.Email.SmarterMail;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service.WebServices
{
    public class SmarterMailService : WebServiceManager, ISmarterMailService
    {
        #region Properties
        private readonly ICacheManager _cacheManager;
        private const string authCacheKey = "Xtoman.Nerkh.SmarterMail.Auth";
        private readonly string baseUrl = "https://mail." + CommonUtility.Domain + "/api/v1";
        #endregion

        #region Constructor
        public SmarterMailService(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }
        #endregion

        #region Actions
        public async Task<SmarterMailAuthenticateUserResponse> AuthenticateUser(string email, string password)
        {
            var username = email.RemoveRight(CommonUtility.Domain.Length + 1);
            var passwordCache = password.RemoveSymbols();
            var result = await _cacheManager.GetAsync(authCacheKey + "." + username + "." + passwordCache, 1440, async () =>
            {
                var action = "auth/authenticate-user";
                return await PostAsync<SmarterMailAuthenticateUserResponse>(action, new { username = email, password });
            });
            return result;
        }

        public async Task<SmarterMailRefreshTokenResponse> RefreshToken(string token)
        {
            var action = "auth/refresh-token";
            return await PostAsync<SmarterMailRefreshTokenResponse>(action, new { token });
        }

        public async Task<SmarterMailBaseResponse> SendEmailAsync(SmarterMailEmail smarterMailEmail)
        {
            var action = "mail/message-put";
            return await PostAsync<SmarterMailBaseResponse>(action, smarterMailEmail);
        }

        public async Task<SmarterMailBaseResponse> SendEmailAsync(MailMessage mailMessage)
        {
            return await SendEmailAsync(new SmarterMailEmail()
            {
                To = mailMessage.To.Select(p => p.ToString()).Join(","),
                BCC = mailMessage.Bcc.Select(p => p.ToString()).Join(","),
                CC = mailMessage.CC.Select(p => p.ToString()).Join(","),
                From = mailMessage.From.Address,
                Subject = mailMessage.Subject,
                Priority = SmarterMailMessagePriorities.High,
                MessageHtml = mailMessage.IsBodyHtml ? mailMessage.Body : "",
                MessagePlainText = mailMessage.IsBodyHtml ? "" : mailMessage.Body
            });
        }
        #endregion

        #region Helpers
        private async Task<T> PostAsync<T>(string action, object inputs = null) where T : SmarterMailBaseResponse, new()
        {
            SmarterMailAuthenticateUserResponse auth = null;
            var restClient = new RestClient(baseUrl);
            var restRequest = new RestRequest(action, Method.POST);
            if (inputs != null)
            {
                var validProperties = inputs.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                foreach (var property in validProperties)
                {
                    var value = property.GetValue(inputs)?.ToString() ?? "";
                    if (value.HasValue())
                        restRequest.AddParameter(property.Name, property.GetValue(inputs)?.ToString() ?? "");
                }
            }
            if (!action.Contains("auth/"))
            {
                // Add authentication bearer token
                auth = await AuthenticateUser("no-reply@extoman.co", "1!2@3#Lr");
                restClient.AddDefaultHeader("Authorization", string.Format("Bearer {0}", auth.AccessToken));
            }
            var searchOutputs = await restClient.ExecuteTaskAsync(restRequest).ConfigureAwait(false);
            var result = LogAndDeserialize<T>(searchOutputs.Content);
            if (!result.Success && result.ResultCode == SmarterMailHttpStatusCode.Unauthorized)
            {
                if (auth != null)
                {
                    // Refresh token and send request again.
                    var refresh = await RefreshToken(auth.RefreshToken);
                    if (refresh.Success)
                        return await PostAsync<T>(action, inputs);
                }
            }
            return result;
        }
        #endregion
    }
}
