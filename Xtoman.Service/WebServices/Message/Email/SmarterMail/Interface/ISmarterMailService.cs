﻿using System.Net.Mail;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Message.Email.SmarterMail;

namespace Xtoman.Service.WebServices
{
    public interface ISmarterMailService
    {
        Task<SmarterMailAuthenticateUserResponse> AuthenticateUser(string email, string password);
        Task<SmarterMailRefreshTokenResponse> RefreshToken(string token);
        Task<SmarterMailBaseResponse> SendEmailAsync(MailMessage mailMessage);
        Task<SmarterMailBaseResponse> SendEmailAsync(SmarterMailEmail smarterMailEmail);
    }
}
