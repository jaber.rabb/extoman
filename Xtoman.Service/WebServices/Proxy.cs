﻿using System.Net;

namespace Xtoman.Service.WebServices
{
    public static class Proxy
    {
        private static WebProxy _defaultProxy;
        public static WebProxy DefaultProxy
        {
            get
            {
                return _defaultProxy ?? (_defaultProxy = new WebProxy("http://127.0.0.1:8118", true) { UseDefaultCredentials = true });
            }
        }
    }
}
