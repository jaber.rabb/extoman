﻿using System.Web;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class PaymentService : WebServiceManager, IPaymentService
    {
        public void MakePayment(object parameters, string merchantUrl, string formName, string formMethod, string submitName, HttpContextBase httpContext = null)
        {
            if (httpContext == null)
                httpContext = HttpContext.Current.HttpContextBase();

            var dataPost = new DataPost
            {
                Url = merchantUrl,
                FormName = formName,
                Method = formMethod,
                SubmitName = submitName
            };

            var parametersProperty = parameters.GetType().GetProperties();
            foreach (var item in parametersProperty)
            {
                string value = item.GetValue(parameters)?.ToString() ?? "";
                dataPost.AddKey(item.Name, value);
            }

            dataPost.Post(httpContext);
        }
    }
}
