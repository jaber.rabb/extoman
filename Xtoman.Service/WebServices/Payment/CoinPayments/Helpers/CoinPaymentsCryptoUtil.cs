﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Xtoman.Domain.WebServices.Payment.CoinPayments;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class CoinPaymentsCryptoUtil
    {
        public static string CalcSignature(string input, string key = null)
        {
            // Use input string to calculate MD5 hash
            using (var md5 = getHasher(key))
            {
                byte[] inputBytes = Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().ToLower();
            }
        }

        private static HashAlgorithm getHasher(string key)
        {
            if (key == null)
            {
                return HMACSHA512.Create();
            }
            else
            {
                byte[] keyBytes = Encoding.ASCII.GetBytes(key);
                return new HMACSHA512(keyBytes);
            }
        }

        public static string DictionaryToFormData(Dictionary<string, string> dict)
        {
            var query = string.Join("&", dict.Keys.Select(key => string.Format("{0}={1}",
                key, HttpUtility.UrlEncode(dict[key]).Replace("+", " ").Replace("%2c", ","))));

            return query;
        }

        /// <summary>
        /// Check sign is valid
        /// </summary>
        /// <param name="source">ParsedIPNResult source property</param>
        /// <param name="hmacSent">HMAC coinpayments sent</param>
        /// <param name="merchant">Merchand ID</param>
        /// <returns></returns>
        public static bool SignIsValid(string source, string hmacSent, string merchant)
        {
            var calcHmac = CalcSignature(source, AppSettingManager.CoinPayments_IPNSecret);
            var hmacIsValid = calcHmac == hmacSent;
            new Exception($"CoinPayments IPN -> Sent HMAC = {hmacSent}, Xtoman Calculated HMAC = {calcHmac}, IPNSecret = {AppSettingManager.CoinPayments_IPNSecret}, Request Source = {source}").LogError();
            if (!hmacIsValid)
                new Exception($"CoinPayments IPN -> Sent HMAC = {hmacSent}, Xtoman Calculated HMAC = {calcHmac}, IPNSecret = {AppSettingManager.CoinPayments_IPNSecret}, Request Source = {source}").LogError();

            var merchantIsValid = merchant == AppSettingManager.CoinPayments_MerchantId;
            if (!merchantIsValid)
                new Exception($"CoinPayments IPN -> Sent Merchant ID = {merchant}, Xtoman Merchant ID = {AppSettingManager.CoinPayments_MerchantId}").LogError();

            return hmacIsValid && merchantIsValid;
        }

        public static (T result, string source) ParseIPNResult<T>(HttpRequestBase request)
            where T : IPNGenericResult
        {
            var dict = new Dictionary<string, string>();
            foreach (var key in request.Form.AllKeys)
                dict.Add(key, request.Form[key]);

            var objStr = JsonConvert.SerializeObject(dict);
            var req = JsonConvert.DeserializeObject<T>(objStr);

            var source = string.Empty;
            using (StreamReader reader = new StreamReader(request.InputStream, Encoding.UTF8))
                source = reader.ReadToEnd();

            return (req, source);
        }

        public static T ParseIPNResult<T>(NameValueCollection requestForm) where T : IPNGenericResult
        {
            var dict = new Dictionary<string, string>();
            foreach (var key in requestForm.AllKeys)
                dict.Add(key, requestForm[key]);

            var objStr = JsonConvert.SerializeObject(dict);
            return JsonConvert.DeserializeObject<T>(objStr);
        }
    }
}
