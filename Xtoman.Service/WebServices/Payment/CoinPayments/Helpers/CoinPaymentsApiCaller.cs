﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices;
using Xtoman.Domain.WebServices.Payment.CoinPayments;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class CoinPaymentsApiCaller
    {
        //private const string ContentType = "application/json";
        private static readonly string url = "https://www.coinpayments.net/api.php";
        public static HttpUrlResponse GetResponse(CoinPaymentsGenericRequest request)
        {
            var body = request.GetQueryString();
            new Exception($"CoinPayments Request Body: {body}").LogError();
            var publicKey = AppSettingManager.CoinPayments_PublicKey;
            var privateKey = AppSettingManager.CoinPayments_PrivateKey;

            var signature = CoinPaymentsCryptoUtil.CalcSignature(body, privateKey);

            using (var webClient = new WebClient())
            {
                //httpClient.Timeout = TimeSpan.FromSeconds(300);
                webClient.Headers.Add("HMAC", signature);
                webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                webClient.Encoding = Encoding.UTF8;

                string response = "";
                try
                {
                    response = webClient.UploadString(url, body);
                }
                catch (WebException e)
                {
                    response = new { error = "Exception while contacting CoinPayments.net: " + e.Message }.Serialize();
                }
                catch (Exception e)
                {
                    response = new { error = "Unknown exception: " + e.Message }.Serialize();
                }

                var contentBody = response;
                List<KeyValuePair<string, IEnumerable<string>>> headers = new List<KeyValuePair<string, IEnumerable<string>>>();
                if (webClient.ResponseHeaders != null && webClient.ResponseHeaders.HasKeys())
                    foreach (var item in webClient.ResponseHeaders.AllKeys)
                        headers.Add(new KeyValuePair<string, IEnumerable<string>>(item, webClient.ResponseHeaders.GetValues(item)));

                var statusCode = response.HasValue() ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
                var isSuccess = statusCode == HttpStatusCode.OK;

                return new HttpUrlResponse(statusCode, isSuccess, headers, contentBody, url, body);
            }
        }
        public async static Task<HttpUrlResponse> GetResponseAsync(CoinPaymentsGenericRequest request)
        {
            var body = request.GetQueryString();
            new Exception($"CoinPayments Request Body: {body}").LogError();
            var publicKey = AppSettingManager.CoinPayments_PublicKey;
            var privateKey = AppSettingManager.CoinPayments_PrivateKey;

            var signature = CoinPaymentsCryptoUtil.CalcSignature(body, privateKey);

            //var servicePointManager = ServicePointManager.FindServicePoint(new Uri(url));
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            using (var httpClient = new HttpClient())
            {
                //httpClient.Timeout = TimeSpan.FromSeconds(300);
                HttpResponseMessage response;

                httpClient.DefaultRequestHeaders.Add("HMAC", signature);

                var requestBody = new StringContent(body, Encoding.UTF8, "application/x-www-form-urlencoded");
                //var requestBody = new StringContent(body);
                response = await httpClient.PostAsync(url, requestBody).ConfigureAwait(false);

                var contentBody = await response.Content.ReadAsStringAsync();
                var headers = response.Headers.AsEnumerable();
                var statusCode = response.StatusCode;
                var isSuccess = response.IsSuccessStatusCode;

                if (!isSuccess || !contentBody.Contains("ok"))
                    new Exception("CoinPayments response headers: " + response.Content.Headers.Serialize() + ". Response body: " + contentBody).LogError();

                return new HttpUrlResponse(statusCode, isSuccess, headers, contentBody, url, body);
            }
        }
    }
}
