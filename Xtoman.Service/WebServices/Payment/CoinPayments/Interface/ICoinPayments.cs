﻿using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Payment.CoinPayments;

namespace Xtoman.Service.WebServices
{
    public interface ICoinPaymentsService
    {
        #region Sync methdos
        /// <summary>
        /// Get Basic Account Information
        /// </summary>
        /// <returns></returns>
        CoinPaymentsBasicInfoResult BasicInfo();
        /// <summary>
        /// Get Exchange Rates / Coin List
        /// </summary>
        /// <returns></returns>
        CoinPaymentsCoinsRateResult ExchangeRatesCoinList();
        /// <summary>
        /// Coin Balances
        /// </summary>
        /// <returns></returns>
        CoinPaymentsCoinBalancesResult CoinBalances();
        /// <summary>
        /// Get Deposit Address
        /// Fees: Addresses returned by this Method are for personal use deposits and reuse the same personal address(es) in your wallet. There is no fee for these deposits but they don't send IPNs. For commercial-use addresses see CallbackAddress Method.
        /// </summary>
        /// <param name="currency">Type of the currency the buyer will be sending.</param>
        /// <returns></returns>
        CoinPaymentsAddressResult DepositAddress(CoinPaymentsCurrency currency);
        /// <summary>
        /// Create Transaction
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        CoinPaymentsCreateTransactionResult CreateTransaction(CreateTransactionInput input);
        /// <summary>
        /// Get Callback Address
        /// Fees: Since callback addresses are designed for commercial use they incur the same 0.5% fee on deposits as transactions created with CreateTransaction Method. For personal use deposits that reuse the same personal address(es) in your wallet that have no fee but don't send IPNs see DepositAddress Method.
        /// </summary>
        /// <param name="currency">Type of the currency the buyer will be sending.</param>
        /// <param name="ipn_url">URL for your IPN callbacks. If not set it will use the IPN URL in your Edit Settings page if you have one set.</param>
        /// <returns></returns>
        CoinPaymentsAddressResult CallbackAddress(CoinPaymentsCurrency currency, string ipn_url = "");

        /// <summary>
        /// Create Transfer
        /// Notes: Transfers are performed as internal coin transfers/accounting entries when possible.For coins not supporting that ability a withdrawal is created instead.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        CoinPaymentsCreateTransferResult CreateTransfer(CreateTransferInput input);
        /// <summary>
        /// Create Withdrawal
        /// Notes: Withdrawals send coins to a specified address or $PayByName tag over the coin networks and optionally send an IPN when complete. If you are sending to another CoinPayments user and you have their $PayByName tag or merchant ID you can also use CreateTransfer Method for faster sends that don't go over the coin networks when possible.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        CoinPaymentsCreateWithdrawalResult CreateWithdrawal(CreateWithdrawalInput input, bool addTransactionFee = false);
        /// <summary>
        /// Convert Coins
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        CoinPaymentsConvertCoinsResult ConvertCoins(ConvertCoinsInput input);
        /// <summary>
        /// Get Withdrawal History
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        CoinPaymentsWithdrawalHistoryResult WithdrawalHistory(WithdrawalHistoryInput input = null);
        /// <summary>
        /// Get Withdrawal Information
        /// </summary>
        /// <param name="id">The withdrawal ID to query.</param>
        /// <returns></returns>
        CoinPaymentsWithdrawalInformationResult WithdrawalInformation(string id);
        /// <summary>
        /// Get Conversion Information
        /// </summary>
        /// <param name="id">The conversion ID to query.</param>
        /// <returns></returns>
        CoinPaymentsConversionInformationResult ConversionInformation(string id);
        /// <summary>
        /// Get $PayByName Profile Information
        /// </summary>
        /// <param name="pbntag">Tag to get information for, such as $CoinPayments or $Alex. Can be with or without a $ at the beginning.</param>
        /// <returns></returns>
        CoinPaymentsPayByNameProfileInfoResult PayByNameProfileInfo(string pbntag);
        /// <summary>
        /// Get $PayByName Tag List
        /// </summary>
        /// <returns></returns>
        CoinPaymentsPayByNameTagListResult PayByNameTagList();
        /// <summary>
        /// Update $PayByName Profile
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        CoinPaymentsPayByNameUpdateProfileResult PayByNameUpdateProfile(PayByNameUpdateProfileInput input);
        /// <summary>
        /// Claim $PayByName Tag
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        CoinPaymentsPayByNameTagClaimResult PayByNameTagClaim(PayByNameTagClaimInput input);
        #endregion

        #region Async methods
        /// <summary>
        /// Get Basic Account Information
        /// </summary>
        /// <returns></returns>
        Task<CoinPaymentsBasicInfoResult> BasicInfoAsync();
        /// <summary>
        /// Get Exchange Rates / Coin List
        /// </summary>
        /// <returns></returns>
        Task<CoinPaymentsCoinsRateResult> ExchangeRatesCoinListAsync();
        /// <summary>
        /// Coin Balances
        /// </summary>
        /// <returns></returns>
        Task<CoinPaymentsCoinBalancesResult> CoinBalancesAsync();
        /// <summary>
        /// Get Deposit Address
        /// Fees: Addresses returned by this Method are for personal use deposits and reuse the same personal address(es) in your wallet. There is no fee for these deposits but they don't send IPNs. For commercial-use addresses see CallbackAddress Method.
        /// </summary>
        /// <param name="currency">Type of the currency the buyer will be sending.</param>
        /// <returns></returns>
        Task<CoinPaymentsAddressResult> DepositAddressAsync(CoinPaymentsCurrency currency);
        /// <summary>
        /// Create transaction
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="currency1"></param>
        /// <param name="currency2"></param>
        /// <param name="custom"></param>
        /// <param name="ipn_url"></param>
        /// <returns></returns>
        Task<CoinPaymentsCreateTransactionResult> CreateTransactionAsync(decimal amount, string currency1, string currency2, string custom, string ipn_url);
        /// <summary>
        /// Create Transaction
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<CoinPaymentsCreateTransactionResult> CreateTransactionAsync(CreateTransactionInput input);
        /// <summary>
        /// Get Callback Address
        /// Fees: Since callback addresses are designed for commercial use they incur the same 0.5% fee on deposits as transactions created with CreateTransaction Method. For personal use deposits that reuse the same personal address(es) in your wallet that have no fee but don't send IPNs see DepositAddress Method.
        /// </summary>
        /// <param name="currency">Type of the currency the buyer will be sending.</param>
        /// <param name="ipn_url">URL for your IPN callbacks. If not set it will use the IPN URL in your Edit Settings page if you have one set.</param>
        /// <returns></returns>
        Task<CoinPaymentsAddressResult> CallbackAddressAsync(CoinPaymentsCurrency currency, string ipn_url);
        Task<CoinPaymentsAddressResult> CallbackAddressAsync(string currency, string ipn_url);
        /// <summary>
        /// Create Transfer
        /// Notes: Transfers are performed as internal coin transfers/accounting entries when possible.For coins not supporting that ability a withdrawal is created instead.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<CoinPaymentsCreateTransferResult> CreateTransferAsync(CreateTransferInput input);
        /// <summary>
        /// Create Withdrawal
        /// Notes: Withdrawals send coins to a specified address or $PayByName tag over the coin networks and optionally send an IPN when complete. If you are sending to another CoinPayments user and you have their $PayByName tag or merchant ID you can also use CreateTransfer Method for faster sends that don't go over the coin networks when possible.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="addTransactionFee">If set to true, add the coin TX fee to the withdrawal amount so the sender pays the TX fee instead of the receiver.</param>
        /// <returns></returns>
        Task<CoinPaymentsCreateWithdrawalResult> CreateWithdrawalAsync(CreateWithdrawalInput input, bool addTransactionFee = false);
        /// <summary>
        /// Convert Coins
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<CoinPaymentsConvertCoinsResult> ConvertCoinsAsync(ConvertCoinsInput input);
        /// <summary>
        /// Get Withdrawal History
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<CoinPaymentsWithdrawalHistoryResult> WithdrawalHistoryAsync(WithdrawalHistoryInput input = null);
        /// <summary>
        /// Get Withdrawal Information
        /// </summary>
        /// <param name="id">The withdrawal ID to query.</param>
        /// <returns></returns>
        Task<CoinPaymentsWithdrawalInformationResult> WithdrawalInformationAsync(string id);
        /// <summary>
        /// Get Conversion Information
        /// </summary>
        /// <param name="id">The conversion ID to query.</param>
        /// <returns></returns>
        Task<CoinPaymentsConversionInformationResult> ConversionInformationAsync(string id);
        /// <summary>
        /// Get $PayByName Profile Information
        /// </summary>
        /// <param name="pbntag">Tag to get information for, such as $CoinPayments or $Alex. Can be with or without a $ at the beginning.</param>
        /// <returns></returns>
        Task<CoinPaymentsPayByNameProfileInfoResult> PayByNameProfileInfoAsync(string pbntag);
        /// <summary>
        /// Get $PayByName Tag List
        /// </summary>
        /// <returns></returns>
        Task<CoinPaymentsPayByNameTagListResult> PayByNameTagListAsync();
        /// <summary>
        /// Update $PayByName Profile
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<CoinPaymentsPayByNameUpdateProfileResult> PayByNameUpdateProfileAsync(PayByNameUpdateProfileInput input);
        /// <summary>
        /// Claim $PayByName Tag
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<CoinPaymentsPayByNameTagClaimResult> PayByNameTagClaimAsync(PayByNameTagClaimInput input);

        /// <summary>
        /// Get Transaction Information
        /// </summary>
        /// <param name="id">Transaction Id (TxnId)</param>
        /// <returns></returns>
        Task<CoinPaymentsTransactionInformationResult> GetTransactionInformationAsync(string id, bool fullResult = false);

        #endregion

        #region Custom sync methods
        decimal ExchangeRate(ECurrencyType typeFrom, ECurrencyType typeTo);
        decimal ExchangeRateToBTC(ECurrencyType typeFrom);
        decimal GetBalance(ECurrencyType eCurrencyAccountType);
        #endregion

        #region Custom async methods
        Task<decimal> ExchangeRateToBTCAsync(ECurrencyType typeFrom);
        Task<decimal> ExchangeRateAsync(ECurrencyType typeFrom, ECurrencyType typeTo);
        Task<decimal> GetBalanceAsync(ECurrencyType eCurrencyAccountType);
        #endregion
    }
}
