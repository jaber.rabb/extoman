﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Payment.CoinPayments;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service.WebServices
{
    public class CoinPaymentsService : PaymentService, ICoinPaymentsService
    {
        private readonly ICacheManager _cacheManager;
        public CoinPaymentsService(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }
        #region Sync methods
        public CoinPaymentsBasicInfoResult BasicInfo()
        {
            var request = new CoinPaymentsBasicInfoRequest();
            return CallApiLogAndDeserialize<CoinPaymentsBasicInfoResult>(request);
        }
        public CoinPaymentsAddressResult CallbackAddress(CoinPaymentsCurrency currency, string ipn_url = "")
        {
            var request = new CoinPaymentCallbackAddressRequest(currency, ipn_url);
            return CallApiLogAndDeserialize<CoinPaymentsAddressResult>(request);
        }
        public CoinPaymentsCoinBalancesResult CoinBalances()
        {
            var request = new CoinBalancesRequest();
            return CallApiLogAndDeserialize<CoinPaymentsCoinBalancesResult>(request);
        }
        public CoinPaymentsConversionInformationResult ConversionInformation(string id)
        {
            var request = new ConversionInformationRequest(id);
            return CallApiLogAndDeserialize<CoinPaymentsConversionInformationResult>(request);
        }
        public CoinPaymentsConvertCoinsResult ConvertCoins(ConvertCoinsInput input)
        {
            var request = new ConvertCoinsRequest(input);
            return CallApiLogAndDeserialize<CoinPaymentsConvertCoinsResult>(request);
        }
        public CoinPaymentsCreateTransactionResult CreateTransaction(CreateTransactionInput input)
        {
            var request = new CreateTransactionRequest(input);
            return CallApiLogAndDeserialize<CoinPaymentsCreateTransactionResult>(request);
        }
        public CoinPaymentsCreateTransferResult CreateTransfer(CreateTransferInput input)
        {
            var request = new CreateTransferRequest(input);
            return CallApiLogAndDeserialize<CoinPaymentsCreateTransferResult>(request);
        }
        public CoinPaymentsCreateWithdrawalResult CreateWithdrawal(CreateWithdrawalInput input, bool addTransactionFee = false)
        {
            var request = new CreateWithdrawalRequest(input, addTransactionFee);
            var response = CoinPaymentsApiCaller.GetResponse(request);
            //dynamic obj = response.RequestBody.Deserialize<dynamic>();

            var result = CallApiLogAndDeserialize<CoinPaymentsCreateWithdrawalResult>(request);
            if (result?.error == "ok")
                return result;

            var secondResult = CallApiLogAndDeserialize<CoinPaymentsCreateMassWithdrawalResult>(request);
            if (secondResult?.error == "ok")
            {
                var single = secondResult.result.FirstOrDefault();
                if (single != null)
                {
                    return new CoinPaymentsCreateWithdrawalResult()
                    {
                        error = "",
                        result = new CreateWithdrawal()
                        {
                            amount = single.amount,
                            id = single.id,
                            status = single.status
                        }
                    };
                }
            }
            return new CoinPaymentsCreateWithdrawalResult()
            {
                error = secondResult?.error ?? "Error in deserializing"
            };
        }
        public CoinPaymentsAddressResult DepositAddress(CoinPaymentsCurrency currency)
        {
            var request = new DepositAddressRequest(currency);
            return CallApiLogAndDeserialize<CoinPaymentsAddressResult>(request);
        }
        public CoinPaymentsCoinsRateResult ExchangeRatesCoinList()
        {
            var request = new CoinsRateRequest();
            return CallApiLogAndDeserialize<CoinPaymentsCoinsRateResult>(request);
        }
        public CoinPaymentsPayByNameProfileInfoResult PayByNameProfileInfo(string pbntag)
        {
            var request = new PayByNameProfileInfoRequest(pbntag);
            return CallApiLogAndDeserialize<CoinPaymentsPayByNameProfileInfoResult>(request);
        }
        public CoinPaymentsPayByNameTagClaimResult PayByNameTagClaim(PayByNameTagClaimInput input)
        {
            var request = new PayByNameTagClaimRequest(input);
            return CallApiLogAndDeserialize<CoinPaymentsPayByNameTagClaimResult>(request);
        }
        public CoinPaymentsPayByNameTagListResult PayByNameTagList()
        {
            var request = new PayByNameTagListRequest();
            return CallApiLogAndDeserialize<CoinPaymentsPayByNameTagListResult>(request);
        }
        public CoinPaymentsPayByNameUpdateProfileResult PayByNameUpdateProfile(PayByNameUpdateProfileInput input)
        {
            var request = new PayByNameUpdateProfileRequest(input);
            return CallApiLogAndDeserialize<CoinPaymentsPayByNameUpdateProfileResult>(request);
        }
        public CoinPaymentsWithdrawalHistoryResult WithdrawalHistory(WithdrawalHistoryInput input = null)
        {
            var request = new WithdrawalHistoryRequest(input);
            return CallApiLogAndDeserialize<CoinPaymentsWithdrawalHistoryResult>(request);
        }
        public CoinPaymentsWithdrawalInformationResult WithdrawalInformation(string id)
        {
            var request = new WithdrawalInformationRequest(id);
            return CallApiLogAndDeserialize<CoinPaymentsWithdrawalInformationResult>(request);
        }
        public decimal ExchangeRate(ECurrencyType typeFrom, ECurrencyType typeTo)
        {
            var exchangeRates = ExchangeRatesCoinList()?.result;
            if (exchangeRates == null) return 0;

            var fromBTCRate = TypeToBTCRate(typeFrom, exchangeRates);
            var toBTCRate = TypeToBTCRate(typeTo, exchangeRates);

            return (fromBTCRate / toBTCRate);
        }
        public decimal ExchangeRateToBTC(ECurrencyType typeFrom)
        {
            var exchangeRates = ExchangeRatesCoinList()?.result;
            if (exchangeRates == null) return 0;

            return TypeToBTCRate(typeFrom, exchangeRates);
        }
        #endregion

        #region Async methods
        public async Task<CoinPaymentsBasicInfoResult> BasicInfoAsync()
        {
            var request = new CoinPaymentsBasicInfoRequest();
            return await CallApiLogAndDeserializeAsync<CoinPaymentsBasicInfoResult>(request);
        }
        public async Task<CoinPaymentsAddressResult> CallbackAddressAsync(CoinPaymentsCurrency currency, string ipn_url)
        {
            var request = new CoinPaymentCallbackAddressRequest(currency, ipn_url);
            return await CallApiLogAndDeserializeAsync<CoinPaymentsAddressResult>(request);
        }
        public async Task<CoinPaymentsAddressResult> CallbackAddressAsync(string currency, string ipn_url)
        {
            var request = new CoinPaymentCallbackAddressRequest(currency, ipn_url);
            return await CallApiLogAndDeserializeAsync<CoinPaymentsAddressResult>(request);
        }
        public async Task<CoinPaymentsCoinBalancesResult> CoinBalancesAsync()
        {
            var request = new CoinBalancesRequest();
            return await CallApiLogAndDeserializeAsync<CoinPaymentsCoinBalancesResult>(request);
        }
        public async Task<CoinPaymentsConversionInformationResult> ConversionInformationAsync(string id)
        {
            var request = new ConversionInformationRequest(id);
            return await CallApiLogAndDeserializeAsync<CoinPaymentsConversionInformationResult>(request);
        }
        public async Task<CoinPaymentsConvertCoinsResult> ConvertCoinsAsync(ConvertCoinsInput input)
        {
            var request = new ConvertCoinsRequest(input);
            return await CallApiLogAndDeserializeAsync<CoinPaymentsConvertCoinsResult>(request);
        }

        public async Task<CoinPaymentsCreateTransactionResult> CreateTransactionAsync(decimal amount, string currency1, string currency2, string custom, string ipn_url)
        {
            //new Exception($"CoinPayments Create Transaction Input: Amount = {amount}, Currency1 = {currency1}, Currency2 = {currency2}, Custom (OrderIdGuid) = {custom}, Ipn_url = (CallBackUrl) = {ipn_url}").LogError();
            var request = new CreateTransactionRequest(amount, "xtomancoin@gmail.com", currency1, currency2, custom, ipn_url);
            return await CallApiLogAndDeserializeAsync<CoinPaymentsCreateTransactionResult>(request);
        }

        public async Task<CoinPaymentsCreateTransactionResult> CreateTransactionAsync(CreateTransactionInput input)
        {
            //new Exception($"CoinPayments Create Transaction Input: {input.Serialize()}").LogError();
            var request = new CreateTransactionRequest(input);
            return await CallApiLogAndDeserializeAsync<CoinPaymentsCreateTransactionResult>(request);
        }
        public async Task<CoinPaymentsCreateTransferResult> CreateTransferAsync(CreateTransferInput input)
        {
            var request = new CreateTransferRequest(input);
            return await CallApiLogAndDeserializeAsync<CoinPaymentsCreateTransferResult>(request);
        }
        public async Task<CoinPaymentsCreateWithdrawalResult> CreateWithdrawalAsync(CreateWithdrawalInput input, bool addTransactionFee = false)
        {
            var request = new CreateWithdrawalRequest(input, addTransactionFee);

            var result = await CallApiLogAndDeserializeAsync<CoinPaymentsCreateWithdrawalResult>(request);
            if (result != null && (result?.error == null || result?.error == "ok"))
                return result;

            if (result?.error != null)
            {
                return new CoinPaymentsCreateWithdrawalResult()
                {
                    error = $"Currency: {input.currency}, Amount: {input.amount}, Address: {input.address}, Tag: {input.dest_tag} FirstResult: {result?.error}" ?? "Error in deserializing"
                };
            }
            return null;
        }
        public async Task<CoinPaymentsAddressResult> DepositAddressAsync(CoinPaymentsCurrency currency)
        {
            var request = new DepositAddressRequest(currency);
            return await CallApiLogAndDeserializeAsync<CoinPaymentsAddressResult>(request);
        }
        public async Task<CoinPaymentsCoinsRateResult> ExchangeRatesCoinListAsync()
        {
            var result = await _cacheManager.GetAsync("Xtoman.CoinPayments.CoinList", 1440, async () => {
                var request = new CoinsRateRequest();
                return (await CallApiLogAndDeserializeAsync<CoinPaymentsCoinsRateResult>(request)).Serialize();
            });
            return result.Deserialize<CoinPaymentsCoinsRateResult>();
        }
        public async Task<CoinPaymentsPayByNameProfileInfoResult> PayByNameProfileInfoAsync(string pbntag)
        {
            var request = new PayByNameProfileInfoRequest(pbntag);
            return await CallApiLogAndDeserializeAsync<CoinPaymentsPayByNameProfileInfoResult>(request);
        }
        public async Task<CoinPaymentsPayByNameTagClaimResult> PayByNameTagClaimAsync(PayByNameTagClaimInput input)
        {
            var request = new PayByNameTagClaimRequest(input);
            return await CallApiLogAndDeserializeAsync<CoinPaymentsPayByNameTagClaimResult>(request);
        }
        public async Task<CoinPaymentsPayByNameTagListResult> PayByNameTagListAsync()
        {
            var request = new PayByNameTagListRequest();
            return await CallApiLogAndDeserializeAsync<CoinPaymentsPayByNameTagListResult>(request);
        }
        public async Task<CoinPaymentsPayByNameUpdateProfileResult> PayByNameUpdateProfileAsync(PayByNameUpdateProfileInput input)
        {
            var request = new PayByNameUpdateProfileRequest(input);
            return await CallApiLogAndDeserializeAsync<CoinPaymentsPayByNameUpdateProfileResult>(request);
        }
        public async Task<CoinPaymentsWithdrawalHistoryResult> WithdrawalHistoryAsync(WithdrawalHistoryInput input = null)
        {
            var request = new WithdrawalHistoryRequest(input);
            return await CallApiLogAndDeserializeAsync<CoinPaymentsWithdrawalHistoryResult>(request);
        }
        public async Task<CoinPaymentsWithdrawalInformationResult> WithdrawalInformationAsync(string id)
        {
            var request = new WithdrawalInformationRequest(id);
            return await CallApiLogAndDeserializeAsync<CoinPaymentsWithdrawalInformationResult>(request);
        }

        public async Task<CoinPaymentsTransactionInformationResult> GetTransactionInformationAsync(string id, bool fullResult = false)
        {
            var request = new GetTransactionInformationRequest(id, fullResult);
            return await CallApiLogAndDeserializeAsync<CoinPaymentsTransactionInformationResult>(request);
        }

        public async Task<decimal> ExchangeRateAsync(ECurrencyType typeFrom, ECurrencyType typeTo)
        {
            var exchangeRates = (await ExchangeRatesCoinListAsync())?.result;
            if (exchangeRates == null) return 0;

            var fromBTCRate = TypeToBTCRate(typeFrom, exchangeRates);
            var toBTCRate = TypeToBTCRate(typeTo, exchangeRates);

            return (fromBTCRate / toBTCRate);
        }
        public async Task<decimal> ExchangeRateToBTCAsync(ECurrencyType typeFrom)
        {
            var exchangeRates = (await ExchangeRatesCoinListAsync())?.result;
            if (exchangeRates == null) return 0;

            return TypeToBTCRate(typeFrom, exchangeRates);
        }
        #endregion

        #region Helpers
        private T1 CallApiLogAndDeserialize<T1>(CoinPaymentsGenericRequest request) where T1 : CoinPaymentsResult, new()
        {
            var response = CoinPaymentsApiCaller.GetResponse(request);
            return LogAndDeserialize<T1>(response.ContentBody, true);
        }

        private async Task<T1> CallApiLogAndDeserializeAsync<T1>(CoinPaymentsGenericRequest request) where T1 : CoinPaymentsResult, new()
        {
            var response = await CoinPaymentsApiCaller.GetResponseAsync(request);
            return LogAndDeserialize<T1>(response.ContentBody, true);
        }

        public decimal GetBalance(ECurrencyType eCurrencyAccountType)
        {
            var balances = CoinBalances();
            var type = eCurrencyAccountType.ToDisplay(DisplayProperty.ShortName);
            return balances?.result?.Where(p => p.Key.ToUpper() == type).Select(p => p.Value.balancef).FirstOrDefault() ?? 0;
        }
        public async Task<decimal> GetBalanceAsync(ECurrencyType eCurrencyAccountType)
        {
            var balances = await CoinBalancesAsync();
            var type = eCurrencyAccountType.ToCoinPaymentsCurrency();
            return balances?.result?.Where(p => p.Key.ToUpper() == type).Select(p => p.Value.balancef).FirstOrDefault() ?? 0;
        }

        private decimal TypeToBTCRate(ECurrencyType typeFrom, Dictionary<string, CoinsRateItem> coinsRate)
        {
            return coinsRate?.Where(p => p.Key.ToUpper() == typeFrom.ToDisplay(DisplayProperty.ShortName))
                .Select(p => p.Value.rate_btc)
                .FirstOrDefault() ?? 0;
        }
        #endregion
    }
}
