﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using Xtoman.Domain.Settings;
using Xtoman.Domain.WebServices.Payment.Saman;
using Xtoman.Service.WebServices.Payment.Saman;
using Xtoman.Utility;
using PaymentIFBindingSoapClientToken = Xtoman.Service.WebServices.Payment.SamanToken.PaymentIFBindingSoapClient;

namespace Xtoman.Service.WebServices
{
    public class SamanService : PaymentService, ISamanService
    {
        #region Fields
        private readonly string password;
        private readonly string merchantId;
        private readonly PaymentSettings _paymentSettings;
        private readonly PaymentIFBindingSoapClient _api;
        private readonly PaymentIFBindingSoapClientToken _apiToken;
        private readonly string merchantUrl = "https://sep.shaparak.ir/Payment.aspx";

        public Dictionary<string, string> ResponseStates => new Dictionary<string, string>
        {
            ["Canceled By User"] = "مقدار تراکنش توسط خریدار کنسل شده است.",
            ["Invalid Amount"] = "مبلغ سند برگشتی، از مبلغ تراکنش اصلی بیشتر است.",
            ["Invalid Transaction"] = "درخواست برگشت یک تراکنش رست یده است، در حالی که تراکنش اصلی پیدا نمی شود.",
            ["Invalid Card Number"] = "شماره کارت اشتباه است.",
            ["No Such Issuer"] = "چنین صادر کننده کارتی وجود ندارد.",
            ["Expired Card Pick Up"] = "از تاریخ انقضای کارت گذشته است و کارت دیگر معتبر نیست.",
            ["Allowable PIN Tries Exceeded Pick Up"] = "رمز کارت) PIN 3 مرتبه اشباه وارد ) شده است در نتیجه کارت غیر فعال خواهد شد.",
            ["Incorrect PIN"] = "خریدار رمز کارت ) PIN(را اشتباه وارد کرده است.",
            ["Exceeds Withdrawal Amount Limit"] = "مبلغ بیش از سقف برداشت می باشد.",
            ["Transaction Cannot Be Completed"] = "تراکنش Authorize شده است) شماره PIN و PAN درست هستند(ولی امکان سند خوردن وجود ندارد.",
            ["Response Received Too Late"] = "تراکنش در شبکه بانکی Timeout خورده است.",
            ["Suspected Fraud Pick Up"] = "خریدار یا فیلد CVV2 و یا فیلد ExpDate را اشتباه زده است. ) یا اصلا وارد نکرده است(",
            ["No Sufficient Funds"] = "موجودی به اندازی کافی در حساب وجود ندارد.",
            ["Issuer Down Slm"] = "سیستم کارت بانک صادر کننده در وضعیت عملیاتی نیست.",
            ["TME Error"] = "کلیه خطاهای دیگر بانکی باعث ایجاد چنین خطایی میگردد"
        };
        #endregion

        #region Constractor
        public SamanService(PaymentSettings paymentSettings)
        {
            if (AppSettingManager.IsLocale)
                WebRequest.DefaultWebProxy = Proxy.DefaultProxy;

            _paymentSettings = paymentSettings;
            _api = new PaymentIFBindingSoapClient();
            _apiToken = new PaymentIFBindingSoapClientToken();

            password = AppSettingManager.Sep_Password;
            merchantId = AppSettingManager.Sep_MerchandId;
        }
        #endregion

        #region Sync Methods
        [Obsolete("this Method is obsolete")]
        public string RequestToken(long amount, string additionalData, long orderId)
        {
            var response = _apiToken.RequestToken(merchantId, orderId.ToString(), amount * 10, 0, 0, 0, 0, 0, 0, additionalData, null, 0);
            if (LogApi)
                new Exception($"RequestToken: {response}").LogError();
            return response;
        }

        [Obsolete("this Method is obsolete")]
        public VerifyTransactionResult VerifyTransaction(string refNum)
        {
            var response = _api.verifyTransaction(refNum, merchantId); //verifyTransaction1
            if (LogApi)
                new Exception($"VerifyTransaction: {response}").LogError();

            var status = SamanMessageStatus.Success;
            if (response < 0)
                status = response.ToInt().ToEnum<SamanMessageStatus>();

            return new VerifyTransactionResult { _amount = response, Status = status };
        }

        [Obsolete("this Method is obsolete")]
        public bool ReverseTransaction(string refNum, string merchantId)
        {
            var response = _api.reverseTransaction(refNum, merchantId, merchantId, password); //reverseTransaction1
            if (LogApi)
                new Exception($"ReverseTransaction: {response}").LogError();

            return response == 1;
        }

        [Obsolete("this Method is obsolete")]
        public void MakePayment(long amount, string additionalData, long orderId, string callBackUrl, HttpContextBase httpContext = null)
        {
            if (httpContext == null)
                httpContext = HttpContext.Current.HttpContextBase();

            var url = httpContext.GetBaseUrl() + "/" + callBackUrl.TrimStart('/');
            var token = RequestToken(amount, additionalData, orderId);

            var postBody = new { Amount = amount.ToString(), MID = merchantId, ResNum = orderId.ToString(), Token = token, RedirectURL = url };
            MakePayment(postBody, merchantUrl, "form1", "post", null, httpContext);
        }
        #endregion

        #region Async Methods
        public async Task<string> RequestTokenAsync(long amount, string additionalData, long orderId)
        {
            var response = await _apiToken.RequestTokenAsync(merchantId, orderId.ToString(), amount * 10, 0, 0, 0, 0, 0, 0, additionalData, null, 0);
            if (LogApi)
                new Exception($"RequestToken: {response}").LogError();
            return response;
        }

        public async Task<VerifyTransactionResult> VerifyTransactionAsync(string refNum)
        {
            var response = await _api.verifyTransactionAsync(refNum, merchantId); //verifyTransaction1
            if (LogApi)
                new Exception($"VerifyTransaction: {response}").LogError();

            var status = SamanMessageStatus.Success;
            if (response < 0)
                status = response.ToInt().ToEnum<SamanMessageStatus>();

            return new VerifyTransactionResult { _amount = response, Status = status };
        }

        public async Task<bool> ReverseTransactionAsync(string refNum, string merchantId)
        {
            var response = await _api.reverseTransactionAsync(refNum, merchantId, merchantId, password); //reverseTransaction1
            if (LogApi)
                new Exception($"ReverseTransaction: {response}").LogError();

            return response == 1;
        }

        public async Task MakePaymentAsync(long amount, string additionalData, long orderId, string callBackUrl, HttpContextBase httpContext = null)
        {
            if (httpContext == null)
                httpContext = HttpContext.Current.HttpContextBase();

            var url = httpContext.GetBaseUrl() + "/" + callBackUrl.TrimStart('/');
            var token = await RequestTokenAsync(amount, additionalData, orderId);

            var postBody = new { Amount = amount.ToString(), MID = merchantId, ResNum = orderId.ToString(), Token = token, RedirectURL = url };
            MakePayment(postBody, merchantUrl, "form1", "post", null, httpContext);
        }
        #endregion

        #region Utility Methods
        public string GetStatusMessage(int resCode)
        {
            var response = resCode.ToEnum<SamanMessageStatus>();
            return response.ToDescription();
        }

        public string GetStateMessage(string state)
        {
            ResponseStates.TryGetValue(state, out string value);
            return value;
        }

        public bool IsStateOk(string state)
        {
            return state == "OK";
        }
        #endregion
    }
}
