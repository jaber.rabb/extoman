﻿using System.Threading.Tasks;
using System.Web;
using Xtoman.Domain.WebServices.Payment.Saman;

namespace Xtoman.Service.WebServices
{
    public interface ISamanService
    {
        string RequestToken(long amount, string additionalData, long orderId);
        VerifyTransactionResult VerifyTransaction(string refNum);
        bool ReverseTransaction(string refNum, string merchantId);
        void MakePayment(long amount, string additionalData, long orderId, string callBackUrl, HttpContextBase httpContext = null);

        Task<string> RequestTokenAsync(long amount, string additionalData, long orderId);
        Task<VerifyTransactionResult> VerifyTransactionAsync(string refNum);
        Task<bool> ReverseTransactionAsync(string refNum, string merchantId);
        Task MakePaymentAsync(long amount, string additionalData, long orderId, string callBackUrl, HttpContextBase httpContext = null);

        string GetStatusMessage(int resCode);
        string GetStateMessage(string state);
        bool IsStateOk(string state);
    }
}
