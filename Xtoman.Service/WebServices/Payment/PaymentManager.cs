﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Domain.WebServices;
using Xtoman.Domain.WebServices.Payment.CoinPayments;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class PaymentManager : IPaymentManager
    {
        private readonly IPerfectMoneyService _perfectMoneyService;
        private readonly IPerfectMoneyStatusService _perfectMoneyStatusService;
        private readonly IPayeerService _payeerService;
        private readonly IPayeerStatusService _payeerStatusService;
        private readonly ICoinPaymentsService _coinPaymentsService;
        private readonly IUserService _userService;
        //private readonly IUserPaymentService _userPaymentService;
        private readonly IOrderService _orderService;
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly PaymentSettings _paymentSettings;
        private readonly string[] PAYEERIPs;
        public PaymentManager(
            IPerfectMoneyService perfectMoneyService,
            IPerfectMoneyStatusService perfectMoneyStatusService,
            IPayeerService payeerService,
            IPayeerStatusService payeerStatusService,
            ICoinPaymentsService coinPaymentsService,
            IUserService userService,
            //IUserPaymentService userPaymentService,
            IOrderService orderService,
            IExchangeOrderService exchangeOrderService,
            PaymentSettings paymentSettings)
        {
            _perfectMoneyService = perfectMoneyService;
            _perfectMoneyStatusService = perfectMoneyStatusService;
            _payeerService = payeerService;
            _payeerStatusService = payeerStatusService;
            _coinPaymentsService = coinPaymentsService;
            _userService = userService;
            _paymentSettings = paymentSettings;
            //_userPaymentService = userPaymentService;
            _orderService = orderService;
            _exchangeOrderService = exchangeOrderService;
            PAYEERIPs = new string[] { "185.71.65.92", "185.71.65.189", "149.202.17.210" };
        }

        public async Task WithdrawFiatUSDAsync(long orderId, ECurrencyType type, decimal receiveAmount, string receiveAddress)
        {
            await _perfectMoneyService.TransferAsync(AppSettingManager.PerfectMoney_USD_Account, Convert.ToDouble(receiveAmount), 1, orderId);
        }

        public CoinPaymentFullAddressResult GetCoinPaymentAddress(ECurrencyType eCurrencyAccountType, string callbackUrl = "")
        {
            var coinType = CoinPaymentsCurrencyHelper.ToCoinPaymentsCurrency(eCurrencyAccountType.ToDisplay(DisplayProperty.ShortName));
            if (!coinType.HasValue) return null;
            //var coinPaymentsDepositAddress = _coinPaymentsService.CreateTransaction(coinType.Value, callbackUrl);
            return new CoinPaymentFullAddressResult()
            {
                //    Address = coinPaymentsDepositAddress.result.address,
                //    PaymentId = coinPaymentsDepositAddress.result.dest_tag,
                //    //Type = CoinPayments
            };
            //UNDONE: CoinPayments
        }

        public CoinPaymentFullAddressResult GetCoinToFiatPaymentAddress(ECurrencyType eCurrencyAccountType, Guid orderGuid, decimal payAmount, string callbackUrl = "")
        {
            var coinType = CoinPaymentsCurrencyHelper.ToCoinPaymentsCurrency(eCurrencyAccountType.ToDisplay(DisplayProperty.ShortName));
            if (coinType.HasValue)
            {
                var coinPaymentsCreateTransaction = _coinPaymentsService.CreateTransaction(new CreateTransactionInput()
                {
                    amount = payAmount,
                    CoinPaymentsCurrency1 = coinType.Value,
                    CoinPaymentsCurrency2 = coinType.Value,
                    custom = orderGuid.ToString(),
                    ipn_url = callbackUrl
                });

                if (coinPaymentsCreateTransaction.result != null)
                {
                    return new CoinPaymentFullAddressResult()
                    {
                        ApiType = WalletApiType.CoinPayments,
                        Id = coinPaymentsCreateTransaction.result.txn_id,
                        Address = coinPaymentsCreateTransaction.result.address,
                        QRUrl = coinPaymentsCreateTransaction.result.qrcode_url,
                        MemoTagPaymentId = coinPaymentsCreateTransaction.result.dest_tag,
                        StatusUrl = coinPaymentsCreateTransaction.result.status_url,
                        ConfirmsNeed = coinPaymentsCreateTransaction.result.confirms_needed,
                        TimeEnd = DateTime.UtcNow.AddSeconds(coinPaymentsCreateTransaction.result.timeout)
                    };
                }
            }
            return null;
        }

        public async Task<CoinPaymentAddressResult> GetCoinPaymentAddressAsync(ECurrencyType eCurrencyAccountType, bool isERC20, string callbackUrl = "")
        {
            var currencySign = isERC20 ? ECurrencyType.Ethereum.ToCoinPaymentsCurrency() : eCurrencyAccountType.ToCoinPaymentsCurrency();
            var cpca = await _coinPaymentsService.CallbackAddressAsync(currencySign, callbackUrl);
            if (cpca.result != null)
            {
                return new CoinPaymentAddressResult
                {
                    Address = cpca.result.address,
                    MemoTagPaymentId = cpca.result.dest_tag,
                    PubKey = cpca.result.pubkey
                };
            }
            else
            {
                if (cpca.error.HasValue())
                    new Exception($"CP CallBackAddress: {cpca.error}").LogError();
            }
            return null;
        }


        //public async Task<CoinPaymentAddressResult> GetCoinToFiatPaymentAddressAsync(ECurrencyType eCurrencyAccountType, string callbackUrl = "")
        //{
        //    var currencySign = eCurrencyAccountType.ToCoinPaymentsCurrency();
        //    var cpca = await _coinPaymentsService.CallbackAddressAsync(currencySign, callbackUrl);
        //    if (cpca.result != null)
        //    {
        //        return new CoinPaymentAddressResult
        //        {
        //            Address = cpca.result.address,
        //            MemoTagPaymentId = cpca.result.dest_tag,
        //            PubKey = cpca.result.pubkey
        //        };
        //    }
        //    else
        //    {
        //        if (cpca.error.HasValue())
        //            new Exception($"CP CallBackAddress: {cpca.error}").LogError();
        //    }
        //    return null;
        //}

        public async Task<CoinPaymentFullAddressResult> GetCoinToFiatPaymentAddressAsync(ECurrencyType eCurrencyAccountType, Guid orderGuid, decimal payAmount, string callbackUrl = "")
        {
            var currencySign = eCurrencyAccountType.ToCoinPaymentsCurrency();
            //var coinPaymentCallbackAddress = await _coinPaymentsService.CallbackAddressAsync(currencySign, callbackUrl);
            var coinPaymentsCreateTransaction = await _coinPaymentsService.CreateTransactionAsync(payAmount, currencySign, currencySign, orderGuid.ToString(), callbackUrl);

            if (coinPaymentsCreateTransaction.result != null)
            {
                return new CoinPaymentFullAddressResult()
                {
                    ApiType = WalletApiType.CoinPayments,
                    Id = coinPaymentsCreateTransaction.result.txn_id,
                    Address = coinPaymentsCreateTransaction.result.address,
                    QRUrl = coinPaymentsCreateTransaction.result.qrcode_url,
                    MemoTagPaymentId = coinPaymentsCreateTransaction.result.dest_tag,
                    StatusUrl = coinPaymentsCreateTransaction.result.status_url,
                    ConfirmsNeed = coinPaymentsCreateTransaction.result.confirms_needed,
                    TimeEnd = DateTime.UtcNow.AddSeconds(coinPaymentsCreateTransaction.result.timeout),
                    //PubKey = coinPaymentsCreateTransaction.result.pubkey
                };
            }
            else
            {
                if (coinPaymentsCreateTransaction.error.HasValue())
                    new Exception($"CP CreateTransaction: {coinPaymentsCreateTransaction.error}").LogError();
            }
            //}
            return null;
        }

        public void CoinPaymentIPNSubmit()
        {

        }

        public async Task<bool> InsertPAYEERPaymentStatusAsync(PAYEERPaymentStatus status)
        {
            if (await _payeerStatusService.TableNoTracking.Where(p => p.m_orderid == status.m_orderid).AnyAsync())
                return true;

            await _payeerStatusService.InsertAsync(status);
            return status.Id != 0;
        }

        public async Task<bool> InsertPerfectMoneyStatusAsync(PerfectMoneyPaymentStatus status)
        {
            if (await _perfectMoneyStatusService.TableNoTracking.Where(p => p.PAYMENT_ID == status.PAYMENT_ID).AnyAsync())
                return true;

            await _perfectMoneyStatusService.InsertAsync(status);
            return status.Id != 0;
        }

        public async Task<(bool status, double amount, string batchNumber)> PaymentPerfectVoucherPaymentAsync(string voucherNumber, string voucherActivation)
        {
            var result = await _perfectMoneyService.EvoucherActivationAsync(AppSettingManager.PerfectMoney_USD_Account, voucherNumber, voucherActivation);
            if (result.ERROR == null || !result.ERROR.HasValue())

                return (true, result.VoucherAmount, result.PaymentBatchNumber);

            return (false, 0, result.ERROR);
        }

        public async Task<bool> IsPAYEERPaymentValidAsync(PAYEERPaymentStatus status, HttpContextBase httpContext = null)
        {
            var amount = await _orderService.AmountByGuidAsync(status.m_orderid);
            var userIp = httpContext.Request.GetClientIpAddress();

            return amount == status.m_amount.ToDecimal() &&
                   PAYEERIPs.Any(x => x.Equals(userIp)) &&
                   !string.IsNullOrEmpty(status.m_operation_id) &&
                   !string.IsNullOrEmpty(status.m_sign) &&
                   status.m_sign == PAYEERStatusSign(status) &&
                   status.m_status == "success";
        }

        public bool IsPAYEERPaymentValid(PAYEERPaymentStatus status, HttpContextBase httpContext = null)
        {
            var amount = _orderService.AmountByGuid(status.m_orderid);
            var userIp = httpContext.Request.GetClientIpAddress();

            return PAYEERIPs.Any(x => x.Equals(userIp)) &&
                   !string.IsNullOrEmpty(status.m_operation_id) &&
                   !string.IsNullOrEmpty(status.m_sign) &&
                   status.m_sign == PAYEERStatusSign(status) &&
                   status.m_status == "success";
        }

        public void MakePayment(decimal amount, ECurrencyType ECAccountType, string orderId, string description, HttpContextBase httpContext = null)
        {
            var currency = "USD";
            switch (ECAccountType)
            {
                case ECurrencyType.PAYEER:
                    description = description.Base64Encode();
                    var sign = PAYEERMakePaymentSign(orderId, amount, currency, description);
                    _payeerService.MakePayment(new MakePayeerPaymentParams()
                    {
                        lang = "en",
                        m_amount = amount,
                        m_curr = currency,
                        m_shop = AppSettingManager.PAYEER_MerchantShopId, //_paymentSettings.PAYEER_MerchantShopId,
                        m_orderid = orderId,
                        m_sign = sign,
                        m_desc = description,
                    }, httpContext);
                    break;
                case ECurrencyType.PerfectMoney:
                    var urlConst = httpContext.GetBaseUrl() + "/" + "{0}".TrimStart('/');
                    var parameters = new MakePerfectMoneyPaymentParams()
                    {
                        PAYEE_ACCOUNT = AppSettingManager.PerfectMoney_USD_Account,
                        PAYMENT_AMOUNT = Convert.ToDouble(amount.ToBlockChainPrice(2)),
                        PAYMENT_UNITS = currency,
                        PAYMENT_ID = orderId,
                        PAYEE_NAME = "Extoman",
                        PAYMENT_URL_METHOD = "POST",
                        NOPAYMENT_URL_METHOD = "POST",
                        SUGGESTED_MEMO = description,
                        STATUS_URL = string.Format(urlConst, "ExchangeOrder/PMStatus/"),
                        PAYMENT_URL = string.Format(urlConst, $"Payment/Success/"),
                        NOPAYMENT_URL = string.Format(urlConst, $"Payment/UnSuccess/")
                    };
                    _perfectMoneyService.MakePayment(parameters, httpContext);
                    break;
            }
        }
        public void MakePayment(decimal amount, ECurrencyType ECAccountType, string orderId, string description, string statusUrl, string successUrl, string noPaymentUrl, HttpContextBase httpContext = null)
        {
            var currency = "USD";
            switch (ECAccountType)
            {
                case ECurrencyType.PAYEER:
                    description = description.Base64Encode();
                    var sign = PAYEERMakePaymentSign(orderId, amount, currency, description);
                    _payeerService.MakePayment(new MakePayeerPaymentParams()
                    {
                        lang = "en",
                        m_amount = amount,
                        m_curr = currency,
                        m_shop = AppSettingManager.PAYEER_MerchantShopId, //_paymentSettings.PAYEER_MerchantShopId,
                        m_orderid = orderId,
                        m_sign = sign,
                        m_desc = description,
                    }, httpContext);
                    break;
                case ECurrencyType.PerfectMoney:
                    var urlConst = httpContext.GetBaseUrl() + "/" + "{0}".TrimStart('/');
                    _perfectMoneyService.MakePayment(new MakePerfectMoneyPaymentParams()
                    {
                        PAYEE_ACCOUNT = AppSettingManager.PerfectMoney_USD_Account,
                        PAYMENT_AMOUNT = Convert.ToDouble(amount.ToBlockChainPrice(2)),
                        PAYMENT_UNITS = currency,
                        PAYMENT_ID = orderId,
                        PAYEE_NAME = "XtraMove",
                        PAYMENT_URL_METHOD = "POST",
                        NOPAYMENT_URL_METHOD = "POST",
                        SUGGESTED_MEMO = description,
                        STATUS_URL = string.Format(urlConst, statusUrl),
                        PAYMENT_URL = string.Format(urlConst, successUrl),
                        NOPAYMENT_URL = string.Format(urlConst, noPaymentUrl)
                    }, httpContext);
                    break;
            }
        }

        #region Helper
        private string PAYEERStatusSign(PAYEERPaymentStatus status)
        {
            string[] arHash = new string[] {
                status.m_operation_id,
                status.m_operation_ps,
                status.m_operation_date,
                status.m_operation_pay_date,
                status.m_shop,
                status.m_orderid,
                status.m_amount,
                status.m_curr,
                status.m_desc,
                status.m_status,
                AppSettingManager.PAYEER_SecretKey//_paymentSettings.PAYEER_SecretKey
            };
            return string.Join(":", arHash).CalculateSha256Hash().ToUpper();
        }

        private string PAYEERMakePaymentSign(string orderId, decimal amount, string currency, string description)
        {
            string[] arHash = new string[] {
                AppSettingManager.PAYEER_MerchantShopId,
                orderId,
                amount.ToString(),
                currency,
                description,
                AppSettingManager.PAYEER_SecretKey
            };
            return string.Join(":", arHash).CalculateSha256Hash().ToUpper();
        }

        public async Task<bool> IsPerfectMoneyPaymentValidAsync(PerfectMoneyPaymentStatus status, HttpContextBase httpContext = null)
        {
            var amount = await _orderService.AmountByGuidAsync(status.PAYMENT_ID);
            if (!status.PAYEE_ACCOUNT.Equals(AppSettingManager.PerfectMoney_USD_Account) || !status.PAYMENT_AMOUNT.Equals(amount.ToString()))
                return false;

            var v2hash = string.Join(":", new string[] {
                status.PAYMENT_ID,
                status.PAYEE_ACCOUNT,
                status.PAYMENT_AMOUNT,
                status.PAYMENT_UNITS,
                status.PAYMENT_BATCH_NUM,
                status.PAYER_ACCOUNT,
                AppSettingManager.PerfectMoney_Alt_Passphere.CalculateMD5Hash().ToUpper(), // Alt Passphere hash
                status.TIMESTAMPGMT
            }).CalculateMD5Hash().ToUpper();

            return v2hash == status.V2_HASH;
        }

        public async Task<List<PerfectMoneyBalanceItem>> PerfectMoneyBalanceAsync()
        {
            var balanceResult = await _perfectMoneyService.BalanceAsync();
            return balanceResult.ERROR == null ||
                string.IsNullOrWhiteSpace(balanceResult.ERROR) ?
                                        balanceResult.Accounts :
                                        null;
        }

        public async Task<PayeerBalanceItemModel> PAYEERBalanceAsync()
        {
            var balanceResult = await _payeerService.CheckBalanceAsync();
            var hasError = balanceResult.auth_error != "0" && balanceResult.errors.Any();
            return hasError ? null : balanceResult.balance;
        }

        public async Task<AutoPayResult> AutoPayExchangeAsync(long orderId)
        {
            string payerAccountNumber = "";
            var autoPayResult = new AutoPayResult() { Status = false, Text = "سفارش قبلا کامل شده است." };
            var exchangeOrder = await _exchangeOrderService.Table
                .Include(p => p.User)
                .Where(p => p.Id == orderId).SingleAsync();
            if (exchangeOrder.PaymentStatus != PaymentStatus.Paid) return new AutoPayResult() { Status = false, Text = "هزینه سفارش پرداخت نشده است." };
            if (exchangeOrder.OrderStatus == OrderStatus.Complete) return autoPayResult;
            switch (exchangeOrder.Exchange.ToCurrency.Type)
            {
                case ECurrencyType.PAYEER:
                    payerAccountNumber = AppSettingManager.PAYEER_AccountId;
                    var payeerResult = await _payeerService
                        .TransferAsync(new PayeerTransferInput()
                        {
                            curIn = "USD",
                            curOut = "USD",
                            sum = exchangeOrder.ReceiveAmount,
                            to = exchangeOrder.ReceiveAddress
                        });
                    var payeerStatus = payeerResult.auth_error == "0" && payeerResult.errors.Count == 0;
                    autoPayResult = new AutoPayResult()
                    {
                        Status = payeerStatus,
                        Text = payeerStatus ? $"Autopayment for order with number: {orderId} succeeded!" : string.Join(", ", payeerResult.errors),
                        TransactionCode = payeerResult.historyId
                    };
                    break;
                case ECurrencyType.PerfectMoney:
                    payerAccountNumber = AppSettingManager.PerfectMoney_USD_Account;
                    var pmResult = await _perfectMoneyService
                        .TransferAsync(exchangeOrder.ReceiveAddress, Convert.ToDouble(exchangeOrder.ReceiveAmount), 1, $"{exchangeOrder.PayAmount} {exchangeOrder.Exchange.FromCurrency.Type.ToDisplay()} To ${exchangeOrder.ReceiveAmount} PM USD", exchangeOrder.Id);
                    var pmStatus = pmResult.ERROR == null || string.IsNullOrWhiteSpace(pmResult.ERROR);
                    autoPayResult = new AutoPayResult()
                    {
                        Status = pmStatus,
                        Text = pmStatus ? $"Autopayment for order with number: {orderId} succeeded!" : pmResult.ERROR,
                        TransactionCode = pmResult.PAYMENT_BATCH_NUM
                    };
                    break;
                default:
                    autoPayResult = new AutoPayResult()
                    {
                        Status = false,
                        Text = "Exchange type not found!",
                        TransactionCode = ""
                    };
                    break;
            }
            if (autoPayResult.Status)
            {
                exchangeOrder.OrderStatus = OrderStatus.Complete;
                exchangeOrder.ReceiveTransactionCode = autoPayResult.TransactionCode;
                exchangeOrder.ReceiveDate = DateTime.Now;
                exchangeOrder.ReceivePayerAccountNo = payerAccountNumber;
                await _exchangeOrderService.UpdateAsync(exchangeOrder);
            }
            return autoPayResult;
        }

        public async Task<AutoPayResult> ManualPayExchangeAsync(long orderId, string transactionCode)
        {
            string payerAccountNumber = "";
            var manualPayResult = new AutoPayResult() { Status = false, Text = "سفارش قبلا کامل شده است." };
            var exchangeOrder = await _exchangeOrderService.Table
                .Include(p => p.User)
                .Where(p => p.Id == orderId).SingleAsync();
            if (exchangeOrder.PaymentStatus != PaymentStatus.Paid) return new AutoPayResult() { Status = false, Text = "هزینه سفارش پرداخت نشده است." };
            if (exchangeOrder.OrderStatus == OrderStatus.Complete) return manualPayResult;
            switch (exchangeOrder.Exchange.ToCurrency.Type)
            {
                case ECurrencyType.PAYEER:
                    payerAccountNumber = AppSettingManager.PAYEER_AccountId;
                    break;
                case ECurrencyType.PerfectMoney:
                    payerAccountNumber = AppSettingManager.PerfectMoney_USD_Account;
                    break;
                case ECurrencyType.Shetab:
                    payerAccountNumber = _paymentSettings.Shetab_CardNumber;
                    break;
            }
            manualPayResult = new AutoPayResult()
            {
                Status = true,
                Text = $"Manualpayment for order with number: {orderId} succeeded!",
                TransactionCode = transactionCode
            };

            exchangeOrder.OrderStatus = OrderStatus.Complete;
            exchangeOrder.ReceiveTransactionCode = manualPayResult.TransactionCode;
            exchangeOrder.ReceiveDate = DateTime.Now;
            exchangeOrder.ReceivePayerAccountNo = payerAccountNumber;
            await _exchangeOrderService.UpdateAsync(exchangeOrder);

            return manualPayResult;
        }

        //public async Task<BalanceResult> AllBalancesAsync()
        //{
        //    var perfectMoneyBalance = await PerfectMoneyBalanceAsync();
        //    var payeerBalance = await PAYEERBalanceAsync();
        //    //var coinsBalance = await _coinPaymentsService.CoinBalancesAsync();
        //    var result = new BalanceResult();
        //    result.Balances.Add(new BalanceResultItem()
        //    {
        //        Type = ECurrencyAccountType.PerfectMoney,
        //        Balance = perfectMoneyBalance?
        //                .Where(p => p.WalletNumber == AppSettingManager.PerfectMoney_USD_Account)
        //                .Select(p => p.Balance).DefaultIfEmpty(0).Sum() ?? 0
        //    });
        //    //Webmoney is missing
        //    result.Balances.Add(new BalanceResultItem() { Type = ECurrencyAccountType.PAYEER, Balance = payeerBalance?.USD.BUDGET ?? 0 });
        //    //result.Balances.Add(new BalanceResultItem() { Type = ECurrencyAccountType.Bitcoin, Balance = coinsBalance?.result?.BTC?.balancef ?? 0 });
        //    //result.Balances.Add(new BalanceResultItem() { Type = ECurrencyAccountType.Ethereum, Balance = coinsBalance?.result?.ETH?.balancef ?? 0 });
        //    //result.Balances.Add(new BalanceResultItem() { Type = ECurrencyAccountType.EthereumClassic, Balance = coinsBalance?.result?.ETC?.balancef ?? 0 });
        //    //result.Balances.Add(new BalanceResultItem() { Type = ECurrencyAccountType.Monero, Balance = coinsBalance?.result?.XMR?.balancef ?? 0 });
        //    return result;
        //}
        #endregion
    }
}
