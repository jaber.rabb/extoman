﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class VandarService : WebServiceManager, IVandarService
    {
        private readonly string baseUrl = "https://vandar.io/api/ipg/2step/";

        public async Task<VandarResult> ConfirmAsync(VandarConfirmInput input)
        {
            // The problem with confirm is True and true in json input. It must be true Except True.
            var result = await PostAsync("confirm", input);
            new Exception($"Vandar Confirm Request Input: ({input.Serialize()})").LogError();
            return LogAndDeserialize<VandarResult>(result, true);
        }

        public async Task<VandarSendResult> SendAsync(VandarSendInput input)
        {
            var result = await PostAsync("send", input);
            new Exception($"Vandar Send Request Input: ({input.Serialize()})").LogError();
            return LogAndDeserialize<VandarSendResult>(result, true);
        }

        public async Task<VandarVerifyResult> VandarVerifyAsync(VandarVerifyInput input)
        {
            var result = await PostAsync("verify", input);
            new Exception($"Vandar Verify Request Input: ({input.Serialize()})").LogError();
            return LogAndDeserialize<VandarVerifyResult>(result, true);
        }

        #region Helpers
        public async Task<string> PostAsync(string action, object obj = null)
        {
            var restClientRequest = new RestClientRequest(baseUrl, action);

            if (obj != null)
            {
                restClientRequest.RequestFormat = DataFormat.Json;
                restClientRequest.AddHeader("Content-Type", "application/json");
                restClientRequest.AddHeader("Accept", "application/json");
                restClientRequest.AddParameter("application/json", obj.Serialize().Replace("True", "true").Replace("False", "false"), ParameterType.RequestBody);
            }

            var searchOutputs = await restClientRequest.SendPostRequestAsync().ConfigureAwait(false);
            return searchOutputs.Content;
        }
        #endregion
    }
}
