﻿using System.Threading.Tasks;
using Xtoman.Domain.WebServices;

namespace Xtoman.Service.WebServices
{
    public interface IVandarService
    {
        Task<VandarSendResult> SendAsync(VandarSendInput input);
        Task<VandarResult> ConfirmAsync(VandarConfirmInput input);
        Task<VandarVerifyResult> VandarVerifyAsync(VandarVerifyInput input);
    }
}
