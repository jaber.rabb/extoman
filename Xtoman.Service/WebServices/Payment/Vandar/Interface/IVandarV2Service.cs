﻿using System;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Payment.Vandar;

namespace Xtoman.Service.WebServices
{
    public interface IVandarV2Service
    {
        /// <summary>
        /// ورود به حساب وندار جهت دریافت توکن
        /// </summary>
        /// <param name="mobile">موبایل</param>
        /// <param name="password">کلمه عبور</param>
        /// <returns>اطلاعات کاربر، توکن و ...</returns>
        Task<VandarLogin> LoginAsync(string mobile = null, string password = null);

        /// <summary>
        /// دریافت لیست کسب و کارها
        /// </summary>
        /// <returns></returns>
        Task<VandarBusinessList> GetBusinessListAsync();

        /// <summary>
        /// دریافت یک کسب و کار
        /// </summary>
        /// <param name="id">آی دی کسب و کار</param>
        /// <returns>کسب و کار</returns>
        Task<VandarBusiness> GetBusinessAsync(string id = null);

        /// <summary>
        /// دریافت لیست کاربرهای کسب و کار
        /// </summary>
        /// <param name="id">آی دی کسب و کار</param>
        /// <returns>لیست کاربرهای کسب و کار</returns>
        Task<VandarBusinessUsers> GetBusinessUsersAsync(string id = null);

        /// <summary>
        /// دریافت لیست شماره شبا های کسب و کار
        /// </summary>
        /// <param name="id">آی دی کسب و کار</param>
        /// <returns>لیست شماره شبا های کسب و کار</returns>
        Task<VandarBusinessIBANs> GetBusinessIBANsAsync(string id = null);

        /// <summary>
        /// افزودن شماره شبا به یک کسب و کار
        /// </summary>
        /// <param name="id">آی دی کسب و کار</param>
        /// <param name="IBAN">(Required) شماره شبا</param>
        /// <returns></returns>
        Task<VandarBusinessIBANs> AddNewIBANForBusinessAsync(string IBAN, string id = null);

        /// <summary>
        /// حذف شماره شبا از یک کسب و کار
        /// </summary>
        /// <param name="id">آی دی کسب و کار</param>
        /// <param name="ibanId">آی دی شماره شبا</param>
        /// <returns></returns>
        Task<VandarBase> DeleteBusinessIBANAsync(string ibanId, string id = null);

        /// <summary>
        /// دریافت لیست درگاه های کسب و کار
        /// </summary>
        /// <returns></returns>
        Task<VandarBusinessIPG> GetBusinessIPGAsync(string id = null);

        /// <summary>
        /// موجودی کیف پول کسب و کار
        /// </summary>
        /// <param name="id">آی دی کسب و کار</param>
        /// <returns></returns>
        Task<VandarBusinessBalance> GetBusinessBalanceAsync(string id = null);

        /// <summary>
        /// لیست تراکنش های کسب و کار
        /// </summary>
        /// <param name="id">آی دی کسب و کار</param>
        /// <param name="fromDate">(Optional) از تاریخ</param>
        /// <param name="toDate">(Optional) تا تاریخ</param>
        /// <param name="statusKind">(Optional) transactions or settlement</param>
        /// <param name="status">(Optional) succeed, failed, pending or canceled.</param>
        /// <param name="channel">(Optional) ipg, form</param>
        /// <param name="ref_id">(Optional) search by ref id.</param>
        /// <param name="tracking_code">(Optional) search by tracking code.</param>
        /// <param name="per_page">(Optional) تعداد تراکنش های هر صفحه</param>
        /// <returns></returns>
        Task<VandarBusinessTransactions> GetBusinessTransactionsAsync(int? page = null, DateTime? fromDate = null, DateTime? toDate = null, string statusKind = null, string status = null, string channel = null, string ref_id = null, string tracking_code = null, int? per_page = null, string id = null);

        /// <summary>
        /// دریافت لیست تسویه ها
        /// </summary>
        /// <param name="id">آی دی کسب و کار</param>
        /// <returns></returns>
        Task<VandarBusinessSettlementsList> GetSettlementsListAsync(int? page = null, string id = null);

        /// <summary>
        /// افزودن درخواست تسویه جدید
        /// </summary>
        /// <param name="id">آی دی کسب و کار</param>
        /// <param name="amount">مقدار به تومان</param>
        /// <param name="iban_id">آی دی شماره شبا کسب و کار</param>
        /// <param name="payment_number">شماره پرداخت دلخواه</param>
        /// <returns></returns>
        Task<VandarBusinessSettlementStore> AddNewSettlementAsync(long amount, string iban_id, int? payment_number = null, string id = null);

        /// <summary>
        /// دریافت اطلاعات تسویه با آی دی
        /// </summary>
        /// <param name="id">آی دی کسب و کار</param>
        /// <param name="settlementId">آی دی درخواست تسویه</param>
        /// <returns></returns>
        Task<VandarBusinessSettlementDisplay> GetSettlementAsync(string settlementId, string id = null);

        /// <summary>
        /// حذف یک درخواست تسویه
        /// </summary>
        /// <param name="id">آی دی کسب و کار</param>
        /// <param name="settlementId">آی دی درخواست تسویه</param>
        /// <returns></returns>
        Task<VandarBase> DeleteSettlementAsync(string transactionId, string id = null);
    }
}
