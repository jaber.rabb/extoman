﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Payment.Vandar;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service.WebServices
{
    public class VandarV2Service : WebServiceManager, IVandarV2Service
    {
        private readonly string baseUrl = "https://api.vandar.io/v2";
        private readonly string defaultMobile = "09131025264";
        private readonly string defaultPassword = "1!2@3#Lr";
        private readonly string defaultBusiness = "extoman";
        private readonly ICacheManager _cacheManager;
        public readonly string tokenKey = "Xtoman.Vandar.Token";
        public VandarV2Service(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        public async Task<VandarLogin> LoginAsync(string mobile = null, string password = null)
        {
            if (!mobile.HasValue())
                mobile = defaultMobile;
            if (!password.HasValue())
                password = defaultPassword;

            var response = await PostAsync("login", new { mobile, password });
            var result = LogAndDeserialize<VandarLogin>(response, true);

            if (result.Status == 1)
            {
                _cacheManager.Remove(tokenKey);
                _cacheManager.Set(tokenKey, result.Data.Token, 7200);
            }

            return result;
        }

        public async Task<VandarBusinessList> GetBusinessListAsync()
        {
            var response = await GetAsync("business");
            return LogAndDeserialize<VandarBusinessList>(response);
        }

        public async Task<VandarBusiness> GetBusinessAsync(string id = null)
        {
            if (!id.HasValue())
                id = defaultBusiness;

            var response = await GetAsync($"business/{id}");
            return LogAndDeserialize<VandarBusiness>(response);
        }

        public async Task<VandarBusinessUsers> GetBusinessUsersAsync(string id = null)
        {
            if (!id.HasValue())
                id = defaultBusiness;

            var response = await GetAsync($"business/{id}/iam");
            return LogAndDeserialize<VandarBusinessUsers>(response);
        }

        public async Task<VandarBusinessIBANs> GetBusinessIBANsAsync(string id = null)
        {
            if (!id.HasValue())
                id = defaultBusiness;

            var response = await GetAsync($"business/{id}/iban");
            return LogAndDeserialize<VandarBusinessIBANs>(response);
        }

        public async Task<VandarBusinessIBANs> AddNewIBANForBusinessAsync(string IBAN, string id = null)
        {
            if (!id.HasValue())
                id = defaultBusiness;

            var response = await PostAsync($"business/{id}/iban/store", new { IBAN });
            return LogAndDeserialize<VandarBusinessIBANs>(response);
        }

        public async Task<VandarBase> DeleteBusinessIBANAsync(string ibanId, string id = null)
        {
            if (!id.HasValue())
                id = defaultBusiness;

            var response = await DeleteAsync($"business/{id}/iban/{ibanId}");
            return LogAndDeserialize<VandarBase>(response);
        }

        public async Task<VandarBusinessIPG> GetBusinessIPGAsync(string id)
        {
            if (!id.HasValue())
                id = defaultBusiness;

            var response = await GetAsync($"business/{id}/ipg/show");
            return LogAndDeserialize<VandarBusinessIPG>(response);
        }

        public async Task<VandarBusinessBalance> GetBusinessBalanceAsync(string id)
        {
            if (!id.HasValue())
                id = defaultBusiness;

            var response = await GetAsync($"business/{id}/balance");
            return LogAndDeserialize<VandarBusinessBalance>(response);
        }

        public async Task<VandarBusinessTransactions> GetBusinessTransactionsAsync(int? page = null, DateTime? fromDate = null, DateTime? toDate = null, string statusKind = null, string status = null, string channel = null, string ref_id = null, string tracking_code = null, int? per_page = null, string id = null)
        {
            if (!id.HasValue())
                id = defaultBusiness;

            var response = await GetAsync($"business/{id}/transaction", new { page, fromDate, toDate, statusKind, status, channel, ref_id, tracking_code, per_page });
            return LogAndDeserialize<VandarBusinessTransactions>(response);
        }

        public async Task<VandarBusinessSettlementsList> GetSettlementsListAsync(int? page = null, string id = null)
        {
            if (!id.HasValue())
                id = defaultBusiness;

            var response = await GetAsync($"business/{id}/settlement", new { page });
            return LogAndDeserialize<VandarBusinessSettlementsList>(response);
        }

        public async Task<VandarBusinessSettlementStore> AddNewSettlementAsync(long amount, string iban_id, int? payment_number = null, string id = null)
        {
            if (!id.HasValue())
                id = defaultBusiness;

            var response = await PostAsync($"business/{id}/settlement/store", new { amount, iban_id, payment_number });
            return LogAndDeserialize<VandarBusinessSettlementStore>(response);
        }

        public async Task<VandarBusinessSettlementDisplay> GetSettlementAsync(string settlementId, string id = null)
        {
            if (!id.HasValue())
                id = defaultBusiness;

            var response = await GetAsync($"business/{id}/settlement/{settlementId}");
            return LogAndDeserialize<VandarBusinessSettlementDisplay>(response);
        }

        public async Task<VandarBase> DeleteSettlementAsync(string transactionId, string id = null)
        {
            if (!id.HasValue())
                id = defaultBusiness;

            var response = await DeleteAsync($"business/{id}/settlement/{transactionId}");
            return LogAndDeserialize<VandarBase>(response);
        }

        #region Helpers
        private async Task<string> GetTokenAsync()
        {
            return await _cacheManager.GetAsync(tokenKey, 7200, async () =>
            {
                var result = await LoginAsync();
                if (result.Status == 1 && result.Data != null)
                {
                    return result.Data.Token;
                }
                throw new Exception($"Vandar Login Error: {result.Message}");
            });
        }

        public async Task<string> PostAsync(string action, object inputs = null)
        {
            var restClientRequest = new RestClientRequest(baseUrl, action);
            restClientRequest.AddJsonParameter(inputs);

            if (!action.Equals("login", StringComparison.InvariantCultureIgnoreCase))
            {
                var accessToken = await GetTokenAsync();
                restClientRequest.AddParameter("Authorization", string.Format("Bearer " + accessToken), ParameterType.HttpHeader);
            }

            var searchOutputs = await restClientRequest.SendPostRequestAsync().ConfigureAwait(false);
            if (searchOutputs.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                new Exception("Vandar Authorization Failed!").LogError();

            return searchOutputs.Content;
        }

        public async Task<string> DeleteAsync(string action)
        {
            var restClientRequest = new RestClientRequest(baseUrl, action);

            var accessToken = await GetTokenAsync();
            restClientRequest.AddParameter("Authorization", string.Format("Bearer " + accessToken), ParameterType.HttpHeader);

            var searchOutputs = await restClientRequest.SendDeleteRequestAsync().ConfigureAwait(false);
            if (searchOutputs.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                new Exception("Vandar Authorization Failed!").LogError();

            return searchOutputs.Content;
        }

        public async Task<string> GetAsync(string action, object inputs = null)
        {
            IEnumerable<PropertyInfo> validProperties = new List<PropertyInfo>();
            var restClientRequest = new RestClientRequest(baseUrl, action);

            if (inputs != null)
            {
                validProperties = inputs.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                if (validProperties.Any())
                    foreach (var property in validProperties)
                    {
                        var value = property.GetValue(inputs)?.ToString() ?? "";
                        if (value.HasValue())
                            restClientRequest.AddQueryParameter(property.Name, property.GetValue(inputs)?.ToString() ?? "");
                    }
            }

            var accessToken = await GetTokenAsync();
            restClientRequest.AddParameter("Authorization", string.Format("Bearer " + accessToken), ParameterType.HttpHeader);

            var searchOutputs = await restClientRequest.SendGetRequestAsync().ConfigureAwait(false);
            if (searchOutputs.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                new Exception("Vandar Authorization Failed!").LogError();

            return searchOutputs.Content;
        }
        #endregion
    }
}