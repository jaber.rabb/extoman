﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using Xtoman.Domain.Settings;
using Xtoman.Domain.WebServices;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class PerfectMoneyService : PaymentService, IPerfectMoneyService
    {
        private const string baseUrl = "https://perfectmoney.com";
        private readonly string merchantUrl = $"{baseUrl}/api/step1.asp";
        private readonly string apiUrl = $"{baseUrl}/acct";
        public string accountMemberId { get; private set; }
        public string passPhrase { get; private set; }
        public string altPassPhrase { get; private set; }
        public string payerAccount { get; private set; }

        private readonly PaymentSettings _paymentSettings;
        public PerfectMoneyService(PaymentSettings paymentSettings)
        {
            _paymentSettings = paymentSettings;
            payerAccount = AppSettingManager.PerfectMoney_USD_Account;
            accountMemberId = AppSettingManager.PerfectMoney_MemberId;
            passPhrase = AppSettingManager.PerfectMoney_Passphere;
            altPassPhrase = AppSettingManager.PerfectMoney_Alt_Passphere;
        }

        #region Sync methods
        public void MakePayment(MakePerfectMoneyPaymentParams parameters, HttpContextBase httpContext = null)
        {
            new Exception("PM MakePayment Parameters:" + parameters.Serialize(JsonSerializeType.NewtonsoftJson)).LogError();
            MakePayment(parameters, merchantUrl, "PMForm", "POST", "PAYMENT_METHOD", httpContext);
        }

        public PerfectMoneyBalanceResult Balance()
        {
            var response = PostRequest("balance");
            return BalanceResultToModel(ParseResponse(response));
        }

        public PerfectMoneyEvoucherPurchaseResult EvoucherPurchase(double amount)
        {
            var response = PostRequest("ev_create", new
            {
                Payer_Account = payerAccount,
                Amount = XmlConvert.ToString(amount)
            });
            return EvoucherPurchaseResultToModel(ParseResponse(response));
        }

        public PerfectMoneyEvoucherActivationResult EvoucherActivation(string payeeAccount, string voucherNumber, string voucherActivationCode)
        {
            var respone = PostRequest("ev_activate", new {
                Payee_Account = payeeAccount,
                ev_number = voucherNumber,
                ev_code = voucherActivationCode
            });
            return EvoucherActivationResultToModel(ParseResponse(respone));
        }

        public PerfectMoneyTransferResult Transfer(string payeeAccount, double amount, int payIn, long paymentId)
        {
            var response = PostRequest("confirm", new
            {
                Payer_Account = payerAccount,
                Payee_Account = payeeAccount,
                Amount = XmlConvert.ToString(amount),
                PAY_IN = payIn,
                PAYMENT_ID = paymentId
            });
            return TransferResultToModel(ParseResponse(response));
        }

        public PerfectMoneyTransferResult Transfer(string payeeAccount, double amount, int payIn, string memo, long paymentId)
        {
            var response = PostRequest("confirm", new
            {
                Payer_Account = payerAccount,
                Payee_Account = payeeAccount,
                Amount = XmlConvert.ToString(amount),
                PAY_IN = payIn,
                PAYMENT_ID = paymentId,
                Memo = memo
            });
            return TransferResultToModel(ParseResponse(response));
        }


        public List<Dictionary<string, string>> GetEvouchersCreatedListing()
        {
            var response = PostRequest("evcsv");
            return ParseListResponse(response);
        }


        public List<PerfectMoneyHistoryResult> GetAccountHistory(DateTime start, DateTime end)
        {
            var response = PostRequest("historycsv");
            return HistoryResultToModel(ParseListResponse(response));
        }
        #endregion

        #region Async methods
        public async Task<PerfectMoneyBalanceResult> BalanceAsync()
        {
            var response = await PostRequestAsync("balance");
            return BalanceResultToModel(ParseResponse(response));
        }

        public async Task<PerfectMoneyEvoucherPurchaseResult> EvoucherPurchaseAsync(double amount)
        {
            var response = await PostRequestAsync("ev_create", new
            {
                Payer_Account = payerAccount,
                Amount = XmlConvert.ToString(amount)
            });
            return EvoucherPurchaseResultToModel(ParseResponse(response));
        }

        public async Task<PerfectMoneyTransferResult> TransferAsync(string payeeAccount, double amount, int payIn, long paymentId)
        {
            var response = await PostRequestAsync("confirm", new
            {
                Payer_Account = payerAccount,
                Payee_Account = payeeAccount,
                Amount = XmlConvert.ToString(amount),
                PAY_IN = payIn,
                PAYMENT_ID = paymentId
            });
            return TransferResultToModel(ParseResponse(response));
        }

        public async Task<PerfectMoneyEvoucherActivationResult> EvoucherActivationAsync(string payeeAccount, string voucherNumber, string voucherActivationCode)
        {
            var respone = await PostRequestAsync("ev_activate", new
            {
                Payee_Account = payeeAccount,
                ev_number = voucherNumber,
                ev_code = voucherActivationCode
            });
            return EvoucherActivationResultToModel(ParseResponse(respone));
        }

        public async Task<PerfectMoneyTransferResult> TransferAsync(string payeeAccount, double amount, int payIn, string memo, long paymentId)
        {
            var response = await PostRequestAsync("confirm", new
            {
                Payer_Account = payerAccount,
                Payee_Account = payeeAccount,
                Amount = XmlConvert.ToString(amount),
                PAY_IN = payIn,
                PAYMENT_ID = paymentId,
                Memo = memo
            });
            return TransferResultToModel(ParseResponse(response));
        }

        public async Task<List<Dictionary<string, string>>> GetEvouchersCreatedListingAsync()
        {
            var response = await PostRequestAsync("evcsv");
            return ParseListResponse(response);
        }

        public async Task<List<PerfectMoneyHistoryResult>> GetAccountHistoryAsync(DateTime start, DateTime end)
        {
            var response = await PostRequestAsync("historycsv");
            return HistoryResultToModel(ParseListResponse(response));
        }
        #endregion

        #region Helpers
        private RestClientRequest PrepareRestClientRequest(string action, object obj = null)
        {
            var urlAction = string.IsNullOrWhiteSpace(action) ? "" : action + ".asp";
            var restClientRequest = new RestClientRequest(apiUrl, urlAction);
            PrepareRestClient(restClientRequest);
            if (obj != null)
            {
                var validProperties = obj.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                foreach (var property in validProperties)
                    restClientRequest.AddParameter(property.Name, property.GetValue(obj)?.ToString() ?? "");
            }
            return restClientRequest;
        }

        private string PostRequest(string action, object obj = null)
        {
            var searchOutputs = PrepareRestClientRequest(action, obj).SendPostRequest();
            return searchOutputs.Content;
        }

        private async Task<string> PostRequestAsync(string action, object obj = null)
        {
            var restClientRequest = PrepareRestClientRequest(action, obj);
            var searchOutputs = await restClientRequest.SendPostRequestAsync();
            return searchOutputs.Content;
        }

        private RestClientRequest PrepareRestClient(RestClientRequest restClientRequest)
        {
            restClientRequest.AddObject(new
            {
                AccountID = accountMemberId,
                PassPhrase = passPhrase
            });
            return restClientRequest;
        }
        #endregion

        #region Response ToModel Helpers
        protected Dictionary<string, string> ParseResponse(string response)
        {
            Dictionary<string, string> results = new Dictionary<string, string>();
            try
            {
                if (response == null) return null;

                Regex regEx = new Regex("<input name='(.*)' type='hidden' value='(.*)'>");
                MatchCollection matches = regEx.Matches(response);
                foreach (Match match in matches)
                {
                    results.Add(match.Groups[1].Value, match.Groups[2].Value);
                }
            }
            catch (Exception ex)
            {
                new Exception(response, ex).LogError();
            }
            return results;
        }

        protected List<Dictionary<string, string>> ParseListResponse(string response)
        {
            string[] lines = response.Split(new char[] { '\r', '\n' });
            if (lines.Length < 2) return null;
            string[] fields = lines[0].Split(new char[] { ',' });

            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();
            Dictionary<string, string> line;
            string[] values;

            for (int y = 1; y < lines.Length; y++)
            {
                values = lines[y].Split(new char[] { ',' });
                if (values.Length != fields.Length) continue;

                line = new Dictionary<string, string>();
                for (int x = 1; x < fields.Length; x++)
                {
                    line.Add(fields[x], values[x]);
                }
                result.Add(line);
            }

            return result;
        }

        protected PerfectMoneyBalanceResult BalanceResultToModel(Dictionary<string, string> balanceResult)
        {
            var accounts = new List<PerfectMoneyBalanceItem>();
            foreach (var item in balanceResult)
            {
                if (item.Key != "ERROR")
                {
                    accounts.Add(new PerfectMoneyBalanceItem()
                    {
                        WalletNumber = item.Key,
                        Balance = Convert.ToDecimal(item.Value)
                    });
                }
            }
            var error = balanceResult.Where(p => p.Key == "ERROR").Select(p => p.Value).FirstOrDefault() ?? "";
            return new PerfectMoneyBalanceResult() { Accounts = accounts, ERROR = error };
        }

        protected PerfectMoneyTransferResult TransferResultToModel(Dictionary<string, string> transferResult)
        {
            var pmrmodel = new PerfectMoneyTransferResult();
            foreach (var item in transferResult)
            {
                switch (item.Key)
                {
                    case "ERROR":
                        pmrmodel.ERROR = item.Value;
                        break;
                    case "Payee_Account":
                        pmrmodel.Payee_Account = item.Value;
                        break;
                    case "Payee_Account_Name":
                        pmrmodel.Payee_Account_Name = item.Value;
                        break;
                    case "Payer_Account":
                        pmrmodel.Payer_Account = item.Value;
                        break;
                    case "PAYMENT_AMOUNT":
                        pmrmodel.PAYMENT_AMOUNT = item.Value;
                        break;
                    case "PAYMENT_BATCH_NUM":
                        pmrmodel.PAYMENT_BATCH_NUM = item.Value;
                        break;
                    case "PAYMENT_ID":
                        pmrmodel.PAYMENT_ID = item.Value;
                        break;
                }
            }
            return pmrmodel;
        }

        protected PerfectMoneyEvoucherActivationResult EvoucherActivationResultToModel(Dictionary<string, string> evoucherActivationResult)
        {
            var pmrmodel = new PerfectMoneyEvoucherActivationResult();
            foreach (var item in evoucherActivationResult)
            {
                switch (item.Key.ToLower())
                {
                    case "error":
                        pmrmodel.ERROR = item.Value;
                        break;
                    case "payee_account":
                    case "payee account":
                        pmrmodel.PayeeAccount = item.Value;
                        break;
                    case "payment_batch_num":
                    case "payment batch num":
                        pmrmodel.PaymentBatchNumber = item.Value;
                        break;
                    case "voucher_num":
                    case "voucher num":
                        pmrmodel.VoucherNumber = item.Value;
                        break;
                    case "voucher_amount_currency":
                    case "voucher amount currency":
                        pmrmodel.VoucherAmountCurrency = item.Value;
                        break;
                    case "voucher_amount":
                    case "voucher amount":
                        pmrmodel.VoucherAmount = Convert.ToDouble(item.Value);
                        break;
                }
            }
            return pmrmodel;
        }

        protected PerfectMoneyEvoucherPurchaseResult EvoucherPurchaseResultToModel(Dictionary<string, string> evoucherPurchaseResult)
        {
            var pmrmodel = new PerfectMoneyEvoucherPurchaseResult();
            foreach (var item in evoucherPurchaseResult)
            {
                switch (item.Key.ToLower())
                {
                    case "error":
                        pmrmodel.ERROR = item.Value;
                        break;
                    case "payer_account":
                    case "payer account":
                        pmrmodel.PayerAccount = item.Value;
                        break;
                    case "payment_amount":
                    case "payment amount":
                        pmrmodel.PaymentAmount = Convert.ToDouble(item.Value);
                        break;
                    case "payment_batch_num":
                    case "payment batch num":
                        pmrmodel.PaymentBatchNumber = item.Value;
                        break;
                    case "voucher_num":
                    case "voucher num":
                        pmrmodel.VoucherNumber = item.Value;
                        break;
                    case "voucher_code":
                    case "voucher code":
                        pmrmodel.VoucherCode = item.Value;
                        break;
                    case "voucher_amount":
                    case "voucher amount":
                        pmrmodel.VoucherAmount = Convert.ToDouble(item.Value);
                        break;
                }
            }
            return pmrmodel;
        }

        protected List<PerfectMoneyHistoryResult> HistoryResultToModel(List<Dictionary<string, string>> historyResult)
        {
            var result = new List<PerfectMoneyHistoryResult>();
            if (historyResult != null && historyResult.Count > 0)
            {
                foreach (var item in historyResult)
                {
                    result.Add(HistoryItemToModel(item));
                }
            }
            return result;
        }

        protected PerfectMoneyHistoryResult HistoryItemToModel(Dictionary<string, string> historyItem)
        {
            var pmrmodel = new PerfectMoneyHistoryResult();
            foreach (var item in historyItem)
            {
                switch (item.Key)
                {
                    case "Payee Account":
                    case "Payee_Account":
                        pmrmodel.Payee_Account = item.Value;
                        break;
                    case "Payer_Account":
                    case "Payer Account":
                        pmrmodel.Payer_Account = item.Value;
                        break;
                    case "Amount":
                        pmrmodel.Amount = Convert.ToDecimal(item.Value);
                        break;
                    case "Fee":
                        pmrmodel.Fee = Convert.ToDecimal(item.Value);
                        break;
                    case "Batch":
                        pmrmodel.Batch = item.Value;
                        break;
                    case "Payment_ID":
                        pmrmodel.Payment_ID = item.Value;
                        break;
                    case "Time":
                        pmrmodel.Time = Convert.ToDateTime(item.Value);
                        break;
                    case "Type":
                        pmrmodel.Type = item.Value;
                        break;
                    case "Currency":
                        pmrmodel.Currency = item.Value;
                        break;
                    case "Memo":
                        pmrmodel.Memo = item.Value;
                        break;
                }
            }
            return pmrmodel;
        }
        #endregion
    }
}
