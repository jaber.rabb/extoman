﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using Xtoman.Domain.WebServices;

namespace Xtoman.Service.WebServices
{
    public interface IPerfectMoneyService
    {
        #region Sync methods
        void MakePayment(MakePerfectMoneyPaymentParams parameters, HttpContextBase httpContext = null);
        PerfectMoneyBalanceResult Balance();
        PerfectMoneyEvoucherPurchaseResult EvoucherPurchase(double amount);
        PerfectMoneyEvoucherActivationResult EvoucherActivation(string payeeAccount, string voucherNumber, string voucherActivationCode);
        PerfectMoneyTransferResult Transfer(string payeeAccount, double amount, int payIn, long paymentId);
        PerfectMoneyTransferResult Transfer(string payeeAccount, double amount, int payIn, string memo, long paymentId);
        List<Dictionary<string, string>> GetEvouchersCreatedListing();
        List<PerfectMoneyHistoryResult> GetAccountHistory(DateTime start, DateTime end);
        #endregion

        #region Async methods
        Task<PerfectMoneyBalanceResult> BalanceAsync();
        Task<PerfectMoneyEvoucherPurchaseResult> EvoucherPurchaseAsync(double amount);
        Task<PerfectMoneyTransferResult> TransferAsync(string payeeAccount, double amount, int payIn, long paymentId);
        Task<PerfectMoneyTransferResult> TransferAsync(string payeeAccount, double amount, int payIn, string memo, long paymentId);
        Task<List<Dictionary<string, string>>> GetEvouchersCreatedListingAsync();
        Task<List<PerfectMoneyHistoryResult>> GetAccountHistoryAsync(DateTime start, DateTime end);
        Task<PerfectMoneyEvoucherActivationResult> EvoucherActivationAsync(string perfectMoney_USD_Account, string voucherNumber, string voucherActivation);
        #endregion
    }
}
