﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Service.WebServices
{
    public class MakePerfectMoneyPaymentParams
    {
        public string PAYEE_ACCOUNT { get; set; }
        public string PAYEE_NAME { get; set; }
        public string PAYMENT_ID { get; set; }
        public double PAYMENT_AMOUNT { get; set; }
        public string PAYMENT_UNITS { get; set; }
        public string STATUS_URL { get; set; }
        public string PAYMENT_URL { get; set; }
        public string PAYMENT_URL_METHOD { get; set; }
        public string NOPAYMENT_URL { get; set; }
        public string NOPAYMENT_URL_METHOD { get; set; }
        public string USERNAME { get; set; }
        public string SUGGESTED_MEMO { get; set; }
        public string BAGGAGE_FIELDS { get; set; }
    }
}
