﻿using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Payment.LocalBitcoins;

namespace Xtoman.Service.WebServices
{
    public interface ILocalBitcoinsService
    {
        #region Async methods

        #region Advertisements

        #endregion

        #region Trades

        #endregion

        #region Account
        /// <summary>
        /// Returns public user profile information.
        /// This API request lets you retrieve the public user information on a LocalBitcoins user. The response contains the same information that is found on an account's public profile page.
        /// Making this request with authentication returns extra information.
        /// If you have left feedback to the account you've requested the field my_feedback will have one of the following string values: trust, positive, neutral, block, block_without_feedback.
        /// If you have also set a feedback message for the user, it can be found from field my_feedback_msg.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        Task<LocalBitcoinsAccountInfo> GetAccountInfoAsync(string userName);
        #endregion

        #region Wallet
        /// <summary>
        /// Gets information about the token owner's wallet balance.
        /// </summary>
        /// <returns></returns>
        Task<LocalBitcoinsWallet> GetWalletAsync();

        /// <summary>
        /// Same as GetWalletAsync() method, but only returns the Message, ReceivingAddress and Total balance.
        /// Use this instead if you don't care about transactions at the moment.
        /// </summary>
        /// <returns></returns>
        Task<LocalBitcoinsWalletBalance> GetWalletBalanceAsync();

        /// <summary>
        /// Sends amount of bitcoins from the token owner's wallet to address.
        /// On success, the response returns a message indicating success.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        Task<LocalBitcoinsWalletSend> WalletSendAsync(string address, decimal amount);

        /// <summary>
        /// This method returns the current outgoing and deposit fees in bitcoins (BTC).
        /// </summary>
        /// <returns></returns>
        Task<LocalBitcoinsFee> FeeAsync();
        #endregion

        #region Invoices

        #endregion

        #region Public Market Data
        Task<LocalBitcoinsBuySellOnline> BuyOnlineAdsIranAsync(int? page = null);
        Task<LocalBitcoinsBuySellOnline> SellOnlineAdsIranAsync(int? page = null);
        #endregion

        #endregion

        #region Sync methods

        #region Wallet
        /// <summary>
        /// Gets information about the token owner's wallet balance.
        /// </summary>
        /// <returns></returns>
        LocalBitcoinsWallet GetWallet();

        /// <summary>
        /// Same as GetWalletAsync() method, but only returns the Message, ReceivingAddress and Total balance.
        /// Use this instead if you don't care about transactions at the moment.
        /// </summary>
        /// <returns></returns>
        LocalBitcoinsWalletBalance GetWalletBalance();
        #endregion

        #endregion

    }
}
