﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Payment.LocalBitcoins;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class LocalBitcoinsService : WebServiceManager, ILocalBitcoinsService
    {
        #region Properties
        private readonly int timeout = 10;
        private readonly Uri baseAddress = new Uri("https://localbitcoins.com/");
        private readonly string nonce = DateTime.UtcNow.ConvertToUnixTimestamp().ToString();
        private readonly string Key = AppSettingManager.LocalBitcoins_Key;
        private readonly string Secret = AppSettingManager.LocalBitcoins_Secret;
        private readonly HttpClient httpClient = new HttpClient()
        {
            BaseAddress = new Uri("https://localbitcoins.com/"),
            Timeout = TimeSpan.FromSeconds(10)
        };
        #endregion

        #region Constructor
        public LocalBitcoinsService()
        {
        }
        #endregion

        #region Async methods

        #region Account
        public async Task<LocalBitcoinsAccountInfo> GetAccountInfoAsync(string userName)
        {
            CheckRequiredProperty(userName, "userName", "GetAccountInfoAsync");
            return await CallApiAsync<LocalBitcoinsAccountInfo>(HttpMethod.Get, $"account_info/{userName}");
        }
        #endregion

        #region Wallet
        public async Task<LocalBitcoinsWallet> GetWalletAsync()
        {
            return await CallApiAsync<LocalBitcoinsWallet>(HttpMethod.Get, $"wallet", true);
        }
        public async Task<LocalBitcoinsWalletBalance> GetWalletBalanceAsync()
        {
            return await CallApiAsync<LocalBitcoinsWalletBalance>(HttpMethod.Get, $"wallet-balance", true);
        }
        public async Task<LocalBitcoinsWalletSend> WalletSendAsync(string address, decimal amount)
        {
            CheckRequiredProperty(address, "address", "WalletSendAsync");
            if (amount <= 0)
            {
                var error = new ArgumentNullException($"LocalBitcoins: amount is zero or lower than zero in WalletSendAsync method");
                error.LogError();
                throw error;
            }
            return await CallApiAsync<LocalBitcoinsWalletSend>(HttpMethod.Post, $"wallet-send", true, new { address, amount });
        }
        public async Task<LocalBitcoinsFee> FeeAsync()
        {
            return await CallApiAsync<LocalBitcoinsFee>(HttpMethod.Get, $"fees");
        }
        #endregion

        #region Public endpoints
        public async Task<LocalBitcoinsBuySellOnline> BuyOnlineAdsIranAsync(int? page = null)
        {
            //var url = "/buy-bitcoins-online/ir/iran-islamic-republic-of/.json";
            var url = "/buy-bitcoins-online/irr/.json";
            if (page.HasValue && page > 1)
                url += "?page=" + page.Value.ToString(CultureInfo.InvariantCulture);
            return await CallPublicAsync<LocalBitcoinsBuySellOnline>(url);
        }

        public async Task<LocalBitcoinsBuySellOnline> SellOnlineAdsIranAsync(int? page = null)
        {
            //var url = "/sell-bitcoins-online/ir/iran-islamic-republic-of/.json";
            var url = "/sell-bitcoins-online/irr/.json";
            if (page.HasValue && page > 1)
                url += "?page=" + page.Value.ToString(CultureInfo.InvariantCulture);
            return await CallPublicAsync<LocalBitcoinsBuySellOnline>(url);
        }
        #endregion

        #endregion

        #region Sync methods

        #region Wallet
        public LocalBitcoinsWallet GetWallet()
        {
            return CallApi<LocalBitcoinsWallet>(HttpMethod.Get, $"wallet", true);
        }

        public LocalBitcoinsWalletBalance GetWalletBalance()
        {
            return CallApi<LocalBitcoinsWalletBalance>(HttpMethod.Get, $"wallet-balance", true);
        }
        #endregion

        #endregion

        #region Helpers
        private T CallApi<T>(HttpMethod method, string action, bool isAuthenticatedRequest = false, object obj = null/*, bool getAsBinary = false*/) where T : LocalBitcoinsResult, new()
        {
            action = $"/api/{action}/";
            var url = action;
            HttpContent httpContent = null;
            var args = new Dictionary<string, string>();
            if (obj != null)
            {
                var argsString = JsonConvert.SerializeObject(obj, Formatting.None, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
                args = JsonConvert.DeserializeObject<Dictionary<string, string>>(argsString);
                if (method == HttpMethod.Post)
                    httpContent = new FormUrlEncodedContent(args);
                else if (method == HttpMethod.Get)
                    url += "?" + args.ToQueryParameters();
            }
            try
            {
                using (var request = new HttpRequestMessage(method, new Uri(httpClient.BaseAddress, url)))
                {
                    request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    if (isAuthenticatedRequest)
                    {
                        var signature = GetSignature(action, nonce, args);
                        request.Headers.Add("Apiauth-Key", Key);
                        request.Headers.Add("Apiauth-Nonce", nonce);
                        request.Headers.Add("Apiauth-Signature", signature);
                        request.Content = httpContent;
                    }
                    var response = httpClient.SendAsync(request).Result;
                    //if (getAsBinary)
                    //{
                    //    var resultAsByteArray = await response.Content.ReadAsByteArrayAsync().ConfigureAwait(false);
                    //    return resultAsByteArray;
                    //}
                    //else{
                    var resultAsString = response.Content.ReadAsStringAsync().Result;
                    //}
                    if (!response.IsSuccessStatusCode)
                    {
                        var json = LogAndDeserialize<dynamic>(resultAsString);
                        var error = new LocalBitcoinsException(true, action, json);
                        error.LogError();
                    }
                    return JsonConvert.DeserializeObject<T>(resultAsString);
                }
            }
            finally
            {
                httpContent?.Dispose();
            }
        }

        private async Task<T> CallPublicAsync<T>(string requestUrl) where T : new()
        {
            var response = await httpClient.GetAsync(requestUrl).ConfigureAwait(false);
            var resultAsString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            return LogAndDeserialize<T>(resultAsString);
        }

        private async Task<T> CallApiAsync<T>(HttpMethod method, string action, bool isAuthenticatedRequest = false, object obj = null/*, bool getAsBinary = false*/) where T : LocalBitcoinsResult, new()
        {
            action = $"/api/{action}/";
            var url = action;
            HttpContent httpContent = null;
            var args = new Dictionary<string, string>();
            if (obj != null)
            {
                var argsString = JsonConvert.SerializeObject(obj, Formatting.None, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
                args = JsonConvert.DeserializeObject<Dictionary<string, string>>(argsString);
                if (method == HttpMethod.Post)
                    httpContent = new FormUrlEncodedContent(args);
                else if (method == HttpMethod.Get)
                    url += "?" + args.ToQueryParameters();
            }
            try
            {
                using (var request = new HttpRequestMessage(method, new Uri(httpClient.BaseAddress, url)))
                {
                    request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    if (isAuthenticatedRequest)
                    {
                        var signature = GetSignature(action, nonce, args);
                        request.Headers.Add("Apiauth-Key", Key);
                        request.Headers.Add("Apiauth-Nonce", nonce);
                        request.Headers.Add("Apiauth-Signature", signature);
                        request.Content = httpContent;
                    }
                    var response = await httpClient.SendAsync(request).ConfigureAwait(false);
                    //if (getAsBinary)
                    //{
                    //    var resultAsByteArray = await response.Content.ReadAsByteArrayAsync().ConfigureAwait(false);
                    //    return resultAsByteArray;
                    //}
                    //else{
                    var resultAsString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    //}
                    if (!response.IsSuccessStatusCode)
                    {
                        var json = LogAndDeserialize<dynamic>(resultAsString);
                        var error = new LocalBitcoinsException(true, action, json);
                        error.LogError();
                    }
                    return JsonConvert.DeserializeObject<T>(resultAsString);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                httpContent?.Dispose();
            }
            return null;
        }

        private string GetSignature(string action, string nonce, Dictionary<string, string> args)
        {
            string paramsStr = null;

            if (args != null && args.Any())
                paramsStr = args.ToQueryParameters();

            var encoding = new ASCIIEncoding();
            var secretByte = encoding.GetBytes(Secret);
            using (var hmacsha256 = new HMACSHA256(secretByte))
            {
                var message = nonce + Key + action;
                if (paramsStr != null)
                {
                    message += paramsStr;
                }
                var messageByte = encoding.GetBytes(message);

                var signature = ByteToString(hmacsha256.ComputeHash(messageByte));
                return signature;
            }
        }

        private string ByteToString(byte[] buff)
        {
            return buff.Aggregate("", (current, t) => current + t.ToString("X2", CultureInfo.InvariantCulture));
        }

        private void CheckRequiredProperty(string propertyValue, string propertyName, string methodName)
        {
            if (!propertyValue.HasValue())
            {
                var error = new ArgumentNullException($"LocalBitcoins: {propertyName} has no value in {methodName}");
                error.LogError();
                throw error;
            }
            if (propertyName == "address" && !IsBitcoinAddressValid(propertyValue))
            {
                var error = new ArgumentException($"LocalBitcoins: Bitcoin address value in {methodName} is not valid.");
                error.LogError();
                throw error;
            }
        }

        private bool IsBitcoinAddressValid(string address)
        {
            var regex = new Regex("^[13][a-km-zA-HJ-NP-Z0-9]{26,33}$");
            return regex.IsMatch(address);
        }
        #endregion
    }
}
