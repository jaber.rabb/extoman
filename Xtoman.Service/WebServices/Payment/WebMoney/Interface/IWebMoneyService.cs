﻿using System.Web;

namespace Xtoman.Service.WebServices
{
    public interface IWebMoneyService
    {
        void MakePayment(MakeWebMoneyPaymentParams parameters, HttpContextBase httpContext = null);
    }
}
