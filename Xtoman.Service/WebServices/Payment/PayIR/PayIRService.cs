﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Payment.PayIR;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service.WebServices
{
    public class PayIRService : WebServiceManager, IPayIRService
    {
        private readonly string baseUrl = "https://pay.ir/pg/";
        private readonly string apiUrl = "https://pay.ir/api/v1/";
        private readonly string cashoutUrl = "https://pay.ir/api/v1.2/";

        private readonly string mobile = AppSettingManager.PayIR_PhoneNumber;
        private readonly string password = AppSettingManager.PayIR_Password;
        private readonly ICacheManager _cacheManager;
        #region Constructor
        public PayIRService(
            ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }
        #endregion

        #region Sync methods

        #endregion

        #region Async methods
        #region GateWay
        public async Task<PayIRSendResult> SendAsync(PayIRSendInput input)
        {
            var result = await PostAsync("send", input);
            new Exception($"PayIR Send Request Input: ({input.Serialize()})").LogError();
            return LogAndDeserialize<PayIRSendResult>(result);
        }

        public async Task<PayIRVerifyResult> VerifyAsync(PayIRVerifyInput input)
        {
            var result = await PostAsync("verify", input);
            new Exception($"PayIR Verify Request Input: ({input.Serialize()})").LogError();
            return LogAndDeserialize<PayIRVerifyResult>(result);
        }
        #endregion
        #region Cashout
        public async Task<PayAuthenticateResult> AuthenticateAsync()
        {
            //var tokenCacheKey = "Xtoman.PayIR.API.Token";

            //if (_cacheManager.IsSet(tokenCacheKey)) { 
            //    var tokenResult = _cacheManager.Get<string>(tokenCacheKey);
            //    return LogAndDeserialize<PayAuthenticateResult>(tokenResult);
            //}

            var input = new { mobile, password };
            var result = await PostAPIAsync("authenticate", input);
            new Exception($"PayIR API Authenticate Request Input: ({input.Serialize()})").LogError();
            var returnVal = LogAndDeserialize<PayAuthenticateResult>(result, true);
            //if (returnVal.Status == 1)
            //    _cacheManager.Set(tokenCacheKey, result, 5);
            return returnVal;
        }

        public async Task<PayCashoutShebaResult> GetShebaDetailAsync(string sheba)
        {
            sheba = sheba.Replace("-", "").Replace(" ", "");
            if (sheba.Length == 26)
                sheba = sheba.ToLower().Replace("ir", "");
            else if (sheba.Length != 24)
            {
                var ex = new Exception("شماره شبای وارد شده 24 رقم نیست");
                ex.LogError();
                throw ex;
            }
            var authenticate = await AuthenticateAsync();
            if (authenticate.Status == 1)
            {
                var url = $"cashout/inquiry?token={authenticate.Token}";
                var result = await PostCashoutAsync(url, new { sheba });
                return LogAndDeserialize<PayCashoutShebaResult> (result, true);
            }
            throw new Exception("PayIR API Authenticate Failed.");
        }

        public Task<PayCashoutInquires> CashoutInquiriesAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<PayCashoutRequestResult> CashoutRequestAsync(int rialAmount, string accountOwner, string shebaNumber, long uId)
        {
            shebaNumber = shebaNumber.Replace("-", "").Replace(" ", "");
            if (shebaNumber.Length == 26)
                shebaNumber = shebaNumber.ToLower().Replace("ir", "");
            else if (shebaNumber.Length != 24)
            {
                var ex = new Exception("شماره شبای وارد شده 24 رقم نیست");
                ex.LogError();
                throw ex;
            }
            var authenticate = await AuthenticateAsync();
            if (authenticate.Status == 1)
            {
                var url = $"cashout/request?token={authenticate.Token}";
                var result = await PostCashoutAsync(url, new
                {
                    amount = rialAmount,
                    name = accountOwner,
                    sheba = shebaNumber,
                    uid = uId
                });
                return LogAndDeserialize<PayCashoutRequestResult>(result, true);
            }
            throw new Exception("PayIR API Authenticate Failed.");
        }

        public Task<PayCashoutBaseResult> DeleteCashoutAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<PayCashoutStatusResult> CashoutStatusAsync(string id)
        {
            var authenticate = await AuthenticateAsync();
            if (authenticate.Status == 1)
            {
                var url = $"cashout/status?token={authenticate.Token}";
                var result = await PostAPIAsync(url, new { id });
                return LogAndDeserialize<PayCashoutStatusResult>(result, true);
            }
            throw new Exception("PayIR API Authenticate Failed.");
        }

        public Task<PayCashoutTrackResult> TrackCashoutAsync(string uid)
        {
            throw new NotImplementedException();
        }

        public async Task<PayCashoutTransactionsResult> TransactionsAsync(int? page = null, long? fromAmountRial = null, long? toAmountRial = null, string transactionId = null, string cardNumber = null, string factorNumber = null, DateTime? fromDate = null, DateTime? toDate = null)
        {
            var authenticate = await AuthenticateAsync();
            if (authenticate.Status == 1)
            {
                var url = $"transaction/get?token={authenticate.Token}";
                if (page.HasValue && page.Value > 1)
                    url += $"&page={page.Value}";

                var result = await PostAPIAsync(url, new
                {
                    from_amount = fromAmountRial,
                    to_amount = toAmountRial,
                    transaction_id = transactionId,
                    card_number = cardNumber,
                    factor_number = factorNumber,
                    from_date = fromDate.HasValue ? fromDate.Value.ToString("yyyy-MM-dd") : "",
                    to_date = toDate.HasValue ? toDate.Value.ToString("yyyy-MM-dd") : ""
                });
                return LogAndDeserialize<PayCashoutTransactionsResult>(result, true);
            }
            throw new Exception("PayIR API Authenticate Failed.");
        }

        public async Task<PayCashoutBalanceResult> CashoutBalanceAsync()
        {
            var authenticate = await AuthenticateAsync();
            if (authenticate.Status == 1)
            {
                var url = $"wallet/balance?token={authenticate.Token}";
                var result = await PostAPIAsync(url);
                return LogAndDeserialize<PayCashoutBalanceResult>(result);
            }
            throw new Exception("PayIR API Authenticate Failed.");
        }
        #endregion
        #endregion

        #region Helpers
        public async Task<string> PostAsync(string action, object obj = null)
        {
            var restClientRequest = new RestClientRequest(baseUrl, action);

            AddObjectToRestClientRequest(restClientRequest, obj);

            var searchOutputs = await restClientRequest.SendPostRequestAsync();
            return searchOutputs.Content;
        }

        public async Task<string> PostCashoutAsync(string action, object obj = null)
        {
            var restClientRequest = new RestClientRequest(cashoutUrl, action);

            AddObjectToRestClientRequest(restClientRequest, obj);

            var searchOutputs = await restClientRequest.SendPostRequestAsync();
            new Exception($"PayIR Cashout Transactions Result: {searchOutputs.Content}").LogError();
            return searchOutputs.Content;
        }

        public async Task<string> PostAPIAsync(string action, object obj = null)
        {
            var restClientRequest = new RestClientRequest(apiUrl, action);

            AddObjectToRestClientRequest(restClientRequest, obj);

            var searchOutputs = await restClientRequest.SendPostRequestAsync();
            new Exception($"PayIR Cashout Transactions Result: {searchOutputs.Content}").LogError();
            return searchOutputs.Content;
        }

        private void AddObjectToRestClientRequest(RestClientRequest restClientRequest, object obj = null)
        {
            if (obj != null)
            {
                var validProperties = obj.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                foreach (var property in validProperties)
                {
                    var propVal = property.GetValue(obj)?.ToString() ?? "";
                    if (propVal.HasValue())
                        restClientRequest.AddParameter(property.Name, propVal);
                }
            }
        }

        //public async Task<string> PostAsync(string action, PayIRBaseRequest input)
        //{
        //    var url = new Uri(baseUrl + action);
        //    var httpClient = new HttpClient();

        //    var body = input.GetQueryString();

        //    using (var request = new HttpRequestMessage(HttpMethod.Post, url))
        //    {
        //        request.Headers.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml,application/json");
        //        request.Headers.TryAddWithoutValidation("Accept-Encoding", "gzip, deflate");
        //        request.Headers.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
        //        //request.Headers.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

        //        request.Content = new StringContent(body, Encoding.UTF8, "application/x-www-form-urlencoded");

        //        using (var response = await httpClient.SendAsync(request).ConfigureAwait(false))
        //        {
        //            response.EnsureSuccessStatusCode();
        //            using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
        //            using (var decompressedStream = new GZipStream(responseStream, CompressionMode.Decompress))
        //            using (var streamReader = new StreamReader(decompressedStream))
        //            {
        //                var responseContent = await streamReader.ReadToEndAsync().ConfigureAwait(false);
        //                return responseContent;
        //            }
        //        }
        //    }
        //}
        #endregion
    }
}
