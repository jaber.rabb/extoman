﻿using System;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Payment.PayIR;

namespace Xtoman.Service.WebServices
{
    public interface IPayIRService
    {
        Task<PayIRSendResult> SendAsync(PayIRSendInput input);
        Task<PayIRVerifyResult> VerifyAsync(PayIRVerifyInput input);

        Task<PayAuthenticateResult> AuthenticateAsync();
        Task<PayCashoutShebaResult> GetShebaDetailAsync(string sheba);
        Task<PayCashoutInquires> CashoutInquiriesAsync();
        Task<PayCashoutRequestResult> CashoutRequestAsync(int rialAmount, string accountOwner, string shebaNumber, long uId);
        Task<PayCashoutBaseResult> DeleteCashoutAsync();
        Task<PayCashoutTrackResult> TrackCashoutAsync(string uid);
        Task<PayCashoutStatusResult> CashoutStatusAsync(string id);
        Task<PayCashoutTransactionsResult> TransactionsAsync(int? page = null, long? fromAmountRial = null, long? toAmountRial = null, string transactionId = null, string cardNumber = null, string factorNumber = null, DateTime? fromDate = null, DateTime? toDate = null);
        Task<PayCashoutBalanceResult> CashoutBalanceAsync();
    }
}
