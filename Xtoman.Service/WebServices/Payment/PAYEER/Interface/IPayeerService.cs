﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Xtoman.Domain.WebServices;

namespace Xtoman.Service.WebServices
{
    public interface IPayeerService
    {
        #region Sync methods
        /// <summary>
        /// Merchant payment gateway
        /// </summary>
        void MakePayment(MakePayeerPaymentParams parameters, HttpContextBase httpContext = null);
        /// <summary>
        /// Authorization Check -> The simplest query is checking authorization.It does not have to be fulfilled separately from primary actions.
        /// </summary>
        /// <returns>Two parameters will be returned in the body of the response: auth_error & errors</returns>
        bool CheckAuthorization();
        /// <summary>
        /// Obtain wallet balance.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>The balance array contains the balance broken down by currency with three parameters for each one: BUDGET => balance, DOSTUPNO => balance minus transactions in progress, DOSTUPNO_SYST => Balance minus transactions in progress, minus funds that could be blocked by the system (to be used as the main balance for the wallet; for example, when checking the balance before a withdrawal)</returns>
        PayeerBalanceResult CheckBalance();
        /// <summary>
        /// Transfer funds within the Payeer system.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>historyId or list of errors</returns>
        PayeerTransferResult Transfer(PayeerTransferInput input);
        KeyValuePair<bool, string> TransferWithError(PayeerTransferInput input);
        /// <summary>
        /// You can check the existence of an account number prior to transfer in the Payeer system..
        /// </summary>
        /// <param name="input"></param>
        /// <returns>If the errors array is blank, this means that the user exists.</returns>
        bool CheckAccountExist(PayeerAccountExistInput input);
        /// <summary>
        /// You can check the existence of an account number prior to transfer in the Payeer system..
        /// </summary>
        /// <param name="input"></param>
        /// <returns>If the errors array is blank, this means that the user exists.</returns>
        KeyValuePair<bool, string> CheckAccountExistWithError(PayeerAccountExistInput input);
        /// <summary>
        /// If during deposit/transfer/withdrawal the withdrawal currency curIn is different from the receiving currency curOut, the amount will be converted based on Payeer’s internal echange rates, which can be obtained using the following method.
        /// </summary>
        /// <param name="input">Output: N => get deposit rates | Y => get withdrawal rates</param>
        /// <returns></returns>
        PayeerAutomaticExchangeRateResult AutomaticExchangeRate(PayeerAutomaticExchangeRateInput input);
        /// <summary>
        /// This method allows you to check the possibility of a payout without actually creating a payout (you can get the withdrawal/reception amount or check errors in parameters)
        /// </summary>
        /// <returns></returns>
        PayeerPayoutResult CheckPayoutPossibility(PayeerPayoutInput input);
        /// <summary>
        /// A payout to any payment system that supports Payeer. You can find a list of payment systems on the corresponding tab in the API user settings or using the special (getPaySystems) method.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        PayeerPayoutResult Payout(PayeerPayoutInput input);
        /// <summary>
        /// List of available payment systems
        /// </summary>
        /// <returns></returns>
        PayeerAvailablePaymentSystemsResult AvailablePaymentSystems();
        /// <summary>
        /// You can use this method to get detailed information on a transaction ID.
        /// </summary>
        /// <param name="historyId"></param>
        /// <returns></returns>
        PayeerTransactionInformationResult TransactionInformation(string historyId);
        #endregion

        #region Async methods
        /// <summary>
        /// Authorization Check -> The simplest query is checking authorization.It does not have to be fulfilled separately from primary actions.
        /// </summary>
        /// <returns>Two parameters will be returned in the body of the response: auth_error & errors</returns>
        Task<bool> CheckAuthorizationAsync();
        /// <summary>
        /// Obtain wallet balance.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>The balance array contains the balance broken down by currency with three parameters for each one: BUDGET => balance, DOSTUPNO => balance minus transactions in progress, DOSTUPNO_SYST => Balance minus transactions in progress, minus funds that could be blocked by the system (to be used as the main balance for the wallet; for example, when checking the balance before a withdrawal)</returns>
        Task<PayeerBalanceResult> CheckBalanceAsync();
        /// <summary>
        /// Transfer funds within the Payeer system.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>historyId or list of errors</returns>
        Task<PayeerTransferResult> TransferAsync(PayeerTransferInput input);
        Task<KeyValuePair<bool, string>> TransferWithErrorAsync(PayeerTransferInput input);
        /// <summary>
        /// You can check the existence of an account number prior to transfer in the Payeer system..
        /// </summary>
        /// <param name="input"></param>
        /// <returns>If the errors array is blank, this means that the user exists.</returns>
        Task<bool> CheckAccountExistAsync(PayeerAccountExistInput input);
        /// <summary>
        /// You can check the existence of an account number prior to transfer in the Payeer system..
        /// </summary>
        /// <param name="input"></param>
        /// <returns>If the errors array is blank, this means that the user exists.</returns>
        Task<KeyValuePair<bool, string>> CheckAccountExistWithErrorAsync(PayeerAccountExistInput input);
        /// <summary>
        /// If during deposit/transfer/withdrawal the withdrawal currency curIn is different from the receiving currency curOut, the amount will be converted based on Payeer’s internal echange rates, which can be obtained using the following method.
        /// </summary>
        /// <param name="input">Output: N => get deposit rates | Y => get withdrawal rates</param>
        /// <returns></returns>
        Task<PayeerAutomaticExchangeRateResult> AutomaticExchangeRateAsync(PayeerAutomaticExchangeRateInput input);
        /// <summary>
        /// This method allows you to check the possibility of a payout without actually creating a payout (you can get the withdrawal/reception amount or check errors in parameters)
        /// </summary>
        /// <returns></returns>
        Task<PayeerPayoutResult> CheckPayoutPossibilityAsync(PayeerPayoutInput input);
        /// <summary>
        /// A payout to any payment system that supports Payeer. You can find a list of payment systems on the corresponding tab in the API user settings or using the special (getPaySystems) method.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PayeerPayoutResult> PayoutAsync(PayeerPayoutInput input);
        /// <summary>
        /// List of available payment systems
        /// </summary>
        /// <returns></returns>
        Task<PayeerAvailablePaymentSystemsResult> AvailablePaymentSystemsAsync();
        /// <summary>
        /// You can use this method to get detailed information on a transaction ID.
        /// </summary>
        /// <param name="historyId"></param>
        /// <returns></returns>
        Task<PayeerTransactionInformationResult> TransactionInformationAsync(string historyId);
        #endregion
        
        #region Not implemented yet methods!
        //Task StoreTransactionInformationAsync();
        //Task TransactionsHistoryAsync();
        //Task MerchantApi();
        #endregion

    }
}
