﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Service.WebServices
{
    public class MakePayeerPaymentParams
    {
        public string m_shop { get; set; }
        public string m_orderid { get; set; }
        public decimal m_amount { get; set; }
        public string m_curr { get; set; }
        public string m_desc { get; set; }
        public string m_sign { get; set; }
        public string lang { get; set; }
    }
}
