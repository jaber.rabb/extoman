﻿using RestSharp;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Xtoman.Domain.WebServices;
using System;
using System.Collections.Generic;
using Xtoman.Utility;
using Xtoman.Domain.Settings;
using Newtonsoft.Json;

namespace Xtoman.Service.WebServices
{
    public class PayeerService : PaymentService, IPayeerService
    {
        private const string baseUrl = "https://payeer.com";
        public string ApiId { get; private set; }
        public string AccountId { get; private set; }
        public string ApiSecretKey { get; private set; }
        private const string Language = "en";
        private readonly string merchantUrl = $"{baseUrl}/merchant";
        private readonly string apiUrl = $"{baseUrl}/ajax/api/";
        private readonly PaymentSettings _paymentSettings;

        public PayeerService(PaymentSettings paymentSettings)
        {
            _paymentSettings = paymentSettings;
            ApiId = AppSettingManager.PAYEER_ApiId;
            AccountId = AppSettingManager.PAYEER_AccountId;
            ApiSecretKey = AppSettingManager.PAYEER_ApiSecretKey;
        }

        #region Sync Methods
        public void MakePayment(MakePayeerPaymentParams parameters, HttpContextBase httpContext = null)
        {
            MakePayment(parameters, merchantUrl, "payeerForm", "GET", "m_process", httpContext);
        }

        public PayeerAutomaticExchangeRateResult AutomaticExchangeRate(PayeerAutomaticExchangeRateInput input)
        {
            var response = PostRequest("getExchangeRate", input);
            return LogAndDeserialize<PayeerAutomaticExchangeRateResult>(response);
        }

        public PayeerAvailablePaymentSystemsResult AvailablePaymentSystems()
        {
            var response = PostRequest("getPaySystems");
            return LogAndDeserialize<PayeerAvailablePaymentSystemsResult>(response);
        }

        public bool CheckAccountExist(PayeerAccountExistInput input)
        {
            var response = PostRequest("checkUser", input);
            var result = LogAndDeserialize<PayeerBaseResult>(response);
            return CheckError(result);
        }
        public KeyValuePair<bool, string> CheckAccountExistWithError(PayeerAccountExistInput input)
        {
            var response = PostRequest("checkUser", input);
            var result = LogAndDeserialize<PayeerBaseResult>(response);
            var text = GetErrorText(result.errors);
            var isSuccess = CheckError(result);
            return new KeyValuePair<bool, string>(isSuccess, text);
        }

        public bool CheckAuthorization()
        {
            var response = PostRequest("");
            var result = LogAndDeserialize<PayeerBaseResult>(response);
            return CheckError(result);
        }

        public PayeerBalanceResult CheckBalance()
        {
            var response = PostRequest("balance");
            return LogAndDeserialize<PayeerBalanceResult>(response);
        }

        public PayeerPayoutResult CheckPayoutPossibility(PayeerPayoutInput input)
        {
            var response = PostRequest("initOutput", input);
            return LogAndDeserialize<PayeerPayoutResult>(response);
        }
        public PayeerPayoutResult Payout(PayeerPayoutInput input)
        {
            var response = PostRequest("output", input);
            return LogAndDeserialize<PayeerPayoutResult>(response);
        }

        public PayeerTransactionInformationResult TransactionInformation(string historyId)
        {
            var response = PostRequest("historyInfo", historyId);
            return LogAndDeserialize<PayeerTransactionInformationResult>(response);
        }

        public PayeerTransferResult Transfer(PayeerTransferInput input)
        {
            var response = PostRequest("transfer", input);
            return LogAndDeserialize<PayeerTransferResult>(response);
        }

        public KeyValuePair<bool, string> TransferWithError(PayeerTransferInput input)
        {
            var response = PostRequest("transfer", input);
            var result = LogAndDeserialize<PayeerTransferResult>(response);
            return new KeyValuePair<bool, string>(CheckError(result), GetTransferErrorText(result.errors));
        }
        #endregion

        #region Async Methods
        public async Task<PayeerAutomaticExchangeRateResult> AutomaticExchangeRateAsync(PayeerAutomaticExchangeRateInput input)
        {
            var response = await PostRequestAsync("getExchangeRate", input);
            return LogAndDeserialize<PayeerAutomaticExchangeRateResult>(response);
        }

        public async Task<PayeerAvailablePaymentSystemsResult> AvailablePaymentSystemsAsync()
        {
            var response = await PostRequestAsync("getPaySystems");
            return LogAndDeserialize<PayeerAvailablePaymentSystemsResult>(response);
        }

        public async Task<bool> CheckAccountExistAsync(PayeerAccountExistInput input)
        {
            var response = await PostRequestAsync("checkUser", input);
            var result = LogAndDeserialize<PayeerBaseResult>(response);
            return CheckError(result);
        }
        public async Task<KeyValuePair<bool, string>> CheckAccountExistWithErrorAsync(PayeerAccountExistInput input)
        {
            var response = await PostRequestAsync("checkUser", input);
            var result = LogAndDeserialize<PayeerBaseResult>(response);
            var text = GetErrorText(result.errors);
            var isSuccess = CheckError(result);
            return new KeyValuePair<bool, string>(isSuccess, text);
        }

        public async Task<bool> CheckAuthorizationAsync()
        {
            var response = await PostRequestAsync("");
            var result = LogAndDeserialize<PayeerBaseResult>(response);
            return CheckError(result);
        }

        public async Task<PayeerBalanceResult> CheckBalanceAsync()
        {
            var response = await PostRequestAsync("balance");
            return LogAndDeserialize<PayeerBalanceResult>(response);
        }

        public async Task<PayeerPayoutResult> CheckPayoutPossibilityAsync(PayeerPayoutInput input)
        {
            var response = await PostRequestAsync("initOutput", input);
            return LogAndDeserialize<PayeerPayoutResult>(response);
        }
        public async Task<PayeerPayoutResult> PayoutAsync(PayeerPayoutInput input)
        {
            var response = await PostRequestAsync("output", input);
            return LogAndDeserialize<PayeerPayoutResult>(response);
        }

        public async Task<PayeerTransactionInformationResult> TransactionInformationAsync(string historyId)
        {
            var response = await PostRequestAsync("historyInfo", historyId);
            return LogAndDeserialize<PayeerTransactionInformationResult>(response);
        }

        public async Task<PayeerTransferResult> TransferAsync(PayeerTransferInput input)
        {
            var response = await PostRequestAsync("transfer", input);
            return LogAndDeserialize<PayeerTransferResult>(response);
        }

        public async Task<KeyValuePair<bool, string>> TransferWithErrorAsync(PayeerTransferInput input)
        {
            var response = await PostRequestAsync("transfer", input);
            var result = LogAndDeserialize<PayeerTransferResult>(response);
            return new KeyValuePair<bool, string>(CheckError(result), GetTransferErrorText(result.errors));
        }
        #endregion

        #region Helpers
        private string PostRequest(string action, object obj = null)
        {
            var urlAction = string.IsNullOrWhiteSpace(action) ? "" : "?" + action;
            var restClientRequest = new RestClientRequest(apiUrl, "api.php" + urlAction);
            PrepareRestClient(restClientRequest);
            restClientRequest.AddParameter("action", action);
            if (obj != null)
            {
                var validProperties = obj.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                foreach (var property in validProperties)
                    restClientRequest.AddParameter(property.Name, property.GetValue(obj)?.ToString() ?? "");
            }
            var searchOutputs = restClientRequest.SendPostRequest();
            return searchOutputs.Content;
        }
        private async Task<string> PostRequestAsync(string action, object obj = null)
        {
            var urlAction = string.IsNullOrWhiteSpace(action) ? "" : "?" + action;
            var restClientRequest = new RestClientRequest(apiUrl, "api.php" + urlAction);
            PrepareRestClient(restClientRequest);
            restClientRequest.AddParameter("action", action);
            if (obj != null)
            {
                var validProperties = obj.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                foreach (var property in validProperties)
                    restClientRequest.AddParameter(property.Name, property.GetValue(obj)?.ToString() ?? "");
            }
            var searchOutputs = await restClientRequest.SendPostRequestAsync();
            return searchOutputs.Content;
        }
        private RestClientRequest PrepareRestClient(RestClientRequest restClientRequest)
        {
            restClientRequest.AddParameter("account", AccountId);
            restClientRequest.AddParameter("apiId", ApiId);
            restClientRequest.AddParameter("apiPass", ApiSecretKey);
            restClientRequest.AddParameter("language", Language);
            return restClientRequest;
        }
        private bool CheckError(PayeerBaseResult result)
        {
            return Convert.ToInt16(result.auth_error) == 0;
        }
        private string GetErrorText(object errors)
        {
            if (errors == null || errors is bool)
                return "";

            var errorsList = JsonConvert.DeserializeObject<List<string>>(errors.ToString());
            return string.Join(", ", errorsList);
        }
        public string GetTransferErrorText(object errors)
        {
            var result = "";
            if (errors == null || errors is bool)
                return result;

            var errorsList = JsonConvert.DeserializeObject<List<string>>(errors.ToString());
            foreach (var item in errorsList)
            {
                var str = item.Replace(" ", "_");
                result += str.ToEnum(PayeerTransferError.noError).ToDisplay() + ". ";
            }
            return result;
        }
        #endregion
    }
}
