﻿using System.Threading.Tasks;
using Xtoman.Blockchain.API.Client;
using Xtoman.Blockchain.API.Wallet;

namespace Xtoman.Service.WebServices
{
    public class BlockchainService : IBlockchainService
    {
        private readonly string apiCode = "";
        private BlockchainHttpClient Client { get; set; }
        public BlockchainService()
        {
            Client = new BlockchainHttpClient(apiCode, "http://localhost:35235");
        }

        public async Task CreateWalletAsync(string password)
        {
            var blockChainWallet = new WalletCreator(Client);
            var response = await blockChainWallet.CreateAsync(password);
        }
    }
}
