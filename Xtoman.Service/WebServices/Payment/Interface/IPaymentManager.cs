﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices;

namespace Xtoman.Service.WebServices
{
    public interface IPaymentManager
    {
        void MakePayment(decimal amount, ECurrencyType ECAccountType, string orderId, string description, HttpContextBase httpContext = null);
        void MakePayment(decimal amount, ECurrencyType ECAccountType, string orderId, string description, string statusUrl, string successUrl, string noPaymentUrl, HttpContextBase httpContext = null);
        Task<bool> InsertPerfectMoneyStatusAsync(PerfectMoneyPaymentStatus status);
        Task<bool> InsertPAYEERPaymentStatusAsync(PAYEERPaymentStatus status);
        bool IsPAYEERPaymentValid(PAYEERPaymentStatus status, HttpContextBase httpContext = null);
        Task<bool> IsPAYEERPaymentValidAsync(PAYEERPaymentStatus status, HttpContextBase httpContext = null);
        Task<bool> IsPerfectMoneyPaymentValidAsync(PerfectMoneyPaymentStatus status, HttpContextBase httpContext = null);
        Task<List<PerfectMoneyBalanceItem>> PerfectMoneyBalanceAsync();
        Task<PayeerBalanceItemModel> PAYEERBalanceAsync();
        Task<AutoPayResult> AutoPayExchangeAsync(long orderId);
        Task<AutoPayResult> ManualPayExchangeAsync(long orderId, string transactionCode);
        //Task<BalanceResult> AllBalancesAsync();

        //New methods
        CoinPaymentFullAddressResult GetCoinPaymentAddress(ECurrencyType eCurrencyAccountType, string callbackUrl = "");
        CoinPaymentFullAddressResult GetCoinToFiatPaymentAddress(ECurrencyType paymentType, Guid orderGuid, decimal payAmount, string callbackUrl);
        Task<CoinPaymentAddressResult> GetCoinPaymentAddressAsync(ECurrencyType eCurrencyAccountType, bool isERC20, string callbackUrl = "");
        //Task<CoinPaymentAddressResult> GetCoinToFiatPaymentAddressAsync(ECurrencyType eCurrencyAccountType, string callbackUrl = "");
        Task<CoinPaymentFullAddressResult> GetCoinToFiatPaymentAddressAsync(ECurrencyType eCurrencyAccountType, Guid orderGuid, decimal payAmount, string callbackUrl = "");
        Task WithdrawFiatUSDAsync(long orderId, ECurrencyType type, decimal receiveAmount, string receiveAddress);
        Task<(bool status, double amount, string batchNumber)> PaymentPerfectVoucherPaymentAsync(string voucherNumber, string voucherActivation);
    }
}
