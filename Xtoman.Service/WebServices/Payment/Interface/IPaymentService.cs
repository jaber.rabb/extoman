﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Xtoman.Service.WebServices
{
    public interface IPaymentService
    {
        void MakePayment(object parameters, string merchantUrl, string formName, string formMethod, string submitName, HttpContextBase httpContext = null);
    }
}
