﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Payment.Finnotech;
using Xtoman.Domain.WebServices.Payment.Finnotech.V2;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class FinnotechOldService : WebServiceManager, IFinnotechOldService
    {
        private readonly string url = /*AppSettingManager.IsLocale ? "https://sandbox.finnotech.ir" : */"https://api.finnotech.ir";
        private readonly string clientId = /*AppSettingManager.IsLocale ? "extoman" : */AppSettingManager.Finnotech_ClientId;
        private readonly string secret = /*AppSettingManager.IsLocale ? "c81b02e40f3504fc507f" : */AppSettingManager.Finnotech_Secret;
        private readonly string urlV2 = "https://apibeta.finnotech.ir/";
        private readonly string clientId2 = AppSettingManager.Finnotech_ClientId2;
        private readonly string secret2 = AppSettingManager.Finnotech_Secret2;
        private readonly string melliCode = /*AppSettingManager.IsLocale ? "1270420224" : */AppSettingManager.Finnotech_NationalCode;
        private readonly string shomareHesab = /*AppSettingManager.IsLocale ? "1111111111001" : */AppSettingManager.Finnotech_AccountNumber;
        private readonly IFinnotechResponseService _finnotechResponseService;
        public FinnotechOldService(IFinnotechResponseService finnotechResponseService)
        {
            _finnotechResponseService = finnotechResponseService;
        }

        #region Async methods

        #region Version 1
        public async Task<FinnotechAuthentication> BasicAuthorizeAsync(bool isDeposit = false)
        {
            var authString = GetAuthenticationString(clientId, secret);
            var bodyData = new FinnotechAuth() { nid = melliCode, deposit = isDeposit ? shomareHesab : null };

            var restClientRequest = new RestClientRequest(url, "dev/v1/oauth2/token");
            restClientRequest.AddHeader("Authorization", "Basic " + authString);
            restClientRequest.AddJsonBody(bodyData);

            restClientRequest.Method = Method.POST;
            var response = await restClientRequest.Client.ExecuteTaskAsync(restClientRequest);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                new Exception($"StatusCode: {response.StatusCode.ToInt()} {response.StatusCode} - Content: {response.Content}").LogError();
            }

            response = restClientRequest.FixString(response);
            var result = LogAndDeserialize<FinnotechAuthentication>(response.Content, true);
            var utcNow = DateTime.UtcNow;
            var endDate = result.AccessToken.CreationDate.FaStringToDateTime("yyyyMMddHHmmss").AddMilliseconds(result.AccessToken.LifeTime);
            if (endDate <= utcNow)
                result = await RefreshTokenAsync(result.AccessToken.RefreshToken);

            result.StatusCode = response.StatusCode.ToInt();
            return result;
        }

        public async Task<FinnotechAuthentication> RefreshTokenAsync(string refreshToken)
        {
            var authString = GetAuthenticationString(clientId, secret);
            var bodyData = new FinnotechRefreshToken() { refresh_token = refreshToken };

            var restClientRequest = new RestClientRequest(url, "dev/v1/oauth2/token");
            restClientRequest.AddHeader("Authorization", "Basic " + authString);
            restClientRequest.AddJsonBody(bodyData);

            restClientRequest.Method = Method.POST;
            var response = await restClientRequest.Client.ExecuteTaskAsync(restClientRequest);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                new Exception($"StatusCode: {response.StatusCode.ToInt()} {response.StatusCode} - Content: {response.Content}").LogError();
            }

            response = restClientRequest.FixString(response);
            var result = LogAndDeserialize<FinnotechAuthentication>(response.Content, true);
            result.StatusCode = response.StatusCode.ToInt();
            return result;
        }

        public async Task<FinnotechCardInformation> GetCardInformationAsync(string cardNumber, int? userId = null)
        {
            var trackId = Guid.NewGuid();
            cardNumber = cardNumber.Trim();
            var reqUrl = $"{url}/mpg/v1/clients/{clientId}/cards/{cardNumber}?trackId={trackId}";
            return await CallAndDeserializeAsync<FinnotechCardInformation>(reqUrl);
        }

        public async Task<FinnotechIdVerification> GetIdVerificationAsync(string nid, string birthDate, string fullName, string firstName, string lastName, string fatherName, int? userId = null)
        {
            var trackId = Guid.NewGuid();
            var reqUrl = $"{url}/oak/v1/clients/{clientId}/nidVerification?trackId={trackId}&nid={nid}&birthDate={birthDate}&fullName={fullName}&firstName={firstName}&lastName={lastName}&fatherName={fatherName}";
            return await CallAndDeserializeAsync<FinnotechIdVerification>(reqUrl, userId, true);
        }

        public async Task<FinnotechIdDetail> GetIdDetailAsync(string nid, string birthDate, int? userId = null)
        {
            var trackId = Guid.NewGuid();
            var reqUrl = $"{url}/oak/v1/clients/{clientId}/nidInquiry?trackId={trackId}&nid={nid}&birthDate={birthDate}";
            return await CallAndDeserializeAsync<FinnotechIdDetail>(reqUrl, userId);
        }

        public async Task<FinnotechBalance> GetBalanceAsync()
        {
            var trackId = Guid.NewGuid();
            var reqUrl = $"{url}/oak/v1/{melliCode}/deposits/{shomareHesab}/balance?trackId={trackId}";
            return await CallAndDeserializeAsync<FinnotechBalance>(reqUrl, null, true);
        }

        public async Task<FinnotechTransferTo> TransferToAsync(long amount, string destinationNumber, string destinationFirstname, string destinationLastname, string paymentNumber, string description, int? userId = null)
        {
            var trackId = Guid.NewGuid();
            var reqUrl = $"{url}/oak/v1/{melliCode}/clients/{clientId}/transferTo?trackId={trackId}";
            return await CallAndDeserializeAsync<FinnotechTransferTo>(reqUrl, userId, true, Method.POST, new { amount, destinationNumber, destinationFirstname, destinationLastname, paymentNumber, description });
        }

        public async Task<FinnotechPayaReport> PayaReportAsync(string fromDate, string toDate, int offset, int length, int? userId = null)
        {
            var trackId = Guid.NewGuid();
            var reqUrl = $"{url}/oak/v1/{melliCode}/deposit/{shomareHesab}/payas?trackId={trackId}&fromDate={fromDate}&toDate={toDate}&offset={offset}&length={length}";
            return await CallAndDeserializeAsync<FinnotechPayaReport>(reqUrl, userId, true);
        }
        #endregion

        #region Version 2
        public async Task<FinnotechResponseV2<FinnotechAuthenticationV2>> BasicAuthorizeV2Async()
        {
            var authString = GetAuthenticationString(clientId, secret);

            var scopes = "";
            foreach (var enumItem in (FinnotechClientCredentialScope[])Enum.GetValues(typeof(FinnotechClientCredentialScope)))
                scopes += enumItem.ToDisplay() + ",";
            scopes.TrimEnd(',');

            var bodyData = new FinnotechBasicAuthV2() { nid = melliCode, scopes = scopes };

            var restClientRequest = new RestClientRequest(url, "dev/v1/oauth2/token");
            restClientRequest.AddHeader("Authorization", "Basic " + authString);
            restClientRequest.AddJsonBody(bodyData);

            restClientRequest.Method = Method.POST;
            var response = await restClientRequest.Client.ExecuteTaskAsync(restClientRequest);
            if (response.StatusCode != HttpStatusCode.OK)
                new Exception($"Finnotech Basic Authorize Error StatusCode: {response.StatusCode.ToInt()} {response.StatusCode} - Content: {response.Content}").LogError();

            response = restClientRequest.FixString(response);
            var result = LogAndDeserialize<FinnotechResponseV2<FinnotechAuthenticationV2>>(response.Content, true);

            if (result != null)
            {
                var now = DateTime.UtcNow.AddHours(-1);
                var endDate = result.Result.CreationDate.FaStringToDateTime("yyyyMMddHHmmss").AddMilliseconds(result.Result.LifeTime).ToUniversalTime();
                if (endDate <= now)
                    result = await RefreshBasicAuthorizeTokenV2Async(result.Result.RefreshToken);

                result.StatusCode = response.StatusCode.ToInt();
            }
            return result;
        }

        public async Task<FinnotechResponseV2<FinnotechAuthenticationV2>> RefreshBasicAuthorizeTokenV2Async(string refreshToken)
        {
            var authString = GetAuthenticationString(clientId, secret);
            var bodyData = new FinnotechBasicAuthRefreshTokenV2() { refresh_token = refreshToken };

            var restClientRequest = new RestClientRequest(url, "dev/v1/oauth2/token");
            restClientRequest.AddHeader("Authorization", "Basic " + authString);
            restClientRequest.AddJsonBody(bodyData);

            restClientRequest.Method = Method.POST;
            var response = await restClientRequest.Client.ExecuteTaskAsync(restClientRequest);
            if (response.StatusCode != HttpStatusCode.OK)
                new Exception($"Finnotech Basic Auth Refresh Error StatusCode: {response.StatusCode.ToInt()} {response.StatusCode} - Content: {response.Content}").LogError();

            response = restClientRequest.FixString(response);
            var result = LogAndDeserialize<FinnotechResponseV2<FinnotechAuthenticationV2>>(response.Content, true);

            return result;
        }

        public async Task<FinnotechResponseV2<FinnotechCardInformationV2>> GetCardInformationV2Async(string cardNumber, int? userId = null)
        {
            var trackId = Guid.NewGuid();
            var reqUrl = $"{urlV2}/mpg/v2/clients/{clientId}/cards/{cardNumber}?trackId={trackId}";
            return await CallAndDeserializeV2Async<FinnotechCardInformationV2>(reqUrl);
        }

        public async Task<FinnotechResponseV2<FinnotechIdVerificationV2>> GetIdVerificationV2Async(string nid, string birthDate, string fullName, string firstName, string lastName, string fatherName, int? userId = null)
        {
            var trackId = Guid.NewGuid();
            var reqUrl = $"{urlV2}/oak/v2/clients/{clientId}/nidVerification?trackId={trackId}&nationalCode={nid}&birthDate={birthDate}&fullName={fullName}&firstName={firstName}&lastName={lastName}&fatherName={fatherName}";
            return await CallAndDeserializeV2Async<FinnotechIdVerificationV2>(reqUrl, userId, true);
        }

        public async Task<FinnotechResponseV2<FinnotechMobileVerifyV2>> VerifyMobileWithIdCardNumberV2Async(string mobile, string nationalCode, int? userId = null)
        {
            var trackId = Guid.NewGuid();
            var reqUrl = $"{urlV2}/mpg/v2/clients/{clientId}/shahkar/verify?trackId={trackId}&mobile={mobile}&nationalCode={nationalCode}";
            return await CallAndDeserializeV2Async<FinnotechMobileVerifyV2>(reqUrl, userId, false);
        }

        #endregion

        #endregion

        #region Helpers
        private async Task<T> CallAndDeserializeAsync<T>(string reqUrl, int? userId = null, bool isDeposit = false, Method method = Method.GET, object bodyObject = null) where T : FinnotechBaseResponse, new()
        {
            var basicAuthorize = await BasicAuthorizeAsync(isDeposit);

            var restClientRequest = new RestClientRequest(url, reqUrl);
            restClientRequest.AddHeader("Authorization", "Bearer " + basicAuthorize.AccessToken.Value);
            restClientRequest.Method = method;
            if (bodyObject != null)
                restClientRequest.AddJsonBody(bodyObject);

            var response = await restClientRequest.Client.ExecuteTaskAsync(restClientRequest);
            if (response.StatusCode != HttpStatusCode.OK)
                new Exception($"StatusCode: {response.StatusCode.ToInt()} {response.StatusCode} - Content: {response.Content}").LogError();

            response = restClientRequest.FixString(response);
            var result = LogAndDeserialize<T>(response.Content);
            result.StatusCode = response.StatusCode.ToInt();
            var finnotechResponse = GetFinnotechResponse(result, userId);
            if (finnotechResponse != null)
            {
                await _finnotechResponseService.InsertAsync(finnotechResponse);
                result.HistoryId = finnotechResponse.Id;
            }
            return result;
        }

        private Finnotech GetFinnotechResponse<T>(T result, int? userId = null) where T : FinnotechBaseResponse, new()
        {
            var typeName = typeof(T).Name;
            switch (typeName)
            {
                case "FinnotechCardInformation":
                    var finnotechCardInfo = result.CastToClass<FinnotechCardInformation>();
                    return new FinnotechResponseCardInfo()
                    {
                        ForUserId = userId,
                        Code = finnotechCardInfo.Code,
                        Description = finnotechCardInfo.Description,
                        TrackId = finnotechCardInfo.TrackId.ToString(),
                        DestCard = finnotechCardInfo.DestCard,
                        DoTime = finnotechCardInfo.DoTime,
                        FarsiMessage = finnotechCardInfo.FarsiMessage,
                        Message = finnotechCardInfo.Message,
                        Name = finnotechCardInfo.Name,
                        Result = finnotechCardInfo.Result,
                        StatusCode = finnotechCardInfo.StatusCode
                    };
                case "FinnotechIdVerification":
                    var finnotechIdVerification = result.CastToClass<FinnotechIdVerification>();
                    return new FinnotechResponseIdVerification()
                    {
                        ForUserId = userId,
                        BirthDate = finnotechIdVerification.BirthDate,
                        Code = finnotechIdVerification.Code,
                        DeathStatus = finnotechIdVerification.DeathStatus,
                        FarsiMessage = finnotechIdVerification.FarsiMessage,
                        FatherName = finnotechIdVerification.FatherName,
                        FatherNameSimilarity = finnotechIdVerification.FatherNameSimilarity,
                        FirstName = finnotechIdVerification.FirstName,
                        FirstNameSimilarity = finnotechIdVerification.FirstNameSimilarity,
                        FullName = finnotechIdVerification.FullName,
                        FullNameSimilarity = finnotechIdVerification.FullNameSimilarity,
                        LastName = finnotechIdVerification.LastName,
                        LastNameSimilarity = finnotechIdVerification.LastNameSimilarity,
                        Message = finnotechIdVerification.Message,
                        NationalId = finnotechIdVerification.NationalId,
                        Status = finnotechIdVerification.Status,
                        StatusCode = finnotechIdVerification.StatusCode,
                        TrackId = finnotechIdVerification.TrackId
                    };
                case "FinnotechBalance":
                    var finnotechBalance = result.CastToClass<FinnotechBalance>();
                    return new FinnotechResponseBalance()
                    {
                        ForUserId = userId,
                        TrackId = finnotechBalance.TrackId,
                        StatusCode = finnotechBalance.StatusCode,
                        Message = finnotechBalance.Message,
                        AvailableBalance = finnotechBalance.AvailableBalance,
                        Code = finnotechBalance.Code,
                        CurrentBalance = finnotechBalance.CurrentBalance,
                        EffectiveBalance = finnotechBalance.EffectiveBalance,
                        FarsiMessage = finnotechBalance.FarsiMessage,
                        Number = finnotechBalance.Number,
                        State = finnotechBalance.State.HasValue() ? finnotechBalance.State.Equals("DONE", StringComparison.InvariantCultureIgnoreCase) ? FinnotechState.DONE : FinnotechState.FAILED : FinnotechState.FAILED
                    };
                case "FinnotechTransferTo":
                    var finnotechTransferTo = result.CastToClass<FinnotechTransferTo>();
                    return new FinnotechResponseTransferTo()
                    {
                        ForUserId = userId,
                        Amount = finnotechTransferTo.Amount,
                        State = finnotechTransferTo.State,
                        FarsiMessage = finnotechTransferTo.FarsiMessage,
                        Code = finnotechTransferTo.Code,
                        Message = finnotechTransferTo.Message,
                        StatusCode = finnotechTransferTo.StatusCode,
                        Description = finnotechTransferTo.Description,
                        DestinationFirstname = finnotechTransferTo.DestinationFirstname,
                        DestinationLastname = finnotechTransferTo.DestinationLastname,
                        DestinationNumber = finnotechTransferTo.DestinationNumber,
                        InquiryDate = finnotechTransferTo.InquiryDate,
                        InquirySequence = finnotechTransferTo.InquirySequence,
                        InquiryTime = finnotechTransferTo.InquiryTime,
                        PaymentNumber = finnotechTransferTo.PaymentNumber,
                        RefCode = finnotechTransferTo.RefCode,
                        SourceFirstname = finnotechTransferTo.SourceFirstname,
                        SourceLastname = finnotechTransferTo.SourceLastname,
                        SourceNumber = finnotechTransferTo.SourceNumber,
                        TrackId = finnotechTransferTo.TrackId,
                        TypeEnum = finnotechTransferTo.TypeEnum
                    };
                case "FinnotechPayaReport":
                    var finnotechPayaReport = result.CastToClass<FinnotechPayaReport>();
                    var fpr = new FinnotechResponsePayaReport()
                    {
                        ForUserId = userId,
                        Code = finnotechPayaReport.Code,
                        Message = finnotechPayaReport.Message,
                        StatusCode = finnotechPayaReport.StatusCode,
                        State = finnotechPayaReport.State,
                        FarsiMessage = finnotechPayaReport.FarsiMessage,
                        Count = finnotechPayaReport.Count,
                        Transactions = new List<FinnotechResponsePayaTransaction>()
                    };
                    foreach (var item in finnotechPayaReport.Transactions)
                    {
                        fpr.Transactions.Add(new FinnotechResponsePayaTransaction()
                        {
                            AdDate = item.AdDate,
                            Amount = item.Amount,
                            Comment = item.Comment,
                            CycleTime = item.CycleTime,
                            PayaCycle = item.PayaCycle,
                            PayId = item.PayId,
                            ReceiverIban = item.ReceiverIban,
                            ReceiverName = item.ReceiverName,
                            Rejected = item.Rejected,
                            ReturnCausality = item.ReturnCausality,
                            ReturnId = item.ReturnId,
                            SenderBank = item.SenderBank,
                            SenderIban = item.SenderIban,
                            SenderName = item.SenderName,
                            TraceId = item.TraceId,
                            TransactionId = item.TransactionId,
                            TransactionDate = item.TransactionDate,
                            Finnotech = fpr
                        });
                    }
                    return fpr;
            }
            return null;
        }

        private string GetAuthenticationString(string clientId, string clientSecret)
        {
            var toEncode = $"{clientId}:{clientSecret}";
            var plainTextBytes = Encoding.UTF8.GetBytes(toEncode);
            return Convert.ToBase64String(plainTextBytes);
        }
        #endregion

        #region Version 2 Helpers
        private async Task<FinnotechResponseV2<T>> CallAndDeserializeV2Async<T>(string reqUrl, int? userId = null, bool isDeposit = false, Method method = Method.GET, object bodyObject = null)
        {
            var basicAuthorize = await BasicAuthorizeAsync(isDeposit);

            var restClientRequest = new RestClientRequest(url, reqUrl);
            restClientRequest.AddHeader("Authorization", "Bearer " + basicAuthorize.AccessToken.Value);
            restClientRequest.Method = method;
            if (bodyObject != null)
                restClientRequest.AddJsonBody(bodyObject);

            var response = await restClientRequest.Client.ExecuteTaskAsync(restClientRequest);
            if (response.StatusCode != HttpStatusCode.OK)
                new Exception($"StatusCode: {response.StatusCode.ToInt()} {response.StatusCode} - Content: {response.Content}").LogError();

            response = restClientRequest.FixString(response);
            var result = LogAndDeserialize<FinnotechResponseV2<T>>(response.Content);
            result.StatusCode = response.StatusCode.ToInt();

            var finnotechResponse = GetFinnotechResponseV2(result, typeof(T).Name, userId);
            if (finnotechResponse != null)
            {
                await _finnotechResponseService.InsertAsync(finnotechResponse);
                result.HistoryId = finnotechResponse.Id;
            }
            return result;
        }

        private Finnotech GetFinnotechResponseV2(object result, string typeName, int? userId = null)
        {
            switch (typeName)
            {
                case "FinnotechMobileVerify":
                    var finnotechMobileVerify = result.CastToClass<FinnotechResponseV2<FinnotechMobileVerifyV2>>();
                    return new FinnotechResponseMobileVerify()
                    {
                        IsValid = finnotechMobileVerify.Result.IsValid
                    };
                case "FinnotechCardInformation":
                    var finnotechCardInfo = result.CastToClass<FinnotechResponseV2<FinnotechCardInformationV2>>();
                    return new FinnotechResponseCardInfo()
                    {
                        ForUserId = userId,
                        Description = finnotechCardInfo.Result.Description,
                        TrackId = finnotechCardInfo.TrackId.ToString(),
                        DestCard = finnotechCardInfo.Result.DestCard,
                        DoTime = finnotechCardInfo.Result.DoTime,
                        Name = finnotechCardInfo.Result.Name,
                        Result = finnotechCardInfo.Result.ResultText + " - " + finnotechCardInfo.Result.ResultText,
                        StatusCode = finnotechCardInfo.StatusCode
                    };
                case "FinnotechIdVerification":
                    var finnotechIdVerification = result.CastToClass< FinnotechResponseV2<FinnotechIdVerificationV2>>();
                    return new FinnotechResponseIdVerification()
                    {
                        ForUserId = userId,
                        BirthDate = finnotechIdVerification.Result.BirthDate,
                        DeathStatus = finnotechIdVerification.Result.DeathStatus,
                        FatherName = finnotechIdVerification.Result.FatherName,
                        FatherNameSimilarity = finnotechIdVerification.Result.FatherNameSimilarity,
                        FirstName = finnotechIdVerification.Result.FirstName,
                        FirstNameSimilarity = finnotechIdVerification.Result.FirstNameSimilarity,
                        FullName = finnotechIdVerification.Result.FullName,
                        FullNameSimilarity = finnotechIdVerification.Result.FullNameSimilarity,
                        LastName = finnotechIdVerification.Result.LastName,
                        LastNameSimilarity = finnotechIdVerification.Result.LastNameSimilarity,
                        NationalId = finnotechIdVerification.Result.NationalCode,
                        StatusCode = finnotechIdVerification.StatusCode,
                        TrackId = finnotechIdVerification.TrackId
                    };
            }
            return null;
        }

        public Task<FinnotechResponseV2<FinnotechBalanceV2>> BalanceV2Async()
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    internal class FinnotechAuth
    {
        public FinnotechAuth()
        {
            grant_type = "client_credentials";
        }
        /// <summary>
        ///  این مقدار باید برابر client_credentials قرار گیرد
        /// </summary>
        [JsonProperty("grant_type")]
        public string grant_type { get; set; }

        /// <summary>
        /// کد ملی ۱۰ رقمی شخص فراخوانی کننده که باید به کلاینت دسترسی داشته باشد
        /// </summary>
        [JsonProperty("nid")]
        public string nid { get; set; }

        /// <summary>
        /// Optional: در صورتی که قصد استفاده از سرویس‌های بدون حساب را دارید (مانند استعلام شبا) نیازی به ارسال این فیلد نیست. 
        /// <para>در صورتی که قصد استفاده از سرویس‌های مبتنی بر حساب را دارید باید این فیلد را ارسال کنید: شماره حساب ۱۳ رقمی بانک آینده که به کلاینت اضافه شده است (برای حسابهای غیر حقیقی لازم است توسط راهبر تایید شده باشد)</para>
        /// </summary>
        [JsonProperty("deposit")]
        public string deposit { get; set; }
    }

    internal class FinnotechRefreshToken
    {
        public FinnotechRefreshToken()
        {
            grant_type = "refresh_token";
        }
        /// <summary>
        ///  این مقدار باید برابر refresh_token قرار گیرد
        /// </summary>
        [JsonProperty("grant_type")]
        public string grant_type { get; set; }

        /// <summary>
        /// رفرش توکن
        /// </summary>
        [JsonProperty("refresh_token")]
        public string refresh_token { get; set; }
    }
}
