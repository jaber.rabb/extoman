﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Payment.Finnotech;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service.WebServices
{
    public class FinnotechService : WebServiceManager, IFinnotechService
    {
        private readonly string url = "https://apibeta.finnotech.ir/";
        private readonly string clientId = AppSettingManager.Finnotech_ClientId;/*"extoman";*/
        private readonly string secret = AppSettingManager.Finnotech_Secret; /*"d73acd24da1a4ece2815";*/
        private readonly string clientCredentialTokenCacheKey = "Xtoman.Finnotech.ClientCredential.Token";
        private readonly string clientCredentialRefreshTokenCacheKey = "Xtoman.Finnotech.ClientCredential.RefreshToken";
        private readonly IFinnotechResponseService _finnotechResponseService;
        private readonly ICacheManager _cacheManager;
        private readonly List<string> clientCredentialScopes = new List<string>
        {
            "card:information:get",
            "card:transfer-peyvand:execute",
            "card:list:get",
            "card:transfer-track:get",
            "card:transfer-peyvand-track:get",
            "oak:iban-inquiry:get",
            "oak:nid-verification:get",
            "oak:card-to-deposit:get",
            "oak:deposit-to-iban:get",
            "facility:card-to-iban:get"
        };

        public FinnotechService(ICacheManager cacheManager, IFinnotechResponseService finnotechResponseService)
        {
            _cacheManager = cacheManager;
            _finnotechResponseService = finnotechResponseService;
        }

        public async Task<string> GetClientCredentialAsync()
        {
            if (_cacheManager.IsSet(clientCredentialTokenCacheKey))
            {
                if (_cacheManager.IsSet(clientCredentialRefreshTokenCacheKey))
                {
                    var refreshToken = _cacheManager.Get<string>(clientCredentialRefreshTokenCacheKey);
                    var refreshResult = await RefreshClientCredentialAsync(refreshToken);
                    if (refreshResult.Status == FinnotechState.DONE)
                        return refreshResult.Result.Value;
                }
                return _cacheManager.Get<string>(clientCredentialTokenCacheKey);
            }
            else
            {
                var clientCredential = await CreateClientCredentialAsync();
                if (clientCredential.Status == FinnotechState.DONE)
                {
                    var lifeTime = (clientCredential.Result.LifeTime / 1000 / 60).ToInt() - 1;
                    _cacheManager.Remove(clientCredentialTokenCacheKey);
                    _cacheManager.Remove(clientCredentialRefreshTokenCacheKey);

                    _cacheManager.Set(clientCredentialTokenCacheKey, clientCredential.Result.Value, lifeTime);
                    _cacheManager.Set(clientCredentialRefreshTokenCacheKey, clientCredential.Result.RefreshToken, lifeTime);

                    return clientCredential.Result.Value;
                }
            }
            throw new Exception("خطای دریافت Client Credential فینوتک.");
        }

        public async Task<FinnotechResponse<FinnotechClientCredential>> CreateClientCredentialAsync(List<string> scopes = null)
        {
            if (scopes == null)
                scopes = clientCredentialScopes;

            var action = "dev/v2/oauth2/token";
            var bodyData = new { grant_type = "client_credentials", nid = "1270420224", scopes = string.Join(",", scopes) };
            var authString = GetAuthenticationString(clientId, secret);

            var restClientRequest = new RestClientRequest(url, action);
            restClientRequest.AddHeader("Authorization", "Basic " + authString);
            restClientRequest.AddJsonBody(bodyData);

            restClientRequest.Method = Method.POST;
            var response = await restClientRequest.Client.ExecuteTaskAsync(restClientRequest);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception($"StatusCode: {response.StatusCode.ToInt()} {response.StatusCode} - Content: {response.Content}");

            var result = LogAndDeserialize<FinnotechResponse<FinnotechClientCredential>>(response.Content);
            return result;
        }

        public async Task<FinnotechResponse<FinnotechRefreshClientCredential>> RefreshClientCredentialAsync(string refreshToken)
        {
            var action = "dev/v2/oauth2/token";
            var bodyData = new { grant_type = "refresh_token", token_type = "CLIENT-CREDENTIAL", refresh_token = refreshToken };
            var authString = GetAuthenticationString(clientId, secret);

            var restClientRequest = new RestClientRequest(url, action);
            restClientRequest.AddHeader("Authorization", "Basic " + authString);
            restClientRequest.AddJsonBody(bodyData);

            restClientRequest.Method = Method.POST;
            var response = await restClientRequest.Client.ExecuteTaskAsync(restClientRequest);
            if (response.StatusCode != HttpStatusCode.OK)
                throw new Exception($"StatusCode: {response.StatusCode.ToInt()} {response.StatusCode} - Content: {response.Content}");

            var result = LogAndDeserialize<FinnotechResponse<FinnotechRefreshClientCredential>>(response.Content);
            return result;
        }

        public async Task<FinnotechResponse<FinnotechCardInformation>> GetCardInformationAsync(string cardNumber, int? userId = null)
        {
            var action = $"mpg/v2/clients/{clientId}/cards/{cardNumber}?trackId={Guid.NewGuid()}";
            return await CallApiAsync<FinnotechCardInformation>(action, userId);
        }

        public async Task<FinnotechResponse<FinnotechIBANByCardNumber>> GetIBANbyCardNumberAsync(string cardNumber, int? userId = null)
        {
            var action = $"facility/v2/clients/{clientId}/cardToIban?trackId={Guid.NewGuid()}&card={cardNumber}";
            return await CallApiAsync<FinnotechIBANByCardNumber>(action, userId);
        }

        public async Task<FinnotechResponse<FinnotechNationalIdVerification>> GetNationalVerificationAsync(string nationalCode, string birthDate, string fullName, string firstName, string lastName, string fatherName, int? userId = null)
        {
            var action = $"oak/v2/clients/{clientId}/nidVerification?trackId={Guid.NewGuid()}&nationalCode={nationalCode}&birthDate={birthDate}&fullName={fullName}&firstName={firstName}&lastName={lastName}&fatherName={fatherName}";
            return await CallApiAsync<FinnotechNationalIdVerification>(action, userId);
        }

        public async Task<string> GetNationalVerificationStringAsync(string nationalCode, string birthDate, string fullName, string firstName, string lastName, string fatherName, int? userId = null)
        {
            var action = $"oak/v2/clients/{clientId}/nidVerification?trackId={Guid.NewGuid()}&nationalCode={nationalCode}&birthDate={birthDate}&fullName={fullName}&firstName={firstName}&lastName={lastName}&fatherName={fatherName}";
            return await CallApiStringAsync(action, userId);
        }
        //public async Task<FinnotechResponse<List<FinnotechInquiry>>> GetAllInquiryRequestsAsync(DateTime? fromDate = null, DateTime? toDate = null, FinnotechState? status = null)
        //{
        //    throw new NotImplementedException();
        //}

        //public async Task<FinnotechResponse<FinnotechInquiry>> GetInquiryRequestAsync(string trackId)
        //{
        //    throw new NotImplementedException();
        //}

        #region Helpers
        private string GetAuthenticationString(string clientId, string clientSecret)
        {
            var toEncode = $"{clientId}:{clientSecret}";
            var plainTextBytes = Encoding.UTF8.GetBytes(toEncode);
            return Convert.ToBase64String(plainTextBytes);
        }

        private async Task<FinnotechResponse<T>> CallApiAsync<T>(string action, int? userId = null, FinnotechAuthType authType = FinnotechAuthType.ClientCredential, Method method = Method.GET, object bodyObject = null) where T : FinnotechResult
        {
            var restClientRequest = new RestClientRequest(url, action);
            restClientRequest.Method = method;
            if (bodyObject != null)
                restClientRequest.AddJsonBody(bodyObject);

            switch (authType)
            {
                case FinnotechAuthType.ClientCredential:
                    var clientCredential = await GetClientCredentialAsync();
                    restClientRequest.AddHeader("Authorization", "Bearer " + clientCredential);
                    break;
                case FinnotechAuthType.AuthorizationCode:
                    break;
            }

            var response = await restClientRequest.Client.ExecuteTaskAsync(restClientRequest);
            if (response.StatusCode != HttpStatusCode.OK)
                new Exception($"StatusCode: {response.StatusCode.ToInt()} {response.StatusCode} - Content: {response.Content}").LogError();

            response = restClientRequest.FixString(response);
            var result = LogAndDeserialize<FinnotechResponse<T>>(response.Content);
            result.StatusCode = response.StatusCode.ToInt();

            var finnotechResponse = GetFinnotechResponse(result, typeof(T).Name, userId);
            if (finnotechResponse != null)
            {
                await _finnotechResponseService.InsertAsync(finnotechResponse);
                result.HistoryId = finnotechResponse.Id;
            }

            return result;
        }

        private async Task<string> CallApiStringAsync(string action, int? userId = null, FinnotechAuthType authType = FinnotechAuthType.ClientCredential, Method method = Method.GET, object bodyObject = null)
        {
            var restClientRequest = new RestClientRequest(url, action);
            restClientRequest.Method = method;
            if (bodyObject != null)
                restClientRequest.AddJsonBody(bodyObject);

            switch (authType)
            {
                case FinnotechAuthType.ClientCredential:
                    var clientCredential = await GetClientCredentialAsync();
                    restClientRequest.AddHeader("Authorization", "Bearer " + clientCredential);
                    break;
                case FinnotechAuthType.AuthorizationCode:
                    break;
            }

            var response = await restClientRequest.Client.ExecuteTaskAsync(restClientRequest);

            response = restClientRequest.FixString(response);
            return $"StatusCode: {response.StatusCode.ToInt()} {response.StatusCode} - Content: {response.Content}";
        }


        private Finnotech GetFinnotechResponse(object result, string typeName, int? userId = null)
        {
            switch (typeName)
            {
                case "FinnotechCardInformation":
                    var finnotechCardInfo = result.CastToClass<FinnotechResponse<FinnotechCardInformation>>();
                    return new FinnotechResponseCardInfo()
                    {
                        ForUserId = userId,
                        Code = finnotechCardInfo.Error?.Code,
                        Message = finnotechCardInfo.Error?.Message,
                        Description = finnotechCardInfo.Result?.Description,
                        TrackId = finnotechCardInfo.TrackId.ToString(),
                        DestCard = finnotechCardInfo.Result?.DestCard,
                        DoTime = finnotechCardInfo.Result?.DoTime,
                        Name = finnotechCardInfo.Result?.Name,
                        Result = finnotechCardInfo.Result?.Result,
                        StatusCode = finnotechCardInfo.StatusCode,
                    };
                case "FinnotechNationalIdVerification":
                    var finnotechIdVerification = result.CastToClass<FinnotechResponse<FinnotechNationalIdVerification>>();
                    return new FinnotechResponseIdVerification()
                    {
                        ForUserId = userId,
                        BirthDate = finnotechIdVerification.Result?.BirthDate,
                        Code = finnotechIdVerification.Error?.Code,
                        DeathStatus = finnotechIdVerification.Result?.DeathStatus,
                        FatherName = finnotechIdVerification.Result?.FatherName,
                        FatherNameSimilarity = finnotechIdVerification.Result?.FatherNameSimilarity,
                        FirstName = finnotechIdVerification.Result?.FirstName,
                        FirstNameSimilarity = finnotechIdVerification.Result?.FirstNameSimilarity,
                        FullName = finnotechIdVerification.Result?.FullName,
                        FullNameSimilarity = finnotechIdVerification.Result?.FullNameSimilarity,
                        LastName = finnotechIdVerification.Result?.LastName,
                        LastNameSimilarity = finnotechIdVerification.Result?.LastNameSimilarity,
                        Message = finnotechIdVerification.Error?.Message,
                        NationalId = finnotechIdVerification.Result?.NationalCode,
                        Status = finnotechIdVerification.Result?.Status.ToDisplay(),
                        StatusCode = finnotechIdVerification.StatusCode,
                        TrackId = finnotechIdVerification.TrackId
                    };
                case "FinnotechIBANByCardNumber":
                    var finnotechIBANByCardNumber = result.CastToClass<FinnotechResponse<FinnotechIBANByCardNumber>>();
                    return new FinnotechResponseIBANDetail() { 
                        ForUserId = userId,
                        AlertCode = finnotechIBANByCardNumber.Result?.AlertCode,
                        BankName = finnotechIBANByCardNumber.Result?.BankName,
                        Card = finnotechIBANByCardNumber.Result?.Card,
                        Code = finnotechIBANByCardNumber.Error?.Code,
                        StatusCode = finnotechIBANByCardNumber.StatusCode,
                        DepositComment = finnotechIBANByCardNumber.Result?.DepositComment,
                        Deposit = finnotechIBANByCardNumber.Result?.Deposit,
                        DepositOwner = finnotechIBANByCardNumber.Result?.DepositOwners.Select(p => p.FirstName + " " + p.LastName).First(),
                        DepositStatus = finnotechIBANByCardNumber.Result?.DepositStatus,
                        DepositDescription = finnotechIBANByCardNumber.Result?.DepositDescription,
                        Iban = finnotechIBANByCardNumber.Result?.Iban,
                        TrackId = finnotechIBANByCardNumber.TrackId,
                        Message = finnotechIBANByCardNumber.Error?.Message
                    };
                    //case "FinnotechBalance":
                    //    var finnotechBalance = result.CastToClass<FinnotechResponse<FinnotechBalance>>();
                    //    return new FinnotechResponseBalance()
                    //    {
                    //        ForUserId = userId,
                    //        TrackId = finnotechBalance.TrackId,
                    //        StatusCode = finnotechBalance.StatusCode,
                    //        Message = finnotechBalance.Message,
                    //        AvailableBalance = finnotechBalance.AvailableBalance,
                    //        Code = finnotechBalance.Code,
                    //        CurrentBalance = finnotechBalance.CurrentBalance,
                    //        EffectiveBalance = finnotechBalance.EffectiveBalance,
                    //        FarsiMessage = finnotechBalance.FarsiMessage,
                    //        Number = finnotechBalance.Number,
                    //        State = finnotechBalance.State.HasValue() ? finnotechBalance.State.Equals("DONE", StringComparison.InvariantCultureIgnoreCase) ? FinnotechState.DONE : FinnotechState.FAILED : FinnotechState.FAILED
                    //    };
                    //case "FinnotechTransferTo":
                    //    var finnotechTransferTo = result.CastToClass<FinnotechTransferTo>();
                    //    return new FinnotechResponseTransferTo()
                    //    {
                    //        ForUserId = userId,
                    //        Amount = finnotechTransferTo.Amount,
                    //        State = finnotechTransferTo.State,
                    //        FarsiMessage = finnotechTransferTo.FarsiMessage,
                    //        Code = finnotechTransferTo.Code,
                    //        Message = finnotechTransferTo.Message,
                    //        StatusCode = finnotechTransferTo.StatusCode,
                    //        Description = finnotechTransferTo.Description,
                    //        DestinationFirstname = finnotechTransferTo.DestinationFirstname,
                    //        DestinationLastname = finnotechTransferTo.DestinationLastname,
                    //        DestinationNumber = finnotechTransferTo.DestinationNumber,
                    //        InquiryDate = finnotechTransferTo.InquiryDate,
                    //        InquirySequence = finnotechTransferTo.InquirySequence,
                    //        InquiryTime = finnotechTransferTo.InquiryTime,
                    //        PaymentNumber = finnotechTransferTo.PaymentNumber,
                    //        RefCode = finnotechTransferTo.RefCode,
                    //        SourceFirstname = finnotechTransferTo.SourceFirstname,
                    //        SourceLastname = finnotechTransferTo.SourceLastname,
                    //        SourceNumber = finnotechTransferTo.SourceNumber,
                    //        TrackId = finnotechTransferTo.TrackId,
                    //        TypeEnum = finnotechTransferTo.TypeEnum
                    //    };
                    //case "FinnotechPayaReport":
                    //    var finnotechPayaReport = result.CastToClass<FinnotechPayaReport>();
                    //    var fpr = new FinnotechResponsePayaReport()
                    //    {
                    //        ForUserId = userId,
                    //        Code = finnotechPayaReport.Code,
                    //        Message = finnotechPayaReport.Message,
                    //        StatusCode = finnotechPayaReport.StatusCode,
                    //        State = finnotechPayaReport.State,
                    //        FarsiMessage = finnotechPayaReport.FarsiMessage,
                    //        Count = finnotechPayaReport.Count,
                    //        Transactions = new List<FinnotechResponsePayaTransaction>()
                    //    };
                    //    foreach (var item in finnotechPayaReport.Transactions)
                    //    {
                    //        fpr.Transactions.Add(new FinnotechResponsePayaTransaction()
                    //        {
                    //            AdDate = item.AdDate,
                    //            Amount = item.Amount,
                    //            Comment = item.Comment,
                    //            CycleTime = item.CycleTime,
                    //            PayaCycle = item.PayaCycle,
                    //            PayId = item.PayId,
                    //            ReceiverIban = item.ReceiverIban,
                    //            ReceiverName = item.ReceiverName,
                    //            Rejected = item.Rejected,
                    //            ReturnCausality = item.ReturnCausality,
                    //            ReturnId = item.ReturnId,
                    //            SenderBank = item.SenderBank,
                    //            SenderIban = item.SenderIban,
                    //            SenderName = item.SenderName,
                    //            TraceId = item.TraceId,
                    //            TransactionId = item.TransactionId,
                    //            TransactionDate = item.TransactionDate,
                    //            Finnotech = fpr
                    //        });
                    //    }
                    //    return fpr;
            }
            return null;
        }

        #endregion
    }

}
