﻿using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Payment.Finnotech;
using Xtoman.Domain.WebServices.Payment.Finnotech.V2;

namespace Xtoman.Service.WebServices
{
    public interface IFinnotechOldService
    {
        #region Version 1
        Task<FinnotechAuthentication> BasicAuthorizeAsync(bool isDeposit = false);
        /// <summary>
        /// برای استعلام شماره کارت های عضو شتاب از این سرویس استفاده کنید.
        /// </summary>
        /// <param name="cardNumber">شماره کارت ۱۶ رقمی</param>
        /// <returns></returns>
        Task<FinnotechCardInformation> GetCardInformationAsync(string cardNumber, int? userId = null);
        /// <summary>
        /// صحت سنجی کد ملی، این سرویس کد ملی، تارخ تولد و نام را دریافت میکند و شباهت نام ارسال شده را با نام واقعی صاحب کد ملی برمیگرداند. نام را هم به صورت یک فیلد و هم به صورت نام و نام خانوادگی جدا میتوانید بفرستید. این سرویس از ساعت ۲۰:۰۰ تا ساعت ۷:۰۰ غیر فعال است . در صورتی که در این بازه سرویس را فراخوانی کنید با خطای 'شما در بازه زمانی تعریف شده قرار ندارید' مواجه خواهید شد .
        /// </summary>
        /// <param name="nid">کد ملی که میخواهید صحت آن را بررسی کنید و اجباری است</param>
        /// <param name="birthDate">تاریخ تولد صاحب این کد ملی که اجباری است، به صورت yyyy/mm/dd شمسی</param>
        /// <param name="fullName">نام و نام خانوادگی که میخواهید صحت آن را بررسی کنید (یکی از فیلدهای نام یا نام خانوادگی و یا نام کامل اجباری است). نام کامل را به صورت نام و یک فاصله و نام خانوادگی بفرستید</param>
        /// <param name="firstName">نام کوچک صاحب کد ملی (یکی از فیلدهای نام یا نام خانوادگی و یا نام کامل اجباری است)</param>
        /// <param name="lastName">نام خانوادگی صاحب کد ملی (یکی از فیلدهای نام یا نام خانوادگی و یا نام کامل اجباری است)</param>
        /// <param name="fatherName">نام پدر صاحب کد ملی (اختیاری است)</param>
        /// <returns></returns>
        Task<FinnotechIdVerification> GetIdVerificationAsync(string nid, string birthDate, string fullName, string firstName, string lastName, string fatherName, int? userId = null);
        /// <summary>
        /// استعلام کد ملی، این سرویس کد ملی و تاریخ تولد دریافت میکند و اطلاعات ثبت احوال را برمیگرداند. این سرویس از ساعت ۲۰:۰۰ تا ساعت ۷:۰۰ غیر فعال است . در صورتی که در این بازه سرویس را فراخوانی کنید با خطای 'شما در بازه زمانی تعریف شده قرار ندارید' مواجه خواهید شد .
        /// <para><b>‫نکته مهم: پارامتر های query باید به صورت encode شده ارسال شوند.</b></para>
        /// </summary>
        /// <param name="nid"> کد ملی که میخواهید صحت آن را بررسی کنید و اجباری است</param>
        /// <param name="birthDate">تاریخ تولد صاحب این کد ملی که اجباری است، به صورت yyyy/mm/dd</param>
        /// <returns></returns>
        Task<FinnotechIdDetail> GetIdDetailAsync(string nid, string birthDate, int? userId = null);
        /// <summary>
        /// دریافت مانده حساب
        /// </summary>
        /// <returns></returns>
        Task<FinnotechBalance> GetBalanceAsync();
        /// <summary>
        /// سرویس انتفال وجه (داخلی، پایا و ساتنا) از حساب کلاینت به حساب دیگران، با استفاده از این سرویس می‌‌توانید از حساب خود در بانک به شماره شبا سایر بانک‌ها و شماره حساب‌ بانک آینده پول واریز کنید.
        /// </summary>
        /// <param name="amount">مبلغ انتقال وجه به ریال</param>
        /// <param name="destinationNumber">شماره حساب مقصد به صورت ۱۳ رقم شماره حساب بانکی یا ۲۶ کاراکتر شماره شبا</param>
        /// <param name="destinationFirstName">نام صاحب حساب مقصد، در صورتی که حساب مقصد شبا باشد حداقل ۲ و حداکثر ۳۰ کاراکتر. برای انتقال وجه داخلی نیازی به این فیلد نیست</param>
        /// <param name="destinationLastname">نام خانوادگی صاحب حساب مقصد، در صورتی که حساب مقصد شبا باشد حداقل ۲ و حداکثر ۳۰ کاراکتر. برای انتقال وجه داخلی نیازی به این فیلد نیست</param>
        /// <param name="paymentNumber">شناسه پرداخت ,این عدد در صورتحساب شما و مشتریان درج میشود و میتواند عددی مانند شماره فاکتور و یا سند داخلی باشد (حداکثر ۱۵ رقم)</param>
        /// <param name="description">شرح انتقال وجه (حداکثر ۳۰ کاراکتر)</param>
        /// <returns></returns>
        Task<FinnotechTransferTo> TransferToAsync(long amount, string destinationNumber, string destinationFirstname, string destinationLastname, string paymentNumber, string description, int? userId = null);
        Task<FinnotechPayaReport> PayaReportAsync(string fromDate, string toDate, int offset, int length, int? userId = null);
        #endregion

        #region Version 2
        Task<FinnotechResponseV2<FinnotechBalanceV2>> BalanceV2Async();
        Task<FinnotechResponseV2<FinnotechMobileVerifyV2>> VerifyMobileWithIdCardNumberV2Async(string mobile, string nationalCode, int? userId = null);
        #endregion
    }
}
