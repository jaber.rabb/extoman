﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Payment.Finnotech;

namespace Xtoman.Service.WebServices
{
    public interface IFinnotechService
    {
        /// <summary>
        /// از این سرویس برای تولید توکن استفاده کنید. <para>Client_Credential رویکرد</para>
        /// </summary>
        /// <returns></returns>
        Task<FinnotechResponse<FinnotechClientCredential>> CreateClientCredentialAsync(List<string> scopes = null);

        /// <summary>
        /// قبل از منقضی شدن توکن با ارسال رفرش توکن، می‌توانید توکن جدید دریافت کنید.
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <returns></returns>
        Task<FinnotechResponse<FinnotechRefreshClientCredential>> RefreshClientCredentialAsync(string refreshToken);

        /// <summary>
        /// برای استعلام شماره کارت های عضو شتاب از این سرویس استفاده کنید.
        /// </summary>
        /// <param name="cardNumber">شماره کارت شتابی 16 رقمی</param>
        /// <returns></returns>
        Task<FinnotechResponse<FinnotechCardInformation>> GetCardInformationAsync(string cardNumber, int? userId = null);


        /// <summary>
        /// سرویس اطلاعات شبا از روی شماره کارت 16 رقمی شتاب
        /// اسامی بانک‌هایی که این سرویس برای آن‌ها قابل انجام است:
        /// بانک آینده, 
        /// بانک اقتصاد نوین, 
        /// بانک کشاورزی, 
        /// بانک سامان, 
        /// بانک سرمایه, 
        /// بانک پاسارگاد, 
        /// بانک صادرات, 
        /// بانک ملی, 
        /// بانک رسالت, 
        /// بانک دی, 
        /// بانک انصار, 
        /// بانک ملت, 
        /// بانک شهر, 
        /// بانک مسکن, 
        /// بانک ایران زمین, 
        /// بانک پارسیان 
        /// </summary>
        /// <param name="cardNumber">شماره کارت شتابی 16 رقمی</param>
        /// <returns></returns>
        Task<FinnotechResponse<FinnotechIBANByCardNumber>> GetIBANbyCardNumberAsync(string cardNumber, int? userId = null);

        /// <summary>
        /// صحت سنجی کد ملی، این سرویس کد ملی، تارخ تولد و نام را دریافت میکند و شباهت نام ارسال شده را با نام واقعی صاحب کد ملی برمیگرداند. نام را هم به صورت یک فیلد و هم به صورت نام و نام خانوادگی جدا میتوانید بفرستید. این سرویس از ساعت ۲۰:۰۰ تا ساعت ۷:۰۰ غیر فعال است . در صورتی که در این بازه سرویس را فراخوانی کنید با خطای 'شما در بازه زمانی تعریف شده قرار ندارید' مواجه خواهید شد .
        /// </summary>
        /// <param name="nationalCode">کد ملی که میخواهید صحت آن را بررسی کنید و اجباری است</param>
        /// <param name="birthDate">تاریخ تولد صاحب این کد ملی که اجباری است، به صورت yyyy/mm/dd</param>
        /// <param name="fullName">نام و نام خانوادگی که میخواهید صحت آن را بررسی کنید (یکی از فیلدهای نام یا نام خانوادگی و یا نام کامل اجباری است)<para>نام کامل را به صورت نام و یک فاصله و نام خانوادگی بفرستید</para></param>
        /// <param name="firstName">نام کوچک صاحب کد ملی (یکی از فیلدهای نام یا نام خانوادگی و یا نام کامل اجباری است)</param>
        /// <param name="lastName">نام خانوادگی صاحب کد ملی (یکی از فیلدهای نام یا نام خانوادگی و یا نام کامل اجباری است)</param>
        /// <param name="fatherName">نام پدر صاحب کد ملی (اختیاری است)</param>
        /// <returns></returns>
        Task<FinnotechResponse<FinnotechNationalIdVerification>> GetNationalVerificationAsync(string nationalCode, string birthDate, string fullName, string firstName, string lastName, string fatherName, int? userId = null);
        Task<string> GetNationalVerificationStringAsync(string nationalCode, string birthDate, string fullName, string firstName, string lastName, string fatherName, int? userId = null);
        //Task<FinnotechResponse<FinnotechInquiry>> GetInquiryRequestAsync(string trackId);
        //Task<FinnotechResponse<List<FinnotechInquiry>>> GetAllInquiryRequestsAsync(DateTime? fromDate = null, DateTime? toDate = null, FinnotechState? status = null);
    }
}
