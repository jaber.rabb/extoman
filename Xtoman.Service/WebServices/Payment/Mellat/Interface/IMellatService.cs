﻿using System.Threading.Tasks;
using System.Web;
using Xtoman.Domain.WebServices.Payment.Mellat;

namespace Xtoman.Service.WebServices
{
    public interface IMellatService
    {
        PayRequestResult PayRequest(long amount, string additionalData, long orderId, string callBackUrl, long payerId = 0);
        MellatResponseMessage VerifyRequest(long orderid, long saleOrderId, long saleReferenceId);
        MellatResponseMessage SettleRequest(long orderId, long saleOrderId, long saleReferenceId);
        void MakePayment(long amount, string additionalData, long orderId, string callBackUrl, long payerId = 0, HttpContextBase httpContext = null);
        Task<MellatResponseMessage> InquiryRequest(long orderId, long saleOrderId, long saleReferenceId);
        Task<PayRequestResult> PayRequestAsync(long amount, string additionalData, string callBackUrl, long orderId, long payerId = 0);
        Task<MellatResponseMessage> VerifyRequestAsync(long orderId, long saleOrderId, long saleReferenceId);
        Task<MellatResponseMessage> SettleRequestAsync(long orderId, long saleOrderId, long saleReferenceId);
        Task MakePaymentAsync(long amount, string additionalData, long orderId, string callBackUrl, long payerId = 0, HttpContextBase httpContext = null);
        string GetResponseMessage(int resCode);
    }
}
