﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using Xtoman.Domain.Settings;
using Xtoman.Domain.WebServices.Payment.Mellat;
using Xtoman.Service.WebServices.Payment.Mellat;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class MellatService : PaymentService, IMellatService
    {
        #region Properties
        private readonly string username;
        private readonly string password;
        private readonly int terminalId;
        private readonly PaymentSettings _paymentSettings;
        private readonly PaymentGatewayClient _paymentGatewayClient;
        private readonly string merchantUrl = "https://bpm.shaparak.ir/pgwchannel/startpay.mellat";
        #endregion

        #region Constructor
        public MellatService(PaymentSettings paymentSettings)
        {
            if (AppSettingManager.IsLocale)
                WebRequest.DefaultWebProxy = Proxy.DefaultProxy;

            _paymentSettings = paymentSettings;
            _paymentGatewayClient = new PaymentGatewayClient();

            password = AppSettingManager.Behpardakht_Password;
            username = AppSettingManager.Behpardakht_Username;
            terminalId = AppSettingManager.Behpardakht_TerminalCode;
        }
        #endregion

        #region Sync Methods
        [Obsolete("this Method is obsolete")]
        public PayRequestResult PayRequest(long amount, string additionalData, long orderId, string callBackUrl, long payerId = 0)
        {
            var now = DateTime.Now;
            var localDate = now.Date.ToString("yyyyMMdd");
            var localTime = now.TimeOfDay.ToString("hhmmss");
            var url = HttpContext.Current.GetBaseUrl() + "/" + callBackUrl.TrimStart('/');
            var response = _paymentGatewayClient.bpPayRequest(terminalId, username, password, orderId, amount * 10, localDate, localTime, additionalData, url, payerId);
            if (AppSettingManager.LogApi)
                new Exception(response).LogError();

            var result = new PayRequestResult();
            try
            {
                if (response.Contains(","))
                {
                    string[] mellatRequestResultArray = response.Split(',');
                    result.ResCode = Convert.ToInt32(mellatRequestResultArray[0]);
                    result.RefId = mellatRequestResultArray[1];
                }
                else
                {
                    result.ResCode = Convert.ToInt32(response);
                }
                result.ResponseMessage = result.ResCode.ToEnum<MellatResponseMessage>();
            }
            catch (Exception ex)
            {
                ex.LogError();
            }

            return result;
        }

        [Obsolete("this Method is obsolete")]
        public MellatResponseMessage VerifyRequest(long orderid, long saleOrderId, long saleReferenceId)
        {
            var response = _paymentGatewayClient.bpVerifyRequest(terminalId, username, password, orderid, saleOrderId, saleReferenceId);
            if (AppSettingManager.LogApi)
                new Exception(response).LogError();

            var resCode = Convert.ToByte(response);
            return (MellatResponseMessage)resCode;
        }

        [Obsolete("this Method is obsolete")]
        public MellatResponseMessage SettleRequest(long orderId, long saleOrderId, long saleReferenceId)
        {
            var response = _paymentGatewayClient.bpSettleRequest(terminalId, username, password, orderId, saleOrderId, saleReferenceId);
            if (AppSettingManager.LogApi)
                new Exception(response).LogError();

            var resCode = Convert.ToByte(response);
            return (MellatResponseMessage)resCode;
        }

        [Obsolete("this Method is obsolete")]
        public void MakePayment(long amount, string additionalData, long orderId, string callBackUrl, long payerId = 0, HttpContextBase httpContext = null)
        {
            var result = PayRequest(amount, additionalData, orderId, callBackUrl, payerId);

            if (result.ResponseMessage == MellatResponseMessage.Success)
                MakePayment(new { RefId = result.RefId }, merchantUrl, "form1", "post", null, httpContext);
        }
        #endregion

        #region Async Methods
        public async Task<MellatResponseMessage> InquiryRequest(long orderId, long saleOrderId, long saleReferenceId)
        {
            var response = await _paymentGatewayClient.bpInquiryRequestAsync(terminalId, username, password, orderId, saleOrderId, saleReferenceId);
            if (AppSettingManager.LogApi)
                new Exception(response.Body.@return).LogError();

            var resCode = Convert.ToByte(response);
            return (MellatResponseMessage)resCode;
        }

        public async Task<PayRequestResult> PayRequestAsync(long amount, string additionalData, string callBackUrl, long orderId, long payerId = 0)
        {
            var now = DateTime.Now;
            var localDate = now.Date.ToString("yyyyMMdd");
            var localTime = now.TimeOfDay.ToString("hhmmss");
            var url = HttpContext.Current.GetBaseUrl() + "/" + callBackUrl.TrimStart('/');
            var result = await _paymentGatewayClient.bpPayRequestAsync(terminalId, username, password, orderId, amount * 10, localDate, localTime, additionalData, url, payerId);
            var response = result.Body.@return;
            if (AppSettingManager.LogApi)
                new Exception(response).LogError();

            var Result = new PayRequestResult();
            var mellatRequestResultArray = response.Split(',');
            Result.ResCode = mellatRequestResultArray[0].ToInt();
            Result.ResponseMessage = Result.ResCode.ToEnum<MellatResponseMessage>();
            try
            {
                Result.RefId = mellatRequestResultArray[1];
            }
            catch (Exception ex)
            {
                ex.LogError();
            }

            return Result;
        }

        public async Task<MellatResponseMessage> VerifyRequestAsync(long orderId, long saleOrderId, long saleReferenceId)
        {
            var result = await _paymentGatewayClient.bpVerifyRequestAsync(terminalId, username, password, orderId, saleOrderId, saleReferenceId);
            var response = result.Body.@return;
            if (AppSettingManager.LogApi)
                new Exception(response).LogError();

            var resCode = Convert.ToByte(response);
            return (MellatResponseMessage)resCode;
        }

        public async Task<MellatResponseMessage> SettleRequestAsync(long orderId, long saleOrderId, long saleReferenceId)
        {
            var result = await _paymentGatewayClient.bpSettleRequestAsync(terminalId, username, password, orderId, saleOrderId, saleReferenceId);
            var resCode = Convert.ToByte(result.Body.@return);
            return (MellatResponseMessage)resCode;
        }

        public async Task MakePaymentAsync(long amount, string additionalData, long orderId, string callBackUrl, long payerId = 0, HttpContextBase httpContext = null)
        {
            var result = await PayRequestAsync(amount, additionalData, callBackUrl, orderId, payerId);

            if (result.ResponseMessage == MellatResponseMessage.Success)
                MakePayment(new { result.RefId }, merchantUrl, "form1", "post", null, httpContext);
        }
        #endregion

        #region Utility Methods
        public string GetResponseMessage(int resCode)
        {
            var response = resCode.ToEnum<MellatResponseMessage>();
            return response.ToDisplay();
        }
        #endregion
    }
}