﻿using System;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class WebServiceManager
    {
        public bool LogApi { get; set; }

        public virtual (T item, bool hasException, Exception exception) LogAndDeserializeException<T>(string response, bool? log = null) where T : new()
        {
            if (log.HasValue)
                LogApi = (bool)log;
            var hasException = false;
            try
            {
                return (response.Deserialize<T>(), hasException, null);
            }
            catch (Exception ex)
            {
                hasException = true;
                new Exception(response, ex).LogError();
                return (new T(), hasException, ex);
            }
            finally
            {
                if (LogApi && !hasException)
                    new Exception(response).LogError();
                LogApi = false;
            }
        }

        public virtual T LogAndDeserialize<T>(string response, bool? log = null) where T : new()
        {
            if (log.HasValue)
                LogApi = (bool)log;
            var hasException = false;
            try
            {
                return response.Deserialize<T>();
            }
            catch (Exception ex)
            {
                hasException = true;
                new Exception(response, ex).LogError();
                return new T();
            }
            finally
            {
                if (LogApi && !hasException)
                    new Exception(response).LogError();
                LogApi = false;
            }
        }

        public virtual T LogAndDeserializeXml<T>(string response, bool? log = null) where T : new()
        {
            if (log.HasValue)
                LogApi = (bool)log;
            var hasException = false;
            try
            {
                return response.DeserializeXml<T>();
            }
            catch (Exception ex)
            {
                hasException = true;
                new Exception(response, ex).LogError();
                return new T();
            }
            finally
            {
                if (LogApi && !hasException)
                    new Exception(response).LogError();
                LogApi = false;
            }
        }
    }
}
