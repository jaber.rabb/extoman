﻿using System.Collections.Generic;
using System.Linq;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.TradingPlatform.Kraken;

namespace Xtoman.Service.WebServices
{
    public static class KrakenAssetPairHelper
    {
        public static string GetAssetAltName(ECurrencyType type)
        {
            return GetAssetName(type).Replace("XX", "X").Replace("Z", "");
        }
        public static string GetAssetName(ECurrencyType type)
        {
            switch (type)
            {
                case ECurrencyType.Bitcoin:
                    return "XXBT";
                case ECurrencyType.Ethereum:
                    return "XETH";
                case ECurrencyType.EthereumClassic:
                    return "XETC";
                case ECurrencyType.Monero:
                    return "XXMR";
                case ECurrencyType.Litecoin:
                    return "XLTC";
                case ECurrencyType.BitcoinCash:
                    return "BCH";
                case ECurrencyType.Dash:
                    return "DASH";
                case ECurrencyType.Zcash:
                    return "XZEC";
                case ECurrencyType.Ripple:
                    return "XXRP";
                case ECurrencyType.USDollar:
                    return "ZUSD";
                case ECurrencyType.Tether:
                    return "USDT";
                case ECurrencyType.Stellar:
                    return "XXLM";
                case ECurrencyType.Euro:
                    return "ZEUR";
                default:
                    return "";
            }
        }
        public static (string pair, KrakenOrderSide side) GetAssetsPairNameAndSide(Dictionary<string, KrakenAssetPair> pairs, ECurrencyType fromType, ECurrencyType toType)
        {
            var pair = "";
            var fromTypeAssetName = GetAssetName(fromType);
            var fromTypeAssetAltName = GetAssetAltName(fromType);
            var toTypeAssetName = GetAssetName(toType);
            var toTypeAssetAltName = GetAssetAltName(toType);
            var side = toType == ECurrencyType.USDollar ||
                       toType == ECurrencyType.Euro ||
                       toType == ECurrencyType.Bitcoin ||
                       (toType == ECurrencyType.Ethereum && fromType != ECurrencyType.Bitcoin)
                                ? KrakenOrderSide.Sell : KrakenOrderSide.Buy;
            if (side == KrakenOrderSide.Sell)
            {
                pair = pairs.Where(p => p.Key == fromTypeAssetName + toTypeAssetName || p.Value.AltName == fromTypeAssetAltName + toTypeAssetAltName)
                    .Select(p => p.Key)
                    .FirstOrDefault();
            }
            else
            {
                pair = pairs.Where(p => p.Key == toTypeAssetName + fromTypeAssetName || p.Value.AltName == toTypeAssetAltName + fromTypeAssetAltName)
                    .Select(p => p.Key)
                    .FirstOrDefault();
            }

            return (pair, side);
        }
    }

}
