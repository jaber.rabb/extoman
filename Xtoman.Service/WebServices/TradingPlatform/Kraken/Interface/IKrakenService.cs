﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.TradingPlatform.Kraken;

namespace Xtoman.Service.WebServices
{
    public interface IKrakenService
    {
        #region Sync methods

        #region Public methods
        /// <summary>
        /// Note: this is to aid in approximating the skew time between the server and client.
        /// </summary>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenServerTimeResult GetServerTime();
        /// <summary>
        /// Get active assets
        /// </summary>
        /// <param name="info">
        /// Info to retrieve (optional):
        /// <para>info = all info (default)</para>
        /// </param>
        /// <param name="assetClass">
        /// Asset class (optional):
        /// <para>currency (default)</para>
        /// </param>
        /// <param name="assets">
        /// Comma delimited list of assets to get info on. (optional. default = all for given asset class).
        /// </param>
        /// <returns>Dictionary of asset names and their info.</returns>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenActiveAssetsResult GetActiveAssets();
        /// <summary>
        /// Get asset pairs
        /// </summary>
        /// <param name="info">
        /// Info to retrieve (optional):
        /// <para>info = all info (default)</para>
        /// <para>leverage = leverage info</para>
        /// <para>fees = fees schedule</para>
        /// <para>margin = margin info</para>
        /// </param>
        /// <param name="pairs">
        /// Comma delimited list of asset pairs to get info on (optional. default = all).
        /// </param>
        /// <returns>Dictionary of pair names and their info.</returns>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenAssetPairsResult GetAssetPairs(List<string> pairs = null);
        /// <summary>
        /// Note: today's prices start at 00:00:00 UTC.
        /// </summary>
        /// <param name="pairs">Comma delimited list of asset pairs to get info on.</param>
        /// <returns>Dictionary of pair names and their ticker info.</returns>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenTickerResult GetTicker(List<string> pairs);
        /// <summary>
        /// Get order book depth
        /// </summary>
        /// <param name="pair">Asset pair to get market depth for.</param>
        /// <param name="count">Maximum number of asks/bids (optional).</param>
        /// <returns>Dictionary of pair name and market depth.</returns>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenOrderBookResult GetOrderBook(string pair, int? count = null);
        /// <summary>
        /// Get recent trades by pair
        /// </summary>
        /// <param name="pair">Asset pair to get trade data for.</param>
        /// <param name="since">Return trade data since given id (optional. exclusive).</param>
        /// <returns>Dictionary of pair name and recent trade data.</returns>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenRecentTradesResult GetRecentTrades(string pair, long since);
        /// <summary>
        /// Get public ticker info for example: BTC/USD pair:
        /// </summary>
        /// <param name="pair"></param>
        /// <param name="since"></param>
        /// <returns></returns>
        KrakenRecentSpreadDataResult GetRecentSpreadData(string pair, long since);
        #endregion

        #region Private user data queries
        /// <summary>
        /// Get balance info
        /// </summary>
        /// <returns>
        /// Array of asset names and balance amount
        /// </returns>
        KrakenBalanceResult GetBalance();
        /// <summary>
        /// Get trade balance
        /// </summary>
        /// <param name="aclass">asset class (optional): currency (default)</param>
        /// <param name="asset">base asset used to determine balance (default = ZUSD)</param>
        /// <returns>
        /// tb = trade balance (combined balance of all currencies)
        /// m = initial margin amount of open positions
        /// n = unrealized net profit/loss of open positions
        /// c = cost basis of open positions
        /// v = current floating valuation of open positions
        /// e = equity = trade balance + unrealized net profit/loss
        /// mf = free margin = equity - initial margin (maximum margin available to open new positions)
        /// ml = margin level = (equity / initial margin) * 100
        /// </returns>
        KrakenTradeBalanceResult GetTradeBalance(string aclass, string asset);
        /// <summary>
        /// Get open orders
        /// </summary>
        /// <param name="trades">whether or not to include trades in output (optional.  default = false)</param>
        /// <param name="userref">restrict results to given user reference id (optional)</param>
        /// <returns>
        /// refid = Referral order transaction id that created this order
        ///userref = user reference id
        ///status = status of order:
        ///  pending = order pending book entry
        ///open = open order
        ///closed = closed order
        ///canceled = order canceled
        ///expired = order expired
        ///opentm = unix timestamp of when order was placed
        ///starttm = unix timestamp of order start time (or 0 if not set)
        ///expiretm = unix timestamp of order end time (or 0 if not set)
        ///descr = order description info
        /// pair = asset pair
        /// type = type of order (buy/sell)
        /// ordertype = order type (See Add standard order)
        /// price = primary price
        /// price2 = secondary price
        /// leverage = amount of leverage
        /// position = position tx id to close (if order is positional)
        /// order = order description
        /// close = conditional close order description (if conditional close set)
        ///vol = volume of order (base currency unless viqc set in oflags)
        ///vol_exec = volume executed (base currency unless viqc set in oflags)
        ///cost = total cost (quote currency unless unless viqc set in oflags)
        ///fee = total fee (quote currency)
        ///price = average price (quote currency unless viqc set in oflags)
        ///stopprice = stop price (quote currency, for trailing stops)
        ///limitprice = triggered limit price (quote currency, when limit based order type triggered)
        ///misc = comma delimited list of miscellaneous info
        ///  stopped = triggered by stop price
        ///  touched = triggered by touch price
        ///  liquidated = liquidation
        ///  partial = partial fill
        ///oflags = comma delimited list of order flags
        /// viqc = volume in quote currency
        ///  plbc = prefer profit/loss in base currency
        ///  nompp = no market price protection 
        ///trades = array of trade ids related to order (if trades info requested and data available)
        KrakenOpenOrdersResult GetOpenOrders(bool includeTrades = false, string userRef = null);
        /// <summary>
        /// Note: Times given by order tx ids are more accurate than unix timestamps. If an order tx
        ///       id is given for the time, the order's open time is used.
        /// </summary>
        /// <param name="includeTrades">
        /// Whether or not to include trades in output (optional. default = false).
        /// </param>
        /// <param name="userRef">Restrict results to given user reference id (optional).</param>
        /// <param name="start">Starting unix timestamp or order tx id of results (optional. exclusive).</param>
        /// <param name="end">Ending unix timestamp or order tx id of results (optional. inclusive).</param>
        /// <param name="offset">Result offset.</param>
        /// <param name="closeTime">
        /// Which time to use (optional).
        /// <para>open</para>
        /// <para>close</para>
        /// <para>both (default)</para>
        /// </param>
        /// <returns>Array of order info.</returns>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenClosedOrdersResult GetClosedOrders(bool includeTrades = false, string userRef = null, long? start = null, long? end = null, int? offset = null, string closeTime = null);
        /// <summary>
        /// Query orders info
        /// </summary>
        /// <param name="transactionIds">txid = comma delimited list of transaction ids to query info about (20 maximum)</param>
        /// <param name="includeTrades">trades = whether or not to include trades in output (optional.  default = false)</param>
        /// <param name="userRef">userref = restrict results to given user reference id (optional)</param>
        /// <returns></returns>
        KrakenOrdersInfoResult QueryOrdersInfo(string transactionIds, bool includeTrades = false, string userRef = null);
        /// <summary>
        /// Note:
        /// <para>
        /// Unless otherwise stated, costs, fees, prices, and volumes are in the asset pair's scale,
        /// not the currency's scale.
        /// </para>
        /// <para>Times given by trade tx ids are more accurate than unix timestamps.</para>
        /// </summary>
        /// <param name="type">
        /// Type of trade (optional):
        /// <para>all = all types (default)</para>
        /// <para>any position = any position (open or closed)</para>
        /// <para>closed position = positions that have been closed</para>
        /// <para>closing position = any trade closing all or part of a position</para>
        /// <para>no position = non-positional trades</para>
        /// </param>
        /// <param name="includeTrades">
        /// Whether or not to include trades related to position in output (optional. default = false).
        /// </param>
        /// <param name="start">Starting unix timestamp or trade tx id of results (optional. exclusive).</param>
        /// <param name="end">Ending unix timestamp or trade tx id of results (optional. inclusive).</param>
        /// <param name="offset">Result offset.</param>
        /// <returns>Dictionary of trade info.</returns>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenTradesHistoryResult GetTradeHistory(KrakenTradeType? type = null, bool includeTrades = false, long? start = null, long? end = null, int? offset = null);
        /// <summary>
        /// Query trades info
        /// </summary>
        /// <param name="transactionIds">
        /// Comma delimited list of transaction ids to query info about (20 maximum).
        /// </param>
        /// <param name="includeTrades">
        /// Whether or not to include trades related to position in output (optional. default = false).
        /// </param>
        /// <returns>Dictionary of trades info.</returns>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenTradesInfoResult QueryTradesInfo(string transactionIds, bool includeTrades = false);
        /// <summary>
        /// Note: Unless otherwise stated, costs, fees, prices, and volumes are in the asset pair's
        ///       scale, not the currency's scale.
        /// </summary>
        /// <param name="transactionIds">
        /// Comma delimited list of transaction ids to restrict output to.
        /// </param>
        /// <param name="doCalculations">
        /// Whether or not to include profit/loss calculations (optional. default = false).
        /// </param>
        /// <returns>Dictionary of open position info.</returns>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenOpenPositionsResult GetOpenPositions(string transactionIds, bool doCalculations = false);
        /// <summary>
        /// Note: Times given by ledger ids are more accurate than unix timestamps.
        /// </summary>
        /// <param name="assetClass">
        /// Asset class (optional):
        /// <para>currency (default)</para>
        /// </param>
        /// <param name="assets">
        /// Comma delimited list of assets to restrict output to (optional. default = all).
        /// </param>
        /// <param name="type">
        /// Type of ledger to retrieve (optional):
        /// <para>all (default)</para>
        /// <para>deposit</para>
        /// <para>withdrawal</para>
        /// <para>trade</para>
        /// <para>margin</para>
        /// </param>
        /// <param name="start">Starting unix timestamp or ledger id of results (optional. exclusive).</param>
        /// <param name="end">Ending unix timestamp or ledger id of results (optional. inclusive).</param>
        /// <param name="offset">Result offset.</param>
        /// <returns>Dictionary of ledgers info.</returns>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenLedgersInfoResult GetLedgersInfo(string assetClass = null, string[] assets = null, KrakenLedgerType? type = null, long? start = null, long? end = null, int? offset = null);
        /// <summary>
        /// Result: associative array of ledgers info
        /// </summary>
        /// <param name="ids">Comma delimited list of ledger ids to query info about (20 maximum).</param>
        /// <returns>Dictionary of ledgers info.</returns>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenLedgersQueryResult QueryLedgers(string[] ids);
        /// <summary>
        /// Note: If an asset pair is on a maker/taker fee schedule, the taker side is given in
        ///       "fees" and maker side in "fees_maker". For pairs not on maker/taker, they will only
        ///       be given in "fees".
        /// </summary>
        /// <param name="pair">Comma delimited list of asset pairs to get fee info on (optional).</param>
        /// <param name="includeFeeInfo">Whether or not to include fee info in results (optional).</param>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenTradeVolumeResult GetTradeVolume(string pair = null, bool includeFeeInfo = false);
        #endregion

        #region Private user trading queries
        /// <summary>
        /// Note:
        /// <para>
        /// See <a href="https://www.kraken.com/help/api#get-tradable-pairs">Get tradable asset
        /// pairs</a> for specifications on asset pair prices, lots, and leverage.
        /// </para>
        /// <para>
        /// Prices can be preceded by +, -, or # to signify the price as a relative amount (with the
        /// exception of trailing stops, which are always relative). + adds the amount to the current
        /// offered price. - subtracts the amount from the current offered price. # will either add
        /// or subtract the amount to the current offered price, depending on the type and order type
        /// used. Relative prices can be suffixed with a % to signify the relative amount as a
        /// percentage of the offered price.
        /// </para>
        /// <para>
        /// For orders using leverage, 0 can be used for the volume to auto-fill the volume needed to
        /// close out your position.
        /// </para>
        /// <para>
        /// If you receive the error "EOrder:Trading agreement required", refer to your API key
        /// management page for further details.
        /// </para>
        /// </summary>
        /// <param name="pair">Asset pair.</param>
        /// <param name="type">Type of order (buy/sell).</param>
        /// <param name="orderType">
        /// Order type:
        /// <para>market</para>
        /// <para>limit (price = limit price)</para>
        /// <para>stop-loss (price = stop loss price)</para>
        /// <para>take-profit (price = take profit price)</para>
        /// <para>stop-loss-profit (price = stop loss price, price2 = take profit price)</para>
        /// <para>stop-loss-profit-limit (price = stop loss price, price2 = take profit price)</para>
        /// <para>stop-loss-limit (price = stop loss trigger price, price2 = triggered limit price)</para>
        /// <para>
        /// take-profit-limit (price = take profit trigger price, price2 = triggered limit price)
        /// </para>
        /// <para>trailing-stop (price = trailing stop offset)</para>
        /// <para>trailing-stop-limit (price = trailing stop offset, price2 = triggered limit offset)</para>
        /// <para>stop-loss-and-limit (price = stop loss price, price2 = limit price)</para>
        /// <para>settle-position</para>
        /// </param>
        /// <param name="volume">Order volume in lots.</param>
        /// <param name="price">Price (optional. dependent upon <paramref name="orderType"/>).</param>
        /// <param name="price2">Secondary price (optional. dependent upon <paramref name="orderType"/>).</param>
        /// <param name="leverage">Amount of leverage desired (optional. default = none).</param>
        /// <param name="orderFlags">
        /// Comma delimited list of order flags (optional):
        /// <para>viqc = volume in quote currency (not available for leveraged orders)</para>
        /// <para>fcib = prefer fee in base currency</para>
        /// <para>fciq = prefer fee in quote currency</para>
        /// <para>nompp = no market price protection</para>
        /// <para>post = post only order (available when <paramref name="orderType"/> = limit)</para>
        /// </param>
        /// <param name="startTime">
        /// Scheduled start time (optional):
        /// <para>0 = now (default)</para>
        /// <para>+{n} = schedule start time {n} seconds from now</para>
        /// <para>{n} = unix timestamp of start time</para>
        /// </param>
        /// <param name="expireTime">
        /// Expiration time (optional):
        /// <para>0 = no expiration (default)</para>
        /// <para>+{n} = expire {n} seconds from now</para>
        /// <para>{n} = unix timestamp of expiration time</para>
        /// </param>
        /// <param name="userRef">User reference id. 32-bit signed number. (optional).</param>
        /// <param name="validate">Validate inputs only. Do not submit order (optional).</param>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenAddOrderResult AddStandardOrder(string pair, KrakenOrderSide type, KrakenOrderType orderType, decimal volume, decimal? price = null, decimal? price2 = null, string leverage = null, List<KrakenOrderFlag> orderFlags = null, string startTime = null, string expireTime = null, string userRef = null, bool? validate = null);
        /// <summary>
        /// Note: <paramref name="transactionId"/> may be a user reference id.
        /// </summary>
        /// <param name="transactionId">Transaction id.</param>
        /// <exception cref="HttpRequestException">There was a problem with the HTTP request.</exception>
        /// <exception cref="KrakenException">There was a problem with the Kraken API call.</exception>
        KrakenCancelOrderResult CancelOpenOrder(string transactionId);
        #endregion

        #region Private user funding queries
        /// <summary>
        /// associative array of deposit methods
        /// </summary>
        /// <param name="aclass">asset class (optional): currency(default)</param>
        /// <param name="asset">asset being deposited</param>
        /// <returns></returns>
        KrakenDepositMethodsResult GetDepositMethods(string asset, string aclass = "");
        /// <summary>
        /// Associative array of deposit addresses
        /// </summary>
        /// <param name="aclass">asset class (optional):currency(default)</param>
        /// <param name="asset">asset being deposited</param>
        /// <param name="method">name of the deposit method</param>
        /// <param name="new">whether or not to generate a new address (optional.  default = false)</param>
        /// <returns></returns>
        KrakenDepositAddressResult GetDepositAddress(string asset, string method, string aclass = "", bool @new = false);

        /// <summary>
        /// Get status of recent deposits
        /// </summary>
        /// <param name="asset">asset being deposited</param>
        /// <param name="method">name of the deposit method</param>
        /// <param name="aclass">asset class (optional): currency(default)</param>
        /// <returns></returns>
        KrakenDepositStatusResult GetRecentDepositStatus(string asset, string method, string aclass = "");
        #endregion

        #endregion

        #region Async methods
        Task<KrakenBalanceResult> GetBalanceAsync();
        Task<KrakenTickerResult> GetTickerAsync(List<string> pairs);
        #endregion
    }
}
