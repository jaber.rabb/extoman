﻿using JackLeitch.RateGate;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.TradingPlatform.Kraken;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service.WebServices
{
    public class KrakenService : WebServiceManager, IKrakenService, IDisposable
    {
        #region Properties
        private readonly string _url = "https://api.kraken.com";
        private readonly int _version = 0;
        private string _key = AppSettingManager.Kraken_Key;
        private string _secret = AppSettingManager.Kraken_Secret;
        //RateGate was taken from http://www.jackleitch.net/2010/10/better-rate-limiting-with-dot-net/
        private RateGate _rateGate;
        private static readonly CultureInfo Culture = CultureInfo.InvariantCulture;
        /// <summary>
        /// Gets or sets the getter function for a nonce (default = <c>DateTime.UtcNow.Ticks</c>).
        /// <para>API expects an increasing value for each request.</para>
        /// </summary>
        public Func<long> GetNonce { get; set; } = () => DateTime.UtcNow.Ticks;
        #endregion

        #region Constructor
        public KrakenService()
        {
            _rateGate = new RateGate(1, TimeSpan.FromSeconds(5));
        }
        #endregion

        #region Dispose & Destructor
        ~KrakenService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_rateGate != null)
                    _rateGate.Dispose();
            }

            _rateGate = null;
        }
        #endregion

        #region Sync Methods
        #region Public queries
        //Get public server time
        //This is to aid in approximatig the skew time between the server and client
        public KrakenServerTimeResult GetServerTime()
        {
            return CallPublicApi<KrakenServerTimeResult>("Time");
        }
        #endregion

        // Get a public list of active assets and their properties:
        /** 
        * Returned assets are keyed by their ISO-4217-A3-X names, example output:
        * 
        * Array
        * (
        *     [error] => Array
        *         (
        *         )
        * 
        *     [result] => Array
        *         (
        *             [XBTC] => Array
        *                 (
        *                     [aclass] => currency
        *                     [altname] => BTC
        *                     [decimals] => 10
        *                     [display_decimals] => 5
        *                 )
        * 
        *             [XLTC] => Array
        *                 (
        *                     [aclass] => currency
        *                     [altname] => LTC
        *                     [decimals] => 10
        *                     [display_decimals] => 5
        *                 )
        * 
        *             [XXRP] => Array
        *                 (
        *                     [aclass] => currency
        *                     [altname] => XRP
        *                     [decimals] => 8
        *                     [display_decimals] => 5
        *                 )
        * 
        *             [ZEUR] => Array
        *                 (
        *                     [aclass] => currency
        *                     [altname] => EUR
        *                     [decimals] => 4
        *                     [display_decimals] => 2
        *                 )
        *             ...
        * )
        */
        public KrakenActiveAssetsResult GetActiveAssets()
        {
            return CallPublicApi<KrakenActiveAssetsResult>("Assets");
        }

        //Get a public list of tradable asset pairs
        public KrakenAssetPairsResult GetAssetPairs(List<string> pairs = null)
        {
            StringBuilder pairString = new StringBuilder();
            if (pairs != null)
            {
                if (pairs.Count() == 0)
                    return null;
                else
                {
                    pairString.Append("pair=");

                    foreach (var item in pairs)
                        pairString.Append(item + ",");

                    pairString.Length--; //disregard trailing comma    
                }
            }

            return CallPublicApi<KrakenAssetPairsResult>("AssetPairs", pairString.ToString());
        }

        // Get public ticker info for example: BTC/USD pair:
        /**
         * Example output:
         *
         * Array
         * (
         *     [error] => Array
         *         (
         *         )
         * 
         *     [result] => Array
         *         (
         *             [XBTCZUSD] => Array
         *                 (
         *                     [a] => Array
         *                         (
         *                             [0] => 106.09583
         *                             [1] => 111
         *                         )
         * 
         *                     [b] => Array
         *                         (
         *                             [0] => 105.53966
         *                             [1] => 4
         *                         )
         * 
         *                     [c] => Array
         *                         (
         *                             [0] => 105.98984
         *                             [1] => 0.13910102
         *                         )
         * 
         *                     ...
         *         )
         * )
         */
        public KrakenTickerResult GetTicker(List<string> pairs)
        {
            if (pairs == null || pairs.Count == 0)
                return null;

            StringBuilder pairString = new StringBuilder("pair=");
            foreach (var item in pairs)
            {
                pairString.Append(item + ",");
            }
            pairString.Length--; //disregard trailing comma

            return CallPublicApi<KrakenTickerResult>("Ticker", pairString.ToString());
        }

        //TODO: Get OHLC data

        /// <summary>
        /// Get public order book
        /// </summary>
        /// <param name="pairs">asset pair to get market depth for</param>
        /// <param name="count">maximum number of ask/bids (optional)</param>
        /// <returns>
        /// pair_name = pair name
        ///             asks = ask side array of array entries([price], [volume], [timestamp])
        ///             bids = bid side array of array entries([price], [volume], [timestamp])
        /// </returns>
        public KrakenOrderBookResult GetOrderBook(string pair, int? count = null)
        {
            if (!pair.HasValue())
                return null;

            string reqs = string.Format("pair={0}", pair);

            if (count.HasValue)
                reqs += string.Format("&count={0}", count.Value.ToString());

            return CallPublicApi<KrakenOrderBookResult>("Depth", reqs);
        }

        /// <summary>
        /// Get recent trades
        /// </summary>
        /// <param name="pair">asset pair to get trade data for</param>
        /// <param name="since">return trade data since given id (exclusive). Unix Time</param>
        /// <remarks> NOTE: the 'since' parameter is subject to change in the future: it's precision may be modified,
        ///             and it may no longer be representative of a timestamp. The best practice is to base it
        ///             on the 'last' value returned in the result set. 
        ///  </remarks>
        /// <returns></returns>
        public KrakenRecentTradesResult GetRecentTrades(string pair, long since)
        {
            if (!pair.HasValue())
                return null;

            string reqs = string.Format("pair={0}", pair);
            reqs += string.Format("&since={0}", since.ToString());
            return CallPublicApi<KrakenRecentTradesResult>("Trades", reqs);
        }

        /// <summary>
        /// Get recent spread data
        /// </summary>
        /// <param name="pair">asset pair to get trade data for</param>
        /// <param name="since">return trade data since given id (inclusive). Unix Time</param>
        /// <remarks> NOTE: Note: "since" is inclusive so any returned data with the same time as the previous
        /// set should overwrite all of the previous set's entries at that time
        ///  </remarks>
        /// <returns></returns>
        public KrakenRecentSpreadDataResult GetRecentSpreadData(string pair, long since)
        {
            if (!pair.HasValue())
                return null;

            string reqs = string.Format("pair={0}", pair);
            reqs += string.Format("&since={0}", since.ToString());
            return CallPublicApi<KrakenRecentSpreadDataResult>("Spread", reqs);
        }
        #endregion

        #region Private user data queries
        /// <summary>
        /// 
        /// </summary>
        /// <returns>
        /// array of asset names and balance amount
        /// </returns>
        public KrakenBalanceResult GetBalance()
        {
            return CallPrivateApi<KrakenBalanceResult>("Balance");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aclass">asset class (optional): currency (default)</param>
        /// <param name="asset">base asset used to determine balance (default = ZUSD)</param>
        /// <returns>
        /// tb = trade balance (combined balance of all currencies)
        /// m = initial margin amount of open positions
        /// n = unrealized net profit/loss of open positions
        /// c = cost basis of open positions
        /// v = current floating valuation of open positions
        /// e = equity = trade balance + unrealized net profit/loss
        /// mf = free margin = equity - initial margin (maximum margin available to open new positions)
        /// ml = margin level = (equity / initial margin) * 100
        /// </returns>
        public KrakenTradeBalanceResult GetTradeBalance(string aclass, string asset)
        {
            string reqs = "";
            if (aclass.HasValue())
                reqs += string.Format("&aclass={0}", aclass);
            if (asset.HasValue())
                reqs += string.Format("&asset={0}", asset);

            return CallPrivateApi<KrakenTradeBalanceResult>("TradeBalance", reqs);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="trades">whether or not to include trades in output (optional.  default = false)</param>
        /// <param name="userref">restrict results to given user reference id (optional)</param>
        /// <returns>
        /// refid = Referral order transaction id that created this order
        ///userref = user reference id
        ///status = status of order:
        ///  pending = order pending book entry
        ///open = open order
        ///closed = closed order
        ///canceled = order canceled
        ///expired = order expired
        ///opentm = unix timestamp of when order was placed
        ///starttm = unix timestamp of order start time (or 0 if not set)
        ///expiretm = unix timestamp of order end time (or 0 if not set)
        ///descr = order description info
        /// pair = asset pair
        /// type = type of order (buy/sell)
        /// ordertype = order type (See Add standard order)
        /// price = primary price
        /// price2 = secondary price
        /// leverage = amount of leverage
        /// position = position tx id to close (if order is positional)
        /// order = order description
        /// close = conditional close order description (if conditional close set)
        ///vol = volume of order (base currency unless viqc set in oflags)
        ///vol_exec = volume executed (base currency unless viqc set in oflags)
        ///cost = total cost (quote currency unless unless viqc set in oflags)
        ///fee = total fee (quote currency)
        ///price = average price (quote currency unless viqc set in oflags)
        ///stopprice = stop price (quote currency, for trailing stops)
        ///limitprice = triggered limit price (quote currency, when limit based order type triggered)
        ///misc = comma delimited list of miscellaneous info
        ///  stopped = triggered by stop price
        ///  touched = triggered by touch price
        ///  liquidated = liquidation
        ///  partial = partial fill
        ///oflags = comma delimited list of order flags
        /// viqc = volume in quote currency
        ///  plbc = prefer profit/loss in base currency
        ///  nompp = no market price protection 
        ///trades = array of trade ids related to order (if trades info requested and data available)
        public KrakenOpenOrdersResult GetOpenOrders(bool includeTrades = false, string userRef = null)
        {
            string reqs = string.Format("&trades={0}", includeTrades);
            if (userRef.HasValue())
                reqs += string.Format("&userref={0}", userRef);

            return CallPrivateApi<KrakenOpenOrdersResult>("OpenOrders", reqs);
        }

        public KrakenClosedOrdersResult GetClosedOrders(bool includeTrades = false, string userRef = null, long? start = null, long? end = null, int? offset = null, string closeTime = null)
        {
            string reqs = string.Format("&trades={0}", includeTrades);
            if (userRef.HasValue())
                reqs += string.Format("&userref={0}", userRef);

            if (start.HasValue)
                reqs += string.Format("&start={0}", start.Value.ToString(Culture));

            if (end.HasValue)
                reqs += string.Format("&end={0}", end.Value.ToString(Culture));

            if (offset.HasValue)
                reqs += string.Format("&ofs={0}", offset.Value.ToString(Culture));

            if (closeTime.HasValue())
                reqs += string.Format("&closetime={0}", closeTime);

            return CallPrivateApi<KrakenClosedOrdersResult>("ClosedOrders", reqs);
        }

        public KrakenOrdersInfoResult QueryOrdersInfo(string transactionIds, bool includeTrades = false, string userRef = null)
        {
            if (!transactionIds.HasValue())
                throw new Exception("TransactionId has not presented!");
            string reqs = string.Format("&txid={0}", transactionIds);
            reqs += string.Format("&trades={0}", includeTrades);
            if (userRef.HasValue())
                reqs += string.Format("&userref={0}", userRef);
            return CallPrivateApi<KrakenOrdersInfoResult>("QueryOrders", reqs);
        }

        public KrakenTradesHistoryResult GetTradeHistory(KrakenTradeType? type = null, bool includeTrades = false, long? start = null, long? end = null, int? offset = null)
        {
            string reqs = "";
            if (type.HasValue)
                reqs += string.Format("&type={0}", type.Value.ToDisplay());

            reqs += string.Format("&trades={0}", includeTrades);
            if (start.HasValue)
                reqs += string.Format("&start={0}", start.Value);
            if (end.HasValue)
                reqs += string.Format("&end={0}", end.Value);
            if (offset.HasValue)
                reqs += string.Format("&ofs={0}", offset.Value);
            return CallPrivateApi<KrakenTradesHistoryResult>("TradesHistory", reqs);
        }

        public KrakenTradesInfoResult QueryTradesInfo(string transactionIds, bool includeTrades = false)
        {
            if (!transactionIds.HasValue())
                throw new Exception("Kraken: TransactionId(s) has not presented!");
            string reqs = string.Format("&txid={0}", transactionIds);
            reqs += string.Format("&trades={0}", includeTrades);

            return CallPrivateApi<KrakenTradesInfoResult>("QueryTrades", reqs);
        }

        public KrakenOpenPositionsResult GetOpenPositions(string transactionIds, bool doCalculations = false)
        {
            if (!transactionIds.HasValue())
                throw new Exception("Kraken: TransactionId(s) has not presented!");
            string reqs = string.Format("&txid={0}", transactionIds);
            reqs += string.Format("&docalcs={0}", doCalculations);

            return CallPrivateApi<KrakenOpenPositionsResult>("OpenPositions", reqs);
        }

        public KrakenLedgersInfoResult GetLedgersInfo(string assetClass = null, string[] assets = null, KrakenLedgerType? type = null, long? start = null, long? end = null, int? offset = null)
        {
            string reqs = "";
            if (assetClass.HasValue())
                reqs += string.Format("&aclass={0}", assetClass);
            if (assets != null && assets.Any())
                reqs += string.Format("&asset={0}", string.Join(",", assets));
            if (type.HasValue)
                reqs += string.Format("&type={0}", type.Value.ToDisplay());
            if (start.HasValue)
                reqs += string.Format("&start={0}", start.Value);
            if (end.HasValue)
                reqs += string.Format("&end={0}", end.Value);
            if (offset.HasValue)
                reqs += string.Format("&ofs={0}", offset.Value);

            return CallPrivateApi<KrakenLedgersInfoResult>("Ledgers", reqs);
        }

        public KrakenLedgersQueryResult QueryLedgers(string[] ids)
        {
            if (ids == null || ids.Count() == 0)
                throw new Exception("Not any ledger id presented!");
            string reqs = string.Format("&id={0}", string.Join(",", ids));

            return CallPrivateApi<KrakenLedgersQueryResult>("QueryLedgers", reqs);
        }

        public KrakenTradeVolumeResult GetTradeVolume(string pair = null, bool includeFeeInfo = false)
        {
            string reqs = "";
            if (pair.HasValue())
                reqs += string.Format("&pair={0}", pair);
            reqs += string.Format("&fee-info={0}", includeFeeInfo);

            return CallPrivateApi<KrakenTradeVolumeResult>("TradeVolume", reqs);
        }
        #endregion

        #region Async methods
        public async Task<KrakenTickerResult> GetTickerAsync(List<string> pairs)
        {
            if (pairs == null || pairs.Count == 0)
                return null;

            StringBuilder pairString = new StringBuilder("pair=");
            foreach (var item in pairs)
            {
                pairString.Append(item + ",");
            }
            pairString.Length--; //disregard trailing comma

            return await CallPublicApiAsync<KrakenTickerResult>("Ticker", pairString.ToString());
        }
        #endregion

        #region Private user trading queries
        public KrakenAddOrderResult AddStandardOrder(string pair, KrakenOrderSide type, KrakenOrderType orderType, decimal volume, decimal? price = null, decimal? price2 = null, string leverage = null, List<KrakenOrderFlag> orderFlags = null, string startTime = null, string expireTime = null, string userRef = null, bool? validate = null)
        {
            if (!pair.HasValue())
                throw new Exception("Kraken: Pair not presented!");
            string reqs = string.Format("&pair={0}", pair);
            reqs += string.Format("&type={0}", type.ToDisplay());
            reqs += string.Format("&ordertype={0}", orderType.ToDisplay());
            reqs += string.Format("&volume={0}", volume);
            if (price.HasValue)
                reqs += string.Format("&price={0}", price.Value);
            if (price2.HasValue)
                reqs += string.Format("&price2={0}", price2.Value);
            if (leverage.HasValue())
                reqs += string.Format("&leverage={0}", leverage);
            if (orderFlags != null && orderFlags.Count > 0)
                reqs += string.Format("&oflags={0}", string.Join(",", orderFlags.Select(p => p.ToDisplay()).ToList()));
            if (startTime.HasValue())
                reqs += string.Format("&starttm={0}", startTime);
            if (expireTime.HasValue())
                reqs += string.Format("&expiretm={0}", expireTime);
            if (userRef.HasValue())
                reqs += string.Format("&userref={0}", userRef);
            if (validate.HasValue)
                reqs += string.Format("&validate={0}", validate);

            reqs += "&trading_agreement=agree";

            // optional closing order to add to system when order gets filled:
            // close[ordertype] = order type
            // close[price] = price
            // close[price2] = secondary price

            return CallPrivateApi<KrakenAddOrderResult>("AddOrder", reqs);
        }

        public KrakenCancelOrderResult CancelOpenOrder(string transactionId)
        {
            if (!transactionId.HasValue())
                throw new Exception("Kraken: Transaction Id not presented!");
            string reqs = string.Format("&txid={0}", transactionId);

            return CallPrivateApi<KrakenCancelOrderResult>("CancelOrder", reqs);
        }
        #endregion

        #region Private user funding queries
        public KrakenDepositMethodsResult GetDepositMethods(string asset, string aclass = "")
        {
            var reqs = "";
            if (aclass.HasValue())
                reqs = string.Format("&aclass={0}", aclass);
            reqs += string.Format("&asset={0}", asset);
            return CallPrivateApi<KrakenDepositMethodsResult>("DepositMethods", reqs);
        }

        public KrakenDepositAddressResult GetDepositAddress(string asset, string method, string aclass = "", bool @new = false)
        {
            if (!method.HasValue())
                throw new Exception("Kraken: Deposit method name not presented!");
            var reqs = "";
            if (aclass.HasValue())
                reqs = string.Format("&aclass={0}", aclass);
            reqs += string.Format("&asset={0}", asset);
            reqs += string.Format("&method={0}", method);
            if (@new)
                reqs += string.Format("&new=true");
            return CallPrivateApi<KrakenDepositAddressResult>("DepositAddresses", reqs);
        }

        public KrakenDepositStatusResult GetRecentDepositStatus(string asset, string method, string aclass = "")
        {
            if (!method.HasValue())
                throw new Exception("Kraken: Deposit method name not presented!");
            var reqs = "";
            if (aclass.HasValue())
                reqs = string.Format("&aclass={0}", aclass);
            reqs += string.Format("&asset={0}", asset);
            reqs += string.Format("&method={0}", method);
            return CallPrivateApi<KrakenDepositStatusResult>("DepositStatus", reqs);
        }
        #endregion

        #region Async methods
        public async Task<KrakenBalanceResult> GetBalanceAsync()
        {
            return await CallPrivateApiAsync<KrakenBalanceResult>("Balance");
        }
        #endregion

        #region Helpers
        private T CallPublicApi<T>(string methodName, string props = null) where T : KrakenBaseResult, new()
        {
            var webRequest = PrepareWebRequest(methodName, props, false);
            if (props != null)
                using (var writer = new StreamWriter(webRequest.GetRequestStream()))
                    writer.Write(props);
            //Make the request
            return MakeWebRequest<T>(webRequest);
        }

        private async Task<T> CallPublicApiAsync<T>(string methodName, string props = null) where T : KrakenBaseResult, new()
        {
            var webRequest = PrepareWebRequest(methodName, props, false);
            if (props != null)
                using (var writer = new StreamWriter(await webRequest.GetRequestStreamAsync()))
                    writer.Write(props);
            return await MakeWebRequestAsync<T>(webRequest);
        }

        private T CallPrivateApi<T>(string methodName, string props = null) where T : KrakenBaseResult, new()
        {
            Int64 nonce = DateTime.Now.Ticks;
            props = "nonce=" + nonce + props;

            string path = string.Format("/{0}/private/{1}", _version, methodName);
            string address = _url + path;
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(address);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";
            webRequest.Headers.Add("API-Key", _key);

            byte[] base64DecodedSecred = Convert.FromBase64String(_secret);

            var np = nonce + Convert.ToChar(0) + props;

            var pathBytes = Encoding.UTF8.GetBytes(path);
            var hash256Bytes = Sha256_hash(np);
            var z = new byte[pathBytes.Count() + hash256Bytes.Count()];
            pathBytes.CopyTo(z, 0);
            hash256Bytes.CopyTo(z, pathBytes.Count());

            var signature = GetHash(base64DecodedSecred, z);

            webRequest.Headers.Add("API-Sign", Convert.ToBase64String(signature));

            if (props != null)
            {
                using (var writer = new StreamWriter(webRequest.GetRequestStream()))
                {
                    writer.Write(props);
                }
            }

            var response = "";
            var hasException = false;
            Exception exception = null;
            try
            {
                //Wait for RateGate
                _rateGate.WaitToProceed();
                using (WebResponse webResponse = webRequest.GetResponse())
                {
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            response = streamReader.ReadLine();
                        }
                    }
                }
            }
            catch (WebException wex)
            {
                hasException = true;
                exception = wex;
                using (HttpWebResponse httpWebResponse = (HttpWebResponse)wex.Response)
                {
                    using (Stream stream = httpWebResponse.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            if (httpWebResponse.StatusCode != HttpStatusCode.InternalServerError)
                            {
                                throw;
                            }
                            response = streamReader.ReadLine();
                        }
                    }
                }

            }
            var result = LogAndDeserializeException<T>(response);
            if (result.hasException)
                exception = result.exception;
            return PrepareResult(result.item, hasException, result.hasException, exception);

            #region Small
            //var webRequest = PrepareWebRequest(methodName, props, true);
            //if (props != null)
            //    using (var writer = new StreamWriter(webRequest.GetRequestStream()))
            //        writer.Write(props);
            //return MakeWebRequest<T>(webRequest);
            #endregion
        }

        private async Task<T> CallPrivateApiAsync<T>(string methodName, string props = null) where T : KrakenBaseResult, new()
        {
            Int64 nonce = DateTime.Now.Ticks;
            props = "nonce=" + nonce + props;

            string path = string.Format("/{0}/private/{1}", _version, methodName);
            string address = _url + path;
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(address);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";
            webRequest.Headers.Add("API-Key", _key);

            byte[] base64DecodedSecred = Convert.FromBase64String(_secret);

            var np = nonce + Convert.ToChar(0) + props;

            var pathBytes = Encoding.UTF8.GetBytes(path);
            var hash256Bytes = Sha256_hash(np);
            var z = new byte[pathBytes.Count() + hash256Bytes.Count()];
            pathBytes.CopyTo(z, 0);
            hash256Bytes.CopyTo(z, pathBytes.Count());

            var signature = GetHash(base64DecodedSecred, z);

            webRequest.Headers.Add("API-Sign", Convert.ToBase64String(signature));

            if (props != null)
            {
                using (var writer = new StreamWriter(await webRequest.GetRequestStreamAsync()))
                {
                    writer.Write(props);
                }
            }

            var response = "";
            var hasException = false;
            Exception exception = null;
            try
            {
                //Wait for RateGate
                _rateGate.WaitToProceed();

                using (WebResponse webResponse = await webRequest.GetResponseAsync())
                {
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            response = await streamReader.ReadLineAsync();
                        }
                    }
                }
            }
            catch (WebException wex)
            {
                hasException = true;
                exception = wex;
                using (HttpWebResponse httpWebResponse = (HttpWebResponse)wex.Response)
                {
                    using (Stream stream = httpWebResponse.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            if (httpWebResponse.StatusCode != HttpStatusCode.InternalServerError)
                            {
                                throw;
                            }
                            response = await streamReader.ReadLineAsync();
                        }
                    }
                }

            }
            var result = LogAndDeserializeException<T>(response);
            if (result.hasException)
                exception = result.exception;
            return PrepareResult(result.item, hasException, result.hasException, exception);

            #region Small
            //var webRequest = PrepareWebRequest(methodName, props, true);
            //if (props != null)
            //    using (var writer = new StreamWriter(await webRequest.GetRequestStreamAsync()))
            //        writer.Write(props);
            //return await MakeWebRequestAsync<T>(webRequest);
            #endregion
        }

        private T MakeWebRequest<T>(HttpWebRequest webRequest) where T : KrakenBaseResult, new()
        {
            var response = "";
            var hasException = false;
            Exception exception = null;
            try
            {
                //Wait for RateGate
                _rateGate.WaitToProceed();

                using (WebResponse webResponse = webRequest.GetResponse())
                {
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            response = streamReader.ReadLine();
                        }
                    }
                }
            }
            catch (WebException wex)
            {
                hasException = true;
                exception = wex;
                using (HttpWebResponse httpWebResponse = (HttpWebResponse)wex.Response)
                {
                    using (Stream stream = httpWebResponse.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            if (httpWebResponse.StatusCode != HttpStatusCode.InternalServerError)
                            {
                                throw;
                            }
                            response = streamReader.ReadLine();
                        }
                    }
                }

            }
            var result = LogAndDeserializeException<T>(response);
            if (result.hasException)
                exception = result.exception;
            return PrepareResult(result.item, hasException, result.hasException, exception);
        }

        private async Task<T> MakeWebRequestAsync<T>(HttpWebRequest webRequest) where T : KrakenBaseResult, new()
        {
            var response = "";
            var hasException = false;
            Exception exception = null;
            try
            {
                //Wait for RateGate
                _rateGate.WaitToProceed();

                using (WebResponse webResponse = await webRequest.GetResponseAsync())
                {
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            response = await streamReader.ReadLineAsync();
                        }
                    }
                }
            }
            catch (WebException wex)
            {
                hasException = true;
                exception = wex;
                using (HttpWebResponse httpWebResponse = (HttpWebResponse)wex.Response)
                {
                    using (Stream stream = httpWebResponse.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            if (httpWebResponse.StatusCode != HttpStatusCode.InternalServerError)
                            {
                                throw wex;
                            }
                            response = await streamReader.ReadLineAsync();
                        }
                    }
                }
            }
            var result = LogAndDeserializeException<T>(response);
            if (result.hasException)
                exception = result.exception;
            return PrepareResult(result.item, hasException, result.hasException, exception);
        }

        private HttpWebRequest PrepareWebRequest(string methodName, string props, bool isSignedRequest)
        {
            Int64 nonce = DateTime.Now.Ticks;
            props = "nonce=" + nonce + props;
            var privatePath = string.Format("/{0}/private/{1}", _version, methodName);
            var publicPath = string.Format("/{0}/public/{1}", _version, methodName);
            var path = isSignedRequest ? privatePath : publicPath;
            var address = _url + path;
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(address);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";

            if (isSignedRequest)
            {
                webRequest.Headers.Add("API-Key", _key);
                byte[] base64DecodedSecred = Convert.FromBase64String(_secret);
                var nonceProps = nonce + Convert.ToChar(0) + props;
                var pathBytes = Encoding.UTF8.GetBytes(path);
                var hash256Bytes = Sha256_hash(nonceProps);
                var z = new byte[pathBytes.Count() + hash256Bytes.Count()];
                pathBytes.CopyTo(z, 0);
                hash256Bytes.CopyTo(z, pathBytes.Count());
                var signature = GetHash(base64DecodedSecred, z);
                webRequest.Headers.Add("API-Sign", Convert.ToBase64String(signature));
            }

            return webRequest;
        }

        private T PrepareResult<T>(T item, bool hasException, bool deserializeHasException, Exception ex) where T : KrakenBaseResult, new()
        {
            if (hasException || deserializeHasException)
            {
                item.ResultType = KrakenGeneralResultType.exception;
                item.Exception = ex;
            }
            else if (item.Errors != null && item.Errors.Count > 0)
            {
                item.ResultType = KrakenGeneralResultType.error;
                item.Exception = new KrakenException(item.Errors, "Kraken has error!");
            }
            else
                item.ResultType = KrakenGeneralResultType.success;
            return item;
        }

        //private string QueryPublic(string methodName, string props = null)
        //{
        //    string address = string.Format("{0}/{1}/public/{2}", _url, _version, methodName);
        //    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(address);
        //    webRequest.ContentType = "application/x-www-form-urlencoded";
        //    webRequest.Method = "POST";

        //    if (props != null)
        //        using (var writer = new StreamWriter(webRequest.GetRequestStream()))
        //            writer.Write(props);

        //    //Make the request
        //    try
        //    {
        //        //Wait for RateGate
        //        _rateGate.WaitToProceed();

        //        using (WebResponse webResponse = webRequest.GetResponse())
        //        {
        //            using (Stream stream = webResponse.GetResponseStream())
        //            {
        //                using (StreamReader streamReader = new StreamReader(stream))
        //                {
        //                    return streamReader.ReadLine();
        //                }
        //            }
        //        }
        //    }
        //    catch (WebException wex)
        //    {
        //        using (HttpWebResponse response = (HttpWebResponse)wex.Response)
        //        {
        //            using (Stream stream = response.GetResponseStream())
        //            {
        //                using (StreamReader streamReader = new StreamReader(stream))
        //                {
        //                    if (response.StatusCode != HttpStatusCode.InternalServerError)
        //                    {
        //                        throw;
        //                    }
        //                    return streamReader.ReadLine();
        //                }
        //            }
        //        }

        //    }
        //}

        //private string QueryPrivate(string methodName, string props = null)
        //{
        //    // generate a 64 bit nonce using a timestamp at tick resolution
        //    Int64 nonce = DateTime.Now.Ticks;
        //    props = "nonce=" + nonce + props;


        //    string path = string.Format("/{0}/private/{1}", _version, methodName);
        //    string address = _url + path;
        //    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(address);
        //    webRequest.ContentType = "application/x-www-form-urlencoded";
        //    webRequest.Method = "POST";
        //    webRequest.Headers.Add("API-Key", _key);


        //    byte[] base64DecodedSecred = Convert.FromBase64String(_secret);

        //    var nonceProps = nonce + Convert.ToChar(0) + props;

        //    var pathBytes = Encoding.UTF8.GetBytes(path);
        //    var hash256Bytes = Sha256_hash(nonceProps);
        //    var z = new byte[pathBytes.Count() + hash256Bytes.Count()];
        //    pathBytes.CopyTo(z, 0);
        //    hash256Bytes.CopyTo(z, pathBytes.Count());

        //    var signature = GetHash(base64DecodedSecred, z);

        //    webRequest.Headers.Add("API-Sign", Convert.ToBase64String(signature));

        //    if (props != null)
        //    {

        //        using (var writer = new StreamWriter(webRequest.GetRequestStream()))
        //        {
        //            writer.Write(props);
        //        }
        //    }

        //    //Make the request
        //    try
        //    {
        //        //Wait for RateGate
        //        _rateGate.WaitToProceed();

        //        using (WebResponse webResponse = webRequest.GetResponse())
        //        {
        //            using (Stream stream = webResponse.GetResponseStream())
        //            {
        //                using (StreamReader streamReader = new StreamReader(stream))
        //                {
        //                    return streamReader.ReadLine();
        //                }
        //            }
        //        }
        //    }
        //    catch (WebException wex)
        //    {
        //        using (HttpWebResponse response = (HttpWebResponse)wex.Response)
        //        {
        //            using (Stream stream = response.GetResponseStream())
        //            {
        //                using (StreamReader streamReader = new StreamReader(stream))
        //                {
        //                    if (response.StatusCode != HttpStatusCode.InternalServerError)
        //                    {
        //                        throw;
        //                    }
        //                    return streamReader.ReadLine();
        //                }
        //            }
        //        }

        //    }
        //}

        private byte[] Sha256_hash(String value)
        {
            using (SHA256 hash = SHA256.Create())
            {
                Encoding enc = Encoding.UTF8;

                Byte[] result = hash.ComputeHash(enc.GetBytes(value));

                return result;
            }
        }

        private byte[] GetHash(byte[] keyByte, byte[] messageBytes)
        {
            using (var hmacsha512 = new HMACSHA512(keyByte))
            {

                Byte[] result = hmacsha512.ComputeHash(messageBytes);

                return result;

            }
        }
        #endregion
    }
}
