﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.TradingPlatform.HitBTC;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class HitBTCService : WebServiceManager, IHitBTCService
    {
        #region Properties
        private readonly string Secret = AppSettingManager.HitBTC_Secret;
        private readonly string Key = AppSettingManager.HitBTC_Key;
        private readonly Uri BaseUrl = new Uri(@"https://api.hitbtc.com/api/2");
        #endregion

        #region Async methods
        public async Task<HitBTCOrder> CancelOrderByClientOrderIdAsync(string clientOrderId)
        {
            CheckRequiredParameter(clientOrderId, "clientOrderId", "CancelOrderByClientOrderIdAsync");
            return await CallAsync<HitBTCOrder>(ApiMethod.DELETE, $"order/{clientOrderId}", true);
        }
        public async Task<List<HitBTCOrder>> CancelOrdersAsync(string symbol = "")
        {
            return await CallAsync<List<HitBTCOrder>>(ApiMethod.DELETE, "order", true, new { symbol });
        }
        public async Task<HitBTCOrder> CreateOrderAsync(string symbol, HitBTCSide side, decimal quanity, HitBTCOrderType? type = null, string clientOrderId = "", HitBTCTimeInForce? timeInForce = null, decimal? price = null, decimal? stopPrice = null, DateTime? expireTime = null, bool? strictValidate = null, bool update = false)
        {
            if (update && !clientOrderId.HasValue())
                CheckRequiredParameter(clientOrderId, "clientOrderId", "CreateOrderAsync");

            var method = update ? ApiMethod.PUT : ApiMethod.POST;
            var action = update ? $"order/{clientOrderId}" : "order";
            return await CallAsync<HitBTCOrder>(method, action, true, new
            {
                symbol,
                side = side.ToString(),
                quanity,
                type = type.HasValue ? type.ToString() : null,
                timeInForce = timeInForce.HasValue ? timeInForce.ToString() : null,
                price,
                stopPrice,
                expireTime,
                strictValidate
            });
        }
        public async Task<HitBTCDepositAddress> DepositAddressAsync(string currency, bool createNew = true)
        {
            CheckRequiredParameter(currency, "currency", "DepositAddressAsync");
            var method = createNew ? ApiMethod.POST : ApiMethod.GET;
            return await CallAsync<HitBTCDepositAddress>(method, $"account/crypto/address/{currency}", true);
        }
        public async Task<List<HitBTCBalance>> GetAccountBalanceAsync()
        {
            return await CallAsync<List<HitBTCBalance>>(ApiMethod.GET, "account/balance", true);
        }
        public async Task<HitBTCOrder> GetActiveOrderByClientOrderIdAsync(string clientOrderId, int? wait = null)
        {
            CheckRequiredParameter(clientOrderId, "clientOrderId", "GetActiveOrderByClientOrderIdAsync");
            return await CallAsync<HitBTCOrder>(ApiMethod.GET, $"order/{clientOrderId}", true, new { wait });
        }
        public async Task<List<HitBTCOrder>> GetActiveOrdersAsync(string symbol = "")
        {
            return await CallAsync<List<HitBTCOrder>>(ApiMethod.GET, "order", true, new { symbol });
        }
        public async Task<List<HitBTCCurrency>> GetAllCurrenciesAsync()
        {
            return await CallAsync<List<HitBTCCurrency>>(ApiMethod.GET, "public/currency");
        }
        public async Task<List<HitBTCSymbol>> GetAllSymbolsAsync()
        {
            return await CallAsync<List<HitBTCSymbol>>(ApiMethod.GET, "public/symbol");
        }
        public async Task<List<HitBTCTicker>> GetAllTickersAsync()
        {
            return await CallAsync<List<HitBTCTicker>>(ApiMethod.GET, "public/ticker");
        }
        public async Task<List<HitBTCCandle>> GetCandlesAsync(string symbol, int? limit = null, HitBTCPeriod? period = null)
        {
            CheckRequiredParameter(symbol, "symbol", "GetCandlesAsync");
            return await CallAsync<List<HitBTCCandle>>(ApiMethod.GET, $"public/candles/{symbol}", false, new { symbol, limit, period = period.HasValue ? period.ToString() : null });
        }
        public async Task<HitBTCCurrency> GetCurrencyAsync(string currency)
        {
            CheckRequiredParameter(currency, "currency", "GetCurrencyAsync");
            return await CallAsync<HitBTCCurrency>(ApiMethod.GET, $"public/currency/{currency}");
        }
        public async Task<List<HitBTCOrderBook>> GetOrderBooksAsync(string symbol, int? limit = null)
        {
            CheckRequiredParameter(symbol, "symbol", "GetOrderBooksAsync");
            return await CallAsync<List<HitBTCOrderBook>>(ApiMethod.GET, $"public/orderbook/{symbol}", false, new { limit });
        }
        public async Task<List<HitBTCOrder>> GetOrdersHistoryAsync(string symbol = "", string clientOrderId = "", DateTime? from = null, DateTime? till = null, int? limit = null, int? offset = null)
        {
            return await CallAsync<List<HitBTCOrder>>(ApiMethod.GET, "history/order", true, new { symbol, clientOrderId, from, till, limit, offset });
        }
        public async Task<HitBTCSymbol> GetSymbolAsync(string symbol)
        {
            CheckRequiredParameter(symbol, "symbol", "GetSymbolAsync");
            return await CallAsync<HitBTCSymbol>(ApiMethod.GET, $"public/symbol/{symbol}");
        }
        public async Task<HitBTCTicker> GetTickerAsync(string symbol)
        {
            CheckRequiredParameter(symbol, "symbol", "GetTickerAsync");
            return await CallAsync<HitBTCTicker>(ApiMethod.GET, $"public/ticker/{symbol}");
        }
        public async Task<List<HitBTCTrade>> GetTradesAsync(string symbol, HitBTCSort? sort = null, HitBTCBy? by = null, string from = "", string till = "", int? limit = null, int? offset = null)
        {
            CheckRequiredParameter(symbol, "symbol", "GetTradesAsync");
            return await CallAsync<List<HitBTCTrade>>(ApiMethod.GET, $"public/trades/{symbol}", false,
                new
                {
                    symbol,
                    sort = sort.HasValue ? sort.ToString() : "",
                    by = by.HasValue ? by.ToString() : "",
                    from,
                    till,
                    limit,
                    offset
                });
        }
        public async Task<List<HitBTCTradeHistory>> GetTradesHistoryAsync(string symbol = "", HitBTCSort? sort = null, HitBTCBy? by = null, DateTime? from = null, DateTime? till = null, int? limit = null, int? offset = null)
        {
            return await CallAsync<List<HitBTCTradeHistory>>(ApiMethod.GET, "history/trades", true,
                new
                {
                    symbol,
                    sort = sort.HasValue ? sort.ToString() : "",
                    by = by.HasValue ? by.ToString() : "",
                    from,
                    till,
                    limit,
                    offset
                });
        }
        public async Task<List<HitBTCBalance>> GetTradingBalanceAsync()
        {
            return await CallAsync<List<HitBTCBalance>>(ApiMethod.GET, "trading/balance", true);
        }
        public async Task<HitBTCTradingCommission> GetTradingCommissionAsync(string symbol)
        {
            CheckRequiredParameter(symbol, "symbol", "GetTradingCommissionAsync");
            return await CallAsync<HitBTCTradingCommission>(ApiMethod.GET, $"trading/fee/{symbol}", true);
        }
        public async Task<List<HitBTCTransactionHistory>> GetTransactionsHistoryAsync()
        {
            return await CallAsync<List<HitBTCTransactionHistory>>(ApiMethod.GET, "account/transactions", true);
        }
        public async Task<HitBTCTransactionHistory> GetTransactionByIdAsync(string id)
        {
            CheckRequiredParameter(id, "id", "GetTransactionByIdAsync");
            return await CallAsync<HitBTCTransactionHistory>(ApiMethod.GET, "account/transactions", true);
        }
        public async Task<HitBTCWithdrawTransfer> TransferAsync(string currency, decimal amount, HitBTCTransferType type)
        {
            CheckRequiredParameter(currency, "currency", "TransferAsync");
            return await CallAsync<HitBTCWithdrawTransfer>(ApiMethod.POST, "account/transfer", true, new { currency, amount, type = type.ToString() });
        }
        public async Task<HitBTCWithdrawTransfer> WithdrawAsync(string currency, decimal amount, string address, string paymentId = "", decimal? networkFee = null, bool? includeFee = null, bool? autoCommit = null)
        {
            CheckRequiredParameter(currency, "currency", "WithdrawAsync");
            CheckRequiredParameter(address, "address", "WithdrawAsync");
            if (amount <= 0)
            {
                var error = new ArgumentException($"HitBTC WithdrawAsync: {amount} is zero!");
                error.LogError();
                throw error;
            }

            return await CallAsync<HitBTCWithdrawTransfer>(ApiMethod.POST, "account/crypto/withdraw", true, new
            {
                currency,
                amount,
                address,
                paymentId,
                networkFee,
                includeFee,
                autoCommit
            });
        }
        public async Task<HitBTCResult> WithdrawCommitAsync(string id)
        {
            CheckRequiredParameter(id, "id", "WithdrawCommitAsync");
            return await CallAsync<HitBTCResult>(ApiMethod.PUT, $"account/crypto/withdraw/{id}", true);
        }
        public async Task<HitBTCResult> WithdrawRollbackAsync(string id)
        {
            CheckRequiredParameter(id, "id", "WithdrawRollbackAsync");
            return await CallAsync<HitBTCResult>(ApiMethod.DELETE, $"account/crypto/withdraw/{id}", true);
        }
        #endregion

        #region Helpers
        private async Task<T> CallAsync<T>(ApiMethod method, string action, bool isAuthenticatedRequest = false, object obj = null) where T : new()
        {
            var restClient = new RestClient(BaseUrl);
            if (isAuthenticatedRequest)
                restClient.Authenticator = new HttpBasicAuthenticator(Key, Secret);

            var restRequest = new RestRequest(action, GetMethod(method));
            if (obj != null)
            {
                var validProperties = obj.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                foreach (var property in validProperties)
                {
                    var value = property.GetValue(obj)?.ToString() ?? "";
                    if (value.HasValue())
                        restRequest.AddParameter(property.Name, value);
                }
            }

            var searchOutputs = await restClient.ExecuteTaskAsync(restRequest).ConfigureAwait(false);
            return LogAndDeserialize<T>(searchOutputs.Content);
        }
        private Method GetMethod(ApiMethod method)
        {
            return (Method)Enum.Parse(typeof(Method), method.ToString());
        }
        private void CheckRequiredParameter(string parameter, string parameterName, string methodName)
        {
            if (!parameter.HasValue())
            {
                var error = new ArgumentException($"HitBTC {methodName}: {parameterName} not presented!");
                error.LogError();
                throw error;
            }
        }
        #endregion
    }
}
