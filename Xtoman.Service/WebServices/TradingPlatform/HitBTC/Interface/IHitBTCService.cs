﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.TradingPlatform.HitBTC;

namespace Xtoman.Service.WebServices
{
    public interface IHitBTCService
    {
        #region Publics
        /// <summary>
        /// Return the specified currency symbol (currency pair). The first listed currency of a symbol is called the base currency, and the second currency is called the quote currency. The currency pair indicates how much of the quote currency is needed to purchase one unit of the base currency.
        /// </summary>
        /// <param name="symbol">Symbol identifier. use the symbol. Example: ETHBTC, ethbtc</param>
        /// <returns></returns>
        Task<HitBTCSymbol> GetSymbolAsync(string symbol);

        /// <summary>
        /// Return the actual list of currency symbols (currency pairs) traded on HitBTC exchange. The first listed currency of a symbol is called the base currency, and the second currency is called the quote currency. The currency pair indicates how much of the quote currency is needed to purchase one unit of the base currency.
        /// </summary>
        /// <returns></returns>
        Task<List<HitBTCSymbol>> GetAllSymbolsAsync();

        /// <summary>
        /// Return the specified currency, tokens, ICO etc.
        /// </summary>
        /// <param name="currency">Currency identifier. simply use the currency. Example: ETC, ETH, btc</param>
        /// <returns></returns>
        Task<HitBTCCurrency> GetCurrencyAsync(string currency);

        /// <summary>
        /// Return the actual list of available currencies, tokens, ICO etc.
        /// </summary>
        /// <returns></returns>
        Task<List<HitBTCCurrency>> GetAllCurrenciesAsync();

        /// <summary>
        /// Return specified ticker information
        /// </summary>
        /// <param name="symbol">Symbol identifier. use the symbol. Example: ETHBTC, ethbtc</param>
        /// <returns></returns>
        Task<HitBTCTicker> GetTickerAsync(string symbol);

        /// <summary>
        /// Return all tickers information
        /// </summary>
        /// <returns></returns>
        Task<List<HitBTCTicker>> GetAllTickersAsync();

        /// <summary>
        /// Get all trades by symbol
        /// </summary>
        /// <param name="symbol">Symbol identifier. use the symbol. Example: ETHBTC, ethbtc</param>
        /// <param name="sort">Optional: Sort direction. Default = Descending</param>
        /// <param name="by">Optional: Filtration definition. Accepted values: id, timestamp. Default = Timestamp</param>
        /// <param name="from">Optional: Number or Datetime</param>
        /// <param name="till">Optional: Number or Datetime</param>
        /// <param name="limit">Optional: Number</param>
        /// <param name="offset">Optional: Number</param>
        /// <returns></returns>
        Task<List<HitBTCTrade>> GetTradesAsync(string symbol, HitBTCSort? sort = null, HitBTCBy? by = null, string from = "", string till = "", int? limit = null, int? offset = null);

        /// <summary>
        /// An order book is an electronic list of buy and sell orders for a specific symbol, organized by price level.
        /// </summary>
        /// <param name="symbol">Symbol identifier. use the symbol. Example: ETHBTC, ethbtc</param>
        /// <param name="limit">Optional: Limit of orderbook levels, default 100. Set 0 to view full orderbook levels</param>
        /// <returns></returns>
        Task<List<HitBTCOrderBook>> GetOrderBooksAsync(string symbol, int? limit = null);

        /// <summary>
        /// An candles used for OHLC a specific symbol.
        /// </summary>
        /// <param name="symbol">Symbol identifier. use the symbol. Example: ETHBTC, ethbtc</param>
        /// <param name="limit">Limit of candles, default = 100.</param>
        /// <param name="period">One of: M1 (one minute), M3, M5, M15, M30, H1, H4, D1, D7, 1M (one month). Default is M30 Async(30 minutes).</param>
        /// <returns></returns>
        Task<List<HitBTCCandle>> GetCandlesAsync(string symbol, int? limit = null, HitBTCPeriod? period = null);
        #endregion

        #region Trading
        /// <summary>
        /// Return list of active orders.
        /// </summary>
        /// <param name="symbol">Optional parameter to filter active orders by symbol</param>
        /// <returns></returns>
        Task<List<HitBTCOrder>> GetActiveOrdersAsync(string symbol = "");

        /// <summary>
        /// Get Active order by clientOrderId
        /// </summary>
        /// <param name="clientOrderId">Client order id</param>
        /// <param name="wait">Optional parameter. Time in milliseconds. Max 60000. Default none. Use long polling request, if order is filled, canceled or expired return order info instantly, or after specified wait time returns actual order info</param>
        /// <returns></returns>
        Task<HitBTCOrder> GetActiveOrderByClientOrderIdAsync(string clientOrderId, int? wait = null);

        /// <summary>
        /// <para><strong>Price accuracy and quantity.</strong></para>
        /// <para>Symbol config contain <em>tickSize</em> parameter which means that <em>price</em> should be divide by <em>tickSize</em> without residue.Quantity should be divide by <em>quantityIncrement</em> without residue.By default, if <em>strictValidate</em> not enabled, server round half down price and quantity for <em>tickSize</em> and <em>quantityIncrement.</em> 
        /// Example for ETHBTC: tickSize = '0.000001' then price '0.046016' - valid, '0.0460165' - invalid</para>
        /// <para><strong>Fees</strong></para>
        /// <para>Fee charged in symbol feeCurrency. Maker-taker fees offer a transaction rebate provideLiquidityRate to those who provide liquidity (the market maker), while charging customers who take that liquidity takeLiquidityRate. 
        /// For buy orders you must have enough available balance including fees.Available balance > price* quantity * (1 + takeLiquidityRate)</para>
        /// <para><strong>Result order status</strong></para>
        /// <para>For orders with timeInForce IOC or FOK REST API returns final order state: filled or expired.
        /// If order can be instantly executed, then REST API return filled or partiallyFilled order's info.</para>
        /// </summary>
        /// <param name="symbol">Symbol identifier. use the symbol. Example: ETHBTC, ethbtc</param>
        /// <param name="side">sell or buy</param>
        /// <param name="quantity">Order quantity</param>
        /// <param name="type">Optional. Default - limit. One of: limit, market, stopLimit, stopMarket</param>
        /// <param name="clientOrderId">Optional parameter, if skipped - will be generated by server. Uniqueness must be guaranteed within a single trading day, including all active orders.</param>
        /// <param name="timeInForce">Optional. Default - GDC. One of: GTC, IOC, FOK, Day, GTD</param>
        /// <param name="price">Order price. Required for limit types.</param>
        /// <param name="stopPrice">Required for stop types.</param>
        /// <param name="expireTime">Required for GTD timeInForce.</param>
        /// <param name="strictValidate">Price and quantity will be checked that they increment within tick size and quantity step. See symbol tickSize and quantityIncrement</param>
        /// <param name="update">if set true clientOrderId must insert and make PUT request otherwise make POST request</param>
        /// <returns></returns>
        Task<HitBTCOrder> CreateOrderAsync(string symbol, HitBTCSide side, decimal quantity, HitBTCOrderType? type = null, string clientOrderId = "", HitBTCTimeInForce? timeInForce = null, decimal? price = null, decimal? stopPrice = null, DateTime? expireTime = null, bool? strictValidate = null, bool update = false);

        /// <summary>
        /// Cancel all active orders, or all active orders for specified symbol.
        /// </summary>
        /// <param name="symbol">Optional parameter to filter active orders by symbol</param>
        /// <returns></returns>
        Task<List<HitBTCOrder>> CancelOrdersAsync(string symbol = "");

        /// <summary>
        /// Cancel specified order by clientOrderId
        /// </summary>
        /// <param name="clientOrderId"></param>
        /// <returns></returns>
        Task<HitBTCOrder> CancelOrderByClientOrderIdAsync(string clientOrderId);

        /// <summary>
        /// Get personal trading commission rate.
        /// </summary>
        /// <param name="symbol">Symbol identifier. use the symbol. Example: ETHBTC, ethbtc</param>
        /// <returns></returns>
        Task<HitBTCTradingCommission> GetTradingCommissionAsync(string symbol);
        #endregion

        #region Trading history
        //Please note, that trading history may be updated with delay up to 30 seconds, with mean delay around 1 second

        /// <summary>
        /// Orders history, Note: All orders older then 24 hours without trades are deleted.
        /// <para>Please note, that trading history may be updated with delay up to 30 seconds, with mean delay around 1 second</para>
        /// </summary>
        /// <param name="symbol">Optional parameter to filter active orders by symbol</param>
        /// <param name="clientOrderId">If set, other parameters will be ignored. Without limit and pagination</param>
        /// <param name="from"></param>
        /// <param name="till"></param>
        /// <param name="limit">Default 100</param>
        /// <param name="offset"></param>
        /// <returns></returns>
        Task<List<HitBTCOrder>> GetOrdersHistoryAsync(string symbol = "", string clientOrderId = "", DateTime? from = null, DateTime? till = null, int? limit = null, int? offset = null);

        /// <summary></summary>
        /// <param name="symbol">Optional parameter to filter active orders by symbol</param>
        /// <param name="sort">DESC or ASC. Default value DESC</param>
        /// <param name="by">timestamp by default, or id</param>
        /// <param name="from"></param>
        /// <param name="till"></param>
        /// <param name="limit">Default 100</param>
        /// <param name="offset"></param>
        /// <returns></returns>
        Task<List<HitBTCTradeHistory>> GetTradesHistoryAsync(string symbol = "", HitBTCSort? sort = null, HitBTCBy? by = null, DateTime? from = null, DateTime? till = null, int? limit = null, int? offset = null);
        #endregion

        #region Account
        /// <summary>
        /// Account balance
        /// </summary>
        /// <returns></returns>
        Task<List<HitBTCBalance>> GetAccountBalanceAsync();

        /// <summary>
        /// List of currencies and amount of available and reserve for trade.
        /// </summary>
        /// <returns></returns>
        Task<List<HitBTCBalance>> GetTradingBalanceAsync();

        /// <summary>
        /// Deposit address
        /// </summary>
        /// <param name="currency">Currency identifier. simply use the currency. Example: ETC, ETH, btc</param>
        /// <param name="createNew">Optional, Default is true, For creating new address make POST request otherwise for current address make GET request</param>
        /// <returns></returns>
        Task<HitBTCDepositAddress> DepositAddressAsync(string currency, bool createNew = true);

        /// <summary>
        /// Withdraw crypto
        /// </summary>
        /// <param name="currency">Currency identifier. simply use the currency. Example: ETC, ETH, btc</param>
        /// <param name="amount">The amount that will be sent to the specified address</param>
        /// <param name="address">The address that the amount will be sent to</param>
        /// <param name="paymentId">Optional parameter for PaymentId, Memo, Tag and etc.</param>
        /// <param name="networkFee">Optional parameter. Too low and too high commission value will be rounded to valid values.</param>
        /// <param name="includeFee">Default false. If set true then total will be spent the specified amount, fee and networkFee will be deducted from the amount</param>
        /// <param name="autoCommit">Default true. If set false then you should commit or rollback transaction in an hour. Used in two phase commit schema.</param>
        /// <returns></returns>
        Task<HitBTCWithdrawTransfer> WithdrawAsync(string currency, decimal amount, string address, string paymentId = "", decimal? networkFee = null, bool? includeFee = null, bool? autoCommit = null);

        /// <summary>
        /// PUT to Commit withdraw by id if autoCommit was false
        /// </summary>
        /// <param name="id">Withdraw id</param>
        /// <returns></returns>
        Task<HitBTCResult> WithdrawCommitAsync(string id);

        /// <summary>
        /// Delete to Rollback withdraw by id if autoCommit was false
        /// </summary>
        /// <param name="id">Withdraw id</param>
        /// <returns></returns>
        Task<HitBTCResult> WithdrawRollbackAsync(string id);

        /// <summary>
        /// Transfer money between trading and account
        /// </summary>
        /// <param name="currency">Currency identifier. simply use the currency. Example: ETC, ETH, btc</param>
        /// <param name="amount">The amount that will transfer between balances</param>
        /// <param name="type">bankToExchange or exchangeToBank</param>
        /// <returns></returns>
        Task<HitBTCWithdrawTransfer> TransferAsync(string currency, decimal amount, HitBTCTransferType type);

        /// <summary>
        /// Get transactions history
        /// </summary>
        /// <returns></returns>
        Task<List<HitBTCTransactionHistory>> GetTransactionsHistoryAsync();

        /// <summary>
        /// Get transaction by transaction id
        /// </summary>
        /// <param name="id">transaction id</param>
        /// <returns></returns>
        Task<HitBTCTransactionHistory> GetTransactionByIdAsync(string id);
        #endregion
    }
}
