﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Xtoman.Domain.WebServices.TradingPlatform.Bitfinex;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class BitfinexService : WebServiceManager, IBitfinexService
    {
        #region Properties
        private DateTime epoch = new DateTime(1970, 1, 1);

        private readonly HMACSHA384 hashMaker = new HMACSHA384(Encoding.UTF8.GetBytes(AppSettingManager.Bitfinex_Secret));
        private readonly string Key = AppSettingManager.Bitfinex_Key;
        private int nonce = 0;
        private string Nonce
        {
            get
            {
                if (nonce == 0)
                {
                    nonce = (int)(DateTime.UtcNow - epoch).TotalSeconds;
                }
                return (nonce++).ToString();
            }
        }
        #endregion

        #region Sync methods

        #region Public Endpoints
        public BitfinexTicker Ticker(BitfinexSymbol symbol)
        {
            string response = SendGetRequest("ticker", symbol.ToDisplay());
            return LogAndDeserialize<BitfinexTicker>(response);
        }

        public List<BitfinexStatsItem> Stats(BitfinexSymbol symbol)
        {
            string response = SendGetRequest("stats", symbol.ToDisplay());
            return LogAndDeserialize<List<BitfinexStatsItem>>(response);
        }

        public BitfinexFundingbook Fundingbook(BitfinexMethod currency, int? limitBids = null, int? limitAsks = null)
        {
            string response = SendGetRequest("lendbook", currency.ToDisplay(DisplayProperty.ShortName), new { limit_bids = limitBids, limit_asks = limitAsks });
            return LogAndDeserialize<BitfinexFundingbook>(response);
        }

        public BitfinexOrderbook Orderbook(BitfinexSymbol symbol, int? limitBids = null, int? limitAsks = null, bool? groupByPrice = null)
        {
            var group = groupByPrice.HasValue ? groupByPrice.Value ? 1 : 0 : new int?();
            string response = SendGetRequest("book", symbol.ToDisplay(), new { limit_bids = limitBids, limit_asks = limitAsks, group = group });
            return LogAndDeserialize<BitfinexOrderbook>(response);
        }

        public List<BitfinexTrade> Trades(BitfinexSymbol symbol, DateTime? fromDateTime = null, int? limitTrades = null)
        {
            var timestamp = fromDateTime.HasValue ? fromDateTime.Value.ConvertToUnixTimestamp() : new long?();
            string response = SendGetRequest("trades", symbol.ToDisplay(), new { timestamp = timestamp, limit_trades = limitTrades });
            return LogAndDeserialize<List<BitfinexTrade>>(response);
        }

        public List<BitfinexLend> Lends(BitfinexMethod currency, DateTime? fromDateTime = null, int? limitLends = null)
        {
            var timestamp = fromDateTime.HasValue ? fromDateTime.Value.ConvertToUnixTimestamp() : new long?();
            string response = SendGetRequest("lends", currency.ToDisplay(DisplayProperty.ShortName), new { timestamp = timestamp, limit_lends = limitLends });
            return LogAndDeserialize<List<BitfinexLend>>(response);
        }

        public List<string> Symbols()
        {
            string response = SendGetRequest("symbols");
            return LogAndDeserialize<List<string>>(response);
        }

        public List<BitfinexSymbolDetail> SymbolsDetails()
        {
            string response = SendGetRequest("symbols_details");
            return LogAndDeserialize<List<BitfinexSymbolDetail>>(response);
        }
        #endregion

        #region Account
        public BitfinexAccountInfos AccountInfos()
        {
            var request = new BitfinexAccountInfosRequest(Nonce);
            string response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<BitfinexAccountInfos>(response);
        }
        public BitfinexAccountFees AccountFees()
        {
            var request = new BitfinexAccountFeesRequest(Nonce);
            string response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<BitfinexAccountFees>(response);
        }
        public BitfinexSummary Summary()
        {
            var request = new BitfinexSummaryRequest(Nonce);
            string response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<BitfinexSummary>(response);
        }
        public BitfinexDeposit Deposit(BitfinexMethod method, BitfinexWalletType walletType, int renew = 1)
        {
            var request = new BitfinexDepositRequest(Nonce, method, walletType, renew);
            string response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<BitfinexDeposit>(response);
        }
        public BitfinexKeyPermissions KeyPermissions()
        {
            var request = new BitfinexKeyPermissionsRequest(Nonce);
            string response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<BitfinexKeyPermissions>(response);
        }
        public BitfinexMarginInformation MarginInformation()
        {
            var request = new BitfinexMarginInformationRequest(Nonce);
            string response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<BitfinexMarginInformation>(response);
        }
        public BitfinexTransferBetweenWallets TransferBetweenWallets(BitfinexWalletType fromWalletType, BitfinexWalletType toWalletType, BitfinexMethod currency, decimal amount)
        {
            var request = new BitfinexTransferBetweenWalletsRequest(Nonce, fromWalletType, toWalletType, currency, amount);
            string response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<BitfinexTransferBetweenWallets>(response);
        }
        public BitfinexWithdrawal Withdrawal(BitfinexWithdrawalInput input)
        {
            var request = new BitfinexWithdrawalRequest(Nonce, input);
            string response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<BitfinexWithdrawal>(response);
        }
        public List<BitfinexBalance> Balances()
        {
            var request = new BitfinexBalancesRequest(Nonce);
            var response = SendAuthenticatedRequest(request, "GET");
            return LogAndDeserialize<List<BitfinexBalance>>(response);
        }
        #endregion

        #region Orders
        public BitfinexNewOrder NewOrder(BitfinexSymbol symbol, float amount, float price, BitfinexOrderExchange exchange, BitfinexOrderSide side, BitfinexOrderType type)
        {
            var request = new BitfinexOrderRequest(Nonce, symbol, amount, price, exchange, side, type);
            var response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<BitfinexNewOrder>(response);
        }
        public BitfinexNewOrder NewBuyOrder(BitfinexSymbol symbol, float amount, float price, BitfinexOrderExchange exchange, BitfinexOrderType type)
        {
            return NewOrder(symbol, amount, price, exchange, BitfinexOrderSide.Buy, type);
        }
        public BitfinexNewOrder NewSellOrder(BitfinexSymbol symbol, float amount, float price, BitfinexOrderExchange exchange, BitfinexOrderType type)
        {
            return NewOrder(symbol, amount, price, exchange, BitfinexOrderSide.Sell, type);
        }
        public BitfinexCancelOrder CancelOrder(long orderId)
        {
            var request = new BitfinexCancelOrderRequest(Nonce, orderId);
            var response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<BitfinexCancelOrder>(response);
        }
        public BitfinexCancelAllOrders CancelAllOrders()
        {
            var request = new BitfinexCancelAllOrdersRequest(Nonce);
            string response = SendAuthenticatedRequest(request, "GET");
            return new BitfinexCancelAllOrders(response);
        }
        public BitfinexOrderStatus GetOrderStatus(long orderId)
        {
            var request = new BitfinexOrderStatusRequest(Nonce, orderId);
            string response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<BitfinexOrderStatus>(response);
        }
        public List<BitfinexOrderStatus> GetActiveOrders()
        {
            var request = new BitfinexActiveOrdersRequest(Nonce);
            string response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<List<BitfinexOrderStatus>>(response);
        }

        public BitfinexNewOrder NewOrder(BitfinexOrderInput newOrder)
        {
            return NewOrder(newOrder.Symbol, newOrder.Amount, newOrder.Price, newOrder.Exchange, newOrder.Side, newOrder.Type);
        }
        public List<BitfinexNewOrder> MultipleNewOrders(List<BitfinexOrderInput> newOrders)
        {
            var request = new BitfinexMultipleOrderRequest(Nonce, newOrders);
            var response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<List<BitfinexNewOrder>>(response);
        }
        public string CancelMultipleOrders(long[] orderIds)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Positions
        public List<BitfinexActivePosition> GetActivePositions()
        {
            var request = new BitfinexActivePositionRequest(Nonce);
            string response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<List<BitfinexActivePosition>>(response);
        }
        #endregion

        #region Historical data
        public List<BitfinexBalanceHistory> BalanceHistory(BitfinexMethod currency, DateTime since, DateTime until, int limit, BitfinexWalletType walletType)
        {
            var request = new BitfinexBalanceHistoryRequest(Nonce, currency, since, until, limit, walletType);
            string response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<List<BitfinexBalanceHistory>>(response);
        }

        public List<BitfinexDepositWithdrawalHistory> DepositWithdrawalHistory(BitfinexMethod currency, DateTime since, DateTime until, int limit, BitfinexWalletType walletType)
        {
            var request = new BitfinexDepositWithdrawalHistoryRequest(Nonce, currency, since, until, limit, walletType);
            string response = SendAuthenticatedRequest(request, "POST");
            return LogAndDeserialize<List<BitfinexDepositWithdrawalHistory>>(response);
        }
        #endregion

        #endregion

        #region Async methods

        #endregion

        #region Helpers
        private String GetHexString(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder(bytes.Length * 2);
            foreach (byte b in bytes)
            {
                sb.Append(String.Format("{0:x2}", b));
            }
            return sb.ToString();
        }
        private string SendGetRequest(string endPoint, string segmentParameter = "", object queryParameters = null)
        {
            var queryItems = new List<string>();
            if (queryParameters != null)
            {
                var validProperties = queryParameters.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                foreach (var property in validProperties)
                {
                    var value = property.GetValue(queryParameters)?.ToString() ?? "";
                    if (value.HasValue())
                        queryItems.Add(property.Name + "=" + value);
                }
            }
            string query = "";
            if (queryItems.Count > 0) query = "?" + string.Join("&", queryItems);
            var wr = WebRequest.Create("https://api.bitfinex.com/v1/" + endPoint + "/" + segmentParameter + query) as HttpWebRequest;
            wr.Method = "GET";
            string response = null;
            try
            {
                var resp = wr.GetResponse() as HttpWebResponse;
                var sr = new StreamReader(resp.GetResponseStream());
                response = sr.ReadToEnd();
                sr.Close();
            }
            catch (WebException ex)
            {
                var sr = new StreamReader(ex.Response.GetResponseStream());
                response = sr.ReadToEnd();
                sr.Close();
                throw new BitfinexException(ex, response);
            }
            return response;
        }
        private string SendAuthenticatedRequest(BitfinexGenericRequest request, string httpMethod)
        {
            var jsonSerializerSettings = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            string json = JsonConvert.SerializeObject(request, Formatting.None, jsonSerializerSettings);
            string json64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(json));
            byte[] data = Encoding.UTF8.GetBytes(json64);
            byte[] hash = hashMaker.ComputeHash(data);
            string signature = GetHexString(hash);

            HttpWebRequest wr = WebRequest.Create("https://api.bitfinex.com" + request.request) as HttpWebRequest;
            wr.Headers.Add("X-BFX-APIKEY", Key);
            wr.Headers.Add("X-BFX-PAYLOAD", json64);
            wr.Headers.Add("X-BFX-SIGNATURE", signature);
            wr.Method = httpMethod;

            string response = null;
            try
            {
                HttpWebResponse resp = wr.GetResponse() as HttpWebResponse;
                StreamReader sr = new StreamReader(resp.GetResponseStream());
                response = sr.ReadToEnd();
                sr.Close();
            }
            catch (WebException ex)
            {
                StreamReader sr = new StreamReader(ex.Response.GetResponseStream());
                response = sr.ReadToEnd();
                sr.Close();
                var bitfinexException = new BitfinexException(ex, response);
                bitfinexException.LogError();
                throw bitfinexException;
            }
            return response;
        }
        #endregion
    }
}
