﻿using System;
using System.Collections.Generic;
using Xtoman.Domain.WebServices.TradingPlatform.Bitfinex;

namespace Xtoman.Service.WebServices
{
    public interface IBitfinexService
    {
        #region Public Endpoints
        /// <summary>
        /// The ticker is a high level overview of the state of the market. It shows you the current best bid and ask, as well as the last trade price. It also includes information such as daily volume and how much the price has moved over the last day.
        /// </summary>
        /// <param name="symbol">Trading symbol, like: "btcusd", "ltcbtc", ...</param>
        /// <returns></returns>
        BitfinexTicker Ticker(BitfinexSymbol symbol);
        /// <summary>
        /// Various statistics about the requested pair.
        /// </summary>
        /// <param name="symbol">Trading symbol, like: "btcusd", "ltcbtc", ...</param>
        /// <returns></returns>
        List<BitfinexStatsItem> Stats(BitfinexSymbol symbol);
        /// <summary>
        /// Get the full margin funding book
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="limitBids">Default = 50, Limit the number of funding bids returned. May be 0 in which case the array of bids is empty</param>
        /// <param name="limitAsks">Default = 50, Limit the number of funding offers returned. May be 0 in which case the array of asks is empty</param>
        /// <returns></returns>
        BitfinexFundingbook Fundingbook(BitfinexMethod currency, int? limitBids = null, int? limitAsks = null);
        /// <summary>
        /// Get the full order book.
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="limitBids">Default = 50, Limit the number of bids returned. May be 0 in which case the array of bids is empty</param>
        /// <param name="limitAsks">Default = 50, Limit the number of asks returned. May be 0 in which case the array of asks is empty</param>
        /// <param name="groupByPrice">Default = true, If true or 1, orders are grouped by price in the orderbook. If false or 0, orders are not grouped and sorted individually</param>
        /// <returns></returns>
        BitfinexOrderbook Orderbook(BitfinexSymbol symbol, int? limitBids = null, int? limitAsks = null, bool? groupByPrice = null);
        /// <summary>
        /// Get a list of the most recent trades for the given symbol.
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="fromDateTime">Only show trades at or after this timestamp</param>
        /// <param name="limitTrades">Default = 50, Limit the number of trades returned. Must be >= 1</param>
        /// <returns></returns>
        List<BitfinexTrade> Trades(BitfinexSymbol symbol, DateTime? fromDateTime = null, int? limitTrades = null);
        /// <summary>
        /// Get a list of the most recent funding data for the given currency: total amount provided and Flash Return Rate (in % by 365 days) over time.
        /// </summary>
        /// <param name="currency"></param>
        /// <param name="fromDateTime">Only show data at or after this timestamp</param>
        /// <param name="limitLends">Limit the amount of funding data returned. Must be >= 1</param>
        /// <returns></returns>
        List<BitfinexLend> Lends(BitfinexMethod currency, DateTime? fromDateTime = null, int? limitLends = null);
        /// <summary>
        /// A list of symbol names.
        /// https://api.bitfinex.com/v1/symbols
        /// </summary>
        /// <returns></returns>
        List<string> Symbols();
        /// <summary>
        /// Get a list of valid symbol IDs and the pair details.
        /// https://api.bitfinex.com/v1/symbols_details
        /// </summary>
        /// <returns></returns>
        List<BitfinexSymbolDetail> SymbolsDetails();
        #endregion

        #region Account
        /// <summary>
        /// Return information about your account (trading fees)
        /// </summary>
        /// <returns>BitfinexAccountInfos object</returns>
        BitfinexAccountInfos AccountInfos();
        /// <summary>
        /// See the fees applied to your withdrawals
        /// </summary>
        /// <returns></returns>
        BitfinexAccountFees AccountFees();
        /// <summary>
        /// Returns a 30-day summary of your trading volume and return on margin funding.
        /// </summary>
        /// <returns></returns>
        BitfinexSummary Summary();
        /// <summary>
        /// Return your deposit address to make a new deposit.
        /// </summary>
        /// <param name="method">Method of deposit (methods accepted: “bitcoin”, “litecoin”, “ethereum”, “tetheruso", "ethereumc", "zcash", "monero", "iota", "bcash"). | BitfinexMethod: "bitcoin", "litecoin", ... "tetheruso" deposit method works only for verified accounts</param>
        /// <param name="walletType">Wallet to deposit in (accepted: “trading”, “exchange”, “deposit”). Your wallet needs to already exist | BitfinexWalletType: "trading","deposit","exchange"</param>
        /// <param name="renew">Default is 0. If set to 1, will return a new unused deposit address</param>
        /// <returns>BitfinexDeposit object include The deposit address if didn't return an error</returns>
        BitfinexDeposit Deposit(BitfinexMethod method, BitfinexWalletType walletType, int renew = 1);
        /// <summary>
        /// Check the permissions of the key being used to generate this request.
        /// </summary>
        /// <returns></returns>
        BitfinexKeyPermissions KeyPermissions();
        /// <summary>
        /// See your trading wallet information for margin trading.
        /// </summary>
        /// <returns></returns>
        BitfinexMarginInformation MarginInformation();
        /// <summary>
        /// Get wallet balances. Ratelimit: 20 req/min
        /// </summary>
        /// <returns>List of BitfinexBalance object</returns>
        List<BitfinexBalance> Balances();
        /// <summary>
        /// Allow you to move available balances between your wallets.
        /// </summary>
        /// <param name="fromWalletType">From wallet type: Deposit, Trading, Exchange</param>
        /// <param name="toWalletType">To wallet type: Deposit, Trading, Exchange</param>
        /// <param name="currency">"BTC", "USD", "LTC", ...</param>
        /// <param name="amount">Amount to transfer</param>
        /// <returns></returns>
        BitfinexTransferBetweenWallets TransferBetweenWallets(BitfinexWalletType fromWalletType, BitfinexWalletType toWalletType, BitfinexMethod currency, decimal amount);
        /// <summary>
        /// Allow you to request a withdrawal from one of your wallet.
        /// </summary>
        /// <param name="input">For ALL withdrawals, you must supply the Withdrawal Type, the Wallet and the Amount. For CRYPTOCURRENCY withdrawals, you will also supply the Address where the funds should be sent.If it is a monero transaction, you can also include a Payment ID. For WIRE WITHDRAWALS, you will need to fill in the beneficiary bank information. In some cases your bank will require the use of an intermediary bank, if this is the case, please supply those fields as well. When submitting a Ripple Withdrawal via API, you should include tag in the payment_id field</param>
        /// <returns>Withdrawal Id</returns>
        BitfinexWithdrawal Withdrawal(BitfinexWithdrawalInput input);
        #endregion

        #region Order
        /// <summary>
        /// New Order. Submit a new Order
        /// </summary>
        /// <param name="symbol">REQUIRED | The name of the symbol (see /symbols).</param>
        /// <param name="amount">REQUIRED | Order size: how much you want to buy or sell</param>
        /// <param name="price">REQUIRED | Price to buy or sell at. Must be positive. Use random number for market orders.</param>
        /// <param name="side">REQUIRED | Either “buy” or “sell”.</param>
        /// <param name="type">REQUIRED | Either “market” / “limit” / “stop” / “trailing-stop” / “fill-or-kill” / “exchange market” / “exchange limit” / “exchange stop” / “exchange trailing-stop” / “exchange fill-or-kill”. (type starting by “exchange ” are exchange orders, others are margin trading orders)</param>
        /// <returns></returns>
        BitfinexNewOrder NewOrder(BitfinexSymbol symbol, float amount, float price, BitfinexOrderExchange exchange, BitfinexOrderSide side, BitfinexOrderType type);
        /// <summary>
        /// New Buy Order. Submit a new buy Order
        /// </summary>
        /// <param name="symbol">REQUIRED | The name of the symbol (see /symbols).</param>
        /// <param name="amount">REQUIRED | Order size: how much you want to buy or sell</param>
        /// <param name="price">REQUIRED | Price to buy or sell at. Must be positive. Use random number for market orders.</param>
        /// <param name="side">REQUIRED | Either “buy” or “sell”.</param>
        /// <param name="type">REQUIRED | Either “market” / “limit” / “stop” / “trailing-stop” / “fill-or-kill” / “exchange market” / “exchange limit” / “exchange stop” / “exchange trailing-stop” / “exchange fill-or-kill”. (type starting by “exchange ” are exchange orders, others are margin trading orders)</param>
        /// <returns></returns>
        BitfinexNewOrder NewBuyOrder(BitfinexSymbol symbol, float amount, float price, BitfinexOrderExchange exchange, BitfinexOrderType type);
        /// <summary>
        /// New Sell Order. Submit a new sell Order
        /// </summary>
        /// <param name="symbol">REQUIRED | The name of the symbol (see /symbols).</param>
        /// <param name="amount">REQUIRED | Order size: how much you want to buy or sell</param>
        /// <param name="price">REQUIRED | Price to buy or sell at. Must be positive. Use random number for market orders.</param>
        /// <param name="side">REQUIRED | Either “buy” or “sell”.</param>
        /// <param name="type">REQUIRED | Either “market” / “limit” / “stop” / “trailing-stop” / “fill-or-kill” / “exchange market” / “exchange limit” / “exchange stop” / “exchange trailing-stop” / “exchange fill-or-kill”. (type starting by “exchange ” are exchange orders, others are margin trading orders)</param>
        /// <returns></returns>
        BitfinexNewOrder NewSellOrder(BitfinexSymbol symbol, float amount, float price, BitfinexOrderExchange exchange, BitfinexOrderType type);
        /// <summary>
        /// Cancel an order.
        /// </summary>
        /// <param name="orderId">The order ID given by NewOrder method</param>
        /// <returns></returns>
        BitfinexCancelOrder CancelOrder(long orderId);
        /// <summary>
        /// Cancel all active orders at once.
        /// </summary>
        /// <returns></returns>
        BitfinexCancelAllOrders CancelAllOrders();
        /// <summary>
        /// Get the status of an order. Is it active? Was it cancelled? To what extent has it been executed? etc.
        /// </summary>
        /// <param name="orderId">The order ID given by NewOrder method</param>
        /// <returns></returns>
        BitfinexOrderStatus GetOrderStatus(long orderId);
        /// <summary>
        /// View your active orders.
        /// </summary>
        /// <returns></returns>
        List<BitfinexOrderStatus> GetActiveOrders();
        /// <summary>
        /// View your active positions.
        /// </summary>
        /// <returns>A list of your active positions.</returns>
        List<BitfinexActivePosition> GetActivePositions();

        BitfinexNewOrder NewOrder(BitfinexOrderInput newOrder);
        List<BitfinexNewOrder> MultipleNewOrders(List<BitfinexOrderInput> newOrders);
        string CancelMultipleOrders(long[] orderIds);
        #endregion

        #region Historical Data
        /// <summary>
        /// View all of your balance ledger entries.
        /// </summary>
        /// <param name="currency">The currency to look for.</param>
        /// <param name="since">Return only the history after this timestamp.</param>
        /// <param name="until">Return only the history before this timestamp.</param>
        /// <param name="limit">Limit the number of entries to return.</param>
        /// <param name="walletType">Return only entries that took place in this wallet. Accepted inputs are: “trading”, “exchange”, “deposit”.</param>
        /// <returns></returns>
        List<BitfinexBalanceHistory> BalanceHistory(BitfinexMethod currency, DateTime since, DateTime until, int limit, BitfinexWalletType walletType);
        /// <summary>
        /// View your past deposits/withdrawals.
        /// </summary>
        /// <param name="currency">The currency to look for.</param>
        /// <param name="since">Return only the history after this timestamp.</param>
        /// <param name="until">Return only the history before this timestamp.</param>
        /// <param name="limit">Limit the number of entries to return.</param>
        /// <param name="walletType">Return only entries that took place in this wallet. Accepted inputs are: “trading”, “exchange”, “deposit”.</param>
        /// <returns></returns>
        List<BitfinexDepositWithdrawalHistory> DepositWithdrawalHistory(BitfinexMethod currency, DateTime since, DateTime until, int limit, BitfinexWalletType walletType);
        #endregion
    }
}
