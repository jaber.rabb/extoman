﻿using JackLeitch.RateGate;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.TradingPlatform.Binance;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service.WebServices
{
    public class BinanceService : WebServiceManager, IBinanceService, IDisposable
    {
        #region Properties
        private readonly string Secret = AppSettingManager.Binance_Secret;
        private readonly string Key = AppSettingManager.Binance_Key;
        //private readonly string ApiUrl = @"https://www.binance.com";
        //private string WebSocketEndPoint = @"wss://stream.binance.com:9443/ws/";
        private readonly HttpClient httpClient = new HttpClient() { BaseAddress = new Uri(@"https://api.binance.com") };
        private BinanceTradingRules Rules;
        private RateGate _rateGate;
        private readonly ICacheManager _cacheManager;
        protected readonly string RULES_CACHE_KEY = $"Xtoman.Binance.Rules";
        private BinanceSystemStatus SystemStatus;
        #endregion

        #region Constructor
        public BinanceService(ICacheManager cacheManager)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            _rateGate = new RateGate(1, TimeSpan.FromSeconds(1));
            _cacheManager = cacheManager;
            httpClient.DefaultRequestHeaders
                    .Accept
                    .Add(new MediaTypeWithQualityHeaderValue("application/json"));

            httpClient.Timeout = new TimeSpan(0, 0, 10);
            if (!AppSettingManager.IsLocale)
            {
                LoadSystemStatus(false);
                if (SystemStatus == BinanceSystemStatus.Normal)
                    LoadRules();
            }
        }
        #endregion

        #region Dispose & Destructor
        ~BinanceService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_rateGate != null)
                    _rateGate.Dispose();
            }

            _rateGate = null;
        }
        #endregion

        #region Sync methods
        public BinanceTradingRules LoadRules()
        {
            if (_cacheManager.IsSet(RULES_CACHE_KEY))
                Rules = _cacheManager.Get<BinanceTradingRules>(RULES_CACHE_KEY);
            else
                Rules = _cacheManager.Get(RULES_CACHE_KEY, () =>
                {
                    return Call<BinanceTradingRules>(ApiMethod.GET, EndPoints.TradingRules);
                });
            return Rules;
        }

        public void LoadSystemStatus(bool throwException = true)
        {
            if (throwException)
            {
                var BinanceSystemStatusResult = Call<BinanceSystemStatusResult>(ApiMethod.GET, EndPoints.SystemStatus);
                SystemStatus = BinanceSystemStatusResult.StatusEnum;
            }
            else
            {
                try
                {
                    var BinanceSystemStatusResult = Call<BinanceSystemStatusResult>(ApiMethod.GET, EndPoints.SystemStatus);
                    SystemStatus = BinanceSystemStatusResult.StatusEnum;
                }
                catch (Exception ex)
                {
                    ex.LogError();
                    SystemStatus = BinanceSystemStatus.Maintenance;
                }
            }
        }

        #region SAPI
        public List<BinanceCoinInfo> GetAllCoinsInformation()
        {
            var result = Call<List<BinanceCoinInfo>>(ApiMethod.GET, EndPoints.AllCoinsInformation, true);
            return result;
        }

        public async Task<BinanceWithdrawResponse> WithdrawSAPIAsync(string asset, decimal amount, string address, string addressTag = null, string network = null, string withdrawOrderId = null, string name = null, bool? transactionFeeFlag = null, long recvWindow = 59999)
        {
            CheckAssetNull(asset);
            if (amount <= 0m)
            {
                var error = new ArgumentException("amount must be greater than zero.", "amount");
                error.LogError();
                throw error;
            }
            if (!address.HasValue())
            {
                var error = new ArgumentException("address cannot be empty. ", "address");
                error.LogError();
                throw error;
            }

            address = address.Trim();
            if (addressTag.HasValue())
                addressTag = addressTag.Trim();

            if (name.HasValue())
                name = name.Replace(" ", "%20");

            var args = $"coin={asset.ToUpper()}&amount={amount}&address={address}"
              + (network.HasValue()  ? $"network={network}" : "")
              + (addressTag.HasValue() ? $"&addressTag={addressTag}" : "")
              + (name.HasValue() ? $"&name={name}" : "")
              + (withdrawOrderId.HasValue() ? $"withdrawOrderId={withdrawOrderId}" : "")
              + (transactionFeeFlag.HasValue ? $"transactionFeeFlag={transactionFeeFlag.ToString().ToLower()}" : "")
              + $"&recvWindow={recvWindow}";

            new Exception("Binance withdraw args: " + args).LogError();
            return await CallAsync<BinanceWithdrawResponse>(ApiMethod.POST, EndPoints.WithdrawSAPI, true, args);
        }

        public async Task<BinanceDepositAddressSAPI> GetDepositAddressAsync(string asset, string network = null, long recvWindow = 59999)
        {
            CheckAssetNull(asset);
            var args = $"coin={asset.ToUpper()}"
                + (network.HasValue() ? $"network={network}" : "")
              + $"&recvWindow={recvWindow}";

            return await CallAsync<BinanceDepositAddressSAPI>(ApiMethod.GET, EndPoints.DepositAddressSAPI, true, args);
        }
        #endregion

        public List<BinanceSymbolPrice> GetAllPrices()
        {
            return Call<List<BinanceSymbolPrice>>(ApiMethod.GET, EndPoints.AllPrices, false);
        }

        public BinanceAccountInfo GetAccountInfo(long recvWindow = 59999)
        {
            //new BinanceException($"recvWindow = {recvWindow}").LogError();
            return Call<BinanceAccountInfo>(ApiMethod.GET, EndPoints.AccountInformation, true, $"recvWindow={recvWindow}");
        }

        public BinanceWithdrawHistory GetWithdrawHistory(string asset = null, BinanceWithdrawStatus? status = null, DateTime? startTime = null, DateTime? endTime = null, long recvWindow = 59999)
        {
            //CheckAssetNull(asset);
            var args = (asset.HasValue() ? $"asset={asset.ToUpper()}" : "")
              + (status.HasValue ? $"&status={(byte)status}" : "")
              + (startTime.HasValue ? $"&startTime={GenerateTimeStamp(startTime.Value)}" : "")
              + (endTime.HasValue ? $"&endTime={GenerateTimeStamp(endTime.Value)}" : "")
              + $"&recvWindow={recvWindow}";

            return Call<BinanceWithdrawHistory>(ApiMethod.GET, EndPoints.WithdrawHistory, true, args);
        }
        #endregion

        #region Async methods

        #region SAPI
        public async Task<List<BinanceCoinInfo>> GetAllCoinsInformationAsync()
        {
            var result = await CallAsync<List<BinanceCoinInfo>>(ApiMethod.GET, EndPoints.AllCoinsInformation, true);
            return result;
        }
        #endregion

        public async Task<BinanceTradingRules> LoadRulesAsync()
        {
            if (_cacheManager.IsSet(RULES_CACHE_KEY))
            {
                Rules = _cacheManager.Get<BinanceTradingRules>(RULES_CACHE_KEY);
                if (Rules == null)
                {
                    _cacheManager.Remove(RULES_CACHE_KEY);
                    Rules = await _cacheManager.Get(RULES_CACHE_KEY, () =>
                    {
                        return CallAsync<BinanceTradingRules>(ApiMethod.GET, EndPoints.TradingRules);
                    });
                }
            }
            else
                Rules = await _cacheManager.Get(RULES_CACHE_KEY, () =>
                {
                    return CallAsync<BinanceTradingRules>(ApiMethod.GET, EndPoints.TradingRules);
                });
            return Rules;
        }

        public async Task<dynamic> TestConnectivityAsync()
        {
            return await CallAsync<dynamic>(ApiMethod.GET, EndPoints.TestConnectivity, false);
        }
        public async Task<BinanceServerInfo> GetServerTimeAsync()
        {
            return await CallAsync<BinanceServerInfo>(ApiMethod.GET, EndPoints.CheckServerTime, false);
        }
        public async Task<BinanceOrderBook> GetOrderBookAsync(string symbol, int limit = 100)
        {
            CheckSymbolNull(symbol);
            var result = await CallAsync<dynamic>(ApiMethod.GET, EndPoints.OrderBook, false, $"symbol={symbol.ToUpper()}&limit={limit}");
            return GetParsedOrderBook(result);
        }
        public async Task<List<BinanceAggregateTrade>> GetAggregateTradesAsync(string symbol, int limit = 500)
        {
            CheckSymbolNull(symbol);
            return await CallAsync<List<BinanceAggregateTrade>>(ApiMethod.GET, EndPoints.AggregateTrades, false, $"symbol={symbol.ToUpper()}&limit={limit}");
        }
        public async Task<List<BinanceCandlestick>> GetCandleSticksAsync(string symbol, BinanceTimeInterval interval, int limit = 500)
        {
            CheckSymbolNull(symbol);
            var result = await CallAsync<dynamic>(ApiMethod.GET, EndPoints.Candlesticks, false, $"symbol={symbol.ToUpper()}&interval={interval.ToDisplay()}&limit={limit}");
            return GetParsedCandlestick(result);
        }
        public async Task<BinancePriceChangeInfo> GetPriceChange24HAsync(string symbol)
        {
            CheckSymbolNull(symbol);
            return await CallAsync<BinancePriceChangeInfo>(ApiMethod.GET, EndPoints.TickerPriceChange24H, false, $"symbol={symbol.ToUpper()}");
        }

        public List<BinancePriceChangeInfo> GetAllPricesChange24H()
        {
            return Call<List<BinancePriceChangeInfo>>(ApiMethod.GET, EndPoints.TickerPriceChange24H, false);
        }
        public async Task<List<BinancePriceChangeInfo>> GetAllPricesChange24HAsync()
        {
            return await CallAsync<List<BinancePriceChangeInfo>>(ApiMethod.GET, EndPoints.TickerPriceChange24H, false);
        }

        public BinanceSymbolPrice GetPrice(string symbol)
        {
            CheckSymbolNull(symbol);
            return Call<BinanceSymbolPrice>(ApiMethod.GET, EndPoints.AllPrices, false, $"symbol={symbol.ToUpper()}");
        }
        public async Task<BinanceSymbolPrice> GetPriceAsync(string symbol)
        {
            CheckSymbolNull(symbol);
            return await CallAsync<BinanceSymbolPrice>(ApiMethod.GET, EndPoints.AllPrices, false, $"symbol={symbol.ToUpper()}");
        }

        public BinanceAveragePrice GetAveragePrice(string symbol)
        {
            CheckSymbolNull(symbol);
            return Call<BinanceAveragePrice>(ApiMethod.GET, EndPoints.AveragePrice, false, $"symbol={symbol.ToUpper()}");
        }

        public async Task<BinanceAveragePrice> GetAveragePriceAsync(string symbol)
        {
            CheckSymbolNull(symbol);
            return await CallAsync<BinanceAveragePrice>(ApiMethod.GET, EndPoints.AveragePrice, false, $"symbol={symbol.ToUpper()}");
        }

        public async Task<List<BinanceSymbolPrice>> GetAllPricesAsync()
        {
            return await CallAsync<List<BinanceSymbolPrice>>(ApiMethod.GET, EndPoints.AllPrices, false);
        }
        public async Task<List<BinanceOrderBookTicker>> GetOrderBookTickerAsync()
        {
            return await CallAsync<List<BinanceOrderBookTicker>>(ApiMethod.GET, EndPoints.OrderBookTicker, false);
        }
        public async Task<BinanceNewOrder> PostNewOrderAsync(string symbol, decimal quantity, decimal price, BinanceOrderSide side, BinanceOrderType orderType = BinanceOrderType.LIMIT, BinanceTimeInForce timeInForce = BinanceTimeInForce.GTC, decimal icebergQty = 0, long recvWindow = 59999)
        {
            new Exception($"{symbol} -> {quantity}").LogError();
            ValidateOrderValue(symbol, orderType, price, quantity, icebergQty);
            //Validates that the order is valid.

            var args = $"symbol={symbol.ToUpper()}&side={side.ToString()}&type={orderType.ToString()}&quantity={quantity}"
                + (orderType == BinanceOrderType.LIMIT ? $"&timeInForce={timeInForce}" : "")
                + (orderType == BinanceOrderType.LIMIT ? $"&price={price}" : "")
                + (icebergQty > 0m ? $"&icebergQty={icebergQty}" : "")
                + "&newOrderRespType=FULL"
                + $"&recvWindow={recvWindow}";
            //new BinanceException("new order args: " + args).LogError();
            return await CallAsync<BinanceNewOrder>(ApiMethod.POST, EndPoints.NewOrder, true, args);
        }

        public BinanceNewOrder PostNewOrder(string symbol, decimal quantity, decimal price, BinanceOrderSide side, BinanceOrderType orderType = BinanceOrderType.LIMIT, BinanceTimeInForce timeInForce = BinanceTimeInForce.GTC, decimal icebergQty = 0, long recvWindow = 59999)
        {
            //Validates that the order is valid.
            ValidateOrderValue(symbol, orderType, price, quantity, icebergQty);

            var args = $"symbol={symbol.ToUpper()}&side={side.ToString()}&type={orderType.ToString()}&quantity={quantity}"
                + (orderType == BinanceOrderType.LIMIT ? $"&timeInForce={timeInForce}" : "")
                + (orderType == BinanceOrderType.LIMIT ? $"&price={price}" : "")
                + (icebergQty > 0m ? $"&icebergQty={icebergQty}" : "")
                + $"&recvWindow={recvWindow}";
            new BinanceException("new order args: " + args).LogError();
            var result = Call<BinanceNewOrder>(ApiMethod.POST, EndPoints.NewOrder, true, args);
            return result;
        }

        public async Task<dynamic> PostNewOrderTestAsync(string symbol, decimal quantity, decimal price, BinanceOrderSide side, BinanceOrderType orderType = BinanceOrderType.LIMIT, BinanceTimeInForce timeInForce = BinanceTimeInForce.GTC, decimal icebergQty = 0, long recvWindow = 59999)
        {
            //Validates that the order is valid.
            ValidateOrderValue(symbol, orderType, price, quantity, icebergQty);

            var args = $"symbol={symbol.ToUpper()}&side={side.ToString()}&type={orderType.ToString()}&quantity={quantity}"
                + (orderType == BinanceOrderType.LIMIT ? $"&timeInForce={timeInForce}" : "")
                + (orderType == BinanceOrderType.LIMIT ? $"&price={price}" : "")
                + (icebergQty > 0m ? $"&icebergQty={icebergQty}" : "")
                + $"&recvWindow={recvWindow}";
            return await CallAsync<dynamic>(ApiMethod.POST, EndPoints.NewOrderTest, true, args);
        }
        public async Task<BinanceOrder> GetOrderAsync(string symbol, long? orderId = null, string origClientOrderId = null, long recvWindow = 59999)
        {
            var args = $"symbol={symbol.ToUpper()}&recvWindow={recvWindow}";
            CheckSymbolNull(symbol);
            if (orderId.HasValue)
                args += $"&orderId={orderId.Value}";
            else if (origClientOrderId.HasValue())
                args += $"&origClientOrderId={origClientOrderId}";
            else
            {
                var error = new ArgumentException("Either orderId or origClientOrderId must be sent.");
                error.LogError();
                throw error;
            }

            return await CallAsync<BinanceOrder>(ApiMethod.GET, EndPoints.QueryOrder, true, args);
        }
        public async Task<BinanceCanceledOrder> CancelOrderAsync(string symbol, long? orderId = null, string origClientOrderId = null, long recvWindow = 59999)
        {
            CheckSymbolNull(symbol);
            var args = $"symbol={symbol.ToUpper()}&recvWindow={recvWindow}";

            if (orderId.HasValue)
                args += $"&orderId={orderId.Value}";
            else if (origClientOrderId.HasValue())
                args += $"&origClientOrderId={origClientOrderId}";
            else
            {
                var error = new ArgumentException("Either orderId or origClientOrderId must be sent.");
                error.LogError();
                throw error;
            }

            return await CallAsync<BinanceCanceledOrder>(ApiMethod.DELETE, EndPoints.CancelOrder, true, args);
        }
        public async Task<List<BinanceOrder>> GetCurrentOpenOrdersAsync(string symbol, long recvWindow = 59999)
        {
            CheckSymbolNull(symbol);
            return await CallAsync<List<BinanceOrder>>(ApiMethod.GET, EndPoints.CurrentOpenOrders, true, $"symbol={symbol.ToUpper()}&recvWindow={recvWindow}");
        }

        public async Task<BinanceAssetsDetails> AssetsDetailAsync()
        {
            return await CallAsync<BinanceAssetsDetails>(ApiMethod.GET, EndPoints.AssetDetail, true);
        }

        public async Task<List<BinanceOrder>> GetAllOrdersAsync(string symbol, long? orderId = null, int limit = 500, long recvWindow = 59999)
        {
            CheckSymbolNull(symbol);
            return await CallAsync<List<BinanceOrder>>(ApiMethod.GET, EndPoints.AllOrders, true, $"symbol={symbol.ToUpper()}&limit={limit}&recvWindow={recvWindow}" + (orderId.HasValue ? $"&orderId={orderId.Value}" : ""));
        }
        public async Task<BinanceAccountInfo> GetAccountInfoAsync(long recvWindow = 59999)
        {
            return await CallAsync<BinanceAccountInfo>(ApiMethod.GET, EndPoints.AccountInformation, true, $"recvWindow={recvWindow}");
        }
        public async Task<List<BinanceTrade>> GetTradeListAsync(string symbol, long recvWindow = 59999)
        {
            CheckSymbolNull(symbol);
            return await CallAsync<List<BinanceTrade>>(ApiMethod.GET, EndPoints.TradeList, true, $"symbol={symbol.ToUpper()}&recvWindow={recvWindow}");
        }

        public async Task<BinanceDepositAddress> GetDepositAddressAsync(string asset, long recvWindow = 59999)
        {
            CheckAssetNull(asset);
            var args = $"asset={asset.ToUpper()}"
              + $"&recvWindow={recvWindow}";

            return await CallAsync<BinanceDepositAddress>(ApiMethod.POST, EndPoints.DepositAddress, true, args);
        }

        public BinanceWithdrawResponse Withdraw(string asset, decimal amount, string address, string addressTag = "", string addressName = "", long recvWindow = 59999)
        {
            CheckAssetNull(asset);
            if (amount <= 0m)
            {
                var error = new ArgumentException("amount must be greater than zero.", "amount");
                error.LogError();
                throw error;
            }
            if (!address.HasValue())
            {
                var error = new ArgumentException("address cannot be empty. ", "address");
                error.LogError();
                throw error;
            }

            var args = $"asset={asset.ToUpper()}&amount={amount}&address={address}"
              + (addressTag.HasValue() ? $"&addressTag={addressTag}" : "")
              + (addressName.HasValue() ? $"&name={addressName}" : "")
              + $"&recvWindow={recvWindow}";

            new Exception("Binance withdraw args: " + args).LogError();
            return Call<BinanceWithdrawResponse>(ApiMethod.POST, EndPoints.Withdraw, true, args);
        }

        public async Task<BinanceWithdrawResponse> WithdrawAsync(string asset, decimal amount, string address, string addressTag = "", string addressName = "", long recvWindow = 59999)
        {
            CheckAssetNull(asset);
            if (amount <= 0m)
            {
                var error = new ArgumentException("amount must be greater than zero.", "amount");
                error.LogError();
                throw error;
            }
            if (!address.HasValue())
            {
                var error = new ArgumentException("address cannot be empty. ", "address");
                error.LogError();
                throw error;
            }

            address = address.Trim();
            if (addressTag.HasValue())
                addressTag = addressTag.Trim();

            var args = $"asset={asset.ToUpper()}&amount={amount}&address={address}"
              + (addressTag.HasValue() ? $"&addressTag={addressTag}" : "")
              + (addressName.HasValue() ? $"&name={addressName}" : "")
              + $"&recvWindow={recvWindow}";

            new Exception("Binance withdraw args: " + args).LogError();
            return await CallAsync<BinanceWithdrawResponse>(ApiMethod.POST, EndPoints.Withdraw, true, args);
        }
        public async Task<BinanceDepositHistory> GetDepositHistoryAsync(string asset, BinanceDepositStatus? status = null, DateTime? startTime = null, DateTime? endTime = null, long recvWindow = 59999)
        {
            CheckAssetNull(asset);
            var args = $"asset={asset.ToUpper()}"
              + (status.HasValue ? $"&status={(int)status}" : "")
              + (startTime.HasValue ? $"&startTime={GenerateTimeStamp(startTime.Value)}" : "")
              + (endTime.HasValue ? $"&endTime={GenerateTimeStamp(endTime.Value)}" : "")
              + $"&recvWindow={recvWindow}";

            return await CallAsync<BinanceDepositHistory>(ApiMethod.GET, EndPoints.DepositHistory, true, args);
        }
        public async Task<BinanceWithdrawHistory> GetWithdrawHistoryAsync(string asset = null, BinanceWithdrawStatus? status = null, DateTime? startTime = null, DateTime? endTime = null, long recvWindow = 59999)
        {
            //CheckAssetNull(asset);
            var args = (asset.HasValue() ? $"asset={asset.ToUpper()}" : "")
              + (status.HasValue ? $"&status={(byte)status}" : "")
              + (startTime.HasValue ? $"&startTime={GenerateTimeStamp(startTime.Value)}" : "")
              + (endTime.HasValue ? $"&endTime={GenerateTimeStamp(endTime.Value)}" : "")
              + $"&recvWindow={recvWindow}";

            return await CallAsync<BinanceWithdrawHistory>(ApiMethod.GET, EndPoints.WithdrawHistory, true, args);
        }
        public async Task<BinanceUserStreamInfo> StartUserStreamAsync()
        {
            return await CallAsync<BinanceUserStreamInfo>(ApiMethod.POST, EndPoints.StartUserStream, false);
        }
        public async Task<dynamic> KeepAliveUserStreamAsync(string listenKey)
        {
            if (!listenKey.HasValue())
            {
                var error = new ArgumentException("listenKey cannot be empty. ", "listenKey");
                error.LogError();
                throw error;
            }
            return await CallAsync<dynamic>(ApiMethod.PUT, EndPoints.KeepAliveUserStream, false, $"listenKey={listenKey}");
        }
        public async Task<dynamic> CloseUserStreamAsync(string listenKey)
        {
            if (!listenKey.HasValue())
            {
                var error = new ArgumentException("listenKey cannot be empty. ", "listenKey");
                error.LogError();
                throw error;
            }
            return await CallAsync<dynamic>(ApiMethod.DELETE, EndPoints.CloseUserStream, false, $"listenKey={listenKey}");
        }
        #endregion

        #region Helpers
        /// <summary>
        /// Validates that a new order is valid before posting it.
        /// </summary>
        /// <param name="orderType">Order type (LIMIT-MARKET).</param>
        /// <param name="symbolInfo">Object with the information of the ticker.</param>
        /// <param name="unitPrice">Price of the transaction.</param>
        /// <param name="quantity">Quantity to transaction.</param>
        /// <param name="stopPrice">Price for stop orders.</param>
        private void ValidateOrderValue(string symbol, BinanceOrderType orderType, decimal unitPrice, decimal quantity, decimal icebergQty)
        {
            // Validating parameters values.
            if (!symbol.HasValue())
            {
                var error = new ArgumentException("Invalid symbol. ", "symbol");
                error.LogError();
                throw error;
            }
            if (quantity <= 0m)
            {
                var error = new ArgumentException("Quantity must be greater than zero.", "quantity");
                error.LogError();
                throw error;
            }
            if (orderType == BinanceOrderType.LIMIT)
            {
                if (unitPrice <= 0m)
                {
                    var error = new ArgumentException("Price must be greater than zero.", "price");
                    error.LogError();
                    throw error;
                }
            }

            // Validating Trading Rules
            if (Rules == null)
                LoadRules();
            if (Rules != null)
            {
                var symbolInfo = Rules.Symbols.Where(r => r.SymbolName.ToUpper() == symbol.ToUpper()).FirstOrDefault();
                var priceFilter = symbolInfo.Filters.Where(r => r.FilterType == "PRICE_FILTER").FirstOrDefault();
                var sizeFilter = symbolInfo.Filters.Where(r => r.FilterType == "LOT_SIZE").FirstOrDefault();
                var minNotionalFilter = symbolInfo.Filters.Where(r => r.FilterType == "MIN_NOTIONAL").FirstOrDefault();

                if (symbolInfo == null)
                {
                    var error = new ArgumentException("Invalid symbol. ", "symbol");
                    error.LogError();
                    throw error;
                }
                if (quantity < sizeFilter.MinQty)
                {
                    var error = new ArgumentException($"Quantity for this symbol is lower than allowed! Quantity must be greater than: {sizeFilter.MinQty}", "quantity");
                    error.LogError();
                    throw error;
                }
                if (quantity > sizeFilter.MaxQty)
                {
                    var error = new ArgumentException($"Quantity for this symbol is higher than allowed! Quantity must be smaller than: {sizeFilter.MaxQty}", "quantity");
                    error.LogError();
                    throw error;
                }
                if (icebergQty > 0m && !symbolInfo.IcebergAllowed)
                {
                    var error = new BinanceException($"Iceberg orders not allowed for this symbol.");
                    error.LogError();
                    throw error;
                }

                if (orderType == BinanceOrderType.LIMIT)
                {
                    if (unitPrice < priceFilter.MinPrice)
                    {
                        var error = new ArgumentException($"Price for this symbol is lower than allowed! Price must be greater than: {priceFilter.MinPrice}", "price");
                        error.LogError();
                        throw error;
                    }
                    if (unitPrice > priceFilter.MaxPrice)
                    {
                        var error = new ArgumentException($"Price for this symbol is higher than allowed! Price must be smaller than: {priceFilter.MaxPrice}", "price");
                        error.LogError();
                        throw error;
                    }
                    var notional = unitPrice * quantity;
                    if (notional < minNotionalFilter.MinNotional)
                    {
                        var error = new ArgumentException($"Price * Quanity for this symbol is lower than allowed! Price * Quanity must be greater than: {minNotionalFilter.MinNotional}", "notional");
                        error.LogError();
                        throw error;
                    }
                }
            }
        }
        private async Task<T> CallAsync<T>(ApiMethod method, string endpoint, bool isSigned = false, string parameters = null) where T : new()
        {
            if (endpoint != EndPoints.SystemStatus && SystemStatus == BinanceSystemStatus.Maintenance)
                throw new BinanceException("System Maintenance");

            if (parameters != null && parameters.StartsWith("&"))
                parameters.RemoveLeft(1);

            var finalEndpoint = endpoint + (parameters.HasValue() ? $"?{parameters}" : "");

            //Wait for RateGate
            _rateGate.WaitToProceed();

            if (isSigned)
            {
                parameters += (parameters.HasValue() ? "&timestamp=" : "timestamp=") + GenerateTimeStamp(DateTime.Now.ToUniversalTime());
                //parameters += (parameters.HasValue() ? "&timestamp=" : "timestamp=") + DateTime.UtcNow.ConvertToUnixTimestamp();
                var signature = GenerateSignature(parameters);
                finalEndpoint = $"{endpoint}?{parameters}&signature={signature}";
            }

            var request = new HttpRequestMessage(new HttpMethod(method.ToString()), finalEndpoint);
            request.Headers.Add("X-MBX-APIKEY", Key);
            var response = await httpClient.SendAsync(request).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                //new Exception($"Binance {endpoint} response: {result}").LogError();
                return LogAndDeserialize<T>(result);
            }

            // We received an error
            if (response.StatusCode == HttpStatusCode.GatewayTimeout)
                throw new BinanceException("Api Request Timeout.");

            // Get te error code and message
            var e = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            // Error Values
            var eCode = 0;
            string eMsg = "";
            if (e.IsValidJson())
            {
                try
                {
                    var i = JObject.Parse(e);

                    eCode = i["code"]?.Value<int>() ?? 0;
                    eMsg = i["msg"]?.Value<string>();
                }
                catch { }
            }
            else
            {
                throw new BinanceException($"Final Endpoint: {finalEndpoint}, Status code: {response.StatusCode.ToInt()}, Error: {e}" + e);
            }

            throw new BinanceException(string.Format("Api Error Code: {0} Message: {1}", eCode, eMsg));
        }

        private T Call<T>(ApiMethod method, string endpoint, bool isSigned = false, string parameters = null) where T : new()
        {
            if (endpoint != EndPoints.SystemStatus && SystemStatus == BinanceSystemStatus.Maintenance)
                throw new BinanceException("System Maintenance");

            if (parameters != null && parameters.StartsWith("&"))
                parameters.RemoveLeft(1);

            var finalEndpoint = endpoint + (parameters.HasValue() ? $"?{parameters}" : "");

            //Wait for RateGate
            _rateGate.WaitToProceed();

            if (isSigned)
            {
                parameters += (parameters.HasValue() ? "&timestamp=" : "timestamp=") + GenerateTimeStamp(DateTime.Now.ToUniversalTime());
                //parameters += (parameters.HasValue() ? "&timestamp=" : "timestamp=") + DateTime.UtcNow.ConvertToUnixTimestamp();
                var signature = GenerateSignature(parameters);
                finalEndpoint = $"{endpoint}?{parameters}&signature={signature}";
            }

            var request = new HttpRequestMessage(new HttpMethod(method.ToString()), finalEndpoint);
            request.Headers.Add("X-MBX-APIKEY", Key);
            var response = httpClient.SendAsync(request).Result;
            if (response.IsSuccessStatusCode)
            {
                response.EnsureSuccessStatusCode();
                var result = response.Content.ReadAsStringAsync().Result;
                //new Exception($"Binance {endpoint} response: {result}").LogError();
                return LogAndDeserialize<T>(result);
            }

            // We received an error
            if (response.StatusCode == HttpStatusCode.GatewayTimeout)
                throw new BinanceException("Api Request Timeout.");

            // Get te error code and message
            var e = response.Content.ReadAsStringAsync().Result;

            // Error Values
            var eCode = 0;
            string eMsg = "";
            if (e.IsValidJson())
            {
                try
                {
                    var i = JObject.Parse(e);

                    eCode = i["code"]?.Value<int>() ?? 0;
                    eMsg = i["msg"]?.Value<string>();
                }
                catch { }
            }
            else
            {
                throw new BinanceException($"Endpoint: {endpoint}, Status code: {response.StatusCode.ToInt()}, Error: {e}" + e);
            }

            throw new BinanceException(string.Format("Api Error Code: {0} Message: {1}", eCode, eMsg));
        }

        private string GenerateSignature(string message)
        {
            var key = Encoding.UTF8.GetBytes(Secret);
            string stringHash;
            using (var hmac = new HMACSHA256(key))
            {
                var hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(message));
                stringHash = BitConverter.ToString(hash).Replace("-", "");
            }
            return stringHash;
        }
        /// <summary>
        /// Gets the orderbook data and generates an OrderBook object.
        /// </summary>
        /// <param name="orderBookData">Dynamic containing the orderbook data.</param>
        /// <returns></returns>
        private BinanceOrderBook GetParsedOrderBook(dynamic orderBookData)
        {
            var result = new BinanceOrderBook
            {
                LastUpdateId = orderBookData.lastUpdateId.Value
            };

            var bids = new List<BinanceOrderBookOffer>();
            var asks = new List<BinanceOrderBookOffer>();

            foreach (JToken item in ((JArray)orderBookData.bids).ToArray())
            {
                bids.Add(new BinanceOrderBookOffer() { Price = decimal.Parse(item[0].ToString()), Quantity = decimal.Parse(item[1].ToString()) });
            }

            foreach (JToken item in ((JArray)orderBookData.asks).ToArray())
            {
                asks.Add(new BinanceOrderBookOffer() { Price = decimal.Parse(item[0].ToString()), Quantity = decimal.Parse(item[1].ToString()) });
            }

            result.Bids = bids;
            result.Asks = asks;

            return result;
        }

        /// <summary>
        /// Gets the candlestick data and generates an Candlestick object.
        /// </summary>
        /// <param name="candlestickData">Dynamic containing the candlestick data.</param>
        /// <returns></returns>
        private IEnumerable<BinanceCandlestick> GetParsedCandlestick(dynamic candlestickData)
        {
            var result = new List<BinanceCandlestick>();

            foreach (JToken item in ((JArray)candlestickData).ToArray())
            {
                result.Add(new BinanceCandlestick()
                {
                    OpenTime = long.Parse(item[0].ToString()),
                    Open = decimal.Parse(item[1].ToString()),
                    High = decimal.Parse(item[2].ToString()),
                    Low = decimal.Parse(item[3].ToString()),
                    Close = decimal.Parse(item[4].ToString()),
                    Volume = decimal.Parse(item[5].ToString()),
                    CloseTime = long.Parse(item[6].ToString()),
                    QuoteAssetVolume = decimal.Parse(item[7].ToString()),
                    NumberOfTrades = int.Parse(item[8].ToString()),
                    TakerBuyBaseAssetVolume = decimal.Parse(item[9].ToString()),
                    TakerBuyQuoteAssetVolume = decimal.Parse(item[10].ToString())
                });
            }

            return result;
        }

        private BinanceDepthMessage GetParsedDepthMessage(dynamic messageData)
        {
            var result = new BinanceDepthMessage
            {
                EventType = messageData.e,
                EventTime = messageData.E,
                Symbol = messageData.s,
                UpdateId = messageData.u
            };

            var bids = new List<BinanceOrderBookOffer>();
            var asks = new List<BinanceOrderBookOffer>();

            foreach (JToken item in ((JArray)messageData.b).ToArray())
                bids.Add(new BinanceOrderBookOffer() { Price = decimal.Parse(item[0].ToString()), Quantity = decimal.Parse(item[1].ToString()) });

            foreach (JToken item in ((JArray)messageData.a).ToArray())
                asks.Add(new BinanceOrderBookOffer() { Price = decimal.Parse(item[0].ToString()), Quantity = decimal.Parse(item[1].ToString()) });

            result.Bids = bids;
            result.Asks = asks;

            return result;
        }

        private void CheckSymbolNull(string symbol)
        {
            if (!symbol.HasValue())
            {
                var error = new ArgumentException("symbol cannot be empty. ", "symbol");
                error.LogError();
                throw error;
            }
        }
        private void CheckAssetNull(string asset)
        {
            if (!asset.HasValue())
            {
                var error = new ArgumentException("asset cannot be empty. ", "asset");
                error.LogError();
                throw error;
            }
        }
        private string GenerateTimeStamp(DateTime baseDateTime)
        {
            var dtOffset = new DateTimeOffset(baseDateTime);
            return dtOffset.ToUnixTimeMilliseconds().ToString();
        }

        public async Task<BinanceBalanceResultModel> GetAllBalancesAsync(List<BinanceSymbolPrice> prices = null, bool zeroIncluded = false)
        {
            var accountInfo = await GetAccountInfoAsync(15000);
            var filteredBalances = zeroIncluded ? accountInfo?.Balances : accountInfo?.Balances?
                .Where(p => p.Free > 0 || p.Locked > 0)
                .ToList();

            if (prices == null)
                prices = await GetAllPricesAsync();

            if (prices == null || !prices.Any())
                throw new BinanceException("Prices are null!");

            var model = new BinanceBalanceResultModel() { Assets = new List<BinanceBalanceItemResultModel>() };
            if (filteredBalances != null && prices != null)
            {
                foreach (var item in filteredBalances)
                {
                    decimal freeBtcValue = 0;
                    decimal freeUsdValue = 0;
                    decimal lockedBtcValue = 0;
                    decimal lockedUsdValue = 0;
                    decimal unitPriceInUSD = 0;
                    decimal unitPriceInBTC = 0;
                    var btcUSDTPrice = prices.Where(p => p.Symbol.Equals("BTCUSDT", StringComparison.InvariantCultureIgnoreCase)).Select(p => p.Price).FirstOrDefault();
                    if (item.Asset.Equals("BTC", StringComparison.InvariantCultureIgnoreCase))
                    {
                        freeBtcValue = item.Free;
                        freeUsdValue = item.Free * btcUSDTPrice;
                        lockedBtcValue = item.Locked;
                        lockedUsdValue = item.Locked * btcUSDTPrice;
                        unitPriceInBTC = 1;
                        unitPriceInUSD = btcUSDTPrice;
                    }
                    else if (item.Asset.Equals("USDT", StringComparison.InvariantCultureIgnoreCase))
                    {
                        freeUsdValue = item.Free;
                        freeBtcValue = item.Free / btcUSDTPrice;
                        lockedUsdValue = item.Locked;
                        lockedBtcValue = item.Locked / btcUSDTPrice;
                        unitPriceInBTC = 1 / btcUSDTPrice;
                        unitPriceInUSD = 1;
                    }
                    else
                    {
                        var perBTCPrice = prices.Where(p => p.Symbol.Equals(item.Asset + "BTC", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                        if (perBTCPrice != null)
                        {
                            freeBtcValue = item.Free * perBTCPrice.Price;
                            lockedBtcValue = item.Locked * perBTCPrice.Price;
                            var perUSDTPrice = prices.Where(p => p.Symbol.Equals(item.Asset + "USDT", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                            if (perUSDTPrice != null)
                            {
                                freeUsdValue = item.Free * perUSDTPrice.Price;
                                lockedUsdValue = item.Locked * perUSDTPrice.Price;
                                unitPriceInUSD = perUSDTPrice.Price;
                            }
                            else
                            {
                                freeUsdValue = freeBtcValue * btcUSDTPrice;
                                lockedUsdValue = lockedBtcValue * btcUSDTPrice;
                                unitPriceInUSD = perBTCPrice.Price * btcUSDTPrice;
                            }
                            unitPriceInBTC = perBTCPrice.Price;
                        }
                    }
                    model.Assets.Add(new BinanceBalanceItemResultModel()
                    {
                        Name = item.Asset,
                        Free = item.Free,
                        Locked = item.Locked,
                        FreeBTCValue = freeBtcValue,
                        FreeUSDValue = freeUsdValue,
                        LockedBTCValue = lockedBtcValue,
                        LockedUSDValue = lockedUsdValue,
                        UnitPriceInBTC = unitPriceInBTC,
                        UnitPriceInUSD = unitPriceInUSD,
                    });
                }
            }
            return model;
        }

        public async Task<decimal> GetAvailableBalanceByAssetAsync(string asset)
        {
            var prices = await GetAllPricesAsync();
            var allBalances = await GetAllBalancesAsync(prices, false);
            decimal rate = 0;
            if (asset.Equals("BTC", StringComparison.InvariantCultureIgnoreCase))
                rate = 1;
            else if (asset.Equals("USDT", StringComparison.InvariantCultureIgnoreCase))
            {
                var btcUSDTPrice = prices.Where(p => p.Symbol.Equals("BTCUSDT", StringComparison.InvariantCultureIgnoreCase)).Select(p => p.Price).FirstOrDefault();
                rate = 1 / btcUSDTPrice;
            }
            else
            {
                rate = prices.Where(p => p.Symbol.Equals(asset + "BTC", StringComparison.InvariantCultureIgnoreCase))
                    .Select(p => p.Price)
                    .DefaultIfEmpty(0)
                    .FirstOrDefault();
            }
            return allBalances.TotalFreeBTCValue * rate;
        }

        public decimal FixBinanceTradeSize(string symbol, decimal amount, decimal avgPrice = 0)
        {
            var lotSizeFilter = Rules.Symbols
                .Where(p => p.SymbolName == symbol)
                .SelectMany(p => p.Filters)
                .Where(p => p.FilterType == "LOT_SIZE")
                .FirstOrDefault();

            var minNotionalFilter = Rules.Symbols
                .Where(p => p.SymbolName == symbol)
                .SelectMany(p => p.Filters)
                .Where(p => p.FilterType == "MIN_NOTIONAL")
                .FirstOrDefault();

            var result = amount + ((amount / 100) * 0.25M); // %0.25 Trade fee
            result = result.Round();

            if (lotSizeFilter != null)
            {
                //Fix step size
                var remind = result % lotSizeFilter.StepSize;
                if (remind != 0)
                    result += lotSizeFilter.StepSize - remind;
            }

            if (minNotionalFilter != null)
            {
                if (avgPrice == 0)
                {
                    if (minNotionalFilter.AveragePriceMins == 0)
                    {
                        var bPrice = GetPrice(symbol);
                        avgPrice = bPrice.Price;
                    }
                    else
                    {
                        var averagePrice = GetAveragePrice(symbol);
                        avgPrice = averagePrice.Price;
                    }
                }
                var notional = avgPrice * result;
                while (notional <= minNotionalFilter.MinNotional)
                {
                    result += lotSizeFilter.StepSize;
                    notional = avgPrice * result;
                }
                return result;
            }
            return result;
        }
        #endregion
    }
}
