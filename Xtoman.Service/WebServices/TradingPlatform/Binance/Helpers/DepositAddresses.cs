﻿using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public static class DepositAddresses
    {
        //public static readonly string ADA_Address = "DdzFFzCqrhsxSJudmCJY5h7CjHnnc9vwzQoM35yxgWzWqapNCXjEjvFNAH8jigjGYa1W15X8a8B63GgTdjM4atX2x5KRWbgHrzQV6VQW";
        //public static readonly string BTC_Address = "15aXtkSvKuBTNwTthwRFkBGt3ae6KHVABc";
        //public static readonly string BCH_Address = "15aXtkSvKuBTNwTthwRFkBGt3ae6KHVABc";
        //public static readonly string DASH_Address = "XwBNUyYyiP5MS61V46o6xtQ54JS4o25Lsh";
        //public static readonly string DCR_Address = "DscfYqrcb7ncdmDTzPVMcorKYSBuYSKfqS2";
        //public static readonly string EOS_Address = "binancecleos";
        //public static readonly string EOS_Tag = "109232871";
        //public static readonly string ETC_Address = "0x96c2b28be07c8a761edea7d99535c40e5357d53b";
        //public static readonly string ETH_Address = "0x96c2b28be07c8a761edea7d99535c40e5357d53b";
        //public static readonly string KMD_Address = "RF4CPWgLbRrcME2AspF43kJsFrmNB57xAz";
        //public static readonly string LTC_Address = "LZ2GRiCcDLvhVtaikeuDQxgpC47m6UFqUL";
        //public static readonly string NAV_Address = "NYKK8VR1aesSqxccjcySXA2zTkjJLgJtpS";
        //public static readonly string NEO_Address = "AVxjN51HPr7NcBcY2xKV53EtvBgsA5sVwr";
        //public static readonly string PIVX_Address = "DQpsTzLgRmQxDoAg5QiW3bC72eWapFFcnN";
        //public static readonly string STEEM_Address = "deepcrypto8";
        //public static readonly string STEEM_Tag = "104770370";
        //public static readonly string STRAT_Address = "SYA3gr1WndE69fxwQiWsUdx6vtMh9FMafY";
        //public static readonly string TRX_Address = "TGPopxpSqirYL3LnN4VatSsPPyGkTo92Ge";
        //public static readonly string USDT_Address = "19ZhFQXW9n57ddy9LzMXdZhvuru38o3zx1";
        //public static readonly string WAVES_Address = "3PFKrErFyhhyiwBuKqQcfb5jqZPsK98YYNx";
        //public static readonly string XEM_Address = "NC64UFOWRO6AVMWFV2BFX2NT6W2GURK2EOX6FFMZ";
        //public static readonly string XEM_Tag = "109820857";
        //public static readonly string XLM_Address = "GAHK7EEG2WWHVKDNT4CEQFZGKF2LGDSW2IVM4S5DP42RBW3K6BTODB4A";
        //public static readonly string XLM_Tag = "1079441652";
        //public static readonly string XMR_Address = "44tLjmXrQNrWJ5NBsEj2R77ZBEgDa3fEe9GLpSf2FRmhexPvfYDUAB7EXX1Hdb3aMQ9FLqdJ56yaAhiXoRsceGJCRS3Jxkn";
        //public static readonly string XMR_Tag = "a38760e464dab2dac1f4c8eed50dfad92b2116f287c4b050a6c7419517ed400a";
        //public static readonly string XVG_Address = "DLzNgdFN15iRu7zBwHQBhEE4beyQWcRNrX";
        //public static readonly string XZC_Address = "aN83M4JSrcutz2ZXhB6gkG9DXJ1M9eazdn";
        //public static readonly string ZEC_Address = "t1SyHg8ot9Lie3b625p6PfH4Yx5SewNUxRv";
        //public static readonly string XRP_Address = "rEb8TK3gBgk5auZkwc6sHnwrGVJH8DuaLh";
        //public static readonly string XRP_Tag = "100714851";
        //public static readonly string DOGE_Address = "DEhdqepSvRTrAvWRKhKoSF6TjmJhicYg86";
        public static readonly string DOGE_Address = AppSettingManager.Binance_DOGE_Address;
        public static readonly string ADA_Address = AppSettingManager.Binance_ADA_Address;
        public static readonly string BTC_Address = AppSettingManager.Binance_BTC_Address;
        public static readonly string BCH_Address = AppSettingManager.Binance_BCH_Address;
        public static readonly string DASH_Address = AppSettingManager.Binance_DASH_Address;
        public static readonly string DCR_Address = AppSettingManager.Binance_DCR_Address;
        public static readonly string EOS_Address = AppSettingManager.Binance_EOS_Address;
        public static readonly string EOS_Tag = AppSettingManager.Binance_EOS_Tag;
        public static readonly string ETC_Address = AppSettingManager.Binance_ETC_Address;
        public static readonly string ETH_Address = AppSettingManager.Binance_ETH_Address;
        public static readonly string KMD_Address = AppSettingManager.Binance_KMD_Address;
        public static readonly string LTC_Address = AppSettingManager.Binance_LTC_Address;
        public static readonly string LSK_Address = AppSettingManager.Binance_LSK_Address;
        public static readonly string NAV_Address = AppSettingManager.Binance_NAV_Address;
        public static readonly string NEO_Address = AppSettingManager.Binance_NEO_Address;
        public static readonly string PIVX_Address = AppSettingManager.Binance_PIVX_Address;
        public static readonly string STEEM_Address = AppSettingManager.Binance_STEEM_Address;
        public static readonly string STEEM_Tag = AppSettingManager.Binance_STEEM_Tag;
        public static readonly string STRAT_Address = AppSettingManager.Binance_STRAT_Address;
        public static readonly string TRX_Address = AppSettingManager.Binance_TRX_Address;
        public static readonly string USDT_Address = AppSettingManager.Binance_USDT_Address;
        public static readonly string WAVES_Address = AppSettingManager.Binance_WAVES_Address;
        public static readonly string XEM_Address = AppSettingManager.Binance_XEM_Address;
        public static readonly string XEM_Tag = AppSettingManager.Binance_XEM_Tag;
        public static readonly string XLM_Address = AppSettingManager.Binance_XLM_Address;
        public static readonly string XLM_Tag = AppSettingManager.Binance_XLM_Tag;
        public static readonly string XMR_Address = AppSettingManager.Binance_XMR_Address;
        public static readonly string XMR_Tag = AppSettingManager.Binance_XMR_Tag;
        public static readonly string XVG_Address = AppSettingManager.Binance_XVG_Address;
        public static readonly string XZC_Address = AppSettingManager.Binance_XZC_Address;
        public static readonly string ZEC_Address = AppSettingManager.Binance_ZEC_Address;
        public static readonly string XRP_Address = AppSettingManager.Binance_XRP_Address;
        public static readonly string XRP_Tag = AppSettingManager.Binance_XRP_Tag;
        public static readonly string RVN_Address = AppSettingManager.Binance_RVN_Address;
        public static readonly string ZEN_Address = AppSettingManager.Binance_ZEN_Address;
        public static readonly string QTUM_Address = AppSettingManager.Binance_QTUM_Address;

        public static string GetTagByShortName(string asset)
        {
            asset = asset.ToUpper();
            switch (asset)
            {
                case "EOS": return EOS_Tag;
                case "STEEM": return STEEM_Tag;
                case "XEM": return XEM_Tag;
                case "XLM": return XLM_Tag;
                case "XMR": return XMR_Tag;
                case "XRP": return XRP_Tag;
            }
            return "";
        }
        public static string GetAddressByShortName(string asset)
        {
            asset = asset.ToUpper();
            switch (asset)
            {
                case "ADA": return ADA_Address;
                case "BTC": return BTC_Address;
                case "BCH": return BCH_Address;
                case "DASH": return DASH_Address;
                case "DCR": return DCR_Address;
                case "EOS": return EOS_Address;
                case "ETC": return ETC_Address;
                case "ETH": return ETH_Address;
                case "KMD": return KMD_Address;
                case "LTC": return LTC_Address;
                case "NAV": return NAV_Address;
                case "NEO": return NEO_Address;
                case "PIVX": return PIVX_Address;
                case "STEEM": return STEEM_Address;
                case "STRAT": return STRAT_Address;
                case "TRX": return TRX_Address;
                case "USDT": return USDT_Address;
                case "WAVES": return WAVES_Address;
                case "XEM": return XEM_Address;
                case "XLM": return XLM_Address;
                case "XMR": return XMR_Address;
                case "XVG": return XVG_Address;
                case "XZC": return XZC_Address;
                case "ZEC": return ZEC_Address;
                case "XRP": return XRP_Address;
                case "ZEN": return ZEN_Address;
                case "RVN": return RVN_Address;
                //
                case "QTUM": return QTUM_Address;
                case "LSK": return LSK_Address;
                case "DOGE": return DOGE_Address;

                // ERC-20 Tokens
                case "LINK": return ETH_Address;
                case "BAT": return ETH_Address;
                case "OMG": return ETH_Address;
                case "ZRX": return ETH_Address;
                case "IOST": return ETH_Address;
                case "ENJ": return ETH_Address;
            }
            return "";
        }
    }
}
