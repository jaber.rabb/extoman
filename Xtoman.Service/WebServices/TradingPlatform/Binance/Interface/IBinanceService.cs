﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.TradingPlatform.Binance;

namespace Xtoman.Service.WebServices
{
    public interface IBinanceService
    {
        BinanceTradingRules LoadRules();
        Task<BinanceTradingRules> LoadRulesAsync();

        BinanceWithdrawHistory GetWithdrawHistory(string asset = null, BinanceWithdrawStatus? status = null, DateTime? startTime = null, DateTime? endTime = null, long recvWindow = 59999);

        #region General
        /// <summary>
        /// Test connectivity to the Rest API.
        /// </summary>
        /// <returns></returns>
        Task<dynamic> TestConnectivityAsync();

        /// <summary>
        /// Test connectivity to the Rest API and get the current server time.
        /// </summary>
        /// <returns></returns>
        Task<BinanceServerInfo> GetServerTimeAsync();
        #endregion

        #region Market Data
        /// <summary>
        /// Get order book for a particular symbol.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <param name="limit">Limit of records to retrieve.</param>
        /// <returns></returns>
        Task<BinanceOrderBook> GetOrderBookAsync(string symbol, int limit = 100);

        /// <summary>
        /// Get compressed, aggregate trades. Trades that fill at the time, from the same order, with the same price will have the quantity aggregated.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <param name="limit">Limit of records to retrieve.</param>
        /// <returns></returns>
        Task<List<BinanceAggregateTrade>> GetAggregateTradesAsync(string symbol, int limit = 500);

        /// <summary>
        /// Kline/candlestick bars for a symbol. Klines are uniquely identified by their open time.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <param name="interval">Time interval to retreive.</param>
        /// <param name="limit">Limit of records to retrieve.</param>
        /// <returns></returns>
        Task<List<BinanceCandlestick>> GetCandleSticksAsync(string symbol, BinanceTimeInterval interval, int limit = 500);

        /// <summary>
        /// 24 hour price change statistics for specified symbol.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <returns></returns>
        Task<BinancePriceChangeInfo> GetPriceChange24HAsync(string symbol);

        /// <summary>
        /// 24 hour all prices change statistics
        /// </summary>
        /// <returns></returns>
        List<BinancePriceChangeInfo> GetAllPricesChange24H();
        /// <summary>
        /// 24 hour all prices change statistics
        /// </summary>
        /// <returns></returns>
        Task<List<BinancePriceChangeInfo>> GetAllPricesChange24HAsync();
        /// <summary>
        /// Latest price for a pair symbol
        /// </summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        BinanceSymbolPrice GetPrice(string symbol);
        /// <summary>
        /// Latest price for a pair symbol
        /// </summary>
        /// <param name="symbol">Symobl example: ETHBTC, XRPBTC, ZECBTC, ... </param>
        /// <returns></returns>
        Task<BinanceSymbolPrice> GetPriceAsync(string symbol);
        /// <summary>
        /// Current average price for a symbol.
        /// </summary>
        /// <param name="symbol">Symobl example: ETHBTC, XRPBTC, ZECBTC, ... </param>
        /// <returns></returns>
        BinanceAveragePrice GetAveragePrice(string symbol);

        /// <summary>
        /// Current average price for a symbol.
        /// </summary>
        /// <param name="symbol">Symobl example: ETHBTC, XRPBTC, ZECBTC, ... </param>
        /// <returns></returns>
        Task<BinanceAveragePrice> GetAveragePriceAsync(string symbol);

        /// <summary>
        /// Latest price for all symbols.
        /// </summary>
        /// <returns>List of BinanceSymbolPrice object</returns>
        List<BinanceSymbolPrice> GetAllPrices();

        /// <summary>
        /// Latest price for all symbols.
        /// </summary>
        /// <returns>List of BinanceSymbolPrice object</returns>
        Task<List<BinanceSymbolPrice>> GetAllPricesAsync();

        /// <summary>
        /// Best price/qty on the order book for all symbols.
        /// </summary>
        /// <returns></returns>
        Task<List<BinanceOrderBookTicker>> GetOrderBookTickerAsync();
        #endregion

        #region Account Information
        /// <summary>
        /// Send in a new order.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <param name="quantity">Quantity to transaction.</param>
        /// <param name="price">Price of the transaction.</param>
        /// <param name="orderType">Order type Async(LIMIT-MARKET).</param>
        /// <param name="side">Order side Async(BUY-SELL).</param>
        /// <param name="timeInForce">Indicates how long an order will remain active before it is executed or expires.</param>
        /// <param name="recvWindow">Specific number of milliseconds the request is valid for.</param>
        /// <returns></returns>
        BinanceNewOrder PostNewOrder(string symbol, decimal quantity, decimal price, BinanceOrderSide side, BinanceOrderType orderType = BinanceOrderType.LIMIT, BinanceTimeInForce timeInForce = BinanceTimeInForce.GTC, decimal icebergQty = 0m, long recvWindow = 59999);

        /// <summary>
        /// Send in a new order.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <param name="quantity">Quantity to transaction.</param>
        /// <param name="price">Price of the transaction.</param>
        /// <param name="orderType">Order type Async(LIMIT-MARKET).</param>
        /// <param name="side">Order side Async(BUY-SELL).</param>
        /// <param name="timeInForce">Indicates how long an order will remain active before it is executed or expires.</param>
        /// <param name="recvWindow">Specific number of milliseconds the request is valid for.</param>
        /// <returns></returns>
        Task<BinanceNewOrder> PostNewOrderAsync(string symbol, decimal quantity, decimal price, BinanceOrderSide side, BinanceOrderType orderType = BinanceOrderType.LIMIT, BinanceTimeInForce timeInForce = BinanceTimeInForce.GTC, decimal icebergQty = 0m, long recvWindow = 59999);

        /// <summary>
        /// Test new order creation and signature/recvWindow long. Creates and validates a new order but does not send it into the matching engine.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <param name="quantity">Quantity to transaction.</param>
        /// <param name="price">Price of the transaction.</param>
        /// <param name="orderType">Order type Async(LIMIT-MARKET).</param>
        /// <param name="side">Order side Async(BUY-SELL).</param>
        /// <param name="timeInForce">Indicates how long an order will remain active before it is executed or expires.</param>
        /// <param name="recvWindow">Specific number of milliseconds the request is valid for.</param>
        /// <returns></returns>
        Task<dynamic> PostNewOrderTestAsync(string symbol, decimal quantity, decimal price, BinanceOrderSide side, BinanceOrderType orderType = BinanceOrderType.LIMIT, BinanceTimeInForce timeInForce = BinanceTimeInForce.GTC, decimal icebergQty = 0m, long recvWindow = 59999);

        /// <summary>
        /// Check an order's status.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <param name="orderId">Id of the order to retrieve.</param>
        /// <param name="origClientOrderId">origClientOrderId of the order to retrieve.</param>
        /// <param name="recvWindow">Specific number of milliseconds the request is valid for.</param>
        /// <returns></returns>
        Task<BinanceOrder> GetOrderAsync(string symbol, long? orderId = null, string origClientOrderId = null, long recvWindow = 59999);

        /// <summary>
        /// Cancel an active order.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <param name="orderId">Id of the order to cancel.</param>
        /// <param name="origClientOrderId">origClientOrderId of the order to cancel.</param>
        /// <param name="recvWindow">Specific number of milliseconds the request is valid for.</param>
        /// <returns></returns>
        Task<BinanceCanceledOrder> CancelOrderAsync(string symbol, long? orderId = null, string origClientOrderId = null, long recvWindow = 59999);

        /// <summary>
        /// Get all open orders on a symbol.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <param name="recvWindow">Specific number of milliseconds the request is valid for.</param>
        /// <returns></returns>
        Task<List<BinanceOrder>> GetCurrentOpenOrdersAsync(string symbol, long recvWindow = 59999);

        /// <summary>
        /// Get all account orders; active, canceled, or filled.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <param name="orderId">If is set, it will get orders >= that orderId. Otherwise most recent orders are returned.</param>
        /// <param name="limit">Limit of records to retrieve.</param>
        /// <param name="recvWindow">Specific number of milliseconds the request is valid for.</param>
        /// <returns></returns>
        Task<List<BinanceOrder>> GetAllOrdersAsync(string symbol, long? orderId = null, int limit = 500, long recvWindow = 59999);

        /// <summary>
        /// Get current account information.
        /// </summary>
        /// <param name="recvWindow">Specific number of milliseconds the request is valid for.</param>
        /// <returns></returns>
        BinanceAccountInfo GetAccountInfo(long recvWindow = 59999);

        /// <summary>
        /// Get current account information.
        /// </summary>
        /// <param name="recvWindow">Specific number of milliseconds the request is valid for.</param>
        /// <returns></returns>
        Task<BinanceAccountInfo> GetAccountInfoAsync(long recvWindow = 59999);

        /// <summary>
        /// Get trades for a specific account and symbol.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <param name="recvWindow">Specific number of milliseconds the request is valid for.</param>
        /// <returns></returns>
        Task<List<BinanceTrade>> GetTradeListAsync(string symbol, long recvWindow = 59999);

        /// <summary>
        /// Get deposit address for a specific asset
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="recvWindow"></param>
        /// <returns></returns>
        Task<BinanceDepositAddress> GetDepositAddressAsync(string asset, long recvWindow = 59999);

        /// <summary>
        /// Submit a withdraw request.
        /// </summary>
        /// <param name="asset">Asset to withdraw.</param>
        /// <param name="amount">Amount to withdraw.</param>
        /// <param name="address">Address where the asset will be deposited.</param>
        /// <param name="addressName">Address name.</param>
        /// <param name="recvWindow">Specific number of milliseconds the request is valid for.</param>
        /// <returns></returns>
        Task<BinanceWithdrawResponse> WithdrawAsync(string asset, decimal amount, string address, string addressTag = "", string addressName = "", long recvWindow = 59999);

        /// <summary>
        /// Submit a withdraw request.
        /// </summary>
        /// <param name="asset">Asset to withdraw.</param>
        /// <param name="amount">Amount to withdraw.</param>
        /// <param name="address">Address where the asset will be deposited.</param>
        /// <param name="addressName">Address name.</param>
        /// <param name="recvWindow">Specific number of milliseconds the request is valid for.</param>
        /// <returns></returns>
        BinanceWithdrawResponse Withdraw(string asset, decimal amount, string address, string addressTag = "", string addressName = "", long recvWindow = 59999);
        /// <summary>
        /// Fetch asset detail.
        /// </summary>
        /// <returns></returns>
        Task<BinanceAssetsDetails> AssetsDetailAsync();

        /// <summary>
        /// Fetch deposit history.
        /// </summary>
        /// <param name="asset">Asset you want to see the information for.</param>
        /// <param name="status">Deposit status.</param>
        /// <param name="startTime">Start time. </param>
        /// <param name="endTime">End time.</param>
        /// <param name="recvWindow">Specific number of milliseconds the request is valid for.</param>
        /// <returns></returns>
        Task<BinanceDepositHistory> GetDepositHistoryAsync(string asset, BinanceDepositStatus? status = null, DateTime? startTime = null, DateTime? endTime = null, long recvWindow = 59999);

        /// <summary>
        /// Fetch withdraw history.
        /// </summary>
        /// <param name="asset">Asset you want to see the information for.</param>
        /// <param name="status">Withdraw status.</param>
        /// <param name="startTime">Start time. </param>
        /// <param name="endTime">End time.</param>
        /// <param name="recvWindow">Specific number of milliseconds the request is valid for.</param>
        /// <returns></returns>
        Task<BinanceWithdrawHistory> GetWithdrawHistoryAsync(string asset = null, BinanceWithdrawStatus? status = null, DateTime? startTime = null, DateTime? endTime = null, long recvWindow = 59999);
        #endregion

        #region User Stream 
        /// <summary>
        /// Start a new user data stream.
        /// </summary>
        /// <returns></returns>
        Task<BinanceUserStreamInfo> StartUserStreamAsync();

        /// <summary>
        /// PING a user data stream to prevent a time out.
        /// </summary>
        /// <param name="listenKey">Listenkey of the user stream to keep alive.</param>
        /// <returns></returns>
        Task<dynamic> KeepAliveUserStreamAsync(string listenKey);

        /// <summary>
        /// Close out a user data stream.
        /// </summary>
        /// <param name="listenKey">Listenkey of the user stream to close.</param>
        /// <returns></returns>
        Task<dynamic> CloseUserStreamAsync(string listenKey);
        #endregion

        #region WebSocket
        /// <summary>
        /// Listen to the Depth endpoint.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <param name="depthHandler">Handler to be used when a message is received.</param>
        //void ListenDepthEndpointAsync(string symbol, BinanceMessageHandler<BinanceDepthMessage> messageHandler);

        /// <summary>
        /// Listen to the Kline endpoint.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <param name="interval">Time interval to retreive.</param>
        /// <param name="klineHandler">Handler to be used when a message is received.</param>
        //void ListenKlineEndpointAsync(string symbol, BinanceTimeInterval interval, BinanceMessageHandler<BinanceKlineMessage> messageHandler);

        /// <summary>
        /// Listen to the Trades endpoint.
        /// </summary>
        /// <param name="symbol">Ticker symbol.</param>
        /// <param name="tradeHandler">Handler to be used when a message is received.</param>
        //void ListenTradeEndpointAsync(string symbol, BinanceMessageHandler<BinanceAggregateTradeMessage> messageHandler);

        /// <summary>
        /// Listen to the User Data endpoint.
        /// </summary>
        /// <param name="accountInfoHandler">Handler to be used when a account message is received.</param>
        /// <param name="tradesHandler">Handler to be used when a trade message is received.</param>
        /// <param name="ordersHandler">Handler to be used when a order message is received.</param>
        /// <returns></returns>
        //string ListenUserDataEndpointAsync(BinanceMessageHandler<BinanceAccountUpdatedMessage> accountInfoHandler, BinanceMessageHandler<BinanceOrderOrTradeUpdatedMessage> tradesHandler, BinanceMessageHandler<BinanceOrderOrTradeUpdatedMessage> ordersHandler);
        #endregion

        #region Custom Async Methods
        Task<BinanceBalanceResultModel> GetAllBalancesAsync(List<BinanceSymbolPrice> prices = null, bool zeroIncluded = false);
        Task<decimal> GetAvailableBalanceByAssetAsync(string asset);
        /// <summary>
        /// Fix trade min notional and step size
        /// </summary>
        /// <param name="symbol"></param>
        /// <param name="tradeAmount"></param>
        /// <param name="tradeRules"></param>
        /// <returns></returns>
        decimal FixBinanceTradeSize(string symbol, decimal amount, decimal avgPrice = 0);
        #endregion

        #region SAPI

        /// <summary>
        /// Get information of coins (available for deposit and withdraw) for user.
        /// </summary>
        /// <returns></returns>
        List<BinanceCoinInfo> GetAllCoinsInformation();
        /// <summary>
        /// Get information of coins (available for deposit and withdraw) for user.
        /// </summary>
        /// <returns></returns>
        Task<List<BinanceCoinInfo>> GetAllCoinsInformationAsync();

        /// <summary>
        /// Withdraw from spot wallet
        /// </summary>
        /// <param name="asset">"BTC", "ETH", etc.</param>
        /// <param name="amount">decimal amount</param>
        /// <param name="address">Withdraw to this address</param>
        /// <param name="addressTag">Secondary address identifier for coins like XRP,XMR etc.</param>
        /// <param name="network"></param>
        /// <param name="withdrawOrderId"></param>
        /// <param name="name">Description of the address. Space in name should be encoded into %20.</param>
        /// <param name="transactionFeeFlag">When making internal transfer, true for returning the fee to the destination account; false for returning the fee back to the departure account. Default false.</param>
        /// <param name="recvWindow"></param>
        /// <returns></returns>
        Task<BinanceWithdrawResponse> WithdrawSAPIAsync(string asset, decimal amount, string address, string addressTag = null, string network = null, string withdrawOrderId = null, string name = null, bool? transactionFeeFlag = null, long recvWindow = 59999);

        /// <summary>
        /// Fetch deposit address with network.
        /// </summary>
        /// <param name="asset">Coin: "BTC", "ETH", etc.</param>
        /// <param name="network">(optional) If network is not send, return with default network of the coin. You can get network and isDefault in networkList in the response of GetAllCoinsInfo method</param>
        /// <param name="recvWindow"></param>
        /// <returns></returns>
        Task<BinanceDepositAddressSAPI> GetDepositAddressAsync(string asset, string network = null, long recvWindow = 59999);
        #endregion
    }
}
