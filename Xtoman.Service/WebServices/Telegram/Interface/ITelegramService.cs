﻿using System.Threading.Tasks;
using Xtoman.Service.WebServices.Telegram.Models;

namespace Xtoman.Service.WebServices
{
    public interface ITelegramService
    {
        string Token { get; set; }
        SendMessageResult SendMessage(string text, string chanelId, ParseMode ParseMode = ParseMode.Html, bool disableWebPagePreview = true, int? replyToMessageId = null);
        Task<SendMessageResult> SendMessageAsync(string text, string chanelId, ParseMode ParseMode = ParseMode.Html, bool disableWebPagePreview = true, int? replyToMessageId = null);
        Task<SendMessageResult> SendImageAsync(string text, string channelId, string filePath, ParseMode parseMode = ParseMode.Html);
    }
}
