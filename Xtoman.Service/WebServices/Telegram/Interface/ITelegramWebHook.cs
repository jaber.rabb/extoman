﻿using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace Xtoman.Service.WebServices
{
    public interface ITelegramWebHook
    {
        Task HandleUpdateAsync(Update update);
        Task<string> SetWebHookAsync(string updateUrl);
        Task<string> DeleteWebHookAsync();
    }
}
