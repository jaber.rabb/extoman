﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Service.WebServices.Telegram.Models;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class TelegramService : WebServiceManager, ITelegramService
    {
        //private static readonly TelegramBotClient Bot = new TelegramBotClient("");
        private string host = "https://api.telegram.org";
        public string Token { get; set; }
        public TelegramService()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            Token = Token.HasValue() ? Token : "504321652:AAFxsOfyvcBRqlzgV9TsHUjtyyMb8HyXBhw";
        }
        public TelegramService(string token = "") : base() => Token = token.HasValue() ? token : Token;

        public SendMessageResult SendMessage(string text, string chanelId, ParseMode ParseMode = ParseMode.Html, bool disableWebPagePreview = true, int? replyToMessageId = null)
        {
            if (!AppSettingManager.IsLocale)
            {
                text = text.Replace("\n", "%0D%0A");
                var url = $"{host}/bot{Token}/sendMessage?chat_id={chanelId}&parse_mode={ParseMode}&disable_web_page_preview={disableWebPagePreview.ToString().ToLower()}&text={text}" + (replyToMessageId != null ? $"&reply_to_message_id={replyToMessageId}" : "");
                var json = Http.Get(url);
                var result = LogAndDeserialize<SendMessageResult>(json);
                return result;
            }
            return new SendMessageResult();
        }
        public async Task<SendMessageResult> SendMessageAsync(string text, string chanelId, ParseMode ParseMode = ParseMode.Html, bool disableWebPagePreview = true, int? replyToMessageId = default(int?))
        {
            try
            {
                if (!AppSettingManager.IsLocale && text.HasValue())
                {
                    text = text.Replace("\n", "%0D%0A");
                    var url = $"{host}/bot{Token}/sendMessage?chat_id={chanelId}&parse_mode={ParseMode}&disable_web_page_preview={disableWebPagePreview.ToString().ToLower()}&text={text}" + (replyToMessageId != null ? $"&reply_to_message_id={replyToMessageId}" : "");
                    var json = await Http.GetAsync(url, 10000).ConfigureAwait(false);
                    var result = LogAndDeserialize<SendMessageResult>(json);
                    return result;
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
            return new SendMessageResult();
        }

        public async Task<SendMessageResult> SendImageAsync(string text, string channelId, string filePath, ParseMode parseMode = ParseMode.Html)
        {
            text = text.Replace("\n", "%0D%0A");
            var url = $"{host}/bot{Token}/sendPhoto";
            var fileName = filePath.Split('\\').Last();
            using (var form = new MultipartFormDataContent())
            {
                form.Add(new StringContent(channelId.ToString(), Encoding.UTF8), "chat_id");
                form.Add(new StringContent(text, Encoding.UTF8), "caption");
                form.Add(new StringContent(parseMode.ToString(), Encoding.UTF8), "parse_mode");
                using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    form.Add(new StreamContent(fileStream), "photo", fileName);

                    using (var client = new HttpClient())
                    {
                        var postResult = await client.PostAsync(url, form).ConfigureAwait(false);
                        var json = await postResult.Content.ReadAsStringAsync();
                        var result = LogAndDeserialize<SendMessageResult>(json);
                        return result;
                    }
                }
            }
        }
    }
}
