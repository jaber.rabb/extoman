﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Service.WebServices.Telegram.Models
{
    public enum ParseMode : byte
    {
        Html = 8,
        Text = 16
    }
}
