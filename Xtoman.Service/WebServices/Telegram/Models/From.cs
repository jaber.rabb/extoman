﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Service.WebServices.Telegram.Models
{
    public class From
    {
        public long id { get; set; }
        public string first_name { get; set; }
        public string username { get; set; }
    }
}
