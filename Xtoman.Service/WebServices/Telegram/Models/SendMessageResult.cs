﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Service.WebServices.Telegram.Models
{
    public class SendMessageResult
    {
        public bool ok { get; set; }
        public Result result { get; set; }
    }
}
