﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Service.WebServices.Telegram.Models
{
    public class Chat
    {
        public decimal id { get; set; }
        public string title { get; set; }
        public string username { get; set; }
        public string type { get; set; }
    }
}
