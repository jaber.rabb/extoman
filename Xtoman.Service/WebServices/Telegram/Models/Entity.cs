﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xtoman.Service.WebServices.Telegram.Models
{
    public class Entity
    {
        public string type { get; set; }
        public int offset { get; set; }
        public int length { get; set; }
    }
}
