﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class TelegramWebHook : ITelegramWebHook
    {
        #region Properties
        //private readonly TelegramBotClient _telegramBotClient = new TelegramBotClient("537457167:AAFSFxFmEv8KyN7oBAp6D9QhjhNGlkB4OI8");
        private readonly TelegramBotClient _telegramBotClient = new TelegramBotClient("629953180:AAFOWpIAIGIeYL4HeXdrAY4WoYgNbmRNQYg");
        #endregion

        #region Constructor
        public TelegramWebHook()
        {
        }
        #endregion

        #region Async Methods
        public async Task HandleUpdateAsync(Update update)
        {
            new Exception(update.Serialize()).LogError();
            long chatId = 0;

            switch (update.Type)
            {
                case UpdateType.Unknown:
                    await _telegramBotClient.SendTextMessageAsync(new ChatId(chatId), $"نوع پیام: نامشخص");
                    break;
                case UpdateType.Message:
                    var message = update.Message;
                    chatId = message.Chat.Id;
                    //await _telegramBotClient.SendTextMessageAsync(new ChatId(chatId), $"نوع پیام: متن | پیام ارسالی: {update.Message.Text}");
                    await HandleMessageAsync(update.Message);
                    break;
                case UpdateType.InlineQuery:
                    await _telegramBotClient.SendTextMessageAsync(new ChatId(chatId), $"نوع پیام: InlineQuery | {update.InlineQuery.Query}");
                    break;
                case UpdateType.ChosenInlineResult:
                    await _telegramBotClient.SendTextMessageAsync(new ChatId(chatId), $"نوع پیام: ChosenInlineResult");
                    break;
                case UpdateType.CallbackQuery:
                    await _telegramBotClient.SendTextMessageAsync(new ChatId(chatId), $"نوع پیام: CallbackQuery | اطلاعات: {update.CallbackQuery.Data}");
                    break;
                case UpdateType.EditedMessage:
                    await _telegramBotClient.SendTextMessageAsync(new ChatId(chatId), $"نوع پیام: متن ویرایش شده");
                    break;
                case UpdateType.ChannelPost:
                    await _telegramBotClient.SendTextMessageAsync(new ChatId(chatId), $"نوع پیام: ChannelPost");
                    break;
                case UpdateType.EditedChannelPost:
                    await _telegramBotClient.SendTextMessageAsync(new ChatId(chatId), $"نوع پیام: EditedChannelPost");
                    break;
                case UpdateType.ShippingQuery:
                    await _telegramBotClient.SendTextMessageAsync(new ChatId(chatId), $"نوع پیام: ShippingQuery");
                    break;
                case UpdateType.PreCheckoutQuery:
                    await _telegramBotClient.SendTextMessageAsync(new ChatId(chatId), $"نوع پیام: PreCheckoutQuery");
                    break;
            }

            await _telegramBotClient.SendTextMessageAsync(new ChatId(chatId), $"UpdateId: {update.Id}, ChatId: {chatId}, MessageId: {(update.Message != null ? update.Message.MessageId : 0)}");
        }

        private async Task HandleMessageAsync(global::Telegram.Bot.Types.Message message)
        {
            var chat = message.Chat;
            if (message.Entities != null)
            {
                foreach (var entity in message.Entities)
                {
                    //await _telegramBotClient.SendTextMessageAsync(new ChatId(chat.Id), $"نوع Entity: {entity.Type.ToString()}");
                    switch (entity.Type)
                    {
                        case MessageEntityType.Mention:
                            break;
                        case MessageEntityType.Hashtag:
                            break;
                        case MessageEntityType.BotCommand:
                            await HandleBotCommandAsync(message);
                            break;
                        case MessageEntityType.Url:
                            break;
                        case MessageEntityType.Email:
                            break;
                        case MessageEntityType.Bold:
                            break;
                        case MessageEntityType.Italic:
                            break;
                        case MessageEntityType.Code:
                            break;
                        case MessageEntityType.Pre:
                            break;
                        case MessageEntityType.TextLink:
                            break;
                        case MessageEntityType.TextMention:
                            break;
                        case MessageEntityType.PhoneNumber:
                            break;
                        case MessageEntityType.Cashtag:
                            break;
                        case MessageEntityType.Unknown:
                            break;
                        default:
                            break;
                    }
                }
            }

            await _telegramBotClient.SendTextMessageAsync(new ChatId(chat.Id), $"نوع پیام : {message.Type.ToString()}");
        }

        private async Task HandleBotCommandAsync(global::Telegram.Bot.Types.Message message)
        {
            switch (message.Text)
            {
                case "/start":
                    ReplyKeyboardMarkup StartReplyKeyboard = new[]
                    {
                        new[] { "🔏 ثبت نام", "🔓 ورود" },
                        new[] { "راهنما" },
                    };

                    var startText = new StringBuilder();
                    startText.AppendLine("به 🤖 ربات اکس تومن خوش آمدید.");
                    startText.AppendLine("برای شروع اگر عضو وب سایت یا ربات اکس تومن هستید از منوی زیر، ورود را بزنید در غیر این صورت ثبت نام نمایید.");
                    startText.AppendLine("برای دریافت راهنمای ربات از دستور /help استفاده نمایید یا بر روی دکمه راهنما کلیک کنید.");
                    startText.AppendLine("----------------------------------");
                    startText.AppendLine("آدرس وب سایت: www.extoman.co");
                    startText.AppendLine("کانال تلگرام: @extoman");
                    startText.AppendLine("پشتیبانی تلگرام: @extomansupport");
                    await _telegramBotClient.SendTextMessageAsync(message.Chat.Id, startText.ToString(), replyMarkup: StartReplyKeyboard);
                    break;
                case "/cancel":
                    break;
                case "/buy":
                    break;
                case "/sell":
                    break;
                case "/exchange":
                    break;
                case "/help":
                    break;
                case "/price":
                    break;
                default:
                    break;
            }
        }

        public async Task<string> SetWebHookAsync(string updateUrl)
        {
            try
            {
                await _telegramBotClient.SetWebhookAsync(updateUrl).ConfigureAwait(false);
                return "Webhook راه اندازی شد.";
            }
            catch (Exception ex)
            {
                var exMsg = ex.Message;
                if (ex.InnerException != null)
                    exMsg += " | " + ex.InnerException.Message;
                return exMsg;
            }
        }

        public async Task<string> DeleteWebHookAsync()
        {
            try
            {
                await _telegramBotClient.SetWebhookAsync("").ConfigureAwait(false);
                return "Webhook حذف شد.";
            }
            catch (Exception ex)
            {
                var exMsg = ex.Message;
                if (ex.InnerException != null)
                    exMsg += " | " + ex.InnerException.Message;
                return exMsg;
            }
        }
        #endregion
    }
}
