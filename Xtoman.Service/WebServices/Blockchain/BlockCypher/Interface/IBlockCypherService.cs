﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Blockchain;

namespace Xtoman.Service.WebServices
{
    public interface IBlockCypherService
    {
        #region Blockchain
        /// <summary>
        /// General information about a blockchain is available by GET-ing the base resource.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <returns>The returned object contains a litany of information about the blockchain, including its height, the time/hash of the latest block, and more.</returns>
        Task<BlockcypherBlockchain> GetBlockchainAsync(BlockcypherNetwork network);

        /// <summary>
        /// If you want more data on a particular block, you can use the Block Hash endpoint.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="input">txstart, Filters response to only include transaction hashes after txstart in the block. limit, Filters response to only include a maximum of limit transactions hashes in the block. Maximum value allowed is 500.</param>
        /// <returns>The returned object contains information about the block, including its height, the total amount of satoshis transacted within it, the number of transactions in it, transaction hashes listed in the canonical order in which they appear in the block, and more. For more detail on the data returned, check the Block object.</returns>
        Task<BlockcypherBlock> GetBlockAsync(BlockcypherNetwork network, string blockHash, int? txstart, int? limit);

        /// <summary>
        /// You can also query for information on a block using its height, using the same resource but with a different variable type.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="input">txstart, Filters response to only include transaction hashes after txstart in the block. limit, Filters response to only include a maximum of limit transactions hashes in the block. Maximum value allowed is 500.</param>
        /// <returns>As above, the returned object contains information about the block, including its hash, the total amount of satoshis transacted within it, the number of transactions in it, transaction hashes listed in the canonical order in which they appear in the block, and more. For more detail on the data returned, check the Block object.</returns>
        Task<BlockcypherBlock> GetBlockHeightAsync(BlockcypherNetwork network, string blockHeight, int? txstart, int? limit);

        /// <summary>
        /// If you’re curious about the adoption of upgrade features on a blockchain, you can use this endpoint to get some information about its state on the network. For example, for bip65 on bitcoin, you could check its state via this URL: https://api.blockcypher.com/v1/btc/main/feature/bip65?token=YOURTOKEN. Generally speaking, for bitcoin, this will follow the form of tracking bipXX (where XX = a number), but the list of features we’re tracking is always changing.
        /// </summary>
        /// <returns></returns>
        Task<BlockcypherFeature> GetFeatureAsync(string name);
        #endregion

        #region Address
        // BlockCypher’s Address API allows you to look up information about public addresses on the blockchain, generate single-use, low-value key pairs with corresponding addresses, help generate multisig addresses, and collect multiple addresses into a single shortcut for address viewing, all based on the coin/chain resource you’ve selected for your endpoints.
        // If you’re new to blockchains, you can think of public addresses as similar to bank account numbers in a traditional ledger.The biggest differences:
        // * Anyone can generate a public address themselves(through ECDSA in Bitcoin). No single authority is needed to generate new addresses; it’s just public-private key cryptography.
        // * Public addresses are significantly more lightweight.Consequently, and unlike traditional bank accounts, you can (and should!) generate new addresses for every transaction.
        // * Addresses can also leverage pay-to-script-hash, which means they can represent exotic things beyond a single private-public key pair; the most prominent example being multi-signature addresses that require n-of-m signatures to spend.
        /// <summary>
        /// The Address Balance Endpoint is the simplest—and fastest—method to get a subset of information on a public address.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="addressOrWalletName">ADDRESS is a string representing the public address (or wallet/HD wallet name) you’re interested in querying</param>
        /// <param name="omitWalletAddresses">(Optional) If omitWalletAddresses is true and you’re querying a Wallet or HDWallet, the response will omit address information (useful to speed up the API call for larger wallets).</param>
        /// <returns>The returned object contains information about the address, including its balance in satoshis and the number of transactions associated with it. The endpoint omits any detailed transaction information, but if that isn’t required by your application, then it’s the fastest and preferred way to get public address information.</returns>
        Task<BlockcypherAddress> GetAddressBalanceAsync(BlockcypherNetwork network, string addressOrWalletName, bool? omitWalletAddresses);

        /// <summary>
        /// The default Address Endpoint strikes a balance between speed of response and data on Addresses. It returns more information about an address’ transactions than the Address Balance Endpoint but doesn’t return full transaction information (like the Address Full Endpoint).
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="addressOrWalletName">(Optional) ADDRESS is a string representing the public address (or wallet/HD wallet name) you’re interested in querying</param>
        /// <param name="unspentOnly">(Optional) If unspentOnly is true, filters response to only include unspent transaction outputs (UTXOs).</param>
        /// <param name="includeScript">(Optional) If includeScript is true, includes raw script of input or output within returned TXRefs.</param>
        /// <param name="includeConfidence">(Optional) If true, includes the confidence attribute (useful for unconfirmed transactions) within returned TXRefs. For more info about this figure, check the Confidence Factor documentation.</param>
        /// <param name="before">(Optional) Filters response to only include transactions below before height in the blockchain.</param>
        /// <param name="after">(Optional) Filters response to only include transactions above after height in the blockchain.</param>
        /// <param name="limit">(Optional) limit sets the minimum number of returned TXRefs; there can be less if there are less than limit TXRefs associated with this address, but there can be more in the rare case of more TXRefs in the block at the bottom of your call. This ensures paging by block height never misses TXRefs. Defaults to 200, maximum is 2000.</param>
        /// <param name="confirmations">(Optional) If set, only returns the balance and TXRefs that have at least this number of confirmations.</param>
        /// <param name="confidence">(Optional) Filters response to only include TXRefs above confidence in percent; e.g., if this is set to 99, will only return TXRefs with 99% confidence or above (including all confirmed TXRefs). For more detail on confidence, check the Confidence Factor documentation.</param>
        /// <param name="omitWalletAddresses">(Optional) If omitWalletAddresses is true and you’re querying a Wallet or HDWallet, the response will omit address information (useful to speed up the API call for larger wallets).</param>
        /// <returns>The returned object contains information about the address, including its balance in satoshis, the number of transactions associated with it, and transaction inputs/outputs in descending order by block height—and if multiple transaction inputs/outputs associated with this address exist within the same block, by descending block index (position in block).</returns>
        Task<BlockcypherAddress> GetAddressAsync(BlockcypherNetwork network, string addressOrWalletName, bool? unspentOnly = null, bool? includeScript = null, bool? includeConfidence = null, int? before = null, int? after = null, int? limit = null, int? confirmations = null, int? confidence = null, bool? omitWalletAddresses = null);

        /// <summary>
        /// The Address Full Endpoint returns all information available about a particular address, including an array of complete transactions instead of just transaction inputs and outputs. Unfortunately, because of the amount of data returned, it is the slowest of the address endpoints, but it returns the most detailed data record.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="before">(Optional) Filters response to only include transactions below before height in the blockchain.</param>
        /// <param name="after">(Optional) Filters response to only include transactions above after height in the blockchain.</param>
        /// <param name="limit">(Optional) limit sets the minimum number of returned TXs; there can be less if there are less than limit TXs associated with this address, but there can also be more in the rare case of more TXs in the block at the bottom of your call. This ensures paging by block height never misses TXs. Defaults to 10, maximum is 50.</param>
        /// <param name="txlimit">(Optional) This filters the TXInputs/TXOutputs within the returned TXs to include a maximum of txlimit items.</param>
        /// <param name="confirmations">(Optional) If set, only returns the balance and TXs that have at least this number of confirmations.</param>
        /// <param name="confidence">(Optional) Filters response to only include TXs above confidence in percent; e.g., if this is set to 99, will only return TXs with 99% confidence or above (including all confirmed TXs). For more detail on confidence, check the Confidence Factor documentation.</param>
        /// <param name="includeHex">(Optional) If true, includes hex-encoded raw transaction for each TX; false by default.</param>
        /// <param name="includeConfidence">(Optional) If true, includes the confidence attribute (useful for unconfirmed transactions) within returned TXs. For more info about this figure, check the Confidence Factor documentation.</param>
        /// <param name="omitWalletAddresses">(Optional) If omitWalletAddresses is true and you’re querying a Wallet or HDWallet, the response will omit address information (useful to speed up the API call for larger wallets).</param>
        /// <returns></returns>
        Task<BlockcypherAddress> GetAddressFullAsync(BlockcypherNetwork network, string addressOrWalletName, int? before = null, int? after = null, int? limit = null, int? txlimit = null, int? confirmations = null, int? confidence = null, bool? includeHex = null, bool? includeConfidence = null, bool? omitWalletAddresses = null);

        /// <summary>
        /// The Generate Address endpoint allows you to generate private-public key-pairs along with an associated public address. No information is required with this POST request.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <returns>The returned object contains a private key in hex-encoded and wif-encoded format, a public key, and a public address.</returns>
        Task<BlockcypherAddressKeychain> GenerateAddressAsync(BlockcypherNetwork network);

        /// <summary>
        /// The Generate Multisig Address Endpoint is a convenience method to help you generate multisig addresses from multiple public keys.After supplying a partially filled-out AddressKeychain object (including only an array of hex-encoded public keys and the script type), the returned object includes the computed public address.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <returns>The returned object contains a private key in hex-encoded and wif-encoded format, a public key, and a public address.</returns>
        Task<BlockcypherAddressKeychain> GenerateMultisigAddressAsync(BlockcypherNetwork network, BlockcypherAddressKeychain addressKeyChain);
        #endregion

        #region Wallet
        // Wallet Info:
        // The Wallet API allows you to group multiple addresses under a single name.It only holds public address information and never requires any private keys.
        // Don’t be confused: this Wallet API has nothing to do with private key management.The best way to think of the Wallet API is a “Set of Public Addresses to Query Together” API, but that name refused to fit into any of our marketing materials.
        // A normal Wallet can be created, deleted, and have addresses added and removed. The Wallet itself can have any custom name as long as it does not start with the standard address prefix (1 or 3 for Bitcoin).
        // Hierarchical Deterministic(HD) Wallets
        // We also offer support for HD Wallets, which make it easy to manage multiple addresses under a single name.All HD wallet addresses are derived from a single seed.Please see BIP32 for more background on HD wallets.
        // HD Wallets can be created, deleted, and have new addresses generated.However, unlike normal Wallets, addresses cannot be removed.
        // When creating a wallet, one can optionally include one or more “subchain” indexes.These subchains can later be referenced when generating new addresses or sending txs.If none are provided in wallet creation, the wallet will derive & use addresses straight from the given extended pubkey. If no index is given when using the wallet with other APIs, it defaults to using the wallet’s first(sub) chain.
        // In BIP32 notation, the wallet layout is m/0, m/1, … and m/i/0, m/i/1, … for each subchain i if the wallet has subchains.For example, the path of the fourth address generated is m/3 for a non-subchain wallet. The path of the fourth address at subchain index two is m/2/3. Note that this is different from the default BIP32 wallet layout.
        // If you want to use BIP32 default wallet layout you can submit the extended public key of m/0’ (which can only be derived from your master private key) with subchain indexes = [0, 1]. Subchain index 0 represents the external chain(of account 0) and will discover all k keypairs that look like: m/0’/0/k.Subchain index 1 represents the internal chain(of account 0) and will discover all k keypairs in m/0’/1/k.
        // If you want to use BIP 44 layout(for BTC), you can submit the extended public key of m/44’/0’/0’. (which can only be derived from your master private key) with subchain indexes = [0, 1]. Subchain index 0 represents the external chain(of account 0) and will discover all k keypairs in m/44’/0’/0’/0/k.Subchain index 1 represents the internal chain(of account 0) and will discover all k keypairs in m/44’/0’/0’/1/k.
        // If an address ahead of current addresses listed in an HD Wallet receives a transaction, it will be added, along with any addresses between the new address and the last used one.We limit looking ahead to 20 addresses; if more than 20 addresses are skipped for the next to an HD Wallet, it won’t be detected/added to the HD Wallet.
        // Using Wallets
        // Both HD Wallets and normal Wallets can be leveraged by the Address API, just by using their $NAME instead of $ADDRESS.They can also be used with Events and with the Transaction API.In general, using a wallet instead of an address in an API will have the effect of batching the set of addresses contained in the wallet.
        // You are required to authenticate with your user token when using a Wallet or HD Wallet in any API endpoint.You can register for a token here.
        // The following code examples should be considered serially; that is to say, the results will appear as if each API call were done sequentially.Also, $NAME is a string representing the name of your wallet, for example: alice
        // As you’ll see in the examples, if you’re using HD Wallets, take care to use the appropriate resource(e.g. /wallets/hd instead of /wallets).
        // HD Wallets and Wallets share the same namespace under a particular coin/chain.For example, under a single token, you cannot have both an HD Wallet and normal Wallet named “alice” on Bitcoin Main.

        /// <summary>
        /// This endpoint allows you to create a new Wallet, by POSTing a partially filled out Wallet object;
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="wallet">For normal wallets, at minimum, you must include the name attribute and at least one public address in the addresses array.</param>
        /// <returns>If successful, it will return the same Wallet object you requested, appended with your user token.If the named wallet already exists under your token, attempting to create a new wallet will return an error. As mentioned above, Wallets and HDWallets share the same namespace, so an error will be returned if you attempt to create a named HD Wallet that shares a name with a normal Wallet under your token.</returns>
        Task<BlockcypherWallet> CreateWalletAsync(BlockcypherNetwork network, BlockcypherWallet wallet);

        /// <summary>
        /// This endpoint allows you to create a new HDWallet, by POSTing a partially filled out HDWallet object; For HD wallets, at minimum, you must include the name and the extended_public_key attributes. The encoding of the key is documented here. You can optionally include subchain_indexes to initialize the wallet with one or more subchains. If not given, the wallet will derive address straight from the given extended pubkey. See BIP32 for more info.
        /// </summary>
        /// <param name="hdWallet">For HD wallets, at minimum, you must include the name and the extended_public_key attributes. The encoding of the key is documented here. You can optionally include subchain_indexes to initialize the wallet with one or more subchains. If not given, the wallet will derive address straight from the given extended pubkey. See BIP32 for more info.</param>
        /// <returns>If successful, it will return the same HDWallet object you requested, appended with your user token. If the named wallet already exists under your token, attempting to create a new wallet will return an error. As mentioned above, Wallets and HDWallets share the same namespace, so an error will be returned if you attempt to create a named HD Wallet that shares a name with a normal Wallet under your token.</returns>
        Task<BlockcypherHDWallet> CreateHDWalletAsync(BlockcypherNetwork network, BlockcypherHDWallet hdWallet);

        /// <summary>
        /// List Wallets for token
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <returns></returns>
        Task<BlockcypherWalletNames> GetWalletNamesAsync(BlockcypherNetwork network);

        /// <summary>
        /// Get Wallet
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <returns>This endpoint returns a Wallet or HDWallet based on its $NAME.</returns>
        Task<BlockcypherWallet> GetWalletAsync(BlockcypherNetwork network, string name);

        /// <summary>
        /// Get HD Wallet
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="name">Wallet name</param>
        /// <returns>This endpoint returns a Wallet or HDWallet based on its $NAME.</returns>
        Task<BlockcypherHDWallet> GetHDWalletAsync(BlockcypherNetwork network, string name);


        /// <summary>
        /// This endpoint allows you to add public addresses to the $NAME wallet, by POSTing a partially filled out Wallet object. You only need to include the additional addresses in a new addresses array in the object. If successful, it will return the newly modified Wallet, including an up-to-date, complete listing of addresses.  You cannot add custom addresses to HD Wallets, since they are all derived from your extended public key. You can always generate new addresses client-side, or using the Generate Address in Wallet Endpoint below.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="name">Wallet name</param>
        /// <returns></returns>
        Task<BlockcypherWallet> AddAddressToWalletByNameAsync(BlockcypherNetwork network, string name, BlockcypherWallet wallet, bool? omitWalletAddresses);

        /// <summary>
        /// This endpoint returns a list of the addresses associated with the $NAME wallet. It returns the addresses in a partially filled out Wallet which you’ll find under the addresses attribute. For HD wallets it returns an HDChain object.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="name">Wallet name</param>
        /// <returns></returns>
        Task<BlockcypherWallet> GetWalletAddressesAsync(BlockcypherNetwork network, string name);

        /// <summary>
        /// This endpoint returns a list of the addresses associated with the $NAME wallet. It returns the addresses in a partially filled out Wallet which you’ll find under the addresses attribute. For HD wallets it returns an HDChain object.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="name">Wallet name</param>
        /// <param name="used">Returns only used addresses if set to true and only unused if false. Only valid on HD wallets.</param>
        /// <param name="zerobalance">Returns only addresses with zero balance if set to true and only addresses with non-zero balance if false. Only valid on HD wallets.</param>
        /// <returns></returns>
        Task<BlockcypherHDChain> GetHDWalletAddressesAsync(BlockcypherNetwork network, string name, bool? used, bool? zerobalance);

        /// <summary>
        /// This endpoint allows you to delete an $ADDRESS associated with the $NAME wallet. As a reminder, you can batch multiple addresses by appending them with semicolons within the $ADDRESS URL parameter. If successful, it will return an HTTP 204 status code with no return object. You cannot remove addresses from HD Wallets.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="name">Wallet name</param>
        /// <param name="address">This endpoint allows you to delete an $ADDRESS associated with the $NAME wallet. As a reminder, you can batch multiple addresses by appending them with semicolons within the $ADDRESS URL parameter</param>
        /// <returns>If successful, it will return an HTTP 204 status code with no return object.</returns>
        Task<bool> RemoveWalletAddressAsync(BlockcypherNetwork network, string name, string address);

        /// <summary>
        /// This endpoint allows you to generate a new address associated with the $NAME wallet, similar to the Generate Address Endpoint. If successful, it will returned the newly modified Wallet composed with an AddressKeychain.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="name">Wallet name</param>
        /// <returns>Only works for regular Wallets; for HD Wallets, use the Derive Address endpoint specified below.</returns>
        Task<BlockcypherWalletAddressKeyChain> GenerateAddressInWalletAsync(BlockcypherNetwork network, string name);

        /// <summary>
        /// This endpoint allows you to derive a new address (or multiple addresses) associated with the $NAME HD Wallet. If successful, it will return an HDWallet but only with the newly derived address(es) represented in its chains field to limit the data transmitted; for the full address list after derivation, you can follow up this API call with the Get Wallet Addresses Endpoint.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="name">HD Wallet name</param>
        /// <returns>Only works for HD Wallets; for regular wallets, use the Generate Address in Wallet endpoint specified above.</returns>
        Task<BlockcypherHDWallet> DeriveAddressInHDWalletAsync(BlockcypherNetwork network, string name, int? count, int? subchain_index);

        /// <summary>
        /// Delete Wallet
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="name">Wallet name</param>
        /// <returns>This endpoint deletes the Wallet or HD Wallet with $NAME. If successful, it will return an HTTP 204 status code with no return object.</returns>
        Task<bool> DeleteWalletAsync(BlockcypherNetwork network, string name);

        /// <summary>
        /// Delete Wallet
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="name">HD Wallet name</param>
        /// <returns>This endpoint deletes the Wallet or HD Wallet with $NAME. If successful, it will return an HTTP 204 status code with no return object.</returns>
        Task<bool> DeleteHDWalletAsync(BlockcypherNetwork network, string name);
        #endregion

        #region Transaction
        // BlockCypher’s Transaction API allows you to look up information about unconfirmed transactions, query transactions based on hash, create and propagate your own transactions, including multisignature transactions, and embed data on the blockchain—all based on the coin/chain resource you’ve selected for your endpoints.
        // If you’re new to blockchains, the idea of transactions is relatively self-explanatory.Here’s what’s going on underneath the hood: a transaction takes previous “unspent transaction outputs” (also known as UTXOs) as “transaction inputs” and creates new “locking scripts” on those inputs such that they are “sent” to new addresses(to become new UTXOs). While most of these public addresses are reference points for single private keys that can “unlock” the newly created UTXOs, occasionally they are sent to more exotic addresses through pay-to-script-hash, typically multisignature addresses.
        // Generally speaking, UTXOs are generated from previous transactions(except for Coinbase inputs).
        // Even if you don’t use the API to assist in creating transactions, we highly recommend using BlockCypher to push your raw transactions.BlockCypher maintains connections to 20% of all nodes on all blockchains it supports, which means we can propagate transactions faster than almost anyone else. Given our speed and reliability, this is especially powerful when used in concert with our Confidence Factor.
        // Validating the Data to Sign
        // For the extra cautious, you can protect yourself from a potential malicious attack on BlockCypher by validating the data we’re asking you to sign.Unfortunately, it’s impossible to do so directly, as pre-signed signature data is hashed twice using SHA256. To get around this, set the includeToSignTx URL flag to true. The optional tosign_tx array will be returned within the TXSkeleton, which you can use in the following way:
        // Hashing the hex-encoded string twice using SHA256 should give you back the corresponding tosign data.
        // Decoding the hex-encoded string using our /txs/decode endpoint(or an independent, client-side source) should give you the output addresses and amounts that match your work-in-progress transaction.
        // If you want to automatically empty your input address(es) without knowing their exact value, your TX request object’s value can be set to -1 to sweep all value from your input address(es) to your output address.Please be advised that this only works with a single output address.
        // There are many manually configurable options available via your TX request object. You can read about them in more detail in the Customizing Transaction Requests section below.
        // # next, you sign the data returned in the tosign array locally
        // # here we're using our signer tool (https://github.com/blockcypher/btcutils/tree/master/signer), but any ECDSA secp256k1 signing tool should work
        // # $PRIVATEKEY here is a hex-encoded private key corresponding to the input from address CEztKBAYNoUEEaPYbkyFeXC5v8Jz9RoZH9 
        // ./signer 32b5ea64c253b6b466366647458cfd60de9cd29d7dc542293aa0b8b7300cd827 $PRIVATEKEY
        // 3045022100921fc36b911094280f07d8504a80fbab9b823a25f102e2bc69b14bcd369dfc7902200d07067d47f040e724b556e5bc3061af132d5a47bd96e901429d53c41e0f8cca
        // Locally Sign Your Transaction
        // With your TXSkeleton returned from the New Transaction Endpoint, you now need to use your private key(s) to sign the data provided in the tosign array.
        // Digital signing can be a difficult process, and is where the majority of issues arise when dealing with cryptocurrency transactions. We are working on integrating client-side signing solutions into our libraries to make this process easier. You can read more about signing here.In the mean time, if you want to experiment with client-side signing, consider using our signer tool.
        // One of the most common errors in the signing process is a data format mismatch. We always return and expect hex-encoded data, but oftentimes, standard signing libraries require byte arrays. Remember to convert your data, and always send hex-encoded signatures to BlockCypher.
        // While we demonstrated the simplest use of our two-endpoint process to create transactions, you can have finer-grain control by modifying the TX request object before sending to /txs/new.
        // By default, we allow unconfirmed UTXOs as inputs when creating transactions.If you only want to allow confirmed UTXOs, set the confirmations value in your TX request object to 1.
        // Instead of providing addresses, you can use prev_hash and output_index within the inputs array of your request object, in which case, we’ll use the inputs as provided and not attempt to generate them from a list of addresses. We will compute change and fees the same way.
        // BlockCypher will set the change address to the first transaction input/address listed in the transaction. To redirect this default behavior, you can set an optional change_address field within the TX request object.
        // Fees in cryptocurrencies can be complex. We provide 2 different ways for you to control the fees included in your transactions:
        // Set the preference property in your TX request object to “high”, “medium”, or “low”. This will calculate and include appropriate fees for your transaction to be included in the next 1-2 blocks, 3-6 blocks or 7 or more blocks respectively. You can see the explicit estimates per kilobyte for these high, medium, and low ranges by calling your base resource through the Chain Endpoint.The default fee calculation is based on a “high” preference.A preference set to “zero” will set no fee.
        // Manually set the fee to a desired amount by setting the fees property in your TX request object. Note that a fee too low may result in an error for some transactions that would require it.
        // To learn more about fees, bitcoinfees.com is a good resource.
        // For even more control, you can also change the script_type in the outputs of your partially filled TX.You’ll notice this used to powerful effect in the section on Multisig Transactions.These are the possible script types:
        // # pay-to-pubkey-hash (most common transaction transferring to a public key hash, and the default behavior if no out)
        // # pay-to-multi-pubkey-hash(multi-signatures transaction, now actually less used than pay-to-script-hash for this purpose)
        // # pay-to-pubkey(used for mining transactions)
        // # pay-to-script-hash(used for transactions relying on arbitrary scripts, now used primarily for multi-sig transactions)
        // # multisig-m-of-n(not present in blockchain, but used by BlockCypher to construct P2SH multisig transactions; see Multisig Transactions for more info)
        // # null-data(sometimes called op-return; used to embed small chunks of data in the blockchain)
        // # empty(no script present, mostly used for mining transaction inputs)
        // # unknown(non-standard script)
        // Due to safety checks within the API’s transaction creation process, the user-set value field of an output may never be 0, except when using a null-data script-type, where it must be 0.
        /// <summary>
        /// The Transaction Hash Endpoint returns detailed information about a given transaction based on its hash.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="txhash">TXHASH is a string representing the hex-encoded transaction hash you’re interested in querying.</param>
        /// <param name="limit">(Optional) Filters TXInputs/TXOutputs, if unset, default is 20.</param>
        /// <param name="instart">(Optional) Filters TX to only include TXInputs from this input index and above.</param>
        /// <param name="outstart">(Optional) Filters TX to only include TXOutputs from this output index and above.</param>
        /// <param name="includeHex">(Optional) If true, includes hex-encoded raw transaction; false by default.</param>
        /// <param name="includeConfidence">(Optional) If true, includes the confidence attribute (useful for unconfirmed transactions). For more info about this figure, check the Confidence Factor documentation.</param>
        /// <returns>The returned object contains detailed information about the transaction, including the value transfered, date received, and a full listing of inputs and outputs.</returns>
        Task<BlockcypherTx> GetTransactionAsync(BlockcypherNetwork network, string txhash, int? limit, int? instart, int? outstart, bool? includeHex, bool? includeConfidence);

        /// <summary>
        /// The Unconfirmed Transactions Endpoint returns an array of the latest transactions relayed by nodes in a blockchain that haven’t been included in any blocks.
        /// Due to transaction malleability it can be difficult to deal with transaction hashes before they’ve been confirmed in blocks. Use caution, and consider applying our Confidence Factor to mitigate potential issues.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <returns>The returned object is an array of transactions that haven’t been included in blocks, arranged in reverse chronological order (latest is first, then older transactions follow).</returns>
        Task<List<BlockcypherTx>> GetUnconfirmedTransactionsAsync(BlockcypherNetwork network);

        #region Creating Transaction
        // Using BlockCypher’s API, you can push transactions to blockchains one of two ways:
        // 1- Use a third party library to create your transactions and push raw transactions
        // 2- Use our two-endpoint process outlined below, where in we generate a TXSkeleton based on your input address, output address, and value to transfer.
        // In either case, for security reasons, we never take possession of your private keys. We do use private keys with our Microtransaction API, but they are for low-value transactions and we discard them immediately from our servers’ memory.
        // Always use HTTPS for creating and pushing transactions.

        /// <summary>
        /// To use BlockCypher’s two-endpoint transaction creation tool, first you need to provide the input address(es), output address, and value to transfer (in satoshis). Provide this in a partially-filled out TX request object.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="blockcypherTx">you only need to provide a single public address within the addresses array of both the input and output of your TX request object. You also need to fill in the value with the amount you’d like to transfer from one address to another. If you’d like, you can even use a Wallet instead of addresses as your input. You just need to use two non-standard fields (your wallet_name and wallet_token) within the inputs array in your transaction, instead of addresses</param>
        /// <param name="includeToSignTx">(Optional) If true, includes tosign_tx array in TXSkeleton, useful for validating data to sign; false by default.</param>
        /// <returns>As a return object, you’ll receive a TXSkeleton containing a slightly-more complete TX alongside data you need to sign in the tosign array. You’ll need this object for the next steps of the transaction creation process. The TXSkeleton returned by this endpoint may contain some data that’s temporary or incomplete, like the hash, size, and the inputs’ script fields. This is by design, as the final TX can only be computed once signed data has been added. Do not rely on these fields until they are returned and sent to the network via the Send Transaction Endpoint outlined below.</returns>
        Task<BlockcypherTxSkeleton> NewTransactionAsync(BlockcypherNetwork network, BlockcypherTx blockcypherTx, bool? includeToSignTx);

        /// <summary>
        /// Once you’ve finished signing the tosign array locally, put that (hex-encoded) data into the signatures array of the TXSkeleton. You also need to include the corresponding (hex-encoded) public key(s) in the pubkeys array, in the order of the addresses/inputs provided. Signature and public key order matters, so make sure they are returned in the same order as the inputs you provided.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="blockcypherTxSkeleton"></param>
        /// <returns>If the transaction was successful, you’ll receive a TXSkeleton with the completed TX (which contains its final hash) and an HTTP Status Code 201.</returns>
        Task<BlockcypherTxSkeleton> SendTransactionAsync(BlockcypherNetwork network, BlockcypherTxSkeleton blockcypherTxSkeleton);
        #endregion

        #region Raw Transaction
        /// <summary>
        /// If you’d prefer to use your own transaction library instead of the recommended path of our two-endpoint transaction generation we’re still happy to help you propagate your raw transactions. Simply send your raw hex-encoded transaction to this endpoint and we’ll leverage our huge network of nodes to propagate your transaction faster than anywhere else.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="txHex">{“tx”:$TXHEX} example: 01000000011935b41d12936df99d322ac8972b74ecff7b79408bbccaf1b2eb8015228beac8000000006b483045022100921fc36b911094280f07d8504a80fbab9b823a25f102e2bc69b14bcd369dfc7902200d07067d47f040e724b556e5bc3061af132d5a47bd96e901429d53c41e0f8cca012102152e2bb5b273561ece7bbe8b1df51a4c44f5ab0bc940c105045e2cc77e618044ffffffff0240420f00000000001976a9145fb1af31edd2aa5a2bbaa24f6043d6ec31f7e63288ac20da3c00000000001976a914efec6de6c253e657a9d5506a78ee48d89762fb3188ac00000000</param>
        /// <returns>If it succeeds, you’ll receive a decoded TX object and an HTTP Status Code 201. If you’d like, you can use the decoded transaction hash alongside an Event to track its progress in the network.</returns>
        Task<BlockcypherTx> PushRawTransactionAsync(BlockcypherNetwork network, string txHex);

        /// <summary>
        /// We also offer the ability to decode raw transactions without sending propagating them to the network; perhaps you want to double-check another client library or confirm that another service is sending proper transactions.
        /// </summary>
        /// <param name="network">Bitcoin, Litecoin or Dogecoin</param>
        /// <param name="txHex">{“tx”:$TXHEX} example: 01000000011935b41d12936df99d322ac8972b74ecff7b79408bbccaf1b2eb8015228beac8000000006b483045022100921fc36b911094280f07d8504a80fbab9b823a25f102e2bc69b14bcd369dfc7902200d07067d47f040e724b556e5bc3061af132d5a47bd96e901429d53c41e0f8cca012102152e2bb5b273561ece7bbe8b1df51a4c44f5ab0bc940c105045e2cc77e618044ffffffff0240420f00000000001976a9145fb1af31edd2aa5a2bbaa24f6043d6ec31f7e63288ac20da3c00000000001976a914efec6de6c253e657a9d5506a78ee48d89762fb3188ac00000000</param>
        /// <returns>If it succeeds, you’ll receive your decoded TX object.</returns>
        Task<BlockcypherTx> DecodeRawTransactionAsync(BlockcypherNetwork network, string txHex);
        #endregion

        #region Multisig Transaction

        // Multisignature transactions are made simple by the method described in the Creating Transactions section, but they deserve special mention.In order to use them, you first need to fund a multisignature address. You use the /txs/new endpoint as before, but instead of the outputs addresses array containing public addresses, it instead contains the public keys associated with the new address.In addition, you must select a script_type of mutlisig-n-of-m, where n and m are numbers(e.g., multisig-2-of-3). The code example demonstrates how the partially filled TX request object would appear.
        // After you’ve set up your request object, you send to /txs/new and receive a partially filled TXSkeleton as before, but with data to sign from the source address.Sign this data and include the public key(s) of the source address—as demonstrated in the Creating Transactions—then send along to the /txs/send endpoint.If it returns with an HTTP Status Code 201, then your multisignature address (via a pay-to-script-hash address) is funded.
        // If you only need a pay-to-script-hash address corresponding to N-of-M multisig and don’t want—or need—to fund it immediately, you should use the comparatively easier Generate Multisig Address Endpoint.
        // Spending Multisig Funds
        // {
        //     "inputs": [{
        //         "addresses"   : [pubkey1, pubkey2, pubkey3],
        //         "script_type" : "multisig-2-of-3"
        //     }],
        //     "outputs": [{
        //         "addresses" : [destAddr],
        //         "value"     : 150000
        //     }]
        // }
        // Once funded, you might want to programmatically spend the money in the address at some point.Here the process is similar, but with the inputs and outputs reversed.As you can see in the code sample, you need to provide the public keys within the inputs addresses array, and the corresponding script_type of multisig-n-of-m(e.g., multisig-2-of-3). Then you follow the same process of sending to /txs/new and getting an array of data to be signed.
        // Each party can send their signed data individually to /txs/send and we can correlate the signatures to the public keys; once we have enough signatures we’ll propagate the transaction.Or you can send all needed signatures alongside ordered public keys with a single call to /txs/send.
        // If you send all signatures in a single call, and you’re spending inputs locked by a multisig-n-of-m script_type, remember that you’ll need n signatures for every element in the TXSkeleton’s tosign array.For example, let’s say /txs/new returns two elements in the tosign array, corresponding to two multisig-2-of-3 inputs locked by pubkey1, pubkey2, and pubkey3:
        // {...,"tosign":[data1, data2],...}
        // Then you’d need to return each piece of data signed twice, preserving order between signatures and pubkeys, resulting in four elements for the signatures and pubkeys arrays:
        // {...,"tosign":[data1, data2], "signatures":[data1sig1, data1sig2, data2sig1, data2sig2], "pubkeys":[pubkey1, pubkey2, pubkey1, pubkey2],...}


        #endregion

        #endregion

        #region Microtransaction API

        #endregion
    }
}
