﻿using JackLeitch.RateGate;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Blockchain;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class BlockCypherService : WebServiceManager, IBlockCypherService
    {
        #region Properties
        private readonly string token = "267011ad0cad4fb3be396b10959249da";
        private readonly string bTCMainBaseUrl = "https://api.blockcypher.com/v1/btc/main";
        private readonly string lTCBaseUrl = "https://api.blockcypher.com/v1/ltc/main";
        private readonly string dogeBaseUrl = "https://api.blockcypher.com/v1/doge/main";

        private readonly string bTCSocketUrl = "wss://socket.blockcypher.com/v1/btc/main";
        private readonly string lTCSocketUrl = "wss://socket.blockcypher.com/v1/ltc/main";
        private readonly string dogeSocketUrl = "wss://socket.blockcypher.com/v1/doge/main";
        //Diff api
        private readonly string ethBaseUrl = "https://api.blockcypher.com/v1/eth/main";

        private RateGate _rateGate;
        #endregion

        #region Constructor
        public BlockCypherService()
        {
            _rateGate = new RateGate(1, TimeSpan.FromSeconds(1));
        }
        #endregion

        #region Async Methods

        #region Blockchain
        public async Task<BlockcypherBlockchain> GetBlockchainAsync(BlockcypherNetwork network)
        {
            // Get
            var baseUrl = GetNetworkBaseUrl(network);
            var url = baseUrl;
            return await GetAsync<BlockcypherBlockchain>(url);
        }

        public async Task<BlockcypherBlock> GetBlockAsync(BlockcypherNetwork network, string blockHash, int? txstart, int? limit)
        {
            // Get
            var baseUrl = GetNetworkBaseUrl(network);
            var url = baseUrl + "/blocks/" + blockHash;

            if (txstart.HasValue || limit.HasValue)
                url += "?";

            if (txstart.HasValue)
                url += "txstart=" + txstart.Value;

            if (limit.HasValue)
            {
                if (txstart.HasValue)
                    url += "&";

                url += "limit=" + limit.Value;
            }

            return await GetAsync<BlockcypherBlock>(url);
        }

        public async Task<BlockcypherBlock> GetBlockHeightAsync(BlockcypherNetwork network, string blockHeight, int? txstart, int? limit)
        {
            // Get
            var baseUrl = GetNetworkBaseUrl(network);
            var url = baseUrl + "/blocks/" + blockHeight;

            if (txstart.HasValue || limit.HasValue)
                url += "?";

            if (txstart.HasValue)
                url += "txstart=" + txstart.Value;

            if (limit.HasValue)
            {
                if (txstart.HasValue)
                    url += "&";

                url += "limit=" + limit.Value;
            }

            return await GetAsync<BlockcypherBlock>(url);
        }

        public async Task<BlockcypherFeature> GetFeatureAsync(string name)
        {
            // Get
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/feature/" + name;

            return await GetAsync<BlockcypherFeature>(url);
        }
        #endregion

        public async Task<BlockcypherAddress> GetAddressBalanceAsync(BlockcypherNetwork network, string addressOrWalletName, bool? omitWalletAddresses)
        {
            // Get
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/addrs/" + addressOrWalletName + "/balance";

            if (omitWalletAddresses.HasValue)
                url += "omitWalletAddresses=" + $"{(omitWalletAddresses.Value ? "true" : "false")}";

            return await GetAsync<BlockcypherAddress>(url);
        }

        public async Task<BlockcypherAddress> GetAddressAsync(BlockcypherNetwork network, string addressOrWalletName, bool? unspentOnly = null, bool? includeScript = null, bool? includeConfidence = null, int? before = null, int? after = null, int? limit = null, int? confirmations = null, int? confidence = null, bool? omitWalletAddresses = null)
        {
            // Get
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/addrs/" + addressOrWalletName;

            return await GetAsync<BlockcypherAddress>(url, new { unspentOnly, includeScript, includeConfidence, before, after, limit, confirmations, confidence, omitWalletAddresses });
        }

        public async Task<BlockcypherAddress> GetAddressFullAsync(BlockcypherNetwork network, string addressOrWalletName, int? before = null, int? after = null, int? limit = null, int? txlimit = null, int? confirmations = null, int? confidence = null, bool? includeHex = null, bool? includeConfidence = null, bool? omitWalletAddresses = null)
        {
            // Get
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/addrs/" + addressOrWalletName + "/full";

            return await GetAsync<BlockcypherAddress>(url, new { before, after, limit, confirmations, confidence, includeHex, includeConfidence, omitWalletAddresses });
        }

        public async Task<BlockcypherAddressKeychain> GenerateAddressAsync(BlockcypherNetwork network)
        {
            // Post
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/addrs";

            return await PostAsync<BlockcypherAddressKeychain>(url);
        }

        public async Task<BlockcypherAddressKeychain> GenerateMultisigAddressAsync(BlockcypherNetwork network, BlockcypherAddressKeychain addressKeyChain)
        {
            // Post
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/addrs";

            return await PostAsync<BlockcypherAddressKeychain>(url, addressKeyChain);
        }

        public async Task<BlockcypherWallet> CreateWalletAsync(BlockcypherNetwork network, BlockcypherWallet wallet)
        {
            // Post
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/wallets";

            return await PostAsync<BlockcypherWallet>(url, wallet);
        }

        public async Task<BlockcypherHDWallet> CreateHDWalletAsync(BlockcypherNetwork network, BlockcypherHDWallet hdWallet)
        {
            // Post
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/wallets/hd";

            return await PostAsync<BlockcypherHDWallet>(url, hdWallet);
        }

        public async Task<BlockcypherWalletNames> GetWalletNamesAsync(BlockcypherNetwork network)
        {
            // Get
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/wallets";

            return await GetAsync<BlockcypherWalletNames>(url);
        }

        public async Task<BlockcypherWallet> GetWalletAsync(BlockcypherNetwork network, string name)
        {
            // Get
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/wallets/" + name;

            return await GetAsync<BlockcypherWallet>(url);
        }

        public async Task<BlockcypherHDWallet> GetHDWalletAsync(BlockcypherNetwork network, string name)
        {
            // Get
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/wallets/hd/" + name;

            return await GetAsync<BlockcypherHDWallet>(url);
        }

        public async Task<BlockcypherWallet> AddAddressToWalletByNameAsync(BlockcypherNetwork network, string name, BlockcypherWallet wallet, bool? omitWalletAddresses)
        {
            // Post
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/wallets/" + name + "/addresses";

            if (omitWalletAddresses.HasValue)
                url += $"?omitWalletAddresses={(omitWalletAddresses.Value ? "true" : "false")}";

            return await PostAsync<BlockcypherWallet>(url, wallet);
        }

        public async Task<BlockcypherWallet> GetWalletAddressesAsync(BlockcypherNetwork network, string name)
        {
            // Get
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/wallets/" + name + "/addresses";

            return await GetAsync<BlockcypherWallet>(url);
        }

        public async Task<BlockcypherHDChain> GetHDWalletAddressesAsync(BlockcypherNetwork network, string name, bool? used, bool? zerobalance)
        {
            // Get
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/wallets/hd/" + name + "/addresses";

            return await GetAsync<BlockcypherHDChain>(url, new { used, zerobalance });
        }

        public async Task<bool> RemoveWalletAddressAsync(BlockcypherNetwork network, string name, string address)
        {
            // Delete
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/wallets/" + name + "/addresses?address=" + address;

            return await DeleteAsync(url);
        }

        public async Task<BlockcypherWalletAddressKeyChain> GenerateAddressInWalletAsync(BlockcypherNetwork network, string name)
        {
            // Post
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/wallets/" + name + "/addresses/generate";

            return await PostAsync<BlockcypherWalletAddressKeyChain>(url);
        }

        public async Task<BlockcypherHDWallet> DeriveAddressInHDWalletAsync(BlockcypherNetwork network, string name, int? count, int? subchain_index)
        {
            // Post
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/wallets/hd/" + name + "/addresses/derive";

            if (count.HasValue || subchain_index.HasValue)
                url += "?";

            if (count.HasValue)
                url += "count=" + count.Value;

            if (subchain_index.HasValue)
            {
                if (count.HasValue)
                    url += "&";

                url += "subchain_index=" + subchain_index.Value;
            }

            return await PostAsync<BlockcypherHDWallet>(url);
        }

        public async Task<bool> DeleteWalletAsync(BlockcypherNetwork network, string name)
        {
            // Delete
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/wallets/" + name;

            return await DeleteAsync(url);
        }

        public async Task<bool> DeleteHDWalletAsync(BlockcypherNetwork network, string name)
        {
            // Delete
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/wallets/hd/" + name;

            return await DeleteAsync(url);
        }

        public async Task<BlockcypherTx> GetTransactionAsync(BlockcypherNetwork network, string txhash, int? limit, int? instart, int? outstart, bool? includeHex, bool? includeConfidence)
        {
            // Get
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/txs/" + txhash;

            return await GetAsync<BlockcypherTx>(url, new { limit, instart, outstart, includeHex, includeConfidence });
        }

        public async Task<List<BlockcypherTx>> GetUnconfirmedTransactionsAsync(BlockcypherNetwork network)
        {
            // Get
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/txs";

            return await GetAsync<List<BlockcypherTx>>(url);
        }

        public async Task<BlockcypherTxSkeleton> NewTransactionAsync(BlockcypherNetwork network, BlockcypherTx blockcypherTx, bool? includeToSignTx)
        {
            // Post
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/txs/new";

            if (includeToSignTx.HasValue)
                url += $"?includeToSignTx={(includeToSignTx.Value ? "true" : "false")}";

            return await PostAsync<BlockcypherTxSkeleton>(url, blockcypherTx);
        }

        public async Task<BlockcypherTxSkeleton> SendTransactionAsync(BlockcypherNetwork network, BlockcypherTxSkeleton blockcypherTxSkeleton)
        {
            // Post
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/txs/send";

            // Signing and creating transactions can be one of the trickiest parts of using blockchains in your applications. 
            // Consequently, when an error is encountered when Creating Transactions, we send back an HTTP Status Code 400 alongside a descriptive array of errors within the TXSkeleton.
            // If the transaction was successful, you’ll receive a TXSkeleton with the completed TX (which contains its final hash) and an HTTP Status Code 201.
            return await PostAsync<BlockcypherTxSkeleton>(url, blockcypherTxSkeleton);
        }

        public async Task<BlockcypherTx> PushRawTransactionAsync(BlockcypherNetwork network, string txHex)
        {
            // Post
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/txs/push";

            return await PostAsync<BlockcypherTx>(url, new { tx = txHex });
        }

        public async Task<BlockcypherTx> DecodeRawTransactionAsync(BlockcypherNetwork network, string txHex)
        {
            // Post
            var baseUrl = GetNetworkBaseUrl(BlockcypherNetwork.Bitcoin);
            var url = baseUrl + "/txs/decode";

            return await PostAsync<BlockcypherTx>(url, new { tx = txHex });
        }
        #endregion

        #region Helpers
        private string GetNetworkBaseUrl(BlockcypherNetwork network)
        {
            switch (network)
            {
                default:
                case BlockcypherNetwork.Bitcoin:
                    return bTCMainBaseUrl;
                case BlockcypherNetwork.Litecoin:
                    return lTCBaseUrl;
                case BlockcypherNetwork.Dogecoin:
                    return dogeBaseUrl;
            }
        }

        private async Task<bool> DeleteAsync(string url)
        {
            // If successful, it will return an HTTP 204 status code with no return object.
            var restClient = new RestClient(url);
            return false;
        }

        private async Task<T> GetAsync<T>(string url, object urlParameters = null) where T : new()
        {
            var restClientRequest = new RestClientRequest(url);
            if (urlParameters != null)
            {
                var validProperties = urlParameters.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                foreach (var property in validProperties)
                {
                    var propVal = property.GetValue(urlParameters)?.ToString() ?? "";
                    if (propVal.HasValue())
                        restClientRequest.AddQueryParameter(property.Name, property.GetValue(urlParameters)?.ToString() ?? "");
                }
            }

            var outPut = await restClientRequest.SendGetRequestAsync();
            return LogAndDeserialize<T>(outPut.Content, true);
        }

        private async Task<T> PostAsync<T>(string url, object inputs = null) where T : new()
        {
            var restClient = new RestClient(url);
            return new T();
        }


        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_rateGate != null)
                    _rateGate.Dispose();
            }

            _rateGate = null;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
