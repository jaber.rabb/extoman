﻿using System.Threading.Tasks;

namespace Xtoman.Service.WebServices
{
    public interface IArzliveService
    {
        int? USDPriceInRial();
        int? USDPriceInToman();
        int? EURPriceInRial();
        int? EURPriceInToman();
        Task<int?> USDPriceInRialAsync();
        Task<int?> USDPriceInTomanAsync();
        Task<int?> EURPriceInRialAsync();
        Task<int?> EURPriceInTomanAsync();
    }
}
