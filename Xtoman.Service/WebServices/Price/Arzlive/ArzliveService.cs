﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service.WebServices
{
    public class ArzliveService : IArzliveService
    {
        private readonly ICacheManager _cacheManager;
        public ArzliveService(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }
        #region Sync methods
        public int? EURPriceInRial()
        {
            try
            {
                var price = new int?();
                var content = GetStringData();
                if (!content.HasValue()) return null;
                var value = content.Remove(0, content.IndexOf("3_41") + 7);
                value = value.Remove(value.IndexOf('"'));
                price = value.ToInt();
                return price;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return null;
            }
        }

        public int? USDPriceInRial()
        {
            try
            {
                var price = new int?();
                var content = GetStringData();
                if (!content.HasValue()) return null;
                var value = content.Remove(0, content.IndexOf("3_40") + 7);
                value = value.Remove(value.IndexOf('"'));
                price = value.ToInt();
                return price;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return null;
            }
        }
        public int? USDPriceInToman()
        {
            var key = $"Xtoman.Arzlive.USDPriceInToman";
            return _cacheManager.Get(key, 20, () =>
            {
                var rialPrice = USDPriceInRial();
                if (rialPrice.HasValue)
                    return rialPrice / 10;
                return null;
            });
        }

        public int? EURPriceInToman()
        {
            var key = $"Xtoman.Arzlive.EURPriceInToman";
            return _cacheManager.Get(key, 20, () =>
            {
                var rialPrice = EURPriceInRial();
                if (rialPrice.HasValue)
                    return rialPrice / 10;
                return null;
            });
        }
        #endregion

        #region Async methods
        public async Task<int?> EURPriceInRialAsync()
        {
            try
            {
                var price = new int?();
                var content = await GetStringDataAsync();
                if (!content.HasValue()) return null;
                var value = content.Remove(0, content.IndexOf("3_41") + 7);
                value = value.Remove(value.IndexOf('"'));
                price = value.ToInt();
                return price;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return null;
            }
        }
        public async Task<int?> EURPriceInTomanAsync()
        {
            var key = $"Xtoman.Arzlive.EURPriceInToman";
            return await _cacheManager.GetAsync(key, 20, async () =>
            {
                var rialPrice = await EURPriceInRialAsync();
                if (rialPrice.HasValue)
                    return rialPrice / 10;
                return null;
            });
        }
        public async Task<int?> USDPriceInRialAsync()
        {
            try
            {
                var price = new int?();
                var content = await GetStringDataAsync();
                if (!content.HasValue()) return null;
                var value = content.Remove(0, content.IndexOf("3_40") + 7);
                value = value.Remove(value.IndexOf('"'));
                price = value.ToInt();
                return price;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return null;
            }
        }
        public async Task<int?> USDPriceInTomanAsync()
        {
            var key = $"Xtoman.Arzlive.USDPriceInToman";
            return await _cacheManager.GetAsync(key, 20, async () =>
            {
                var rialPrice = await USDPriceInRialAsync();
                if (rialPrice.HasValue)
                    return rialPrice / 10;
                return null;
            });
        }

        #endregion

        #region Helpers
        private async Task<string> GetStringDataAsync()
        {
            using (WebClient client = new WebClient())
            {
                Stream stream = await client.OpenReadTaskAsync("http://service.arzlive.com/p.js");
                StreamReader reader = new StreamReader(stream);
                var content = reader.ReadLine(); //first line
                return content;
            }
        }
        private string GetStringData()
        {
            using (WebClient client = new WebClient())
            {
                Stream stream = client.OpenRead("http://service.arzlive.com/p.js");
                StreamReader reader = new StreamReader(stream);
                var content = reader.ReadLine(); //first line
                return content;
            }
        }
        #endregion
    }
}
