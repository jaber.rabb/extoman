﻿using RestSharp;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price.CoinCap;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service.WebServices
{
    public class CoinCapService : WebServiceManager, ICoinCapService
    {
        private readonly ICacheManager _cacheManager;
        private const string url = "http://coincap.io";
        public CoinCapService(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        #region Async Methods
        public async Task<CoinCapHistory> GetHistoryAsync(string assetName, CoinCapHistoryTimes time = CoinCapHistoryTimes.SevenDays)
        {
            return await CallAsync<CoinCapHistory>("history", new { a = time.ToDisplay(), b = assetName });
        }
        #endregion

        #region Helpers
        private async Task<T> CallAsync<T>(string action, object segments = null) where T : new()
        {
            var response = await GetRequestAsync(action, segments);
            return LogAndDeserialize<T>(response);
        }

        private async Task<string> GetRequestAsync(string action, object segments = null)
        {
            System.Reflection.PropertyInfo[] validProperties = null;
            var requestUrl = url + "/" + action;
            if (segments != null)
            {
                validProperties = segments.GetType().GetProperties();
                foreach (var property in validProperties)
                {
                    var value = property.GetValue(segments)?.ToString() ?? "";
                    if (value.HasValue())
                        requestUrl += "/{" + property.Name + "}";
                }
            }
            var restClient = new RestClientRequest(requestUrl);
            if (validProperties != null && validProperties.Any())
            {
                foreach (var property in validProperties)
                {
                    var value = property.GetValue(segments)?.ToString() ?? "";
                    if (value.HasValue())
                        restClient.AddUrlSegment(property.Name, property.GetValue(segments)?.ToString() ?? "");
                }
            }

            var searchOutputs = await restClient.SendGetRequestAsync();
            return searchOutputs.Content;
        }
        #endregion
    }
}
