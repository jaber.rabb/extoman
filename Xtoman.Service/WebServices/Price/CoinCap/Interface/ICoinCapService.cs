﻿using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price.CoinCap;

namespace Xtoman.Service.WebServices
{
    public interface ICoinCapService
    {
        #region Async
        Task<CoinCapHistory> GetHistoryAsync(string assetName, CoinCapHistoryTimes time = CoinCapHistoryTimes.SevenDays);
        #endregion
    }
}
