﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Extensions;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price.CryptoCompare;

namespace Xtoman.Service.WebServices
{
    public class CryptoCompare : WebServiceManager, ICryptoCompare
    {
        private const string baseUrl = "https://min-api.cryptocompare.com/data/";

        #region Async methods
        //public async Task<CCAllCoinsResult> AllCoinsAsync()
        //{

        //}
        public async Task<CCPriceResult> PriceAsync(string symbol)
        {
            if (!symbol.HasValue()) return null;
            var response = await GetRequestAsync("price", new { fsym = symbol, tsym = "BTC,USD,IRR" });
            return LogAndDeserialize<CCPriceResult>(response);
        }
        #endregion

        #region Helpers
        private async Task<string> GetRequestAsync(string action, object inputs = null)
        {
            var restClient = new RestClientRequest(baseUrl, action);
            if (inputs != null)
            {
                var validProperties = inputs.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                foreach (var property in validProperties)
                {
                    var value = property.GetValue(inputs)?.ToString() ?? "";
                    if (value.HasValue())
                        restClient.AddQueryParameter(property.Name, property.GetValue(inputs)?.ToString() ?? "");
                }
            }
            var searchOutputs = await restClient.SendGetRequestAsync();
            return searchOutputs.Content;
        }
        #endregion
    }
}
