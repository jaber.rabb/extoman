﻿using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price.CryptoCompare;

namespace Xtoman.Service.WebServices
{
    public interface ICryptoCompare
    {
        Task<CCPriceResult> PriceAsync(string symbol);
    }
}
