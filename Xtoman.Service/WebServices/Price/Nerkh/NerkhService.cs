﻿using RestSharp;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price.Nerkh;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service.WebServices.Price.Nerkh
{
    // nerkh-api.ir
    public class NerkhService : WebServiceManager, INerkhService
    {
        private readonly ICacheManager _cacheManager;
        private const string url = "http://nerkh-api.ir/api/ec29621a45df2fe10d01c90c25e5ff04"; //API Code = ec29621a45df2fe10d01c90c25e5ff04
        private const string eurKey = "Xtoman.Nerkh.EuroPrice";
        private const string usdKey = "Xtoman.Nerkh.DollarPrice";

        public NerkhService(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        #region Async methods
        public async Task<(long USD, long EUR)> USDAndEURPriceAsync()
        {
            (long usd, long eur) result = (0, 0);
            try
            {
                if (!_cacheManager.IsSet(eurKey) && !_cacheManager.IsSet(usdKey))
                {
                    var nerkhCurrency = await GetAllCurrenciesAsync();
                    result.eur = _cacheManager.Get(eurKey, 5, () =>
                    {
                        return nerkhCurrency.Data.Prices.Where(p => p.Key == "EUR").Select(p => p.Value.CurrentToman).DefaultIfEmpty(0).FirstOrDefault();
                    });
                    result.usd = _cacheManager.Get(usdKey, 5, () =>
                    {
                        return nerkhCurrency.Data.Prices.Where(p => p.Key == "USD").Select(p => p.Value.CurrentToman).DefaultIfEmpty(0).FirstOrDefault();
                    });
                }
                else if (!_cacheManager.IsSet(eurKey) && _cacheManager.IsSet(usdKey))
                {
                    result.usd = _cacheManager.Get<long>(usdKey);
                    result.eur = await EuroPriceAsync();
                }
                else if (_cacheManager.IsSet(eurKey) && !_cacheManager.IsSet(usdKey))
                {
                    result.usd = await DollarPriceAsync();
                    result.eur = _cacheManager.Get<long>(eurKey);
                }
                else
                {
                    result.usd = _cacheManager.Get<long>(usdKey);
                    result.eur = _cacheManager.Get<long>(eurKey);
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
            return result;
        }

        public async Task<long> EuroPriceAsync()
        {
            return await _cacheManager.GetAsync(eurKey, 5, async () =>
            {
                var response = await GetRequestAsync("currency", new { filter = "EUR" });
                var nerkhCurrency = LogAndDeserialize<NerkhCurrency>(response);
                return nerkhCurrency.Data.Prices.Where(p => p.Key == "EUR").Select(p => p.Value.CurrentToman).DefaultIfEmpty(0).FirstOrDefault();
            });
        }

        public async Task<long> DollarPriceAsync()
        {
            return await _cacheManager.GetAsync(usdKey, 5, async () =>
            {
                var response = await GetRequestAsync("currency", new { filter = "USD" });
                var nerkhCurrency = LogAndDeserialize<NerkhCurrency>(response);
                return nerkhCurrency.Data.Prices.Where(p => p.Key == "USD").Select(p => p.Value.CurrentToman).DefaultIfEmpty(0).FirstOrDefault();
            });
        }

        public async Task<NerkhCurrency> GetAllCurrenciesAsync()
        {
            var response = await GetRequestAsync("currency");
            return LogAndDeserialize<NerkhCurrency>(response);
        }
        #endregion

        #region Sync methods
        public (long USD, long EUR) USDAndEURPrice()
        {
            (long usd, long eur) result = (0, 0);
            try
            {
                if (!_cacheManager.IsSet(eurKey) && !_cacheManager.IsSet(usdKey))
                {
                    var nerkhCurrency = GetAllCurrencies();
                    result.eur = _cacheManager.Get(eurKey, 5, () =>
                    {
                        return nerkhCurrency.Data.Prices.Where(p => p.Key == "EUR").Select(p => p.Value.CurrentToman).DefaultIfEmpty(0).FirstOrDefault();
                    });
                    result.usd = _cacheManager.Get(usdKey, 5, () =>
                    {
                        return nerkhCurrency.Data.Prices.Where(p => p.Key == "USD").Select(p => p.Value.CurrentToman).DefaultIfEmpty(0).FirstOrDefault();
                    });
                }
                else if (!_cacheManager.IsSet(eurKey) && _cacheManager.IsSet(usdKey))
                {
                    result.usd = _cacheManager.Get<long>(usdKey);
                    result.eur = EuroPrice();
                }
                else if (_cacheManager.IsSet(eurKey) && !_cacheManager.IsSet(usdKey))
                {
                    result.usd = DollarPrice();
                    result.eur = _cacheManager.Get<long>(eurKey);
                }
                else
                {
                    result.usd = _cacheManager.Get<long>(usdKey);
                    result.eur = _cacheManager.Get<long>(eurKey);
                }

            }
            catch (Exception ex)
            {
                ex.LogError();
            }
            return result;
        }


        public long EuroPrice()
        {
            return _cacheManager.Get(eurKey, 5, () =>
            {
                var response = GetRequest("currency", new { filter = "EUR" });
                var nerkhCurrency = LogAndDeserialize<NerkhCurrency>(response);
                return nerkhCurrency.Data.Prices.Where(p => p.Key == "EUR").Select(p => p.Value.CurrentToman).DefaultIfEmpty(0).FirstOrDefault();
            });
        }

        public long DollarPrice()
        {
            return _cacheManager.Get(usdKey, 5, () =>
            {
                var response = GetRequest("currency", new { filter = "USD" });
                var nerkhCurrency = LogAndDeserialize<NerkhCurrency>(response);
                return nerkhCurrency.Data.Prices.Where(p => p.Key == "USD").Select(p => p.Value.CurrentToman).DefaultIfEmpty(0).FirstOrDefault();
            });
        }

        public NerkhCurrency GetAllCurrencies()
        {
            var response = GetRequest("currency");
            return LogAndDeserialize<NerkhCurrency>(response);
        }
        #endregion

        #region Helpers
        private string GetRequest(string action, object inputs = null)
        {
            IEnumerable<PropertyInfo> validProperties = new List<PropertyInfo>();
            var restClient = new RestClient(url);
            var restAction = action;

            var restRequest = new RestRequest(restAction, Method.GET);
            if (validProperties.Any())
                foreach (var property in validProperties)
                {
                    var value = property.GetValue(inputs)?.ToString() ?? "";
                    if (value.HasValue())
                        restRequest.AddQueryParameter(property.Name, property.GetValue(inputs)?.ToString() ?? "");
                }

            var searchOutputs = restClient.Execute(restRequest);
            return searchOutputs.Content;
        }

        private async Task<string> GetRequestAsync(string action, object inputs = null)
        {
            IEnumerable<PropertyInfo> validProperties = new List<PropertyInfo>();
            var restClientRequest = new RestClientRequest(url, action)
            {
                Method = Method.GET,
                RequestFormat = DataFormat.Json
            };

            //var restClient = new RestClient(url);
            //var restAction = action;

            //var restRequest = new RestRequest(restAction, Method.GET);
            if (validProperties.Any())
                foreach (var property in validProperties)
                {
                    var value = property.GetValue(inputs)?.ToString() ?? "";
                    if (value.HasValue())
                        restClientRequest.AddQueryParameter(property.Name, property.GetValue(inputs)?.ToString() ?? "");
                }

            var searchOutputs = await restClientRequest.SendGetRequestAsync().ConfigureAwait(false);
            return searchOutputs.Content;
        }
        #endregion
    }
}
