﻿using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price.Nerkh;

namespace Xtoman.Service.WebServices.Price.Nerkh
{
    public interface INerkhService
    {
        Task<long> EuroPriceAsync();
        Task<long> DollarPriceAsync();
        Task<(long USD, long EUR)> USDAndEURPriceAsync();
        Task<NerkhCurrency> GetAllCurrenciesAsync();

        long EuroPrice();
        long DollarPrice();
        (long USD, long EUR) USDAndEURPrice();
        NerkhCurrency GetAllCurrencies();
    }
}
