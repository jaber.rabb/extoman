﻿using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price.TGJU;

namespace Xtoman.Service.WebServices
{
    public interface ITGJUService
    {
        #region USD
        int? USDPriceInRial();
        int? USDPriceInToman();
        Task<int?> USDPriceInRialAsync();
        Task<int?> USDPriceInTomanAsync();
        #endregion

        #region EUR
        int? EURPriceInRial();
        int? EURPriceInToman();
        Task<int?> EURPriceInRialAsync();
        Task<int?> EURPriceInTomanAsync();
        #endregion

        #region All
        SanaPrices GetAll();
        Task<SanaPrices> GetAllAsync();
        #endregion
    }
}
