﻿using RestSharp;
using RestSharp.Extensions;
using System.Globalization;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price.TGJU;
using Xtoman.Utility.Caching;

namespace Xtoman.Service.WebServices
{
    public class TGJUService : WebServiceManager, ITGJUService
    {
        private readonly ICacheManager _cacheManager;
        private const string url = "http://www.tgju.org/?act=sanarateservice&client=tgju&noview&type=json";
        public TGJUService(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        #region USD
        public int? USDPriceInRial()
        {
            var allPrices = GetAll();
            if (allPrices != null && allPrices.SanaBuyUsd != null && allPrices.SanaBuyUsd.Price.HasValue() &&
                int.TryParse(allPrices.SanaBuyUsd.Price, NumberStyles.AllowThousands, CultureInfo.InvariantCulture, out int usdPrice))
            {
                return usdPrice;
            }
            return null;
        }

        public int? USDPriceInToman()
        {
            var key = $"Xtoman.TGJU.USDPriceInToman";
            return _cacheManager.Get(key, 20, () =>
            {
                var rialPrice = USDPriceInRial();
                if (rialPrice.HasValue)
                    return rialPrice / 10;
                return null;
            });
        }

        public async Task<int?> USDPriceInRialAsync()
        {
            var allPrices = await GetAllAsync();
            if (allPrices != null && allPrices.SanaBuyUsd != null && allPrices.SanaBuyUsd.Price.HasValue() &&
                int.TryParse(allPrices.SanaBuyUsd.Price, NumberStyles.AllowThousands, CultureInfo.InvariantCulture, out int usdPrice))
            {
                return usdPrice;
            }
            return null;
        }

        public async Task<int?> USDPriceInTomanAsync()
        {
            var key = $"Xtoman.TGJU.USDPriceInToman";
            return await _cacheManager.GetAsync(key, 20, async () =>
            {
                var rialPrice = await USDPriceInRialAsync();
                if (rialPrice.HasValue)
                    return rialPrice / 10;
                return null;
            });
        }
        #endregion

        #region EUR
        public int? EURPriceInRial()
        {
            var allPrices = GetAll();
            if (allPrices != null && allPrices.SanaBuyEur != null && allPrices.SanaBuyEur.Price.HasValue() &&
                int.TryParse(allPrices.SanaBuyEur.Price, NumberStyles.AllowThousands, CultureInfo.InvariantCulture, out int eurPrice))
            {
                return eurPrice;
            }
            return null;
        }

        public int? EURPriceInToman()
        {
            var key = $"Xtoman.TGJU.EURPriceInToman";
            return _cacheManager.Get(key, 20, () =>
            {
                var rialPrice = EURPriceInRial();
                if (rialPrice.HasValue)
                    return rialPrice / 10;
                return null;
            });
        }

        public async Task<int?> EURPriceInRialAsync()
        {
            var allPrices = await GetAllAsync();
            if (allPrices != null && allPrices.SanaBuyEur != null && allPrices.SanaBuyEur.Price.HasValue() &&
                int.TryParse(allPrices.SanaBuyEur.Price, NumberStyles.AllowThousands, CultureInfo.InvariantCulture, out int eurPrice))
            {
                return eurPrice;
            }
            return null;
        }

        public async Task<int?> EURPriceInTomanAsync()
        {
            var key = $"Xtoman.TGJU.EURPriceInToman";
            return await _cacheManager.GetAsync(key, 20, async () =>
            {
                var rialPrice = await EURPriceInRialAsync();
                if (rialPrice.HasValue)
                    return rialPrice / 10;
                return null;
            });
        }
        #endregion

        #region All
        public SanaPrices GetAll()
        {
            var restClient = new RestClientRequest(url) { Timeout = 5000 };
            var searchOutputs = restClient.SendGetRequest();
            return LogAndDeserialize<SanaPrices>(searchOutputs.Content);
        }

        public async Task<SanaPrices> GetAllAsync()
        {
            var restClient = new RestClientRequest(url) { Timeout = 5000 };
            var searchOutputs = await restClient.SendGetRequestAsync().ConfigureAwait(false);
            var sanaPrices = LogAndDeserialize<SanaPrices>(searchOutputs.Content);
            return sanaPrices;
        }
        #endregion
    }
}
