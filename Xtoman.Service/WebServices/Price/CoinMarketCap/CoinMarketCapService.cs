﻿using JackLeitch.RateGate;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price.CoinMarketCap;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class CoinMarketCapService : WebServiceManager, ICoinMarketCapService
    {
        private const string url = "https://api.coinmarketcap.com/v1";

        #region Properties
        private RateGate _rateGate;
        #endregion

        #region Constructor
        public CoinMarketCapService()
        {
            _rateGate = new RateGate(1, TimeSpan.FromSeconds(6));
        }
        #endregion

        #region Sync methods
        public CoinMarketCapGlobal GetGlobal()
        {
            var response = GetRequest("global");
            return LogAndDeserialize<CoinMarketCapGlobal>(response);
        }

        public CoinMarketCapTicker GetTicker(string id)
        {
            var response = GetRequest("ticker", new { id, convert = "EUR" });
            return LogAndDeserialize<List<CoinMarketCapTicker>>(response).FirstOrDefault();
        }

        public List<CoinMarketCapTicker> GetTicker(int? start = null, int? limit = null)
        {
            var response = GetRequest("ticker", new { start, limit, convert = "EUR" });
            return LogAndDeserialize<List<CoinMarketCapTicker>>(response);
        }

        public decimal ExchangeRate(CoinMarketCapTicker fromTicker, CoinMarketCapTicker toTicker)
        {
            var btcRate = fromTicker.Price_btc / toTicker.Price_btc;
            var usdRate = fromTicker.Price_usd / toTicker.Price_usd;
            return btcRate.HasValue && btcRate <= usdRate ? btcRate.Value : usdRate.Value;
        }

        public decimal ExchangeRate(List<CoinMarketCapTicker> coinMarketCapTickers, string fromCoinMarketId, string toCoinMarketId)
        {
            var fromTypeTicker = coinMarketCapTickers.Where(p => p.Id == fromCoinMarketId).FirstOrDefault();
            var toTypeTicker = coinMarketCapTickers.Where(p => p.Id == toCoinMarketId).FirstOrDefault();
            return ExchangeRate(fromTypeTicker, toTypeTicker);
        }

        public decimal ExchangeRate(string fromCoinMarketId, string toCoinMarketId)
        {
            var fromTypeTicker = GetTicker(fromCoinMarketId);
            var toTypeTicker = GetTicker(toCoinMarketId);
            return ExchangeRate(fromTypeTicker, toTypeTicker);
        }
        #endregion

        #region Async methods
        public async Task<CoinMarketCapGlobal> GetGlobalAsync()
        {
            var response = await GetRequestAsync("global");
            return LogAndDeserialize<CoinMarketCapGlobal>(response);
        }

        public async Task<CoinMarketCapTicker> GetTickerAsync(string id)
        {
            var response = await GetRequestAsync("ticker", new { id });
            return LogAndDeserialize<List<CoinMarketCapTicker>>(response).FirstOrDefault();
        }

        public async Task<List<CoinMarketCapTicker>> GetTickerAsync(int? start = null, int? limit = null)
        {
            var response = await GetRequestAsync("ticker", new { start, limit });
            return LogAndDeserialize<List<CoinMarketCapTicker>>(response);
        }

        public async Task<List<CoinMarketCapExchangeRate>> ExchangeRateAsync()
        {
            var result = new List<CoinMarketCapExchangeRate>();
            var tickers = await GetTickerAsync(0, 500);
            return result;
        }

        public async Task<decimal> ExchangeRateAsync(string fromCoinMarketId, string toCoinMarketId)
        {
            var fromTypeTicker = await GetTickerAsync(fromCoinMarketId);
            var toTypeTicker = await GetTickerAsync(toCoinMarketId);
            var btcRate = fromTypeTicker.Price_btc / toTypeTicker.Price_btc;
            var usdRate = fromTypeTicker.Price_usd / toTypeTicker.Price_usd;
            return btcRate <= usdRate ? btcRate.Value : usdRate.Value;
        }
        #endregion

        #region Dispose & Destructor
        ~CoinMarketCapService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_rateGate != null)
                    _rateGate.Dispose();
            }

            _rateGate = null;
        }
        #endregion

        #region Helpers
        private string GetRequest(string action, object inputs = null)
        {
            _rateGate.WaitToProceed();
            IEnumerable<PropertyInfo> validProperties = new List<PropertyInfo>();
            var restClient = new RestClient(url);
            var restAction = action;

            if (inputs != null)
            {
                validProperties = inputs.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                foreach (var property in validProperties)
                {
                    var value = property.GetValue(inputs)?.ToString() ?? "";
                    if (property.Name == "id")
                        restAction += "/{id}";
                }
            }

            var restRequest = new RestRequest(restAction, Method.GET);
            if (validProperties.Any())
                foreach (var property in validProperties)
                {
                    var value = property.GetValue(inputs)?.ToString() ?? "";
                    if (property.Name == "id")
                        restRequest.AddUrlSegment("id", value);
                    else if (value.HasValue())
                        restRequest.AddParameter(property.Name, property.GetValue(inputs)?.ToString() ?? "");
                }

            var searchOutputs = restClient.Execute(restRequest);
            return searchOutputs.Content;
        }

        private async Task<string> GetRequestAsync(string action, object inputs = null)
        {
            _rateGate.WaitToProceed();
            var restClient = new RestClient(url);
            var restRequest = new RestRequest(action + "/{id}", Method.GET);
            if (inputs != null)
            {
                var validProperties = inputs.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                if (!validProperties.Any(x => x.Name.Equals("id", StringComparison.InvariantCultureIgnoreCase)))
                {
                    restRequest = new RestRequest(action, Method.GET);
                }
                foreach (var property in validProperties)
                {
                    var value = property.GetValue(inputs)?.ToString() ?? "";
                    if (property.Name == "id")
                        restRequest.AddUrlSegment("id", value);
                    else if (value.HasValue())
                        restRequest.AddParameter(property.Name, property.GetValue(inputs)?.ToString() ?? "");
                }
            }
            var searchOutputs = await restClient.ExecuteTaskAsync(restRequest).ConfigureAwait(false);
            return searchOutputs.Content;
        }

        //private async Task<string> GetRequestAsync(HttpClient client, string action, object inputs = null)
        //{
        //    var requestUrl = AppUrl.Create(url, action, "{id}", inputs);
        //    var response = await client.GetAsync(requestUrl);
        //    if (response.IsSuccessStatusCode)
        //    {
        //        return response.
        //    }
        //}

        public decimal Past24HOverallChangePercent()
        {
            var tickers = GetTicker(0, 9999);
            tickers = tickers.Where(p => p.Percent_change_24h.HasValue && p.Percent_change_24h > 0).ToList();
            return tickers.Select(p => p.Percent_change_24h.Value).DefaultIfEmpty(0).Sum() / tickers.Count;
        }

        public decimal Past7DaysOverallChangePercent()
        {
            var tickers = GetTicker(0, 9999);
            tickers = tickers.Where(p => p.Percent_change_7d.HasValue && p.Percent_change_7d > 0).ToList();
            return tickers.Select(p => p.Percent_change_7d.Value).DefaultIfEmpty(0).Sum() / tickers.Count;
        }
        #endregion

    }

    public class CoinMarketCapExchangeRate
    {
        public string FromCoinMarketId { get; set; }
        public string ToCoinMarketId { get; set; }
        public decimal Rate { get; set; }
    }
}
