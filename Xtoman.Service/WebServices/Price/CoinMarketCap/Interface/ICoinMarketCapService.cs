﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price.CoinMarketCap;

namespace Xtoman.Service.WebServices
{
    public interface ICoinMarketCapService
    {
        #region Sync methods
        /// <summary>
        /// Ticker (Specific Currency)
        /// </summary>
        /// <param name="id">string id name</param>
        /// <returns></returns>
        CoinMarketCapTicker GetTicker(string id);
        /// <summary>
        /// Ticker list of currencies
        /// </summary>
        /// <param name="start">return results from rank [start] and above</param>
        /// <param name="limit">return a maximum of [limit] results (default is 100, use 0 to return all results)</param>
        /// <returns></returns>
        List<CoinMarketCapTicker> GetTicker(int? start = null, int? limit = null);
        /// <summary>
        /// Global Data
        /// </summary>
        /// <returns></returns>
        CoinMarketCapGlobal GetGlobal();
        decimal ExchangeRate(CoinMarketCapTicker fromTicker, CoinMarketCapTicker toTicker);
        decimal ExchangeRate(List<CoinMarketCapTicker> coinMarketCapTickers, string fromCoinMarketId, string toCoinMarketId);
        decimal ExchangeRate(string fromCoinMarketId, string toCoinMarketId);
        decimal Past24HOverallChangePercent();
        decimal Past7DaysOverallChangePercent();
        #endregion

        #region Async methods
        /// <summary>
        /// Ticker (Specific Currency)
        /// </summary>
        /// <param name="id">string id name</param>
        /// <returns></returns>
        Task<CoinMarketCapTicker> GetTickerAsync(string id);
        /// <summary>
        /// Ticker list of currencies
        /// </summary>
        /// <param name="start">return results from rank [start] and above</param>
        /// <param name="limit">return a maximum of [limit] results (default is 100, use 0 to return all results)</param>
        Task<List<CoinMarketCapTicker>> GetTickerAsync(int? start = null, int? limit = null);
        /// <summary>
        /// Global Data
        /// </summary>
        /// <returns></returns>
        Task<CoinMarketCapGlobal> GetGlobalAsync();
        Task<decimal> ExchangeRateAsync(string fromCoinMarketId, string toCoinMarketId);
        #endregion
    }
}
