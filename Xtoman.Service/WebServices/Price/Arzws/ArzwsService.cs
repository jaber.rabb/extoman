﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Extensions;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price;
using Xtoman.Utility.Caching;

namespace Xtoman.Service.WebServices
{
    public class ArzwsService : WebServiceManager, IArzwsService
    {
        private readonly ICacheManager _cacheManager;
        private const string token = "b0673a87-88ab-4ab9-134d-08d608e90c85";
        private const string url = "http://core.arzws.com";
        private const string eurKey = "Xtoman.Arzws.EuroPrice";
        private const string usdKey = "Xtoman.Arzws.DollarPrice";

        public ArzwsService(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        public async Task<(long USD, long EUR)> USDAndEURPriceAsync()
        {
            (long usd, long eur) result = (0, 0);
            if (!_cacheManager.IsSet(eurKey) || !_cacheManager.IsSet(usdKey))
            {
                var arzwsArz = await GetAllCurrenciesAsync();
                result.eur = _cacheManager.Get(eurKey, 5, () =>
                {
                    return arzwsArz.BazarExchange
                        .Where(p => p.Icon.Equals("bazar_eur", StringComparison.InvariantCultureIgnoreCase))
                        .Select(p => p.CurrentToman).DefaultIfEmpty(0).FirstOrDefault();
                });
                result.usd = _cacheManager.Get(usdKey, 5, () =>
                {
                    return arzwsArz.BazarExchange
                        .Where(p => p.Icon.Equals("bazar_usd", StringComparison.InvariantCultureIgnoreCase))
                        .Select(p => p.CurrentToman).DefaultIfEmpty(0).FirstOrDefault();
                });
            }
            else
            {
                result.usd = _cacheManager.Get<long>(usdKey);
                result.eur = _cacheManager.Get<long>(eurKey);
            }
            return result;
        }

        public async Task<ArzwsArz> GetAllCurrenciesAsync()
        {
            var response = await GetRequestAsync("api/core", new { what = "bazarex" });
            return LogAndDeserialize<ArzwsArz>(response);
        }

        private async Task<string> GetRequestAsync(string action, object inputs = null)
        {
            var restClient = new RestClient(url);
            var restAction = action;

            var restRequest = new RestRequest(restAction, Method.GET);
            restRequest.AddQueryParameter("Token", token);

            var validProperties = inputs.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
            if (validProperties.Any())
                foreach (var property in validProperties)
                {
                    var value = property.GetValue(inputs)?.ToString() ?? "";
                    if (value.HasValue())
                        restRequest.AddQueryParameter(property.Name, property.GetValue(inputs)?.ToString() ?? "");
                }

            var searchOutputs = await restClient.ExecuteTaskAsync(restRequest).ConfigureAwait(false);
            return searchOutputs.Content;
        }

        public ArzwsArz GetAllCurrencies()
        {
            var response = GetRequest("api/core", new { what = "bazarex" });
            return LogAndDeserialize<ArzwsArz>(response);
        }

        private string GetRequest(string action, object inputs = null)
        {
            var restClient = new RestClient(url);
            var restAction = action;

            var restRequest = new RestRequest(restAction, Method.GET);
            restRequest.AddQueryParameter("Token", token);

            var validProperties = inputs.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
            if (validProperties.Any())
                foreach (var property in validProperties)
                {
                    var value = property.GetValue(inputs)?.ToString() ?? "";
                    if (value.HasValue())
                        restRequest.AddQueryParameter(property.Name, property.GetValue(inputs)?.ToString() ?? "");
                }

            var searchOutputs = restClient.Execute(restRequest);
            return searchOutputs.Content;
        }

        public (long USD, long EUR) USDAndEURPrice()
        {
            (long usd, long eur) result = (0, 0);
            if (!_cacheManager.IsSet(eurKey) || !_cacheManager.IsSet(usdKey))
            {
                var arzwsArz = GetAllCurrencies();
                result.eur = _cacheManager.Get(eurKey, 5, () =>
                {
                    return arzwsArz.BazarExchange
                        .Where(p => p.Icon.Equals("bazar_eur", System.StringComparison.InvariantCultureIgnoreCase))
                        .Select(p => p.CurrentToman).DefaultIfEmpty(0).FirstOrDefault();
                });
                result.usd = _cacheManager.Get(usdKey, 5, () =>
                {
                    return arzwsArz.BazarExchange
                        .Where(p => p.Icon.Equals("bazar_usd", System.StringComparison.InvariantCultureIgnoreCase))
                        .Select(p => p.CurrentToman).DefaultIfEmpty(0).FirstOrDefault();
                });
            }
            else
            {
                result.usd = _cacheManager.Get<long>(usdKey);
                result.eur = _cacheManager.Get<long>(eurKey);
            }
            return result;
        }
    }
}
