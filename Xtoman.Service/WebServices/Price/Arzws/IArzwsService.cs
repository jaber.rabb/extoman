﻿using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price;

namespace Xtoman.Service.WebServices
{
    public interface IArzwsService
    {
        Task<(long USD, long EUR)> USDAndEURPriceAsync();
        Task<ArzwsArz> GetAllCurrenciesAsync();

        (long USD, long EUR) USDAndEURPrice();
        ArzwsArz GetAllCurrencies();
    }
}
