﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Price.Digiarz;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service.WebServices
{
    public class DigiarzService : WebServiceManager, IDigiarzService
    {
        private readonly ICacheManager _cacheManager;
        private const string url = "https://digiarz.com/webservice";
        private readonly string[] digiArzCoins = new string[] { "BTC", "ETH", "ETC", "DASH", "XMR", "XRP", "ZEC", "FCT", "LTC", "DOGE", "DASH" };

        public DigiarzService(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }
        #region Sync methods
        public DigiarzPriceResult AllPrices()
        {
            var response = GetRequest();
            return LogAndDeserialize<DigiarzPriceResult>(response);
        }
        public DigiarzBitcoinPriceResult BitcoinPrice()
        {
            var response = GetRequest(new { c = DigiarzCoin.Bitcoin.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzBitcoinPriceResult>(response);
        }
        public DigiarzEthereumPriceResult EthereumPrice()
        {
            var response = GetRequest(new { c = DigiarzCoin.Ethereum.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzEthereumPriceResult>(response);
        }
        public DigiarzEthereumClassicPriceResult EthereumClassicPrice()
        {
            var response = GetRequest(new { c = DigiarzCoin.EthereumClassic.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzEthereumClassicPriceResult>(response);
        }
        public DigiarzDashPriceResult DashPrice()
        {
            var response = GetRequest(new { c = DigiarzCoin.Dash.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzDashPriceResult>(response);
        }
        public DigiarzDogecoinPriceResult DogecoinPrice()
        {
            var response = GetRequest(new { c = DigiarzCoin.Dogecoin.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzDogecoinPriceResult>(response);
        }
        public DigiarzMoneroPriceResult MoneroPrice()
        {
            var response = GetRequest(new { c = DigiarzCoin.Monero.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzMoneroPriceResult>(response);
        }
        public DigiarzRipplePriceResult RipplePrice()
        {
            var response = GetRequest(new { c = DigiarzCoin.Ripple.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzRipplePriceResult>(response);
        }
        public DigiarzZCashPriceResult ZCashPrice()
        {
            var response = GetRequest(new { c = DigiarzCoin.ZCash.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzZCashPriceResult>(response);
        }
        public DigiarzLitecoinPriceResult LitecoinPrice()
        {
            var response = GetRequest(new { c = DigiarzCoin.Litecoin.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzLitecoinPriceResult>(response);
        }
        public DigiarzFactomPriceResult FactomPrice()
        {
            var response = GetRequest(new { c = DigiarzCoin.Factom.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzFactomPriceResult>(response);
        }
        public DigiarzTypeResult Get(DigiarzCoin type)
        {
            switch (type)
            {
                case DigiarzCoin.Bitcoin:
                    return (BitcoinPrice())?.BTC;
                case DigiarzCoin.Ethereum:
                    return (EthereumPrice())?.ETH;
                case DigiarzCoin.EthereumClassic:
                    return (EthereumClassicPrice())?.ETC;
                case DigiarzCoin.Monero:
                    return (MoneroPrice())?.XMR;
                case DigiarzCoin.Ripple:
                    return (RipplePrice())?.XRP;
                case DigiarzCoin.ZCash:
                    return (ZCashPrice())?.ZEC;
                case DigiarzCoin.Factom:
                    return (FactomPrice())?.FCT;
                case DigiarzCoin.Litecoin:
                    return (LitecoinPrice())?.LTC;
                case DigiarzCoin.Dogecoin:
                    return (DogecoinPrice())?.DOGE;
                case DigiarzCoin.Dash:
                    return (DashPrice())?.DASH;
            }
            return null;
        }
        public DigiarzTypeResult Get(ECurrencyType type)
        {
            var digiArzCoin = ECAccountTypeToDigiarzCoin(type);
            if (!digiArzCoin.HasValue) return null;
            return Get(digiArzCoin.Value);
        }
        public decimal ExchangeRate(ECurrencyType fromType, ECurrencyType toType)
        {
            var fromDigiarzCoin = ECAccountTypeToDigiarzCoin(fromType);
            var toDigiarzCoin = ECAccountTypeToDigiarzCoin(toType);
            if (!fromDigiarzCoin.HasValue || !toDigiarzCoin.HasValue)
                return 0;

            var fromRate = Get(fromDigiarzCoin.Value);
            var toRate = Get(toDigiarzCoin.Value);

            var fromRateUSD = fromRate?.rates?.USD?.rate ?? 0;
            var toRateUSD = toRate?.rates?.USD?.rate ?? 0;
            var rateOfUSD = toRateUSD > 0 ? fromRateUSD / toRateUSD : 0;

            var fromRateTMN = fromRate?.rates?.TMN?.rate ?? 0;
            var toRateTMN = toRate?.rates?.TMN?.rate ?? 0;
            var rateOfTMN = toRateTMN > 0 ? fromRateTMN / toRateTMN : 0;

            return rateOfTMN <= rateOfUSD ? rateOfTMN : rateOfUSD;
        }
        public decimal USDPriceInToman()
        {
            var key = $"Xtoman.Digiarz.USDPriceInToman";
            return _cacheManager.Get(key, 20, () =>
            {
                var digiArzBTCPriceRates = BitcoinPrice()?.BTC.rates;
                return digiArzBTCPriceRates?.TMN?.rate / digiArzBTCPriceRates?.USD?.rate ?? 0;
            });
        }
        #endregion

        #region Async methods
        public async Task<DigiarzPriceResult> AllPricesAsync()
        {
            var response = await GetRequestAsync();
            return LogAndDeserialize<DigiarzPriceResult>(response);
        }
        public async Task<DigiarzBitcoinPriceResult> BitcoinPriceAsync()
        {
            var response = await GetRequestAsync(new { c = DigiarzCoin.Bitcoin.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzBitcoinPriceResult>(response);
        }
        public async Task<DigiarzEthereumPriceResult> EthereumPriceAsync()
        {
            var response = await GetRequestAsync(new { c = DigiarzCoin.Ethereum.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzEthereumPriceResult>(response);
        }
        public async Task<DigiarzEthereumClassicPriceResult> EthereumClassicPriceAsync()
        {
            var response = await GetRequestAsync(new { c = DigiarzCoin.EthereumClassic.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzEthereumClassicPriceResult>(response);
        }
        public async Task<DigiarzDashPriceResult> DashPriceAsync()
        {
            var response = await GetRequestAsync(new { c = DigiarzCoin.Dash.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzDashPriceResult>(response);
        }
        public async Task<DigiarzDogecoinPriceResult> DogecoinPriceAsync()
        {
            var response = await GetRequestAsync(new { c = DigiarzCoin.Dogecoin.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzDogecoinPriceResult>(response);
        }
        public async Task<DigiarzMoneroPriceResult> MoneroPriceAsync()
        {
            var response = await GetRequestAsync(new { c = DigiarzCoin.Monero.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzMoneroPriceResult>(response);
        }
        public async Task<DigiarzRipplePriceResult> RipplePriceAsync()
        {
            var response = await GetRequestAsync(new { c = DigiarzCoin.Ripple.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzRipplePriceResult>(response);
        }
        public async Task<DigiarzZCashPriceResult> ZCashPriceAsync()
        {
            var response = await GetRequestAsync(new { c = DigiarzCoin.ZCash.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzZCashPriceResult>(response);
        }
        public async Task<DigiarzLitecoinPriceResult> LitecoinPriceAsync()
        {
            var response = await GetRequestAsync(new { c = DigiarzCoin.Litecoin.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzLitecoinPriceResult>(response);
        }
        public async Task<DigiarzFactomPriceResult> FactomPriceAsync()
        {
            var response = await GetRequestAsync(new { c = DigiarzCoin.Factom.ToDisplay(DisplayProperty.ShortName) });
            return LogAndDeserialize<DigiarzFactomPriceResult>(response);
        }
        public async Task<DigiarzTypeResult> GetAsync(DigiarzCoin type)
        {
            switch (type)
            {
                case DigiarzCoin.Bitcoin:
                    return (await BitcoinPriceAsync())?.BTC;
                case DigiarzCoin.Ethereum:
                    return (await EthereumPriceAsync())?.ETH;
                case DigiarzCoin.EthereumClassic:
                    return (await EthereumClassicPriceAsync())?.ETC;
                case DigiarzCoin.Monero:
                    return (await MoneroPriceAsync())?.XMR;
                case DigiarzCoin.Ripple:
                    return (await RipplePriceAsync())?.XRP;
                case DigiarzCoin.ZCash:
                    return (await ZCashPriceAsync())?.ZEC;
                case DigiarzCoin.Factom:
                    return (await FactomPriceAsync())?.FCT;
                case DigiarzCoin.Litecoin:
                    return (await LitecoinPriceAsync())?.LTC;
                case DigiarzCoin.Dogecoin:
                    return (await DogecoinPriceAsync()).DOGE;
                case DigiarzCoin.Dash:
                    return (await DashPriceAsync())?.DASH;
            }
            return null;
        }
        public async Task<DigiarzTypeResult> GetAsync(ECurrencyType type)
        {
            var digiArzCoin = ECAccountTypeToDigiarzCoin(type);
            if (!digiArzCoin.HasValue) return null;
            return await GetAsync(digiArzCoin.Value);
        }
        public async Task<decimal> ExchangeRateAsync(ECurrencyType fromType, ECurrencyType toType)
        {
            var fromDigiarzCoin = ECAccountTypeToDigiarzCoin(fromType);
            var toDigiarzCoin = ECAccountTypeToDigiarzCoin(toType);
            if (!fromDigiarzCoin.HasValue || !toDigiarzCoin.HasValue)
                return 0;

            var fromRate = await GetAsync(fromDigiarzCoin.Value);
            var toRate = await GetAsync(toDigiarzCoin.Value);

            return PrepareRate(fromRate, toRate);
        }
        #endregion

        #region Helper
        private decimal PrepareRate(DigiarzTypeResult fromRate, DigiarzTypeResult toRate)
        {
            var fromRateUSD = fromRate?.rates?.USD?.rate ?? 0;
            var toRateUSD = toRate?.rates?.USD?.rate ?? 0;
            var rateOfUSD = fromRateUSD / toRateUSD;

            var fromRateTMN = fromRate?.rates?.TMN?.rate ?? 0;
            var toRateTMN = toRate?.rates?.TMN?.rate ?? 0;
            var rateOfTMN = fromRateTMN / toRateTMN;

            return rateOfTMN <= rateOfUSD ? rateOfTMN : rateOfUSD;
        }
        private string GetRequest(object input = null)
        {
            var restClient = new RestClientRequest(url, "api")
            {
                Timeout = 5000
            };
            if (input != null)
            {
                var validProperties = input.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                foreach (var property in validProperties)
                    restClient.AddQueryParameter(property.Name, property.GetValue(input)?.ToString() ?? "");
            }
            var searchOutputs = restClient.SendGetRequest();
            return searchOutputs.Content;
        }
        private async Task<string> GetRequestAsync(object input = null)
        {
            var restClient = new RestClientRequest(url, "api");
            if (input != null)
            {
                var validProperties = input.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                foreach (var property in validProperties)
                {
                    restClient.AddQueryParameter(property.Name, property.GetValue(input)?.ToString() ?? "");
                }
            }
            var searchOutputs = await restClient.SendGetRequestAsync().ConfigureAwait(false);
            return searchOutputs.Content;
        }
        private ECurrencyType? DigiarzCoinToECAccountType(DigiarzCoin digiarzcoin)
        {
            switch (digiarzcoin)
            {
                case DigiarzCoin.Bitcoin:
                    return ECurrencyType.Bitcoin;
                case DigiarzCoin.Ethereum:
                    return ECurrencyType.Ethereum;
                case DigiarzCoin.EthereumClassic:
                    return ECurrencyType.EthereumClassic;
                case DigiarzCoin.Monero:
                    return ECurrencyType.Monero;
                case DigiarzCoin.Ripple:
                    return ECurrencyType.Ripple;
                case DigiarzCoin.ZCash:
                    return ECurrencyType.Zcash;
                case DigiarzCoin.Factom:
                    return ECurrencyType.Factom;
                case DigiarzCoin.Litecoin:
                    return ECurrencyType.Litecoin;
                case DigiarzCoin.Dogecoin:
                    return ECurrencyType.Dogecoin;
                case DigiarzCoin.Dash:
                    return ECurrencyType.Dash;
            }
            return null;
        }
        private DigiarzCoin? ECAccountTypeToDigiarzCoin(ECurrencyType eCurrencyAccountType)
        {
            switch (eCurrencyAccountType)
            {
                case ECurrencyType.Bitcoin:
                    return DigiarzCoin.Bitcoin;
                case ECurrencyType.Ethereum:
                    return DigiarzCoin.Ethereum;
                case ECurrencyType.EthereumClassic:
                    return DigiarzCoin.EthereumClassic;
                case ECurrencyType.Monero:
                    return DigiarzCoin.Monero;
                case ECurrencyType.Ripple:
                    return DigiarzCoin.Ripple;
                case ECurrencyType.Zcash:
                    return DigiarzCoin.ZCash;
                case ECurrencyType.Factom:
                    return DigiarzCoin.Factom;
                case ECurrencyType.Litecoin:
                    return DigiarzCoin.Litecoin;
                case ECurrencyType.Dogecoin:
                    return DigiarzCoin.Dogecoin;
                case ECurrencyType.Dash:
                    return DigiarzCoin.Dash;
            }
            return null;
        }
        public async Task<decimal> USDPriceInTomanAsync()
        {
            var key = $"Xtoman.Digiarz.USDPriceInToman";
            return await _cacheManager.GetAsync(key, 20, async () =>
            {
                var digiArzBTCPriceRates = (await BitcoinPriceAsync())?.BTC.rates;
                return digiArzBTCPriceRates?.TMN?.rate / digiArzBTCPriceRates?.USD?.rate ?? 0;
            });

        }
        #endregion
    }

    public enum DigiarzCoin
    {
        [Display(Name = "Bitcoin", ShortName = "BTC")]
        Bitcoin,
        [Display(Name = "Ethereum", ShortName = "ETH")]
        Ethereum,
        [Display(Name = "Ethereum classic", ShortName = "ETC")]
        EthereumClassic,
        [Display(Name = "Monero", ShortName = "XMR")]
        Monero,
        [Display(Name = "Ripple", ShortName = "XRP")]
        Ripple,
        [Display(Name = "ZCash", ShortName = "ZEC")]
        ZCash,
        [Display(Name = "Factom", ShortName = "FCT")]
        Factom,
        [Display(Name = "Litecoin", ShortName = "LTC")]
        Litecoin,
        [Display(Name = "Dogecoin", ShortName = "DOGE")]
        Dogecoin,
        [Display(Name = "Digital cash", ShortName = "DASH")]
        Dash
    }
}
