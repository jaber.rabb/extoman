﻿using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Price.Digiarz;

namespace Xtoman.Service.WebServices
{
    public interface IDigiarzService
    {
        #region Sync methods
        DigiarzPriceResult AllPrices();
        DigiarzBitcoinPriceResult BitcoinPrice();
        DigiarzEthereumPriceResult EthereumPrice();
        DigiarzEthereumClassicPriceResult EthereumClassicPrice();
        DigiarzDashPriceResult DashPrice();
        DigiarzDogecoinPriceResult DogecoinPrice();
        DigiarzMoneroPriceResult MoneroPrice();
        DigiarzRipplePriceResult RipplePrice();
        DigiarzZCashPriceResult ZCashPrice();
        DigiarzLitecoinPriceResult LitecoinPrice();
        DigiarzFactomPriceResult FactomPrice();
        DigiarzTypeResult Get(DigiarzCoin type);
        DigiarzTypeResult Get(ECurrencyType type);
        //decimal GetUSD(ECurrencyAccountType type);
        decimal ExchangeRate(ECurrencyType fromType, ECurrencyType toType);
        decimal USDPriceInToman();
        #endregion

        #region Async methods
        Task<DigiarzPriceResult> AllPricesAsync();
        Task<DigiarzBitcoinPriceResult> BitcoinPriceAsync();
        Task<DigiarzEthereumPriceResult> EthereumPriceAsync();
        Task<DigiarzEthereumClassicPriceResult> EthereumClassicPriceAsync();
        Task<DigiarzDashPriceResult> DashPriceAsync();
        Task<DigiarzDogecoinPriceResult> DogecoinPriceAsync();
        Task<DigiarzMoneroPriceResult> MoneroPriceAsync();
        Task<DigiarzRipplePriceResult> RipplePriceAsync();
        Task<DigiarzZCashPriceResult> ZCashPriceAsync();
        Task<DigiarzLitecoinPriceResult> LitecoinPriceAsync();
        Task<DigiarzFactomPriceResult> FactomPriceAsync();
        Task<DigiarzTypeResult> GetAsync(DigiarzCoin type);
        Task<DigiarzTypeResult> GetAsync(ECurrencyType type);
        Task<decimal> ExchangeRateAsync(ECurrencyType fromType, ECurrencyType toType);
        Task<decimal> USDPriceInTomanAsync();
        #endregion
    }
}
