﻿using RestSharp;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price.Cryptonator;

namespace Xtoman.Service.WebServices
{
    public class CryptonatorService : WebServiceManager, ICryptonatorService
    {
        #region Properties
        private readonly string baseUrl = "https://api.cryptonator.com/api";
        #endregion

        #region Async Methods
        public async Task<CryptonatorTicker> GetTickerAsync(string fromCurrency, string toCurrency)
        {
            var response = await GetResponseAsync("ticker", fromCurrency, toCurrency);
            return LogAndDeserialize<CryptonatorTicker>(response);
        }

        public async Task<CryptonatorFullTicker> GetTickerFullAsync(string fromCurrency, string toCurrency)
        {
            var response = await GetResponseAsync("full", fromCurrency, toCurrency);
            return LogAndDeserialize<CryptonatorFullTicker>(response);
        }
        #endregion

        #region Helper
        private async Task<string> GetResponseAsync(string action, string fromCurrency, string toCurrency)
        {
            var restClient = new RestClientRequest(baseUrl, action + "/" + $"{fromCurrency}-{toCurrency}");
            var response = await restClient.SendGetRequestAsync();
            return response.Content;
        }
        #endregion
    }
}
