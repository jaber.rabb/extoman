﻿using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price.Cryptonator;

namespace Xtoman.Service.WebServices
{
    public interface ICryptonatorService
    {
        Task<CryptonatorTicker> GetTickerAsync(string fromCurrency, string toCurrency);
        Task<CryptonatorFullTicker> GetTickerFullAsync(string fromCurrency, string toCurrency);
    }
}
