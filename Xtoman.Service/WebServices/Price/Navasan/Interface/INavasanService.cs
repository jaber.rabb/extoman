﻿using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price.Navasan;

namespace Xtoman.Service.WebServices
{
    public interface INavasanService
    {
        Task<NavasanLatest> GetAllLatestAsync();
        Task<NavasanLatestItem> GetLatestUSDPriceAsync();
        Task<long> DollarPriceAsync();
    }
}