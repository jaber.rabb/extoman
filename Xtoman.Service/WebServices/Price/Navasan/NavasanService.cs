﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Price.Navasan;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service.WebServices
{
    public class NavasanService : WebServiceManager, INavasanService
    {
        private readonly ICacheManager _cacheManager;
        private readonly string ApiKey = "9Zc9cA3mvBPJPqHfjlLtHc7eY51pYGbc";
        private readonly string BaseUrl = "https://api.navasan.net";
        private const string usdKey = "Xtoman.Navasan.DollarPrice";

        public NavasanService(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        public async Task<NavasanLatest> GetAllLatestAsync()
        {
            var response = await GetRequestAsync("latest");
            var result = LogAndDeserialize<NavasanLatest>(response);
            return result;
        }

        public async Task<NavasanLatestItem> GetLatestUSDPriceAsync()
        {
            try
            {
                //usd_sell
                var response = await GetRequestAsync("latest", new { item = "usd_sell" });
                var result = LogAndDeserialize<NavasanLatest>(response);
                return result.UsdSell;
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
            return new NavasanLatestItem() { Value = 0 };
        }

        public async Task<long> DollarPriceAsync()
        {
            try
            {
                return await _cacheManager.GetAsync(usdKey, 5, async () =>
                {
                    var latestUSDPrice = await GetLatestUSDPriceAsync();
                    return latestUSDPrice.Value;
                });
            }
            catch (Exception ex)
            {
                ex.LogError();
                return 0;
            }
        }

        #region Helpers
        private async Task<string> GetRequestAsync(string action, object inputs = null)
        {
            try
            {
                IEnumerable<PropertyInfo> validProperties = new List<PropertyInfo>();
                var restClient = new RestClient(BaseUrl);
                var restAction = action;

                var restRequest = new RestRequest(restAction, Method.GET);
                if (inputs != null)
                {
                    validProperties = inputs.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));
                    if (validProperties.Any())
                        foreach (var property in validProperties)
                        {
                            var value = property.GetValue(inputs)?.ToString() ?? "";
                            if (value.HasValue())
                                restRequest.AddQueryParameter(property.Name, property.GetValue(inputs)?.ToString() ?? "");
                        }
                }

                restRequest.AddQueryParameter("api_key", ApiKey);

                var searchOutputs = await restClient.ExecuteTaskAsync(restRequest).ConfigureAwait(false);
                return searchOutputs.Content;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return "";
            }
        }
        #endregion
    }
}
