﻿using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Partner.Changer;

namespace Xtoman.Service.WebServices
{
    public interface IChangerService
    {
        /// <summary>
        /// Get the current exchange rate of a specific pair.
        /// </summary>
        /// <param name="from">From ECurrencyType</param>
        /// <param name="to">To ECurrencyType</param>
        /// <param name="amount">Optional: You can specify the amount of send you want to exchange to get the exact rate that will be applied to your exchange.</param>
        /// <param name="isFromAmount">Optional: Can be either send if you want to specify the amount to be sent or receive if you want to specify the amount to be received. By default send is chosen.</param>
        /// <returns></returns>
        Task<ChangerExchangeRate> GetExchangeRateAsync(ECurrencyType from, ECurrencyType to, decimal? amount = null, bool isFromAmount = true);


    }
}
