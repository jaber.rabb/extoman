﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Partner.Changelly;

namespace Xtoman.Service.WebServices
{
    public interface IChangellyService
    {
        #region Sync methods
        /// <summary>
        /// Returns a list of enabled currencies as a flat array.
        /// </summary>
        /// <returns></returns>
        List<string> GetCurrencies();
        /// <summary>
        /// Returns a full list of currencies as an array of objects. Each object has an "enabled" field displaying current availability of a coin.
        /// </summary>
        /// <returns></returns>
        List<CurrencyFullItem> GetCurrenciesFull();
        /// <summary>
        /// Returns a minimum allowed payin amount required for a currency pair. Amounts less than a minimal will most likely fail the transaction.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        decimal? GetMinimumAmount(string from, string to);
        /// <summary>
        /// Returns estimated exchange value with your API partner fee included.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        decimal? GetExchangeAmount(string from, string to, decimal amount);
        decimal? GetExchangeAmount(string from, string to);
        decimal? GetExchangeAmount(GetExchangeAmountInput input);
        List<ExchangeAmount> GetExchangeAmount(List<GetExchangeAmountInput> input);
        /// <summary>
        /// Deprecated. Returns a pay-in address. A transaction ID will be generated later, while processing of the pay-in (when we will receive money). It’s better to use the "createTransaction" method instead, as "createTransaction" returns "transactionId" to monitor a transaction status.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="userPayoutAddress"></param>
        /// <returns></returns>
        [Obsolete("منسوخ شده. بهتر است از CreateTransaction استفاده شود")]
        string GenerateAddress(string from, string to, string userPayoutAddress);
        /// <summary>
        /// Creates a new transaction, generates a pay-in address and returns Transaction object with an ID field to track a transaction status.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="userPayoutAddress"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        CreateTransactionResult CreateTransaction(string from, string to, string userPayoutAddress, decimal amount);
        /// <summary>
        /// Returns status of a given transaction using a transaction ID provided.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        ChangellyStatus? GetStatus(string transactionId);
        /// <summary>
        /// Returns an array of all transactions or a filtered list of transactions.
        /// </summary>
        /// <returns></returns>
        List<TransactionResult> GetTransactions();
        List<TransactionResult> GetTransactions(GetTransactionsInput input);
        bool ExchangeAvailable(string from, string to);
        #endregion

        #region Async methods
        /// <summary>
        /// Returns a list of enabled currencies as a flat array.
        /// </summary>
        /// <returns></returns>
        Task<List<string>> GetCurrenciesAsync();
        /// <summary>
        /// Returns a full list of currencies as an array of objects. Each object has an "enabled" field displaying current availability of a coin.
        /// </summary>
        /// <returns></returns>
        Task<List<CurrencyFullItem>> GetCurrenciesFullAsync();
        /// <summary>
        /// Returns a minimum allowed payin amount required for a currency pair. Amounts less than a minimal will most likely fail the transaction.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        Task<decimal?> GetMinimumAmountAsync(string from, string to);
        /// <summary>
        /// Returns estimated exchange value with your API partner fee included.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        Task<decimal?> GetExchangeAmountAsync(string from, string to, decimal amount);
        Task<decimal?> GetExchangeAmountAsync(string from, string to);
        Task<decimal?> GetExchangeAmountAsync(GetExchangeAmountInput input);
        Task<List<ExchangeAmount>> GetExchangeAmountAsync(List<GetExchangeAmountInput> input);
        /// <summary>
        /// Deprecated. Returns a pay-in address. A transaction ID will be generated later, while processing of the pay-in Async(when we will receive money). It’s better to use the "createTransaction" method instead, as "createTransaction" returns "transactionId" to monitor a transaction status.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="userPayoutAddress"></param>
        /// <returns></returns>
        [Obsolete("منسوخ شده. بهتر است از CreateTransactionAsync استفاده شود")]
        Task<string> GenerateAddressAsync(string from, string to, string userPayoutAddress);
        /// <summary>
        /// Creates a new transaction, generates a pay-in address and returns Transaction object with an ID field to track a transaction status.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="userPayoutAddress"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        Task<CreateTransactionResult> CreateTransactionAsync(string from, string to, string userPayoutAddress, decimal amount);
        /// <summary>
        /// Returns status of a given transaction using a transaction ID provided.
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        Task<ChangellyStatus?> GetStatusAsync(string transactionId);
        /// <summary>
        /// Returns an array of all transactions or a filtered list of transactions.
        /// </summary>
        /// <returns></returns>
        Task<List<TransactionResult>> GetTransactionsAsync();
        Task<List<TransactionResult>> GetTransactionsAsync(GetTransactionsInput input);
        Task<bool> ExchangeAvailableAsync(string from, string to);
        #endregion
    }
}
