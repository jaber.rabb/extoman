﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Domain.Settings;
using Xtoman.Domain.WebServices.Partner.Changelly;
using Xtoman.Utility;

namespace Xtoman.Service.WebServices
{
    public class ChangellyService : WebServiceManager, IChangellyService
    {
        private static string apiKey;
        private static string apiSecret;
        private static readonly Encoding U8 = Encoding.UTF8;
        private static readonly string apiUrl = "https://api.changelly.com";
        private readonly PaymentSettings _paymentSettings;
        private static string id;
        public ChangellyService(PaymentSettings paymentSettings)
        {
            _paymentSettings = paymentSettings;
            apiKey = AppSettingManager.Changelly_Key;
            apiSecret = AppSettingManager.Changelly_Secret;
            id = DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }

        #region Sync methods
        public List<string> GetCurrencies()
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getCurrencies"",
    ""params"": []
}}";
            var response = GetResult(message);
            if (response.HasValue())
                return LogAndDeserialize<GetCurrenciesResult>(response).result;

            return null;
        }
        public List<CurrencyFullItem> GetCurrenciesFull()
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getCurrenciesFull"",
    ""params"": []
}}";
            var response = GetResult(message);
            if (response.HasValue())
                return LogAndDeserialize<GetCurrenciesFullResult>(response).result;

            return null;
        }
        public decimal? GetMinimumAmount(string from, string to)
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getMinAmount"",
    ""params"":{{
        ""from"": ""{from}"",
        ""to"": ""{to}""
    }}
}}";
            var response = GetResult(message);
            if (response.HasValue())
                return LogAndDeserialize<GetDecimalAmountResult>(response).result;

            return null;
        }
        public decimal? GetExchangeAmount(string from, string to, decimal amount)
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getExchangeAmount"",
    ""params"":{{
        ""from"": ""{from}"",
        ""to"": ""{to}"",
        ""amount"" : ""{amount}""
    }}
}}";
            var response = GetResult(message);
            if (response.HasValue())
                return LogAndDeserialize<GetDecimalAmountResult>(response).result;

            return null;
        }
        public decimal? GetExchangeAmount(GetExchangeAmountInput input)
        {
            return GetExchangeAmount(input.From, input.To, input.Amount);
        }
        public List<ExchangeAmount> GetExchangeAmount(List<GetExchangeAmountInput> input)
        {
            var parms = "";
            foreach (var item in input)
            {
                parms +=
$@"{{
    ""from"": ""{item.From}"",
    ""to"": ""{item.To}"",
    ""amount"": ""{item.Amount}""
}},";
            }
            if (string.IsNullOrWhiteSpace(parms))
                return null;

            parms = parms.Substring(0, parms.Length - 1);
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getExchangeAmount"",
    ""params"": [{parms}]
}}";
            var response = GetResult(message);
            if (response.HasValue())
                return LogAndDeserialize<GetExchangeAmountResult>(response).result;

            return null;
        }
        [Obsolete("منسوخ شده. بهتر است از CreateTransaction استفاده شود")]
        public string GenerateAddress(string from, string to, string userPayoutAddress)
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""generateAddress"",
    ""params"":{{
        ""from"": ""{from}"",
        ""to"": ""{to}"",
        ""address"": ""{userPayoutAddress}""
    }}
}}";
            return GetResult(message);
        }
        public CreateTransactionResult CreateTransaction(string from, string to, string userPayoutAddress, decimal amount)
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""createTransaction"",
    ""params"":{{
        ""from"": ""{from}"",
        ""to"": ""{to}"",
        ""address"": ""{userPayoutAddress}"",
        ""amount"" : {amount}
    }}
}}";
            var response = GetResult(message);
            if (response.HasValue())
                return LogAndDeserialize<CreateTransactionResponseResult>(response).result;

            return null;
        }
        public ChangellyStatus? GetStatus(string transactionId)
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getStatus"",
    ""params"":{{
  	    ""id"": ""{transactionId}""
    }}
}}";
            var response = GetResult(message);
            if (response.HasValue())
                //return (ChangellyStatus)Enum.Parse(typeof(ChangellyStatus), LogAndDeserialize<GetStatusResult>(response).result);
                return LogAndDeserialize<GetStatusResult>(response).EnumResult;

            return null;
        }
        public List<TransactionResult> GetTransactions()
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getTransactions"",
    ""params"": []
}}";
            var response = GetResult(message);
            if (response.HasValue())
                return LogAndDeserialize<GetTransactionResult>(response).result;

            return null;
        }
        public List<TransactionResult> GetTransactions(GetTransactionsInput input)
        {
            var currency = input.Currency.HasValue() ? $@"""{input.Currency}""" : "null";
            var address = input.Address.HasValue() ? $@"""{input.Address}""" : "null";
            var extraId = input.ExtraId.HasValue() ? $@"""{input.ExtraId}""" : "null";
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getTransactions"",
    ""params"": {{
        ""currency"": {currency},
        ""address"": {address},
        ""extraId"": {extraId},
        ""limit"": {input.Limit},
        ""offset"": {input.Offset}
    }}
}}";
            var response = GetResult(message);
            if (response.HasValue())
                return LogAndDeserialize<GetTransactionResult>(response).result;

            return null;
        }
        public decimal? GetExchangeAmount(string from, string to)
        {
            return GetExchangeAmount(from, to, 1);
        }
        public bool ExchangeAvailable(string from, string to)
        {
            var currencies = GetCurrenciesFull();
            var exchangeCurrencies = currencies?.Where(p => p.name == from || p.name == to || p.fullName == from || p.fullName == to).Select(p => p.enabled).ToList();
            if (exchangeCurrencies == null || exchangeCurrencies.Count != 2) return false;
            return exchangeCurrencies[0] && exchangeCurrencies[1];
        }
        #endregion

        #region Async methods
        public async Task<List<string>> GetCurrenciesAsync()
        {
            var message = 
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getCurrencies"",
    ""params"": []
}}";

            var response = await GetResultAsync(message);
            if (response.HasValue())
                return LogAndDeserialize<GetCurrenciesResult>(response).result;

            return null;
        }
        public async Task<List<CurrencyFullItem>> GetCurrenciesFullAsync()
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getCurrenciesFull"",
    ""params"": []
}}";

            var response = await GetResultAsync(message);
            if (response.HasValue())
                return LogAndDeserialize<GetCurrenciesFullResult>(response).result;

            return null;
        }
        public async Task<decimal?> GetMinimumAmountAsync(string from, string to)
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getMinAmount"",
    ""params"":{{
        ""from"": ""{from}"",
        ""to"": ""{to}""
    }}
}}";
            var response = await GetResultAsync(message);
            if (response.HasValue())
                return LogAndDeserialize<GetDecimalAmountResult>(response).result;

            return null;
        }
        public async Task<decimal?> GetExchangeAmountAsync(string from, string to, decimal amount)
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getExchangeAmount"",
    ""params"":{{
        ""from"": ""{from}"",
        ""to"": ""{to}"",
        ""amount"" : ""{amount}""
    }}
}}";
            var response = await GetResultAsync(message);
            if (response.HasValue())
                return LogAndDeserialize<GetDecimalAmountResult>(response).result;

            return null;
        }
        public async Task<decimal?> GetExchangeAmountAsync(GetExchangeAmountInput input)
        {
            return await GetExchangeAmountAsync(input.From, input.To, input.Amount);
        }
        public async Task<List<ExchangeAmount>> GetExchangeAmountAsync(List<GetExchangeAmountInput> input)
        {
            var parms = "";
            foreach (var item in input)
            {
                parms +=
$@"{{
    ""from"": ""{item.From}"",
    ""to"": ""{item.To}"",
    ""amount"": ""{item.Amount}""
}},";
            }
            if (string.IsNullOrWhiteSpace(parms))
                return null;

            parms = parms.Substring(0, parms.Length - 1);
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getExchangeAmount"",
    ""params"": [{parms}]
}}";
            var response = await GetResultAsync(message);
            if (response.HasValue())
                return LogAndDeserialize<GetExchangeAmountResult>(response).result;

            return null;
        }
        [Obsolete("منسوخ شده. بهتر است از CreateTransactionAsync استفاده شود")]
        public async Task<string> GenerateAddressAsync(string from, string to, string userPayoutAddress)
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""generateAddress"",
    ""params"":{{
        ""from"": ""{from}"",
        ""to"": ""{to}"",
        ""address"": ""{userPayoutAddress}""
    }}
}}";
            return await GetResultAsync(message);
        }
        public async Task<CreateTransactionResult> CreateTransactionAsync(string from, string to, string userPayoutAddress, decimal amount)
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""createTransaction"",
    ""params"":{{
        ""from"": ""{from}"",
        ""to"": ""{to}"",
        ""address"": ""{userPayoutAddress}"",
        ""amount"" : {amount}
    }}
}}";
            var response = await GetResultAsync(message);
            if (response.HasValue())
                return LogAndDeserialize<CreateTransactionResponseResult>(response).result;

            return null;
        }
        public async Task<ChangellyStatus?> GetStatusAsync(string transactionId)
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getStatus"",
    ""params"":{{
  	    ""id"": ""{transactionId}""
    }}
}}";
            var response = await GetResultAsync(message);
            if (response.HasValue())
                //return (ChangellyStatus)Enum.Parse(typeof(ChangellyStatus), LogAndDeserialize<GetStatusResult>(response).result);
                return LogAndDeserialize<GetStatusResult>(response).EnumResult;
            return null;
        }
        public async Task<List<TransactionResult>> GetTransactionsAsync()
        {
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getTransactions"",
    ""params"": []
}}";
            var response = await GetResultAsync(message);
            if (response.HasValue())
                return LogAndDeserialize<GetTransactionResult>(response).result;

            return null;
        }
        public async Task<List<TransactionResult>> GetTransactionsAsync(GetTransactionsInput input)
        {
            var currency = input.Currency.HasValue() ? $@"""{input.Currency}""" : "null";
            var address = input.Address.HasValue() ? $@"""{input.Address}""" : "null";
            var extraId = input.ExtraId.HasValue() ? $@"""{input.ExtraId}""" : "null";
            var message =
$@"{{
    ""jsonrpc"": ""2.0"",
    ""id"": ""{id}"",
    ""method"": ""getTransactions"",
    ""params"": {{
        ""currency"": {currency},
        ""address"": {address},
        ""extraId"": {extraId},
        ""limit"": {input.Limit},
        ""offset"": {input.Offset}
    }}
}}";
            var response = await GetResultAsync(message);
            if (response.HasValue())
                return LogAndDeserialize<GetTransactionResult>(response).result;

            return null;
        }
        public async Task<decimal?> GetExchangeAmountAsync(string from, string to)
        {
            return await GetExchangeAmountAsync(from, to, 1);
        }
        public async Task<bool> ExchangeAvailableAsync(string from, string to)
        {
            var currencies = await GetCurrenciesFullAsync();
            var exchangeCurrencies = currencies.Where(p => p.name == from || p.name == to || p.fullName == from || p.fullName == to).Select(p => p.enabled).ToList();
            if (exchangeCurrencies.Count != 2) return false;
            return exchangeCurrencies[0] && exchangeCurrencies[1];
        }
        #endregion

        #region Helpers
        private static string GetResult(string message)
        {
            var Client = new WebClient();
            Client.Headers.Set("Content-Type", "application/json");
            Client.Headers.Add("api-key", apiKey);
            Client.Headers.Add("sign", Sign(message));
            string response = "";
            try
            {
                response = Client.UploadString(apiUrl, message);
            }
            catch (WebException e)
            {
                response = new { error = "Exception while contacting Changelly.com: " + e.Message }.Serialize();
            }
            catch (Exception e)
            {
                response = new { error = "Unknown exception: " + e.Message }.Serialize();
            }
            return response;
        }
        private static async Task<string> GetResultAsync(string message)
        {
            var Client = new WebClient();
            Client.Headers.Set("Content-Type", "application/json");
            Client.Headers.Add("api-key", apiKey);
            Client.Headers.Add("sign", Sign(message));
            string response = "";
            try
            {
                response = await Client.UploadStringTaskAsync(apiUrl, message);
            }
            catch (WebException e)
            {
                response = new { error = "Exception while contacting Changelly.com: " + e.Message }.Serialize();
            }
            catch (Exception e)
            {
                response = new { error = "Unknown exception: " + e.Message }.Serialize();
            }
            return response;
        }
        private static string ToHexString(byte[] array)
        {
            var hex = new StringBuilder(array.Length * 2);
            foreach (byte b in array)
                hex.AppendFormat("{0:x2}", b);

            return hex.ToString();
        }
        private static string Sign(string message)
        {
            var hmac = new HMACSHA512(U8.GetBytes(apiSecret));
            var hashmessage = hmac.ComputeHash(U8.GetBytes(message));
            var sign = ToHexString(hashmessage);
            return sign;
        }
        #endregion
    }
}