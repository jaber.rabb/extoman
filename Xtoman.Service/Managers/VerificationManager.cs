﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Service.Managers
{
    public class VerificationManager : IVerificationManager
    {
        #region Properties
        private readonly IFinnotechResponseCardInfoService _finnotechResponseCardInfoService;
        //private readonly IMessageManager _messageManager;
        private readonly IFinnotechService _finnotechService;
        private readonly IUserBankCardService _userBankCardService;
        #endregion
        public VerificationManager(
            IFinnotechResponseCardInfoService finnotechResponseCardInfoService,
            //IMessageManager messageManager,
            IFinnotechService finnotechService,
            IUserBankCardService userBankCardService)
        {
            _finnotechResponseCardInfoService = finnotechResponseCardInfoService;
            //_messageManager = messageManager;
            _finnotechService = finnotechService;
            _userBankCardService = userBankCardService;
        }

        public async Task<bool> AutoCardValidationAsync(int userBankCardId)
        {
            try
            {
                var bankCard = await _userBankCardService.Table.Include(p => p.User).SingleAsync(p => p.Id == userBankCardId);
                var finnotech = new FinnotechCardInfo();
                var responseCard = bankCard.CardNumber.RemoveRight(12) + "-" + bankCard.CardNumber.RemoveLeft(4).RemoveRight(10) + "xx-xxxx-" + bankCard.CardNumber.RemoveLeft(12);
                var finnotechCardOldResponse = bankCard.FinnotechHistoryId.HasValue ?
                    await _finnotechResponseCardInfoService.Table.Where(p => p.Id == bankCard.FinnotechHistoryId || p.UserBankCardId == bankCard.Id).FirstOrDefaultAsync() :
                    await _finnotechResponseCardInfoService.Table.Where(p => p.Result == "0" && p.DestCard == responseCard).FirstOrDefaultAsync();

                if (finnotechCardOldResponse != null && finnotechCardOldResponse.StatusCode == 200)
                {
                    finnotechCardOldResponse.ForUserId = bankCard.UserId;
                    finnotechCardOldResponse.UserBankCardId = bankCard.Id;
                    await _finnotechResponseCardInfoService.UpdateAsync(finnotechCardOldResponse);
                    finnotech = new FinnotechCardInfo()
                    {
                        Description = finnotechCardOldResponse.Description,
                        Result = finnotechCardOldResponse.Result,
                        TrackId = new Guid(finnotechCardOldResponse.TrackId),
                        Name = finnotechCardOldResponse.Name,
                        DestCard = finnotechCardOldResponse.DestCard,
                        DoTime = finnotechCardOldResponse.DoTime,
                    };
                }
                else
                {
                    var finnotechCard = await _finnotechService.GetCardInformationAsync(bankCard.CardNumber);
                    if (finnotechCard != null)
                    {
                        var toInsert = new FinnotechResponseCardInfo()
                        {
                            Code = finnotechCard.Error?.Code,
                            Description = finnotechCard.Result.Description,
                            DestCard = finnotechCard.Result.DestCard,
                            ForUserId = bankCard.UserId,
                            DoTime = finnotechCard.Result.DoTime,
                            Message = finnotechCard.Error?.Message,
                            Name = finnotechCard.Result.Name,
                            TrackId = finnotechCard.TrackId.ToString(),
                            StatusCode = finnotechCard.StatusCode,
                            Result = finnotechCard.Result.Result,
                            UserBankCardId = bankCard.Id
                        };
                        await _finnotechResponseCardInfoService.InsertAsync(toInsert);
                        bankCard.FinnotechHistoryId = toInsert.Id;
                        await _userBankCardService.UpdateAsync(bankCard);

                        finnotech = new FinnotechCardInfo()
                        {
                            Description = finnotechCard.Result.Description,
                            Result = finnotechCard.Result.Result,
                            TrackId = new Guid(finnotechCard.TrackId),
                            Name = finnotechCard.Result.Name,
                            DestCard = finnotechCard.Result.DestCard,
                            DoTime = finnotechCard.Result.DoTime
                        };
                    }
                }
                var fullName = (bankCard.User.FirstName + " " + bankCard.User.LastName);
                if (bankCard.User.IdentityVerificationStatus == VerificationStatus.Confirmed && finnotech.Name.HasValue() && (finnotech.Name == fullName || finnotech.Name.Contains(fullName)))
                {
                    bankCard.VerificationStatus = VerificationStatus.Confirmed;
                    bankCard.VerifyDate = DateTime.UtcNow;

                    await _userBankCardService.UpdateAsync(bankCard);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
            return false;
        }
    }

    public class FinnotechCardInfo
    {
        [Display(Name = "کارت مقصد")]
        public string DestCard { get; set; }
        [Display(Name = "نام صاحب کارت")]
        public string Name { get; set; }
        [Display(Name = "نتیجه")]
        public string Result { get; set; }
        [Display(Name = "متن نتیجه")]
        public string ResultText => Result;
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        [Display(Name = "زمان انجام عملیات")]
        public string DoTime { get; set; }
        [Display(Name = "کد پیگیری")]
        public Guid TrackId { get; set; }
    }
}
