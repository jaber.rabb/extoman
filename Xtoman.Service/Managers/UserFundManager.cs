﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Service.Managers
{
    public class UserFundManager : IUserFundManager
    {
        private readonly IDiscountService _discountService;
        private readonly ICommissionService _commissionService;
        private readonly IUserIncomeService _userIncomeService;
        private readonly IUserWithdrawService _userWithdrawService;
        private readonly IExchangeOrderService _exchangeOrderService;
        public UserFundManager(
            IDiscountService discountService,
            ICommissionService commissionService,
            IUserIncomeService userIncomeService,
            IUserWithdrawService userWithdrawService,
            IExchangeOrderService exchangeOrderService)
        {
            _discountService = discountService;
            _commissionService = commissionService;
            _userIncomeService = userIncomeService;
            _userWithdrawService = userWithdrawService;
            _exchangeOrderService = exchangeOrderService;
        }

        public async Task<Discount> GetDiscountByUserIdAsync(int userId)
        {
            var userTotalOrdersTomanValue = await _exchangeOrderService.TableNoTracking
                .Where(p => p.UserId == userId)
                .Select(p => p.DollarPriceInToman * p.BTCPriceInUSD * p.BTCValueOfOrder)
                .DefaultIfEmpty(0)
                .SumAsync();

            return await _discountService.TableNoTracking
                .OrderByDescending(p => p.MinValue)
                .Where(p => userTotalOrdersTomanValue >= p.MinValue)
                .FirstOrDefaultAsync();
        }

        public async Task<decimal> GetDiscountPercentByUserIdAsync(int userId)
        {
            var discount = await GetDiscountByUserIdAsync(userId);
            return discount.Percent;
        }

        public async Task<int> GetDiscountIdByUserIdAsync(int userId)
        {
            var discount = await GetDiscountByUserIdAsync(userId);
            return discount.Id;
        }

        public async Task<decimal> CalculateUserOrdersValueInTomanAsync(int userId)
        {
            var userOrders = await _exchangeOrderService.TableNoTracking
                .Where(p => p.UserId == userId && p.OrderStatus == OrderStatus.Complete)
                .Select(p => new {
                    p.PayAmount,
                    p.Exchange.FromCurrency.Type
                })
                .ToListAsync();

            var value = 0M;
            foreach (var item in userOrders)
            {
                if (item.Type == ECurrencyType.Shetab)
                {
                    value += item.PayAmount;
                }
                else if (item.Type == ECurrencyType.Bitcoin)
                {

                }
            }
            return value;
        }
    }
}
