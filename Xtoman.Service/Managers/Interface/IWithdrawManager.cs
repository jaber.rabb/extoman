﻿using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IWithdrawManager
    {
        Task<(bool success, string message)> WithdrawAsync(ExchangeOrder order);
        WalletAddress GetOurWalletAddress(ECurrencyType currencyType, WalletApiType walletType);
        Task<(bool status, string text)> WithdrawCoinPaymentsToWalletAddressAsync(ECurrencyType currencyType, string address, string tag);
        Task<(bool status, string text)> WithdrawFromCoinPaymentsToBinanceAsync(ECurrencyType type);
        Task<(bool status, string text)> WithdrawFromCoinPaymentsForOrderAsync(ExchangeOrder order);
        Task<(bool status, string text)> WithdrawFromVandarForOrderAsync(ExchangeOrder order);
        Task<(bool status, string text)> CancelWithdrawFromVandarAsync(long id);
        Task<(bool status, string text)> WithdrawFromPayIRForOrderAsync(ExchangeOrder order);
    }
}
