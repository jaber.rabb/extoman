﻿using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IOrderManager
    {
        Task CompleteFiatTomanToFiatUSDOrderAsync(ExchangeOrder order);
        Task CompleteFiatToCoinOrderAsync(ExchangeOrder order);
    }
}
