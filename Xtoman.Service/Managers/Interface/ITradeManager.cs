﻿using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface ITradeManager
    {
        Task<TradeOrderResult> BuyCryptoTradeOrderAsync(ExchangeOrder order);
        Task<decimal> SellCryptoTradeOrderAsync(ExchangeOrder order, bool minusTransactionFee = false);
        //Task<(bool tradeResult, decimal fromAmount, decimal toAmount)> SellCryptoTradeOrderAsync(ExchangeOrder order);
        Task<TradeOrderResult> TryTradeAgain(ExchangeOrder order);
        Task<bool> CoinToCoinTradeOrderAsync(ExchangeOrder order);
    }
}