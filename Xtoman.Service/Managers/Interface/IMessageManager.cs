﻿using System.Threading.Tasks;

namespace Xtoman.Service
{
    public interface IMessageManager
    {
        Task NewRegisteredUserAsync(string emailBody, string emailOrPhone, string phoneCode, string phoneConfirmUrl, int? userId = null);
        Task NewPostAddedAsync(string emailBody, string title, string summary, string url, string imageFilePath);
        Task UserVerifyRequestAsync(string emailBody, string userEmail, string userPhoneNumber, string userName, string imgPath = "");
        Task SupportTicketSubmitedAsync(string emailBody, string email, string phoneNumber, string subject, string text, string ticketUrl);
        Task ForgotPasswordAsync(string emailBody, string emailOrPhone, string userName, string fullName, string confirmUrl, string code);
        Task UserVerifyRespondAsync(string emailBody, string userName, string email, string phoneNumber, string description, bool accepted);
        Task EmailChangedAsync(string emailBody, string fullName, string email);
        Task<string> TomanPaidAsync(string phoneNumber, string orderGuid, string transactionId = null, string payType = null, string status = null);
        Task PhoneNumberChangedAsync(string phoneNumber, string phoneCode, string phoneConfirmUrl);
        Task PhoneNumberAddedAsync(string phoneNumber, string phoneCode, string phoneConfirmUrl);
        Task NewOrderSubmitedAsync(string emailBody, string email, string phoneNumber, string userName, string orderGuid, string fromCurrencyName, string toCurrencyName, string payAmount, string receiveAmount);
        Task OrderPaidByTomanAsync(string emailBody, string email, string phoneNumber, string userName, long orderId, string orderGuid, decimal paidAmount);
        Task OrderPaidByFiatUSDAsync(string emailBody, string email, string phoneNumber, string userName, long orderId, string orderGuid, decimal paidAmount);
        Task OrderPaidAsync(string telegramText, string receiveAddress, string memo);
        Task SupportTicketAnsweredAsync(string emailBody, string ticketSubject, string ticketUrl, string emailOrPhoneNumber, string answerUser);
        Task CoinPaymentsPaidAsync(string emailBody, string email, string phoneNumber, string fullName, string orderGuid, string receivedAmount);
        Task TelegramAdminMessageAsync(string message);
        Task EmailAddedAsync(string emailAddedEmailBody, string fullName, string email);
        Task PhoneNumberConfirmSMSAsync(string code, string phoneNumber);
        Task EmailConfirmAsync(string confirmEmailBody, string emailAddress, string fullName);
        Task TelephoneVerifiedAsync(string emailBody, string fullName, string telephone, string phoneNumber, string email, string description, bool accepted);
        Task BankCardVerifiedAsync(string emailBody, string fullName, string cardNumber, string phoneNumber, string email, string description, bool accepted);
    }
}
