﻿using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IUserFundManager
    {
        Task<Discount> GetDiscountByUserIdAsync(int userId);
        Task<decimal> GetDiscountPercentByUserIdAsync(int userId);
        Task<int> GetDiscountIdByUserIdAsync(int userId);
    }
}
