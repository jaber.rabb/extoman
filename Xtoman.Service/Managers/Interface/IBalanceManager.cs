﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Domain;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IBalanceManager
    {
        Task<List<AvailableBalanceItem>> GetAllBalancesAsync(bool minusReserved = false);
        Task<decimal> GetBalanceAsync(ECurrencyType type, bool minusReserved = false);
        Task<decimal> GetBalanceAsync(string unitSign, bool minusReserved = false);
        decimal GetBalance(ECurrencyType type, bool minusReserved = false);
        Task<decimal> BinanceCurrenyBalanceAsync(ECurrencyType type);
        Task<decimal> GetPerfectUSDBalanceAsync();

        #region Version 2
        Task<List<BalanceItem>> GetAll2Async(bool minusReserved = false);
        #endregion
    }
}