﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IPriceManager
    {
        #region Sync methods
        ExchangeRate GetExchangeRate(ExchangeType exchangeType);
        ExchangeRate GetExchangeRate(int fromCurrencyId, int toCurrencyId);
        ExchangeRate GetExchangeRate(int exchangTypeId);
        ExchangePriceResult GetExchangeAmount(int exchangeTypeId, decimal amount, bool isFrom = true, bool minusReserved = false);
        ExchangePriceResult GetExchangeAmount(int fromCurrencyId, int toCurrencyId, decimal amount, bool isFrom = true, bool minusReserved = false);
        ExchangePriceResult GetExchangeAmount(ExchangeType exchangeType, decimal amount, bool isFrom = true, bool minusReserved = false);
        (decimal usdPrice, decimal eurPrice) USDEURPrice();
        #endregion

        #region Async methods
        Task<List<ExchangeRate>> GetAllBuyAndSellAsync();
        Task<ExchangeRate> GetExchangeRateAsync(ExchangeType exchangeType);
        Task<ExchangeRate> GetExchangeRateAsync(int fromCurrencyId, int toCurrencyId);
        Task<ExchangeRate> GetExchangeRateAsync(int exchangTypeId);
        Task<ExchangePriceResult> GetExchangeAmountAsync(int exchangeTypeId, decimal amount, bool isFrom = true, bool minusReserved = false);
        Task<ExchangePriceResult> GetExchangeAmountAsync(int fromCurrencyId, int toCurrencyId, decimal amount, bool isFrom = true, bool minusReserved = false);
        Task<ExchangePriceResult> GetExchangeAmountAsync(ExchangeType exchangeType, decimal amount, bool isFrom = true, bool minusReserved = false);
        Task<List<ExchangeRate>> GetExchangeRateAsync();
        Task<(decimal usdPrice, decimal eurPrice)> USDEURPriceAsync();
        Task<decimal> ReCalculateReceiveAmountAsync(ExchangeOrder order);
        //Task<ExchangeRate> GetExchangeRate2Async(ExchangeType exchangeType);
        //Task<ExchangeRate> GetExchangeRate2Async(int fromCurrencyId, int toCurrencyId);
        //Task<ExchangeRate> GetExchangeRate2Async(int exchangTypeId);
        #endregion
    }
}
