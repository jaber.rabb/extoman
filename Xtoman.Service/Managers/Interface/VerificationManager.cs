﻿using System.Threading.Tasks;

namespace Xtoman.Service
{
    public interface IVerificationManager
    {
        Task<bool> AutoCardValidationAsync(int userBankCardId);
    }
}
