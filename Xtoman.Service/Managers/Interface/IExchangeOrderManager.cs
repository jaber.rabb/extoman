﻿using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IExchangeOrderManager
    {
        Task<bool> TradeWithdrawAndCompleteOrderAsync(ExchangeOrder order);
    }
}
