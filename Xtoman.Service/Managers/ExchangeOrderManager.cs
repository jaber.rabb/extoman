﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.TradingPlatform.Kraken;
using Xtoman.Service.WebServices;

namespace Xtoman.Service
{
    public class ExchangeOrderManager : IExchangeOrderManager
    {
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly ITradeService _tradeService;
        private readonly IKrakenService _krakenService;
        private readonly IUserService _userService;
        public ExchangeOrderManager(
            IExchangeOrderService exchangeOrderService,
            IKrakenService krakenService,
            ITradeService tradeService,
            IUserService userService)
        {
            _exchangeOrderService = exchangeOrderService;
            _krakenService = krakenService;
            _tradeService = tradeService;
            _userService = userService;
        }

        public async Task<bool> IsOrderTomanAmountValidForUserAsync(int userId, int amount)
        {
            var maxAmount = 2000000; // 2 Million Toman
            var fromDate = new DateTime(2019,6,1);
            var now = DateTime.UtcNow;
            if (await _userService.TableNoTracking.Where(p => p.Id == userId && p.RegDate > fromDate).AnyAsync())
            {
                var userShetabOrders = await _exchangeOrderService.TableNoTracking
                    .Where(p => p.Exchange.FromCurrency.Type == ECurrencyType.Shetab && (p.PaymentStatus == PaymentStatus.Paid || p.PaymentStatus == PaymentStatus.Voided))
                    .ToListAsync();
                if (userShetabOrders.Count > 0)
                {
                    var totalShetabOrdersAmount = userShetabOrders.Sum(p => p.PayAmount);
                    var firstOrderDate = userShetabOrders.OrderBy(p => p.InsertDate).Select(p => p.InsertDate).FirstOrDefault();
                    if (firstOrderDate <= now)
                    {

                    }
                }
                return amount > maxAmount ? false : true;
            }
            return true;
        }

        public async Task<bool> TradeWithdrawAndCompleteOrderAsync(ExchangeOrder order)
        {
            //1: Trade
            bool krakenTradeStatus = false;
            var trades = new List<Trade>();
            var krakenTradeExist = order.OrderTrades.Where(p => p.TradingPlatform == TradingPlatform.Kraken).Any();
            var toCurrencyType = order.Exchange.ToCurrency.Type;
            var krakenVolume = order.ReceiveAmount + order.GatewayFee;
            if (krakenVolume > 0 && !krakenTradeExist)
            {
                var fromKrakenAsset = order.Exchange.FromCurrency.Type.ToKrakenAsset();
                var krakenTradePair = $"{fromKrakenAsset}ZUSD";
                var krakenTradeFeeResult = _krakenService.GetAssetPairs(new List<string>() { krakenTradePair });
                var krakenPairInfo = krakenTradeFeeResult.Result.FirstOrDefault().Value;
                krakenVolume += (krakenVolume * krakenPairInfo.Fees[0][1] / 100);
                try
                {
                    var retryCount = 0;
                    while (!krakenTradeStatus && retryCount < 3)
                    {
                        retryCount++;
                        var krakenTradeResult = _krakenService.AddStandardOrder(krakenTradePair, KrakenOrderSide.Buy, KrakenOrderType.Market, krakenVolume, null, null, null, new List<KrakenOrderFlag>() { KrakenOrderFlag.PreferFeeInQuoteCurrency });
                        if (krakenTradeResult.ResultType != KrakenGeneralResultType.success || (krakenTradeResult.Errors != null && krakenTradeResult.Errors.Count > 0))
                        {
                            order.OrderStatus = OrderStatus.TradeFailed;
                            if (krakenTradeResult.ResultType == KrakenGeneralResultType.exception)
                                order.Errors = "KRK!" + krakenTradeResult.Exception.Message;
                            else if (krakenTradeResult.Errors != null && krakenTradeResult.Errors.Count > 0)
                                order.Errors = "KRK! " + string.Join(",", krakenTradeResult.Errors);
                            await _exchangeOrderService.UpdateAsync(order);
                        }
                        else
                        {
                            krakenTradeStatus = true;
                            var krakenTradeId = krakenTradeResult.Result.TransactionIds.FirstOrDefault();
                            var krakenTrade = new Trade()
                            {
                                TradingPlatform = TradingPlatform.Kraken,
                                ApiTradeId = krakenTradeId,
                                ExchangeOrderId = order.Id,
                                FromCryptoType = ECurrencyType.USDollar,
                                ToCryptoType = order.Exchange.ToCurrency.Type,
                                TradingFeeType = order.Exchange.ToCurrency.Type,
                                ToAmount = krakenVolume,
                                TradeStatus = TradeStatus.Completed,
                            };

                            System.Threading.Thread.Sleep(500);

                            var krakenOrderDetails = _krakenService.QueryOrdersInfo(krakenTradeId);
                            if (!krakenOrderDetails.Errors.Any() && krakenOrderDetails.ResultType == KrakenGeneralResultType.success)
                            {
                                var krakenOrder = krakenOrderDetails.Result.Values.FirstOrDefault();
                                krakenTrade.FromAmount = krakenOrder.Cost;
                                krakenTrade.TradingFee = krakenOrder.Fee;
                            }
                            else
                            {
                                krakenTrade.TradeStatus = TradeStatus.Voided;
                            }
                            await _tradeService.InsertAsync(krakenTrade);
                            order.Errors = null;
                            order.OrderStatus = OrderStatus.Processing;
                            await _exchangeOrderService.UpdateAsync(order);
                            trades.Add(krakenTrade);
                        }
                    }
                }
                catch (Exception ex)
                {
                    order.OrderStatus = OrderStatus.TradeFailed;
                    order.Errors = "KRK! " + ex.Message;
                    await _exchangeOrderService.UpdateAsync(order);
                    krakenTradeStatus = false;
                }
            }
            else
            {
                krakenTradeStatus = true;
            }
            //2: Withdraw

            //3: 
            return false;
        }
    }
}
