﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.TradingPlatform.Binance;
using Xtoman.Service.WebServices;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service
{
    public class BalanceManager : IBalanceManager
    {
        #region Properties
        private readonly ICacheManager _cacheManager;
        private readonly IBinanceService _binanceService;
        private readonly IPerfectMoneyService _perfectMoneyService;
        private readonly ICoinPaymentsService _coinPaymentsService;
        private readonly ICurrencyTypeService _currencyTypeService;
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly List<CurrencyType> _cachedCurrencyTypes;
        #endregion

        #region Constructor
        public BalanceManager(
            ICacheManager cacheManager,
            IBinanceService binanceService,
            IPerfectMoneyService perfectMoneyService,
            ICoinPaymentsService coinPaymentsService,
            IExchangeOrderService exchangeOrderService,
            ICurrencyTypeService currencyTypeService)
        {
            _cacheManager = cacheManager;
            _binanceService = binanceService;
            _perfectMoneyService = perfectMoneyService;
            _coinPaymentsService = coinPaymentsService;
            _currencyTypeService = currencyTypeService;
            _exchangeOrderService = exchangeOrderService;

            _cachedCurrencyTypes = _currencyTypeService.GetAllActiveCached();
        }
        #endregion

        public async Task<List<AvailableBalanceItem>> GetAllBalancesAsync(bool minusReserved = false)
        {
            var result = new List<AvailableBalanceItem>();
            var binanceAccountInfo = await _binanceService.GetAccountInfoAsync(50000);
            var availableUSDT = binanceAccountInfo.Balances
                .Where(p => p.Asset.Equals("USDT", StringComparison.InvariantCultureIgnoreCase))
                .Select(p => p.Free).FirstOrDefault();

            var binancePrices = await _binanceService.GetAllPricesAsync();

            foreach (var currencyType in _cachedCurrencyTypes)
            {
                if (!currencyType.IsFiat)
                {
                    if (minusReserved)
                    {
                        var reservedUSDTs = await CalculateReservedUSDTsAsync(binancePrices);
                        availableUSDT -= reservedUSDTs;
                    }

                    result.Add(new AvailableBalanceItem()
                    {
                        Amount = CalculateTradeableUSDT(currencyType, availableUSDT, binancePrices),
                        Type = currencyType.Type,
                        Name = currencyType.Name
                    });
                }
                else
                {
                    switch (currencyType.Type)
                    {
                        case ECurrencyType.Shetab:
                            result.Add(new AvailableBalanceItem()
                            {
                                Amount = currencyType.Available.GetValueOrDefault(0),
                                Type = currencyType.Type,
                                Name = currencyType.Name
                            });
                            break;
                        case ECurrencyType.PerfectMoney:
                        case ECurrencyType.PerfectMoneyVoucher:
                            result.Add(new AvailableBalanceItem()
                            {
                                Amount = await GetPerfectUSDBalanceAsync(),
                                Name = currencyType.Name,
                                Type = currencyType.Type
                            });
                            break;
                        default:
                            result.Add(new AvailableBalanceItem()
                            {
                                Amount = 0,
                                Type = currencyType.Type,
                                Name = currencyType.Name
                            });
                            break;
                    }
                }
            }
            return result;
        }

        public async Task<decimal> GetBalanceAsync(ECurrencyType type, bool minusReserved = false)
        {
            var currencyType = _cachedCurrencyTypes.Where(p => p.Type == type).FirstOrDefault();
            if (currencyType != null)
            {
                if (!currencyType.IsFiat)
                {
                    var binancePrices = await _binanceService.GetAllPricesAsync();
                    var binanceAccountInfo = await _binanceService.GetAccountInfoAsync(50000);
                    var availableUSDT = binanceAccountInfo.Balances
                        .Where(p => p.Asset.Equals("USDT", StringComparison.InvariantCultureIgnoreCase))
                        .Select(p => p.Free).FirstOrDefault();

                    if (minusReserved)
                    {
                        var reservedUSDTs = await CalculateReservedUSDTsAsync(binancePrices);
                        availableUSDT -= reservedUSDTs;
                    }

                    var available = CalculateTradeableUSDT(currencyType, availableUSDT, binancePrices);
                    return available < 0 ? 0 : available;
                }
                else
                {
                    switch (currencyType.Type)
                    {
                        case ECurrencyType.Shetab:
                            return currencyType.Available.GetValueOrDefault(0);
                        case ECurrencyType.PerfectMoney:
                        case ECurrencyType.PerfectMoneyVoucher:
                            return await GetPerfectUSDBalanceAsync();
                        default:
                            return 0;
                    }
                }
            }
            return 0;
        }

        public async Task<decimal> GetBalanceAsync(string unitSign, bool minusReserved = false)
        {
            var currency = ECurrencyTypeHelper.ToECurrencyAccountType(unitSign);
            return await GetBalanceAsync(currency.Value, minusReserved);
        }

        public async Task<decimal> BinanceCurrenyBalanceAsync(ECurrencyType type)
        {
            var binanceAsset = type.ToBinanceAsset();
            var result = await _binanceService.GetAccountInfoAsync(50000);
            return result.Balances
                .Where(p => p.Asset == binanceAsset)
                .Select(p => p.Free).FirstOrDefault();
        }

        public decimal GetBalance(ECurrencyType type, bool minusReserved = false)
        {
            var currencyType = _cachedCurrencyTypes.Where(p => p.Type == type).FirstOrDefault();
            if (currencyType != null)
            {
                if (!currencyType.IsFiat)
                {
                    var binancePrices = _binanceService.GetAllPrices();
                    var binanceAccountInfo = _binanceService.GetAccountInfo();
                    var availableUSDT = binanceAccountInfo.Balances
                        .Where(p => p.Asset.Equals("USDT", StringComparison.InvariantCultureIgnoreCase))
                        .Select(p => p.Free).FirstOrDefault();

                    if (minusReserved)
                    {
                        var reservedUSDTs = CalculateReservedUSDTs(binancePrices);
                        availableUSDT -= reservedUSDTs;
                    }

                    var available = CalculateTradeableUSDT(currencyType, availableUSDT, binancePrices);
                    return available < 0 ? 0 : available;
                }
                else
                {
                    switch (currencyType.Type)
                    {
                        case ECurrencyType.Shetab:
                            return currencyType.Available.GetValueOrDefault(0);
                        case ECurrencyType.PerfectMoney:
                            return GetPerfectUSDBalance();
                        default:
                            return 0;
                    }
                }
            }
            return 0;
        }

        private decimal GetPerfectUSDBalance()
        {
            var usdBalance = _perfectMoneyService.Balance()?.Accounts?.Where(p => p.WalletNumber == AppSettingManager.PerfectMoney_USD_Account).FirstOrDefault()?.Balance ?? 0;
            return usdBalance - (usdBalance * 5 / 1000);
        }

        private decimal CalculateReservedUSDTs(List<BinanceSymbolPrice> binancePrices)
        {
            var reservedOrders = _exchangeOrderService.TableNoTracking
                .Where(p => !p.Exchange.ToCurrency.IsFiat)
                .Where(p => p.OrderStatus.HasFlag(OrderStatus.Pending | OrderStatus.Processing))
                .Where(p => p.PaymentStatus.HasFlag(PaymentStatus.InPayment))
                .Select(p => new { Amount = p.ReceiveAmount + p.GatewayFee, p.Exchange.ToCurrency.Type })
                .GroupBy(p => p.Type)
                .Select(p => new CurrencyAmountItem
                {
                    Type = p.Key,
                    Amount = p.Sum(x => x.Amount),
                })
                .ToList();

            var totalReservedUSDT = 0M;
            foreach (var reservedOrder in reservedOrders)
            {
                if (reservedOrder.Type == ECurrencyType.Tether)
                    totalReservedUSDT += reservedOrder.Amount;
                else
                {
                    var asset = reservedOrder.Type.ToBinanceAsset();
                    var pair = asset + "USDT";
                    if (binancePrices.Any(x => x.Pair == asset))
                    {
                        var price = binancePrices.Where(p => p.Pair == $"{asset}USDT").Select(p => p.Price).DefaultIfEmpty(0).FirstOrDefault();
                        totalReservedUSDT += (reservedOrder.Amount * price) + (reservedOrder.Amount * price / 1000);
                    }
                    else
                    {
                        pair = asset + "BTC";
                        var priceInBTC = binancePrices.Where(p => p.Pair == $"{asset}BTC").Select(p => p.Price).DefaultIfEmpty(0).FirstOrDefault();
                        var totalReservedBTC = (reservedOrder.Amount * priceInBTC) + (reservedOrder.Amount * priceInBTC / 1000);

                        var bTCPrice = binancePrices.Where(p => p.Pair == $"BTCUSDT").Select(p => p.Price).DefaultIfEmpty(0).FirstOrDefault();
                        totalReservedUSDT = (totalReservedBTC * bTCPrice) + (totalReservedBTC * bTCPrice / 1000);
                    }
                }
            }
            return totalReservedUSDT;
        }


        #region Helper
        private decimal CalculateTradeableUSDT(CurrencyType currencyType, decimal availableUSDT, List<BinanceSymbolPrice> binancePrices)
        {
            if (currencyType.Type == ECurrencyType.Tether)
                return availableUSDT;

            var binanceAsset = currencyType.Type.ToBinanceAsset();

            var pair = binanceAsset + "USDT";
            if (binancePrices.Where(p => p.Symbol.Equals(pair, StringComparison.InvariantCultureIgnoreCase)).Any())
            {
                var price = binancePrices.Where(p => p.Symbol.Equals(pair, StringComparison.InvariantCultureIgnoreCase))
                    .Select(p => p.Price)
                    .DefaultIfEmpty(0)
                    .FirstOrDefault();

                var available = availableUSDT / price;
                available = available - (available / 1000);
                available = available - (currencyType.TransactionFee ?? 0);
                return available < 0 ? 0 : available;
            }
            else
            {
                pair = binanceAsset + "BTC";
                if (binancePrices.Where(p => p.Symbol.Equals(pair, StringComparison.InvariantCultureIgnoreCase)).Any())
                {
                    var availableBTC = CalculateTradeableUSDT(_cachedCurrencyTypes.Where(p => p.Type == ECurrencyType.Bitcoin).FirstOrDefault(), availableUSDT, binancePrices);
                    availableBTC = availableBTC - (availableBTC / 1000);

                    var priceInBTC = binancePrices.Where(p => p.Symbol.Equals(pair, StringComparison.InvariantCultureIgnoreCase))
                    .Select(p => p.Price)
                    .DefaultIfEmpty(0)
                    .FirstOrDefault();

                    var available = (availableBTC / priceInBTC) - (currencyType.TransactionFee ?? 0);
                    return available < 0 ? 0 : available;
                }
            }
            if (binanceAsset != "DOGE")
                new Exception($"for the asset {binanceAsset} available was 0 because it doesn't exist in binance").LogError();
            return 0;
        }
        private async Task<decimal> CalculateReservedUSDTsAsync(List<BinanceSymbolPrice> binancePrices)
        {
            var reservedOrders = await _exchangeOrderService.TableNoTracking
                .Where(p => !p.Exchange.ToCurrency.IsFiat)
                .Where(p => p.OrderStatus.HasFlag(OrderStatus.Pending | OrderStatus.Processing))
                .Where(p => p.PaymentStatus.HasFlag(PaymentStatus.InPayment))
                .Select(p => new { Amount = p.ReceiveAmount + p.GatewayFee, p.Exchange.ToCurrency.Type })
                .GroupBy(p => p.Type)
                .Select(p => new CurrencyAmountItem
                {
                    Type = p.Key,
                    Amount = p.Sum(x => x.Amount),
                })
                .ToListAsync();

            var totalReservedUSDT = 0M;
            foreach (var reservedOrder in reservedOrders)
            {
                if (reservedOrder.Type == ECurrencyType.Tether)
                    totalReservedUSDT += reservedOrder.Amount;
                else
                {
                    var asset = reservedOrder.Type.ToBinanceAsset();
                    var pair = asset + "USDT";
                    if (binancePrices.Any(x => x.Pair == asset))
                    {
                        var price = binancePrices.Where(p => p.Pair == $"{asset}USDT").Select(p => p.Price).DefaultIfEmpty(0).FirstOrDefault();
                        totalReservedUSDT += (reservedOrder.Amount * price) + (reservedOrder.Amount * price / 1000);
                    }
                    else
                    {
                        pair = asset + "BTC";
                        var priceInBTC = binancePrices.Where(p => p.Pair == $"{asset}BTC").Select(p => p.Price).DefaultIfEmpty(0).FirstOrDefault();
                        var totalReservedBTC = (reservedOrder.Amount * priceInBTC) + (reservedOrder.Amount * priceInBTC / 1000);

                        var bTCPrice = binancePrices.Where(p => p.Pair == $"BTCUSDT").Select(p => p.Price).DefaultIfEmpty(0).FirstOrDefault();
                        totalReservedUSDT = (totalReservedBTC * bTCPrice) + (totalReservedBTC * bTCPrice / 1000);
                    }
                }
            }
            return totalReservedUSDT;
        }

        public async Task<decimal> GetPerfectUSDBalanceAsync()
        {
            var usdBalance = (await _perfectMoneyService.BalanceAsync().ConfigureAwait(false))?.Accounts?.Where(p => p.WalletNumber == AppSettingManager.PerfectMoney_USD_Account).FirstOrDefault()?.Balance ?? 0;
            return usdBalance - (usdBalance * 5 / 1000);
        }

        public async Task<List<BalanceItem>> GetAll2Async(bool minusReserved = false)
        {
            var result = new List<BalanceItem>();
            var binancePrices = await _binanceService.GetAllPricesAsync();
            var binanceBalances = await _binanceService.GetAllBalancesAsync();
            var availableUSDT = binanceBalances.Assets
                .Where(p => p.Name.Equals("USDT", StringComparison.InvariantCultureIgnoreCase))
                .Select(p => p.Free).FirstOrDefault();

            foreach (var currencyType in _cachedCurrencyTypes)
            {
                if (!currencyType.IsFiat)
                {
                    if (minusReserved)
                    {
                        var reservedUSDTs = await CalculateReservedUSDTsAsync(binancePrices);
                        availableUSDT -= reservedUSDTs;
                    }

                    result.Add(new BalanceItem()
                    {
                        Amount = CalculateTradeableUSDT(currencyType, availableUSDT, binancePrices),
                        Type = currencyType.Type,
                        Name = currencyType.Name
                    });
                }
                else
                {
                    switch (currencyType.Type)
                    {
                        case ECurrencyType.PerfectMoney:
                        case ECurrencyType.PerfectMoneyVoucher:
                            result.Add(new BalanceItem()
                            {
                                Amount = await GetPerfectUSDBalanceAsync(),
                                Name = currencyType.Name,
                                Type = currencyType.Type
                            });
                            break;
                        default:
                            result.Add(new BalanceItem()
                            {
                                Amount = 0,
                                Type = currencyType.Type,
                                Name = currencyType.Name
                            });
                            break;
                    }
                }
            }
            return result;
        }
        #endregion
    }

    #region Object Classes
    public class CurrencyAmountItem
    {
        public decimal Amount { get; set; }
        public ECurrencyType Type { get; set; }
    }

    public class AvailableBalanceItem : CurrencyAmountItem
    {
        public string Name { get; set; }
    }
    #endregion
}
