﻿// Copyright (c) Microsoft Corporation, Inc. All rights reserved.
// Licensed under the MIT License, Version 2.0. See License.txt in the project root for license information.

using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity.SqlServer.Utilities;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Service.Managers
{
    /// <summary>
    ///     TokenProvider that generates time based codes using the user's security stamp
    /// </summary>
    /// <typeparam name="TUser"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public class AppTotpSecurityStampBasedTokenProvider : TotpSecurityStampBasedTokenProvider<AppUser, int>
    {
        internal async Task<AppSecurityToken> CreateSecurityTokenAsync(int userId, UserManager<AppUser, int> manager)
        {
            var securityStamp = await manager.GetSecurityStampAsync(userId).WithCurrentCulture();
            var securityStampBytes = Encoding.Unicode.GetBytes(securityStamp);
            return
                new AppSecurityToken(securityStampBytes);
        }

        public override async Task<string> GenerateAsync(string purpose, UserManager<AppUser, int> manager, AppUser user)
        {
            var token = await CreateSecurityTokenAsync(user.Id, manager).WithCurrentCulture();
            var modifier = await GetUserModifierAsync(purpose, manager, user).WithCurrentCulture();
            return AppOTPAuthenticationService.GenerateCode(token, modifier).ToString("D6", CultureInfo.InvariantCulture);
        }

        public override async Task<bool> ValidateAsync(string purpose, string token, UserManager<AppUser, int> manager, AppUser user)
        {
            if (!Int32.TryParse(token, out int code))
                return false;
            var securityToken = await CreateSecurityTokenAsync(user.Id, manager).WithCurrentCulture();
            var modifier = await GetUserModifierAsync(purpose, manager, user).WithCurrentCulture();
            return securityToken != null && AppOTPAuthenticationService.ValidateCode(securityToken, code, modifier);
        }
    }
}