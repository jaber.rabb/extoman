﻿using System;
using System.Net.Mail;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Service
{
    public class MessageManager : IMessageManager
    {
        #region Properties
        private const string reportChannelId = "-1001377473448";
        private const string publicChannelId = "@extoman";
        private const string reportReceiverEmail = "reports@extoman.com";
        private const string reportReceiverPhoneNumber = "09336705380";
        private readonly ITelegramService _telegramService;
        private readonly MessageSettings _messageSetting;
        private readonly IQueuedMessageService _queuedMessageService;
        private readonly IUserService _userService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly ISMSSenderService _smsSenderService;
        private readonly IFarazsmsService _farazsmsService;
        private readonly ISmarterMailService _smarterMailService;
        #endregion

        #region Constroctor
        public MessageManager(
            ITelegramService telegramService,
            MessageSettings messageSettings,
            IQueuedMessageService queuedMessageService,
            IUserService userService,
            IEmailAccountService emailAccountService,
            ISMSSenderService sMSSenderService,
            IFarazsmsService farazsmsService,
            ISmarterMailService smarterMailService)
        {
            _telegramService = telegramService;
            _messageSetting = messageSettings;
            _queuedMessageService = queuedMessageService;
            _userService = userService;
            _emailAccountService = emailAccountService;
            _smsSenderService = sMSSenderService;
            _farazsmsService = farazsmsService;
            _smarterMailService = smarterMailService;
        }
        #endregion

        #region Async methods
        public async Task NewRegisteredUserAsync(string emailBody, string emailOrPhone, string phoneCode, string phoneConfirmUrl, int? userId = null)
        {
            #region Telegram
            #region ToAdmin
            if (_messageSetting.Telegram_Admin_Registration)
            {
                try
                {
                    var userIdText = userId.HasValue ? $"و کد {userId.Value} " : "";
                    var message = $@"کاربر جدید با نام کاربری {emailOrPhone} {userIdText} ثبت نام کرد.";
                    var channelId = _messageSetting.Telegram_Admin_Registration_ChannelId.HasValue() ? _messageSetting.Telegram_Admin_Registration_ChannelId : reportChannelId;
                    var result = await _telegramService.SendMessageAsync(message, channelId).ConfigureAwait(false);
                    if (!result.ok)
                        new Exception($"Telegram: New user register message send failed. user = {emailOrPhone}").LogError();
                }
                catch (Exception)
                {
                }
            }
            #endregion
            #endregion

            #region Email
            var emailAccountId = _messageSetting.Email_Registration_EmailAccountId != 0 ? _messageSetting.Email_Registration_EmailAccountId : new int?();
            #region To User
            if (_messageSetting.Email_User_Registration && emailOrPhone.IsEmail())
            {
                var email = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"{emailOrPhone} به جمع کاربران ایکس تومن خوش آمدید.",
                    To = emailOrPhone,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(email);
            }
            #endregion
            #region To Admin
            //if (_messageSetting.Email_Admin_Registration)
            //{
            //    var adminEmailBody = $"کاربر جدید با نام کاربری {emailOrPhone} ثبت نام کرد.";
            //    var to = _messageSetting.ReportsReceiver_EmailAccount.HasValue() ? _messageSetting.ReportsReceiver_EmailAccount : reportReceiverEmail;
            //    var email = new QueuedMessage()
            //    {
            //        EmailAccountId = emailAccountId,
            //        Subject = $"کاربر جدید {emailOrPhone}",
            //        To = to,
            //        Type = QueuedMessageType.Email,
            //        Text = adminEmailBody
            //    };
            //    await _queuedMessageService.InsertAsync(email);
            //}
            #endregion
            #endregion

            #region SMS
            #region To User
            if (_messageSetting.SMS_User_Registration && emailOrPhone.IsMobile())
            {
                var to = emailOrPhone.Replace("-", "");
                await _farazsmsService.NewRegisterAsync(to, emailOrPhone);
                await _farazsmsService.VerifyPhoneAsync(to, phoneCode, true);
            }
            #endregion
            #region To Admin
            if (_messageSetting.SMS_Admin_Registration)
            {
                var to = _messageSetting.ReportsReceiver_PhoneNumber.HasValue() ? _messageSetting.ReportsReceiver_PhoneNumber : reportReceiverPhoneNumber;
                var sms = new QueuedMessage()
                {
                    To = to,
                    Text = $"کاربر جدید با نام {emailOrPhone} ثبت نام کرد.",
                    Type = QueuedMessageType.SMS
                };
                await _queuedMessageService.InsertAsync(sms);
            }
            #endregion
            #endregion
        }
        public async Task NewPostAddedAsync(string emailBody, string title, string summary, string url, string imageFilePath)
        {
            #region Telegram
            url = $"https://www.extoman.co/post/{url}";
            var message = $"{title}\n{summary}\n<a href=\"{url}\">{url}</a>";
            await _telegramService.SendImageAsync(message, publicChannelId, imageFilePath);
            #endregion

            //#region Email to all members
            //var usersEmail = await _userService.TableNoTracking.Select(p => p.Email).Where(p => p != null).ToListAsync();
            //var emailAccountId = _messageSetting.Default_EmailAccountId > 0 ? _messageSetting.Default_EmailAccountId : new int?();
            //var toAdd = new List<QueuedMessage>();
            //foreach (var emailAddress in usersEmail)
            //{
            //    toAdd.Add(new QueuedMessage()
            //    {
            //        To = emailAddress,
            //        EmailAccountId = emailAccountId,
            //        Subject = title,
            //        Type = QueuedMessageType.Email,
            //        Text = emailBody
            //    });
            //}
            //await _queuedMessageService.InsertAsync(toAdd);
            //#endregion
        }
        public async Task UserVerifyRequestAsync(string emailBody, string userEmail, string userPhoneNumber, string userName, string imgPath = "")
        {
            #region Telegram
            if (_messageSetting.Telegram_Admin_UserVerifyRequest)
            {
                var message = $"کاربر {userName} درخواست احراز هویت دارد.";
                var channelId = _messageSetting.Telegram_Admin_UserVerifyRequest_ChannelId.HasValue() ? _messageSetting.Telegram_Admin_UserVerifyRequest_ChannelId : reportChannelId;
                var result = await _telegramService.SendMessageAsync(message, channelId).ConfigureAwait(false);
                if (!result.ok)
                    new Exception($"Telegram: User verify request message for user {userName} send failed").LogError();
            }
            #endregion

            #region Email
            var emailAccountId = _messageSetting.Email_UserVerifyRequest_EmailAccountId != 0 ? _messageSetting.Email_UserVerifyRequest_EmailAccountId : new int?();
            #region To User
            if (_messageSetting.Email_User_UserVerifyRequest && userEmail.HasValue() && userEmail.IsEmail())
            {
                var email = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"کاربر ایکس تومن {userName} درخواست احراز هویت شما ثبت گردید.",
                    To = userEmail,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(email);
            }
            #endregion
            #region To Admin
            if (_messageSetting.Email_Admin_UserVerifyRequest)
            {
                emailBody = $"کاربر {userName} درخواست احراز هویت دارد.";
                var to = _messageSetting.ReportsReceiver_EmailAccount.HasValue() ? _messageSetting.ReportsReceiver_EmailAccount : reportReceiverEmail;
                var email = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"کاربر {userName} درخواست احراز هویت دارد.",
                    To = to,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(email);
            }
            #endregion
            #endregion

            #region SMS
            #region To User
            if (_messageSetting.SMS_User_UserVerifyRequest && userPhoneNumber.HasValue() && userPhoneNumber.IsMobile())
            {
                var to = userPhoneNumber.Replace("-", "");
                await _farazsmsService.VerifyIdentityAsync(to, userName);
            }
            #endregion
            #region To Admin
            if (_messageSetting.SMS_Admin_UserVerifyRequest)
            {
                var to = _messageSetting.ReportsReceiver_PhoneNumber.HasValue() ? _messageSetting.ReportsReceiver_PhoneNumber : reportReceiverPhoneNumber;
                var sms = new QueuedMessage()
                {
                    To = to,
                    Text = $"کاربر {userName} درخواست احراز هویت دارد.",
                    Type = QueuedMessageType.SMS
                };
                await _queuedMessageService.InsertAsync(sms);
            }
            #endregion
            #endregion
        }
        public async Task UserVerifyRespondAsync(string emailBody, string userName, string email, string phoneNumber, string description, bool accepted)
        {
            var acceptedMessage = accepted ? "تبریک! درخواست احراز هویت شما تایید شده است." : "متاسفانه درخواست احراز هویت شما تایید نشد.";
            #region Email To User
            if (_messageSetting.Email_User_UserVerifyRespond && email.HasValue())
            {
                var emailAccountId = _messageSetting.Email_UserVerifyRespond_EmailAccountId != 0 ? _messageSetting.Email_UserVerifyRespond_EmailAccountId : new int?();
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"کاربر ایکس تومن {userName} {acceptedMessage}",
                    To = email,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
            #endregion

            #region SMS To User

            if (_messageSetting.SMS_User_UserVerifyRespond && phoneNumber.HasValue())
            {
                var to = phoneNumber.Replace("-", "");
                await _farazsmsService.VerifyIdentityAsync(to, userName, accepted, description);
            }
            #endregion
        }
        public async Task SupportTicketSubmitedAsync(string emailBody, string email, string phoneNumber, string subject, string text, string ticketUrl)
        {
            #region Telegram
            var userName = email.HasValue() ? email : phoneNumber;
            if (_messageSetting.Telegram_Admin_SupportTicketSubmit)
            {
                var message = $"کاربر {userName} درخواست پشتیبانی دارد.";
                var channelId = _messageSetting.Telegram_Admin_SupportTicketSubmit_ChannelId.HasValue() ? _messageSetting.Telegram_Admin_SupportTicketSubmit_ChannelId : reportChannelId;
                var result = await _telegramService.SendMessageAsync(message, channelId).ConfigureAwait(false);
                if (!result.ok)
                    new Exception($"Telegram: User submit ticket message for user {userName} send failed").LogError();
            }
            #endregion

            #region Email
            var emailAccountId = _messageSetting.Email_SupportTicketSubmit_EmailAccountId != 0 ? _messageSetting.Email_SupportTicketSubmit_EmailAccountId : new int?();
            #region To User
            if (_messageSetting.Email_User_SupportTicketSubmit && email.HasValue() && email.IsEmail())
            {
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"کاربر ایکس تومن {userName} درخواست پشتیبانی شما ثبت گردید.",
                    To = email,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
            #endregion
            #region To Admin
            if (_messageSetting.Email_Admin_SupportTicketSubmit)
            {
                emailBody = $"کاربر {userName} درخواست پشتیبانی دارد. موضوع: {subject}, متن درخواست: {text}";
                var to = _messageSetting.ReportsReceiver_EmailAccount.HasValue() ? _messageSetting.ReportsReceiver_EmailAccount : reportReceiverEmail;
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"کاربر {userName} درخواست پشتیبانی دارد.",
                    To = to,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
            #endregion
            #endregion

            #region SMS
            #region To User
            if (_messageSetting.SMS_User_SupportTicketSubmit && phoneNumber.HasValue() && phoneNumber.IsMobile())
            {
                var to = phoneNumber.Replace("-", "");
                await _farazsmsService.TicketSubmitedAsync(to, userName);
            }
            #endregion
            #region To Admin
            if (_messageSetting.SMS_Admin_SupportTicketSubmit)
            {
                var to = _messageSetting.ReportsReceiver_PhoneNumber.HasValue() ? _messageSetting.ReportsReceiver_PhoneNumber : reportReceiverPhoneNumber;
                var sms = new QueuedMessage()
                {
                    To = to,
                    Text = $"کاربر {userName} درخواست پشتیبانی دارد.",
                    Type = QueuedMessageType.SMS
                };
                await _queuedMessageService.InsertAsync(sms);
            }
            #endregion
            #endregion
        }
        public async Task ForgotPasswordAsync(string emailBody, string emailOrPhone, string userName, string fullName, string confirmUrl, string code)
        {
            #region Telegram
            if (_messageSetting.Telegram_Admin_ForgotPassword)
            {
                var message = $"کاربر {userName} کلمه عبور خود را فراموش کرده است.";
                var channelId = _messageSetting.Telegram_Admin_ForgotPassword_ChannelId.HasValue() ? _messageSetting.Telegram_Admin_ForgotPassword_ChannelId : reportChannelId;
                var result = await _telegramService.SendMessageAsync(message, channelId).ConfigureAwait(false);
                if (!result.ok)
                    new Exception($"Telegram: Forgot password message for {userName} to admin send failed").LogError();
            }
            #endregion

            #region Email
            var emailAccountId = _messageSetting.Email_ForgotPassword_EmailAccountId != 0 ? _messageSetting.Email_ForgotPassword_EmailAccountId : new int?();
            #region To User
            if (_messageSetting.Email_User_ForgotPassword && emailOrPhone.IsEmail())
            {
                var subject = $"کاربر ایکس تومن {userName} کلمه عبور خود را فراموش کرده اید؟";
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = subject,
                    To = emailOrPhone,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                _queuedMessageService.Insert(queuedMessage);

                #region Test
                //var fpMailMesssage = PrepareMailMessage(emailOrPhone, subject, emailBody);
                //var fpEmailResult = await _smarterMailService.SendEmailAsync(fpMailMesssage);

                //if (fpEmailResult == null)
                //    new Exception("Email failed: خطا").LogError();

                //else if (!fpEmailResult.Success)
                //    new Exception("Email failed: " + fpEmailResult.Message).LogError();
                #endregion

            }
            #endregion
            #endregion

            #region SMS
            #region To User
            if (_messageSetting.SMS_User_ForgotPassword && emailOrPhone.IsMobile())
            {
                var to = emailOrPhone.Replace("-", "");
                await _farazsmsService.ForgotPasswordCodeAsync(to, userName, code);
            }
            #endregion
            #endregion
        }

        public async Task<string> TomanPaidAsync(string phoneNumber, string orderGuid, string transactionId = null, string payType = null, string status = null)
        {
            var result = "";
            if (!payType.HasValue() && !status.HasValue())
            {
                result = await _farazsmsService.TomanCardToCardPaidAsync(phoneNumber, orderGuid, transactionId);
            }
            else
            {
                if (status.HasValue())
                {
                    if (!payType.HasValue())
                        payType = "پایا یا ساتنا";
                    result = await _farazsmsService.TomanIBANPaidAsync(phoneNumber, orderGuid, payType, status);
                }
            }
            return result;
        }

        public async Task EmailChangedAsync(string emailBody, string fullName, string email)
        {
            if (_messageSetting.Email_User_EmailChanged && email.HasValue() && email.IsEmail())
            {
                var emailAccountId = _messageSetting.Email_EmailChanged_EmailAccountId != 0 ? _messageSetting.Email_EmailChanged_EmailAccountId : new int?();
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"کاربر ایکس تومن، {fullName} ایمیل شما تغییر یافته است.",
                    To = email,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
        }

        public async Task EmailAddedAsync(string emailBody, string fullName, string email)
        {
            if (_messageSetting.Email_User_EmailChanged && email.HasValue() && email.IsEmail())
            {
                var emailAccountId = _messageSetting.Email_EmailChanged_EmailAccountId != 0 ? _messageSetting.Email_EmailChanged_EmailAccountId : new int?();
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"کاربر ایکس تومن، {fullName} ایمیل شما ثبت شد. ایمیل خود را تایید نمایید.",
                    To = email,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
        }

        public async Task PhoneNumberChangedAsync(string phoneNumber, string phoneCode, string phoneConfirmUrl)
        {
            if (_messageSetting.SMS_User_PhoneNumberChanged && phoneNumber.HasValue() && phoneNumber.IsMobile())
            {
                var to = phoneNumber.Replace("-", "");
                await _farazsmsService.VerifyPhoneAsync(to, phoneCode, false);
            }
        }

        public async Task PhoneNumberConfirmSMSAsync(string code, string phoneNumber)
        {
            await _farazsmsService.VerifyPhoneJustCodeAsync(phoneNumber, code);
        }

        public async Task PhoneNumberAddedAsync(string phoneNumber, string phoneCode, string phoneConfirmUrl)
        {
            if (_messageSetting.SMS_User_PhoneNumberChanged && phoneNumber.HasValue() && phoneNumber.IsMobile())
            {
                var to = phoneNumber.Replace("-", "");
                await _farazsmsService.VerifyPhoneAsync(to, phoneCode);
            }
        }

        public async Task NewOrderSubmitedAsync(string emailBody, string email, string phoneNumber, string userName, string orderGuid, string fromCurrencyName, string toCurrencyName, string payAmount, string receiveAmount)
        {
            #region Telegram
            if (_messageSetting.Telegram_Admin_NewOrder)
            {
                var message = $"سفارش جدید ثبت شد.\n{fromCurrencyName} به {toCurrencyName}\nقابل پرداخت برای مشتری:{payAmount}\nمقدار دریافتی مشتری:{receiveAmount}";
                var channelId = _messageSetting.Telegram_Admin_NewOrder_ChannelId.HasValue() ? _messageSetting.Telegram_Admin_NewOrder_ChannelId : reportChannelId;
                var result = await _telegramService.SendMessageAsync(message, channelId).ConfigureAwait(false);
                if (!result.ok)
                    new Exception($"Telegram: New order message for user {userName} send failed").LogError();
            }
            #endregion

            #region Email
            var emailAccountId = _messageSetting.Email_NewOrder_EmailAccountId != 0 ? _messageSetting.Email_NewOrder_EmailAccountId : new int?();
            #region To User
            if (_messageSetting.Email_User_NewOrder && email.HasValue() && email.IsEmail())
            {
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"کاربر ایکس تومن، سفارش مبادله {fromCurrencyName} به {toCurrencyName} با کد پیگیری {orderGuid} ثبت گردید.",
                    To = email,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
            #endregion
            #region To Admin
            if (_messageSetting.Email_Admin_NewOrder)
            {
                emailBody = $"سفارش جدید به شماره {orderGuid} با مبلغ قابل پرداخت {payAmount} و مقدار دریافتی {receiveAmount} توسط کاربر {userName} ثبت گردید.";
                var subject = $"کاربر {userName} سفارش جدید ثبت کرد. کد سفارش {orderGuid}";
                var to = _messageSetting.ReportsReceiver_EmailAccount.HasValue() ? _messageSetting.ReportsReceiver_EmailAccount : reportReceiverEmail;
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = subject,
                    To = to,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
            #endregion
            #endregion

            #region SMS
            #region To User
            if (_messageSetting.SMS_User_NewOrder && phoneNumber.HasValue() && phoneNumber.IsMobile())
            {
                var to = phoneNumber.Replace("-", "");
                await _farazsmsService.OrderAsync(to, userName, orderGuid);
            }
            #endregion
            #region To Admin
            if (_messageSetting.SMS_Admin_NewOrder)
            {
                var to = _messageSetting.ReportsReceiver_PhoneNumber.HasValue() ? _messageSetting.ReportsReceiver_PhoneNumber : reportReceiverPhoneNumber;
                var sms = new QueuedMessage()
                {
                    To = to,
                    Text = $"سفارش جدید به شماره {orderGuid} با مبلغ قابل پرداخت {payAmount} و مقدار دریافتی {receiveAmount} توسط کاربر {userName} ثبت گردید.",
                    Type = QueuedMessageType.SMS
                };
                await _queuedMessageService.InsertAsync(sms);
            }
            #endregion
            #endregion
        }

        public async Task OrderPaidByFiatUSDAsync(string emailBody, string email, string phoneNumber, string userName, long orderId, string orderGuid, decimal paidAmount)
        {
            #region Telegram          
            if (_messageSetting.Telegram_Admin_NewOrder)
            {
                var telegramText = $"سفارش کاربر {userName} به شماره {orderId}, به مبلغ {paidAmount.ToPriceWithoutFloat()} دلار پرداخت شد.";
                var channelId = _messageSetting.Telegram_Admin_NewOrder_ChannelId.HasValue() ? _messageSetting.Telegram_Admin_NewOrder_ChannelId : reportChannelId;
                var result = await _telegramService.SendMessageAsync(telegramText, channelId).ConfigureAwait(false);
                if (!result.ok)
                    new Exception($"Telegram: Order paid message for user {userName} send failed").LogError();
            }
            #endregion

            #region Email
            var emailAccountId = _messageSetting.Email_NewOrder_EmailAccountId != 0 ? _messageSetting.Email_NewOrder_EmailAccountId : new int?();
            #region To User
            if (_messageSetting.Email_User_NewOrder && email.HasValue() && email.IsEmail())
            {
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"کاربر ایکس تومن، سفارش مبادله به شماره {orderGuid} توسط شما پرداخت شد.",
                    To = email,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
            #endregion
            #region To Admin
            if (_messageSetting.Email_Admin_NewOrder)
            {
                emailBody = $"سفارش {orderGuid} پرداخت شد. {paidAmount.ToPriceWithoutFloat()} دلار.";
                var subject = $"کاربر {userName} پرداخت سفارش را انجام داد. کد سفارش {orderGuid}";
                var to = _messageSetting.ReportsReceiver_EmailAccount.HasValue() ? _messageSetting.ReportsReceiver_EmailAccount : reportReceiverEmail;
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = subject,
                    To = to,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
            #endregion
            #endregion

            #region SMS
            #region To User
            if (_messageSetting.SMS_User_NewOrder && phoneNumber.HasValue() && phoneNumber.IsMobile())
            {
                var to = phoneNumber.Replace("-", "");
                await _farazsmsService.OrderAsync(to, userName, orderGuid, false);
            }
            #endregion
            #endregion
        }

        public async Task OrderPaidByTomanAsync(string emailBody, string email, string phoneNumber, string userName, long orderId, string orderGuid, decimal paidAmount)
        {
            #region Telegram          
            if (_messageSetting.Telegram_Admin_NewOrder)
            {
                var telegramText = $"سفارش کاربر {userName} به شماره {orderId}, به مبلغ {paidAmount.ToPriceWithoutFloat()} تومان پرداخت شد.";
                var channelId = _messageSetting.Telegram_Admin_NewOrder_ChannelId.HasValue() ? _messageSetting.Telegram_Admin_NewOrder_ChannelId : reportChannelId;
                var result = await _telegramService.SendMessageAsync(telegramText, channelId).ConfigureAwait(false);
                if (!result.ok)
                    new Exception($"Telegram: Order paid message for user {userName} send failed").LogError();
            }
            #endregion

            #region Email
            var emailAccountId = _messageSetting.Email_NewOrder_EmailAccountId != 0 ? _messageSetting.Email_NewOrder_EmailAccountId : new int?();
            #region To User
            if (_messageSetting.Email_User_NewOrder && email.HasValue() && email.IsEmail())
            {
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"کاربر ایکس تومن، سفارش مبادله به شماره {orderGuid} توسط شما پرداخت شد.",
                    To = email,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
            #endregion
            #region To Admin
            if (_messageSetting.Email_Admin_NewOrder)
            {
                emailBody = $"سفارش {orderGuid} پرداخت شد. {paidAmount.ToPriceWithoutFloat()} تومان.";
                var subject = $"کاربر {userName} پرداخت سفارش را انجام داد. کد سفارش {orderGuid}";
                var to = _messageSetting.ReportsReceiver_EmailAccount.HasValue() ? _messageSetting.ReportsReceiver_EmailAccount : reportReceiverEmail;
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = subject,
                    To = to,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
            #endregion
            #endregion

            #region SMS
            #region To User
            if (_messageSetting.SMS_User_NewOrder && phoneNumber.HasValue() && phoneNumber.IsMobile())
            {
                var to = phoneNumber.Replace("-", "");
                await _farazsmsService.OrderAsync(to, userName, orderGuid, false);
            }
            #endregion
            #endregion
        }

        public async Task OrderPaidAsync(string telegramText, string receiveAddress, string memo)
        {
            await _telegramService.SendMessageAsync(telegramText, reportChannelId).ConfigureAwait(false);
            //await _telegramService.SendMessageAsync(receiveAddress, reportChannelId).ConfigureAwait(false);
            //if (memo.HasValue())
            //    await _telegramService.SendMessageAsync(memo, reportChannelId).ConfigureAwait(false);
        }

        public async Task SupportTicketAnsweredAsync(string emailBody, string ticketSubject, string ticketUrl, string emailOrPhoneNumber, string answerUser)
        {
            #region Telegram
            if (_messageSetting.Telegram_Admin_SupportTicketSubmit)
            {
                var message = $"درخواست پشتیبانی {emailOrPhoneNumber} توسط {answerUser} پاسخ داده شد.";
                var channelId = _messageSetting.Telegram_Admin_SupportTicketSubmit_ChannelId.HasValue() ? _messageSetting.Telegram_Admin_SupportTicketSubmit_ChannelId : reportChannelId;
                var result = await _telegramService.SendMessageAsync(message, channelId).ConfigureAwait(false);
                if (!result.ok)
                    new Exception($"Telegram: Submit ticket answer message for user {emailOrPhoneNumber} by {answerUser} send failed").LogError();
            }
            #endregion

            #region Email
            var emailAccountId = _messageSetting.Email_SupportTicketSubmit_EmailAccountId != 0 ? _messageSetting.Email_SupportTicketSubmit_EmailAccountId : new int?();
            #region To User
            if (_messageSetting.Email_User_SupportTicketSubmit && emailOrPhoneNumber.HasValue() && emailOrPhoneNumber.IsEmail())
            {
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"کاربر ایکس تومن {emailOrPhoneNumber} درخواست پشتیبانی شما با موضوع {ticketSubject} پاسخ داده شد.",
                    To = emailOrPhoneNumber,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
            #endregion
            #endregion

            #region SMS
            #region To User
            if (_messageSetting.SMS_User_SupportTicketSubmit && emailOrPhoneNumber.HasValue() && emailOrPhoneNumber.IsMobile())
            {
                var to = emailOrPhoneNumber.Replace("-", "");
                await _farazsmsService.TicketAnsweredAsync(to, emailOrPhoneNumber, ticketSubject);
            }
            #endregion
            #endregion
        }

        public async Task CoinPaymentsPaidAsync(string emailBody, string email, string phoneNumber, string fullName, string orderGuid, string receivedAmount)
        {
            if (email.HasValue() && email.IsEmail())
            {
                var queuedMessage = new QueuedMessage()
                {
                    Subject = $"کاربر ایکس تومن {fullName} پرداخت {receivedAmount} شما به ایکس تومن با موفقیت انجام شد.",
                    To = email,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }

            if (phoneNumber.HasValue() && phoneNumber.IsMobile())
            {
                var to = phoneNumber.Replace("-", "");
                await _farazsmsService.OrderAsync(to, fullName, orderGuid, false);
            }

            await _telegramService.SendMessageAsync($"پرداخت کوین برای سفارش کاربر {fullName} با مقدار {receivedAmount} به ایکس تومن انجام شد.", reportChannelId).ConfigureAwait(false);
        }

        public async Task TelegramAdminMessageAsync(string message)
        {
            await _telegramService.SendMessageAsync(message, reportChannelId).ConfigureAwait(false);
        }

        public async Task EmailConfirmAsync(string confirmEmailBody, string email, string fullName)
        {
            if (_messageSetting.Email_User_EmailChanged && email.HasValue())
            {
                var emailAccountId = _messageSetting.Email_EmailChanged_EmailAccountId != 0 ? _messageSetting.Email_EmailChanged_EmailAccountId : new int?();
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"کاربر ایکس تومن، {fullName} ایمیل خود را تایید نمایید.",
                    To = email,
                    Type = QueuedMessageType.Email,
                    Text = confirmEmailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
        }

        public async Task TelephoneVerifiedAsync(string emailBody, string fullName, string telephone, string phoneNumber, string email, string description, bool accepted)
        {
            var acceptedMessage = accepted ? $"تلفن ثابت شما {telephone} تایید شده است." : $"متاسفانه تلفن ثابت شما {telephone} تایید نشد.";
            #region Email To User
            if (_messageSetting.Email_User_UserVerifyRespond && email.HasValue())
            {
                var emailAccountId = _messageSetting.Email_UserVerifyRespond_EmailAccountId != 0 ? _messageSetting.Email_UserVerifyRespond_EmailAccountId : new int?();
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"کاربر ایکس تومن {fullName} {acceptedMessage}",
                    To = email,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
            #endregion

            #region SMS To User
            if (_messageSetting.SMS_User_UserVerifyRespond && phoneNumber.HasValue())
            {
                var to = phoneNumber.Replace("-", "");
                await _farazsmsService.VerifyTelephoneAsync(phoneNumber, telephone, accepted);
            }
            #endregion
        }

        public async Task BankCardVerifiedAsync(string emailBody, string fullName, string cardNumber, string phoneNumber, string email, string description, bool accepted)
        {
            var acceptedMessage = accepted ? $"کارت بانکی شما {cardNumber} تایید شده است." : $"متاسفانه کارت بانکی {cardNumber} تایید نشد.";
            #region Email To User
            if (_messageSetting.Email_User_UserVerifyRespond && email.HasValue())
            {
                var emailAccountId = _messageSetting.Email_UserVerifyRespond_EmailAccountId != 0 ? _messageSetting.Email_UserVerifyRespond_EmailAccountId : new int?();
                var queuedMessage = new QueuedMessage()
                {
                    EmailAccountId = emailAccountId,
                    Subject = $"کاربر ایکس تومن {fullName} {acceptedMessage}",
                    To = email,
                    Type = QueuedMessageType.Email,
                    Text = emailBody
                };
                await _queuedMessageService.InsertAsync(queuedMessage);
            }
            #endregion

            #region SMS To User
            if (_messageSetting.SMS_User_UserVerifyRespond && phoneNumber.HasValue())
            {
                var to = phoneNumber.Replace("-", "");
                await _farazsmsService.VerifyCardAsync(phoneNumber, cardNumber, accepted);
            }
            #endregion
        }
        #endregion

        #region Helpers
        private MailMessage PrepareMailMessage(string emailOrPhone, string subject, string emailBody)
        {
            return new MailMessage("no-reply@extoman.co", emailOrPhone, subject, emailBody)
            {
                IsBodyHtml = true
            };
        }
        #endregion
    }
}
