﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Utility;

namespace Xtoman.Service
{
    public class OrderManager : IOrderManager
    {
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly ITradeManager _tradeManager;
        private readonly IMessageManager _messageManager;
        private readonly IWithdrawManager _withdrawManager;

        public OrderManager(IExchangeOrderService exchangeOrderService,
            ITradeManager tradeManager,
            IMessageManager messageManager,
            IWithdrawManager withdrawManager)
        {
            _exchangeOrderService = exchangeOrderService;
            _tradeManager = tradeManager;
            _messageManager = messageManager;
            _withdrawManager = withdrawManager;
        }

        public async Task CompleteFiatTomanToFiatUSDOrderAsync(ExchangeOrder order)
        {
            try
            {
                order.OrderStatus = OrderStatus.Processing;
                order.Errors = null;
                await _exchangeOrderService.UpdateAsync(order);
                var (withdrawSuccess, withdrawMessage) = await _withdrawManager.WithdrawAsync(order);
                await _messageManager.TelegramAdminMessageAsync(withdrawMessage);
                if (withdrawSuccess)
                    order.OrderStatus = OrderStatus.Complete;
                else
                    order.OrderStatus = OrderStatus.WithdrawFailed;
                await _exchangeOrderService.UpdateAsync(order);
            }
            catch (Exception ex)
            {
                ex.LogError();
                await _messageManager.TelegramAdminMessageAsync($"خطا هنگام تکمیل مراحل سفارش خرید پرفکت با شتاب | {ex.Message}");
                order.OrderStatus = OrderStatus.InComplete;
            }
        }

        public async Task CompleteFiatToCoinOrderAsync(ExchangeOrder order)
        {
            try
            {
                order.OrderStatus = OrderStatus.Processing;
                order.Errors = null;
                await _exchangeOrderService.UpdateAsync(order);
                var trades = new List<Trade>();
                var binanceTradeResult = false;
                //Trade on binance and kraken while success by 3 retry
                var tradeRetryCount = 0;
                while (!binanceTradeResult && tradeRetryCount < 3)
                {
                    tradeRetryCount++;
                    var tradeResult = await _tradeManager.BuyCryptoTradeOrderAsync(order);
                    binanceTradeResult = tradeResult.BinanceResult;
                    trades = tradeResult.Trades;
                }
                var binanceTrade = trades.Where(p => p.TradingPlatform == TradingPlatform.Binance).FirstOrDefault();
                if (binanceTradeResult)
                {
                    if (binanceTrade != null)
                    {
                        var binanceTelegramMessage = $"تبدیل {binanceTrade.FromCryptoType.ToDisplay()} با مقدار {binanceTrade.FromAmount.ToBlockChainPrice()} به {binanceTrade.ToCryptoType.ToDisplay()} با مقدار {binanceTrade.ToAmount.ToBlockChainPrice()} انجام شد.";
                        await _messageManager.TelegramAdminMessageAsync(binanceTelegramMessage);
                    }
                    await Task.Delay(1000);
                    var (withdrawSuccess, withdrawMessage) = await _withdrawManager.WithdrawAsync(order);
                    if (withdrawSuccess)
                    {
                        order.OrderStatus = OrderStatus.Complete;
                        await _exchangeOrderService.UpdateAsync(order);
                        await _messageManager.TelegramAdminMessageAsync(withdrawMessage);
                    }
                    else
                    {
                        var errorMessage = $"خطا در هنگام برداشت از Binance برای سفارش شماره {order.Id}. Error: {withdrawMessage}";
                        await _messageManager.TelegramAdminMessageAsync(errorMessage);
                        if (withdrawMessage.Contains("Address verification failed"))
                            order.OrderStatus = OrderStatus.WithdrawFailed;

                        await _exchangeOrderService.UpdateAsync(order);
                    }
                }
                if (order.Errors.HasValue())
                {
                    await _messageManager.TelegramAdminMessageAsync(order.Errors);
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
                await _messageManager.TelegramAdminMessageAsync($"خطا هنگام تکمیل مراحل سفارش خرید کوین با شتاب | {ex.Message}");
                order.OrderStatus = OrderStatus.InComplete;
            }
        }
    }
}
