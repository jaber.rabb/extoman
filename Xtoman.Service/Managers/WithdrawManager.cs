﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.Payment.CoinPayments;
using Xtoman.Service.WebServices;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service
{
    public class WithdrawManager : IWithdrawManager
    {
        private readonly IBinanceService _binanceService;
        private readonly IWithdrawService _withdrawService;
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly IPerfectMoneyService _perfectMoneyService;
        private readonly IKrakenService _krakenService;
        private readonly ILocalBitcoinsService _localBitcoinsService;
        private readonly ICoinPaymentsService _coinPaymentsService;
        private readonly IVandarV2Service _vandarV2Service;
        private readonly IPayIRService _payIRService;
        private readonly IFinnotechService _finnotechService;
        private readonly ICacheManager _cacheManager;
        public WithdrawManager(
            IBinanceService binanceService,
            IWithdrawService withdrawService,
            IExchangeOrderService exchangeOrderService,
            IPerfectMoneyService perfectMoneyService,
            IKrakenService krakenService,
            ILocalBitcoinsService localBitcoinsService,
            ICoinPaymentsService coinPaymentsService,
            IVandarV2Service vandarV2Service,
            IPayIRService payIRService,
            IFinnotechService finnotechService,
            ICacheManager cacheManager)
        {
            _binanceService = binanceService;
            _withdrawService = withdrawService;
            _exchangeOrderService = exchangeOrderService;
            _perfectMoneyService = perfectMoneyService;
            _krakenService = krakenService;
            _localBitcoinsService = localBitcoinsService;
            _coinPaymentsService = coinPaymentsService;
            _vandarV2Service = vandarV2Service;
            _payIRService = payIRService;
            _finnotechService = finnotechService;
            _cacheManager = cacheManager;
        }
        public async Task<(bool success, string message)> WithdrawAsync(ExchangeOrder order)
        {
            var withdrawId = "";
            var withdrawSuccess = false;
            var withdrawMessage = "";
            if (order.Exchange.ToCurrency.IsFiat)
            {
                if (!order.OrderWithdraws.Any())
                {
                    try
                    {
                        var roundedPmAmount = decimal.Round(order.ReceiveAmount, 2);
                        var pmAmount = Convert.ToDouble(roundedPmAmount);
                        switch (order.Exchange.ToCurrency.Type)
                        {
                            case ECurrencyType.Shetab:
                                break;
                            case ECurrencyType.PAYEER:
                                break;
                            case ECurrencyType.PerfectMoney:
                                var pmTransferResult = await _perfectMoneyService.TransferAsync(order.ReceiveAddress.ToUpperFirst(), pmAmount, 1, order.Id);
                                if (pmTransferResult != null)
                                {
                                    withdrawSuccess = !pmTransferResult.ERROR.HasValue() && pmTransferResult.PAYMENT_BATCH_NUM.HasValue();
                                    withdrawMessage = withdrawSuccess ? $"برداشت از PerfectMoney و واریز {(order.ReceiveAmount).ToPriceWithoutFloat()} {order.Exchange.ToCurrency.UnitSign} بابت سفارش شماره {order.Id} به آدرس کیف پول {order.ReceiveAddress} انجام شد." : pmTransferResult.ERROR;
                                    withdrawId = withdrawSuccess ? pmTransferResult.PAYMENT_BATCH_NUM : "";
                                    if (withdrawSuccess)
                                    {
                                        var pmWithdraw = new Withdraw()
                                        {
                                            Amount = order.ReceiveAmount,
                                            Status = WithdrawStatus.Completed,
                                            TransactionId = pmTransferResult.PAYMENT_BATCH_NUM,
                                            WithdrawType = WithdrawFromType.ApiWallet,
                                            ECurrencyType = order.Exchange.ToCurrency.Type,
                                            OrderId = order.Id,
                                            Fee = order.GatewayFee,
                                            ApiWithdrawId = pmTransferResult.PAYMENT_ID
                                        };
                                        await _withdrawService.InsertAsync(pmWithdraw);
                                    }
                                }
                                break;
                            case ECurrencyType.PerfectMoneyVoucher:
                                var pmVoucherResult = await _perfectMoneyService.EvoucherPurchaseAsync(pmAmount);
                                if (pmVoucherResult != null)
                                {
                                    withdrawSuccess = !pmVoucherResult.ERROR.HasValue() && pmVoucherResult.VoucherCode.HasValue() && pmVoucherResult.VoucherNumber.HasValue();
                                    withdrawMessage = withdrawSuccess ? "کد ووچر ایجاد شد" : pmVoucherResult.ERROR;
                                    withdrawId = withdrawSuccess ? pmVoucherResult.PaymentBatchNumber : "";
                                    if (withdrawSuccess)
                                    {
                                        order.ReceiveAddress = pmVoucherResult.VoucherNumber;
                                        order.ReceiveMemoTagPaymentId = pmVoucherResult.VoucherCode;
                                        await _exchangeOrderService.UpdateAsync(order);

                                        var pmVWithdraw = new Withdraw()
                                        {
                                            Amount = order.ReceiveAmount,
                                            Status = WithdrawStatus.Completed,
                                            TransactionId = pmVoucherResult.PaymentBatchNumber,
                                            WithdrawType = WithdrawFromType.ApiWallet,
                                            ECurrencyType = order.Exchange.ToCurrency.Type,
                                            OrderId = order.Id,
                                            Fee = order.GatewayFee,
                                            ApiWithdrawId = "Voucher"
                                        };
                                        await _withdrawService.InsertAsync(pmVWithdraw);
                                    }
                                }
                                break;
                            case ECurrencyType.WebMoney:
                                break;
                            default:
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.LogError();
                        withdrawMessage = ex.Message;
                    }
                }
            }
            else
            {
                if (!order.OrderWithdraws.Any())
                {
                    var amount = order.ReceiveAmount + order.GatewayFee;
                    //Check CoinPayments and Withdraw from CoinPayments, If ...
                    //var cpBalance = await _coinPaymentsService.GetBalanceAsync(order.Exchange.ToCurrency.Type);
                    //if (cpBalance >= amount)
                    //{
                    //    amount = order.ReceiveAmount;
                    //    // Withdraw from CoinPayments
                    //    if (!order.OrderWithdraws.Where(p => p.TradingPlatform == TradingPlatform.Binance).Any())
                    //    {
                    //        try
                    //        {
                    //            if (order.Exchange.ExchangeFiatCoinType == ExchangeFiatCoinType.CoinToCoin && order.OrderTrades.Where(p => p.ToCryptoType == order.Exchange.ToCurrency.Type).Any())
                    //                amount = order.OrderTrades.Where(p => p.ToCryptoType == order.Exchange.ToCurrency.Type).Select(p => p.ToAmount).First();

                    //            var withdrawResult = await _coinPaymentsService.CreateWithdrawalAsync(new CreateWithdrawalInput
                    //            {
                    //                address = order.ReceiveAddress,
                    //                amount = amount,
                    //                auto_confirm = 1,
                    //                dest_tag = order.ReceiveMemoTagPaymentId,
                    //                note = "For order number: " + order.Id,

                    //            });
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            order.Errors = ex.Message;
                    //            order.OrderStatus = OrderStatus.WithdrawFailed;
                    //            await _exchangeOrderService.UpdateAsync(order);
                    //            ex.LogError();
                    //            withdrawMessage = ex.Message;
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //Else withdraw from Binance
                    var binanceAsset = order.Exchange.ToCurrency.Type.ToBinanceAsset();
                    if (!order.OrderWithdraws.Where(p => p.TradingPlatform == TradingPlatform.Binance).Any())
                    {
                        try
                        {
                            if (order.Exchange.ExchangeFiatCoinType == ExchangeFiatCoinType.CoinToCoin && order.OrderTrades.Where(p => p.ToCryptoType == order.Exchange.ToCurrency.Type).Any())
                                amount = order.OrderTrades.Where(p => p.ToCryptoType == order.Exchange.ToCurrency.Type).Select(p => p.ToAmount).First();

                            var withdrawResult = await _binanceService.WithdrawAsync(binanceAsset, amount, order.ReceiveAddress, order.ReceiveMemoTagPaymentId, order.Id.ToString());
                            withdrawSuccess = withdrawResult != null && withdrawResult.Success;
                            var walletAddressText = order.ReceiveAddress;
                            if (order.ReceiveMemoTagPaymentId.HasValue())
                                walletAddressText += $" و Payment Tag Id: {order.ReceiveMemoTagPaymentId}";
                            withdrawMessage = withdrawSuccess ? $"برداشت از Binance و واریز {(amount).ToBlockChainPrice()} {order.Exchange.ToCurrency.UnitSign} بابت سفارش شماره {order.Id} به آدرس کیف پول {walletAddressText} انجام شد." : withdrawResult.Msg;
                            withdrawId = withdrawResult?.Id;
                        }
                        catch (Exception ex)
                        {
                            order.Errors = ex.Message;
                            order.OrderStatus = OrderStatus.WithdrawFailed;
                            await _exchangeOrderService.UpdateAsync(order);
                            ex.LogError();
                            withdrawMessage = ex.Message;
                        }
                    }
                    else
                    {
                        return (true, $"برای سفارش {order.Id} قبلا خروجی از بایننس زده شده.");
                    }
                    if (withdrawSuccess)
                    {
                        var withdraw = new Withdraw()
                        {
                            Amount = amount,
                            ApiWithdrawId = withdrawId,
                            ECurrencyType = order.Exchange.ToCurrency.Type,
                            Fee = order.GatewayFee,
                            Status = WithdrawStatus.Processing,
                            WalletApiType = WalletApiType.Binance,
                            WithdrawType = WithdrawFromType.TradingPlatform,
                            TradingPlatform = TradingPlatform.Binance,
                            OrderId = order.Id
                        };
                        await _withdrawService.InsertAsync(withdraw);
                    }
                }
                //}
                else
                {
                    return (true, $"برای سفارش {order.Id} قبلا خروجی از بایننس زده شده.");
                }
            }
            return (withdrawSuccess, withdrawMessage);
        }

        public WalletAddress GetOurWalletAddress(ECurrencyType currencyType, WalletApiType walletType)
        {
            var cacheKey = $"Xtoman.{walletType.ToDisplay()}.Address.{currencyType.ToDisplay(DisplayProperty.ShortName)}";
            //If cache is not available get, cach and return.
            return _cacheManager.Get(cacheKey, 180, () =>
            {
                var result = new WalletAddress()
                {
                    CurrencyType = currencyType,
                    WalletType = walletType
                };
                switch (walletType)
                {
                    case WalletApiType.LocalBitcoins:
                        // var localResult = await _localBitcoinsService.GetDeposit
                        break;
                    case WalletApiType.Binance:
                        //try
                        //{
                        //    var binanceResult = await _binanceService.GetDepositAddressAsync(currencyType.ToBinanceAsset());
                        //    if (binanceResult != null && binanceResult.Success)
                        //    {
                        //        result.Address = binanceResult.Address;
                        //        result.PayTagId = binanceResult.AddressTag;
                        //    }
                        //}
                        //catch (Exception ex)
                        //{
                        //    ex.LogError();
                        result.Address = DepositAddresses.GetAddressByShortName(currencyType.ToDisplay(DisplayProperty.ShortName));
                        result.PayTagId = DepositAddresses.GetTagByShortName(currencyType.ToDisplay(DisplayProperty.ShortName));
                        //}
                        break;
                    case WalletApiType.Kraken:
                        var krakenDepositMethods = _krakenService.GetDepositMethods(currencyType.ToKrakenAsset());
                        if (krakenDepositMethods.Result != null)
                        {
                            var krakenResult = _krakenService.GetDepositAddress(currencyType.ToKrakenAsset(), krakenDepositMethods.Result.First().Method);
                            if (krakenResult.Result != null && krakenResult.Result.Any())
                            {
                                var krakenDepositAddress = krakenResult.Result.First();
                                result.Address = krakenDepositAddress.Address;
                                result.PayTagId = krakenDepositAddress.Tag;
                            }
                            else
                            {
                                result.Error = "Address not found!";
                            }
                        }
                        else
                        {
                            result.Error = "Deposit method not found!";
                        }
                        break;
                }
                if (result.Address.HasValue())
                    return result;
                throw new Exception("Failed to get address");
            });
        }

        public async Task<(bool status, string text)> WithdrawFromCoinPaymentsToBinanceAsync(ECurrencyType currencyType)
        {
            (bool status, string text) = (false, "");
            try
            {
                await Task.Delay(2000);
                var cpType = currencyType.ToDisplay(DisplayProperty.ShortName).ToCoinPaymentsCurrency();
                if (cpType.HasValue)
                {
                    var ourWallet = GetOurWalletAddress(currencyType, WalletApiType.Binance);
                    if (ourWallet != null && ourWallet.Address.HasValue())
                    {
                        var walletAddress = ourWallet.Address;
                        var walletAddressTag = ourWallet.PayTagId;
                        var amount = await _coinPaymentsService.GetBalanceAsync(currencyType);
                        var coinList = await _coinPaymentsService.ExchangeRatesCoinListAsync();
                        if (coinList?.error == "ok")
                        {
                            var coinItem = coinList.result.Where(p => p.Key.ToUpper() == currencyType.ToCoinPaymentsCurrency()).Select(p => p.Value).FirstOrDefault();
                            amount -= (coinItem.tx_fee + (coinItem.tx_fee / 10));
                        }
                        var result = await _coinPaymentsService.CreateWithdrawalAsync(new CreateWithdrawalInput()
                        {
                            address = walletAddress,
                            amount = amount,
                            auto_confirm = 1,
                            CoinPaymentsCurrency = cpType.Value,
                            dest_tag = walletAddressTag,
                            note = "Withdrawal for Extoman",
                        }, true);
                        var addressText = walletAddressTag.HasValue() ? $"{walletAddress} و آدرس تگ {walletAddressTag}" : walletAddress;
                        if (result.result != null && result.result.statusEnum == CreateWithdrawalStatus.Withdrawal_created_with_no_email_confirmation)
                        {
                            status = true;
                            text = $"انتقال {amount.ToBlockChainPrice()} {currencyType.ToDisplay(DisplayProperty.ShortName)} از کوین پیمنت به بایننس به آدرس {addressText} ثبت گردید.";
                        }
                        else
                        {
                            var amountAddressText = $"مقدار: {amount.ToBlockChainPrice()}, آدرس {addressText}";
                            text = result.error.HasValue() ? result.error + " " + amountAddressText : "خطای نا مشخص " + amountAddressText + " " + result.Serialize();
                        }
                    }
                    else
                    {
                        text = "خطا هنگام دریافت آدرس";
                    }
                }
                else
                {
                    text = "CoinPayments CurrencyType has no value or not found!";
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
                text = "خطا هنگام برداشت از کوین پیمنت " + ex.Message;
            }
            return (status, text);
        }

        public async Task<(bool status, string text)> WithdrawFromCoinPaymentsForOrderAsync(ExchangeOrder order)
        {
            (bool status, string text) = (false, "");
            try
            {
                var currencyType = order.Exchange.ToCurrency.Type;
                var cpType = currencyType.ToDisplay(DisplayProperty.ShortName).ToCoinPaymentsCurrency();
                if (cpType.HasValue)
                {
                    var amount = order.ReceiveAmount + order.GatewayFee;
                    var coinList = await _coinPaymentsService.ExchangeRatesCoinListAsync();
                    if (coinList?.error == "ok")
                    {
                        var coinItem = coinList.result.Where(p => p.Key.ToUpper() == currencyType.ToCoinPaymentsCurrency()).Select(p => p.Value).FirstOrDefault();
                        amount -= coinItem.tx_fee;
                    }
                    var result = await _coinPaymentsService.CreateWithdrawalAsync(new CreateWithdrawalInput()
                    {
                        address = order.ReceiveAddress,
                        amount = amount,
                        auto_confirm = 1,
                        CoinPaymentsCurrency = cpType.Value,
                        dest_tag = order.ReceiveMemoTagPaymentId,
                        note = "Withdrawal for Extoman",
                    }, false);
                    if (result.result != null && result.result.statusEnum == CreateWithdrawalStatus.Withdrawal_created_with_no_email_confirmation)
                    {
                        status = true;
                        text = "درخواست انتقال با موفقیت ثبت شد.";
                    }
                    else
                    {
                        text = result.error.HasValue() ? $"CoinPayments withdraw for order:{order.Id}, Currency: {cpType.Value.ToDisplay()}, Amount: {amount}, {result.error}" : $"خطای نا مشخص هنگام برداشت از CoinPayments برای سفارش {order.Id}" + result.Serialize();
                    }
                }
                else
                {
                    text = "CoinPayments CurrencyType has no value!";
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
                text = ex.Message;
            }
            return (status, text);
        }

        public async Task<(bool status, string text)> WithdrawCoinPaymentsToWalletAddressAsync(ECurrencyType currencyType, string address, string tag)
        {
            (bool status, string text) = (false, "");
            try
            {
                var cpType = currencyType.ToDisplay(DisplayProperty.ShortName).ToCoinPaymentsCurrency();
                if (cpType.HasValue)
                {
                    var amount = await _coinPaymentsService.GetBalanceAsync(currencyType);
                    var coinList = await _coinPaymentsService.ExchangeRatesCoinListAsync();
                    if (coinList?.error == "ok")
                    {
                        var coinItem = coinList.result.Where(p => p.Key.ToUpper() == currencyType.ToCoinPaymentsCurrency()).Select(p => p.Value).FirstOrDefault();
                        amount -= (coinItem.tx_fee + (coinItem.tx_fee / 10));
                    }
                    var result = await _coinPaymentsService.CreateWithdrawalAsync(new CreateWithdrawalInput()
                    {
                        address = address,
                        amount = amount,
                        auto_confirm = 1,
                        CoinPaymentsCurrency = cpType.Value,
                        dest_tag = tag,
                        note = "Withdrawal for Extoman",
                    }, true);
                    if (result.result != null && result.result.statusEnum == CreateWithdrawalStatus.Withdrawal_created_with_no_email_confirmation)
                    {
                        status = true;
                        text = "درخواست انتقال با موفقیت ثبت شد.";
                    }
                    else
                    {
                        text = result.error.HasValue() ? $"Currency: {cpType.Value.ToDisplay()}, Amount: {amount}, {result.error}" : "خطای نا مشخص " + result.Serialize();
                    }
                }
                else
                {
                    text = "CoinPayments CurrencyType has no value!";
                }
            }
            catch (Exception ex)
            {
                ex.LogError();
                text = ex.Message;
            }
            return (status, text);
        }

        public async Task<(bool status, string text)> WithdrawFromPayIRForOrderAsync(ExchangeOrder order)
        {
            (bool status, string text) = (false, "");
            try
            {
                var payIrAvailableBalance = await _payIRService.CashoutBalanceAsync();
                if (payIrAvailableBalance?.Data.CashoutableAmountToman >= (order.ReceiveAmount + order.Exchange.ToCurrency.TransactionFee ?? 0))
                {
                    order.ReceiveMemoTagPaymentId = order.ReceiveMemoTagPaymentId.ToUpper();
                    if (order.ReceiveMemoTagPaymentId.StartsWith("IR"))
                    {
                        var partMaxAmount = 50000000 - (order.Exchange.ToCurrency.TransactionFee ?? 0);
                        var parts = 1;
                        if (order.ReceiveAmount > partMaxAmount)
                            parts = Math.Truncate((order.ReceiveAmount / partMaxAmount) + 1).ToInt();

                        if (!order.OrderWithdraws.Any(x => x.Status.HasFlag(WithdrawStatus.Completed | WithdrawStatus.Processing | WithdrawStatus.Voided)))
                        {
                            var fullName = order.User.FirstName + " " + order.User.LastName;
                            var ibanBank = "";

                            var paySheba = await _payIRService.GetShebaDetailAsync(order.ReceiveMemoTagPaymentId);
                            var shebaIsOk = paySheba.Status == 1 && paySheba.Data != null && paySheba.Data.Status == "ACTIVE";

                            if (shebaIsOk)
                            {
                                fullName = paySheba.Data.Name;
                                ibanBank = paySheba.Data.Bank;
                            }
                            else
                            {
                                var userBankCard = order.ReceiveAddress.DigitOnly();
                                var finnotechCardToSheba = await _finnotechService.GetIBANbyCardNumberAsync(userBankCard);
                                if (finnotechCardToSheba.Result != null && finnotechCardToSheba.Result.Iban.HasValue())
                                {
                                    fullName = finnotechCardToSheba.Result.DepositOwners.Select(p => p.FirstName + " " + p.LastName).First();
                                    ibanBank = finnotechCardToSheba.Result.BankName;
                                    order.ReceiveMemoTagPaymentId = finnotechCardToSheba.Result.Iban;
                                    await _exchangeOrderService.UpdateAsync(order);
                                    shebaIsOk = true;
                                }
                            }

                            if (shebaIsOk)
                            {
                                var fee = order.ReceiveAmount * 1 / 100;
                                if (fee > 4000)
                                    fee = 4000;

                                if (parts == 1)
                                {
                                    (status, text) = await CashoutPayIrAsync(order, order.ReceiveAmount, fullName, ibanBank, fee);
                                }
                                else
                                {
                                    var partsDic = new Dictionary<bool, string>();
                                    var totalReceiveAmount = order.ReceiveAmount;
                                    for (int i = 1; i <= parts; i++)
                                    {
                                        var receiveAmount = i == parts ? totalReceiveAmount - ((parts - 1) * partMaxAmount) : partMaxAmount;
                                        var (partStatus, partText) = await CashoutPayIrAsync(order, receiveAmount, fullName, ibanBank, fee, i);
                                        partsDic.Add(partStatus, partText);
                                    }
                                    status = partsDic.All(p => p.Key);
                                    text = status ? $"درخواست تسویه به شماره شبا {order.ReceiveMemoTagPaymentId} {(ibanBank.HasValue() ? ("بانک " + ibanBank) : "")} به نام {fullName} با مبلغ {order.ReceiveAmount.ToPriceWithoutFloat()} تومان برای سفارش شماره {order.Id} در پی دات آی آر ثبت شد. کارمزد {fee.ToPriceWithoutFloat()} تومان." : "تسویه ناقص";
                                }
                            }
                            else
                            {
                                text = $"درخواست تسویه به شماره شبا {order.ReceiveMemoTagPaymentId} انجام نشد. {paySheba?.Error + " " + string.Join(", ", paySheba?.Messages)}".Trim();
                            }
                        }
                        else
                        {
                            if (order.OrderWithdraws.Count == parts)
                            {
                                if (order.OrderWithdraws.All(x => x.Status == WithdrawStatus.Processing || x.Status == WithdrawStatus.Completed))
                                {
                                    text = "تسویه قبلا انجام شده.";
                                }
                                else
                                {
                                    var paySheba = await _payIRService.GetShebaDetailAsync(order.ReceiveMemoTagPaymentId);
                                    if (paySheba.Status == 1 && paySheba.Data != null && paySheba.Data.Status == "ACTIVE")
                                    {
                                        var fullName = paySheba.Data.Name;
                                        var ibanBank = paySheba.Data.Bank;
                                        var fee = order.ReceiveAmount * 1 / 100;
                                        if (fee > 4000)
                                            fee = 4000;

                                        var dictStaText = new Dictionary<bool, string>();
                                        var withdrawCounter = 0;
                                        foreach (var withdraw in order.OrderWithdraws)
                                        {
                                            withdrawCounter++;
                                            var part = parts == 1 ? 0 : withdrawCounter;
                                            if (withdraw.Status == WithdrawStatus.Failure)
                                            {
                                                var payIRWithdrawResult = await _payIRService.CashoutRequestAsync(((withdraw.Amount) * 10).ToInt(), fullName, order.ReceiveMemoTagPaymentId, (order.Id.ToString() + withdrawCounter).ToInt());
                                                var error = payIRWithdrawResult?.Error ?? "Error: " + payIRWithdrawResult.Error + ". ";
                                                var msg = payIRWithdrawResult?.Messages.Length > 0 ? $"Message: {string.Join(", ", payIRWithdrawResult?.Messages)}" : "";
                                                new Exception($"PayIr Cashout: {error}{msg}").LogError();
                                                if (payIRWithdrawResult.Status == 1)
                                                {
                                                    status = true;
                                                    text = $"درخواست تسویه به شماره شبا {order.ReceiveMemoTagPaymentId} {(ibanBank.HasValue() ? ("بانک " + ibanBank) : "")} به نام {fullName} با مبلغ {withdraw.Amount.ToPriceWithoutFloat()} تومان برای سفارش شماره {order.Id} در پی دات آی آر ثبت شد. کارمزد {fee.ToPriceWithoutFloat()} تومان.";
                                                }
                                                else
                                                {
                                                    text = $"درخواست تسویه مبلغ {withdraw.Amount.ToPriceWithoutFloat()} تومان به شماره شبای {order.ReceiveMemoTagPaymentId} به پی ارسال شد اما با خطای '{(string.Join(", ", payIRWithdrawResult?.Messages) + " " + payIRWithdrawResult?.Error).Trim()}' مواجه شد.";
                                                }

                                                withdraw.ApiWithdrawId = payIRWithdrawResult?.Data?.CashoutId.ToString();
                                                withdraw.WalletApiType = WalletApiType.PayIR;
                                                withdraw.TransactionId = payIRWithdrawResult?.Data?.StatusLabel;
                                                withdraw.ECurrencyType = ECurrencyType.Shetab;
                                                withdraw.OrderId = order.Id;
                                                withdraw.Status = payIRWithdrawResult.Status == 1 ? WithdrawStatus.Processing : WithdrawStatus.Failure;
                                                withdraw.TradingPlatform = TradingPlatform.None;
                                                withdraw.WithdrawType = WithdrawFromType.BankAccount;
                                                withdraw.Fee = fee;

                                                _withdrawService.Context.MarkAsChanged(withdraw);
                                                await _withdrawService.UpdateAsync(withdraw);

                                                order.ReceiveTransactionCode = payIRWithdrawResult?.Data?.CashoutId.ToString();
                                                if (!status)
                                                    order.Errors += $"{string.Join(", ", payIRWithdrawResult?.Messages)} {payIRWithdrawResult?.Error}".Trim();
                                                await _exchangeOrderService.UpdateAsync(order);

                                                dictStaText.Add(status, text);
                                            }
                                        }
                                        status = dictStaText.All(p => p.Key);
                                        text = status ? $"درخواست تسویه به شماره شبا {order.ReceiveMemoTagPaymentId} {(ibanBank.HasValue() ? ("بانک " + ibanBank) : "")} به نام {fullName} با مبلغ {order.ReceiveAmount.ToPriceWithoutFloat()} تومان برای سفارش شماره {order.Id} در پی دات آی آر ثبت شد. کارمزد {fee.ToPriceWithoutFloat()} تومان." : "تسویه ناقص";
                                    }
                                }
                            }
                            else
                            {
                                status = false;
                                text = "امکان تسویه کامل وجود ندارد. بخشی از تسویه ممکن است انجام شده باشد.";
                            }
                        }
                        //text = "";
                    }
                    else
                    {
                        text = $"شماره شبا وارد شده {order.ReceiveMemoTagPaymentId} می باشد که اشتباه است.";
                    }
                }
                else
                {
                    text = $"موجودی قابل برداشت پی دات آی آر برای تسویه حساب سفارش شماره {order.Id} کافی نیست";
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("facility:card-to-iban:get"))
                {
                    text = $"شماره شبای وارد شده اشتباه است. ارتباط با فینوتک برای تبدیل شماره کارت به شماره شبا برقرار نشد.";
                }
                else
                {
                    ex.LogError();
                    text = ex.Message;
                    if (ex.InnerException != null)
                        text += " | " + ex.InnerException.Message;
                }
                order.Errors = text;
                await _exchangeOrderService.UpdateAsync(order);
            }

            if (status = false || !await _withdrawService.TableNoTracking.Where(p => p.OrderId == order.Id && p.Status == WithdrawStatus.Completed || p.Status == WithdrawStatus.Processing).AnyAsync())
            {
                order.OrderStatus = OrderStatus.WithdrawFailed;
                await _exchangeOrderService.UpdateAsync(order);
            }
            else
            {
                order.OrderStatus = OrderStatus.Complete;
                await _exchangeOrderService.UpdateAsync(order);
            }

            return (status, text);
        }

        public async Task<(bool status, string text)> WithdrawFromVandarForOrderAsync(ExchangeOrder order)
        {
            (bool status, string text) = (false, "");
            try
            {
                order.ReceiveMemoTagPaymentId = order.ReceiveMemoTagPaymentId.ToUpper();
                if (order.ReceiveMemoTagPaymentId.StartsWith("IR"))
                {
                    order.ReceiveTransactionCode = "";
                    order.OrderStatus = OrderStatus.Complete;
                    await _exchangeOrderService.UpdateAsync(order);

                    var orderIBAN = order.ReceiveMemoTagPaymentId;

                    var vandarIBANs = await _vandarV2Service.GetBusinessIBANsAsync();
                    string ibanId = "";
                    string ibanOwner = "";
                    string ibanBank = "";
                    if (vandarIBANs.Data.Ibans.Any(x => x.IBAN.Equals(order.ReceiveMemoTagPaymentId)))
                    {
                        ibanId = vandarIBANs.Data.Ibans.Where(p => p.IBAN.Equals(order.ReceiveMemoTagPaymentId)).Select(p => p.Id).FirstOrDefault();
                        ibanOwner = vandarIBANs.Data.Ibans.Where(p => p.IBAN.Equals(order.ReceiveMemoTagPaymentId)).Select(p => p.AccountOwner.Select(x => x.FirstName + " " + x.LastName).FirstOrDefault()).FirstOrDefault();
                        ibanBank = vandarIBANs.Data.Ibans.Where(p => p.IBAN.Equals(order.ReceiveMemoTagPaymentId)).Select(p => p.BankName).FirstOrDefault();
                    }
                    else
                    {
                        var addIbanResult = await _vandarV2Service.AddNewIBANForBusinessAsync(order.ReceiveMemoTagPaymentId);
                        if (addIbanResult.Status == 1)
                        {
                            ibanId = addIbanResult.Data.Ibans.Where(p => p.IBAN.Equals(order.ReceiveMemoTagPaymentId)).Select(p => p.Id).FirstOrDefault();
                            ibanOwner = addIbanResult.Data.Ibans.Where(p => p.IBAN.Equals(order.ReceiveMemoTagPaymentId)).Select(p => p.AccountOwner.Select(x => x.FirstName + " " + x.LastName).FirstOrDefault()).FirstOrDefault();
                            ibanBank = addIbanResult.Data.Ibans.Where(p => p.IBAN.Equals(order.ReceiveMemoTagPaymentId)).Select(p => p.BankName).FirstOrDefault();
                        }
                        else
                        {
                            text = $"خطا در هنگام افزودن شماره شبا برای سفارش {order.Id}. {(addIbanResult.Error.HasValue() ? addIbanResult.Error : addIbanResult.Message)}";
                        }
                    }
                    if (ibanId.HasValue())
                    {
                        if (!order.OrderWithdraws.Any(x => x.Status.HasFlag(WithdrawStatus.Completed | WithdrawStatus.Processing | WithdrawStatus.Voided)))
                        {
                            status = true;
                            var vandarWithdrawResult = await _vandarV2Service.AddNewSettlementAsync(order.ReceiveAmount.ToLong(), ibanId, order.Id.ToInt());
                            if (vandarWithdrawResult.Status == 1)
                            {
                                text = $"درخواست تسویه به شماره شبا {order.ReceiveMemoTagPaymentId} بانک {ibanBank} به نام {ibanOwner} با مبلغ {order.ReceiveAmount.ToPriceWithoutFloat()} تومان برای سفارش شماره {order.Id} در وندار ثبت شد.";
                            }
                            else
                            {
                                text = $"درخواست تسویه مبلغ {order.ReceiveAmount.ToPriceWithoutFloat()} تومان به شماره شبای {order.ReceiveMemoTagPaymentId} به وندار ارسال شد اما با خطای '{vandarWithdrawResult.Error}' مواجه شد.";
                            }
                            await _withdrawService.InsertAsync(new Withdraw()
                            {
                                Amount = order.ReceiveAmount,
                                ApiWithdrawId = vandarWithdrawResult?.Data?.Settlement?.Select(p => p.Key).FirstOrDefault(),
                                WalletApiType = WalletApiType.Vandar,
                                TransactionId = vandarWithdrawResult?.Data?.Settlement?.Select(p => p.Value?.TransactionId.ToString()).FirstOrDefault(),
                                ECurrencyType = ECurrencyType.Shetab,
                                OrderId = order.Id,
                                Status = vandarWithdrawResult.Status == 1 ? WithdrawStatus.Processing : WithdrawStatus.Failure,
                                TradingPlatform = TradingPlatform.None,
                                WithdrawType = WithdrawFromType.BankAccount,
                                Fee = vandarWithdrawResult?.Data?.Settlement?.Select(p => p.Value?.IbanId.ToDecimal()).FirstOrDefault() ?? 0
                            });
                            order.ReceiveTransactionCode = vandarWithdrawResult?.Data?.Settlement?.Select(p => p.Value?.TransactionId.ToString()).FirstOrDefault();
                            await _exchangeOrderService.UpdateAsync(order);
                        }
                        else
                        {
                            text = "یک درخواست تسویه قبلا ثبت شده.";
                        }
                    }
                    else
                    {
                        if (!text.HasValue())
                        {
                            text = "خطا در هنگام دریافت آی دی شماره شبا از وندار";
                        }
                    }
                }
                else
                {
                    text = $"شماره شبا وارد شده {order.ReceiveMemoTagPaymentId} می باشد که اشتباه است.";
                }
            }
            catch (Exception ex)
            {
                text = ex.Message;
            }
            return (status, text);
        }

        public async Task<(bool status, string text)> CancelWithdrawFromVandarAsync(long id)
        {
            (bool status, string text) = (false, "");
            try
            {
                var withdraw = await _withdrawService.Table.SingleAsync(p => p.Id == id);
                var settlement = await _vandarV2Service.GetSettlementAsync(withdraw.ApiWithdrawId);
                if (settlement.Status == 1)
                {
                    if (settlement.Data.Settlement.Status == "PENDING")
                    {
                        var deleteSettlementResult = await _vandarV2Service.DeleteSettlementAsync(withdraw.TransactionId);
                        if (deleteSettlementResult.Status == 1)
                        {
                            withdraw.Status = WithdrawStatus.Canceled;
                            await _withdrawService.UpdateAsync(withdraw);
                            text = "درخواست تسویه از وندار لغو گردید.";
                            status = true;

                            var exchangeOrder = await _exchangeOrderService.Table.SingleAsync(p => p.Id == withdraw.OrderId);
                            exchangeOrder.OrderStatus = OrderStatus.Processing;
                            exchangeOrder.ReceiveTransactionCode = "";
                            await _exchangeOrderService.UpdateAsync(exchangeOrder);
                        }
                        else
                        {
                            text = "خطا هنگام لغو درخواست تسویه وندار: " + deleteSettlementResult.Error;
                        }
                    }
                    else
                    {
                        if (settlement.Data.Settlement.Status == "CANCELED")
                        {
                            text = $"تسویه از وندار با شماره تراکنش {settlement.Data.Settlement.TransactionId} قبلا لغو شده";
                        }
                        else
                        {
                            text = $"تسویه از وندار با شماره تراکنش {settlement.Data.Settlement.TransactionId} قابل لغو نیست یا قبلا لغو شده.";
                        }
                    }
                }
                else
                {
                    text = "خطا در هنگام دریافت اطلاعات درخواست تسویه";
                }
            }
            catch (Exception ex)
            {
                text = ex.Message;
            }
            return (status, text);
        }

        private async Task<(bool status, string text)> CashoutPayIrAsync(ExchangeOrder order, decimal amount, string fullName, string ibanBank, decimal fee, int partNumber = 0)
        {
            string text = "";
            bool status = false;
            // Check not withdrawed before then send.
            var payIRWithdrawResult = await _payIRService.CashoutRequestAsync(((amount) * 10).ToInt(), fullName, order.ReceiveMemoTagPaymentId, (order.Id.ToString() + partNumber).ToInt());
            var error = payIRWithdrawResult?.Error ?? "Error: " + payIRWithdrawResult.Error + ". ";
            var msg = payIRWithdrawResult?.Messages.Length > 0 ? $"Message: {string.Join(", ", payIRWithdrawResult?.Messages)}" : "";
            new Exception($"PayIr Cashout: {error}{msg}").LogError();
            if (payIRWithdrawResult.Status == 1)
            {
                status = true;
                text = $"درخواست تسویه به شماره شبا {order.ReceiveMemoTagPaymentId} {(ibanBank.HasValue() ? ("بانک " + ibanBank) : "")} به نام {fullName} با مبلغ {amount.ToPriceWithoutFloat()} تومان برای سفارش شماره {order.Id} در پی دات آی آر ثبت شد. کارمزد {fee.ToPriceWithoutFloat()} تومان.";
            }
            else
            {
                text = $"درخواست تسویه مبلغ {amount.ToPriceWithoutFloat()} تومان به شماره شبای {order.ReceiveMemoTagPaymentId} به پی ارسال شد اما با خطای '{(string.Join(", ", payIRWithdrawResult?.Messages) + " " + payIRWithdrawResult?.Error).Trim()}' مواجه شد.";
            }

            await _withdrawService.InsertAsync(new Withdraw()
            {
                Amount = amount,
                ApiWithdrawId = payIRWithdrawResult?.Data?.CashoutId.ToString(),
                WalletApiType = WalletApiType.PayIR,
                TransactionId = payIRWithdrawResult?.Data?.StatusLabel,
                ECurrencyType = ECurrencyType.Shetab,
                OrderId = order.Id,
                Status = payIRWithdrawResult.Status == 1 ? WithdrawStatus.Processing : WithdrawStatus.Failure,
                TradingPlatform = TradingPlatform.None,
                WithdrawType = WithdrawFromType.BankAccount,
                Fee = fee
            });
            order.ReceiveTransactionCode = payIRWithdrawResult?.Data?.CashoutId.ToString();
            if (!status)
                order.Errors += $"{string.Join(", ", payIRWithdrawResult?.Messages)} {payIRWithdrawResult?.Error}".Trim();
            await _exchangeOrderService.UpdateAsync(order);

            return (status, text);
        }
    }

    [Serializable]
    public class WalletAddress
    {
        public WalletApiType WalletType { get; set; }
        public ECurrencyType CurrencyType { get; set; }
        public string Address { get; set; }
        public string PayTagId { get; set; }
        public string Error { get; set; }
    }
}