﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Utility;

namespace Xtoman.Service
{
    public class ReferralManager : IReferralManager
    {
        private readonly IUserIncomeService _userIncomeService;
        private readonly IOrderService _orderService;
        private readonly ICommissionService _commissionService;
        public ReferralManager(
            IUserIncomeService userIncomeService,
            IOrderService orderService,
            ICommissionService commissionService)
        {
            _userIncomeService = userIncomeService;
            _orderService = orderService;
            _commissionService = commissionService;
        }

        public async Task<Commission> GetCommissionByUserIdAsync(int id)
        {
            var commissionLevels = await _commissionService.TableNoTracking.OrderBy(p => p.MinValue).ToListAsync();
            var referrerTotalIncomes = await _userIncomeService.TableNoTracking
                .Where(p => p.UserId == id)
                .Select(p => p.Value).DefaultIfEmpty(0).SumAsync();

            var referrerCurrentPercent = commissionLevels.Select(p => p.Percent).FirstOrDefault();
            foreach (var item in commissionLevels)
                if (referrerTotalIncomes >= item.MinValue)
                    referrerCurrentPercent = item.Percent;

            return commissionLevels.Where(p => p.Percent == referrerCurrentPercent).FirstOrDefault();
        }

        public async Task AddIncomeAsync(Order order)
        {
            if (order.PaymentStatus == PaymentStatus.Paid && order.OrderStatus == OrderStatus.Complete)
            {
                if (order.User.ReferrerId.HasValue)
                {
                    if (order is ExchangeOrder)
                    {
                        var isCalculatedBefore = await _userIncomeService.TableNoTracking.Where(p => p.Order.Id == order.Id).AnyAsync();
                        if (!isCalculatedBefore)
                        {
                            // 1- Calculate System Profit In Toman
                            var exchangeOrder = order as ExchangeOrder;
                            int? systemIncomeInToman = null;
                            switch (exchangeOrder.Exchange.ExchangeFiatCoinType)
                            {
                                case ExchangeFiatCoinType.CoinToCoin:
                                    // ToDo: It's complicated, so complete it at last.
                                    break;
                                case ExchangeFiatCoinType.CoinToFiatUSD:
                                    systemIncomeInToman = CalculateSystemIncomeCoinToFiatUSD(exchangeOrder);
                                    break;
                                case ExchangeFiatCoinType.CoinToFiatToman:
                                    systemIncomeInToman = CalculateSystemIncomeCoinToFiatToman(exchangeOrder);
                                    break;
                                case ExchangeFiatCoinType.FiatUSDToCoin:
                                    systemIncomeInToman = CalculateSystemIncomeFiatUSDToCoin(exchangeOrder);
                                    break;
                                case ExchangeFiatCoinType.FiatTomanToCoin:
                                    systemIncomeInToman = CalculateSystemIncomeFiatTomanToCoin(exchangeOrder);
                                    break;
                                case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman:
                                    systemIncomeInToman = CalculateSystemIncomeFiatUSDToFiatToman(exchangeOrder);
                                    break;
                                case ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham:
                                    systemIncomeInToman = CalculateSystemIncomeFiatTomanToFiatUSD(exchangeOrder);
                                    break;
                                case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham:
                                    // ToDo
                                    break;
                            }

                            if (systemIncomeInToman.HasValue)
                            {
                                // 2- Calculate Referrer Income
                                var commissionLevel = await GetCommissionByUserIdAsync(order.User.ReferrerId.Value);

                                // 3- Add Income
                            }
                        }
                    }
                }
            }
            await Task.Delay(0);
        }

        private int? CalculateSystemIncomeCoinToFiatUSD(ExchangeOrder exchangeOrder)
        {
            var toTether = exchangeOrder.OrderTrades.Where(p => p.ToCryptoType == ECurrencyType.Tether).Select(p => p.ToAmount).FirstOrDefault();
            if (toTether > 0)
            {
                if (exchangeOrder.DollarPriceInToman > 0)
                {
                    var incomeInDollar = toTether - exchangeOrder.ReceiveAmount;
                    if (incomeInDollar > 0)
                        return (incomeInDollar * exchangeOrder.DollarPriceInToman).ToInt();
                }
                return 0;
            }
            return null;
        }

        private int? CalculateSystemIncomeCoinToFiatToman(ExchangeOrder exchangeOrder)
        {
            var toTether = exchangeOrder.OrderTrades.Where(p => p.ToCryptoType == ECurrencyType.Tether).Select(p => p.ToAmount).FirstOrDefault();
            if (toTether > 0)
            {
                if (exchangeOrder.DollarPriceInToman > 0)
                {
                    var calculateOrderUSDPrice = (exchangeOrder.ReceiveAmount / toTether).ToInt();
                    if (calculateOrderUSDPrice < exchangeOrder.DollarPriceInToman)
                        return ((exchangeOrder.DollarPriceInToman - calculateOrderUSDPrice) * toTether).ToInt();
                }
                return 0;
            }
            return null;
        }

        private int? CalculateSystemIncomeFiatUSDToFiatToman(ExchangeOrder exchangeOrder)
        {
            if (exchangeOrder.PayAmount > 0)
            {
                if (exchangeOrder.DollarPriceInToman > 0)
                {
                    var calculateOrderUSDPrice = (exchangeOrder.ReceiveAmount / exchangeOrder.PayAmount).ToInt();
                    if (calculateOrderUSDPrice < exchangeOrder.DollarPriceInToman)
                        return ((exchangeOrder.DollarPriceInToman - calculateOrderUSDPrice) * exchangeOrder.PayAmount).ToInt();
                }
                return 0;
            }
            return null;
        }

        private int? CalculateSystemIncomeFiatUSDToCoin(ExchangeOrder exchangeOrder)
        {
            var fromTether = exchangeOrder.OrderTrades.Where(p => p.FromCryptoType == ECurrencyType.Tether).Select(p => p.FromAmount).FirstOrDefault();
            if (fromTether > 0)
            {
                if (exchangeOrder.DollarPriceInToman > 0)
                {
                    var incomeInDollar = (exchangeOrder.PayAmount - fromTether);
                    if (incomeInDollar > 0m)
                        return (incomeInDollar * exchangeOrder.DollarPriceInToman).ToInt();
                }
                return 0;
            }
            return null;
        }

        private int? CalculateSystemIncomeFiatTomanToFiatUSD(ExchangeOrder exchangeOrder)
        {
            if (exchangeOrder.ReceiveAmount > 0)
            {
                if (exchangeOrder.DollarPriceInToman > 0)
                {
                    var calculateOrderUSDPrice = (exchangeOrder.PayAmount / exchangeOrder.ReceiveAmount).ToInt();
                    if (calculateOrderUSDPrice > exchangeOrder.DollarPriceInToman)
                        return ((calculateOrderUSDPrice - exchangeOrder.DollarPriceInToman) * exchangeOrder.ReceiveAmount).ToInt();
                }
                return 0;
            }
            return null;
        }

        private int? CalculateSystemIncomeFiatTomanToCoin(ExchangeOrder exchangeOrder)
        {
            var fromTether = exchangeOrder.OrderTrades.Where(p => p.FromCryptoType == ECurrencyType.Tether).Select(p => p.FromAmount).FirstOrDefault();
            if (fromTether > 0)
            {
                if (exchangeOrder.DollarPriceInToman > 0)
                {
                    var calculateOrderUSDPrice = (exchangeOrder.PayAmount / fromTether).ToInt();
                    if (calculateOrderUSDPrice > exchangeOrder.DollarPriceInToman)
                        return ((calculateOrderUSDPrice - exchangeOrder.DollarPriceInToman) * fromTether).ToInt();
                }
                return 0;
            }
            return null;
        }

        private async Task AddIncomeAsync(Order order, int amount, int userId)
        {
            var userIncome = new UserIncome()
            {
                UserId = userId,
                Value = amount
            };
            order.UserIncome = userIncome;
            await _orderService.UpdateAsync(order);
        }
    }
}
