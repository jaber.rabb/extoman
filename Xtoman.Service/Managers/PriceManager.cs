﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Service.WebServices;
using Xtoman.Service.WebServices.Price.Nerkh;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service
{
    public class PriceManager : IPriceManager
    {
        #region Properties
        private static HttpClient Client = new HttpClient();
        private readonly IExchangeTypeService _exchangeTypeService;
        //private readonly IExchangeTypeRateService _exchangeTypeRateService;
        //private readonly IDigiarzService _digiArzService;
        private readonly ICoinPaymentsService _coinPaymentsService;
        //private readonly IArzliveService _arzliveService;
        //private readonly IChangellyService _changellyService;
        private readonly ICoinMarketCapService _coinMarketCapService;
        private readonly IPayeerService _payeerService;
        private readonly IPerfectMoneyService _perfectMoneyService;
        //private readonly IWebMoneyService _webMoneyService;
        private readonly PaymentSettings _paymentSettings;
        private readonly ICacheManager _cacheManager;
        private readonly IBalanceManager _balanceManager;
        //private readonly ITGJUService _tGJUService;
        private readonly INerkhService _nerkhService;
        //private readonly INavasanService _navasanService;
        //private readonly IArzwsService _arzwsService;
        private readonly IBinanceService _binanceService;
        #endregion

        #region Constructor
        public PriceManager(
           IExchangeTypeService exchangeTypeService,
           //IExchangeTypeRateService exchangeTypeRateService,
           //IDigiarzService digiArzService,
           ICoinPaymentsService coinPaymentsService,
           //IArzliveService arzliveService,
           //IChangellyService changellyService,
           ICoinMarketCapService coinMarketCapService,
           IPayeerService payeerService,
           IPerfectMoneyService perfectMoneyService,
           //IWebMoneyService webMoneyService,
           PaymentSettings paymentSettings,
           ICacheManager cacheManager,
           IBalanceManager balanceManager,
           IBinanceService binanceService,
           INerkhService nerkhService)
           //INavasanService navasanService
        //IArzwsService arzwsService
        //ITGJUService tGJUService
        {
            _exchangeTypeService = exchangeTypeService;
            //_exchangeTypeRateService = exchangeTypeRateService;
            //_digiArzService = digiArzService;
            _coinPaymentsService = coinPaymentsService;
            //_arzliveService = arzliveService;
            //_changellyService = changellyService;
            _coinMarketCapService = coinMarketCapService;
            _payeerService = payeerService;
            _perfectMoneyService = perfectMoneyService;
            //_webMoneyService = webMoneyService;
            _paymentSettings = paymentSettings;
            _cacheManager = cacheManager;
            _balanceManager = balanceManager;
            _nerkhService = nerkhService;
            //_navasanService = navasanService;
            //_tGJUService = tGJUService;
            //_arzwsService = arzwsService;
            _binanceService = binanceService;
        }
        #endregion

        #region Sync methods
        public ExchangeRate GetExchangeRate(ExchangeType exchangeType)
        {
            var key = $"Xtoman.ExchangeRate.{exchangeType.FromCurrencyId}.{exchangeType.ToCurrencyId}";
            return _cacheManager.Get(key, 1, () =>
            {
                var retryCount = 0;
                var (usdPrice, eurPrice) = USDEURPrice();
                //var usdPrice = _paymentSettings.USDPrice;
                //var eurPrice = _paymentSettings.EURPrice;
                var exchangeRate = (rate: 0M, apiType: ExchangeApiType.None/*, usdPriceInToman: 0M, eurPriceInToman: 0M*/);
                var reversedRate = false;
                while (exchangeRate.rate == 0 && retryCount < 3)
                {
                    retryCount++;
                    switch (exchangeType.ExchangeFiatCoinType)
                    {
                        case ExchangeFiatCoinType.CoinToCoin:
                            exchangeRate = CoinToCoinExchangeRateCalculator(exchangeType);
                            reversedRate = true;
                            break;
                        case ExchangeFiatCoinType.CoinToFiatUSD:
                        case ExchangeFiatCoinType.CoinToFiatToman:
                            exchangeRate = CoinToFiatExchangeRateCalculator(exchangeType, usdPrice.ToInt(), true);
                            reversedRate = true;
                            break;
                        case ExchangeFiatCoinType.FiatUSDToCoin:
                        case ExchangeFiatCoinType.FiatTomanToCoin:
                            exchangeRate = CoinToFiatExchangeRateCalculator(exchangeType, usdPrice.ToInt(), false);
                            break;
                        case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman:
                        case ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham:
                        case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham:
                            exchangeRate = FiatToFiatExchangeRateCalculator(exchangeType);
                            break;
                    }
                }
                return new ExchangeRate()
                {
                    ApiType = exchangeRate.apiType,
                    Rate = exchangeRate.rate,
                    ReversedRate = reversedRate,
                    IsAvailable = ExchangeIsAvailable(exchangeType, exchangeRate.rate),
                    FromPrecision = exchangeType.FromCurrency.MaxPrecision,
                    ToPrecision = exchangeType.ToCurrency.MaxPrecision,
                    USDPriceInToman = usdPrice.ToInt(),
                    EURPriceInToman = eurPrice.ToInt()
                };
            });
        }
        public ExchangeRate GetExchangeRate(int fromCurrencyId, int toCurrencyId)
        {
            var exchangeType = _exchangeTypeService.TableNoTracking
                .Include(p => p.FromCurrency)
                .Include(p => p.ToCurrency)
                //.Include(p => p.ExchangeTypeRates)
                .Where(p => p.FromCurrencyId == fromCurrencyId && p.ToCurrencyId == toCurrencyId)
                .SingleOrDefault();

            return GetExchangeRate(exchangeType);
        }
        public ExchangeRate GetExchangeRate(int exchangeTypeId)
        {
            var exchangeType = _exchangeTypeService.TableNoTracking
                .Include(p => p.FromCurrency)
                .Include(p => p.ToCurrency)
                //.Include(p => p.ExchangeTypeRates)
                .SingleOrDefault(p => p.Id == exchangeTypeId);

            return GetExchangeRate(exchangeType);
        }

        public ExchangePriceResult GetExchangeAmount(int fromCurrencyId, int toCurrencyId, decimal amount, bool isFrom = true, bool minusReserved = false)
        {
            var exchangeType = _exchangeTypeService.TableNoTracking
                .Include(p => p.FromCurrency)
                .Include(p => p.ToCurrency)
                //.Include(p => p.ExchangeTypeRates)
                .Where(p => p.FromCurrencyId == fromCurrencyId && p.ToCurrencyId == toCurrencyId)
                .SingleOrDefault();

            return GetExchangeAmount(exchangeType, amount, isFrom, minusReserved);
        }
        public ExchangePriceResult GetExchangeAmount(int exchangeTypeId, decimal amount, bool isFrom = true, bool minusReserved = false)
        {
            var exchangeType = _exchangeTypeService.TableNoTracking
                .Include(p => p.FromCurrency)
                .Include(p => p.ToCurrency)
                //.Include(p => p.ExchangeTypeRates)
                .SingleOrDefault(p => p.Id == exchangeTypeId);

            return GetExchangeAmount(exchangeType, amount, isFrom, minusReserved);
        }
        public ExchangePriceResult GetExchangeAmount(ExchangeType exchangeType, decimal amount, bool isFrom = true, bool minusReserved = false)
        {
            var result = new ExchangePriceResult() { IsAvailable = exchangeType != null && !exchangeType.Disabled };
            if (amount <= 0 || exchangeType == null || exchangeType.Disabled) return result;


            var toCurrencyBalance = 0M;
            if (!AppSettingManager.IsLocale)
                toCurrencyBalance = _balanceManager.GetBalance(exchangeType.ToCurrency.Type, minusReserved);
            else
                toCurrencyBalance = 9999999999;

            //var toCurrencyBalance = exchangeType.ToCurrency.IsFiat ? GetFiatBalance(exchangeType.ToCurrency) : GetCoinBalance(exchangeType.ToCurrency.Type);
            var exchangeRate = GetExchangeRate(exchangeType);
            //var fromAmount = isFrom ? amount : exchangeRate.Rate > 0 ? exchangeRate.ReversedRate ? amount / exchangeRate.Rate : amount * exchangeRate.Rate : 0;
            //var toAmount = isFrom && exchangeRate.Rate > 0 ? exchangeRate.ReversedRate ? amount * exchangeRate.Rate : amount / exchangeRate.Rate : exchangeRate.Rate > 0 ? amount : 0;
            var fromMaxAmount = CalculateFromMaxAmount(toCurrencyBalance, exchangeRate.Rate, exchangeType.ExchangeFiatCoinType);
            var toMaxAmount = exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.CoinToCoin ? -1 : fromMaxAmount * exchangeRate.Rate;

            var (fromAmount, toAmount) = CalculateToAmountFromAmount(exchangeType, amount, exchangeRate.Rate, isFrom, exchangeRate.ReversedRate);
            return new ExchangePriceResult()
            {
                FromAmount = fromAmount,
                ToAmount = toAmount,
                FromMaxAmount = fromMaxAmount,
                FromMinAmount = exchangeType.FromCurrency.MinimumReceiveAmount,
                IsAvailable = exchangeRate.IsAvailable,
                ToMaxAmount = toMaxAmount,
                ToMinAmount = exchangeType.ToCurrency.MinimumReceiveAmount,
                ApiType = exchangeRate.ApiType,
                Rate = exchangeRate.Rate,
                FromPrecision = exchangeType.FromCurrency.MaxPrecision,
                ToPrecision = exchangeType.ToCurrency.MaxPrecision,
                ReversedRate = exchangeRate.ReversedRate,
                TransactionFee = exchangeType.ToCurrency.TransactionFee ?? 0,
                USDPriceInToman = exchangeRate.USDPriceInToman,
                BTCPriceInUSD = exchangeRate.BTCPriceInUSD,
                BTCValueOfOrder = exchangeRate.BTCValueOfOrder,
                BTCPriceInEUR = exchangeRate.BTCPriceInEUR,
                EURPriceInToman = exchangeRate.EURPriceInToman,
                TomanValueOfOrder = exchangeRate.TomanValueOfOrder
            };
        }
        #endregion

        #region Async methods
        public async Task<ExchangeRate> GetExchangeRateAsync(ExchangeType exchangeType)
        {
            var key = $"Xtoman.ExchangeRate.{exchangeType.FromCurrencyId}.{exchangeType.ToCurrencyId}";
            return await _cacheManager.GetAsync(key, 1, async () =>
            {
                var retryCount = 0;
                var btcPriceInUSD = 0;
                var (usdPrice, eurPrice) = await USDEURPriceAsync();
                //var usdPrice = _paymentSettings.USDPrice;
                //var euroPrice = _paymentSettings.EURPrice;
                var exchangeRate = (rate: 0M, btcPriceInUSD: 0M, apiType: ExchangeApiType.None);
                var reversedRate = false;
                while (exchangeRate.rate == 0 && retryCount < 3)
                {
                    retryCount++;
                    switch (exchangeType.ExchangeFiatCoinType)
                    {
                        case ExchangeFiatCoinType.CoinToCoin:
                            exchangeRate = await CoinToCoinExchangeRateCalculatorAsync(exchangeType);
                            reversedRate = true;
                            break;
                        case ExchangeFiatCoinType.CoinToFiatUSD:
                        case ExchangeFiatCoinType.CoinToFiatToman:
                            exchangeRate = await CoinToFiatExchangeRateCalculatorAsync(exchangeType, usdPrice.ToInt(), true);
                            reversedRate = true;
                            break;
                        case ExchangeFiatCoinType.FiatUSDToCoin:
                        case ExchangeFiatCoinType.FiatTomanToCoin:
                            exchangeRate = await CoinToFiatExchangeRateCalculatorAsync(exchangeType, usdPrice.ToInt(), false);
                            break;
                        case ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham:
                            exchangeRate = await FiatToFiatExchangeRateCalculatorAsync(exchangeType);
                            break;
                        case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman:
                        case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham:
                            exchangeRate = await FiatToFiatExchangeRateCalculatorAsync(exchangeType);
                            reversedRate = true;
                            break;
                    }
                }
                return new ExchangeRate()
                {
                    ApiType = exchangeRate.apiType,
                    Rate = exchangeRate.rate,
                    ReversedRate = reversedRate,
                    IsAvailable = ExchangeIsAvailable(exchangeType, exchangeRate.rate),
                    FromPrecision = exchangeType.FromCurrency.MaxPrecision,
                    ToPrecision = exchangeType.ToCurrency.MaxPrecision,
                    BTCPriceInUSD = btcPriceInUSD,
                    USDPriceInToman = usdPrice.ToInt(),
                    EURPriceInToman = eurPrice.ToInt(),
                };
            });
        }

        public async Task<ExchangeRate> GetExchangeRateAsync(int fromCurrencyId, int toCurrencyId)
        {
            var exchangeType = await _exchangeTypeService.TableNoTracking
                .Include(p => p.FromCurrency)
                .Include(p => p.ToCurrency)
                //.Include(p => p.ExchangeTypeRates)
                .Where(p => p.FromCurrencyId == fromCurrencyId && p.ToCurrencyId == toCurrencyId)
                .SingleOrDefaultAsync();

            return await GetExchangeRateAsync(exchangeType);
        }

        public async Task<List<ExchangeRate>> GetExchangeRateAsync()
        {
            var exchangeTypes = await _exchangeTypeService.TableNoTracking
                .Include(p => p.FromCurrency)
                .Include(p => p.ToCurrency)
                .ToListAsync();

            var result = new List<ExchangeRate>();
            var (usdPrice, eurPrice) = await USDEURPriceAsync();
            foreach (var item in exchangeTypes)
                result.Add(await GetExchangeRateForAllAsync(item, usdPrice));

            return result;
        }

        public async Task<ExchangeRate> GetExchangeRateAsync(int exchangeTypeId)
        {
            var exchangeType = await _exchangeTypeService.TableNoTracking
                .Include(p => p.FromCurrency)
                .Include(p => p.ToCurrency)
                //.Include(p => p.ExchangeTypeRates)
                .SingleOrDefaultAsync(p => p.Id == exchangeTypeId);

            return await GetExchangeRateAsync(exchangeType);
        }

        public async Task<ExchangePriceResult> GetExchangeAmountAsync(int fromCurrencyId, int toCurrencyId, decimal amount, bool isFrom = true, bool minusReserved = false)
        {
            var exchangeType = await _exchangeTypeService.TableNoTracking
                .Include(p => p.FromCurrency)
                .Include(p => p.ToCurrency)
                //.Include(p => p.ExchangeTypeRates)
                .Where(p => p.FromCurrencyId == fromCurrencyId && p.ToCurrencyId == toCurrencyId)
                .SingleOrDefaultAsync();
            return await GetExchangeAmountAsync(exchangeType, amount, isFrom, minusReserved);
        }
        public async Task<ExchangePriceResult> GetExchangeAmountAsync(int exchangeTypeId, decimal amount, bool isFrom = true, bool minusReserved = false)
        {
            var exchangeType = await _exchangeTypeService.TableNoTracking
                .Include(p => p.FromCurrency)
                .Include(p => p.ToCurrency)
                //.Include(p => p.ExchangeTypeRates)
                .SingleOrDefaultAsync(p => p.Id == exchangeTypeId);

            return await GetExchangeAmountAsync(exchangeType, amount, isFrom, minusReserved);
        }
        public async Task<ExchangePriceResult> GetExchangeAmountAsync(ExchangeType exchangeType, decimal amount, bool isFrom = true, bool minusReserved = false)
        {
            var result = new ExchangePriceResult() { IsAvailable = exchangeType != null && !exchangeType.Disabled };
            if (amount <= 0 || exchangeType == null || exchangeType.Disabled) return result;

            var toCurrencyBalance = 0M;
            if (!AppSettingManager.IsLocale)
                toCurrencyBalance = await _balanceManager.GetBalanceAsync(exchangeType.ToCurrency.Type, minusReserved);
            else
                toCurrencyBalance = 9999999999;
            //var toCurrencyBalance = exchangeType.ToCurrency.IsFiat ? GetFiatBalance(exchangeType.ToCurrency) : GetCoinBalance(exchangeType.ToCurrency.Type);

            var exchangeRate = await GetExchangeRateAsync(exchangeType);
            //var fromAmount = isFrom ? amount : exchangeRate.Rate > 0 ? exchangeRate.ReversedRate ? amount / exchangeRate.Rate : amount * exchangeRate.Rate : 0;
            //var toAmount = isFrom && exchangeRate.Rate > 0 ? exchangeRate.ReversedRate ? amount * exchangeRate.Rate : amount / exchangeRate.Rate : exchangeRate.Rate > 0 ? amount : 0;

            var fromMaxAmount = CalculateFromMaxAmount(toCurrencyBalance, exchangeRate.Rate, exchangeType.ExchangeFiatCoinType);
            var toMaxAmount = exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.CoinToCoin ? -1 : fromMaxAmount * exchangeRate.Rate;
            var (fromAmount, toAmount) = CalculateToAmountFromAmount(exchangeType, amount, exchangeRate.Rate, isFrom, exchangeRate.ReversedRate);
            var isFromShetabToPM = exchangeType.ToCurrency.Type.ToString().Contains("PerfectMoney") && exchangeType.FromCurrency.Type == ECurrencyType.Shetab;

            return new ExchangePriceResult()
            {
                FromAmount = fromAmount,
                ToAmount = toAmount,
                FromMaxAmount = exchangeType.FromCurrency.Type == ECurrencyType.Shetab ? 50000000 : fromMaxAmount,
                FromMinAmount = isFromShetabToPM ? 0 : exchangeType.FromCurrency.MinimumReceiveAmount,
                IsAvailable = exchangeRate.IsAvailable,
                ToMaxAmount = toMaxAmount,
                ToMinAmount = isFromShetabToPM ? 2 : exchangeType.ToCurrency.MinimumReceiveAmount,
                ApiType = exchangeRate.ApiType,
                Rate = exchangeRate.Rate,
                FromPrecision = exchangeType.FromCurrency.MaxPrecision,
                ToPrecision = exchangeType.ToCurrency.MaxPrecision,
                ReversedRate = exchangeRate.ReversedRate,
                TransactionFee = exchangeType.ToCurrency.Type.ToString().Contains("PerfectMoney") ? toAmount - (toAmount * exchangeType.ToCurrency.TransactionFee ?? 0 / 100) : exchangeType.ToCurrency.TransactionFee ?? 0,
                USDPriceInToman = exchangeRate.USDPriceInToman,
                BTCPriceInUSD = exchangeRate.BTCPriceInUSD,
                BTCValueOfOrder = exchangeRate.BTCValueOfOrder,
                BTCPriceInEUR = exchangeRate.BTCPriceInEUR,
                EURPriceInToman = exchangeRate.EURPriceInToman,
                TomanValueOfOrder = exchangeRate.TomanValueOfOrder
            };
        }
        #endregion

        #region Helpers
        private bool ExchangeIsAvailable(ExchangeType exchangeType, decimal exchangeRate)
        {
            return !exchangeType.Disabled && exchangeRate > 0;
        }
        private decimal GetFiatBalance(CurrencyType currencyType)
        {
            switch (currencyType.Type)
            {
                case ECurrencyType.Shetab:
                    return currencyType.Available.Value;
                case ECurrencyType.PAYEER:
                    return _payeerService.CheckBalance()?.balance?.USD?.BUDGET ?? 0;
                case ECurrencyType.PerfectMoney:
                    return _perfectMoneyService.Balance()?.Accounts?.Where(p => p.WalletNumber == AppSettingManager.PerfectMoney_USD_Account).FirstOrDefault()?.Balance ?? 0;
                    //case ECurrencyAccountType.WebMoney:
                    //    return _webMoneyService.Balance() // WebMoney not implemented yet
            }
            return 0;
        }
        private decimal GetCoinBalance(ECurrencyType type)
        {
            return _coinPaymentsService.GetBalance(type);
        }
        private decimal CalculateFromMaxAmount(decimal toCurrencyBalance, decimal exchangeRate, ExchangeFiatCoinType exchangeFiatCoinType)
        {
            if (exchangeFiatCoinType == ExchangeFiatCoinType.CoinToCoin)
                return -1;

            var estimatedFromAmount = exchangeRate > 0 ? exchangeFiatCoinType == ExchangeFiatCoinType.FiatUSDToCoin ? toCurrencyBalance * exchangeRate : toCurrencyBalance / exchangeRate : 0;
            return estimatedFromAmount - (estimatedFromAmount * 2 / 100);
        }

        private async Task<ExchangeRate> GetExchangeRateForAllAsync(ExchangeType exchangeType, decimal usdPrice)
        {
            var key = $"Xtoman.ExchangeRate.{exchangeType.FromCurrencyId}.{exchangeType.ToCurrencyId}";

            return await _cacheManager.GetAsync(key, 1, async () =>
            {
                var retryCount = 0;
                var btcPriceInUSD = 0;
                var exchangeRate = (rate: 0M, btcPriceInUSD: 0M, apiType: ExchangeApiType.None);
                var reversedRate = false;
                while (exchangeRate.rate == 0 && retryCount < 3)
                {
                    retryCount++;
                    switch (exchangeType.ExchangeFiatCoinType)
                    {
                        case ExchangeFiatCoinType.CoinToCoin:
                            exchangeRate = await CoinToCoinExchangeRateCalculatorAsync(exchangeType);
                            reversedRate = true;
                            break;
                        case ExchangeFiatCoinType.CoinToFiatUSD:
                        case ExchangeFiatCoinType.CoinToFiatToman:
                            exchangeRate = await CoinToFiatExchangeRateCalculatorAsync(exchangeType, usdPrice.ToInt(), true);
                            reversedRate = true;
                            break;
                        case ExchangeFiatCoinType.FiatUSDToCoin:
                        case ExchangeFiatCoinType.FiatTomanToCoin:
                            exchangeRate = await CoinToFiatExchangeRateCalculatorAsync(exchangeType, usdPrice.ToInt(), false);
                            break;
                        case ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham:
                            exchangeRate = await FiatToFiatExchangeRateCalculatorAsync(exchangeType);
                            break;
                        case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman:
                        case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham:
                            exchangeRate = await FiatToFiatExchangeRateCalculatorAsync(exchangeType);
                            reversedRate = true;
                            break;
                    }
                }
                return new ExchangeRate()
                {
                    ApiType = exchangeRate.apiType,
                    Rate = exchangeRate.rate,
                    ReversedRate = reversedRate,
                    IsAvailable = ExchangeIsAvailable(exchangeType, exchangeRate.rate),
                    FromPrecision = exchangeType.FromCurrency.MaxPrecision,
                    ToPrecision = exchangeType.ToCurrency.MaxPrecision,
                    BTCPriceInUSD = btcPriceInUSD,
                    USDPriceInToman = usdPrice.ToInt()
                };
            });
        }

        private decimal CoinToCoinExchangeRateCalculator(ExchangeType exchangeType, out ExchangeApiType apiType)
        {
            var fromType = exchangeType.FromCurrency.Type;
            var toType = exchangeType.ToCurrency.Type;
            //var changellyRate = _changellyService.ExchangeAvailable(exchangeType.FromCurrency.ChangellyName, exchangeType.ToCurrency.ChangellyName) ? _changellyService.GetExchangeAmount(exchangeType.FromCurrency.ChangellyName, exchangeType.ToCurrency.ChangellyName, 1) : 0;
            var coinPaymentsRate = _coinPaymentsService.ExchangeRate(fromType, toType);
            var coinMarketCapRate = _coinMarketCapService.ExchangeRate(exchangeType.FromCurrency.CoinMarketCapId, exchangeType.ToCurrency.CoinMarketCapId);
            //var digiArzRate = _digiArzService.ExchangeRate(fromType, toType);

            var rates = new List<ExchangeRate>()
            {
                //new ExchangeRate() { ApiType = ExchangeApiType.Changelly, Rate = changellyRate.Value },
                new ExchangeRate() { ApiType = ExchangeApiType.CoinPayments, Rate = coinPaymentsRate },
                new ExchangeRate() { ApiType = ExchangeApiType.CoinMarketCap, Rate = coinMarketCapRate },
                //new ExchangeRate() { ApiType = ExchangeApiType.DigiArz, Rate = digiArzRate }
            };
            var ourBestRate = rates.Where(p => p.Rate > 0).OrderBy(p => p.Rate).FirstOrDefault();
            var exchangeRate = ourBestRate?.Rate ?? 0;

            apiType = ourBestRate?.ApiType ?? ExchangeApiType.None;
            return exchangeRate - (exchangeRate * exchangeType.ExtraFeePercent / 100);
        }

        private decimal FiatToFiatExchangeRateCalculator(ExchangeType exchangeType, out ExchangeApiType priceApiType)
        {
            priceApiType = ExchangeApiType.None;
            if (exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham)
            {
                return 1 + (1 * exchangeType.ExtraFeePercent / 100);
            }
            else
            {
                var (usdPrice, eurPrice) = USDEURPrice();
                var extraFee = usdPrice * exchangeType.ExtraFeePercent / 100;
                if (exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham)
                    return usdPrice + extraFee;
                else if (exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman)
                    return usdPrice - extraFee;
            }
            return 0;
        }
        private decimal GetCoinUSDPrice(CurrencyType currencyType, bool lowerPrice, out ExchangeApiType exchangeApiType)
        {
            var coinMarketCapTicker = _coinMarketCapService.GetTicker(currencyType.CoinMarketCapId);
            //var digiArzResult = _digiArzService.Get(currencyType.Type);

            var coinMarketCapUSDPrice = coinMarketCapTicker?.Price_usd ?? 0;
            //var digiArzUSDPrice = digiArzResult?.rates?.USD?.rate ?? 0;

            var prices = new List<ExchangeRate>()
            {
                new ExchangeRate() { ApiType = ExchangeApiType.CoinMarketCap, Rate = coinMarketCapUSDPrice },
                //new ExchangeRate() { ApiType = ExchangeApiType.DigiArz, Rate = digiArzUSDPrice }
            };
            var q = prices.Where(p => p.Rate != 0);
            q = lowerPrice ? q.OrderBy(p => p.Rate) : q.OrderByDescending(p => p.Rate);
            var bestPrice = q.FirstOrDefault();
            exchangeApiType = bestPrice?.ApiType ?? ExchangeApiType.None;
            return q.Select(p => p.Rate).FirstOrDefault();
        }
        private (decimal rate, ExchangeApiType apiType) CoinToCoinExchangeRateCalculator(ExchangeType exchangeType)
        {
            var fromType = exchangeType.FromCurrency.Type;
            var toType = exchangeType.ToCurrency.Type;

            //var changellyRate = _changellyService.ExchangeAvailable(exchangeType.FromCurrency.ChangellyName, exchangeType.ToCurrency.ChangellyName) ?
            //_changellyService.GetExchangeAmount(exchangeType.FromCurrency.ChangellyName, exchangeType.ToCurrency.ChangellyName, 1) : 0;
            var coinPaymentsRate = _coinPaymentsService.ExchangeRate(fromType, toType);
            var coinMarketCapRate = _coinMarketCapService.ExchangeRate(exchangeType.FromCurrency.CoinMarketCapId, exchangeType.ToCurrency.CoinMarketCapId);
            //var digiArzRate = _digiArzService.ExchangeRate(fromType, toType);

            var rates = new List<ExchangeRate>()
            {
                //new ExchangeRate() { ApiType = ExchangeApiType.Changelly, Rate = changellyRate.Value },
                new ExchangeRate() { ApiType = ExchangeApiType.CoinPayments, Rate = coinPaymentsRate },
                new ExchangeRate() { ApiType = ExchangeApiType.CoinMarketCap, Rate = coinMarketCapRate },
                //new ExchangeRate() { ApiType = ExchangeApiType.DigiArz, Rate = digiArzRate }
            };
            var ourBestRate = rates.Where(p => p.Rate > 0).OrderBy(p => p.Rate).FirstOrDefault();
            var exchangeRate = ourBestRate?.Rate ?? 0;

            var apiType = ourBestRate?.ApiType ?? ExchangeApiType.None;
            var rate = exchangeRate - (exchangeRate * exchangeType.ExtraFeePercent / 100);
            return (rate, apiType);
        }
        private (decimal rate, ExchangeApiType apiType) CoinToFiatExchangeRateCalculator(ExchangeType exchangeType, int usdPrice, bool isCoinToFiat)
        {
            var coinUSDPrice = GetCoinUSDPrice(isCoinToFiat ? exchangeType.FromCurrency : exchangeType.ToCurrency, true);
            var extraFee = (coinUSDPrice.rate * exchangeType.ExtraFeePercent / 100);
            var rate = isCoinToFiat ? coinUSDPrice.rate - extraFee : coinUSDPrice.rate + extraFee;
            return (rate * usdPrice, coinUSDPrice.apiType);
        }
        private (decimal rate, ExchangeApiType apiType) FiatToFiatExchangeRateCalculator(ExchangeType exchangeType)
        {
            var apiType = ExchangeApiType.None;
            if (exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham)
            {
                return (1 + (1 * exchangeType.ExtraFeePercent / 100), apiType);
            }
            else
            {
                var (usdPrice, eurPrice) = USDEURPrice();
                var extraFee = usdPrice * exchangeType.ExtraFeePercent / 100;
                if (exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham)
                    return (usdPrice + extraFee, ExchangeApiType.None);
                else if (exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman)
                    return (usdPrice - extraFee, ExchangeApiType.None);
            }
            return (0, ExchangeApiType.None);
        }
        private (decimal rate, ExchangeApiType apiType) GetCoinUSDPrice(CurrencyType currencyType, bool lowerPrice)
        {
            var coinMarketCapTicker = _coinMarketCapService.GetTicker(currencyType.CoinMarketCapId);
            //var digiArzResult = _digiArzService.Get(currencyType.Type);

            var coinMarketCapUSDPrice = coinMarketCapTicker?.Price_usd ?? 0;
            //var digiArzUSDPrice = digiArzResult?.rates?.USD?.rate ?? 0;

            var prices = new List<ExchangeRate>()
            {
                new ExchangeRate() { ApiType = ExchangeApiType.CoinMarketCap, Rate = coinMarketCapUSDPrice },
                //new ExchangeRate() { ApiType = ExchangeApiType.DigiArz, Rate = digiArzUSDPrice }
            };
            var q = prices.Where(p => p.Rate != 0);
            q = lowerPrice ? q.OrderBy(p => p.Rate) : q.OrderByDescending(p => p.Rate);
            var bestPrice = q.FirstOrDefault();
            var apiType = bestPrice?.ApiType ?? ExchangeApiType.None;
            var rate = q.Select(p => p.Rate).FirstOrDefault();
            return (rate, apiType);
        }

        private (decimal fromAmount, decimal toAmount) CalculateToAmountFromAmount(ExchangeType exchangeType, decimal amount, decimal rate, bool isFrom, bool reversedRate)
        {
            var fromAmount = 0M;
            var toAmount = 0M;
            switch (exchangeType.ExchangeFiatCoinType)
            {
                case ExchangeFiatCoinType.CoinToCoin:
                case ExchangeFiatCoinType.CoinToFiatUSD:
                case ExchangeFiatCoinType.CoinToFiatToman:
                    fromAmount = CalculateFromAmount(amount, rate, isFrom, reversedRate, exchangeType.FromCurrency.Step);
                    toAmount = CalculateToAmount(fromAmount, rate, true, true, exchangeType.ToCurrency.Step);
                    break;
                case ExchangeFiatCoinType.FiatUSDToCoin:
                case ExchangeFiatCoinType.FiatTomanToCoin:
                    toAmount = CalculateToAmount(amount, rate, isFrom, reversedRate, exchangeType.ToCurrency.Step);
                    fromAmount = CalculateFromAmount(toAmount, rate, false, false, 1);
                    break;
                case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman:
                case ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham:
                case ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham:
                    fromAmount = CalculateFromAmount(amount, rate, isFrom, reversedRate);
                    toAmount = CalculateToAmount(amount, rate, isFrom, reversedRate);
                    break;
            }

            fromAmount = FixMaxPrecision(fromAmount, exchangeType.FromCurrency.MaxPrecision);
            toAmount = FixMaxPrecision(toAmount, exchangeType.ToCurrency.MaxPrecision);

            return (fromAmount, toAmount);
        }

        private decimal FixMaxPrecision(decimal fromAmount, int maxPrecision)
        {
            return Convert.ToDecimal(fromAmount.ToBlockChainPrice(maxPrecision));
        }

        private decimal CalculateFromAmount(decimal amount, decimal rate, bool isFrom, bool reversed)
        {
            var result = 0M;
            if (isFrom)
                result = amount;
            else if (rate > 0)
            {
                if (reversed)
                    result = amount / rate;
                else
                    result = amount * rate;
            }

            return result;
        }
        private decimal CalculateFromAmount(decimal amount, decimal rate, bool isFrom, bool reversed, decimal roundStep)
        {
            var result = 0M;
            if (isFrom)
                result = amount;
            else if (rate > 0)
            {
                if (reversed)
                    result = amount / rate;
                else
                    result = amount * rate;
            }
            return result.RoundByStep(roundStep); // + Max Precision
        }

        private decimal CalculateToAmount(decimal amount, decimal rate, bool isFrom, bool reversed, decimal roundStep)
        {
            var result = 0M;
            if (rate > 0)
            {
                if (isFrom)
                {
                    if (reversed)
                        result = amount * rate;
                    else
                        result = amount / rate;
                }
                else
                {
                    result = amount;
                }
            }
            return result.RoundByStep(roundStep);
        }

        private decimal CalculateToAmount(decimal amount, decimal rate, bool isFrom, bool reversed)
        {
            var result = 0M;
            if (rate > 0)
            {
                if (isFrom)
                {
                    if (reversed)
                        result = amount * rate;
                    else
                        result = amount / rate;
                }
                else
                {
                    result = amount;
                }
            }
            return result;
        }
        //public decimal USDPrice()
        //{
        //    //var arzliveUSDPrice = _arzliveService.USDPriceInToman();
        //    var tgjuUSDPrice = _tGJUService.USDPriceInToman();
        //    var usdPrices = new List<ExchangeRate>()
        //        {
        //            //new ExchangeRate() { ApiType = ExchangeApiType.ArzLive, Rate = arzliveUSDPrice.Value },
        //            new ExchangeRate() { ApiType = ExchangeApiType.TGJU, Rate = tgjuUSDPrice.Value },
        //            new ExchangeRate() { ApiType = ExchangeApiType.None, Rate = _paymentSettings.USDPrice }
        //        };
        //    var bestRate = usdPrices.Where(p => p.Rate > 4500).OrderByDescending(p => p.Rate).FirstOrDefault();
        //    var bestUSDPRice = bestRate?.Rate ?? 0;
        //    if (bestRate != null && bestRate.ApiType == ExchangeApiType.TGJU)
        //        bestUSDPRice += (bestUSDPRice * 4 / 100);
        //    return bestUSDPRice;
        //}

        public (decimal usdPrice, decimal eurPrice) USDEURPrice()
        {
            long navasanUSD = 0;
            //var (nerkhUSD, nerkhEUR) = _arzwsService.USDAndEURPrice();
            var (nerkhUSD, nerkhEUR) = _nerkhService.USDAndEURPrice();
            //try
            //{
            //    navasanUSD = _navasanService.DollarPriceAsync().Result;
            //}
            //catch (Exception)
            //{
            //    //ex.LogError();
            //}
            //Dollar
            var usdPrices = new List<ExchangeRate>()
                {
                    new ExchangeRate() { ApiType = ExchangeApiType.NerkhAPI, Rate = nerkhUSD },
                    new ExchangeRate() { ApiType = ExchangeApiType.NavasanAPI, Rate = navasanUSD },
                    //new ExchangeRate() { ApiType = ExchangeApiType.None, Rate = _paymentSettings.USDPrice }
                };
            var bestUSDRate = usdPrices.Where(p => p.Rate >= 0 && p.Rate >= (_paymentSettings.USDPrice / 3 * 2)).OrderByDescending(p => p.Rate).FirstOrDefault();
            var bestUSDPRice = bestUSDRate?.Rate ?? 0;

            //Euro
            var eurPrices = new List<ExchangeRate>()
            {
                new ExchangeRate() { ApiType = ExchangeApiType.NerkhAPI, Rate = nerkhEUR },
                //new ExchangeRate() { ApiType = ExchangeApiType.None, Rate = _paymentSettings.EURPrice }
            };

            var bestEURRate = eurPrices.Where(p => p.Rate >= 0 && p.Rate >= (_paymentSettings.EURPrice / 3 * 2)).OrderByDescending(p => p.Rate).FirstOrDefault();
            var bestEURPRice = bestEURRate?.Rate ?? 0;

            if (bestUSDPRice <= 0)
                throw new Exception($"خطا در هنگام دریافت نرخ دلار. nerkhapi: {nerkhUSD}, navasan: {navasanUSD}");

            return (bestUSDPRice, bestEURPRice);
        }

        private async Task<decimal> GetFiatBalanceAsync(CurrencyType currencyType)
        {
            switch (currencyType.Type)
            {
                case ECurrencyType.Shetab:
                    return currencyType.Available.Value;
                case ECurrencyType.PAYEER:
                    return (await _payeerService.CheckBalanceAsync())?.balance?.USD?.BUDGET ?? 0;
                case ECurrencyType.PerfectMoney:
                    return (await _perfectMoneyService.BalanceAsync())?.Accounts?.Where(p => p.WalletNumber == AppSettingManager.PerfectMoney_USD_Account).FirstOrDefault()?.Balance ?? 0;
                    //case ECurrencyAccountType.WebMoney:
                    //    return _webMoneyService.Balance() // WebMoney not implemented yet
            }
            return 0;
        }
        private async Task<decimal> GetCoinBalanceAsync(ECurrencyType type)
        {
            return await _coinPaymentsService.GetBalanceAsync(type);
        }
        private async Task<(decimal rate, decimal btcUSDPrice, ExchangeApiType apiType)> CoinToCoinExchangeRateCalculatorAsync(ExchangeType exchangeType)
        {
            var fromType = exchangeType.FromCurrency.Type;
            var toType = exchangeType.ToCurrency.Type;

            //var changellyRate = await _changellyService.ExchangeAvailableAsync(exchangeType.FromCurrency.ChangellyName, exchangeType.ToCurrency.ChangellyName) ?
            //                    await _changellyService.GetExchangeAmountAsync(exchangeType.FromCurrency.ChangellyName, exchangeType.ToCurrency.ChangellyName, 1) : 0;
            //var coinPaymentsRate = await _coinPaymentsService.ExchangeRateAsync(fromType, toType);
            //var coinMarketCapRate = await _coinMarketCapService.ExchangeRateAsync(exchangeType.FromCurrency.CoinMarketCapId, exchangeType.ToCurrency.CoinMarketCapId);
            var binanceRate = await GetCoinToCoinRateAsync(exchangeType.FromCurrency.Type, exchangeType.ToCurrency.Type);
            //var digiArzRate = await _digiArzService.ExchangeRateAsync(fromType, toType);

            var rates = new List<ExchangeRate>()
            {
                //new ExchangeRate() { ApiType = ExchangeApiType.Changelly, Rate = changellyRate.Value },
                //new ExchangeRate() { ApiType = ExchangeApiType.CoinPayments, Rate = coinPaymentsRate },
                new ExchangeRate() { ApiType = ExchangeApiType.Binance, Rate = binanceRate.rate },
                //new ExchangeRate() { ApiType = ExchangeApiType.DigiArz, Rate = digiArzRate }
            };
            var ourBestRate = rates.Where(p => p.Rate > 0).OrderBy(p => p.Rate).FirstOrDefault();
            var exchangeRate = ourBestRate?.Rate ?? 0;

            var apiType = ourBestRate?.ApiType ?? ExchangeApiType.None;
            var rate = exchangeRate - (exchangeRate * exchangeType.ExtraFeePercent / 100);
            return (rate, 0M, apiType);
        }
        private async Task<(decimal rate, decimal btcUSDPrice, ExchangeApiType apiType)> CoinToFiatExchangeRateCalculatorAsync(ExchangeType exchangeType, int usdPrice, bool isCoinToFiat)
        {
            var coinUSDPrice = await GetCoinUSDPriceAsync(isCoinToFiat ? exchangeType.FromCurrency : exchangeType.ToCurrency, isCoinToFiat);
            var extraFee = (coinUSDPrice.rate * exchangeType.ExtraFeePercent / 100);
            var rate = isCoinToFiat ? coinUSDPrice.rate - extraFee : coinUSDPrice.rate + extraFee;
            if (exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatTomanToCoin || exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.CoinToFiatToman)
                return (rate * usdPrice, coinUSDPrice.rate, coinUSDPrice.apiType);

            return (rate, coinUSDPrice.rate, coinUSDPrice.apiType);
        }
        private async Task<(decimal rate, decimal btcUSDPrice, ExchangeApiType apiType)> FiatToFiatExchangeRateCalculatorAsync(ExchangeType exchangeType)
        {
            var apiType = ExchangeApiType.None;
            if (exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatUSDEuroDirham)
            {
                return (1 + (1 * exchangeType.ExtraFeePercent / 100), 0M, apiType);
            }
            else
            {
                var (usdPrice, eurPrice) = await USDEURPriceAsync();
                var extraFee = usdPrice * exchangeType.ExtraFeePercent / 100;
                if (exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatTomanToFiatUSDEuroDirham)
                    return (usdPrice + extraFee, 0M, ExchangeApiType.None);
                else if (exchangeType.ExchangeFiatCoinType == ExchangeFiatCoinType.FiatUSDEuroDirhamToFiatToman)
                    return (usdPrice - extraFee, 0M, ExchangeApiType.None);
            }
            return (0, 0, ExchangeApiType.None);
        }

        private async Task<(decimal rate, ExchangeApiType)> GetCoinToCoinRateAsync(ECurrencyType fromType, ECurrencyType toType)
        {
            var allPrices = await _binanceService.GetAllPricesAsync();
            if (fromType == ECurrencyType.Tether || toType == ECurrencyType.Tether)
            {
                if (toType == ECurrencyType.Tether)
                {
                    var symbol = $"{fromType.ToBinanceAsset()}USDT";
                    var toUSDTPrice = allPrices.Where(p => p.Symbol == symbol).Select(p => p.Price).FirstOrDefault();
                    return (toUSDTPrice, ExchangeApiType.Binance);
                }
                else
                {
                    var symbol = $"{toType.ToBinanceAsset()}USDT";
                    var toUSDTPrice = allPrices.Where(p => p.Symbol == symbol).Select(p => p.Price).FirstOrDefault();
                    return (1 / toUSDTPrice, ExchangeApiType.Binance);
                }
            }
            else if (fromType == ECurrencyType.Bitcoin || toType == ECurrencyType.Bitcoin)
            {
                if (toType == ECurrencyType.Bitcoin)
                {
                    var symbol = $"{fromType.ToBinanceAsset()}BTC";
                    var toBTCPrice = allPrices.Where(p => p.Symbol == symbol).Select(p => p.Price).FirstOrDefault();
                    return (toBTCPrice, ExchangeApiType.Binance);
                }
                else
                {
                    var symbol = $"{toType.ToBinanceAsset()}BTC";
                    var toBTCPrice = allPrices.Where(p => p.Symbol == symbol).Select(p => p.Price).FirstOrDefault();
                    return (1 / toBTCPrice, ExchangeApiType.Binance);
                }
            }
            else
            {
                var fromBtcPrice = allPrices.Where(p => p.Symbol == $"{fromType.ToBinanceAsset()}BTC").Select(p => p.Price).FirstOrDefault();
                var toBtcPrice = allPrices.Where(p => p.Symbol == $"{toType.ToBinanceAsset()}BTC").Select(p => p.Price).FirstOrDefault();

                return (fromBtcPrice / toBtcPrice, ExchangeApiType.Binance);
            }
        }

        private async Task<(decimal rate, ExchangeApiType apiType)> GetCoinUSDPriceAsync(CurrencyType currencyType, bool lowerPrice)
        {
            var coinUSDPrice = 0m;

            if (currencyType.Type == ECurrencyType.Tether)
            {
                coinUSDPrice = 1;
            }
            else
            {
                var allPrices = await _binanceService.GetAllPricesAsync();
                if (allPrices.Any(p => p.Symbol == $"{currencyType.Type.ToBinanceAsset()}USDT"))
                {
                    coinUSDPrice = allPrices.Where(p => p.Symbol == $"{currencyType.Type.ToBinanceAsset()}USDT").Select(p => p.Price).FirstOrDefault();
                }
                else
                {
                    var btcPrice = allPrices.Where(p => p.Symbol == "BTCUSDT").Select(p => p.Price).FirstOrDefault();
                    var binanceToBTCPrice = allPrices.Where(p => p.Symbol == $"{currencyType.Type.ToBinanceAsset()}BTC").Select(p => p.Price).FirstOrDefault();
                    coinUSDPrice = btcPrice * binanceToBTCPrice;
                }
            }
            //var coinMarketCapTicker = await _coinMarketCapService.GetTickerAsync(currencyType.CoinMarketCapId);
            //var coinMarketCapUSDPrice = coinMarketCapTicker?.Price_usd ?? 0;
            //var digiArzResult = await _digiArzService.GetAsync(currencyType.Type);

            //var coinMarketCapUSDPriceBasedOnBTCPrice = 0M;
            //if (currencyType.CoinMarketCapId != "bitcoin")
            //{
            //    var coinMarketCapBTCTicker = await _coinMarketCapService.GetTickerAsync("bitcoin");
            //    var coinMarketCapBTCUSDPrice = coinMarketCapBTCTicker?.Price_usd ?? 0;
            //    coinMarketCapUSDPriceBasedOnBTCPrice = coinMarketCapTicker?.Price_btc ?? 0;

            //    coinMarketCapUSDPriceBasedOnBTCPrice *= coinMarketCapBTCUSDPrice;
            //}
            //var digiArzUSDPrice = digiArzResult?.rates?.USD?.rate ?? 0;

            var usdPrices = new List<ExchangeRate>()
            {
                new ExchangeRate() { ApiType = ExchangeApiType.Binance, Rate = coinUSDPrice },
                //new ExchangeRate() { ApiType = ExchangeApiType.CoinMarketCap, Rate = coinMarketCapUSDPriceBasedOnBTCPrice },
                //new ExchangeRate() { ApiType = ExchangeApiType.DigiArz, Rate = digiArzUSDPrice }
            };
            var usdQuery = usdPrices.Where(p => p.Rate != 0);
            usdQuery = lowerPrice ? usdQuery.OrderBy(p => p.Rate) : usdQuery.OrderByDescending(p => p.Rate);
            var bestPrice = usdQuery.FirstOrDefault();
            var apiType = bestPrice?.ApiType ?? ExchangeApiType.None;
            var rate = bestPrice.Rate;
            return (rate, apiType);
        }

        //public async Task<decimal> USDPriceAsync()
        //{
        //    //var arzliveUSDPrice = await _arzliveService.USDPriceInTomanAsync();
        //    var tgjuPrices = await _tGJUService.GetAllAsync();
        //    var tgjuUSDPrice = tgjuPrices != null && tgjuPrices.SanaBuyUsd != null && tgjuPrices.SanaBuyUsd.Price.HasValue() ? tgjuPrices.SanaBuyUsd.Price.ToInt() : new int?();
        //    var tgjuEURPrice = tgjuPrices != null && tgjuPrices.SanaBuyEur != null && tgjuPrices.SanaBuyEur.Price.HasValue() ? tgjuPrices.SanaBuyEur.Price.ToInt() : new int?();
        //    var usdPrices = new List<ExchangeRate>()
        //        {
        //            //new ExchangeRate() { ApiType = ExchangeApiType.ArzLive, Rate = arzliveUSDPrice.Value },
        //            new ExchangeRate() { ApiType = ExchangeApiType.TGJU, Rate = tgjuUSDPrice.Value },
        //            new ExchangeRate() { ApiType = ExchangeApiType.None, Rate = _paymentSettings.USDPrice }
        //        };

        //    var eurPrices = new List<ExchangeRate>()
        //    {
        //        new ExchangeRate() { ApiType = ExchangeApiType.TGJU, Rate = tgjuEURPrice.Value }
        //    };
        //    var bestUSDRate = usdPrices.Where(p => p.Rate > 4500).OrderByDescending(p => p.Rate).FirstOrDefault();
        //    var bestUSDPRice = bestUSDRate?.Rate ?? 0;
        //    if (bestUSDRate != null && bestUSDRate.ApiType == ExchangeApiType.TGJU)
        //        bestUSDPRice += (bestUSDPRice * 4 / 100);
        //    return bestUSDPRice;
        //}

        public async Task<(decimal usdPrice, decimal eurPrice)> USDEURPriceAsync()
        {
            long navasanUSD = 0;
            //try
            //{
            //    navasanUSD = await _navasanService.DollarPriceAsync();
            //}
            //catch (Exception)
            //{
            //    //ex.LogError();
            //}
            //var (nerkhUSD, nerkhEUR) = await _arzwsService.USDAndEURPriceAsync();
            var (nerkhUSD, nerkhEUR) = await _nerkhService.USDAndEURPriceAsync();
            //Dollar
            var usdPrices = new List<ExchangeRate>()
                {
                    new ExchangeRate() { ApiType = ExchangeApiType.NerkhAPI, Rate = nerkhUSD },
                    new ExchangeRate() { ApiType = ExchangeApiType.NavasanAPI, Rate = navasanUSD },
                    //new ExchangeRate() { ApiType = ExchangeApiType.None, Rate = _paymentSettings.USDPrice }
                };
            var bestUSDRate = usdPrices.Where(p => p.Rate >= 0 && p.Rate >= (_paymentSettings.USDPrice / 3 * 2)).OrderByDescending(p => p.Rate).FirstOrDefault();
            var bestUSDPRice = bestUSDRate?.Rate ?? 0;

            //Euro
            var eurPrices = new List<ExchangeRate>()
            {
                new ExchangeRate() { ApiType = ExchangeApiType.NerkhAPI, Rate = nerkhEUR },
                //new ExchangeRate() { ApiType = ExchangeApiType.None, Rate = _paymentSettings.EURPrice }
            };

            var bestEURRate = eurPrices.Where(p => p.Rate >= 0 && p.Rate >= (_paymentSettings.EURPrice / 3 * 2)).OrderByDescending(p => p.Rate).FirstOrDefault();
            var bestEURPRice = bestEURRate?.Rate ?? 0;

            if (bestUSDPRice <= 0)
                throw new Exception($"خطا در هنگام دریافت نرخ دلار. async nerkhapi: {nerkhUSD}, navasan: {navasanUSD}");

            return (bestUSDPRice, bestEURPRice);
        }

        public async Task<decimal> ReCalculateReceiveAmountAsync(ExchangeOrder order)
        {
            try
            {
                var exchangeRate = await GetExchangeRateAsync(order.Exchange);
                var (fromAmount, toAmount) = CalculateToAmountFromAmount(order.Exchange, order.PaidAmount, exchangeRate.Rate, true, exchangeRate.ReversedRate);
                return toAmount;
            }
            catch (Exception ex)
            {
                ex.LogError();
            }
            return 0;
        }

        public Task<List<ExchangeRate>> GetAllBuyAndSellAsync()
        {
            throw new NotImplementedException();
        }
        #endregion
    }

    #region Result classes
    public class ExchangeRate
    {
        public ExchangeApiType ApiType { get; set; }
        public decimal Rate { get; set; }
        public bool IsAvailable { get; set; }
        public byte FromPrecision { get; set; }
        public byte ToPrecision { get; set; }
        public bool ReversedRate { get; set; }
        public int USDPriceInToman { get; set; }
        public int EURPriceInToman { get; set; }
        public decimal BTCPriceInUSD { get; set; }
        public decimal BTCPriceInEUR { get; set; }
        public decimal BTCValueOfOrder { get; set; }
        public decimal TomanValueOfOrder { get; set; }
        //public decimal CoinPriceInBTC { get; set; }
        //public decimal CoinPriceInUSD { get; set; }
    }

    public class ExchangePriceResult : ExchangeRate
    {
        public decimal FromAmount { get; set; }
        public decimal FromMinAmount { get; set; }
        public decimal FromMaxAmount { get; set; }
        public decimal ToAmount { get; set; }
        public decimal ToMinAmount { get; set; }
        public decimal ToMaxAmount { get; set; }
        public decimal TransactionFee { get; set; }
    }
    #endregion
}