﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.WebServices.TradingPlatform.Binance;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Service
{
    public class TradeManager : ITradeManager
    {
        private readonly IBinanceService _binanceService;
        private readonly IExchangeOrderService _exchangeOrderService;
        private readonly ITradeService _tradeService;
        public TradeManager(
            IBinanceService binanceService,
            IExchangeOrderService exchangeOrderService,
            ITradeService tradeService)
        {
            _binanceService = binanceService;
            _exchangeOrderService = exchangeOrderService;
            _tradeService = tradeService;
        }

        public async Task<TradeOrderResult> BuyCryptoTradeOrderAsync(ExchangeOrder order)
        {
            var trades = new List<Trade>();
            var binanceResult = false;
            if (!order.Exchange.ToCurrency.IsFiat)
            {
                var toCurrencyType = order.Exchange.ToCurrency.Type.ToBinanceAsset();
                if (toCurrencyType != "USDT") // If to USDT No need to trade just withdraw USDT
                {
                    var tradeRules = await _binanceService.LoadRulesAsync();
                    var binancePrices = await _binanceService.GetAllPricesChange24HAsync();

                    var tradeSymbol = toCurrencyType + "USDT";

                    var lotSizeFilter = tradeRules.Symbols
                        .Where(p => p.SymbolName == tradeSymbol)
                        .SelectMany(p => p.Filters)
                        .Where(p => p.FilterType == "LOT_SIZE")
                        .FirstOrDefault();

                    var minNotionalFilter = tradeRules.Symbols
                        .Where(p => p.SymbolName == tradeSymbol)
                        .SelectMany(p => p.Filters)
                        .Where(p => p.FilterType == "MIN_NOTIONAL")
                        .FirstOrDefault();

                    var tradeQty = FixBinanceTradeLotSizeIncludeTradeFee(order.ReceiveAmount, order.GatewayFee, lotSizeFilter);

                    var symbols = binancePrices.Select(p => p.Symbol).ToList();
                    if (symbols.Contains(tradeSymbol))
                    {
                        if (minNotionalFilter != null)
                            tradeQty = await FixBinanceMinNotionalAsync(tradeSymbol, tradeQty, lotSizeFilter.StepSize, minNotionalFilter);

                        if (!order.OrderTrades.Any())
                        {
                            // Buy with USDT
                            var isTradeStatus = tradeRules.Symbols
                                .Where(p => p.SymbolName.Equals(tradeSymbol, StringComparison.InvariantCultureIgnoreCase))
                                .Select(p => p.Status.Equals("TRADING", StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();

                            if (isTradeStatus)
                            {
                                try
                                {
                                    var orderSide = BinanceOrderSide.BUY;
                                    var tradeResult = await _binanceService.PostNewOrderAsync(tradeSymbol, tradeQty, 0, orderSide, BinanceOrderType.MARKET, BinanceTimeInForce.GTC);
                                    var orderQty = tradeResult.Fills.Select(p => (p.Price * p.Qty)).DefaultIfEmpty(0).Sum();
                                    var binanceTrade = new Trade()
                                    {
                                        ExchangeOrderId = order.Id,
                                        ApiTradeId = tradeResult.ClientOrderId,
                                        TradingPlatform = TradingPlatform.Binance,
                                        FromCryptoType = ECurrencyType.Tether,
                                        ToCryptoType = order.Exchange.ToCurrency.Type,
                                        Price = tradeResult.Price,
                                        FromAmount = orderQty,
                                        TradingFee = tradeResult.Fills.Select(p => p.Commission).DefaultIfEmpty(0).Sum(),
                                        ToAmount = tradeQty,
                                        TradeStatus = TradeStatus.Completed,
                                        TradingFeeType = order.Exchange.ToCurrency.Type,
                                    };
                                    await _tradeService.InsertAsync(binanceTrade);
                                    trades.Add(binanceTrade);
                                    binanceResult = true;
                                }
                                catch (Exception ex)
                                {
                                    order.OrderStatus = OrderStatus.TradeFailed;
                                    order.Errors = "Binance! " + ex.Message;
                                    await _exchangeOrderService.UpdateAsync(order);
                                }
                            }
                        }
                        else
                        {
                            binanceResult = true;
                        }
                    }
                    else
                    {
                        if (order.OrderTrades.Count < 2)
                        {
                            // Buy BTC with USDT then buy asset with BTC
                            tradeSymbol = toCurrencyType + "BTC";
                            var assetPriceInBTC = binancePrices.Where(p => p.Symbol.Equals(tradeSymbol, StringComparison.InvariantCultureIgnoreCase))
                                .Select(p => (p.LastPrice + p.HighPrice) / 2)
                                .DefaultIfEmpty(0).FirstOrDefault();

                            var isTradeStatus = !tradeRules.Symbols
                                .Where(p => p.SymbolName.Equals(tradeSymbol, StringComparison.InvariantCultureIgnoreCase) ||
                                            p.SymbolName.Equals("BTCUSDT", StringComparison.InvariantCultureIgnoreCase))
                                .Any(p => !p.Status.Equals("TRADING", StringComparison.InvariantCultureIgnoreCase));

                            if (isTradeStatus)
                            {
                                var orderSide = BinanceOrderSide.BUY;
                                try
                                {
                                    if (!order.OrderTrades.Any(x => x.FromCryptoType == ECurrencyType.Tether))
                                    {
                                        var btcToBuyQty = (assetPriceInBTC * tradeQty);
                                        lotSizeFilter = tradeRules.Symbols
                                            .Where(p => p.SymbolName == "BTCUSDT")
                                            .SelectMany(p => p.Filters)
                                            .Where(p => p.FilterType == "LOT_SIZE")
                                            .FirstOrDefault();

                                        minNotionalFilter = tradeRules.Symbols
                                            .Where(p => p.SymbolName == "BTCUSDT")
                                            .SelectMany(p => p.Filters)
                                            .Where(p => p.FilterType == "MIN_NOTIONAL")
                                            .FirstOrDefault();

                                        btcToBuyQty = FixBinanceTradeLotSizeIncludeTradeFee(btcToBuyQty, 0, lotSizeFilter);

                                        if (minNotionalFilter != null)
                                            btcToBuyQty = await FixBinanceMinNotionalAsync("BTCUSDT", btcToBuyQty, lotSizeFilter.StepSize, minNotionalFilter);

                                        var btcTradeResult = await _binanceService.PostNewOrderAsync("BTCUSDT", btcToBuyQty, 0, orderSide, BinanceOrderType.MARKET, BinanceTimeInForce.GTC);
                                        var orderQty = btcTradeResult.Fills.Select(p => (p.Price * p.Qty)).DefaultIfEmpty(0).Sum();
                                        var binanceTrade = new Trade()
                                        {
                                            ExchangeOrderId = order.Id,
                                            ApiTradeId = btcTradeResult.ClientOrderId,
                                            TradingPlatform = TradingPlatform.Binance,
                                            FromCryptoType = ECurrencyType.Tether,
                                            ToCryptoType = ECurrencyType.Bitcoin,
                                            Price = btcTradeResult.Fills.Select(p => p.Price).DefaultIfEmpty(0).Sum(),
                                            FromAmount = orderQty,
                                            TradingFee = btcTradeResult.Fills.Select(p => p.Commission).DefaultIfEmpty(0).Sum(),
                                            ToAmount = btcToBuyQty,
                                            TradeStatus = TradeStatus.Completed,
                                            TradingFeeType = order.Exchange.ToCurrency.Type,
                                        };
                                        await _tradeService.InsertAsync(binanceTrade);
                                        trades.Add(binanceTrade);
                                    }
                                    binanceResult = true;
                                }
                                catch (Exception ex)
                                {
                                    order.OrderStatus = OrderStatus.TradeFailed;
                                    order.Errors = "Binance! BTCUSDT trade:" + ex.Message;
                                    await _exchangeOrderService.UpdateAsync(order);
                                }
                                if (binanceResult && trades.Any(x => x.ToCryptoType == ECurrencyType.Bitcoin))
                                {
                                    try
                                    {
                                        if (!order.OrderTrades.Any(x => x.FromCryptoType == ECurrencyType.Bitcoin))
                                        {
                                            lotSizeFilter = tradeRules.Symbols
                                                .Where(p => p.SymbolName == tradeSymbol)
                                                .SelectMany(p => p.Filters)
                                                .Where(p => p.FilterType == "LOT_SIZE")
                                                .FirstOrDefault();

                                            minNotionalFilter = tradeRules.Symbols
                                                .Where(p => p.SymbolName == tradeSymbol)
                                                .SelectMany(p => p.Filters)
                                                .Where(p => p.FilterType == "MIN_NOTIONAL")
                                                .FirstOrDefault();

                                            tradeQty = FixBinanceTradeLotSizeIncludeTradeFee(order.ReceiveAmount, order.GatewayFee, lotSizeFilter);
                                            if (minNotionalFilter != null)
                                                tradeQty = await FixBinanceMinNotionalAsync(tradeSymbol, tradeQty, lotSizeFilter.StepSize, minNotionalFilter);

                                            var tradeResult = await _binanceService.PostNewOrderAsync(tradeSymbol, tradeQty, 0, orderSide, BinanceOrderType.MARKET, BinanceTimeInForce.GTC);
                                            var orderQty = tradeResult.Fills.Select(p => (p.Price * p.Qty)).DefaultIfEmpty(0).Sum();
                                            var binanceTrade = new Trade()
                                            {
                                                ExchangeOrderId = order.Id,
                                                ApiTradeId = tradeResult.ClientOrderId,
                                                TradingPlatform = TradingPlatform.Binance,
                                                FromCryptoType = ECurrencyType.Bitcoin,
                                                ToCryptoType = order.Exchange.ToCurrency.Type,
                                                Price = tradeResult.Fills.Select(p => p.Price).DefaultIfEmpty(0).Sum(),
                                                FromAmount = orderQty,
                                                TradingFee = tradeResult.Fills.Select(p => p.Commission).DefaultIfEmpty(0).Sum(),
                                                ToAmount = tradeQty,
                                                TradeStatus = TradeStatus.Completed,
                                                TradingFeeType = order.Exchange.ToCurrency.Type,
                                            };
                                            await _tradeService.InsertAsync(binanceTrade);
                                            trades.Add(binanceTrade);
                                        }
                                        binanceResult = true;
                                    }
                                    catch (Exception ex)
                                    {
                                        order.OrderStatus = OrderStatus.TradeFailed;
                                        order.Errors = "Binance! Second trade" + ex.Message;
                                        await _exchangeOrderService.UpdateAsync(order);
                                        binanceResult = false;
                                    }
                                }
                                else
                                {
                                    binanceResult = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    binanceResult = true;
                }
            }
            else
            {
                binanceResult = true;
            }

            var result = new TradeOrderResult()
            {
                BinanceResult = binanceResult,
                Trades = trades
            };
            return result;
        }

        public async Task<bool> CoinToCoinTradeOrderAsync(ExchangeOrder order)
        {
            var binancePrices = await _binanceService.GetAllPricesChange24HAsync();
            var symbols = binancePrices.Select(p => p.Symbol).ToList();
            var fromAsset = order.Exchange.FromCurrency.Type.ToBinanceAsset();
            var toAsset = order.Exchange.ToCurrency.Type.ToBinanceAsset();
            var fromAmount = order.PaidAmount - (order.PaidAmount * 0.5m / 100) - (order.Exchange.FromCurrency.TransactionFee ?? 0); // Minus CoinPayment Fees
            fromAmount -= (fromAmount * order.ExchangeExtraFeePercentInTime / 100); // Minus Exchange Fee

            if (order.Exchange.FromCurrency.Type == ECurrencyType.Bitcoin)
            {
                if (order.Exchange.ToCurrency.Type == ECurrencyType.Tether)
                {
                    // Sell Bitcoin to USDT, example: "BTCUSDT"
                    var trade = new Trade() // N
                    {
                        ExchangeOrderId = order.Id,
                        TradingPlatform = TradingPlatform.Binance,
                        FromAmount = fromAmount,
                        FromCryptoType = ECurrencyType.Bitcoin,
                        ToCryptoType = ECurrencyType.Tether,
                        TradeStatus = TradeStatus.Queue
                    };
                    await _tradeService.InsertAsync(trade);
                    return true;
                }
                else
                {
                    // Buy (ToCurrency) with BTC
                    var trade = new Trade() // Y Example : "XRPBTC"
                    {
                        ExchangeOrderId = order.Id,
                        TradingPlatform = TradingPlatform.Binance,
                        FromAmount = fromAmount,
                        FromCryptoType = ECurrencyType.Bitcoin,
                        ToCryptoType = order.Exchange.ToCurrency.Type,
                        TradeStatus = TradeStatus.Queue
                    };
                    await _tradeService.InsertAsync(trade);
                    return true;
                }
            }
            else if (order.Exchange.FromCurrency.Type == ECurrencyType.Tether)
            {
                // If symbol (XXXUSDT) exist Buy (ToCurrency) coin with USDT
                if (symbols.Contains($"{toAsset}USDT"))
                {
                    var trade = new Trade() // Y
                    {
                        ExchangeOrderId = order.Id,
                        TradingPlatform = TradingPlatform.Binance,
                        FromAmount = fromAmount,
                        FromCryptoType = ECurrencyType.Tether,
                        ToCryptoType = order.Exchange.ToCurrency.Type,
                        TradeStatus = TradeStatus.Queue
                    };
                    await _tradeService.InsertAsync(trade);
                    return true;
                }
                // Else Buy BTC with USDT then Buy (ToCurrency) coin with BTC
                else
                {
                    var trade1 = new Trade()
                    {
                        ExchangeOrderId = order.Id,
                        TradingPlatform = TradingPlatform.Binance,
                        FromAmount = fromAmount,
                        FromCryptoType = ECurrencyType.Tether,
                        ToCryptoType = ECurrencyType.Bitcoin,
                        TradeStatus = TradeStatus.Queue
                    };
                    await _tradeService.InsertAsync(trade1);

                    var trade2 = new Trade()
                    {
                        ExchangeOrderId = order.Id,
                        TradingPlatform = TradingPlatform.Binance,
                        FromCryptoType = ECurrencyType.Bitcoin,
                        ToCryptoType = order.Exchange.ToCurrency.Type,
                        TradeStatus = TradeStatus.Queue
                    };
                    await _tradeService.InsertAsync(trade2);
                    return true;
                }
            }
            else
            {
                // If symbol (FromTo) or (ToFrom) Exist Sell/Buy coin
                if (symbols.Contains($"{fromAsset}{toAsset}") || symbols.Contains($"{toAsset}{fromAsset}")) // Example: DCR/ETH || ETH/DCR
                {
                    var trade = new Trade()
                    {
                        ExchangeOrderId = order.Id,
                        TradingPlatform = TradingPlatform.Binance,
                        FromAmount = fromAmount,
                        FromCryptoType = order.Exchange.FromCurrency.Type,
                        ToCryptoType = order.Exchange.ToCurrency.Type,
                        TradeStatus = TradeStatus.Queue
                    };
                    await _tradeService.InsertAsync(trade);
                    return true;
                }
                // Else sell FromCurrency To BTC then Buy (ToCurrency) coin with BTC if ToCurrency and FromCurrency both exist in symbols
                else if (symbols.Contains(fromAsset + "BTC") && symbols.Contains(toAsset + "BTC"))
                {
                    var trade1 = new Trade()
                    {
                        ExchangeOrderId = order.Id,
                        TradingPlatform = TradingPlatform.Binance,
                        FromAmount = fromAmount,
                        FromCryptoType = order.Exchange.FromCurrency.Type,
                        ToCryptoType = ECurrencyType.Bitcoin,
                        TradeStatus = TradeStatus.Queue
                    };
                    await _tradeService.InsertAsync(trade1);

                    var trade2 = new Trade() // From and To Amount Must Be Calculate
                    {
                        ExchangeOrderId = order.Id,
                        TradingPlatform = TradingPlatform.Binance,
                        FromCryptoType = ECurrencyType.Bitcoin,
                        ToCryptoType = order.Exchange.ToCurrency.Type,
                        TradeStatus = TradeStatus.Queue
                    };
                    await _tradeService.InsertAsync(trade2);
                    return true;
                }
                // Else symobls is not valid
                return false;
            }
        }

        public async Task<decimal> SellCryptoTradeOrderAsync(ExchangeOrder order, bool minusTransactionFee = false)
        {
            var tradeQty = order.PaidAmount - (order.PaidAmount * 0.5M / 100);
            if (minusTransactionFee)
                tradeQty -= (order.Exchange.FromCurrency.TransactionFee ?? 0);

            if (!order.Exchange.FromCurrency.IsFiat)
            {
                if (order.Exchange.FromCurrency.Type != ECurrencyType.Tether && order.OrderTrades.Count == 0)
                {
                    var tradeRules = await _binanceService.LoadRulesAsync();
                    if (order.Exchange.FromCurrency.Type == ECurrencyType.Bitcoin)
                    {
                        var tradeSymbol = "BTCUSDT";
                        var lotSizeFilter = tradeRules.Symbols
                            .Where(p => p.SymbolName == tradeSymbol)
                            .SelectMany(p => p.Filters)
                            .Where(p => p.FilterType == "LOT_SIZE")
                            .FirstOrDefault();

                        var minNotional = tradeRules.Symbols
                            .Where(p => p.SymbolName == tradeSymbol)
                            .SelectMany(p => p.Filters)
                            .Where(p => p.FilterType == "MIN_NOTIONAL")
                            .FirstOrDefault();

                        tradeQty = FixBinanceTradeLotSizeIncludeTradeFee(tradeQty, 0, lotSizeFilter);
                        tradeQty = await FixBinanceMinNotionalAsync(tradeSymbol, tradeQty, lotSizeFilter.StepSize, minNotional);

                        var binanceTrade = new Trade()
                        {
                            TradingPlatform = TradingPlatform.Binance,
                            ExchangeOrderId = order.Id,
                            ToCryptoType = ECurrencyType.Tether,
                            FromCryptoType = ECurrencyType.Bitcoin,
                            TradingFeeType = order.Exchange.ToCurrency.Type,
                            FromAmount = tradeQty,
                            TradeStatus = TradeStatus.Queue
                        };
                        await _tradeService.InsertAsync(binanceTrade);
                    }
                    else
                    {
                        var asset = order.Exchange.FromCurrency.Type.ToBinanceAsset();
                        var tradeSymbol = $"{asset}USDT";
                        var binancePrices = await _binanceService.GetAllPricesChange24HAsync();
                        var symbols = binancePrices.Select(p => p.Symbol).ToList();
                        if (symbols.Contains(tradeSymbol))
                        {
                            var lotSizeFilter = tradeRules.Symbols
                                .Where(p => p.SymbolName == tradeSymbol)
                                .SelectMany(p => p.Filters)
                                .Where(p => p.FilterType == "LOT_SIZE")
                                .FirstOrDefault();

                            var minNotional = tradeRules.Symbols
                                .Where(p => p.SymbolName == tradeSymbol)
                                .SelectMany(p => p.Filters)
                                .Where(p => p.FilterType == "MIN_NOTIONAL")
                                .FirstOrDefault();

                            tradeQty = FixBinanceTradeLotSizeIncludeTradeFee(tradeQty, 0, lotSizeFilter);
                            tradeQty = await FixBinanceMinNotionalAsync(tradeSymbol, tradeQty, lotSizeFilter.StepSize, minNotional);

                            var binanceTrade = new Trade()
                            {
                                TradingPlatform = TradingPlatform.Binance,
                                ExchangeOrderId = order.Id,
                                ToCryptoType = ECurrencyType.Tether,
                                FromCryptoType = order.Exchange.FromCurrency.Type,
                                TradingFeeType = order.Exchange.ToCurrency.Type,
                                FromAmount = tradeQty,
                                TradeStatus = TradeStatus.Queue
                            };
                            await _tradeService.InsertAsync(binanceTrade);
                        }
                        else
                        {
                            tradeSymbol = $"{asset}BTC";

                            var lotSizeFilter = tradeRules.Symbols
                                .Where(p => p.SymbolName == tradeSymbol)
                                .SelectMany(p => p.Filters)
                                .Where(p => p.FilterType == "LOT_SIZE")
                                .FirstOrDefault();

                            var minNotional = tradeRules.Symbols
                                .Where(p => p.SymbolName == tradeSymbol)
                                .SelectMany(p => p.Filters)
                                .Where(p => p.FilterType == "MIN_NOTIONAL")
                                .FirstOrDefault();

                            tradeQty = FixBinanceTradeLotSizeIncludeTradeFee(tradeQty, 0, lotSizeFilter);
                            tradeQty = await FixBinanceMinNotionalAsync(tradeSymbol, tradeQty, lotSizeFilter.StepSize, minNotional);

                            var firstTrade = new Trade()
                            {
                                TradingPlatform = TradingPlatform.Binance,
                                ExchangeOrderId = order.Id,
                                ToCryptoType = ECurrencyType.Bitcoin,
                                FromCryptoType = order.Exchange.FromCurrency.Type,
                                TradingFeeType = order.Exchange.ToCurrency.Type,
                                FromAmount = tradeQty,
                                TradeStatus = TradeStatus.Queue
                            };
                            await _tradeService.InsertAsync(firstTrade);

                            var secondTrade = new Trade()
                            {
                                TradingPlatform = TradingPlatform.Binance,
                                ExchangeOrderId = order.Id,
                                ToCryptoType = ECurrencyType.Tether,
                                FromCryptoType = ECurrencyType.Bitcoin,
                                TradingFeeType = order.Exchange.ToCurrency.Type,
                                TradeStatus = TradeStatus.Queue
                            };
                            await _tradeService.InsertAsync(secondTrade);
                        }
                    }
                }
            }
            order.Errors = null;
            order.OrderStatus = OrderStatus.Processing;
            await _exchangeOrderService.UpdateAsync(order);

            return tradeQty;
        }

        //public async Task<(bool tradeResult, decimal fromAmount, decimal toAmount)> SellCryptoTradeOrderAsync(ExchangeOrder order)
        //{
        //    var tradeResult = false;
        //    var toAmount = 0M;
        //    var tradeQty = order.PaidAmount - (order.PaidAmount * 0.5M / 100) - (order.Exchange.FromCurrency.TransactionFee ?? 0);
        //    if (!order.Exchange.FromCurrency.IsFiat)
        //    {
        //        try
        //        {
        //            if (order.Exchange.FromCurrency.Type == ECurrencyType.Tether)
        //            {
        //                tradeResult = true;
        //                toAmount = 1;
        //            }
        //            else if (order.Exchange.FromCurrency.Type == ECurrencyType.Bitcoin)
        //            {
        //                try
        //                {
        //                    if (!order.OrderTrades.Any())
        //                    {
        //                        var tradeSymbol = "BTCUSDT";
        //                        var tradeRules = await _binanceService.LoadRulesAsync();
        //                        var lotSizeFilter = tradeRules.Symbols
        //                            .Where(p => p.SymbolName == tradeSymbol)
        //                            .SelectMany(p => p.Filters)
        //                            .Where(p => p.FilterType == "LOT_SIZE")
        //                            .FirstOrDefault();

        //                        tradeQty = FixBinanceTradeLotSizeIncludeTradeFee(tradeQty, 0, lotSizeFilter);
        //                        var tradeInserted = false;
        //                        try
        //                        {
        //                            var binanceTradeResult = await _binanceService.PostNewOrderAsync(tradeSymbol, tradeQty, 0, BinanceOrderSide.SELL, BinanceOrderType.MARKET, BinanceTimeInForce.GTC).ConfigureAwait(false);
        //                            if (binanceTradeResult == null || binanceTradeResult.StatusEnum.HasFlag(BinanceTradeStatus.REJECTED | BinanceTradeStatus.CANCELED | BinanceTradeStatus.VOIDED | BinanceTradeStatus.EXPIRED))
        //                            {
        //                                tradeResult = false;
        //                            }
        //                            else if (binanceTradeResult != null && binanceTradeResult.StatusEnum.HasFlag(BinanceTradeStatus.FILLED | BinanceTradeStatus.PARTIALLY_FILLED | BinanceTradeStatus.NEW))
        //                            {
        //                                var binanceTrade = new Trade()
        //                                {
        //                                    TradingPlatform = TradingPlatform.Binance,
        //                                    ApiTradeId = binanceTradeResult.ClientOrderId,
        //                                    ExchangeOrderId = order.Id,
        //                                    TradingFee = binanceTradeResult.Fills.Select(p => p.Commission).DefaultIfEmpty(0).Sum(),
        //                                    ToCryptoType = ECurrencyType.Tether,
        //                                    FromCryptoType = ECurrencyType.Bitcoin,
        //                                    TradingFeeType = order.Exchange.ToCurrency.Type,
        //                                    FromAmount = tradeQty,
        //                                    ToAmount = binanceTradeResult.Fills.Select(p => (p.Price * p.Qty)).DefaultIfEmpty(0).Sum(),
        //                                    TradeStatus = binanceTradeResult.StatusEnum == BinanceTradeStatus.FILLED ? TradeStatus.Completed : TradeStatus.Completed,
        //                                };
        //                                order.Errors = null;
        //                                order.OrderStatus = OrderStatus.Processing;
        //                                await _exchangeOrderService.UpdateAsync(order);
        //                                await _tradeService.InsertAsync(binanceTrade);
        //                                tradeInserted = true;
        //                                tradeResult = true;
        //                                toAmount = binanceTrade.ToAmount;
        //                            }
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            ex.LogError();
        //                        }
        //                        if (!tradeInserted)
        //                        {
        //                            var binanceTrade = new Trade()
        //                            {
        //                                TradingPlatform = TradingPlatform.Binance,
        //                                ExchangeOrderId = order.Id,
        //                                ToCryptoType = ECurrencyType.Tether,
        //                                FromCryptoType = ECurrencyType.Bitcoin,
        //                                TradingFeeType = order.Exchange.ToCurrency.Type,
        //                                FromAmount = tradeQty,
        //                                TradeStatus = TradeStatus.Queue
        //                            };
        //                            order.Errors = null;
        //                            order.OrderStatus = OrderStatus.Processing;
        //                            await _exchangeOrderService.UpdateAsync(order);
        //                            await _tradeService.InsertAsync(binanceTrade);
        //                            tradeInserted = true;
        //                            tradeResult = false;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        var queueTrades = order.OrderTrades.Where(p => p.TradeStatus == TradeStatus.Queue).ToList();
        //                        if (queueTrades != null && queueTrades.Count == 1)
        //                        {
        //                            var queue = queueTrades.FirstOrDefault();
        //                            var symbol = queue.FromCryptoType.ToBinanceAsset() + queue.ToCryptoType.ToBinanceAsset();
        //                            tradeQty = queue.FromAmount;
        //                            var binanceTradeResult = await _binanceService.PostNewOrderAsync(symbol, tradeQty, 0, BinanceOrderSide.SELL, BinanceOrderType.MARKET, BinanceTimeInForce.GTC).ConfigureAwait(false);
        //                            if (!binanceTradeResult.StatusEnum.HasFlag(BinanceTradeStatus.REJECTED | BinanceTradeStatus.CANCELED | BinanceTradeStatus.VOIDED | BinanceTradeStatus.EXPIRED))
        //                            {
        //                                queue.TradingPlatform = TradingPlatform.Binance;
        //                                queue.ApiTradeId = binanceTradeResult.ClientOrderId;
        //                                queue.ExchangeOrderId = order.Id;
        //                                queue.TradingFee = binanceTradeResult.Fills.Select(p => p.Commission).DefaultIfEmpty(0).Sum();
        //                                queue.ToCryptoType = ECurrencyType.Tether;
        //                                queue.FromCryptoType = ECurrencyType.Bitcoin;
        //                                queue.TradingFeeType = order.Exchange.ToCurrency.Type;
        //                                queue.FromAmount = tradeQty;
        //                                queue.ToAmount = binanceTradeResult.Fills.Select(p => (p.Price * p.Qty)).DefaultIfEmpty(0).Sum();
        //                                queue.TradeStatus = binanceTradeResult.StatusEnum == BinanceTradeStatus.FILLED ? TradeStatus.Completed : TradeStatus.Completed;

        //                                await _tradeService.UpdateAsync(queue);

        //                                order.Errors = null;
        //                                order.OrderStatus = OrderStatus.Processing;
        //                                await _exchangeOrderService.UpdateAsync(order);

        //                                toAmount = queue.ToAmount;
        //                            }
        //                            tradeResult = true;
        //                        }
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    ex.LogError();
        //                    tradeResult = false;
        //                }
        //            }
        //            else
        //            {
        //                if (order.OrderTrades.Count == 0)
        //                {
        //                    tradeResult = true;
        //                    order.OrderStatus = OrderStatus.Processing;
        //                    await _exchangeOrderService.UpdateAsync(order);
        //                    var asset = order.Exchange.FromCurrency.Type.ToBinanceAsset();
        //                    var tradeSymbol = $"{asset}USDT";
        //                    var binancePrices = await _binanceService.GetAllPricesChange24HAsync();
        //                    var symbols = binancePrices.Select(p => p.Symbol).ToList();
        //                    if (symbols.Contains(tradeSymbol))
        //                    {
        //                        var tradeRules = await _binanceService.LoadRulesAsync();
        //                        var lotSizeFilter = tradeRules.Symbols
        //                            .Where(p => p.SymbolName == tradeSymbol)
        //                            .SelectMany(p => p.Filters)
        //                            .Where(p => p.FilterType == "LOT_SIZE")
        //                            .FirstOrDefault();

        //                        tradeQty = FixBinanceTradeLotSizeIncludeTradeFee(tradeQty, 0, lotSizeFilter);
        //                        var binanceTrade = new Trade()
        //                        {
        //                            TradingPlatform = TradingPlatform.Binance,
        //                            ExchangeOrderId = order.Id,
        //                            ToCryptoType = ECurrencyType.Tether,
        //                            FromCryptoType = order.Exchange.FromCurrency.Type,
        //                            TradingFeeType = order.Exchange.ToCurrency.Type,
        //                            FromAmount = tradeQty,
        //                            TradeStatus = TradeStatus.Queue
        //                        };
        //                        await _tradeService.InsertAsync(binanceTrade);
        //                    }
        //                    else
        //                    {
        //                        tradeSymbol = $"{asset}BTC";
        //                        var tradeRules = await _binanceService.LoadRulesAsync();
        //                        var lotSizeFilter = tradeRules.Symbols
        //                            .Where(p => p.SymbolName == tradeSymbol)
        //                            .SelectMany(p => p.Filters)
        //                            .Where(p => p.FilterType == "LOT_SIZE")
        //                            .FirstOrDefault();

        //                        tradeQty = FixBinanceTradeLotSizeIncludeTradeFee(tradeQty, 0, lotSizeFilter);

        //                        var firstTrade = new Trade()
        //                        {
        //                            TradingPlatform = TradingPlatform.Binance,
        //                            ExchangeOrderId = order.Id,
        //                            ToCryptoType = ECurrencyType.Bitcoin,
        //                            FromCryptoType = order.Exchange.FromCurrency.Type,
        //                            TradingFeeType = order.Exchange.ToCurrency.Type,
        //                            FromAmount = tradeQty,
        //                            TradeStatus = TradeStatus.Queue
        //                        };
        //                        await _tradeService.InsertAsync(firstTrade);

        //                        var secondTrade = new Trade()
        //                        {
        //                            TradingPlatform = TradingPlatform.Binance,
        //                            ExchangeOrderId = order.Id,
        //                            ToCryptoType = ECurrencyType.Tether,
        //                            FromCryptoType = ECurrencyType.Bitcoin,
        //                            TradingFeeType = order.Exchange.ToCurrency.Type,
        //                            TradeStatus = TradeStatus.Queue
        //                        };
        //                        await _tradeService.InsertAsync(secondTrade);
        //                    }
        //                }
        //                else if (order.OrderTrades.Count == 1)
        //                {
        //                    tradeResult = true;
        //                }
        //                else if (order.OrderTrades.Count == 2)
        //                {
        //                    tradeResult = true;
        //                    // ToDo: Do binance orders

        //                    //var firstTrade = order.OrderTrades.Where(p => p.ToCryptoType == ECurrencyType.Bitcoin).FirstOrDefault();
        //                    //var secondTrade = order.OrderTrades.Where(p => p.FromCryptoType == ECurrencyType.Bitcoin).FirstOrDefault();
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ex.LogError();
        //        }
        //    }
        //    else
        //    {
        //        tradeResult = true;
        //    }
        //    return (tradeResult, tradeQty, toAmount);
        //}

        public async Task<TradeOrderResult> TryTradeAgain(ExchangeOrder order)
        {

            if (!order.Exchange.ToCurrency.IsFiat)
            {
                return await BuyCryptoTradeOrderAsync(order);
            }
            else
            {
                var tradeQty = await SellCryptoTradeOrderAsync(order);
                return new TradeOrderResult()
                {
                    BinanceResult = true,
                    Trades = new List<Trade>()
                };
            }
        }

        #region Helpers
        private async Task<decimal> FixBinanceMinNotionalAsync(string symbol, decimal tradeQty, decimal step, BinanceFilter minNotionalFilter)
        {
            // minNotionalFilter.MinNotional;
            var price = 0m;
            if (minNotionalFilter.AveragePriceMins == 0)
            {
                var bPrice = await _binanceService.GetPriceAsync(symbol);
                price = bPrice.Price;
            }
            else
            {
                var averagePrice = await _binanceService.GetAveragePriceAsync(symbol);
                price = averagePrice.Price;
            }

            var notional = price * tradeQty;
            while (notional <= minNotionalFilter.MinNotional)
            {
                tradeQty += step;
                notional = price * tradeQty;
            }
            return tradeQty;
        }

        private decimal FixBinanceTradeLotSizeIncludeTradeFee(decimal receiveAmount, decimal gatewayFee, BinanceFilter lotSizeFilter)
        {
            var result = receiveAmount + gatewayFee;
            result += ((result / 100) * 0.25M); // %0.25 Trade fee
            result = result.Round();

            if (lotSizeFilter != null)
            {
                //Fix step size
                var remind = result % lotSizeFilter.StepSize;
                if (remind != 0)
                    result += lotSizeFilter.StepSize - remind;
            }
            return result;
        }
        #endregion
    }

    public class TradeOrderResult
    {
        public bool BinanceResult { get; set; }
        public List<Trade> Trades { get; set; }
    }
}
