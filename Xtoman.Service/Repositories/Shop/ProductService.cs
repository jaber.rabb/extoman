﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class ProductService : RepositoryBase<Product>, IProductService
    {
        public ProductService(IUnitOfWork context) : base(context)
        {
        }
    }
}
