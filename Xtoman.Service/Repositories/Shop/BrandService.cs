﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class BrandService : RepositoryBase<Brand>, IBrandService
    {
        public BrandService(IUnitOfWork context) : base(context)
        {
        }

        public async Task<bool> NameIsUniqueAsync(string name, string alternativeName)
        {
            return !await TableNoTracking.Where(p => p.Name == name || p.AlternativeName == alternativeName).AnyAsync();
        }

        public async Task<bool> NameIsUniqueAsync(string name, string alternativeName, int id)
        {
            return !await TableNoTracking.Where(p => p.Id != id).Where(p => p.Name == name || p.AlternativeName == alternativeName).AnyAsync();
        }
    }
}
