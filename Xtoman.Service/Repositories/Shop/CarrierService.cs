﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class CarrierService : RepositoryBase<Carrier>, ICarrierService
    {
        public CarrierService(IUnitOfWork context) : base(context)
        {
        }
    }
}
