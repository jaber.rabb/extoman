﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class ShopAttributeService : RepositoryBase<ShopAttribute>, IShopAttributeService
    {
        public ShopAttributeService(IUnitOfWork context) : base(context)
        {
        }
    }
}
