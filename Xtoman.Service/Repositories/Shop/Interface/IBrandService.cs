﻿using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IBrandService : IRepository<Brand>
    {
        Task<bool> NameIsUniqueAsync(string name, string alternativeName);
        Task<bool> NameIsUniqueAsync(string name, string alternativeName, int id);
    }
}
