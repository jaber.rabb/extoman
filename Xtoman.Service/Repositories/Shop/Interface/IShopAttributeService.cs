﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IShopAttributeService : IRepository<ShopAttribute>
    {
    }
}
