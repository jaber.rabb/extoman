﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class SpecificationService : RepositoryBase<Specification>, ISpecificationService
    {
        public SpecificationService(IUnitOfWork context) : base(context)
        {
        }
    }
}
