﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class ShopAttributeGroupService : RepositoryBase<ShopAttributeGroup>, IShopAttributeGroupService
    {
        public ShopAttributeGroupService(IUnitOfWork context) : base(context)
        {
        }
    }
}
