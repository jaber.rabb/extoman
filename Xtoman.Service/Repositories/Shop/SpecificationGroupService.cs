﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class SpecificationGroupService : RepositoryBase<SpecificationGroup>, ISpecificationGroupService
    {
        public SpecificationGroupService(IUnitOfWork context) : base(context)
        {
        }
    }
}
