﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class SupplierService : RepositoryBase<Supplier>, ISupplierService
    {
        public SupplierService(IUnitOfWork context) : base(context)
        {
        }
    }
}
