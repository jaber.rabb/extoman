﻿using System.Collections.Generic;
using System.Linq;
using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service
{
    public class AppRoleService : CacheRepositoryBase<AppRole, AppRoleForCache>, IAppRoleService
    {
        public AppRoleService(IUnitOfWork context, ICacheManager cacheManager, CacheSettings cacheSettings) : base(context, cacheManager, cacheSettings)
        {
        }

        public List<AppRole> GetAllCached()
        {
            var cachedEntity = GetAllCached(p => new AppRoleForCache
            {
                Id = p.Id,
                Name = p.Name,
                DisplayName = p.DisplayName,
                //Permissions = p.Permissions.ToList()
            });
            var result = new List<AppRole>();
            foreach (var item in cachedEntity)
            {
                var appRole = new AppRole()
                {
                    DisplayName = item.DisplayName,
                    Id = item.Id,
                    Name = item.Name
                };
                foreach (var permission in item.Permissions)
                    appRole.Permissions.Add(permission);

                result.Add(appRole);
            }

            return result;
        }
    }
}
