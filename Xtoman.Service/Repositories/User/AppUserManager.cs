﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataProtection;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Service.Managers;

namespace Xtoman.Service
{
    public class AppUserManager
            : UserManager<AppUser, int>, IAppUserManager
    {
        private readonly IDataProtectionProvider _dataProtectionProvider;
        private readonly IAppRoleManager _roleManager;
        private readonly IUserStore<AppUser, int> _userStore;
        private readonly IDbSet<AppUser> _users;
        private readonly Lazy<Func<IIdentity>> _identity;
        private AppTotpSecurityStampBasedTokenProvider smsTokenProvider = new AppTotpSecurityStampBasedTokenProvider();
        //private readonly IWorkflowMessageService _workflowMessageService;
        private AppUser _user;

        //private IIdentity GetUserIdentity()
        //{
        //    if (HttpContext.Current != null && HttpContext.Current.User != null)
        //    {
        //        return HttpContext.Current.User.Identity;
        //    }
        //    return ClaimsPrincipal.Current?.Identity;
        //}

        public AppUserManager(IUserStore<AppUser, int> uerStore,
            IUnitOfWork uow,
            Lazy<Func<IIdentity>> identity,
            IAppRoleManager roleManager,
            IDataProtectionProvider dataProtectionProvider,
            IIdentityMessageService smsService,
            IIdentityMessageService emailService/*, IWorkflowMessageService workflowMessageService*/)
            : base(uerStore)
        {
            _userStore = uerStore;
            _identity = identity;//GetUserIdentity();
            _users = uow.Set<AppUser>();
            _roleManager = roleManager;
            _dataProtectionProvider = dataProtectionProvider;
            //_workflowMessageService = workflowMessageService;
            SmsService = smsService;
            EmailService = emailService;

            CreateAppUserManager();
        }

        public override async Task<string> GenerateChangePhoneNumberTokenAsync(int userId, string phoneNumber)
        {
            var user = await base.FindByIdAsync(userId);
            var token = await smsTokenProvider.GenerateAsync("Confirm PhoneNumber", this, user);
            return token;
        }

        public override async Task<bool> VerifyChangePhoneNumberTokenAsync(int userId, string token, string phoneNumber)
        {
            var user = await base.FindByIdAsync(userId);
            if (user.PhoneNumber == phoneNumber)
            {
                var valid = await smsTokenProvider.ValidateAsync("Confirm PhoneNumber", token, this, user);
                user.PhoneNumberConfirmed = valid;
                var result = await UpdateAsync(user);
                if (result.Succeeded)
                    return valid;
            }
            return false;
        }

        public async Task<string> GenerateSMSPasswordResetToken(int userId)
        {
            var user = await base.FindByIdAsync(userId);
            var token = await smsTokenProvider.GenerateAsync("Reset Password", this, user);
            return token;
        }

        public async Task<IdentityResult> SMSPasswordResetAsync(int userId, string token, string newPassword)
        {
            var user = await base.FindByIdAsync(userId);
            var valid = await smsTokenProvider.ValidateAsync("Reset Password", token, this, user);
            if (valid)
            {
                var passwordStore = Store as IUserPasswordStore<AppUser, int>;

                var result = await UpdatePassword(passwordStore, user, newPassword);
                if (!result.Succeeded)
                {
                    return result;
                }
                return await UpdateAsync(user);
            }
            else
            {
                return IdentityResult.Failed("InvalidToken");
            }
        }

        public async Task<bool> GetGoogleAuthenticationEnabledAsync(int userId)
        {
            var user = await FindByIdAsync(userId);
            return (user?.IsGoogleAuthenticatorEnabled ?? false) && (user?.GoogleAuthenticatorSecretKey.HasValue() ?? false);
        }

        public async Task<AppUser> FindByEmailOrUsernameOrPhoneAsync(string emailOrUsernameOrPhone)
        {
            return await FindByNameAsync(emailOrUsernameOrPhone) ?? await FindByEmailAsync(emailOrUsernameOrPhone) ?? await FindByPhoneAsync(emailOrUsernameOrPhone);
        }

        public async Task<AppUser> FindByEmailOrUsernameOrPhoneAsync(string emailOrUsernameOrPhone, string password)
        {
            var user = await FindByEmailOrUsernameOrPhoneAsync(emailOrUsernameOrPhone);
            if (await CheckPasswordAsync(user, password))
                return user;

            return null;
        }

        public override async Task<IdentityResult> CreateAsync(AppUser user)
        {
            if (!string.IsNullOrEmpty(user.Email))
                if (await FindByEmailAsync(user.Email) != null)
                    return IdentityResult.Failed("This email address registered before!");
            if (!string.IsNullOrEmpty(user.PhoneNumber))
                if (await FindByPhoneAsync(user.PhoneNumber) != null)
                    return IdentityResult.Failed("This phone number registered before!");
            return await base.CreateAsync(user);
        }
        public override async Task<IdentityResult> CreateAsync(AppUser user, string password)
        {
            if (!string.IsNullOrEmpty(user.Email))
                if (await FindByEmailAsync(user.Email) != null)
                    return IdentityResult.Failed("This email address registered before!");
            if (!string.IsNullOrEmpty(user.PhoneNumber))
                if (await FindByPhoneAsync(user.PhoneNumber) != null)
                    return IdentityResult.Failed("This phone number registered before!");
            return await base.CreateAsync(user, password);
        }

        public AppUser FindById(int userId)
        {
            return _users.Find(userId);
        }

        public async Task<AppUser> FindByPhoneAsync(string phone)
        {
            return await _users.SingleOrDefaultAsync(x => x.PhoneNumber == phone);
        }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(AppUser appUser)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await CreateIdentityAsync(appUser, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            userIdentity.AddClaim(new Claim("FullName", appUser.FullName));
            return userIdentity;
        }

        public Task<List<AppUser>> GetAllUsersAsync()
        {
            return Users.ToListAsync();
        }

        public AppUser GetCurrentUser()
        {
            return _user ?? (_user = FindById(GetCurrentUserId()));
        }

        public async Task<AppUser> GetCurrentUserAsync()
        {
            return _user ?? (_user = await FindByIdAsync(GetCurrentUserId()));
        }

        public int GetCurrentUserId()
        {
            return _identity.Value().GetUserId<int>();
        }

        public async Task<bool> HasPassword(int userId)
        {
            var user = await FindByIdAsync(userId);
            return user?.PasswordHash != null;
        }

        public async Task<bool> HasPhoneNumber(int userId)
        {
            var user = await FindByIdAsync(userId);
            return user?.PhoneNumber != null;
        }

        public Func<CookieValidateIdentityContext, Task> OnValidateIdentity() => SecurityStampValidator.OnValidateIdentity<AppUserManager, AppUser, int>(
                         validateInterval: TimeSpan.FromMinutes(15), // RememberMe -> 0 solution or 15 solution
                         regenerateIdentityCallback: GenerateUserIdentityAsync,
                         getUserIdCallback: (id) =>
                         {
                             try
                             {
                                 return id.GetUserId<int>();
                             }
                             catch (Exception)
                             {
                             }
                             return 0;
                         });

        private void CreateAppUserManager()
        {

            // Configure validation logic for usernames
            this.UserValidator = new UserValidator<AppUser, int>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = false
            };

            // Configure validation logic for passwords
            this.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false
            };

            // Configure user lockout defaults
            this.UserLockoutEnabledByDefault = true;
            this.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            this.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug in here.
            this.RegisterTwoFactorProvider("PhoneCode", new PhoneNumberTokenProvider<AppUser, int>
            {
                MessageFormat = "Your security code is: {0}"
            });
            this.RegisterTwoFactorProvider("EmailCode", new EmailTokenProvider<AppUser, int>
            {
                Subject = "SecurityCode",
                BodyFormat = "Your security code is {0}"
            });
            this.RegisterTwoFactorProvider("GoogleAuthenticator", new GoogleAuthenticatorTokenProvider());

            if (_dataProtectionProvider != null)
            {
                var dataProtector = _dataProtectionProvider.Create("ASP.NET Identity"); ;
                this.UserTokenProvider = new DataProtectorTokenProvider<AppUser, int>(dataProtector);
            }
        }

        private async Task<ClaimsIdentity> GenerateUserIdentityAsync(AppUserManager manager, AppUser AppUser)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(AppUser, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public async Task<AppUser> FindByEmailOrPhoneAsync(string emailOrPhone)
        {
            return await FindByEmailAsync(emailOrPhone) ?? await FindByPhoneAsync(emailOrPhone);
        }

        //public async Task<Tuple<AppUser, bool>> AddIfNotExists(string email, string firstName, string lastName, string mobile, string phone, bool gender = true)
        //{
        //    var user = await FindByPhoneAsync(mobile);
        //    if (user != null)
        //    {
        //        return new Tuple<AppUser, bool>(user, false);
        //    }
        //    if (email != null)
        //        user = await FindByEmailAsync(email);
        //    if (user != null)
        //    {
        //        return new Tuple<AppUser, bool>(user, false);
        //    }
        //    user = new AppUser
        //    {
        //        UserName = email ?? mobile,
        //        Email = email,
        //        FirstName = firstName,
        //        LastName = lastName,
        //        Gender = gender,
        //        PhoneNumber = mobile,
        //    };
        //    var password = new Random().Next(100000, 999999).ToString();
        //    await CreateAsync(user, password);
        //    //_workflowMessageService.SendPasswordMessage(user, password);
        //    return new Tuple<AppUser, bool>(user, true);
        //}

        //#region LogicalRemove
        //public async Task<bool> LogicalRemove(long id)
        //{
        //    _unitOfWork.EnableFiltering(UserFilters.NotSystemAccountList);
        //    var result = await _users.Where(a => a.Id == id).UpdateAsync(a => new User { IsDeleted = true });
        //    return result > 0;
        //}
        //#endregion

        //#region Validations
        //public bool CheckUserNameExist(string userName, long? id)
        //{
        //    return id == null
        //        ? _users.Any(a => a.UserName == userName.ToLower())
        //        : _users.Any(a => a.UserName == userName.ToLower() && a.Id != id.Value);
        //}

        //public bool CheckEmailExist(string email, int? id = null)
        //{
        //    //email = email.FixGmailDots();
        //    return _users.Any(a => a.Email == email.ToLower() && a.Id != id);
        //}


        //public bool CheckGooglePlusIdExist(string googlePlusId, int? id = null)
        //{
        //    return _users.Any(a => a.GooglePlusId == googlePlusId && a.Id != id);
        //}

        //public bool CheckFacebookIdExist(string faceBookId, int? id = null)
        //{
        //    return _users.Any(a => a.FaceBookId == faceBookId && a.Id != id);
        //}

        //public bool CheckPhoneNumberExist(string phoneNumber, int? id = null)
        //{
        //    return _users.Any(a => a.PhoneNumber == phoneNumber && a.Id != id);
        //}
        //#endregion
    }

    //public static class IdentityExtention
    //{
    //    public static string GetLocolizedErrors(this IdentityResult result)
    //    {
    //        var list = new List<string>();
    //        foreach (var err in result.Errors)
    //        {
    //            string error;
    //            switch (err)
    //            {
    //                case "An unknown failure has occured.":
    //                    error = "خطا";
    //                    break;
    //                case "Incorrect password.":
    //                    error = "رمزعبور اشتباه است.";
    //                    break;
    //                case "Passwords must have at least one digit ('0'-'9').":
    //                    error = "رمزعبور باید شامل عدد باشد.";
    //                    break;
    //                case "Passwords must have at least one lowercase ('a'-'z').":
    //                    error = "رمزعبور باید شامل حروف کوچک باشد.";
    //                    break;
    //                case "Passwords must have at least one non letter or digit character.":
    //                    error = "رمزعبور باید شامل یک کاراکتر باشد.";
    //                    break;
    //                case "Passwords must have at least one uppercase ('A'-'Z').":
    //                    error = "رمزعبور باید شامل حروف بزرگ باشد.";
    //                    break;
    //                case "User already in role.":
    //                    error = "کاربر در این نقش می باشد.";
    //                    break;
    //                case "User already has a password set.":
    //                    error = "کاربر دارای رمزعبور می باشد.";
    //                    break;
    //                case "User is not in role.":
    //                    error = "کاربر در این نقش نمی باشد.";
    //                    break;
    //                case "Invalid token.":
    //                    error = "توکن نامعتبر است.";
    //                    break;
    //                default:
    //                    error = Utility.CommonUtility.ReplaceByFirstMatch("Email '(.+)' is already taken.", err, "ایمیل '{0}' موجود است.");
    //                    error = Utility.CommonUtility.ReplaceByFirstMatch("Name (.+) is already taken.", err, "نام {0} موجود است.");
    //                    error = Utility.CommonUtility.ReplaceByFirstMatch("Email '(.+)' is invalid.", err, "ایمیل '{0}' نامعتبر است.");
    //                    error = Utility.CommonUtility.ReplaceByFirstMatch("User name (.+) is invalid, can only contain letters or digits.", err, "نام کاربری {0} نامعتبر است، نام کاربری تنها می تواند شامل عدد و حروف باشد.");
    //                    error = Utility.CommonUtility.ReplaceByFirstMatch("Passwords must be at least (.+) characters.", err, "رمزعبور باید حداقل شامل {0} کاراکتر باشد.");
    //                    error = Utility.CommonUtility.ReplaceByFirstMatch("(.+) cannot be null or empty.", err, "نمیتواند خالی باشد. {0}");
    //                    error = Utility.CommonUtility.ReplaceByFirstMatch("Role {0} does not exist.", err, "نقش {0} وجود ندارد.");
    //                    error = Utility.CommonUtility.ReplaceByFirstMatch("User {0} does not exist.", err, "کاربر {0} وجود ندارد.");
    //                    break;
    //            }
    //            list.Add(error);
    //        }
    //        return string.Join("</br>", list);
    //    }
    //}
}
