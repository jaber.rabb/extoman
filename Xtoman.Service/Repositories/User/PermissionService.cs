﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class PermissionService : RepositoryBase<Permission>, IPermissionService
    {
        public PermissionService(IUnitOfWork context) : base(context)
        {
        }
    }
}
