﻿using Microsoft.AspNet.Identity.EntityFramework;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class AppUserStore :
        UserStore<AppUser, AppRole, int, AppUserLogin, AppUserRole, AppUserClaim>,
        IAppUserStore
        {
        //private readonly IDbSet<AppUser> _myUserStore;
        public AppUserStore(AppDbContext context)
            : base(context)
        {
            //_myUserStore = context.Set<AppUser>();
        }

        //public override Task<AppUser> FindByIdAsync(int userId)
        //{
        //   return Task.FromResult(_myUserStore.Find(userId));
        //}
    }
}
