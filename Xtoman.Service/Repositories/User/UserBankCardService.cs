﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class UserBankCardService : RepositoryBase<UserBankCard>, IUserBankCardService
    {
        public UserBankCardService(IUnitOfWork context) : base(context)
        {
        }

        public async Task<bool> IsUniqueAsync(int userId, string cardNumber)
        {
            return !await TableNoTracking.Where(p => p.UserId == userId && p.CardNumber == cardNumber).AnyAsync();
        }
    }
}
