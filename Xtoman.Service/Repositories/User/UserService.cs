﻿using Microsoft.AspNet.Identity;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Utility;

namespace Xtoman.Service
{
    public class UserService : RepositoryBase<AppUser>, IUserService
    {
        private readonly IAppUserManager _userManager;
        private readonly IAppSignInManager _signInManager;
        private readonly IIdentity _identity;
        public UserService(IUnitOfWork uow, IIdentity identity,
            IAppSignInManager signInManager,
            IAppUserManager userManager) : base(uow)
        {
            _identity = identity;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public async Task<string> GetCurrentUserEmailAsync()
        {
            if (_identity.IsAuthenticated)
                return await _userManager.GetEmailAsync(_identity.GetUserId<int>());
            return "";
        }

        public async Task<bool> EmailIsUniqueAsync(string email)
        {
            if (!email.HasValue())
                return false;
            return !await TableNoTracking.AnyAsync(p => p.Email == email);
        }

        public List<AppUser> GetAllUsers()
        {
            return TableNoTracking.ToList();
        }

        public async Task<bool> UserExistByUsernameAsync(string username)
        {
            return await TableNoTracking.AnyAsync(p => p.UserName.Equals(username));
        }

        public async Task<int> GetUserIdByUsernameAsync(string username)
        {
            return await TableNoTracking.Where(p => p.UserName.Equals(username)).Select(p => p.Id).SingleAsync();
        }

        public async Task<bool> UsernameIsUniqueAsync(string userName)
        {
            if (!userName.HasValue())
                return false;
            return !await TableNoTracking.AnyAsync(p => p.UserName == userName);
        }

        public async Task<bool> PhoneIsUniqueAsync(string phonenumber)
        {
            if (!phonenumber.HasValue())
                return false;
            return !await TableNoTracking.AnyAsync(p => p.PhoneNumber == phonenumber);
        }

        public async Task<string> GetUserNameByEmailAsync(string email)
        {
            return await TableNoTracking.Where(p => p.Email.Equals(email)).Select(p => p.UserName).SingleAsync();
        }

        public async Task<AppUser> GetByIdAsNoTrackingAsync(int userId)
        {
            return await TableNoTracking.Where(p => p.Id == userId).SingleAsync();
        }

        public async Task<bool> EmailIsUniqueAsync(string email, int id)
        {
            if (!email.HasValue())
                return false;
            return !await TableNoTracking.AnyAsync(p => p.Email == email && p.Id != id);
        }

        public async Task<bool> PhoneIsUniqueAsync(string phonenumber, int id)
        {
            return !await TableNoTracking.AnyAsync(p => p.PhoneNumber == phonenumber && p.Id != id);
        }

        public async Task<int> GetUserIdByUsernameOrEmailOrPhoneAsync(string usernameOrEmailOrPhone)
        {
            return await TableNoTracking.Where(p => p.UserName == usernameOrEmailOrPhone || p.Email == usernameOrEmailOrPhone || p.PhoneNumber == usernameOrEmailOrPhone).Select(p => p.Id).SingleAsync();
        }

        public async Task<bool> EmailOrPhoneIsUniqueAsync(string emailOrPhone)
        {
            if (!emailOrPhone.HasValue())
                return false;

            if (emailOrPhone.IsEmail())
                return !await TableNoTracking.AnyAsync(p => p.Email == emailOrPhone);
            else
                return !await TableNoTracking.AnyAsync(p => p.PhoneNumber == emailOrPhone);
        }

        public async Task<(bool isVerified, bool fiatVerified)> IsVerifiedAsync(int userId)
        {
            var user = await TableNoTracking.Where(p => p.Id == userId)
                .Include(p => p.UserBankCards)
                .Select(p => new {
                    p.IdentityVerificationVerifyDate,
                    p.EmailConfirmed,
                    p.PhoneNumberConfirmed,
                    p.TelephoneConfirmationStatus,
                    p.Telephone,
                    p.UserBankCards
                })
                .SingleAsync();
            var isVerified = user.IdentityVerificationVerifyDate != null
                          && user.IdentityVerificationVerifyDate <= DateTime.UtcNow
                          && user.EmailConfirmed
                          && user.PhoneNumberConfirmed
                          && user.TelephoneConfirmationStatus == Domain.Models.VerificationStatus.Confirmed;

            var fiatVerified = user.UserBankCards.Any(x => x.VerificationStatus == Domain.Models.VerificationStatus.Confirmed);
            return (isVerified, fiatVerified);
        }

        public async Task<VerificationStatus> VerificationStatus(int userId)
        {
            return await TableNoTracking.Where(p => p.Id == userId)
                .Select(p => p.IdentityVerificationStatus)
                .SingleAsync();
        }

        public async Task<bool> ReferrerExistAsync(string r)
        {
            if (r.HasValue() && Regex.Matches(r, "([xX])(?=.*?[0-9].*)(.*)").Count > 0)
            {
                var id = r.RemoveLeft(1).ToInt();
                return await TableNoTracking.AnyAsync(p => p.Id == id);
            }
            return false;
        }

        public async Task<bool> IsBuyLimitedAsync(int userId)
        {
            return await TableNoTracking.Where(p => p.Id == userId).Select(p => p.IsBuyLimited).FirstOrDefaultAsync();
        }
    }
}
