using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IAppRoleManager : IDisposable
    {
        /// <summary>
        /// Used to validate roles before persisting changes
        /// </summary>
        IIdentityValidator<AppRole> RoleValidator { get; set; }

        /// <summary>
        /// Create a role
        /// </summary>
        /// <param name="role"/>
        /// <returns/>
        Task<IdentityResult> CreateAsync(AppRole role);

        /// <summary>
        /// Update an existing role
        /// </summary>
        /// <param name="role"/>
        /// <returns/>
        Task<IdentityResult> UpdateAsync(AppRole role);

        /// <summary>
        /// Delete a role
        /// </summary>
        /// <param name="role"/>
        /// <returns/>
        Task<IdentityResult> DeleteAsync(AppRole role);

        /// <summary>
        /// Returns true if the role exists
        /// </summary>
        /// <param name="roleName"/>
        /// <returns/>
        Task<bool> RoleExistsAsync(string roleName);

        /// <summary>
        /// Find a role by id
        /// </summary>
        /// <param name="roleId"/>
        /// <returns/>
        Task<AppRole> FindByIdAsync(int roleId);

        /// <summary>
        /// Find a role by name
        /// </summary>
        /// <param name="roleName"/>
        /// <returns/>
        Task<AppRole> FindByNameAsync(string roleName);

        // Our new custom methods

        AppRole FindRoleByName(string roleName);
        IdentityResult CreateRole(AppRole role);
        List<AppUser> GetUsersInRoleById(int id);
        Task<List<AppUser>> GetUsersInRoleByIdAsync(int id);
        IList<AppUserRole> GetAppUsersInRole(string roleName);
        //IList<AppUser> GetAppUsersInRole(string roleName);
        IList<AppRole> FindUserRoles(int userId);
        string[] GetRolesForUser(int userId);
        bool IsUserInRole(int userId, string roleName);
        Task<List<AppRole>> GetAllAppRolesAsync();
    }
}