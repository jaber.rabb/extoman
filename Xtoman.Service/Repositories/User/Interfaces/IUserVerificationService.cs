﻿using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IUserVerificationService : IRepository<UserVerification>
    {
        Task ChangedAsync(int userId, UserVerificationType type, VerificationStatus status, string description = null, int? adminUserId = null, int? mediaId = null);
        Task AdminChangedEmailVerificationStatus(int adminUserId, int userId, bool confirmed, string description);
        Task AdminChangedMobileVerificationStatus(int adminUserId, int userId, bool confirmed, string description);
        Task UserConfirmedEmail(int userId);
        Task UserConfirmedMobile(int userId, bool confirmed);
    }
}
