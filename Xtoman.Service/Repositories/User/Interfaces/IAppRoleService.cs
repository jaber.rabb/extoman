﻿using System.Collections.Generic;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IAppRoleService : IRepository<AppRole>
    {
        List<AppRole> GetAllCached();
    }
}
