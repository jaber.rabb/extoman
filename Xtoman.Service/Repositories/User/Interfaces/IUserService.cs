﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IUserService : IRepository<AppUser>
    {
        Task<string> GetCurrentUserEmailAsync();
        Task<bool> EmailIsUniqueAsync(string email);
        List<AppUser> GetAllUsers();
        Task<bool> UsernameIsUniqueAsync(string userName);
        Task<bool> PhoneIsUniqueAsync(string phonenumber);
        Task<string> GetUserNameByEmailAsync(string email);
        Task<AppUser> GetByIdAsNoTrackingAsync(int userId);
        Task<bool> EmailIsUniqueAsync(string email, int id);
        Task<bool> PhoneIsUniqueAsync(string phonenumber, int id);
        Task<int> GetUserIdByUsernameOrEmailOrPhoneAsync(string usernameOrEmailOrPhone);
        Task<bool> EmailOrPhoneIsUniqueAsync(string emailOrPhone);
        //Task<bool> IsVerifiedAsync(int userId);
        Task<(bool isVerified, bool fiatVerified)> IsVerifiedAsync(int userId);
        Task<VerificationStatus> VerificationStatus(int userId);
        Task<bool> ReferrerExistAsync(string referrer);
        Task<bool> IsBuyLimitedAsync(int userId);
    }
}
