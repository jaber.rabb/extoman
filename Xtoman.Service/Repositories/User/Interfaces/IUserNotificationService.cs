﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IUserNotificationService : IRepository<UserNotification>
    {
        Task<List<UserNotification>> GetByUserIdAsync(int userId);
        Task<int> GetUserUnreadsCountAsync(int userId);
    }
}
