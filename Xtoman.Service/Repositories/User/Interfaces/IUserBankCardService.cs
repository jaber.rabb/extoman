﻿using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IUserBankCardService : IRepository<UserBankCard>
    {
        Task<bool> IsUniqueAsync(int userId, string cardNumber);
    }
}
