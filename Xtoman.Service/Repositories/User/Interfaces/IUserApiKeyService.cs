﻿using Xtoman.Data;
using Xtoman.Domain.Models.Api;

namespace Xtoman.Service
{
    public interface IUserApiKeyService : IRepository<UserApiKey>
    {
    }
}
