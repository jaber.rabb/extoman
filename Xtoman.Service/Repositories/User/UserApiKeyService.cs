﻿using Xtoman.Data;
using Xtoman.Domain.Models.Api;

namespace Xtoman.Service
{
    public class UserApiKeyService : RepositoryBase<UserApiKey>, IUserApiKeyService
    {
        public UserApiKeyService(IUnitOfWork context) : base(context)
        {

        }
    }
}
