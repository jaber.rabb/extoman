﻿using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class UserVerificationService : RepositoryBase<UserVerification>, IUserVerificationService
    {
        public UserVerificationService(IUnitOfWork context) : base(context)
        {
        }

        public async Task ChangedAsync(int userId, UserVerificationType type, VerificationStatus status, string description = null, int? adminUserId = null, int? mediaId = null)
        {
            await InsertAsync(new UserVerification()
            {
                Description = description,
                InsertUserId = adminUserId,
                UserId = userId,
                MediaId = mediaId,
                Status = status,
                Type = type
            });
        }

        #region By Admin
        public async Task AdminChangedEmailVerificationStatus(int adminUserId, int userId, bool confirmed, string description)
        {
            await ChangedAsync(userId, UserVerificationType.Email, confirmed ? VerificationStatus.Confirmed : VerificationStatus.NotConfirmed, description, adminUserId);
        }
        public async Task AdminChangedMobileVerificationStatus(int adminUserId, int userId, bool confirmed, string description)
        {
            await ChangedAsync(userId, UserVerificationType.Mobile, confirmed ? VerificationStatus.Confirmed : VerificationStatus.NotConfirmed, description, adminUserId);
        }
        #endregion

        #region ByUser
        public async Task UserConfirmedEmail(int userId)
        {
            await ChangedAsync(userId, UserVerificationType.Email, VerificationStatus.Confirmed);
        }
        public async Task UserConfirmedMobile(int userId, bool confirmed)
        {
            await ChangedAsync(userId, UserVerificationType.Mobile, VerificationStatus.Confirmed);
        }
        #endregion
    }
}
