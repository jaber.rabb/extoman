﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class AppRoleManager : RoleManager<AppRole, int>, IAppRoleManager
    {
        private readonly IUnitOfWork _uow;
        private readonly IRoleStore<AppRole, int> _roleStore;
        private readonly IDbSet<AppUser> _users;
        public AppRoleManager(IUnitOfWork uow, IRoleStore<AppRole, int> roleStore)
            : base(roleStore)
        {
            _uow = uow;
            _roleStore = roleStore;
            _users = _uow.Set<AppUser>();
        }

        public AppRole FindRoleByName(string roleName)
        {
            return this.FindByName(roleName); // RoleManagerExtensions
        }

        public IdentityResult CreateRole(AppRole role)
        {
            return this.Create(role); // RoleManagerExtensions
        }

        public async Task<List<AppUser>> GetUsersInRoleByIdAsync(int id)
        {
            var result = await this.Roles.AsNoTracking().Where(role => role.Id == id)
                 .SelectMany(role => role.Users)
                 .Select(p => p.User)
                 .ToListAsync();
            return result;
        }

        public List<AppUser> GetUsersInRoleById(int id)
        {
            var result = this.Roles.AsNoTracking().Where(role => role.Id == id)
                 .SelectMany(role => role.Users)
                 .Select(p => p.User)
                 .ToList();
            return result;
        }

        public IList<AppUserRole> GetAppUsersInRole(string roleName)
        {
            return this.Roles.Where(role => role.Name == roleName)
                             .SelectMany(role => role.Users)
                             .ToList();
            // = this.FindByName(roleName).Users
        }

        //public IList<AppUser> GetAppUsersInRole(string roleName)
        //{
        //    var roleUserIdsQuery = from role in this.Roles
        //                           where role.Name == roleName
        //                           from user in role.Users
        //                           select user.UserId;
        //    return _users.Where(AppUser => roleUserIdsQuery.Contains(AppUser.Id))
        //                 .ToList();
        //}

        public IList<AppRole> FindUserRoles(int userId)
        {
            var userRolesQuery = from role in this.Roles
                                 from user in role.Users
                                 where user.UserId == userId
                                 select role;

            return userRolesQuery.OrderBy(x => x.Name).ToList();
        }

        public string[] GetRolesForUser(int userId)
        {
            var roles = FindUserRoles(userId);
            if (roles == null || !roles.Any())
            {
                return new string[] { };
            }

            return roles.Select(x => x.Name).ToArray();
        }

        public bool IsUserInRole(int userId, string roleName)
        {
            var userRolesQuery = from role in this.Roles
                                 where role.Name == roleName
                                 from user in role.Users
                                 where user.UserId == userId
                                 select role;
            var userRole = userRolesQuery.FirstOrDefault();
            return userRole != null;
        }

        public async Task<List<AppRole>> GetAllAppRolesAsync()
        {
            return await this.Roles.Include(p => p.Permissions).ToListAsync();
        }

        /// <summary>
        /// Find a role by id
        /// </summary>
        /// <param name="roleId"/>
        /// <returns/>
        public override Task<AppRole> FindByIdAsync(int roleId)
        {
            return this.Roles.Include(p => p.Permissions).SingleAsync(p => p.Id == roleId);
        }
    }

}
