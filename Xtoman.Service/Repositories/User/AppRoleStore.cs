﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class AppRoleStore : IAppRoleStore
    {
        private readonly IRoleStore<AppRole, int> _roleStore;

        public AppRoleStore(IRoleStore<AppRole, int> roleStore)
        {
            _roleStore = roleStore;
        }
    }
}
