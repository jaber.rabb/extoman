﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class UserLoginHistoryService : RepositoryBase<UserLoginHistory>, IUserLoginHistoryService
    {
        public UserLoginHistoryService(IUnitOfWork context) : base(context)
        {
        }
    }
}
