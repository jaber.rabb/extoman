﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class UserNotifyAlertService : RepositoryBase<UserNotifyAlert>, IUserNotifyAlertService
    {
        public UserNotifyAlertService(IUnitOfWork context) : base(context)
        {
        }
    }
}
