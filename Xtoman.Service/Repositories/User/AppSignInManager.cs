﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using Xtoman.Domain;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class AppSignInManager : SignInManager<AppUser, int>, IAppSignInManager
    {
        private readonly AppUserManager _userManager;
        private readonly IAuthenticationManager _authenticationManager;
        private readonly IUserLoginHistoryService _userLoginHistoryService;

        public AppSignInManager(
            AppUserManager userManager,
            IAuthenticationManager authenticationManager,
            IUserLoginHistoryService userLoginHistoryService) :
            base(userManager, authenticationManager)
        {
            _userManager = userManager;
            _authenticationManager = authenticationManager;
            _userLoginHistoryService = userLoginHistoryService;
        }
        public async Task<SignInStatus> EmailOrPhoneSignInAsync(SignInModel model)
        {
            var user = await _userManager.FindByEmailOrPhoneAsync(model.EmailOrPhone);
            if (user == null)
                return SignInStatus.Failure;

            var status = await PasswordSignInAsync(user.UserName, model.Password, model.IsPersistent, model.ShouldLockout);
            var userLoginHistory = new UserLoginHistory()
            {
                Browser = model.Browser,
                IPAddress = model.IPAddress,
                OwinSignInStatus = (byte)status,
                UserId = user.Id,
            };
            await _userLoginHistoryService.InsertAsync(userLoginHistory);
            return status;
        }

        public async Task<SignInStatus> EmailOrPhoneSignInAsync(string emailOrPhone, string password, bool isPersistent, bool shouldLockout)
        {
            var userName = await _userManager.FindByEmailOrPhoneAsync(emailOrPhone);
            if (userName == null)
                return SignInStatus.Failure;
            return await PasswordSignInAsync(userName?.UserName, password, isPersistent, shouldLockout);
        }

        public async Task<(SignInStatus signInStatus, AppUser user)> EmailOrUsernameOrPhoneSignInAsync(SignInModel2 model)
        {
            var user = await _userManager.FindByEmailOrUsernameOrPhoneAsync(model.EmailOrPhoneOrUserName);
            if (user == null)
                return (SignInStatus.Failure, null);

            var status = await PasswordSignInAsync(user.UserName, model.Password, model.IsPersistent, model.ShouldLockout);
            var userLoginHistory = new UserLoginHistory()
            {
                Browser = model.Browser,
                IPAddress = model.IPAddress,
                OwinSignInStatus = (byte)status,
                UserId = user.Id,
            };
            await _userLoginHistoryService.InsertAsync(userLoginHistory);
            return (status, user);
        }

        public async Task<SignInStatus> EmailOrUsernameOrPhoneSignInAsync(string emailOrUsernameOrPhone, string password, bool isPersistent, bool shouldLockout)
        {
            var userName = await _userManager.FindByEmailOrUsernameOrPhoneAsync(emailOrUsernameOrPhone);
            if (userName == null)
                return SignInStatus.Failure;
            return await PasswordSignInAsync(userName?.UserName, password, isPersistent, shouldLockout);
        }
    }
}
