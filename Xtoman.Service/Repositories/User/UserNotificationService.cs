﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class UserNotificationService : RepositoryBase<UserNotification>, IUserNotificationService
    {
        public UserNotificationService(IUnitOfWork context) : base(context)
        {
        }

        public async Task<List<UserNotification>> GetByUserIdAsync(int userId)
        {
            return await TableNoTracking.Where(p => p.UserId == userId).ToListAsync();
        }

        public async Task<int> GetUserUnreadsCountAsync(int userId)
        {
            return await TableNoTracking.Where(p => p.UserId == userId && !p.IsRead).CountAsync();
        }
    }
}
