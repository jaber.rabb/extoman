﻿using Microsoft.AspNet.Identity;
using System.Security.Principal;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class CurrentUser : ICurrentUser
    {
        private readonly IIdentity _identity;
        private readonly IAppUserManager _userManager;
        private AppUser _user;
        public CurrentUser(IIdentity identity, IAppUserManager userManager)
        {
            _identity = identity;
            _userManager = userManager;
        }
        public AppUser User
        {
            get
            {
                return _user ?? (_user = _userManager.FindById(_identity.GetUserId<int>()));
            }
        }
    }

}
