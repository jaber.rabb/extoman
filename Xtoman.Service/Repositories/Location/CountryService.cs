﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class CountryService : RepositoryBase<Country>, ICountryService
    {
        public CountryService(IUnitOfWork context) : base(context)
        {
        }

        public async Task<int> GetCountryIdByIsoCodeAsync(string countryCode)
        {
            return await TableNoTracking.Where(p => p.IsoCode == countryCode).Select(p => p.Id).FirstOrDefaultAsync();
        }
    }
}
