﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IProvinceStateService : IRepository<ProvinceState>
    {
    }
}
