﻿using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Data;

namespace Xtoman.Service
{
    public interface ICountryService : IRepository<Country>
    {
        Task<int> GetCountryIdByIsoCodeAsync(string countryCode);
    }
}
