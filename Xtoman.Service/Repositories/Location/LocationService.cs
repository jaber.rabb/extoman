﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class LocationService : RepositoryBase<Location>, ILocationService
    {
        public LocationService(IUnitOfWork uow) : base(uow)
        {
        }
    }
}
