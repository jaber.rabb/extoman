﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class ContentService : RepositoryBase<Content>, IContentService
    {
        public ContentService(IUnitOfWork uow) : base(uow)
        {
        }
    }
}
