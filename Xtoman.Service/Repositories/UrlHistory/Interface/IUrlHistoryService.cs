﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IUrlHistoryService
    {
        bool LoadAllUrlHistoriesOnStartup { get; set; }
        IQueryable<UrlHistory> Table { get; }
        IQueryable<UrlHistory> TableNoTracking { get; }
        IList<UrlHistoryForCaching> GetAllUrlHistoriesCached();
        Task<List<UrlHistoryForCaching>> GetAllUrlHistoriesCachedAsync();
        bool Delete(UrlHistory entity);
        bool Delete(int id);
        int? FindEntityIdByUrl<T>(string url, int languageId) where T : BaseEntity, IUrlHistoryEntity;
        Task<int?> FindEntityIdByUrlAsync<T>(string url, int languageId) where T : BaseEntity, IUrlHistoryEntity;
        bool IsUniqueUrl<T>(string url, int languageId) where T : BaseEntity, IUrlHistoryEntity;
        Task<bool> IsUniqueUrlAsync<T>(string url, int languageId) where T : BaseEntity, IUrlHistoryEntity;
        List<UrlHistory> GetAllOfType<T>();
        UrlHistory GetById(int id);
        List<string> GetUrls(int entityId, string entityName, int languageId);
        List<string> GetUrls<T>(T entity, int languageId) where T : BaseEntity, IUrlHistoryEntity;
        List<string> GetUrls<T>(T entity, int entityId, int languageId);
        Task<List<string>> GetUrlsAsync(int entityId, string entityName, int languageId);
        Task<List<string>> GetUrlsAsync<T>(T entity, int languageId) where T : BaseEntity, IUrlHistoryEntity;
        Task<List<string>> GetUrlsAsync<T>(T entity, int entityId, int languageId);
        bool Insert(UrlHistory entity);
        bool SaveUrl(int entityId, string entityName, string url, int languageId);
        bool SaveUrl<T>(T entity, string url, int languageId) where T : BaseEntity, IUrlHistoryEntity;
        bool SaveUrl<T>(T entity, int entityId, string url, int languageId) where T : BaseEntity, IUrlHistoryEntity;
        Task SaveUrlAsync(int entityId, string entityName, string url, int languageId);
        Task SaveUrlAsync<T>(T entity, string url, int languageId) where T : BaseEntity, IUrlHistoryEntity;
        Task SaveUrlAsync<T>(T entity, int entityId, string url, int languageId) where T : BaseEntity, IUrlHistoryEntity;
        bool Update(UrlHistory entity);
    }

}
