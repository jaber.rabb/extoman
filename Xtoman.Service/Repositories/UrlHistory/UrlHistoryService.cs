﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using System.Security.Principal;
using Xtoman.Utility.Caching;
using Xtoman.Data;
using Xtoman.Domain.Models;
using System.Threading.Tasks;
using Xtoman.Utility;
using System.Data.Entity;
using Xtoman.Domain.Settings;

namespace Xtoman.Service
{
    public class UrlHistoryService : CacheRepositoryBase<UrlHistory, UrlHistoryForCaching>, IUrlHistoryService
    {
        #region Constants
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : language ID
        /// {1} : entity ID
        /// {2} : entity Name
        /// </remarks>
        private const string CACHEPROPERTY_KEY = "Xtoman.urlhistory.value-{0}-{1}-{2}";
        /// <summary>
        /// Key for caching
        /// </summary>
        private const string CACHEPROPERTY_ALL_KEY = "Xtoman.urlhistory.all";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string CACHEPROPERTY_PATTERN_KEY = "Xtoman.urlhistory.";
        #endregion

        #region Fields
        private readonly ICacheManager _cacheManager;
        private readonly IIdentity _identity;
        private readonly IUnitOfWork _uow;

        public bool LoadAllUrlHistoriesOnStartup { get; set; }
        #endregion

        #region Ctor
        public UrlHistoryService(IUnitOfWork uow, ICacheManager cacheManager, CacheSettings cacheSettings, IIdentity identity) : base(uow, cacheManager, cacheSettings)
        {
            LoadAllUrlHistoriesOnStartup = false;
            _cacheManager = cacheManager;
            _identity = identity;
            _uow = uow;
        }
        #endregion

        #region Utilities
        public virtual IList<UrlHistoryForCaching> GetAllUrlHistoriesCached()
        {
            //cache
            var key = CACHEPROPERTY_ALL_KEY;
            return _cacheManager.Get(key, () =>
            {
                var list = TableNoTracking.Select(p => new UrlHistoryForCaching
                {
                    Id = p.Id,
                    EntityId = p.EntityId,
                    LanguageId = p.LanguageId,
                    EntityName = p.EntityName,
                    Url = p.Url
                }).ToList();
                return list;
            });
        }

        public virtual Task<List<UrlHistoryForCaching>> GetAllUrlHistoriesCachedAsync()
        {
            //cache
            var key = CACHEPROPERTY_ALL_KEY;
            return _cacheManager.GetAsync(key, async () =>
            {
                var list = await TableNoTracking.Select(p => new UrlHistoryForCaching
                {
                    Id = p.Id,
                    EntityId = p.EntityId,
                    LanguageId = p.LanguageId,
                    EntityName = p.EntityName,
                    Url = p.Url
                }).ToListAsync();
                return list;
            });
        }
        #endregion

        #region Base Methods
        public new bool Insert(UrlHistory entity)
        {
            var result = Insert(entity);
            _cacheManager.RemoveByPattern(CACHEPROPERTY_PATTERN_KEY);
            return result;
        }

        public new bool Update(UrlHistory entity)
        {
            var result = Update(entity);
            _cacheManager.RemoveByPattern(CACHEPROPERTY_PATTERN_KEY);
            return result;
        }

        public new bool Delete(UrlHistory entity)
        {
            var result = Delete(entity);
            _cacheManager.RemoveByPattern(CACHEPROPERTY_PATTERN_KEY);
            return result;
        }

        public new bool Delete(int id)
        {
            var result = Delete(id);
            _cacheManager.RemoveByPattern(CACHEPROPERTY_PATTERN_KEY);
            return result;
        }
        public UrlHistory GetById(int id)
        {
            if (id == 0)
                return null;
            return GetById(id);
        }
        public virtual List<UrlHistory> GetAllOfType<T>()
        {
            var entityName = typeof(T).Name;
            return Table.Where(p => p.EntityName == entityName).ToList();
        }
        public int? FindEntityIdByUrl<T>(string url, int languageId) where T : BaseEntity, IUrlHistoryEntity
        {
            var entityName = typeof(T).Name;
            if (LoadAllUrlHistoriesOnStartup)
            {
                return GetAllUrlHistoriesCached()
                    .Where(p => p.EntityName == entityName && p.LanguageId == languageId && p.Url == url)
                    .Select(p => new { p.EntityId }).SingleOrDefault()?.EntityId;
            }
            else
            {
                return TableNoTracking
                     .Where(p => p.EntityName == entityName && p.LanguageId == languageId && p.Url == url)
                     .Select(p => new { p.EntityId }).SingleOrDefault()?.EntityId;
            }
        }

        public async Task<int?> FindEntityIdByUrlAsync<T>(string url, int languageId) where T : BaseEntity, IUrlHistoryEntity
        {
            var entityName = typeof(T).Name;
            if (LoadAllUrlHistoriesOnStartup)
            {
                var list = await GetAllUrlHistoriesCachedAsync();
                return list.Where(p => p.EntityName == entityName && p.LanguageId == languageId && p.Url == url)
                    .Select(p => new { p.EntityId }).SingleOrDefault()?.EntityId;
            }
            else
            {
                var entity = await TableNoTracking
                    .Where(p => p.EntityName == entityName && p.LanguageId == languageId && p.Url == url)
                    .Select(p => new { p.EntityId }).SingleOrDefaultAsync();
                return entity?.EntityId;
            }
        }

        public bool IsUniqueUrl<T>(string url, int languageId) where T : BaseEntity, IUrlHistoryEntity
        {
            var entityName = typeof(T).Name;
            if (LoadAllUrlHistoriesOnStartup)
            {
                return GetAllUrlHistoriesCached()
                    .Any(p => p.EntityName == entityName && p.LanguageId == languageId && p.Url == url) == false;
            }
            else
            {
                return TableNoTracking
                     .Any(p => p.EntityName == entityName && p.LanguageId == languageId && p.Url == url) == false;
            }
        }

        public async Task<bool> IsUniqueUrlAsync<T>(string url, int languageId) where T : BaseEntity, IUrlHistoryEntity
        {
            var entityName = typeof(T).Name;
            if (LoadAllUrlHistoriesOnStartup)
            {
                var list = await GetAllUrlHistoriesCachedAsync();
                return !list.Any(p => p.EntityName == entityName && p.LanguageId == languageId && p.Url == url);
            }
            else
            {
                var result = await TableNoTracking.AnyAsync(p => p.EntityName == entityName && p.LanguageId == languageId && p.Url == url);
                return !result;
            }
        }
        #endregion

        #region GetUrls
        public virtual List<string> GetUrls<T>(T entity, int languageId) where T : BaseEntity, IUrlHistoryEntity
        {
            if (entity == null)
                throw new ArgumentNullException("entity");
            var entityId = entity.Id;
            return GetUrls(entity, entityId, languageId);
        }
        public virtual List<string> GetUrls<T>(T entity, int entityId, int languageId)
        {
            var entityName = typeof(T).Name;
            return GetUrls(entityId, entityName, languageId);
        }
        public virtual List<string> GetUrls(int entityId, string entityName, int languageId)
        {
            if (entityId == 0 || string.IsNullOrEmpty(entityName))
                return new List<string>();

            var key = string.Format(CACHEPROPERTY_KEY, languageId, entityId, entityName);
            if (LoadAllUrlHistoriesOnStartup)
            {
                var value = _cacheManager.Get(key, () =>
                {
                    //load all records (we know they are cached)
                    var source = GetAllUrlHistoriesCached();
                    var query = source.Where(p => p.LanguageId == languageId && p.EntityId == entityId && p.EntityName == entityName).Select(p => p.Url);
                    return query.ToList();
                });
                return value;
            }
            else
            {
                //gradual loading
                var value = _cacheManager.Get(key, () =>
                {
                    var query = TableNoTracking.Where(p => p.LanguageId == languageId && p.EntityId == entityId && p.EntityName == entityName).Select(p => p.Url);
                    return query.ToList();
                });
                return value;
            }
        }
        #endregion

        #region GetUrls Async
        public virtual Task<List<string>> GetUrlsAsync<T>(T entity, int languageId) where T : BaseEntity, IUrlHistoryEntity
        {
            CheckHelper.NotNull(entity, nameof(entity));

            return GetUrlsAsync(entity, entity.Id, languageId);
        }

        public virtual Task<List<string>> GetUrlsAsync<T>(T entity, int entityId, int languageId)
        {
            var entityName = typeof(T).Name;
            return GetUrlsAsync(entityId, entityName, languageId);
        }

        public virtual async Task<List<string>> GetUrlsAsync(int entityId, string entityName, int languageId)
        {
            if (entityId == 0 || string.IsNullOrEmpty(entityName))
                return new List<string>();

            var key = string.Format(CACHEPROPERTY_KEY, languageId, entityId, entityName);
            if (LoadAllUrlHistoriesOnStartup)
            {
                var value = await _cacheManager.GetAsync(key, async () =>
                {
                    //load all records (we know they are cached)
                    var source = await GetAllUrlHistoriesCachedAsync();
                    var list = source.Where(p => p.LanguageId == languageId && p.EntityId == entityId && p.EntityName == entityName).Select(p => p.Url).ToList();
                    return list;
                });
                return value;
            }
            else
            {
                //gradual loading
                var value = await _cacheManager.GetAsync(key, async () =>
                {
                    var list = await TableNoTracking.Where(p => p.LanguageId == languageId && p.EntityId == entityId && p.EntityName == entityName).Select(p => p.Url).ToListAsync();
                    return list;
                });
                return value;
            }
        }
        #endregion

        #region SaveUrl
        public virtual bool SaveUrl<T>(T entity, string url, int languageId) where T : BaseEntity, IUrlHistoryEntity
        {
            if (entity == null)
                throw new ArgumentNullException("entity");
            var entityName = typeof(T).Name;
            return SaveUrl(entity.Id, entityName, url, languageId);
        }

        public virtual bool SaveUrl<T>(T entity, int entityId, string url, int languageId) where T : BaseEntity, IUrlHistoryEntity
        {
            var entityName = typeof(T).Name;
            return SaveUrl(entityId, entityName, url, languageId);
        }

        public virtual bool SaveUrl(int entityId, string entityName, string url, int languageId)
        {
            var result = false;
            if (string.IsNullOrWhiteSpace(url) == false)
            {
                var urlExists = TableNoTracking.Any(p => p.EntityId == entityId && p.EntityName == entityName && p.Url == url && p.LanguageId == languageId);
                if (urlExists == false)
                {
                    //insert
                    var entity = new UrlHistory
                    {
                        EntityId = entityId,
                        EntityName = entityName,
                        Url = url,
                        LanguageId = languageId,
                        InsertDate = DateTime.Now,
                        InsertUserId = _identity.GetUserId<int>()
                    };
                    result = Insert(entity);
                }
            }
            return result;
        }
        #endregion

        #region SaveUrl Async
        public virtual Task SaveUrlAsync<T>(T entity, string url, int languageId) where T : BaseEntity, IUrlHistoryEntity
        {
            CheckHelper.NotNull(entity, nameof(entity));
            var entityName = typeof(T).Name;
            return SaveUrlAsync(entity.Id, entityName, url, languageId);
        }

        public virtual Task SaveUrlAsync<T>(T entity, int entityId, string url, int languageId) where T : BaseEntity, IUrlHistoryEntity
        {
            var entityName = typeof(T).Name;
            return SaveUrlAsync(entityId, entityName, url, languageId);
        }

        public virtual async Task SaveUrlAsync(int entityId, string entityName, string url, int languageId)
        {
            if (url.HasValue(true))
            {
                var urlExists = await TableNoTracking.AnyAsync(p => p.EntityId == entityId && p.EntityName == entityName && p.Url == url && p.LanguageId == languageId);
                if (!urlExists)
                {
                    //insert
                    var entity = new UrlHistory
                    {
                        EntityId = entityId,
                        EntityName = entityName,
                        Url = url,
                        LanguageId = languageId == 0 ? new int?() : languageId,
                        InsertDate = DateTime.Now,
                        InsertUserId = _identity.GetUserId<int>()
                    };
                    await InsertAsync(entity);
                }
            }
        }
        #endregion
    }
}
