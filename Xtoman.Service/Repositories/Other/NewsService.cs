﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class NewsService : RepositoryBase<News>, INewsService
    {
        public NewsService(IUnitOfWork context) : base(context)
        {
        }
    }
}
