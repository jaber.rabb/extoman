﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class SupportTicketReplyService : RepositoryBase<SupportTicketReply>, ISupportTicketReplyService
    {
        public SupportTicketReplyService(IUnitOfWork context) : base(context)
        {
        }
    }
}
