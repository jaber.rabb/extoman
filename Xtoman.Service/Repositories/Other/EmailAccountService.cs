﻿using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service
{
    public class EmailAccountService : CacheRepositoryBase<EmailAccount, EmailAccountCache>, IEmailAccountService
    {
        public EmailAccountService(IUnitOfWork context, ICacheManager cacheManager, CacheSettings cacheSettings) : base(context, cacheManager, cacheSettings)
        {
        }

        public async Task<EmailAccount> GetCachedByIdAsync(int id)
        {
            var cachedEmailAccount = await GetCachedByIdAsync(id, p => new EmailAccountCache
            {
                Id = p.Id,
                DisplayName = p.DisplayName,
                Email = p.Email,
                EnableSsl = p.EnableSsl,
                Host = p.Host,
                Password = p.Password,
                Port = p.Port,
                UseDefaultCredentials = p.UseDefaultCredentials,
                Username = p.Username
            });

            return cachedEmailAccount.CastToClass<EmailAccount>();
        }
    }
}
