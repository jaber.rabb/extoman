﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class SupportTicketService : RepositoryBase<SupportTicket>, ISupportTicketService
    {
        public SupportTicketService(IUnitOfWork context) : base(context)
        {
        }
    }
}
