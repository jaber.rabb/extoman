﻿using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IEmailAccountService : IRepository<EmailAccount>
    {
        Task<EmailAccount> GetCachedByIdAsync(int id);
    }
}
