﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class UserWithdrawService : RepositoryBase<UserWithdraw>, IUserWithdrawService
    {
        public UserWithdrawService(IUnitOfWork context) : base(context)
        {
        }
    }
}
