﻿using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Utility.Caching;

namespace Xtoman.Service
{
    public class CommissionService : CacheRepositoryBase<Commission, CommissionForCache>, ICommissionService
    {
        public CommissionService(IUnitOfWork context, ICacheManager cacheManager, CacheSettings cacheSettings) : base(context, cacheManager, cacheSettings)
        {
        }
    }
}
