﻿using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Utility.Caching;

namespace Xtoman.Service
{
    public class DiscountService : CacheRepositoryBase<Discount, DiscountForCache>, IDiscountService
    {
        public DiscountService(IUnitOfWork context, ICacheManager cacheManager, CacheSettings cacheSettings) : base(context, cacheManager, cacheSettings)
        {
        }
    }
}
