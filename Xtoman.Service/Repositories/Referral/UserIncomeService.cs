﻿using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class UserIncomeService : RepositoryBase<UserIncome>, IUserIncomeService
    {
        public UserIncomeService(IUnitOfWork context) : base(context)
        {
        }
    }
}
