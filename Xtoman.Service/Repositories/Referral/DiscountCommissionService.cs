﻿using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Utility.Caching;

namespace Xtoman.Service
{
    public class DiscountCommissionService : CacheRepositoryBase<DiscountCommission, DiscountCommissionForCache>, IDiscountCommissionService
    {
        public DiscountCommissionService(IUnitOfWork context, ICacheManager cacheManager, CacheSettings cacheSettings) : base(context, cacheManager, cacheSettings)
        {
        }
    }
}
