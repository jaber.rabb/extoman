﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class TelegramBotLogService : RepositoryBase<TelegramBotLog>, ITelegramBotLogService
    {
        public TelegramBotLogService(IUnitOfWork context) : base(context)
        {
        }
    }
}
