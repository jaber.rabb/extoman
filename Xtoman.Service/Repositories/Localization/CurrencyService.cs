﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class CurrencyService : RepositoryBase<Currency>, ICurrencyService
    {
        public CurrencyService(IUnitOfWork uow) : base(uow)
        {
        }
    }
}