﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    /// <summary>
    /// Localized entity service interface
    /// </summary>
    public partial interface ILocalizedPropertyService
    {
        /// <summary>
        /// Deletes a localized property
        /// </summary>
        /// <param name="localizedProperty">Localized property</param>
        bool DeleteLocalizedProperty(LocalizedProperty localizedProperty);

        /// <summary>
        /// Gets a localized property
        /// </summary>
        /// <param name="localizedPropertyId">Localized property identifier</param>
        /// <returns>Localized property</returns>
        LocalizedProperty GetLocalizedPropertyById(int localizedPropertyId);

        /// <summary>
        /// Inserts a localized property
        /// </summary>
        /// <param name="localizedProperty">Localized property</param>
        bool InsertLocalizedProperty(LocalizedProperty localizedProperty);

        /// <summary>
        /// Updates the localized property
        /// </summary>
        /// <param name="localizedProperty">Localized property</param>
        bool UpdateLocalizedProperty(LocalizedProperty localizedProperty);

        /// <summary>
        /// Find localized value by type
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="languageId">Language ID</param>
        string GetLocalizedValue<T>(T entity, Expression<Func<T, string>> keySelector, int languageId) where T : BaseEntity, ILocalizedEntity;

        /// <summary>
        /// Find localized value by type
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <typeparam name="TPropType">TPropType</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="languageId">Language ID</param>
        /// <returns></returns>
        TPropType GetLocalizedValue<T, TPropType>(T entity, Expression<Func<T, TPropType>> keySelector, int languageId) where T : BaseEntity, ILocalizedEntity;

        /// <summary>
        /// Find localized value
        /// </summary>
        /// <param name="languageId">Language identifier</param>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <returns>Found localized value</returns>
        string GetLocalizedValue(int languageId, int entityId, string localeKeyGroup, string localeKey);


        /// <summary>
        /// Find localized value by type
        /// </summary>
        /// <typeparam name="T">type of value</typeparam>
        /// <param name="languageId">Language identifier</param>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <returns>Found localized value</returns>
        /// <summary>
        T GetLocalizedValue<T>(int languageId, int entityId, string localeKeyGroup, string localeKey);

        /// <summary>
        /// Find localized entity
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="languageId">Language ID</param>
        T GetLocalizedValue<T>(T entity, int languageId) where T : BaseEntity, ILocalizedEntity;

        /// <summary>
        /// Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="languageId">Language ID</param>
        bool SaveLocalizedValue<T>(T entity, Expression<Func<T, string>> keySelector, string localeValue, int languageId) where T : BaseEntity, ILocalizedEntity;

        /// <summary>
        /// Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <typeparam name="TPropType">TPropType</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="languageId">Language ID</param>
        bool SaveLocalizedValue<T, TPropType>(T entity, Expression<Func<T, TPropType>> keySelector, TPropType localeValue, int languageId) where T : BaseEntity, ILocalizedEntity;


        /// <summary>
        /// Save localized value
        /// </summary>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <param name="languageId">Language identifier</param>
        //bool SaveLocalizedValue(int entityId, string localeValue, string localeKeyGroup, string localeKey, int languageId);


        /// <summary>
        /// Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <param name="languageId">Language identifier</param>
        bool SaveLocalizedValue<T>(int entityId, T localeValue, string localeKeyGroup, string localeKey, int languageId);


        /// <summary>
        /// Save localized value for entity
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="languageId">Language identifier</param>
        /// <param name="saveNonLocalizedProperty">saveNonLocalizedProperty</param>
        bool SaveLocalizedValue<T>(T entity, int languageId
            //, bool saveNonLocalizedProperty = false
            ) where T : BaseEntity, ILocalizedEntity;

        bool GetLocalizedProperty(int entityId, string localeKeyGroup, string localeKey, string LocalValue);

        /// <summary>
        /// Find localized value by type
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="languageId">Language ID</param>
        Task<string> GetLocalizedValueAsync<T>(T entity, Expression<Func<T, string>> keySelector, int languageId) where T : BaseEntity, ILocalizedEntity;

        /// <summary>
        /// Find localized value by type
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <typeparam name="TPropType">TPropType</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="languageId">Language ID</param>
        /// <returns></returns>
        Task<TPropType> GetLocalizedValueAsync<T, TPropType>(T entity, Expression<Func<T, TPropType>> keySelector, int languageId) where T : BaseEntity, ILocalizedEntity;

        /// <summary>
        /// Find localized value
        /// </summary>
        /// <param name="languageId">Language identifier</param>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <returns>Found localized value</returns>
        Task<string> GetLocalizedValueAsync(int languageId, int entityId, string localeKeyGroup, string localeKey);

        /// <summary>
        /// Find localized value by type
        /// </summary>
        /// <typeparam name="T">type of value</typeparam>
        /// <param name="languageId">Language identifier</param>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <returns>Found localized value</returns>
        /// <summary>
        Task<T> GetLocalizedValueAsync<T>(int languageId, int entityId, string localeKeyGroup, string localeKey);

        /// <summary>
        /// Find localized entity
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="languageId">Language ID</param>
        Task<T> GetLocalizedValueAsync<T>(T entity, int languageId) where T : BaseEntity, ILocalizedEntity;

        /// <summary>
        /// Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="languageId">Language ID</param>
        Task SaveLocalizedValueAsync<T>(T entity, Expression<Func<T, string>> keySelector, string localeValue, int languageId) where T : BaseEntity, ILocalizedEntity;

        /// <summary>
        /// Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <typeparam name="TPropType">TPropType</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="languageId">Language ID</param>
        Task SaveLocalizedValueAsync<T, TPropType>(T entity, Expression<Func<T, TPropType>> keySelector, TPropType localeValue, int languageId) where T : BaseEntity, ILocalizedEntity;

        /// <summary>
        /// Save localized value
        /// </summary>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <param name="languageId">Language identifier</param>
        //bool SaveLocalizedValueAsync(int entityId, string localeValue, string localeKeyGroup, string localeKey, int languageId);

        /// <summary>
        /// Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <param name="languageId">Language identifier</param>
        Task SaveLocalizedValueAsync<T>(int entityId, T localeValue, string localeKeyGroup, string localeKey, int languageId);

        /// <summary>
        /// Save localized value for entity
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="languageId">Language identifier</param>
        /// <param name="saveNonLocalizedProperty">saveNonLocalizedProperty</param>
        Task SaveLocalizedValueAsync<T>(T entity, int languageId /*, bool saveNonLocalizedProperty = false*/ ) where T : BaseEntity, ILocalizedEntity;

    }
}
