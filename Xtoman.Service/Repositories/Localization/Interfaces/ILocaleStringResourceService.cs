﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface ILocaleStringResourceService : IRepository<LocaleStringResource>
    {
        bool AddOrUpdateResource(LocaleStringResource entity);
    }
}
