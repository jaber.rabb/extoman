﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class LanguageService : RepositoryBase<Language>, ILanguageService
    {
        public LanguageService(IUnitOfWork uow) : base(uow)
        {
        }
    }
}
