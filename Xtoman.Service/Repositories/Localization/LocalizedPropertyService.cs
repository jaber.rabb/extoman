﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service
{
    /// <summary>
    /// Provides information about localizable entities
    /// </summary>
    public partial class LocalizedPropertyService : CacheRepositoryBase<LocalizedProperty, LocalizedPropertyForCaching>, ILocalizedPropertyService
    {
        #region Constants
        public static readonly string CACHE_KEY_NAME = "Xtoman";
        public static readonly string TYPE_NAME = typeof(LocalizedProperty).Name.ToLower();
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : language ID
        /// {1} : entity ID
        /// {2} : locale key group
        /// {3} : locale key
        /// </remarks>
        protected string LOCALIZEDPROPERTY_KEY = $"{CACHE_KEY_NAME}.{TYPE_NAME}.{{0}}-{{1}}-{{2}}-{{3}}";
        /// <summary>
        /// Key for caching
        /// </summary>
        protected readonly string LOCALIZEDPROPERTY_ALL_KEY = $"{CACHE_KEY_NAME}.{TYPE_NAME}.all";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        protected readonly string LOCALIZEDPROPERTY_PATTERN_KEY = $"{CACHE_KEY_NAME}.{TYPE_NAME}.";
        #endregion

        #region Fields
        private readonly ICacheManager _cacheManager;
        private readonly LocalizationSettings _localizationSettings;
        private readonly CacheSettings _cacheSettings;

        #endregion

        #region Ctor
        public LocalizedPropertyService(IUnitOfWork context, ICacheManager cacheManager, CacheSettings cacheSettings, LocalizationSettings localizationSettings) : base(context, cacheManager, cacheSettings)
        {
            _cacheManager = cacheManager;
            _cacheSettings = cacheSettings;
            _localizationSettings = localizationSettings;
        }

        #endregion

        #region Utilities
        /// <summary>
        /// Gets localized properties
        /// </summary>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <returns>Localized properties</returns>
        protected virtual IList<LocalizedProperty> GetLocalizedProperties(int entityId, string localeKeyGroup)
        {
            if (entityId == 0 || string.IsNullOrEmpty(localeKeyGroup))
                return new List<LocalizedProperty>();

            var query = Table.OrderBy(p => p.Id).Where(p => p.EntityId == entityId && p.LocaleKeyGroup == localeKeyGroup);
            var props = query.ToList();
            return props;
        }

        /// <summary>
        /// Gets all cached localized properties
        /// </summary>
        /// <returns>Cached localized properties</returns>
        protected virtual IList<LocalizedPropertyForCaching> GetAllLocalizedPropertiesCached()
        {
            //cache
            var key = LOCALIZEDPROPERTY_ALL_KEY;
            return _cacheManager.Get(key, () =>
            {
                var list = TableNoTracking.Select(p => new LocalizedPropertyForCaching
                {
                    Id = p.Id,
                    EntityId = p.EntityId,
                    LanguageId = p.LanguageId,
                    LocaleKeyGroup = p.LocaleKeyGroup,
                    LocaleKey = p.LocaleKey,
                    LocaleValue = p.LocaleValue
                }).ToList();
                return list;
            });
        }
        #endregion

        #region Methods
        /// <summary>
        /// Deletes a localized property
        /// </summary>
        /// <param name="localizedProperty">Localized property</param>
        public virtual bool DeleteLocalizedProperty(LocalizedProperty localizedProperty)
        {
            if (localizedProperty == null)
                throw new ArgumentNullException("localizedProperty");

            var result = Delete(localizedProperty);

            //cache
            _cacheManager.RemoveByPattern(LOCALIZEDPROPERTY_PATTERN_KEY);

            return result;
        }

        /// <summary>
        /// Gets a localized property
        /// </summary>
        /// <param name="localizedPropertyId">Localized property identifier</param>
        /// <returns>Localized property</returns>
        public virtual LocalizedProperty GetLocalizedPropertyById(int localizedPropertyId)
        {
            if (localizedPropertyId == 0)
                return null;

            return GetById(localizedPropertyId);
        }

        /// <summary>
        /// Inserts a localized property
        /// </summary>
        /// <param name="localizedProperty">Localized property</param>
        public virtual bool InsertLocalizedProperty(LocalizedProperty localizedProperty)
        {
            if (localizedProperty == null)
                throw new ArgumentNullException("localizedProperty");

            try
            {
                Insert(localizedProperty);

                //cache
                _cacheManager.RemoveByPattern(LOCALIZEDPROPERTY_PATTERN_KEY);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Updates the localized property
        /// </summary>
        /// <param name="localizedProperty">Localized property</param>
        public virtual bool UpdateLocalizedProperty(LocalizedProperty localizedProperty)
        {
            if (localizedProperty == null)
                throw new ArgumentNullException("localizedProperty");

            try
            {
                Update(localizedProperty);

                //cache
                _cacheManager.RemoveByPattern(LOCALIZEDPROPERTY_PATTERN_KEY);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region GetLocalizedValue

        /// <summary>
        /// Find localized value by type
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="languageId">Language ID</param>
        public virtual string GetLocalizedValue<T>(T entity, Expression<Func<T, string>> keySelector, int languageId) where T : BaseEntity, ILocalizedEntity
        {
            return GetLocalizedValue<T, string>(entity, keySelector, languageId);
        }

        /// <summary>
        /// Find localized value by type
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <typeparam name="TPropType">TPropType</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="languageId">Language ID</param>
        /// <returns></returns>
        public virtual TPropType GetLocalizedValue<T, TPropType>(T entity, Expression<Func<T, TPropType>> keySelector, int languageId) where T : BaseEntity, ILocalizedEntity
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            var propInfo = keySelector.GetPropInfo();
            var localeKeyGroup = typeof(T).Name;
            var localeKey = propInfo.Name;

            return GetLocalizedValue<TPropType>(languageId, entity.Id, localeKeyGroup, localeKey);
        }

        /// <summary>
        /// Find localized value
        /// </summary>
        /// <param name="languageId">Language identifier</param>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <returns>Found localized value</returns>
        public virtual string GetLocalizedValue(int languageId, int entityId, string localeKeyGroup, string localeKey)
        {
            return GetLocalizedValue<string>(languageId, entityId, localeKeyGroup, localeKey);
        }

        /// <summary>
        /// Find localized value by type
        /// </summary>
        /// <typeparam name="T">type of value</typeparam>
        /// <param name="languageId">Language identifier</param>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <returns>Found localized value</returns>
        /// <summary>
        public virtual T GetLocalizedValue<T>(int languageId, int entityId, string localeKeyGroup, string localeKey)
        {
            if (languageId == 0)
                throw new ArgumentOutOfRangeException("languageId", "Language ID should not be 0");

            var key = string.Format(LOCALIZEDPROPERTY_KEY, languageId, entityId, localeKeyGroup, localeKey);
            if (_localizationSettings.LoadAllLocalizedPropertiesOnStartup)
            {
                var value = _cacheManager.Get(key, () =>
                {
                    //load all records (we know they are cached)
                    var source = GetAllLocalizedPropertiesCached();
                    var query = source.Where(p => p.LanguageId == languageId && p.EntityId == entityId && p.LocaleKeyGroup == localeKeyGroup && p.LocaleKey == localeKey).Select(p => p.LocaleValue);
                    var localeValue = query.FirstOrDefault();
                    //little hack here. nulls aren't cacheable so set it to ""
                    if (localeValue == null)
                        localeValue = "";
                    return localeValue;
                });
                return value.ConvertTo<T>();
            }
            else
            {
                //gradual loading
                var value = _cacheManager.Get(key, () =>
                {
                    var query = TableNoTracking.Where(p => p.LanguageId == languageId && p.EntityId == entityId && p.LocaleKeyGroup == localeKeyGroup && p.LocaleKey == localeKey).Select(p => p.LocaleValue);
                    var localeValue = query.FirstOrDefault();
                    //little hack here. nulls aren't cacheable so set it to ""
                    if (localeValue == null)
                        localeValue = "";
                    return localeValue;
                });
                return value.ConvertTo<T>();
            }
        }

        /// <summary>
        /// Find localized entity
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="languageId">Language ID</param>
        public virtual T GetLocalizedValue<T>(T entity, int languageId) where T : BaseEntity, ILocalizedEntity
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            var props = typeof(T).GetProperties().Where(p => Attribute.IsDefined(p, typeof(LocalizedPropertyAttribute)) && p.CanRead && p.CanWrite);

            foreach (var prop in props)
            {
                var localeKeyGroup = typeof(T).Name;
                var localeKey = prop.Name;

                var valueStr = GetLocalizedValue(languageId, entity.Id, localeKeyGroup, localeKey);
                var value = Convert.ChangeType(valueStr, prop.PropertyType);

                //convert string value (from database) to type of property
                //object value = CommonHelper.GetNopCustomTypeConverter(prop.PropertyType).ConvertFromInvariantString(setting);
                //set property
                prop.SetValue(entity, value, null);
            }
            return entity;
        }
        #endregion

        #region SaveLocalizedValue
        /// <summary>
        /// Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="languageId">Language ID</param>
        public virtual bool SaveLocalizedValue<T>(T entity, Expression<Func<T, string>> keySelector, string localeValue, int languageId) where T : BaseEntity, ILocalizedEntity
        {
            return SaveLocalizedValue<T, string>(entity, keySelector, localeValue, languageId);
        }

        /// <summary>
        /// Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <typeparam name="TPropType">TPropType</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="languageId">Language ID</param>
        public virtual bool SaveLocalizedValue<T, TPropType>(T entity, Expression<Func<T, TPropType>> keySelector, TPropType localeValue, int languageId) where T : BaseEntity, ILocalizedEntity
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            var propInfo = keySelector.GetPropInfo();
            var localeKeyGroup = typeof(T).Name;
            var localeKey = propInfo.Name;

            return SaveLocalizedValue(entity.Id, localeValue, localeKeyGroup, localeKey, languageId);
        }

        ///// <summary>
        ///// Save localized value
        ///// </summary>
        ///// <param name="entityId">Entity identifier</param>
        ///// <param name="localeValue">Locale value</param>
        ///// <param name="localeKeyGroup">Locale key group</param>
        ///// <param name="localeKey">Locale key</param>
        ///// <param name="languageId">Language identifier</param>
        //public virtual bool SaveLocalizedValue(int entityId, string localeValue, string localeKeyGroup, string localeKey, int languageId)
        //{
        //    return SaveLocalizedValue<string>(entityId, localeValue, localeKeyGroup, localeKey, languageId);
        //}

        /// <summary>
        /// Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <param name="languageId">Language identifier</param>
        public virtual bool SaveLocalizedValue<T>(int entityId, T localeValue, string localeKeyGroup, string localeKey, int languageId)
        {
            if (languageId == 0)
                throw new ArgumentOutOfRangeException("languageId", "Language ID should not be 0");

            var props = GetLocalizedProperties(entityId, localeKeyGroup);
            var prop = props.FirstOrDefault(p => p.LanguageId == languageId && p.LocaleKey.Equals(localeKey, StringComparison.InvariantCultureIgnoreCase)); //should be culture invariant

            var localeValueStr = localeValue.ToString(); //CommonHelper.To<string>(localeValue);
            var result = false;

            if (prop != null)
            {
                if (string.IsNullOrWhiteSpace(localeValueStr))
                {
                    //delete
                    //result = DeleteLocalizedProperty(prop);
                    Entities.Remove(prop);
                }
                else
                {
                    //update
                    prop.LocaleValue = localeValueStr;
                    //result = UpdateLocalizedProperty(prop);
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(localeValueStr))
                {
                    //insert
                    prop = new LocalizedProperty
                    {
                        EntityId = entityId,
                        LanguageId = languageId,
                        LocaleKey = localeKey,
                        LocaleKeyGroup = localeKeyGroup,
                        LocaleValue = localeValueStr
                    };
                    Entities.Add(prop);
                    //result = InsertLocalizedProperty(prop);
                }
            }
            return result;
        }

        /// <summary>
        /// Save localized value for entity
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="languageId">Language identifier</param>
        /// <param name="saveNonLocalizedProperty">saveNonLocalizedProperty</param>
        public virtual bool SaveLocalizedValue<T>(T entity, int languageId
            //, bool saveNonLocalizedProperty = false
            ) where T : BaseEntity, ILocalizedEntity
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            var props = typeof(T).GetProperties().Where(p => (Attribute.IsDefined(p, typeof(LocalizedPropertyAttribute)) && p.CanRead && p.CanWrite)/* || p.Name == "Id"*/);
            var result = 0;
            var count = 0;


            foreach (var prop in props)
            {

                count++;
                var localeKeyGroup = typeof(T).Name;
                var localeKey = prop.Name;

                //Duck typing is not supported in C#. That's why we're using dynamic type
                dynamic value = prop.GetValue(entity, null);
                if (value != null)
                    result += SaveLocalizedValue(entity.Id, value, localeKeyGroup, localeKey, languageId) ? 1 : 0;
                else
                    result += SaveLocalizedValue(entity.Id, "", localeKeyGroup, localeKey, languageId) ? 1 : 0;
            }

            result = Context.SaveChanges();
            _cacheManager.RemoveByPattern(LOCALIZEDPROPERTY_PATTERN_KEY);
            return result == count;

            //var success = true;
            //if (saveNonLocalizedProperty)
            //{
            //    success = false;
            //    var entry = _localizedPropertyRepository.Context.Entry(entity);
            //    foreach (var prop in props)
            //    {
            //        entry.Property(prop.Name).IsModified = false;
            //    }
            //    success = _localizedPropertyRepository.Context.SaveChanges() > 0;
            //}
            //return success && result == count;
        }

        public bool GetLocalizedProperty(int entityId, string localeKeyGroup, string localeKey, string LocalValue)
        {
            return TableNoTracking.Any(x => x.EntityId != entityId &&
            x.LocaleKeyGroup == localeKeyGroup &&
            x.LocaleKey == localeKey &&
            x.LocaleValue == LocalValue);
        }

        #endregion

        #region Utilities async
        /// <summary>
        /// Gets localized properties
        /// </summary>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <returns>Localized properties</returns>
        protected virtual async Task<IList<LocalizedProperty>> GetLocalizedPropertiesAsync(int entityId, string localeKeyGroup)
        {
            if (entityId == 0 || !localeKeyGroup.HasValue())
                return new List<LocalizedProperty>();
            return await Table.OrderBy(p => p.Id).Where(p => p.EntityId == entityId && p.LocaleKeyGroup == localeKeyGroup).ToListAsync();
        }

        /// <summary>
        /// Gets all cached localized properties
        /// </summary>
        /// <returns>Cached localized properties</returns>
        protected virtual async Task<IList<LocalizedPropertyForCaching>> GetAllLocalizedPropertiesCachedAsync()
        {
            //cache
            var key = LOCALIZEDPROPERTY_ALL_KEY;
            return await _cacheManager.GetAsync(key, () =>
            {
                return TableNoTracking.Select(p => new LocalizedPropertyForCaching
                {
                    Id = p.Id,
                    EntityId = p.EntityId,
                    LanguageId = p.LanguageId,
                    LocaleKeyGroup = p.LocaleKeyGroup,
                    LocaleKey = p.LocaleKey,
                    LocaleValue = p.LocaleValue
                }).ToListAsync();
            });
        }
        #endregion

        #region GetLocalizedValue
        /// <summary>
        /// Find localized value by type
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="languageId">Language ID</param>
        public virtual Task<string> GetLocalizedValueAsync<T>(T entity, Expression<Func<T, string>> keySelector, int languageId) where T : BaseEntity, ILocalizedEntity
        {
            return GetLocalizedValueAsync<T, string>(entity, keySelector, languageId);
        }

        /// <summary>
        /// Find localized value by type
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <typeparam name="TPropType">TPropType</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="languageId">Language ID</param>
        /// <returns></returns>
        public virtual Task<TPropType> GetLocalizedValueAsync<T, TPropType>(T entity, Expression<Func<T, TPropType>> keySelector, int languageId) where T : BaseEntity, ILocalizedEntity
        {
            CheckHelper.NotNull(entity, nameof(entity));

            var propInfo = keySelector.GetPropInfo();
            var localeKeyGroup = typeof(T).Name;
            var localeKey = propInfo.Name;

            return GetLocalizedValueAsync<TPropType>(languageId, entity.Id, localeKeyGroup, localeKey);
        }

        /// <summary>
        /// Find localized value
        /// </summary>
        /// <param name="languageId">Language identifier</param>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <returns>Found localized value</returns>
        public virtual Task<string> GetLocalizedValueAsync(int languageId, int entityId, string localeKeyGroup, string localeKey)
        {
            return GetLocalizedValueAsync<string>(languageId, entityId, localeKeyGroup, localeKey);
        }

        /// <summary>
        /// Find localized value by type
        /// </summary>
        /// <typeparam name="T">type of value</typeparam>
        /// <param name="languageId">Language identifier</param>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <returns>Found localized value</returns>
        /// <summary>
        public virtual async Task<T> GetLocalizedValueAsync<T>(int languageId, int entityId, string localeKeyGroup, string localeKey)
        {
            if (languageId == 0)
                throw new ArgumentOutOfRangeException(nameof(languageId), "Language ID should not be 0");

            var key = string.Format(LOCALIZEDPROPERTY_KEY, languageId, entityId, localeKeyGroup, localeKey);
            if (_localizationSettings.LoadAllLocalizedPropertiesOnStartup)
            {
                var value = await _cacheManager.GetAsync(key, async () =>
                {
                    //load all records (we know they are cached)
                    var source = await GetAllLocalizedPropertiesCachedAsync();
                    var localeValue = source.Where(p => p.LanguageId == languageId && p.EntityId == entityId && p.LocaleKeyGroup == localeKeyGroup && p.LocaleKey == localeKey)
                        .Select(p => p.LocaleValue).FirstOrDefault();
                    //little hack here. nulls aren't cacheable so set it to ""
                    return localeValue ?? "";
                });
                return value.ConvertTo<T>();
            }
            else
            {
                //gradual loading
                var value = await _cacheManager.GetAsync(key, async () =>
                {
                    var localeValue = await TableNoTracking.Where(p => p.LanguageId == languageId && p.EntityId == entityId && p.LocaleKeyGroup == localeKeyGroup && p.LocaleKey == localeKey)
                        .Select(p => p.LocaleValue).FirstOrDefaultAsync();
                    //little hack here. nulls aren't cacheable so set it to ""
                    return localeValue ?? "";
                });
                return value.ConvertTo<T>();
            }
        }

        /// <summary>
        /// Find localized entity
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="languageId">Language ID</param>
        public virtual async Task<T> GetLocalizedValueAsync<T>(T entity, int languageId) where T : BaseEntity, ILocalizedEntity
        {
            CheckHelper.NotNull(entity, nameof(entity));

            var props = typeof(T).GetProperties().Where(p => Attribute.IsDefined(p, typeof(LocalizedPropertyAttribute)) && p.CanRead && p.CanWrite);

            foreach (var prop in props)
            {
                var localeKeyGroup = typeof(T).Name;
                var localeKey = prop.Name;

                var valueStr = await GetLocalizedValueAsync(languageId, entity.Id, localeKeyGroup, localeKey);
                var value = Convert.ChangeType(valueStr, prop.PropertyType);

                //convert string value (from database) to type of property
                //object value = CommonHelper.GetNopCustomTypeConverter(prop.PropertyType).ConvertFromInvariantString(setting);
                //set property
                prop.SetValue(entity, value, null);
            }
            return entity;
        }
        #endregion

        #region SaveLocalizedValue
        /// <summary>
        /// Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="languageId">Language ID</param>
        public virtual Task SaveLocalizedValueAsync<T>(T entity, Expression<Func<T, string>> keySelector, string localeValue, int languageId) where T : BaseEntity, ILocalizedEntity
        {
            return SaveLocalizedValueAsync<T, string>(entity, keySelector, localeValue, languageId);
        }

        /// <summary>
        /// Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <typeparam name="TPropType">TPropType</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="languageId">Language ID</param>
        public virtual Task SaveLocalizedValueAsync<T, TPropType>(T entity, Expression<Func<T, TPropType>> keySelector, TPropType localeValue, int languageId) where T : BaseEntity, ILocalizedEntity
        {
            CheckHelper.NotNull(entity, nameof(entity));

            var propInfo = keySelector.GetPropInfo();
            var localeKeyGroup = typeof(T).Name;
            var localeKey = propInfo.Name;

            return SaveLocalizedValueAsync(entity.Id, localeValue, localeKeyGroup, localeKey, languageId);
        }

        ///// <summary>
        ///// Save localized value
        ///// </summary>
        ///// <param name="entityId">Entity identifier</param>
        ///// <param name="localeValue">Locale value</param>
        ///// <param name="localeKeyGroup">Locale key group</param>
        ///// <param name="localeKey">Locale key</param>
        ///// <param name="languageId">Language identifier</param>
        //public virtual bool SaveLocalizedValue(int entityId, string localeValue, string localeKeyGroup, string localeKey, int languageId)
        //{
        //    return SaveLocalizedValue<string>(entityId, localeValue, localeKeyGroup, localeKey, languageId);
        //}

        /// <summary>
        /// Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <param name="languageId">Language identifier</param>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="languageId"/></exception>
        public virtual async Task SaveLocalizedValueAsync<T>(int entityId, T localeValue, string localeKeyGroup, string localeKey, int languageId)
        {
            if (languageId == 0)
                throw new ArgumentOutOfRangeException(nameof(languageId), "Language ID should not be 0");

            var props = await GetLocalizedPropertiesAsync(entityId, localeKeyGroup);
            var prop = props.FirstOrDefault(p => p.LanguageId == languageId && p.LocaleKey.Equals(localeKey, StringComparison.InvariantCultureIgnoreCase)); //should be culture invariant

            var localeValueStr = localeValue.ToString(); //CommonHelper.To<string>(localeValue);
            if (prop != null)
            {
                if (localeValueStr.HasValue(true))
                {
                    //delete
                    //result = DeleteLocalizedProperty(prop);
                    Entities.Remove(prop);
                }
                else
                {
                    //update
                    prop.LocaleValue = localeValueStr;
                    //result = UpdateLocalizedProperty(prop);
                }
            }
            else
            {
                if (localeValueStr.HasValue(true))
                {
                    //insert
                    prop = new LocalizedProperty
                    {
                        EntityId = entityId,
                        LanguageId = languageId,
                        LocaleKey = localeKey,
                        LocaleKeyGroup = localeKeyGroup,
                        LocaleValue = localeValueStr
                    };
                    Entities.Add(prop);
                    //result = InsertLocalizedProperty(prop);
                }
            }
        }

        /// <summary>
        /// Save localized value for entity
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="languageId">Language identifier</param>
        /// <param name="saveNonLocalizedProperty">saveNonLocalizedProperty</param>
        public virtual async Task SaveLocalizedValueAsync<T>(T entity, int languageId /*, bool saveNonLocalizedProperty = false*/) where T : BaseEntity, ILocalizedEntity
        {
            CheckHelper.NotNull(entity, nameof(entity));

            var props = typeof(T).GetProperties().Where(p => (Attribute.IsDefined(p, typeof(LocalizedPropertyAttribute)) && p.CanRead && p.CanWrite)/* || p.Name == "Id"*/);

            foreach (var prop in props)
            {
                var localeKeyGroup = typeof(T).Name;
                var localeKey = prop.Name;

                //Duck typing is not supported in C#. That's why we're using dynamic type
                dynamic value = prop.GetValue(entity, null);
                if (value != null)
                    await SaveLocalizedValueAsync(entity.Id, value, localeKeyGroup, localeKey, languageId);
                else
                    await SaveLocalizedValueAsync(entity.Id, "", localeKeyGroup, localeKey, languageId);
            }
            await Context.SaveChangesAsync();
            _cacheManager.RemoveByPattern(LOCALIZEDPROPERTY_PATTERN_KEY);
        }
        #endregion
    }
}
