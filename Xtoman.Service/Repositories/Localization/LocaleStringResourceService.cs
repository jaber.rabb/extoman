﻿using System;
using System.Data.Entity.Migrations;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class LocaleStringResourceService : RepositoryBase<LocaleStringResource>, ILocaleStringResourceService
    {
        public LocaleStringResourceService(IUnitOfWork uow) : base(uow)
        {
        }

        /// <summary>
        /// AddOrUpdate entity
        /// </summary>
        /// <param name="entity">Entity</param>
        public virtual bool AddOrUpdateResource(LocaleStringResource entity)
        {
            Entities.AddOrUpdate(p => new { p.LanguageId, p.Name }, entity);
            return Convert.ToBoolean(Context.SaveChanges());
        }
    }
}
