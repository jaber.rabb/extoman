﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IFinnotechResponseCardInfoService : IRepository<FinnotechResponseCardInfo>
    {
    }
}
