﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class FinnotechResponseService : RepositoryBase<Finnotech>, IFinnotechResponseService
    {
        public FinnotechResponseService(IUnitOfWork context) : base(context)
        {
        }
    }
}
