﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class FinnotechResponseIdVerificationService : RepositoryBase<FinnotechResponseIdVerification>, IFinnotechResponseIdVerificationService
    {
        public FinnotechResponseIdVerificationService(IUnitOfWork context) : base(context)
        {
        }
    }
}
