﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class FinnotechResponseBalanceService : RepositoryBase<FinnotechResponseBalance>, IFinnotechResponseBalanceService
    {
        public FinnotechResponseBalanceService(IUnitOfWork context) : base(context)
        {
        }
    }
}
