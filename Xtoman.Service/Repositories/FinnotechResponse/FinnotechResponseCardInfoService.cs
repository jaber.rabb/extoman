﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class FinnotechResponseCardInfoService : RepositoryBase<FinnotechResponseCardInfo>, IFinnotechResponseCardInfoService
    {
        public FinnotechResponseCardInfoService(IUnitOfWork context) : base(context)
        {
        }
    }
}