﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class FinnotechResponseTransferService : RepositoryBase<FinnotechResponseTransferTo>, IFinnotechResponseTransferService
    {
        public FinnotechResponseTransferService(IUnitOfWork context) : base(context)
        {
        }
    }
}
