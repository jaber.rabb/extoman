﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Utility;

namespace Xtoman.Service
{
    public class ApiTokenService : RepositoryBase<ApiToken>, IApiTokenService
    {
        public ApiTokenService(IUnitOfWork context) : base(context)
        {
        }

        public async Task CreateApiToken(ApiToken ApiToken)
        {
            await InvalidateApiTokens(ApiToken.UserId);
            Entities.Add(ApiToken);
        }

        public async Task UpdateApiToken(int userId, string accessTokenHash)
        {
            var token = await Entities.FirstOrDefaultAsync(x => x.UserId == userId);
            if (token != null)
                token.AccessTokenHash = accessTokenHash;
        }

        public async Task DeleteExpiredTokens()
        {
            var now = DateTime.UtcNow;
            var ApiTokens = await Entities.Where(x => x.RefreshTokenExpiresUtc < now).ToListAsync();
            foreach (var ApiToken in ApiTokens)
                Entities.Remove(ApiToken);
        }

        public async Task DeleteToken(string refreshTokenIdHash)
        {
            var token = await FindTokenByHash(refreshTokenIdHash);
            if (token != null)
                Entities.Remove(token);
        }

        public Task<ApiToken> FindTokenByHash(string refreshTokenIdHash)
        {
            return Entities.FirstOrDefaultAsync(x => x.RefreshTokenIdHash == refreshTokenIdHash);
        }

        public async Task InvalidateApiTokens(int userId)
        {
            var ApiTokens = await Entities.Where(x => x.UserId == userId).ToListAsync();
            foreach (var ApiToken in ApiTokens)
                Entities.Remove(ApiToken);
        }

        public async Task<bool> IsValidToken(string accessToken, int userId)
        {
            var accessTokenHash = accessToken.ToSHA256Hash();
            var ApiToken = await Entities.FirstOrDefaultAsync(x => x.AccessTokenHash == accessTokenHash && x.UserId == userId);
            return true;
        }
    }
}
