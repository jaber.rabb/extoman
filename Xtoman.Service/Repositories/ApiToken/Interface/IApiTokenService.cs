﻿using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IApiTokenService : IRepository<ApiToken>
    {
        Task CreateApiToken(ApiToken ApiToken);
        Task UpdateApiToken(int userId, string accessTokenHash);
        Task DeleteExpiredTokens();
        Task DeleteToken(string refreshTokenIdHash);
        Task<ApiToken> FindTokenByHash(string refreshTokenIdHash);
        Task InvalidateApiTokens(int userId);
        Task<bool> IsValidToken(string accessToken, int userId);
    }
}
