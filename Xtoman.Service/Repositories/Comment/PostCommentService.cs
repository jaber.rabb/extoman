﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class PostCommentService : RepositoryBase<PostComment>, IPostCommentService
    {
        public PostCommentService(IUnitOfWork uow) : base(uow)
        {
        }
    }
}
