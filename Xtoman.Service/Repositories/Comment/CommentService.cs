﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class CommentService : RepositoryBase<Comment>, ICommentService
    {
        public CommentService(IUnitOfWork uow) : base(uow)
        {
        }
    }
}
