﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class NewsCategoryService : RepositoryBase<NewsCategory>, INewsCategoryService
    {
        public NewsCategoryService(IUnitOfWork context) : base(context)
        {
        }

    }
}