﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class ProductCategoryService : RepositoryBase<ProductCategory>, IProductCategoryService
    {
        public ProductCategoryService(IUnitOfWork context) : base(context)
        {
        }
    }
}
