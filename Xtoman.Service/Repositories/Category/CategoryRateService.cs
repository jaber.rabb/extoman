﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class CategoryRateService : RepositoryBase<CategoryRate>, ICategoryRateService
    {
        public CategoryRateService(IUnitOfWork context) : base(context)
        {
        }
    }
}
