﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class CategoryService : RepositoryBase<Category>, ICategoryService
    {
        public CategoryService(IUnitOfWork context) : base(context)
        {
        }

    }
}