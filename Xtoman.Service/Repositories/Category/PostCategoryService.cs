﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class PostCategoryService : RepositoryBase<PostCategory>, IPostCategoryService
    {
        public PostCategoryService(IUnitOfWork context) : base(context)
        {
        }

    }
}