﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface INewsCategoryService : IRepository<NewsCategory>
    {
    }
}