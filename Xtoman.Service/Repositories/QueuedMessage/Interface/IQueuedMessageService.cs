﻿using System.Collections.Generic;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IQueuedMessageService : IRepository<QueuedMessage>
    {
        List<QueuedMessage> GetAllNotSent(int maxTry = 3);
    }
}