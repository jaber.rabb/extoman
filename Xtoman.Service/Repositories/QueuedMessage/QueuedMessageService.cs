﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class QueuedMessageService : RepositoryBase<QueuedMessage>, IQueuedMessageService
    {
        public QueuedMessageService(IUnitOfWork context) : base(context)
        {
        }

        public List<QueuedMessage> GetAllNotSent(int maxTry = 3)
        {
            return Table.Where(p => !p.IsSent && p.TryCount < maxTry)
                .Include(p => p.EmailAccount)
                .ToList();
        }
    }
}
