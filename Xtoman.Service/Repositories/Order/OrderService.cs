﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class OrderService : RepositoryBase<Order>, IOrderService
    {
        public OrderService(IUnitOfWork context) : base(context)
        {
        }

        public async Task<decimal> AmountByGuidAsync(string guid)
        {
            return await TableNoTracking.Where(p => p.Guid.ToString() == guid).Select(p => p.PayAmount).SingleAsync();
        }

        public decimal AmountByGuid(string guid)
        {
            return TableNoTracking.Where(p => p.Guid.ToString() == guid).Select(p => p.PayAmount).Single();
        }
    }
}