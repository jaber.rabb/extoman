﻿using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IOrderService : IRepository<Order>
    {
        Task<decimal> AmountByGuidAsync(string guid);
        decimal AmountByGuid(string guid);
    }
}
