﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class OrderNoteService : RepositoryBase<OrderNote>, IOrderNoteService
    {
        public OrderNoteService(IUnitOfWork context) : base(context)
        {
        }
    }
}
