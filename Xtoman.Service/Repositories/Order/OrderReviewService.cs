﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class OrderReviewService : RepositoryBase<OrderReview>, IOrderReviewService
    {
        public OrderReviewService(IUnitOfWork context) : base(context)
        {
        }
    }
}
