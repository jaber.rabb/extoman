﻿using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IPostService : IRepository<Post>
    {
        Task<bool> DeleteTagsAsync(int id);
        Task<bool> DeleteCategoriesAsync(int id);
        Task<bool> DeleteMediaAsync(int id);
    }
}
