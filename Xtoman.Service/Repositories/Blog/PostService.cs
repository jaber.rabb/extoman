﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Utility;

namespace Xtoman.Service
{
    public class PostService : RepositoryBase<Post>, IPostService
    {
        public PostService(IUnitOfWork context) : base(context)
        {
        }

        public async Task<bool> DeleteCategoriesAsync(int id)
        {
            var entity = await Table.Include(p => p.Categories).SingleAsync(p => p.Id == id);
            var list = entity.Categories.ToList();
            list.ForEach(p => entity.Categories.Remove(p));
            return Convert.ToBoolean(await Context.SaveChangesAsync());
        }

        public async Task<bool> DeleteMediaAsync(int id)
        {
            var entity = await Table.Include(p => p.Media).SingleAsync(p => p.Id == id);
            entity.MediaId = null;
            entity.Medias.ToList().ForEach(p => entity.Medias.Remove(p));
            return Convert.ToBoolean(await Context.SaveChangesAsync());
        }

        public async Task<bool> DeleteTagsAsync(int id)
        {
            var entity = await Table.Include(p => p.Tags).SingleAsync(p => p.Id == id);
            var list = entity.Tags.ToList();
            list.ForEach(p => entity.Tags.Remove(p));
            return Convert.ToBoolean(await Context.SaveChangesAsync());
        }
    }
}
