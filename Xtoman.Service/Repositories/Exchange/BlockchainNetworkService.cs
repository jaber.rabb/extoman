﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class BlockchainNetworkService : RepositoryBase<BlockchainNetwork>, IBlockchainNetworkService
    {
        public BlockchainNetworkService(IUnitOfWork context) : base(context)
        {
        }
    }
}
