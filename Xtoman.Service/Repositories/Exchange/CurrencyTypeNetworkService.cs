﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class CurrencyTypeNetworkService : RepositoryBase<CurrencyTypeNetwork>, ICurrencyTypeNetworkService
    {
        public CurrencyTypeNetworkService(IUnitOfWork context) : base(context)
        {
        }
    }
}
