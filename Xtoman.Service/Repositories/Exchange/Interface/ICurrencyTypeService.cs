﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface ICurrencyTypeService : IRepository<CurrencyType>
    {
        List<CurrencyType> GetAllCached();
        List<CurrencyType> GetAllActiveCached();
        Task<List<CurrencyType>> GetAllCachedAsync();
        Task<List<CurrencyType>> GetAllActiveCachedAsync();
        Task<int> GetCachedByTypeAsync(ECurrencyType tether);
        int GetCachedByType(ECurrencyType tether);
    }
}
