﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Utility;
using Xtoman.Utility.Caching;
using System.Linq;

namespace Xtoman.Service
{
    public class CurrencyTypeService : CacheRepositoryBase<CurrencyType, CurrencyTypeForCaching>, ICurrencyTypeService
    {
        public CurrencyTypeService(IUnitOfWork context, ICacheManager cacheManager, CacheSettings cacheSettings) : base(context, cacheManager, cacheSettings)
        {
        }

        public List<CurrencyType> GetAllActiveCached()
        {
            var allCached = GetAllCached();
            return allCached.Where(p => !p.Disabled && !p.Invisible).ToList();
        }

        public async Task<List<CurrencyType>> GetAllActiveCachedAsync()
        {
            var allCached = await GetAllCachedAsync();
            return allCached.Where(p => !p.Disabled && !p.Invisible).ToList();
        }

        public List<CurrencyType> GetAllCached()
        {
            var cachedEntity = GetAllCached(p => new CurrencyTypeForCaching
            {
                AddressRegEx = p.AddressRegEx,
                AlternativeName = p.AlternativeName,
                Available = p.Available,
                CoinMarketCapId = p.CoinMarketCapId,
                Description = p.Description,
                Disabled = p.Disabled,
                HasMemo = p.HasMemo,
                Id = p.Id,
                IsFiat = p.IsFiat,
                MaxPrecision = p.MaxPrecision,
                MediaId = p.MediaId,
                MemoDescription = p.MemoDescription,
                MemoName = p.MemoName,
                MemoRegEx = p.MemoRegEx,
                MinimumReceiveAmount = p.MinimumReceiveAmount,
                Name = p.Name,
                Step = p.Step,
                TransactionFee = p.TransactionFee,
                Type = p.Type,
                UnitSign = p.UnitSign,
                UnitSignIsAfter = p.UnitSignIsAfter,
                Url = p.Url,
                IsERC20 = p.IsERC20,
                ConfirmsNeed = p.ConfirmsNeed,
                Invisible = p.Invisible,
            });
            var result = new List<CurrencyType>();
            foreach (var item in cachedEntity)
                result.Add(item.CastToClass<CurrencyType>());

            return result;
        }


        public async Task<List<CurrencyType>> GetAllCachedAsync()
        {
            var cachedEntity = await GetAllCachedAsync(p => new CurrencyTypeForCaching
            {
                AddressRegEx = p.AddressRegEx,
                AlternativeName = p.AlternativeName,
                Available = p.Available,
                CoinMarketCapId = p.CoinMarketCapId,
                Description = p.Description,
                Disabled = p.Disabled,
                HasMemo = p.HasMemo,
                Id = p.Id,
                IsFiat = p.IsFiat,
                MaxPrecision = p.MaxPrecision,
                MediaId = p.MediaId,
                MemoDescription = p.MemoDescription,
                MemoName = p.MemoName,
                MemoRegEx = p.MemoRegEx,
                MinimumReceiveAmount = p.MinimumReceiveAmount,
                Name = p.Name,
                Step = p.Step,
                TransactionFee = p.TransactionFee,
                Type = p.Type,
                UnitSign = p.UnitSign,
                UnitSignIsAfter = p.UnitSignIsAfter,
                Url = p.Url,
                IsERC20 = p.IsERC20,
                Invisible = p.Invisible,
                ConfirmsNeed = p.ConfirmsNeed
            });
            var result = new List<CurrencyType>();
            foreach (var item in cachedEntity)
                result.Add(item.CastToClass<CurrencyType>());

            return result;
        }

        public int GetCachedByType(ECurrencyType tether)
        {
            var all = GetAllActiveCached();
            return all.Where(p => p.Type == ECurrencyType.Tether).Select(p => p.Id).FirstOrDefault();
        }

        public async Task<int> GetCachedByTypeAsync(ECurrencyType tether)
        {
            var all = await GetAllActiveCachedAsync();
            return all.Where(p => p.Type == ECurrencyType.Tether).Select(p => p.Id).FirstOrDefault();
        }
    }
}
