﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class ExchangeOrderService : RepositoryBase<ExchangeOrder>, IExchangeOrderService
    {
        public ExchangeOrderService(IUnitOfWork context) : base(context)
        {
        }
    }
}