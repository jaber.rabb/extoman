﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class ExchangeTypeService : RepositoryBase<ExchangeType>, IExchangeTypeService
    {
        public ExchangeTypeService(IUnitOfWork context) : base(context)
        {
        }
    }
}
