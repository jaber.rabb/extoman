﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class ExchangeTypeRateService : RepositoryBase<ExchangeTypeRate>, IExchangeTypeRateService
    {
        public ExchangeTypeRateService(IUnitOfWork context) : base(context)
        {
        }
    }
}
