﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class MediaThumbnailService : RepositoryBase<MediaThumbnail>, IMediaThumbnailService
    {
        public MediaThumbnailService(IUnitOfWork context) : base(context)
        {
        }
    }
}
