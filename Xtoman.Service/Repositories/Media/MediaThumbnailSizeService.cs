﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service
{
    public class MediaThumbnailSizeService : CacheRepositoryBase<MediaThumbnailSize, MediaThumbnailSizeCache>, IMediaThumbnailSizeService
    {
        public MediaThumbnailSizeService(IUnitOfWork context, ICacheManager cacheManager, CacheSettings cacheSettings) : base(context, cacheManager, cacheSettings)
        {
        }

        public async Task<List<MediaThumbnailSize>> GetAllCachedAsync()
        {
            var cachedEntity = await GetAllCachedAsync(p => new MediaThumbnailSizeCache
            {
                Crop = p.Crop,
                Height = p.Height,
                Id = p.Id,
                Name = p.Name,
                Watermark = p.Watermark,
                Width = p.Width
            });
            var result = new List<MediaThumbnailSize>();
            foreach (var item in cachedEntity)
            {
                result.Add(item.CastToClass<MediaThumbnailSize>());
            }
            return result;
        }

        public async Task<MediaThumbnailSize> GetCachedByIdAsync(int id)
        {
            var cachedEntity = await GetCachedByIdAsync(id, p => new MediaThumbnailSizeCache
            {
                Crop = p.Crop,
                Height = p.Height,
                Id = p.Id,
                Name = p.Name,
                Watermark = p.Watermark,
                Width = p.Width
            });

            return cachedEntity.CastToClass<MediaThumbnailSize>();
        }
    }
}
