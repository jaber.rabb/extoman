﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IMediaThumbnailSizeService : IRepository<MediaThumbnailSize>
    {
        Task<List<MediaThumbnailSize>> GetAllCachedAsync();
        Task<MediaThumbnailSize> GetCachedByIdAsync(int id);
    }
}
