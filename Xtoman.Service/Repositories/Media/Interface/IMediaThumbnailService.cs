﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IMediaThumbnailService : IRepository<MediaThumbnail>
    {
    }
}
