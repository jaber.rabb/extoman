﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class PayeerStatusService : RepositoryBase<PAYEERPaymentStatus>, IPayeerStatusService
    {
        public PayeerStatusService(IUnitOfWork context) : base(context)
        {
        }
    }
}
