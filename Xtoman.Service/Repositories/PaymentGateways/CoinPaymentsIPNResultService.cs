﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class CoinPaymentsIPNResultService : RepositoryBase<CoinPaymentsIPNResult>, ICoinPaymentsIPNResultService
    {
        public CoinPaymentsIPNResultService(IUnitOfWork context) : base(context)
        {
        }
    }
}
