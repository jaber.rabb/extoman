﻿using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class PerfectMoneyStatusService : RepositoryBase<PerfectMoneyPaymentStatus>, IPerfectMoneyStatusService
    {
        public PerfectMoneyStatusService(IUnitOfWork context) : base(context)
        {
        }
    }
}
