﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class PostTagService : RepositoryBase<PostTag>, IPostTagService
    {
        public PostTagService(IUnitOfWork context) : base(context)
        {
        }

        public async Task<List<PostTag>> AddIfNotExistsListAsync(string[] tags)
        {
            var existeds = await TableNoTracking.Where(p => tags.Any(x => x == p.Name)).ToListAsync();
            var existedNames = existeds.Select(p => p.Name).ToList();
            var toInsert = new List<PostTag>();

            foreach (var name in tags)
            {
                if (!existedNames.Any(p => p == name))
                {
                    toInsert.Add(new PostTag() { Name = name });
                }
            }

            await InsertAsync(toInsert);
            return toInsert.Concat(existeds).ToList();
        }
    }
}