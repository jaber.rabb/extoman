﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IPostTagService : IRepository<PostTag>
    {
        Task<List<PostTag>> AddIfNotExistsListAsync(string[] tags);
    }
}
