﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class TradeService : RepositoryBase<Trade>, ITradeService
    {
        public TradeService(IUnitOfWork context) : base(context)
        {
        }
    }
}
