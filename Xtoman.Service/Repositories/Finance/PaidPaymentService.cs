﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class PaidPaymentService : RepositoryBase<PaidPayment>, IPaidPaymentService
    {
        public PaidPaymentService(IUnitOfWork context) : base(context)
        {
        }
    }
}
