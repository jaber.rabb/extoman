﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class ManualTradingDepositService : RepositoryBase<ManualTradingDeposit>, IManualTradingDepositService
    {
        public ManualTradingDepositService(IUnitOfWork context) : base(context)
        {
        }
    }
}
