﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class WithdrawService : RepositoryBase<Withdraw>, IWithdrawService
    {
        public WithdrawService(IUnitOfWork context) : base(context)
        {
        }
    }
}
