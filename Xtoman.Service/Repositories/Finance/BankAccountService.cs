﻿using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public class BankAccountService : RepositoryBase<BankAccount>, IBankAccountService
    {
        public BankAccountService(IUnitOfWork context) : base(context)
        {
        }
    }
}
