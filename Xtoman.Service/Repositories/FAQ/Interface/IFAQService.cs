﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IFAQService : IRepository<FAQ>
    {
        Task<List<FAQ>> GetAllCachedAsync();
    }
}
