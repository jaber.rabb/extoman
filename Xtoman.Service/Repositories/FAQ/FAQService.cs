﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service
{
    public class FAQService : CacheRepositoryBase<FAQ, FAQCached>, IFAQService
    {
        public FAQService(IUnitOfWork context, ICacheManager cacheManager, CacheSettings cacheSettings) : base(context, cacheManager, cacheSettings)
        {
        }

        public async Task<List<FAQ>> GetAllCachedAsync()
        {
            var cachedEntity = await GetAllCachedAsync(p => new FAQCached
            {
                Answer = p.Answer,
                GroupName = p.GroupName,
                Id = p.Id,
                Question = p.Question,
                Url = p.Url
            });
            var result = new List<FAQ>();
            foreach (var item in cachedEntity)
            {
                result.Add(item.CastToClass<FAQ>());
            }
            return result;
        }
    }
}
