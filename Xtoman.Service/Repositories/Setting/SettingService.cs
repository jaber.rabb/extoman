﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xtoman.Data;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Utility;
using Xtoman.Utility.Caching;

namespace Xtoman.Service
{
    //public class SettingService : RepositoryBase<Setting>, ISettingService
    //{
    //    public SettingService(IUnitOfWork context) : base(context)
    //    {
    //    }
    //}

    /// <summary>
    /// Setting manager
    /// </summary>
    public class SettingService : ISettingService
    {
        #region Constants
        /// <summary>
        /// Key for caching
        /// </summary>
        private const string SETTINGS_ALL_KEY = "Xtoman.setting.all";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string SETTINGS_PATTERN_KEY = "Xtoman.setting.";
        #endregion

        #region Fields
        private readonly IRepository<Setting> _settingRepository;
        //private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        #endregion

        #region Ctor
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="eventPublisher">Event publisher</param>
        /// <param name="settingRepository">Setting repository</param>
        public SettingService(ICacheManager cacheManager, /*IEventPublisher eventPublisher,*/
            IRepository<Setting> settingRepository)
        {
            _cacheManager = cacheManager;
            //_eventPublisher = eventPublisher;
            _settingRepository = settingRepository;
        }
        #endregion

        #region Utilities
        /// <summary>
        /// Gets all settings
        /// </summary>
        /// <returns>Settings</returns>
        protected virtual async Task<IList<SettingCached>> GetAllSettingsCachedAsync()
        {
            //cache
            const string key = SETTINGS_ALL_KEY;
            return await _cacheManager.GetAsync(key, async () =>
            {
                //we use no tracking here for performance optimization
                //anyway records are loaded only for read-only operations
                var settings = await GetAllSettingsAsync();
                var list = settings.Select(p => new SettingCached
                {
                    Id = p.Id,
                    Name = p.Name,
                    Value = p.Value,
                }).ToList();
                return list;
            });
        }

        protected virtual IList<SettingCached> GetAllSettingsCached()
        {
            //cache
            const string key = SETTINGS_ALL_KEY;
            return _cacheManager.Get(key, () =>
            {
                //we use no tracking here for performance optimization
                //anyway records are loaded only for read-only operations
                var settings = GetAllSettings();
                var list = settings.Select(p => new SettingCached
                {
                    Id = p.Id,
                    Name = p.Name,
                    Value = p.Value,
                }).ToList();
                return list;
            });
        }
        #endregion

        #region Methods

        /// <summary>
        /// Adds a setting
        /// </summary>
        /// <param name="setting">Setting</param>
        /// <param name="clearCache">A value indicating whether to clear cache after setting update</param>
        public virtual async Task InsertSettingAsync(Setting setting, bool clearCache = true)
        {
            await _settingRepository.InsertAsync(setting);

            //cache
            if (clearCache)
                _cacheManager.RemoveByPattern(SETTINGS_PATTERN_KEY);

            //event notification
            //_eventPublisher.EntityInserted(setting);
        }

        /// <summary>
        /// Updates a setting
        /// </summary>
        /// <param name="setting">Setting</param>
        /// <param name="clearCache">A value indicating whether to clear cache after setting update</param>
        public virtual async Task UpdateSettingAsync(Setting setting, bool clearCache = true)
        {
            await _settingRepository.UpdateAsync(setting);

            //cache
            if (clearCache)
                _cacheManager.RemoveByPattern(SETTINGS_PATTERN_KEY);

            //event notification
            //_eventPublisher.EntityUpdated(setting);
        }

        /// <summary>
        /// Deletes a setting
        /// </summary>
        /// <param name="setting">Setting</param>
        public virtual async Task DeleteSettingAsync(Setting setting)
        {
            await _settingRepository.DeleteAsync(setting);

            //cache
            _cacheManager.RemoveByPattern(SETTINGS_PATTERN_KEY);

            //event notification
            //_eventPublisher.EntityDeleted(setting);
        }

        /// <summary>
        /// Deletes settings
        /// </summary>
        /// <param name="settings">Settings</param>
        public virtual async Task DeleteSettingsAsync(IList<Setting> settings)
        {
            await _settingRepository.DeleteAsync(settings);

            //cache
            _cacheManager.RemoveByPattern(SETTINGS_PATTERN_KEY);

            //event notification
            //foreach (var setting in settings)
            //    _eventPublisher.EntityDeleted(setting);
        }

        /// <summary>
        /// Gets a setting by identifier
        /// </summary>
        /// <param name="settingId">Setting identifier</param>
        /// <returns>Setting</returns>
        public virtual Task<Setting> GetSettingByIdAsync(int settingId)
        {
            return _settingRepository.GetByIdAsync(settingId);
        }

        public virtual T GetSettingWithoutCache<T>(string name)
        {
            return _settingRepository.TableNoTracking.Where(p => p.Name == name).Select(p => p.Value).Single().ConvertTo<T>();
        }

        /// <summary>
        /// Get setting by name
        /// </summary>
        /// <param name="name">name</param>
        /// <returns>Setting</returns>
        public virtual async Task<Setting> GetSettingAsync(string name)
        {
            var settings = await GetAllSettingsCachedAsync();
            var settingsByName = settings.SingleOrDefault(p => p.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
            if (settingsByName != null)
                return await GetSettingByIdAsync(settingsByName.Id);
            return null;
        }

        /// <summary>
        /// Get setting by name
        /// </summary>
        /// <param name="name">name</param>
        /// <returns>Setting</returns>
        public virtual async Task<SettingCached> GetSettingWithCacheAsync(string name)
        {
            var settings = await GetAllSettingsCachedAsync();
            var settingsByName = settings.SingleOrDefault(p => p.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
            return settingsByName;
        }

        public virtual SettingCached GetSettingWithCache(string name)
        {
            var settings = GetAllSettingsCached();
            var settingsByName = settings.SingleOrDefault(p => p.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
            return settingsByName;
        }

        /// <summary>
        /// Get setting value by name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="defaultValue">Default value</param>
        /// <returns>Setting value</returns>
        public virtual async Task<T> GetSettingByKeyAsync<T>(string name, T defaultValue = default(T))
        {
            var setting = await GetSettingWithCacheAsync(name);
            //var setting = GetSetting(name);
            if (setting != null)
                return setting.Value.ConvertTo<T>();
            return defaultValue;
        }

        public virtual T GetSettingByKey<T>(string name, T defaultValue = default(T))
        {
            var setting = GetSettingWithCache(name);
            //var setting = GetSetting(name);
            if (setting != null)
                return setting.Value.ConvertTo<T>();
            return defaultValue;
        }

        /// <summary>
        /// Set setting value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="name">name</param>
        /// <param name="value">Value</param>
        /// <param name="clearCache">A value indicating whether to clear cache after setting update</param>
        public virtual async Task SetSettingAsync(string name, object value, bool clearCache = true)
        {
            var valueStr = value == null ? null : Convert.ToString(value, CultureInfo.InvariantCulture);

            //var setting = GetSetting(name) ?? new Setting { Name = name, Value = valueStr };
            //var result = _settingRepository.AddOrUpdate(setting);
            //if (clearCache)
            //    _cacheManager.RemoveByPattern(SETTINGS_PATTERN_KEY);
            //return result;

            var setting = await GetSettingAsync(name);
            if (setting != null)
            {
                setting.Value = valueStr;
                await UpdateSettingAsync(setting, clearCache);
            }
            else
            {
                await InsertSettingAsync(new Setting
                {
                    Name = name,
                    Value = valueStr
                }, clearCache);
            }
        }

        /// <summary>
        /// Gets all settings
        /// </summary>
        /// <returns>Settings</returns>
        public virtual async Task<IList<Setting>> GetAllSettingsAsync()
        {
            return await _settingRepository.Table.OrderBy(p => p.Name).ToListAsync();
        }

        public virtual IList<Setting> GetAllSettings()
        {
            return _settingRepository.Table.OrderBy(p => p.Name).ToList();
        }

        /// <summary>
        /// Determines whether a setting exists
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <typeparam name="TPropType">Property type</typeparam>
        /// <param name="settings">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="storeId">Store identifier</param>
        /// <returns>true -setting exists; false - does not exist</returns>
        public virtual async Task<bool> SettingExistsAsync<T, TPropType>(T settings, Expression<Func<T, TPropType>> keySelector) where T : ISettings, new()
        {
            var key = settings.GetSettingKey(keySelector);
            var setting = await GetSettingByKeyAsync<string>(key);
            return setting != null;
        }

        /// <summary>
        /// Load settings
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        public virtual async Task<T> LoadSettingAsync<T>() where T : ISettings, new()
        {
            var settings = Activator.CreateInstance<T>();

            foreach (var prop in typeof(T).GetProperties().Where(p => p.CanRead && p.CanWrite))
            {
                var name = typeof(T).Name + "." + prop.Name;
                //load by store
                var setting = await GetSettingByKeyAsync<string>(name);
                if (setting == null)
                    continue;

                //convert string value (from database) to type of property
                object value = setting.ConvertTo(prop.PropertyType);
                //object value = CommonHelper.GetNopCustomTypeConverter(prop.PropertyType).ConvertFromInvariantString(setting);

                prop.SetValue(settings, value, null);
            }

            //var list = typeof(T).GetProperties().Where(p => p.CanRead && p.CanWrite).Select(p => typeof(T).Name + "." + p.Name).ToList();
            //var settingList = _settingRepository.TableNoTracking.Where(p => list.Contains(p.Name)).ToList();
            //foreach (var prop in typeof(T).GetProperties())
            //{
            //    var name = typeof(T).Name + "." + prop.Name;
            //    var setting = settingList.Single(p => p.Name == name);
            //    prop.SetValue(settings, setting, null);
            //}

            return settings;
        }

        public virtual T LoadSetting<T>() where T : ISettings, new()
        {
            return (T)LoadSetting(typeof(T));
        }

        public virtual object LoadSetting(Type type)
        {
            if (!typeof(ISettings).IsAssignableFrom(type))
                throw new ArgumentException("Type is not an ISettings", nameof(type));

            var settings = Activator.CreateInstance(type);

            foreach (var prop in type.GetProperties().Where(p => p.CanRead && p.CanWrite))
            {
                var name = type.Name + "." + prop.Name;
                //load by store
                var setting = GetSettingByKey<string>(name);
                if (setting == null)
                    continue;

                //convert string value (from database) to type of property
                object value = setting.ConvertTo(prop.PropertyType);
                //object value = CommonHelper.GetNopCustomTypeConverter(prop.PropertyType).ConvertFromInvariantString(setting);

                prop.SetValue(settings, value, null);
            }

            //var list = typeof(T).GetProperties().Where(p => p.CanRead && p.CanWrite).Select(p => typeof(T).Name + "." + p.Name).ToList();
            //var settingList = _settingRepository.TableNoTracking.Where(p => list.Contains(p.Name)).ToList();
            //foreach (var prop in typeof(T).GetProperties())
            //{
            //    var name = typeof(T).Name + "." + prop.Name;
            //    var setting = settingList.Single(p => p.Name == name);
            //    prop.SetValue(settings, setting, null);
            //}

            return settings;
        }

        /// <summary>
        /// Save settings object
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="settings">Setting instance</param>
        public virtual async Task SaveSettingAsync<T>(T settings) where T : ISettings, new()
        {
            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            foreach (var prop in typeof(T).GetProperties().Where(p => p.CanRead && p.CanWrite))
            {
                var key = typeof(T).Name + "." + prop.Name;
                //Duck typing is not supported in C#. That's why we're using dynamic type
                var value = prop.GetValue(settings, null);
                await SetSettingAsync(key, value, false);
            }
            //and now clear cache
            ClearCache();
        }

        /// <summary>
        /// Save settings object
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <typeparam name="TPropType">Property type</typeparam>
        /// <param name="settings">Settings</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="clearCache">A value indicating whether to clear cache after setting update</param>
        public virtual Task SaveSettingAsync<T, TPropType>(T settings, Expression<Func<T, TPropType>> keySelector, bool clearCache = true) where T : ISettings, new()
        {
            var key = settings.GetSettingKey(keySelector);
            var propInfo = keySelector.GetPropInfo();
            //Duck typing is not supported in C#. That's why we're using dynamic type
            var value = propInfo.GetValue(settings, null);
            return SetSettingAsync(key, value, clearCache);
        }

        /// <summary>
        /// Delete all settings
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        public virtual async Task DeleteSettingAsync<T>() where T : ISettings, new()
        {
            var settingsToDelete = new List<Setting>();
            var allSettings = await GetAllSettingsAsync();
            foreach (var prop in typeof(T).GetProperties())
            {
                var key = typeof(T).Name + "." + prop.Name;
                settingsToDelete.AddRange(allSettings.Where(x => x.Name.Equals(key, StringComparison.InvariantCultureIgnoreCase)));
            }

            await DeleteSettingsAsync(settingsToDelete);
        }

        /// <summary>
        /// Delete settings object
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <typeparam name="TPropType">Property type</typeparam>
        /// <param name="settings">Settings</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="storeId">Store ID</param>
        public virtual async Task DeleteSettingAsync<T, TPropType>(T settings, Expression<Func<T, TPropType>> keySelector) where T : ISettings, new()
        {
            var name = settings.GetSettingKey(keySelector);
            var setting = await GetSettingAsync(name);
            if (setting != null)
                await DeleteSettingAsync(setting);
        }

        /// <summary>
        /// Clear cache
        /// </summary>
        public virtual void ClearCache()
        {
            _cacheManager.RemoveByPattern(SETTINGS_PATTERN_KEY);
        }
        #endregion
    }
}
