﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Xtoman.Domain.Models;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Service
{
    public class EmailSenderService : IEmailSenderService
    {
        private readonly bool UseDefaultCredentials = false;
        private readonly string Host = "cp200.servercap.com";
        private readonly int Port = 587;
        //private readonly int IMAPPort = 995;
        //private readonly int POP3Port = 995;
        private readonly bool EnableSsl = true;
        private readonly NetworkCredential Credentials = new NetworkCredential("no-reply@extoman.com", "1!2@3#Lr");
        private readonly string FromAddress = "no-reply@extoman.com";

        private readonly ISmarterMailService _smarterMailService;
        public EmailSenderService(ISmarterMailService smarterMailService)
        {
            _smarterMailService = smarterMailService;
        }

        /// <summary>
        /// Sends an email
        /// </summary>
        /// <param name="emailAccount">Email account to use</param>
        /// <param name="subject">Subject</param>
        /// <param name="body">Body</param>
        /// <param name="fromAddress">From address</param>
        /// <param name="fromName">From display name</param>
        /// <param name="toAddress">To address</param>
        /// <param name="toName">To display name</param>
        /// <param name="replyTo">ReplyTo address</param>
        /// <param name="replyToName">ReplyTo display name</param>
        /// <param name="bcc">BCC addresses list</param>
        /// <param name="cc">CC addresses list</param>
        /// <param name="attachmentFilePath">Attachment file path</param>
        /// <param name="attachmentFileName">Attachment file name. If specified, then this file name will be sent to a recipient. Otherwise, "AttachmentFilePath" name will be used.</param>
        public virtual bool SendEmail(EmailAccount emailAccount, string subject, string body,
            string fromAddress, string fromName, string toAddress, string toName,
             string replyTo = null, string replyToName = null,
            IEnumerable<string> bcc = null, IEnumerable<string> cc = null,
            string attachmentFilePath = null, string attachmentFileName = null)
        {
            try
            {
                var (smtpClient, mailMessage) = PrepareSmtpClient(emailAccount, subject, body, fromAddress, fromName, toAddress, toName, replyTo, replyToName, bcc, cc, attachmentFilePath, attachmentFileName);
                var result = _smarterMailService.SendEmailAsync(mailMessage).Result;
                //return result.Success;
                //using (smtpClient)
                //{
                //    smtpClient.Send(mailMessage);
                //}
            }
            catch (Exception ex)
            {
                var data = new { emailAccount, subject, body, fromAddress, fromName, toAddress, toName };
                var error = new Exception($"Email send failed: {ex.Message}, Data: {data.Serialize(JsonSerializeType.NewtonsoftJson)}");
                throw error;
            }
            return true;
        }

        private (SmtpClient smtpClient, MailMessage mailMessage) PrepareSmtpClient(EmailAccount emailAccount, string subject, string body, string fromAddress, string fromName, string toAddress, string toName, string replyTo, string replyToName, IEnumerable<string> bcc, IEnumerable<string> cc, string attachmentFilePath, string attachmentFileName)
        {
            if (!fromAddress.HasValue())
                fromAddress = emailAccount != null ? emailAccount.Email : FromAddress;
            var message = new MailMessage { From = new MailAddress(fromAddress, fromName) };
            //from, to, reply to
            message.To.Add(new MailAddress(toAddress, toName));
            if (replyTo.HasValue())
                message.ReplyToList.Add(new MailAddress(replyTo, replyToName));

            //BCC
            if (bcc != null)
            {
                foreach (var address in bcc.Where(bccValue => !string.IsNullOrWhiteSpace(bccValue)))
                    message.Bcc.Add(address.Trim());
            }

            //CC
            if (cc != null)
            {
                foreach (var address in cc.Where(ccValue => !string.IsNullOrWhiteSpace(ccValue)))
                    message.CC.Add(address.Trim());
            }

            //content
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;

            //create  the file attachment for this e-mail message
            if (attachmentFilePath.HasValue() && File.Exists(attachmentFilePath))
            {
                var attachment = new Attachment(attachmentFilePath);
                attachment.ContentDisposition.CreationDate = File.GetCreationTime(attachmentFilePath);
                attachment.ContentDisposition.ModificationDate = File.GetLastWriteTime(attachmentFilePath);
                attachment.ContentDisposition.ReadDate = File.GetLastAccessTime(attachmentFilePath);
                if (attachmentFileName.HasValue())
                {
                    attachment.Name = attachmentFileName;
                }
                message.Attachments.Add(attachment);
            }

            //send email
            var smtpClient = new SmtpClient()
            {
                UseDefaultCredentials = emailAccount != null ? emailAccount.UseDefaultCredentials : UseDefaultCredentials,
                Host = emailAccount != null ? emailAccount.Host : Host,
                Port = emailAccount != null ? emailAccount.Port : Port,
                EnableSsl = emailAccount != null ? emailAccount.EnableSsl : EnableSsl,
                Credentials = emailAccount != null ? emailAccount.UseDefaultCredentials ? CredentialCache.DefaultNetworkCredentials : new NetworkCredential(emailAccount.Username, emailAccount.Password) : Credentials
            };
            return (smtpClient, message);
        }
    }
}