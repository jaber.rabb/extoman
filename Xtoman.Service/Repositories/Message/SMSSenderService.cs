﻿using System;
using System.Threading.Tasks;
using Xtoman.Domain.WebServices.Message.SMS.PayamTak;
using Xtoman.Service.WebServices;
using Xtoman.Utility;

namespace Xtoman.Service
{
    public class SMSSenderService : ISMSSenderService
    {
        private readonly IFarazsmsService _farazsmsService;
        public SMSSenderService(
            IFarazsmsService farazsmsService)
        {
            _farazsmsService = farazsmsService;
        }

        public bool SendSMS(string to, string body)
        {
            try
            {
                var result = _farazsmsService.SendSms(to, body);
                return result;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return false;
            }
        }

        //public async Task<bool> SendSMSAsync(string to, string body)
        //{
            //try
            //{
            //    var result = await _farazsmsService.SendSMSAsync(to, body);
            //    return result;
            //}
            //catch (Exception ex)
            //{
            //    ex.LogError();
                //return false;
            //}
        //}
    }
}
