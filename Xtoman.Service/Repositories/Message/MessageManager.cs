﻿using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Domain.Models;
using Xtoman.Domain.Settings;
using Xtoman.Service.WebServices;

namespace Xtoman.Service
{
    public class MessageManager : IMessageManager
    {
        private readonly IEmailSenderService _emailSenderService;
        private readonly ISMSSenderService _smsSenderService;
        private readonly ITelegramService _telegramService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly IUserNotificationService _userNotificationService;
        private readonly MessageSettings _messageSettings;
        public MessageManager(
            IEmailSenderService emailSenderService,
            ISMSSenderService smsSenderService,
            ITelegramService telegramService,
            IEmailAccountService emailAccountService,
            IUserNotificationService userNotificationService,
            MessageSettings messageSettings)
        {
            _emailSenderService = emailSenderService;
            _smsSenderService = smsSenderService;
            _telegramService = telegramService;
            _emailAccountService = emailAccountService;
            _messageSettings = messageSettings;
            _userNotificationService = userNotificationService;
        }

        public async Task ForgotPasswordMessageAsync(AppUser user, string emailConfirmUrl)
        {
            #region To user
            if (_messageSettings.Email_User_ForgotPassword)
            {
                var emailAccount = await _emailAccountService.GetCachedByIdAsync(_messageSettings.Email_ForgotPassword_EmailAccountId);
                var subject = $"Xtoman - Dear {user.FullName}, Reset your password!";
                string body = $@"<html><body><h2>Hi dear {user.FullName},</h2>
We've received a request to reset your password. If you didn't make the request, just ignore this email. 
Otherwise, you can reset your password using this link:
<a class='btn' href='{emailConfirmUrl}'>Click here to reset your password</a></br>Thank you</br>Xtoman</body></html>";

                _emailSenderService.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.FriendlyName, user.Email,
                    user.FullName ?? user.Email);
            }
            if (_messageSettings.SMS_User_ForgotPassword && user.PhoneNumber.HasValue() && user.PhoneNumberConfirmed)
            {
                await _smsSenderService.SendSMSAsync(user.PhoneNumber,
                    $"Dear {user.FullName}, Reset your password! Open this link: {emailConfirmUrl}");
            }
            #endregion

            #region To admin
            if (_messageSettings.Email_Admin_ForgotPassword)
            {
                //
            }
            if (_messageSettings.SMS_Admin_ForgotPassword)
            {
                //
            }
            if (_messageSettings.Telegram_Admin_ForgotPassword)
            {
            }
            #endregion
        }

        public async Task RegisterPaymentSuccessMessageAsync(AppUser user, object status, ECurrencyType ecAccType)
        {
            // Send notifications to the income receivers tooooooooooooooooooooo.
            switch (ecAccType)
            {
                case ECurrencyType.PAYEER:
                    var payeerPaymentStatus = status as PAYEERPaymentStatus;
                    break;
                case ECurrencyType.PerfectMoney:
                    var perfectMoneyPaymentStatus = status as PerfectMoneyPaymentStatus;
                    break;
            }
            #region To user

            #endregion

            #region To admin

            #endregion
        }

        public async Task PinCodeMessageAsync(AppUser user, string pinCode)
        {
            #region To user

            #endregion
        }

        public async Task RegisterMessageAsync(AppUser user, string emailConfirmUrl)
        {
            #region To user
            if (_messageSettings.Email_User_Registration)
            {
                var emailAccount = await _emailAccountService.GetCachedByIdAsync(_messageSettings.Email_Registration_EmailAccountId);
                var subject = $"Xtoman - Conragulation {user.FullName}, you have been registered!";
                string body = $@"";

                _emailSenderService.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.FriendlyName, user.Email,
                    user.FullName ?? user.Email);
            }
            if (_messageSettings.SMS_User_Registration)
            {
                //
            }
            await _userNotificationService.InsertAsync(new UserNotification
            {
                NotificationType = NotificationType.info,
                Title = $"Conragulation {user.FullName}, you have been registered!",
                Text = "",
                UserId = user.Id
            });
            #endregion

            #region To admin
            if (_messageSettings.Email_Admin_Registration)
            {
                //
            }
            if (_messageSettings.SMS_Admin_Registration)
            {
                //
            }
            if (_messageSettings.Telegram_Admin_Registration)
            {
            }
            #endregion
        }

        public async Task SupportTicketMessageAsync(SupportTicket supportTicket)
        {
            #region To user

            #endregion

            #region To admin

            #endregion
        }

        public async Task WithdrawalPaidAsync(UserWithdrawn userWithdrawn)
        {
            #region To user
            if (_messageSettings.Email_User_WithdrawRequest)
            {

            }
            if (_messageSettings.SMS_User_WithdrawRequest)
            {

            }
            #endregion

            #region To admin

            #endregion
        }

        public async Task WithdrawRequestMessageAsync(AppUser user, UserWithdrawn withdraw)
        {
            #region To user
            if (_messageSettings.Email_User_WithdrawRequest)
            {
                var emailAccount = await _emailAccountService.GetCachedByIdAsync(_messageSettings.Email_WithdrawRequest_EmailAccountId);
                var subject = $"Xtoman - Withdraw request with amount of {withdraw.Amount} sent!";
                string body = $@"";

                _emailSenderService.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.FriendlyName, user.Email,
                    user.FullName ?? user.Email);
            }
            if (_messageSettings.SMS_User_WithdrawRequest)
            {
                //
            }
            await _userNotificationService.InsertAsync(new UserNotification
            {
                NotificationType = NotificationType.info,
                Title = $"Withdraw request with amount of {withdraw.Amount} sent!",
                Text = "",
                UserId = user.Id
            });
            #endregion

            #region To admin
            if (_messageSettings.Email_Admin_WithdrawRequest)
            {
                //
            }
            if (_messageSettings.SMS_Admin_WithdrawRequest)
            {
                //
            }
            if (_messageSettings.Telegram_Admin_WithdrawRequest)
            {
                var telegramAdminText = $@"<strong>Withdraw Request</strong>
Name: <strong>{user.FullName}</strong>
Username: <strong>{user.UserName}</strong>
Email: <strong>{user.Email}</strong>
Phone number: <strong>{user.PhoneNumber}</strong>

Withdraw date: <strong>{withdraw.InsertDate}</strong>
ECurrency: <strong>{withdraw.ECAccType} - {withdraw.EcAccountNo}</strong>
Total balance: <strong>{user.TotalBalance}</strong>
Withdraw amount: <strong>{withdraw.Amount}</strong>
<a href='https://admin.Xtoman.com/Withdraw/Details/{withdraw.Id}'>Details on admin dashboard</a>";

                _telegramService.Token = _messageSettings.Telegram_Admin_WithdrawRequest_Token;
                await _telegramService.SendMessageAsync(telegramAdminText, _messageSettings.Telegram_Admin_WithdrawRequest_ChannelId);
            }
            #endregion
        }
    }
}
