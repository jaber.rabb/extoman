﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xtoman.Domain.Models;

namespace Xtoman.Service
{
    public interface IMessageManager
    {
        Task WithdrawRequestMessageAsync(AppUser user, UserWithdrawn withdraw);
        Task RegisterMessageAsync(AppUser user, string emailConfirmUrl);
        Task ForgotPasswordMessageAsync(AppUser user, string emailConfirmUrl);
        Task RegisterPaymentSuccessMessageAsync(AppUser user, object status, ECurrencyType ecAccType);
        Task SupportTicketMessageAsync(SupportTicket supportTicket);
        Task PinCodeMessageAsync(AppUser user, string pinCode);
        Task WithdrawalPaidAsync(UserWithdrawn userWithdrawn);
    }
}
