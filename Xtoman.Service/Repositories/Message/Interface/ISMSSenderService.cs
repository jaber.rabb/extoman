﻿using System.Threading.Tasks;

namespace Xtoman.Service
{
    public interface ISMSSenderService
    {
        bool SendSMS(string to, string body);
        //Task<bool> SendSMSAsync(string to, string body);
    }
}
